Require Import List String Map MapTypes Values Ast ZArith Lia.
Import ListNotations.

Inductive External := 
| EVar : ConstOrVar -> External
| EFDecl : (list (String_Equality.t * mode * name)) -> External.

Record env := mkenv
{
  localVars : StringMapModule.t ConstOrVar
; globalVars : StringMapModule.t ConstOrVar
; funDefs : StringMapModule.t element_declararif
; localfunDecls : StringMapModule.t (list (ident * mode * name))
; externals : StringMapModule.t (StringMapModule.t External)
}.

Definition record_select (rec: StringMapModule.t value) id := StringMapModule.get id rec.

Definition array_select (arr: NatMapModule.t value) id := NatMapModule.get id arr.

Definition id_select_from_value v id := match v with
| Undefined => Some Undefined
| Int _ => None
| Bool _ => None
| ArrayV _ _ => None
| STR _ => None
| RecordV r => ( match record_select r id with | None => Some Undefined | res => res end)
end.

Definition idx_select_from_value v id := match v with
| Undefined => Some Undefined
| Int v => Some (Int v)
| Bool v => Some (Bool v)
| ArrayV a default => ( match array_select a id with | None => Some (default) | res => res end)
| STR _ => None
| RecordV _ => None
end.

Inductive varType :=
| Value (v: ConstOrVar)
| DefinedFunction (f: element_declararif)
| FunctionDeclaration : list (ident * mode * name) -> varType.

Definition getIdValue id env := 
            match StringMapModule.get id env.(localVars) with
              | None => 
                  (
                    match StringMapModule.get id env.(globalVars) with
                      | None => 
                          (
                            match StringMapModule.get id env.(funDefs) with
                            | None =>
                              (
                                 match StringMapModule.get id env.(localfunDecls) with
                                 | None => None
                                 | Some r => Some (FunctionDeclaration r)
                                 end           
                              )
                            | Some r => Some (DefinedFunction r)
                            end
                          )
                      | Some r => Some (Value r)
                    end
                  )
              | Some r => Some (Value r)
            end.

Theorem getIdValue_None : 
  forall env id, 
    StringMapModule.get id env.(localVars) = None /\ 
    StringMapModule.get id env.(globalVars) = None /\ 
    StringMapModule.get id env.(funDefs) = None /\ 
    StringMapModule.get id env.(localfunDecls) = None <->
    getIdValue id env = None.
Proof.
split.
intros.
destruct H.
destruct H0.
destruct H1.
unfold getIdValue.
rewrite H.
rewrite H0.
rewrite H1.
rewrite H2.
auto.
unfold getIdValue.
case_eq (StringMapModule.get id (localVars env0)).
intros; inversion H0.
case_eq (StringMapModule.get id (globalVars env0)).
intros; inversion H1.
case_eq (StringMapModule.get id (funDefs env0)).
intros; inversion H2.
case_eq (StringMapModule.get id (localfunDecls env0)).
intros; inversion H3.
auto.
Qed.

Definition setIdValue id value env := 
    match StringMapModule.get id env.(localVars) with
      | None => 
          (
            match StringMapModule.get id env.(globalVars) with
              | None => None
              | Some ((VariableValue _)) => 
                  Some {|
                    localVars := env.(localVars);
                    globalVars := StringMapModule.set id (VariableValue value) env.(globalVars);
                    funDefs := env.(funDefs);
                    localfunDecls := env.(localfunDecls);
                    externals := env.(externals)
                  |}
              | _ => None
            end
          )
      | Some ((VariableValue _)) => 
          Some {|
            localVars := StringMapModule.set id (VariableValue value) env.(localVars);
            globalVars := env.(globalVars);
            funDefs := env.(funDefs);
            localfunDecls := env.(localfunDecls);
            externals := env.(externals)
          |}
      | Some _ => None
    end.

Lemma get_set_correct : forall env env' id id' v, 
    setIdValue id v env = Some env' -> 
    getIdValue id' env' =   
    if String_Equality.eq id' id then 
      (
        Some (Value (VariableValue v))
      )
      else getIdValue id' env.
Proof.
unfold setIdValue.
unfold getIdValue.
intros env0 env' id.
case_eq (StringMapModule.get id (localVars env0)).
induction c; intros; inversion H0.
simpl.
rewrite StringMapModule.gsspec.
case_eq (String_Equality.eq id' id); auto.
case_eq (StringMapModule.get id (globalVars env0)).
induction c; intros; inversion H1.
simpl.
case_eq (String_Equality.eq id' id); auto.
intros.
apply String_Equality.eq_spec_true in H2.
rewrite H2 in *.
rewrite H0.
rewrite StringMapModule.gsspec.
rewrite StringMapModule.Xeq_refl; auto.
rewrite StringMapModule.gsspec.
intros.
rewrite H2; auto.
intros.
inversion H1.
Qed.

Lemma get_set_variable : forall id env v, (exists v', getIdValue id env = Some (Value (VariableValue v'))) <-> (exists env', setIdValue id v env = Some env').
Proof.
unfold getIdValue.
unfold setIdValue.
split; intros; destruct H.
induction (StringMapModule.get id (localVars env0)).
inversion H.
exists ({|
    localVars := StringMapModule.set id (VariableValue v) (localVars env0);
    globalVars := globalVars env0;
    funDefs := funDefs env0;
    localfunDecls := localfunDecls env0;
    externals := externals env0 |}); auto.
induction (StringMapModule.get id (globalVars env0)).
inversion H.
exists ({|
    localVars := localVars env0;
    globalVars := StringMapModule.set id (VariableValue v) (globalVars env0);
    funDefs := funDefs env0;
    localfunDecls := localfunDecls env0;
    externals := externals env0 |}); auto.
induction (StringMapModule.get id (funDefs env0)).
inversion H.
induction (StringMapModule.get id (localfunDecls env0)); inversion H.
induction (StringMapModule.get id (localVars env0)).
induction a.
inversion H.
inversion H.
exists v0; auto.
induction (StringMapModule.get id (globalVars env0)).
induction a.
inversion H.
inversion H.
exists v0; auto.
inversion H.
Qed.

Definition record_set (rec: list (string * value)) id v := StringMapModule.set id v rec.

Definition array_set (arr: list (nat * value)) id v := NatMapModule.set id v arr.

Definition id_set_from_value v id val := match v with
| Undefined => Some (RecordV [(id, val)])
| Int _ => None
| Bool _ => None
| ArrayV _ _ => None
| STR _ => None
| RecordV r => Some (RecordV (StringMapModule.set id val r))
end.

Theorem id_select_set_correct: forall id id' vi v vi',
  id_set_from_value vi id v = Some vi' ->
  id_select_from_value vi' id' = if String_Equality.eq id' id then Some v else id_select_from_value vi id'.
Proof.
induction vi; simpl; intros; inversion H; simpl.
unfold record_select.
simpl.
case_eq (String_Equality.eq id' id); auto.
unfold record_select.
rewrite StringMapModule.gsspec.
case_eq (String_Equality.eq id' id); auto.
Qed.

Definition idx_set_from_value v id val := match v with
| Undefined => Some (ArrayV [(id, val)] Undefined)
| Int v => Some (ArrayV [(id, val)] (Int v))
| Bool v => Some (ArrayV [(id, val)] (Bool v))
| ArrayV a d => Some (ArrayV (NatMapModule.set id val a) d)
| STR _ => None
| RecordV _ => None
end.

Theorem idx_select_set_correct: forall id id' vi v vi',
  idx_set_from_value vi id v = Some vi' ->
  idx_select_from_value vi' id' = if Nat_Equality.eq id' id then Some v else idx_select_from_value vi id'.
Proof.
induction vi; simpl; intros; inversion H; simpl; unfold array_select; simpl.
case_eq (Nat_Equality.eq id' id); auto.
case_eq (Nat_Equality.eq id' id); auto.
case_eq (Nat_Equality.eq id' id); auto.
rewrite NatMapModule.gsspec.
case_eq (Nat_Equality.eq id' id); auto.
Qed.

Inductive ids_select_from_value : value -> list string -> value -> Prop :=
| ids_select_nil : forall v, ids_select_from_value v [] v
| ids_select_cons : forall v' v'' v hid tid, 
    id_select_from_value v hid = Some v' ->
    ids_select_from_value v' tid v'' -> 
    ids_select_from_value v (hid::tid) v''.

Example ids_select_from_value_ex: ids_select_from_value (RecordV [("a"%string, RecordV [("c"%string, Int 5)])]) ["a"%string; "c"%string] (Int 5).
Proof.
apply (ids_select_cons (RecordV [("c"%string, Int 5)])).
simpl.
auto.
apply (ids_select_cons (Int 5)).
simpl.
auto.
constructor.
Qed.

Fixpoint ids_select_from_value_f v ids := match ids with
| [] => Some v
| hid::tid => 
  (
    match id_select_from_value v hid with
    | None => None
    | Some v' => ids_select_from_value_f v' tid
    end
  )
end.

Theorem ids_select_from_value_f_correct: forall ids v v',
  ids_select_from_value v ids v' <->
  ids_select_from_value_f v ids = Some v'.
Proof.
induction ids.
split.
intros.
inversion H.
simpl.
auto.
simpl.
intros.
inversion H.
constructor.
split.
intros.
inversion H.
apply IHids in H5.
simpl.
rewrite H3.
auto.
simpl.
case_eq (id_select_from_value v a).
intros.
apply IHids in H0.
econstructor; eauto.
intros.
inversion H0.
Qed.

Inductive idxs_select_from_value : value -> list nat -> value -> Prop :=
| idxs_select_nil : forall v, idxs_select_from_value v [] v
| idxs_select_cons : forall v' v'' v hid tid, 
    idx_select_from_value v hid = Some v' ->
    idxs_select_from_value v' tid v'' -> 
    idxs_select_from_value v (hid::tid) v''.

Example idxs_select_from_value_ex: idxs_select_from_value (ArrayV [(0, ArrayV [(0, Int 5)] Undefined)] Undefined) [0; 0] (Int 5).
Proof.
apply (idxs_select_cons (ArrayV [(0, Int 5)] Undefined)).
simpl.
auto.
apply (idxs_select_cons (Int 5)).
simpl.
auto.
constructor.
Qed.

Fixpoint idxs_select_from_value_f v ids := match ids with
| [] => Some v
| hid::tid => 
  (
    match idx_select_from_value v hid with
    | None => None
    | Some v' => idxs_select_from_value_f v' tid
    end
  )
end.

Theorem idxs_select_from_value_f_correct: forall ids v v',
  idxs_select_from_value v ids v' <->
  idxs_select_from_value_f v ids = Some v'.
Proof.
induction ids.
split.
intros.
inversion H.
simpl.
auto.
simpl.
intros.
inversion H.
constructor.
split.
intros.
inversion H.
apply IHids in H5.
simpl.
rewrite H3.
auto.
simpl.
case_eq (idx_select_from_value v a).
intros.
apply IHids in H0.
econstructor; eauto.
intros.
inversion H0.
Qed.

(* 
  a : record (a = RecordV [])
  
  a.b.c = 5
      a = RecordV [(b, RecordV [(c , Int 5)])]
*)

Inductive ids_set_from_value : value -> list string -> value -> value -> Prop :=
| ids_set_nil: forall vi vf, ids_set_from_value vi [] vf vf
| ids_set_cons: forall vi' vf vi val vf' hid tid, 
    id_select_from_value vi hid = Some vi' ->
    ids_set_from_value vi' tid val vf ->
    id_set_from_value vi hid vf = Some vf' ->
    ids_set_from_value vi (hid::tid) val vf'.

Example ids_set_from_value_ex: ids_set_from_value (RecordV [("c"%string, Int 5)]) ["a"%string; "b"%string] (Bool true) (RecordV [("c"%string, Int 5); ("a"%string, RecordV [("b"%string, Bool true)])]).
Proof.
apply (ids_set_cons Undefined (RecordV [("b"%string, Bool true)])).
simpl.
auto.
apply (ids_set_cons Undefined (Bool true)).
simpl.
auto.
constructor.
simpl.
auto.
simpl.
auto.
Qed.

Fixpoint ids_set_from_value_f v ids val:= match ids with
| [] => Some val
| h::t => 
  (
    match id_select_from_value v h with
    | None => None
    | Some v' => 
      (
        match ids_set_from_value_f v' t val with
        | None => None
        | Some v'' => id_set_from_value v h v''
        end
      )
    end
  )
end.

Theorem ids_set_from_value_f_correct: forall ids v v' v'',
  ids_set_from_value v ids v' v'' <->
  ids_set_from_value_f v ids v' = Some v''.
Proof.
induction ids.
split.
intros.
inversion H.
rewrite H3 in *.
simpl.
auto.
simpl.
intros.
inversion H.
constructor.
split.
intros.
inversion H.
simpl.
rewrite H2.
eapply IHids in H4.
rewrite H4.
auto.
simpl.
case_eq (id_select_from_value v a).
intro.
case_eq (ids_set_from_value_f v0 ids v').
intros.
apply IHids in H.
econstructor; eauto.
intros.
inversion H1.
intros.
inversion H0.
Qed.

Lemma ids_get_set_f_correct: forall ids vi ass_v vf, 
    ids_set_from_value_f vi ids ass_v = Some vf -> 
    ids_select_from_value_f vf ids = Some ass_v.
Proof.
induction ids; simpl.
auto.
intro.
case_eq (id_select_from_value vi a).
intros v H ass_v vf.
case_eq (ids_set_from_value_f v ids ass_v).
intros.
erewrite (id_select_set_correct); eauto.
assert (String_Equality.eq a a = true).
apply String_Equality.eq_spec_true.
auto.
rewrite H2.
eapply IHids; eauto.
intros.
inversion H1.
intros.
inversion H0.
Qed.  

Inductive idxs_set_from_value : value -> list nat -> value -> value -> Prop :=
| idxs_set_nil: forall vi vf, idxs_set_from_value vi [] vf vf
| idxs_set_cons: forall vi' vf vi val vf' hid tid, 
    idx_select_from_value vi hid = Some vi' ->
    idxs_set_from_value vi' tid val vf ->
    idx_set_from_value vi hid vf = Some vf' ->
    idxs_set_from_value vi (hid::tid) val vf'.
    
Example idxs_set_from_value_ex: idxs_set_from_value (ArrayV [(1, Int 5)] Undefined) [0; 1] (Bool true) (ArrayV [(1, Int 5); (0, ArrayV [(1, Bool true)] Undefined)] Undefined).
Proof.
apply (idxs_set_cons Undefined (ArrayV [(1, Bool true)] Undefined)).
simpl.
auto.
apply (idxs_set_cons Undefined (Bool true)).
simpl.
auto.
constructor.
simpl.
auto.
simpl.
auto.
Qed.

Fixpoint idxs_set_from_value_f v ids val:= match ids with
| [] => Some val
| h::t => 
  (
    match idx_select_from_value v h with
    | None => None
    | Some v' => 
      (
        match idxs_set_from_value_f v' t val with
        | None => None
        | Some v'' => idx_set_from_value v h v''
        end
      )
    end
  )
end.

Theorem idxs_set_from_value_f_correct: forall ids v v' v'',
  idxs_set_from_value v ids v' v'' <->
  idxs_set_from_value_f v ids v' = Some v''.
Proof.
induction ids.
split.
intros.
inversion H.
rewrite H3 in *.
simpl.
auto.
simpl.
intros.
inversion H.
constructor.
split.
intros.
inversion H.
simpl.
rewrite H2.
eapply IHids in H4.
rewrite H4.
auto.
simpl.
case_eq (idx_select_from_value v a).
intro.
case_eq (idxs_set_from_value_f v0 ids v').
intros.
apply IHids in H.
econstructor; eauto.
intros.
inversion H1.
intros.
inversion H0.
Qed.

Definition getIdsValue ids env := match ids with
| [] => None
| hid::[] => getIdValue hid env
| hid::hid0::tid0  => 
    match StringMapModule.get hid env.(localVars) with
    | None => 
        (
          match StringMapModule.get hid env.(globalVars) with
          | None =>
            (
              match StringMapModule.get hid env.(funDefs) with
              | None =>
                (
                  match StringMapModule.get hid env.(localfunDecls) with
                  | None =>
                    (
                      match StringMapModule.get hid env.(externals) with
                      | None => None
                      | Some lib => 
                        (
                          match StringMapModule.get hid0 lib with
                          | None => None
                          | Some (EVar (ConstantValue v)) => 
                            (
                              match ids_select_from_value_f v tid0 with
                              | Some v' => Some (Value (ConstantValue v'))
                              | None => None
                              end
                            )
                          | Some (EVar (VariableValue v)) => 
                            (
                              match ids_select_from_value_f v tid0 with
                              | Some v' => Some (Value (VariableValue v'))
                              | None => None
                              end
                            )
                          | Some (EFDecl sig) => 
                              (
                                match tid0 with
                                | [] => Some (FunctionDeclaration sig)
                                | _ => None
                                end
                              )
                          end
                        )
                      end
                    )
                  | Some _ => None
                  end
                )
              | Some _ => None
              end
            )
          | Some (ConstantValue v) => 
              (
                match ids_select_from_value_f v (hid0::tid0) with
                | Some v' => Some (Value (ConstantValue v'))
                | None => None
                end
              )
          | Some (VariableValue v) => 
              (
                match ids_select_from_value_f v (hid0::tid0) with
                | Some v' => Some (Value (VariableValue v'))
                | None => None
                end
              )
          end
        )
      | Some (ConstantValue v) => 
          (
            match ids_select_from_value_f v (hid0::tid0) with
            | Some v' => Some (Value (ConstantValue v'))
            | None => None
            end
          )
      | Some (VariableValue v) => 
          (
            match ids_select_from_value_f v (hid0::tid0) with
            | Some v' => Some (Value (VariableValue v'))
            | None => None
            end
          )
      end
end.

Definition setIdsValue ids s_value env := match ids with
| [] => None
| hid::[] => setIdValue hid s_value env
| hid::hid0::tid0 => 
    (
      match StringMapModule.get hid env.(localVars) with
      | None => 
        (
          match StringMapModule.get hid env.(globalVars) with
          | None =>
            (
              match StringMapModule.get hid env.(funDefs) with
              | None =>
                (
                  match StringMapModule.get hid env.(localfunDecls) with
                  | Some _ => None
                  | None => 
                    (
                      match StringMapModule.get hid env.(externals) with
                      | None => 
                        (
                          match ids_set_from_value_f Undefined tid0 s_value with
                          | None => None
                          | Some f_value => 
                              Some ({|
                                localVars := env.(localVars);
                                globalVars := env.(globalVars);
                                funDefs := env.(funDefs);
                                localfunDecls := env.(localfunDecls);
                                externals := StringMapModule.set hid (StringMapModule.set hid0 (EVar (VariableValue f_value)) []) env.(externals)
                              |})
                          end
                        )
                      | Some lib => 
                          (
                            match StringMapModule.get hid0 lib with
                            | None => 
                              (
                                  match ids_set_from_value_f Undefined tid0 s_value with
                                  | None => None
                                  | Some f_value => 
                                      Some ({|
                                        localVars := env.(localVars);
                                        globalVars := env.(globalVars);
                                        funDefs := env.(funDefs);
                                        localfunDecls := env.(localfunDecls);
                                        externals := StringMapModule.set hid (StringMapModule.set hid0 (EVar (VariableValue f_value)) lib) env.(externals)
                                      |})
                                  end
                                )
                            | Some (EVar (ConstantValue v)) => None
                            | Some (EVar (VariableValue v)) => 
                              (
                                match ids_set_from_value_f v tid0 s_value with
                                | Some v' => Some ({|
                                              localVars := env.(localVars);
                                              globalVars := env.(globalVars);
                                              localfunDecls := env.(localfunDecls);
                                              funDefs := env.(funDefs);
                                              externals := StringMapModule.set hid (StringMapModule.set hid0 (EVar (VariableValue v')) lib) env.(externals)
                                            |})
                                | None => None
                                end
                              )
                            | Some (EFDecl _) => None
                            end
                        )
                      end
                    )
                  end
                )
              | Some _ => None
              end
            )
         | Some ((ConstantValue v)) => None
         | Some ((VariableValue v)) => 
            (
              match ids_set_from_value_f v (hid0::tid0) s_value with
              | Some v' => Some ({|
                            localVars := env.(localVars);
                            globalVars := StringMapModule.set hid (VariableValue v') env.(globalVars);
                            funDefs := env.(funDefs);
                            localfunDecls := env.(localfunDecls);
                            externals := env.(externals)
                          |})
              | None => None
              end
            )
        end)
      | Some ((ConstantValue v)) => None
      | Some ((VariableValue v)) => 
        (
          match ids_set_from_value_f v (hid0::tid0) s_value with
          | Some v' => Some ({|
                            localVars := StringMapModule.set hid (VariableValue v') env.(localVars);
                            globalVars := env.(globalVars);
                            localfunDecls := env.(localfunDecls);
                            funDefs := env.(funDefs);
                            externals := env.(externals)
                          |})
          | None => None
          end
        )
      end)
end.

Theorem get_set_ids_correct: forall ids env a_value env', 
  setIdsValue ids a_value env = Some env' -> (getIdsValue ids env' = 
  Some (Value (VariableValue a_value)) ).
Proof.
unfold setIdsValue.
unfold getIdsValue.
intro.
case_eq ids.
intros.
inversion H0.
intros k l Hids env0.
case_eq l.
intros.
apply (get_set_correct env0 env' k k) in H0.
rewrite StringMapModule.Xeq_refl in H0.
auto.
case_eq (StringMapModule.get k (localVars env0)).
induction c; [intros; inversion H1|].
intros H t l0 H0 a_value.
case_eq (ids_set_from_value_f v (t :: l0) a_value); intros; inversion H2.
simpl.
rewrite StringMapModule.gsspec.
rewrite StringMapModule.Xeq_refl.
simpl in H1.
case_eq (id_select_from_value v t); intros; rewrite H3 in *.
case_eq (ids_set_from_value_f v1 l0 a_value); intros; rewrite H5 in *.
erewrite id_select_set_correct; eauto.
rewrite StringMapModule.Xeq_refl.
erewrite ids_get_set_f_correct; eauto.
inversion H1.
inversion H1.
case_eq (StringMapModule.get k (globalVars env0)).
induction c; [intros; inversion H2|].
intros H H0 t l0 H1 a_value.
case_eq (ids_set_from_value_f v (t :: l0) a_value); intros; inversion H3.
simpl.
rewrite H0.
rewrite StringMapModule.gsspec.
rewrite StringMapModule.Xeq_refl.
simpl in H2.
case_eq (id_select_from_value v t); intros; rewrite H4 in *.
case_eq (ids_set_from_value_f v1 l0 a_value); intros; rewrite H6 in *.
erewrite id_select_set_correct; eauto.
rewrite StringMapModule.Xeq_refl.
erewrite ids_get_set_f_correct; eauto.
inversion H2.
inversion H2.
case_eq (StringMapModule.get k (funDefs env0)); [intros; inversion H3|].
case_eq (StringMapModule.get k (localfunDecls env0)); [intros; inversion H4|].
case_eq (StringMapModule.get k (externals env0)).
intros t H H0 H1 H2 H3 t0 l0 H4.
case_eq (StringMapModule.get t0 t).
induction e; intros.
induction c; [inversion H6|].
case_eq (ids_set_from_value_f v l0 a_value); intros; rewrite H7 in *; inversion H6; simpl.
rewrite H3.
rewrite H2.
rewrite H1.
rewrite H0.
rewrite StringMapModule.gsspec.
rewrite StringMapModule.Xeq_refl.
rewrite StringMapModule.gsspec.
rewrite StringMapModule.Xeq_refl.
erewrite ids_get_set_f_correct; eauto.
inversion H6.
intros.
case_eq (ids_set_from_value_f Undefined l0 a_value); intros; rewrite H7 in *; inversion H6.
simpl.
rewrite H3.
rewrite H2.
rewrite H1.
rewrite H0.
rewrite StringMapModule.gsspec.
rewrite StringMapModule.Xeq_refl.
rewrite StringMapModule.gsspec.
rewrite StringMapModule.Xeq_refl.
erewrite ids_get_set_f_correct; eauto.
intros.
case_eq (ids_set_from_value_f Undefined l0 a_value); intros; rewrite H6 in *; inversion H5.
simpl.
rewrite H3.
rewrite H2.
rewrite H1.
rewrite H0.
rewrite StringMapModule.gsspec.
rewrite StringMapModule.Xeq_refl.
simpl.
rewrite StringMapModule.Xeq_refl.
erewrite ids_get_set_f_correct; eauto.
Qed.

Theorem funDeclarationIsNotFetchable : forall id id' t p env,
    getIdValue id env = Some (FunctionDeclaration p) ->
    getIdsValue (id::id'::t) env = None.
Proof.
unfold getIdValue.
unfold getIdsValue.
intros.
case_eq (StringMapModule.get id (localVars env0)); intros; rewrite H0 in H; [inversion H|].
case_eq (StringMapModule.get id (globalVars env0)); intros; rewrite H1 in H; [inversion H|].
case_eq (StringMapModule.get id (funDefs env0)); intros; rewrite H2 in H; [inversion H|].
case_eq (StringMapModule.get id (localfunDecls env0)); intros; rewrite H3 in H; [|inversion H]; auto.
Qed.

Theorem funDefinitionIsNotFetchable : forall id id' t p env,
    getIdValue id env = Some (DefinedFunction p) ->
    getIdsValue (id::id'::t) env = None.
Proof.
unfold getIdsValue.
unfold getIdValue.
intros.
case_eq (StringMapModule.get id (localVars env0)); intros; rewrite H0 in H; [inversion H|].
case_eq (StringMapModule.get id (globalVars env0)); intros; rewrite H1 in H; [inversion H|].
case_eq (StringMapModule.get id (funDefs env0)); intros; rewrite H2 in H; auto.
case_eq (StringMapModule.get id (localfunDecls env0)); intros; rewrite H3 in H; [|inversion H]; auto.
Qed.

Definition declareLocalVar id value env := 
      match StringMapModule.get id env.(localVars) with
        | None => 
            (
              match StringMapModule.get id env.(funDefs) with
              | None =>
                (
                   match StringMapModule.get id env.(localfunDecls) with
                   | None => 
                      (
                        match StringMapModule.get id env.(externals) with
                        | Some _ => None
                        | None =>
                            Some ({|
                                  localVars := StringMapModule.set id value env.(localVars);
                                  globalVars := env.(globalVars);
                                  localfunDecls := env.(localfunDecls);
                                  funDefs := env.(funDefs);
                                  externals := env.(externals)
                                |})
                        end
                      )
                   | Some r => None
                   end           
                )
              | Some r => None
              end
            )
        | Some r => None
      end.

Lemma declareLocalVarCorrect : forall ids id env env' val, 
    declareLocalVar id val env = Some env' ->
    getIdsValue ids env' = 
      (
        match ids with
        | [] => None
        | hids::tids =>
          (
            if String_Equality.eq hids id then 
            ( match val with
              | ConstantValue v => 
                (
                  match ids_select_from_value_f v tids with
                  | Some v' => Some (Value (ConstantValue v'))
                  | None => None
                  end
                )
              | VariableValue v => 
                (
                  match ids_select_from_value_f v tids with
                  | Some v' => Some (Value (VariableValue v'))
                  | None => None
                  end
                )
              end
            ) else getIdsValue ids env
          )
        end
      ).
Proof.
intro.
case ids.
simpl; auto.
unfold declareLocalVar.
unfold getIdsValue.
intros.
case_eq l; intros.
unfold getIdValue.
case_eq (String_Equality.eq t id); intros.
apply String_Equality.eq_spec_true in H1; subst.
case_eq (StringMapModule.get id (localVars env0)); intros; rewrite H0 in H; [inversion H|].
case_eq (StringMapModule.get id (funDefs env0)); intros; rewrite H1 in H; [inversion H|].
case_eq (StringMapModule.get id (localfunDecls env0)); intros; rewrite H2 in H; [inversion H|].
case_eq (StringMapModule.get id (externals env0)); intros; rewrite H3 in H; inversion H; simpl.
rewrite StringMapModule.gsspec.
rewrite StringMapModule.Xeq_refl; auto.
induction val; auto.
case_eq (StringMapModule.get id (localVars env0)); intros; rewrite H2 in H; [inversion H|].
case_eq (StringMapModule.get id (funDefs env0)); intros; rewrite H3 in H; [inversion H|].
case_eq (StringMapModule.get id (localfunDecls env0)); intros; rewrite H4 in H; [inversion H|].
case_eq (StringMapModule.get id (externals env0)); intros; rewrite H5 in H; inversion H; simpl.
rewrite StringMapModule.gsspec.
rewrite H1.
auto.
case_eq (String_Equality.eq t id); intros.
apply String_Equality.eq_spec_true in H1.
rewrite H1 in *.
case_eq (StringMapModule.get id (localVars env0)); intros; rewrite H2 in H; [inversion H|].
case_eq (StringMapModule.get id (funDefs env0)); intros; rewrite H3 in H; [inversion H|].
case_eq (StringMapModule.get id (localfunDecls env0)); intros; rewrite H4 in H; [inversion H|].
case_eq (StringMapModule.get id (externals env0)); intros; rewrite H5 in H; inversion H; simpl.
rewrite StringMapModule.gsspec.
rewrite StringMapModule.Xeq_refl; auto.
case_eq (StringMapModule.get id (localVars env0)); intros; rewrite H2 in H; [inversion H|].
case_eq (StringMapModule.get id (funDefs env0)); intros; rewrite H3 in H; [inversion H|].
case_eq (StringMapModule.get id (localfunDecls env0)); intros; rewrite H4 in H; [inversion H|].
case_eq (StringMapModule.get id (externals env0)); intros; rewrite H5 in H; inversion H; simpl.
rewrite StringMapModule.gsspec.
rewrite H1.
auto.
Qed.

Definition declareGlobalVar id value env := 
      match StringMapModule.get id env.(globalVars) with
        | None => 
            (
              match StringMapModule.get id env.(funDefs) with
              | None =>
                (
                   match StringMapModule.get id env.(localfunDecls) with
                   | None => 
                      (
                        match StringMapModule.get id env.(externals) with
                        | Some _ => None
                        | None =>
                            Some ({|
                                  localVars := env.(localVars);
                                  globalVars := StringMapModule.set id value env.(globalVars);
                                  localfunDecls := env.(localfunDecls);
                                  funDefs := env.(funDefs);
                                  externals := env.(externals)
                                |})
                        end
                      )
                   | Some r => None
                   end           
                )
              | Some r => None
              end
            )
        | Some r => None
      end.

Lemma declareGlobalVarCorrect : forall ids id env env' val, 
    declareGlobalVar id val env = Some env' ->
    StringMapModule.get id (localVars env) = None ->
    getIdsValue ids env' = 
      (
        match ids with
        | [] => None
        | hids::tids =>
          (
            if String_Equality.eq hids id then 
            ( match val with
              | ConstantValue v => 
                (
                  match ids_select_from_value_f v tids with
                  | Some v' => Some (Value (ConstantValue v'))
                  | None => None
                  end
                )
              | VariableValue v => 
                (
                  match ids_select_from_value_f v tids with
                  | Some v' => Some (Value (VariableValue v'))
                  | None => None
                  end
                )
              end
            ) else getIdsValue ids env
          )
        end
      ).
Proof.
intro.
case ids.
simpl; auto.
unfold declareGlobalVar.
unfold getIdsValue.
intros.
case_eq l; intros.
unfold getIdValue.
case_eq (String_Equality.eq t id); intros.
apply String_Equality.eq_spec_true in H2.
rewrite H2 in *.
case_eq (StringMapModule.get id (globalVars env0)); intros; rewrite H3 in H; [inversion H|].
case_eq (StringMapModule.get id (funDefs env0)); intros; rewrite H4 in H; [inversion H|].
case_eq (StringMapModule.get id (localfunDecls env0)); intros; rewrite H5 in H; [inversion H|].
case_eq (StringMapModule.get id (externals env0)); intros; rewrite H6 in H; inversion H; simpl.
rewrite StringMapModule.gsspec.
rewrite StringMapModule.Xeq_refl; auto.
rewrite H0.
induction val; auto.
case_eq (StringMapModule.get id (globalVars env0)); intros; rewrite H3 in H; [inversion H|].
case_eq (StringMapModule.get id (funDefs env0)); intros; rewrite H4 in H; [inversion H|].
case_eq (StringMapModule.get id (localfunDecls env0)); intros; rewrite H5 in H; [inversion H|].
case_eq (StringMapModule.get id (externals env0)); intros; rewrite H6 in H; inversion H; simpl.
rewrite StringMapModule.gsspec.
induction (StringMapModule.get t (localVars env0)); auto.
rewrite H2.
auto.
case_eq (String_Equality.eq t id); intros.
apply String_Equality.eq_spec_true in H2.
rewrite H2 in *.
case_eq (StringMapModule.get id (globalVars env0)); intros; rewrite H3 in H; [inversion H|].
case_eq (StringMapModule.get id (funDefs env0)); intros; rewrite H4 in H; [inversion H|].
case_eq (StringMapModule.get id (localfunDecls env0)); intros; rewrite H5 in H; [inversion H|].
case_eq (StringMapModule.get id (externals env0)); intros; rewrite H6 in H; inversion H; simpl.
rewrite StringMapModule.gsspec.
rewrite StringMapModule.Xeq_refl; rewrite H0; auto.
case_eq (StringMapModule.get id (globalVars env0)); intros; rewrite H3 in H; [inversion H|].
case_eq (StringMapModule.get id (funDefs env0)); intros; rewrite H4 in H; [inversion H|].
case_eq (StringMapModule.get id (localfunDecls env0)); intros; rewrite H5 in H; [inversion H|].
case_eq (StringMapModule.get id (externals env0)); intros; rewrite H6 in H; inversion H; simpl.
rewrite StringMapModule.gsspec.
induction (StringMapModule.get t (localVars env0)); auto.
rewrite H2; auto.
Qed.

Definition defineFunction ed env := 
      match StringMapModule.get ed.(f_name) env.(localVars) with
        | None => 
            (
              match StringMapModule.get ed.(f_name) env.(globalVars) with
                | None => 
                    (
                      match StringMapModule.get ed.(f_name) env.(externals) with
                      | None =>
                        (
                          match StringMapModule.get ed.(f_name) env.(funDefs) with
                          | Some _ => None
                          | None =>
                            (
                              Some ({|
                                localVars := env.(localVars);
                                globalVars := env.(globalVars);
                                localfunDecls := env.(localfunDecls);
                                funDefs := StringMapModule.set ed.(f_name) ed env.(funDefs);
                                externals := env.(externals)
                              |})
                            )
                          end
                        )
                      | Some r => None
                      end
                    )
                | Some r => None
              end
            )
        | Some r => None
      end.

Lemma defineFunctionCorrect : forall id env env' ed, 
    defineFunction ed env = Some env' ->
    getIdValue id env' = 
      if String_Equality.eq id ed.(f_name) then
        Some (DefinedFunction ed)
      else
        getIdValue id env.
Proof.
unfold getIdValue.
unfold defineFunction.
intros.
case_eq (StringMapModule.get (f_name ed) (localVars env0)); intros; rewrite H0 in H; [inversion H|].
case_eq (StringMapModule.get (f_name ed) (globalVars env0)); intros; rewrite H1 in H; [inversion H|].
case_eq (StringMapModule.get (f_name ed) (externals env0)); intros; rewrite H2 in H; [inversion H|].
case_eq (StringMapModule.get (f_name ed) (funDefs env0)); intros; rewrite H3 in H; inversion H; simpl.
rewrite StringMapModule.gsspec.
case_eq (String_Equality.eq id (f_name ed)); auto; intros.
apply String_Equality.eq_spec_true in H4.
rewrite H4 in *.
rewrite H0.
rewrite H1.
auto.
Qed.

Lemma defineFunctionSucced : forall env ed, 
    StringMapModule.get ed.(f_name) env.(localVars) = None /\
    StringMapModule.get ed.(f_name) env.(globalVars) = None /\
    StringMapModule.get ed.(f_name) env.(externals) = None /\
    StringMapModule.get ed.(f_name) env.(funDefs) = None <->
    exists e, defineFunction ed env = Some e.
Proof.
intros.
unfold defineFunction.
split; intros.
destruct H.
destruct H0.
destruct H1.
rewrite H, H0, H1, H2; auto.
exists {|
    localVars := localVars env0;
    globalVars := globalVars env0;
    funDefs := StringMapModule.set (f_name ed) ed (funDefs env0);
    localfunDecls := (localfunDecls env0);
    externals := externals env0 |}; auto.
destruct H.
case_eq (StringMapModule.get (f_name ed) (localVars env0)); intros; rewrite H0 in H; [inversion H|].
case_eq (StringMapModule.get (f_name ed) (globalVars env0)); intros; rewrite H1 in H; [inversion H|].
case_eq (StringMapModule.get (f_name ed) (externals env0)); intros; rewrite H2 in H; [inversion H|].
case_eq (StringMapModule.get (f_name ed) (funDefs env0)); intros; rewrite H3 in H; inversion H; auto.
Qed.

Lemma defineFunctionGetIds : forall hids hids2 ids env ed env',
   defineFunction ed env = Some env' ->
   getIdsValue (hids::hids2::ids) env' = getIdsValue (hids::hids2::ids) env.
Proof.
unfold defineFunction.
unfold getIdsValue.
intros.
case_eq (StringMapModule.get (f_name ed) (localVars env0)); intros; rewrite H0 in H; [inversion H|].
case_eq (StringMapModule.get (f_name ed) (globalVars env0)); intros; rewrite H1 in H; [inversion H|].
case_eq (StringMapModule.get (f_name ed) (externals env0)); intros; rewrite H2 in H; [inversion H|].
case_eq (StringMapModule.get (f_name ed) (funDefs env0)); intros; rewrite H3 in H; inversion H.
simpl.
induction (StringMapModule.get hids (localVars env0)); auto.
induction (StringMapModule.get hids (globalVars env0)); auto.
rewrite StringMapModule.gsspec.
case_eq (String_Equality.eq hids (f_name ed)); auto; intros.
apply String_Equality.eq_spec_true in H4.
rewrite H4.
rewrite H2.
rewrite H3.
induction (StringMapModule.get (f_name ed) (localfunDecls env0)); auto.
Qed.

Definition declareFunction fid fparams env := 
      match StringMapModule.get fid env.(localVars) with
        | None => 
            (
              match StringMapModule.get fid env.(globalVars) with
                | None => 
                    (
                      match StringMapModule.get fid env.(externals) with
                      | None =>
                        (
                          match StringMapModule.get fid env.(funDefs) with
                          | Some _ => None
                          | None =>
                              (
                                match StringMapModule.get fid env.(localfunDecls) with
                                | Some _ => None
                                | None =>
                                    Some ({|
                                      localVars := env.(localVars);
                                      globalVars := env.(globalVars);
                                      localfunDecls := StringMapModule.set fid fparams env.(localfunDecls);
                                      funDefs := env.(funDefs);
                                      externals := env.(externals)
                                    |})
                                end
                              )
                          end
                        )
                      | Some r => None
                      end
                    )
                | Some r => None
              end
            )
        | Some r => None
      end.

Lemma declareFunctionCorrect : forall id env env' fid fparams, 
    declareFunction fid fparams env = Some env' ->
    getIdValue id env' = 
      if String_Equality.eq id fid then
        Some (FunctionDeclaration fparams) 
      else
        getIdValue id env.
Proof.
unfold getIdValue.
unfold declareFunction.
intros.
case_eq (StringMapModule.get fid (localVars env0)); intros; rewrite H0 in H; [inversion H|].
case_eq (StringMapModule.get fid (globalVars env0)); intros; rewrite H1 in H; [inversion H|].
case_eq (StringMapModule.get fid (externals env0)); intros; rewrite H2 in H; [inversion H|].
case_eq (StringMapModule.get fid (funDefs env0)); intros; rewrite H3 in H; [inversion H|].
case_eq (StringMapModule.get fid (localfunDecls env0)); intros; rewrite H4 in H; inversion H; simpl.
rewrite StringMapModule.gsspec.
case_eq (String_Equality.eq id fid); auto; intros.
apply String_Equality.eq_spec_true in H5.
rewrite H5 in *.
rewrite H0.
rewrite H1.
rewrite H3.
auto.
Qed.

Lemma declareFunctionSucced : forall env fid fparams, 
    StringMapModule.get fid env.(localVars) = None /\
    StringMapModule.get fid env.(globalVars) = None /\
    StringMapModule.get fid env.(externals) = None /\
    StringMapModule.get fid env.(funDefs) = None /\
    StringMapModule.get fid env.(localfunDecls) = None <->
    exists e, declareFunction fid fparams env = Some e.
Proof.
unfold declareFunction.
split; intros.
destruct H.
destruct H0.
destruct H1.
destruct H2.
rewrite H, H0, H1, H2, H3.
exists {|
    localVars := localVars env0;
    globalVars := globalVars env0;
    funDefs := funDefs env0;
    localfunDecls := StringMapModule.set fid fparams (localfunDecls env0);
    externals := externals env0 |}; auto.
destruct H.
case_eq (StringMapModule.get fid (localVars env0)); intros; rewrite H0 in H; [inversion H|].
case_eq (StringMapModule.get fid (globalVars env0)); intros; rewrite H1 in H; [inversion H|].
case_eq (StringMapModule.get fid (externals env0)); intros; rewrite H2 in H; [inversion H|].
case_eq (StringMapModule.get fid (funDefs env0)); intros; rewrite H3 in H; [inversion H|].
case_eq (StringMapModule.get fid (localfunDecls env0)); intros; rewrite H4 in H; inversion H.
intuition.
Qed.

Lemma declareFunctionGetIds : forall hids hids2 ids env fid fparams env',
   declareFunction fid fparams env = Some env' ->
   getIdsValue (hids::hids2::ids) env' = getIdsValue (hids::hids2::ids) env.
Proof.
unfold declareFunction.
unfold getIdsValue.
intros.
case_eq (StringMapModule.get fid (localVars env0)); intros; rewrite H0 in H; [inversion H|].
case_eq (StringMapModule.get fid (globalVars env0)); intros; rewrite H1 in H; [inversion H|].
case_eq (StringMapModule.get fid (externals env0)); intros; rewrite H2 in H; [inversion H|].
case_eq (StringMapModule.get fid (funDefs env0)); intros; rewrite H3 in H; [inversion H|].
case_eq (StringMapModule.get fid (localfunDecls env0)); intros; rewrite H4 in H; inversion H.
simpl.
rewrite StringMapModule.gsspec.
case_eq (String_Equality.eq hids fid); auto; intros.
apply String_Equality.eq_spec_true in H5.
rewrite H5.
rewrite H0.
rewrite H1.
rewrite H4.
rewrite H2.
rewrite H3.
auto.
Qed.

Definition initEnv := mkenv [] [] [] [] [].









