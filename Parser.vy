%{
Require Import String.
Require Ast MapTypes.
Require Import ZArith Ascii.
Local Open Scope Z_scope.
Local Open Scope bool_scope.
Local Open Scope string_scope.
Require Import List.
Import ListNotations.

Inductive fun_follow : Type := 
| Empty : fun_follow
| Rename : Ast.name -> fun_follow
| Body : list Ast.f_local_dec -> Ast.instruction_s -> MapTypes.String_Equality.t -> fun_follow.
	
Fixpoint appendStmts l s := match l with
| Ast.ISeqEnd e => Ast.ISeqCons e (Ast.ISeqEnd s)
| Ast.ISeqCons h t => Ast.ISeqCons h (appendStmts t s)
end.
%}

/* Inspired from https://www.adaic.org/resources/add_content/standards/95lrm/grammar9x.y */
/******* A YACC grammar for Ada 9X *********************************/
/* Copyright (C) Intermetrics, Inc. 1994 Cambridge, MA  USA        */
/* Copying permitted if accompanied by this statement.             */
/* Derivative works are permitted if accompanied by this statement.*/
/* This grammar is thought to be correct as of May 1, 1994         */
/* but as usual there is *no warranty* to that effect.             */
/*******************************************************************/

%token <MapTypes.String_Equality.t> IDENTIFIER
%token <Z> NUMERIC_LIT
%token <bool> BOOLEAN_LIT
%token <ascii> CHAR_LIT
%token <MapTypes.String_Equality.t> CHAR_STRING
%token ABORT
%token ABS
%token ABSTRACT
%token ACCEPT
%token ACCESS
%token ALIASED
%token ALL
%token AND
%token ARRAY
%token AT
%token BEGiN
%token BODY
%token CASE
%token CONSTANT
%token DECLARE
%token DELAY
%token DELTA
%token DIGITS
%token DO
%token ELSE
%token ELSIF
%token END
%token ENTRY
%token EXCEPTION
%token EXIT
%token FOR
%token FUNCTION
%token GENERIC
%token GOTO
%token IF
%token IN
%token IS
%token LIMITED
%token LOOP
%token MOD
%token NEW
%token NOT
%token NuLL
%token OF
%token OR
%token OTHERS
%token OUT
%token PACKAGE
%token PRAGMA
%token PRIVATE
%token PROCEDURE
%token PROTECTED
%token RAISE
%token RANGE
%token RECORD
%token REM
%token RENAMES
%token REQUEUE
%token RETURN
%token REVERSE
%token SELECT
%token SEPARATE
%token SUBTYPE
%token TAGGED
%token TASK
%token TERMINATE
%token THEN
%token TYPE
%token UNTIL
%token USE
%token WHEN
%token WHILE
%token WITH
%token XOR
%token EOF
%token TIC
%token DOT_DOT
%token LT_LT
%token BOX
%token LT_EQ
%token EXPON
%token NE
%token GT_GT
%token GE
%token IS_ASSIGNED
%token RIGHT_SHAFT
%token DOT 
%token LT
%token LPARENT 
%token PLUS 
%token BOR 
%token BAND 
%token MULT 
%token RPARENT 
%token SEMICOL 
%token MINUS 
%token DIV 
%token COMMA 
%token GT 
%token COL 
%token EQ 
%token  ASSIGN BEGIN_LOOP BUILD_TAG 
%token  COMPARE_MESSAGES COMPARE_REINTEGRATION_ARRAY CONVERGE 
%token  DIV2 ENCODE END_BRANCH 
%token END_INIT END_ITERATION  FAILURE_ALARM 
%token INIT INITOPEL  
%token  NEW_SAFE_CONTEXT NEW_SIGNATURE 
%token READ READ_CONTEXT 
%token READ_HARD_INPUT READ_MESS READ_MESS_UNFILTERED  
%token   WRITE WRITE_HARD_OUTPUT 
%token WRITE_MESS WRITE_OUTVAR WRITE_REDUND_ARRAY
%token SEC_OPEL
%token OPS_SIGNATURE OPS_FILE FIRST ADDRESS LAST DIFF EQUAL Tr HARD_INPUT_VALID IS_VALID C W


%start goal_symbol

%type <Ast.compilation> goal_symbol compilation comp_unit
%type <MapTypes.String_Equality.t * (list Ast.declaration_de_base)> pkg_spec  pkg_decl
%type <MapTypes.String_Equality.t * (list Ast.declaration_1_body) * (list Ast.declaration_2_body)> pkg_body
%type <Ast.expression> expression preceded(COMMA,expression) parenthesized_primary factor
%type <Ast.expression> simple_expression primary
%type <list ((MapTypes.String_Equality.t * Ast.mode) * Ast.name)> param_s
%type <Ast.name * (option Ast.name)> pair(preceded(COMMA,sec_opel_parametre_nom_secu),option(preceded(COMMA,sec_opel_parametre_nom_secu)))
%type <option (Ast.name * (option Ast.name))> option(pair(preceded(COMMA,sec_opel_parametre_nom_secu),option(preceded(COMMA,sec_opel_parametre_nom_secu))))
%type <Ast.expression> term
%type <option Ast.expression> option(preceded(COMMA,expression))
%type <Ast.pragma> pragma
%type <Ast.case_instruction> alternative_s
%type <Ast.instruction> unlabeled procedure_call loop_stmt compound_stmt
%type <Ast.instruction > statement
%type <Ast.instruction > simple_stmt
%type <Ast.instruction_s > statement_s handled_stmt_s block_body basic_loop
%type <((MapTypes.String_Equality.t * Ast.mode) * Ast.name)> param 
%type <list ((MapTypes.String_Equality.t * Ast.mode) * Ast.name)> formal_part_opt formal_part
%type <MapTypes.String_Equality.t * list ((MapTypes.String_Equality.t * Ast.mode) * Ast.name)> subprog_spec
%type <Ast.selected_com> selected_comp
%type <Ast.name> subtype_ind name component_subtype_def
%type <Ast.name> sec_opel_t
%type <Ast.name> sec_opel_nom
%type <Ast.name> sec_opel_parametre_nom_secu
%type <Ast.name> sec_opel_parametre_nom
%type <Ast.name> renames
%type <Ast.mode> mode
%type <Ast.literal> literal
%type <Ast.sec_opel_parametre_literal> sec_opel_parametre_litteral
%type <Ast.sec_opel_parametre> sec_opel_parametre
%type <Ast.sec_opel_binary_bool_op> sec_opel_operateur_relationnel
%type <Ast.sec_opel_binary_bool_op> sec_opel_operateur_logique
%type <(list MapTypes.String_Equality.t) * bool> context_spec
%type <list MapTypes.String_Equality.t> with_clause
%type <Ast.name> preceded(COMMA,sec_opel_parametre_nom_secu)
%type <bool> use_clause_opt
%type <MapTypes.String_Equality.t> use_clause def_id
%type <option MapTypes.String_Equality.t> c_id_opt
%type <list MapTypes.String_Equality.t> separated_nonempty_list(DOT,simple_name) pragma_file_arg separated_nonempty_list(COMMA,nom_simple_d_unite)
%type <list Ast.expression_or_string_lit> separated_nonempty_list(COMMA, expression_or_string_lit) pragma_arg_s delimited(LPARENT,pragma_arg_s,RPARENT)
%type <Ast.expression_or_string_lit> expression_or_string_lit
%type <option(list Ast.expression_or_string_lit)> option(delimited(LPARENT,pragma_arg_s,RPARENT))
%type <Ast.unit> unit
%type <Ast.type_designator> type_design ctype_desing
%type <Ast.declaration_de_base> type_decl
%type <Ast.declaration_de_base> subtype_decl
%type <Ast.declaration_de_base> subprog_decl
%type <unit> null_stmt sec_opel_hard_input_valid sec_opel_is_valid
%type <Ast.instruction> sec_opel_write_redund_array if_stmt case_stmt
%type <Ast.instruction> sec_opel_write_outvar
%type <Ast.instruction> sec_opel_write_mess
%type <Ast.instruction> sec_opel_write_hard_output
%type <Ast.instruction> sec_opel_write
%type <Ast.instruction> sec_opel_read_mess_unfiltered
%type <Ast.instruction> sec_opel_read_mess
%type <Ast.instruction> sec_opel_read_hard_input
%type <Ast.instruction> sec_opel_new_signature
%type <Ast.instruction> sec_opel_new_safe_context
%type <Ast.instruction> sec_opel_instruction_appel_de_procedure
%type <Ast.instruction> sec_opel_initopel
%type <Ast.instruction> sec_opel_init
%type <Ast.instruction> sec_opel_failure_alarm
%type <Ast.instruction> sec_opel_end_iteration
%type <Ast.instruction> sec_opel_end_init
%type <Ast.instruction> sec_opel_end_branch
%type <Ast.instruction> sec_opel_converge
%type <Ast.instruction> sec_opel_compare_reintegration_array
%type <Ast.instruction> sec_opel_compare_messages
%type <Ast.instruction> sec_opel_begin_loop
%type <Ast.declaration_de_base> rename_decl decl_item
%type <Ast.declaration_de_base> decl
%type <list(Ast.declaration_de_base)> decl_item_s1 decl_item_s
%type <Ast.sec_opel_expression_if> sec_opel_expression_if
%type <Ast.sec_opel_expression_d_affectation> sec_opel_expression_d_affectation sec_opel_read_context sec_opel_read
%type <Ast.sec_opel_expression_d_affectation> sec_opel_div2 sec_opel_encode
%type <Ast.sec_opel_expression_d_affectation> sec_opel_build_tag
%type <Ast.sec_opel_expression_d_affectation> sec_opel_assign
%type <Ast.sec_opel_expression_booleenne> sec_opel_diff
%type <Ast.sec_opel_expression_booleenne> sec_opel_equal
%type <Ast.sec_opel_expression_booleenne> sec_opel_expression_booleenne
%type <Ast.sec_opel_w> sec_opel_w iteration
%type <Ast.binary_op> relational multiplying logical adding
%type <Ast.expression> relation
%type <Ast.declaration_de_base> rename_unit
%type <Ast.element_declararif> subprog_body
%type <Ast.f_local_dec> subprog_local_decl
%type <list Ast.f_local_dec> list(subprog_local_decl)
%type <(list (MapTypes.String_Equality.t * Ast.name))> record_type type_def record_def comp_list comp_decl_s
%type <((MapTypes.String_Equality.t * Ast.name))> comp_decl
%type <MapTypes.String_Equality.t> simple_name nom_simple_d_unite
%type <Ast.declaration_2_body> procs_for_body
%type <list Ast.declaration_2_body> list(procs_for_body)
%type <unit> terminated(SEC_OPEL,DOT)
%type <Ast.obj> object_decl
%type <Ast.declaration_1_body> decl_base_for_body
%type <list Ast.declaration_1_body> list(decl_base_for_body)
%type <option unit> option(terminated(SEC_OPEL,DOT))
%type <fun_follow> end_procs_for_body
%type <Ast.elif_instruction> else_part
%type <option Ast.name> option(preceded(COMMA,sec_opel_parametre_nom_secu))

%%

%public separated_nonempty_list(separator, X):
  x = X
    { [ x ] }
| x = X; separator; xs = separated_nonempty_list(separator, X)
    { x :: xs }

%public preceded(opening, X):
  opening; x = X
    { x }

%public pair(X, Y):
  x = X; y = Y
    { (x, y) }

%public option(X):
  /* nothing */
    { None }
| x = X
    { Some x }

%public delimited(opening, X, closing):
  opening; x = X; closing
    { x }

%public terminated(X, closing):
  x = X; closing
    { x }

%public list(X):
  /* nothing */
    { [] }
| x = X; xs = list(X)
    { x :: xs }



goal_symbol : c = compilation EOF	{ c }

null_stmt : NuLL SEMICOL	{ tt }

literal : 
	| n = NUMERIC_LIT	{ Ast.Int_lit n }
	| n = BOOLEAN_LIT	{ Ast.Bool_lit n }

pragma : 
	| PRAGMA id = IDENTIFIER SEMICOL	{ Ast.Pragma id [] }
	| PRAGMA sn = IDENTIFIER LPARENT args = pragma_arg_s RPARENT SEMICOL	{ Ast.Pragma sn args }
	| PRAGMA option( terminated(SEC_OPEL, DOT)) OPS_FILE LPARENT id = pragma_file_arg COMMA e = expression RPARENT SEMICOL  { Ast.SEC_OPEL_File id e }
	| PRAGMA option( terminated(SEC_OPEL, DOT)) OPS_SIGNATURE LPARENT n = sec_opel_parametre_nom_secu COMMA e = expression COMMA e2 = expression RPARENT SEMICOL  { Ast.SEC_OPEL_Signature n e e2 }

expression_or_string_lit : 
	| e = expression {Ast.Exp e}	
	| e = CHAR_STRING {Ast.StringLit e}	

pragma_file_arg :
	| i = IDENTIFIER { [i] }
	| i = IDENTIFIER DOT i2 = IDENTIFIER { [i; i2] }
	
pragma_arg_s : 
	| a =  separated_nonempty_list(COMMA, expression_or_string_lit)	{ a }


sec_opel_parametre_litteral :
	| l = NUMERIC_LIT { Ast.Int_lit l }
	| MINUS l = NUMERIC_LIT { Ast.Int_lit (-l) }
	| b = BOOLEAN_LIT { Ast.Bool_lit b }

sec_opel_nom :
	| cs = selected_comp { cs }

sec_opel_parametre_nom_secu :
	| cs = selected_comp { cs }


sec_opel_parametre_nom :
	| p = sec_opel_parametre_nom_secu  { p }

sec_opel_parametre :
	| p = sec_opel_parametre_litteral { Ast.SEC_OPEL_Value p }
	| p = sec_opel_parametre_nom { Ast.SEC_OPEL_Var p  }
	| LPARENT p = sec_opel_parametre RPARENT  { p }

	

decl : o = object_decl	{ Ast.Declaration_d_objet o }
	| t = type_decl	{ t }
	| s = subtype_decl	{ s }
	| s = subprog_decl	{ s }
	| r = rename_decl	{ r }

type_design :
    | n = selected_comp { Ast.TypeSimple n }
    | n = selected_comp LPARENT e = expression RPARENT { Ast.TypeComposeArray1 n e }
    | n = selected_comp LPARENT e = expression COMMA e1 = expression RPARENT { Ast.TypeComposeArray2 n e e1 }

ctype_desing : 
    { Ast.TypeSimple ["Fint"] }
    | t = type_design { t }

object_decl : 
    | id_list = def_id COL CONSTANT indic_s_type = ctype_desing IS_ASSIGNED e = expression SEMICOL	
		{ 
		    Ast.ConstantVar id_list indic_s_type e
		}
	| id_list = def_id COL indic_s_type = type_design SEMICOL	
		{ 
		    Ast.VariableVar id_list indic_s_type
		}

def_id  : id = IDENTIFIER	{ id }
	

type_decl : TYPE id = IDENTIFIER IS comp_type = type_def SEMICOL	
		{ Ast.Definition_de_type_Article id comp_type }
	
type_def :
	| r = record_type	{ r }


subtype_decl : SUBTYPE id = IDENTIFIER IS s = type_design SEMICOL	
	{ 
	  	Ast.Declaration_de_sous_type id s
	}
	

subtype_ind :
	| n = name	{ n }

component_subtype_def : m = subtype_ind	
			{ 
			  	 m
			}
	

record_type : r = record_def	
		{
				r
		}
	

record_def : 
	| RECORD c = comp_list END RECORD	
		{
				c
		}
	

comp_list : d = comp_decl_s 	
				{ d }

comp_decl_s : c = comp_decl	{ [c] }
	| l = comp_decl_s c = comp_decl	
		{
				l ++ [c]
		}
	

comp_decl : ids = def_id COL t = component_subtype_def SEMICOL	
			{
				(ids, t)
			}

decl_item_s : 	{ [] }
	| l = decl_item_s1	{ l }
	

decl_item_s1 : e = decl_item	{ [e] }
	| l = decl_item_s1 e = decl_item	{ l ++ [e] }
	

decl_item : d = decl	{ d }
	| use_clause	{ Ast.Use_Clause }
	| p = pragma	{ Ast.Pragma_Declaration p }
	

name : 
	| cs = selected_comp	{ cs }

simple_name : id = IDENTIFIER	{ id }
	
selected_comp : 
	| s = separated_nonempty_list(DOT, simple_name )	{ s }
	| SEC_OPEL DOT s = separated_nonempty_list(DOT, simple_name )	{ ("SEC_OPEL")::s }
	
	

expression : r = relation	{ r }
	| e = expression l = logical r = relation	{ Ast.Ebinary l e r }

logical : AND	{ Ast.And }
	| OR	{ Ast.Or }
	| XOR	{ Ast.XOr }
	
relation : e = simple_expression	{ e }
	| e = simple_expression r = relational e2 = simple_expression	{ Ast.Ebinary r e e2 }

relational : EQ	{ Ast.Equal }
	| NE	{ Ast.NEqual }
	| LT	{ Ast.LessThan }
	| LT_EQ	{ Ast.LessOrEq }
	| GT	{ Ast.GreaterThan }
	| GE	{ Ast.GreaterOrEq }


simple_expression : 
	| t = term	{ t }
	| MINUS t = term	{ 
	    match t with
	    | Ast.ELit(Ast.Int_lit v) => Ast.ELit(Ast.Int_lit (-v))
	    | _ => Ast.Eunary Ast.NumericUnaryMinus t end }
	| e = simple_expression op = adding t = term	{ Ast.Ebinary op e t }
	
adding  : PLUS	{ Ast.EAdd }
	| MINUS	{ Ast.EMinus }

term    : f = factor	{ f }
	| t = term op = multiplying f = factor	{ Ast.Ebinary op t f }
	

multiplying : MULT	{ Ast.EMult }
	| DIV	{ Ast.Ediv }
	| MOD	{ Ast.Emod }
	| REM	{ Ast.Erem }
	

factor : p = primary	{ p }
	| NOT p = primary	{ Ast.Eunary Ast.BoolNot p }
	| ABS p = primary	{ Ast.Eunary Ast.NumericAbs p }
	| p = primary EXPON p2 = primary	{ Ast.Ebinary Ast.EPow p p2 }
	

primary : p = literal {Ast.ELit p}
	| n = name	{ Ast.EVar n }
	| e = parenthesized_primary	{ e }

parenthesized_primary :
	| LPARENT e = expression RPARENT	{ e }
	
statement_s : s = statement	{ Ast.ISeqEnd s }
	| l = statement_s s = statement	{ appendStmts l s }
	
statement : i = unlabeled	{ i }

unlabeled : i = simple_stmt	{ i }
	| i = compound_stmt	{ i }
	| p = pragma	{ Ast.IPragma p }
	

simple_stmt : null_stmt	{ Ast.INull }
	| n = sec_opel_parametre_nom_secu IS_ASSIGNED e = sec_opel_expression_d_affectation SEMICOL	{ Ast.IAssign n e }
	| p = procedure_call	{ p	}

compound_stmt : i = if_stmt	{ i }
	| i = case_stmt	{ i }
	| i = loop_stmt	{ i }

sec_opel_expression_d_affectation : 
	| NOT s = sec_opel_parametre_nom_secu { Ast.SOEAffBool (Ast.SOEBNot s) }
	| MINUS s = sec_opel_parametre_nom_secu { Ast.SOEAffExpUMinus s }
	| s1 = sec_opel_parametre_nom_secu PLUS s2 = sec_opel_parametre { Ast.SOEAffExpBinary Ast.SOEAdd (Ast.SEC_OPEL_Var s1) s2 }
	| s1 = sec_opel_parametre MINUS s2 = sec_opel_parametre { Ast.SOEAffExpBinary Ast.SOEMinus s1 s2 }
	| s1 = sec_opel_parametre_nom_secu MULT s2 = sec_opel_parametre { Ast.SOEAffExpBinary Ast.SOEMult (Ast.SEC_OPEL_Var s1) s2 }
	| s1 = sec_opel_parametre_nom_secu op = sec_opel_operateur_relationnel s2 = sec_opel_parametre { Ast.SOEAffBool (Ast.SOEBBinaire op s1 s2) }
	| s1 = sec_opel_parametre_nom_secu op = sec_opel_operateur_logique s2 = sec_opel_parametre_nom_secu 
	    { Ast.SOEAffBool(Ast.SOEBBinaire op s1 (Ast.SEC_OPEL_Var s2)) }
	| s = sec_opel_assign { s }
	| s = sec_opel_build_tag { s }
	| s = sec_opel_diff { Ast.SOEAffBool s }
	| s = sec_opel_div2 { s }
	| s = sec_opel_equal { Ast.SOEAffBool s }
	| s = sec_opel_encode { s }
	| s = sec_opel_read { s }
	| s = sec_opel_read_context { s }


sec_opel_assign : option( terminated(SEC_OPEL, DOT)) ASSIGN LPARENT p = sec_opel_parametre RPARENT { Ast.SOEAffAssign p }

sec_opel_build_tag : option( terminated(SEC_OPEL, DOT)) BUILD_TAG LPARENT p1 = sec_opel_parametre_nom_secu
p2 = option( pair( preceded (COMMA, sec_opel_parametre_nom_secu), option(preceded (COMMA, sec_opel_parametre_nom_secu)) ) ) RPARENT 
			{ Ast.SOEAffBuildTag p1 p2 }


sec_opel_div2 : option( terminated(SEC_OPEL, DOT)) DIV2 LPARENT p = sec_opel_parametre_nom_secu COMMA e = expression RPARENT
			{
				Ast.SOEAffDiv2 p e
			}


sec_opel_encode : 
	| option( terminated(SEC_OPEL, DOT)) ENCODE LPARENT e = expression RPARENT { Ast.SOEAffEncode e }

sec_opel_read : option( terminated(SEC_OPEL, DOT)) READ LPARENT p1 = sec_opel_parametre_nom_secu
p2 = option( pair( preceded (COMMA, sec_opel_parametre_nom_secu), option(preceded (COMMA, sec_opel_parametre_nom_secu)) ) ) RPARENT 
			{ 
				Ast.SOEAffRead p1 p2
			}

sec_opel_read_context : option( terminated(SEC_OPEL, DOT)) READ_CONTEXT { Ast.SOEAffReadContext }
													
if_stmt : IF c = sec_opel_expression_if THEN s = statement_s e = else_part END IF SEMICOL
		{ 
			Ast.IIf c s e
		}

else_part :  ELSE s = statement_s	{ Ast.IElse s }
			| ELSIF c = sec_opel_expression_if THEN s = statement_s e = else_part { Ast.IElif c s e }

sec_opel_expression_if : 
	| s = sec_opel_expression_booleenne { Ast.SOExpression_booleenne s }
	| sec_opel_is_valid { Ast.SOIs_valid }
	| sec_opel_hard_input_valid { Ast.SOHard_input_valid }
	| s =  sec_opel_t { Ast.SOt s }

	
sec_opel_expression_booleenne : 
	| NOT e = sec_opel_parametre_nom_secu { Ast.SOEBNot e }
	| n = sec_opel_parametre_nom_secu op = sec_opel_operateur_relationnel p = sec_opel_parametre 
				{ Ast.SOEBBinaire op n p }
	| n = sec_opel_parametre_nom_secu op = sec_opel_operateur_logique p = sec_opel_parametre_nom_secu 
				{ Ast.SOEBBinaire op n (Ast.SEC_OPEL_Var p) }
	| d = sec_opel_diff { d  }
	| e = sec_opel_equal { e }

sec_opel_equal : option( terminated(SEC_OPEL, DOT)) EQUAL LPARENT n = sec_opel_parametre_nom_secu COMMA p = sec_opel_parametre RPARENT { Ast.SOEBBinaire Ast.SOReq n p }

sec_opel_diff : option( terminated(SEC_OPEL, DOT)) DIFF LPARENT n = sec_opel_parametre_nom_secu COMMA p = sec_opel_parametre RPARENT { Ast.SOEBBinaire Ast.SORdiff n p }

sec_opel_is_valid : option( terminated(SEC_OPEL, DOT)) IS_VALID { tt }

sec_opel_hard_input_valid : option( terminated(SEC_OPEL, DOT)) HARD_INPUT_VALID { tt }

sec_opel_t : option( terminated(SEC_OPEL, DOT)) Tr LPARENT s = sec_opel_parametre_nom_secu RPARENT { s }

sec_opel_operateur_relationnel : 
	| LT	{ Ast.SORless }
	| LT_EQ	{ Ast.SORleq }
	| GT	{ Ast.SORgreater }
	| GE	{ Ast.SORgeq }

sec_opel_operateur_logique : 
	| AND	{ Ast.SOLand }
	| OR	{ Ast.SOLor }

case_stmt : CASE option( terminated(SEC_OPEL, DOT)) C LPARENT p = sec_opel_parametre_nom_secu RPARENT IS a = alternative_s END CASE SEMICOL
		{
			Ast.ICase p a
		}
	
alternative_s : 
    WHEN e = simple_expression RIGHT_SHAFT s = statement_s r = alternative_s	{ Ast.ICaseCons (e, s) r }
    | WHEN OTHERS RIGHT_SHAFT s = statement_s	{ Ast.ICaseFinishDefault s }
	

loop_stmt : i = iteration stmts = basic_loop SEMICOL
		{
			Ast.ILoop i stmts
		}
	

iteration :	
	| WHILE w = sec_opel_w	{ w }

sec_opel_w : option( terminated(SEC_OPEL, DOT)) W LPARENT e = sec_opel_expression_booleenne RPARENT { Ast.SOWBool e }
	| option( terminated(SEC_OPEL, DOT)) W LPARENT p = sec_opel_parametre_nom_secu RPARENT { Ast.SOWNSecu p }

basic_loop : LOOP s = statement_s END LOOP	{ s }

block_body : BEGiN s = handled_stmt_s	{ s }
	

handled_stmt_s : stmts = statement_s 	
		{
			stmts
		}
	 
subprog_decl : s = subprog_spec SEMICOL	{ let (n, param) := s in Ast.Declaration_de_sous_programme n param }
	
subprog_spec : 
	| PROCEDURE n = IDENTIFIER f = formal_part_opt	{ n, f }


formal_part_opt : 	{ [] }
	| p = formal_part	{ p }
	

formal_part : LPARENT p = param_s RPARENT	{ p }
	

param_s : p = param	{ [p] }
	| l = param_s SEMICOL p = param	{ l ++ [p] }
	

param :
	id = def_id COL m = mode marque = name
			{ 
				((id, m), marque)
			}

mode :	{ Ast.In }
	| IN	{ Ast.In }
	| OUT	{ Ast.Out }
	| IN OUT	{ Ast.InOut }

	
subprog_local_decl :
    o = object_decl { Ast.LocalDecVar o }
    | o = pragma { Ast.LocalDecPragma o }


subprog_body : PROCEDURE n = IDENTIFIER p = formal_part_opt IS	
	       	d = list(subprog_local_decl) stmts = block_body END id = IDENTIFIER SEMICOL	{ 
			    {|
					Ast.f_name := n;
					Ast.f_args := p;
					Ast.f_local_decs := d ;
					Ast.f_instrs := stmts
				|}
	      	}
	

procedure_call : 
	| n = sec_opel_nom args = option(delimited(LPARENT, pragma_arg_s, RPARENT)) SEMICOL	
		{ 
			Ast.ICallDefinedFunction n args 
		}
	| i = sec_opel_instruction_appel_de_procedure SEMICOL	{ i }
	
sec_opel_begin_loop : option( terminated(SEC_OPEL, DOT)) BEGIN_LOOP LPARENT e = expression RPARENT { Ast.SOInstructionBeginLoop e }

sec_opel_converge : option( terminated(SEC_OPEL, DOT)) CONVERGE LPARENT e = expression RPARENT  { Ast.SOInstructionConverge e }

sec_opel_compare_messages : option( terminated(SEC_OPEL, DOT)) COMPARE_MESSAGES LPARENT p1 = sec_opel_parametre_nom_secu COMMA p2 = sec_opel_parametre_nom_secu COMMA p3 = sec_opel_parametre_nom_secu COMMA e = expression RPARENT 
		{ 
			Ast.SOInstructionCompareM p1 p2 p3 e
		}

sec_opel_compare_reintegration_array : option( terminated(SEC_OPEL, DOT)) COMPARE_REINTEGRATION_ARRAY LPARENT p1 = sec_opel_parametre_nom_secu COMMA p2 = sec_opel_parametre_nom_secu COMMA p3 = sec_opel_parametre_nom_secu COMMA p4 = sec_opel_parametre_nom_secu COMMA p5 = sec_opel_parametre_nom_secu COMMA p6 = sec_opel_parametre_nom_secu COMMA p7 = sec_opel_parametre_nom_secu COMMA e = expression RPARENT
		{
			Ast.SOInstructionCompareR p1 p2 p3 p4 p5 p6 p7 e
		}

sec_opel_end_branch : option( terminated(SEC_OPEL, DOT)) END_BRANCH LPARENT e = expression RPARENT { Ast.SOInstructionEndBranch e }

sec_opel_end_init : option( terminated(SEC_OPEL, DOT)) END_INIT { Ast.SOInstructionEndInit }

sec_opel_end_iteration : option( terminated(SEC_OPEL, DOT)) END_ITERATION { Ast.SOInstructionEndIter }

sec_opel_failure_alarm : option( terminated(SEC_OPEL, DOT)) FAILURE_ALARM { Ast.SOInstructionFailAlarm }

sec_opel_init : 
	| option( terminated(SEC_OPEL, DOT)) INIT LPARENT n = sec_opel_parametre_nom_secu e = option(preceded(COMMA, expression)) RPARENT { Ast.SOInstructionInit n e }

sec_opel_initopel : option( terminated(SEC_OPEL, DOT)) INITOPEL { Ast.SOInstructionInitopel }

sec_opel_new_safe_context : option( terminated(SEC_OPEL, DOT)) NEW_SAFE_CONTEXT LPARENT e1 = expression COMMA e2 = expression COMMA e3 = expression RPARENT
			{ Ast.SOInstructionNewSafe e1 e2 e3 }

sec_opel_new_signature : option( terminated(SEC_OPEL, DOT)) NEW_SIGNATURE LPARENT p = sec_opel_parametre_nom_secu COMMA e1 = expression COMMA e2 = expression COMMA e3 = expression RPARENT
			{
				Ast.SOInstructionNewSig p e1 e2 e3
			}

sec_opel_read_hard_input : option( terminated(SEC_OPEL, DOT)) READ_HARD_INPUT LPARENT p1 = sec_opel_parametre_nom_secu COMMA p2 = sec_opel_parametre_nom_secu COMMA e1 = expression COMMA e2 = expression RPARENT
			{
				Ast.SOInstructionReadHardInput p1 p2 e1 e2
			}

sec_opel_read_mess : option( terminated(SEC_OPEL, DOT)) READ_MESS LPARENT p1 = sec_opel_parametre_nom_secu COMMA p2 = sec_opel_parametre_nom_secu COMMA p3 = sec_opel_parametre_nom_secu COMMA e1 = expression COMMA e2 = expression COMMA e3 = expression COMMA e4 = expression RPARENT
			{
				Ast.SOInstructionReadMess p1 p2 p3 e1 e2 e3 e4
			} 

sec_opel_read_mess_unfiltered : option( terminated(SEC_OPEL, DOT)) READ_MESS_UNFILTERED LPARENT p1 = sec_opel_parametre_nom_secu COMMA p2 = sec_opel_parametre_nom_secu COMMA p3 = sec_opel_parametre_nom_secu COMMA e1 = expression COMMA e2 = expression COMMA e3 = expression RPARENT 
			{
				Ast.SOInstructionReadMessUnfil p1 p2 p3 e1 e2 e3
			}

sec_opel_write : option( terminated(SEC_OPEL, DOT)) WRITE LPARENT p1 = sec_opel_parametre_nom_secu COMMA p2 = sec_opel_parametre_nom_secu COMMA p3 = sec_opel_parametre_nom_secu p4 = option(preceded(COMMA, sec_opel_parametre_nom_secu)) RPARENT
			{
				Ast.SOInstructionWrite p1 p2 p3 p4
			}

sec_opel_write_hard_output : option( terminated(SEC_OPEL, DOT)) WRITE_HARD_OUTPUT LPARENT p1 = sec_opel_parametre_nom_secu COMMA p2 = sec_opel_parametre_nom_secu COMMA e1 = expression COMMA e2 = expression RPARENT
			{
				Ast.SOInstructionWriteHardOutput p1 p2 e1 e2
			}

sec_opel_write_mess : option( terminated(SEC_OPEL, DOT)) WRITE_MESS LPARENT p1 = sec_opel_parametre_nom_secu COMMA p2 = sec_opel_parametre_nom_secu COMMA e1 = expression COMMA e2 = expression COMMA e3 = expression RPARENT 
			{
				Ast.SOInstructionWriteMess p1 p2 e1 e2 e3
			}

sec_opel_write_outvar : option( terminated(SEC_OPEL, DOT)) WRITE_OUTVAR LPARENT p1 = sec_opel_parametre_nom_secu COMMA p2 = sec_opel_parametre_nom_secu COMMA e1 = expression COMMA e2 = expression COMMA e3 = expression RPARENT 
			{
				Ast.SOInstructionWriteOutvar p1 p2 e1 e2 e3
			}

sec_opel_write_redund_array : option( terminated(SEC_OPEL, DOT)) WRITE_REDUND_ARRAY LPARENT p1 = sec_opel_parametre_nom_secu COMMA p2 = sec_opel_parametre_nom_secu COMMA e1 = expression COMMA e2 = expression COMMA e3 = expression RPARENT 
			{
				Ast.SOInstructionWriteRedund p1 p2 e1 e2 e3
			}


sec_opel_instruction_appel_de_procedure : 
	| s = sec_opel_begin_loop { s }
	| s = sec_opel_converge { s }
	| s = sec_opel_compare_messages { s }
	| s = sec_opel_compare_reintegration_array { s }
	| s = sec_opel_end_branch { s }
	| s = sec_opel_end_init { s}
	| s = sec_opel_end_iteration { s} 
	| s = sec_opel_failure_alarm { s}
	| s = sec_opel_init { s }
	| s = sec_opel_initopel { s}
	| s = sec_opel_new_safe_context { s }
	| s = sec_opel_new_signature { s }
	| s = sec_opel_read_hard_input { s }
	| s = sec_opel_read_mess { s }
	| s = sec_opel_read_mess_unfiltered { s }
	| s = sec_opel_write { s }
	| s = sec_opel_write_hard_output { s }
	| s = sec_opel_write_mess { s }
	| s = sec_opel_write_outvar { s }
	| s = sec_opel_write_redund_array { s }



pkg_decl : p = pkg_spec SEMICOL	{ p }

pkg_spec : PACKAGE n = simple_name IS d = decl_item_s END rename = c_id_opt	
			{
				(n, d)
			}
	
c_id_opt : | n = simple_name	{ Some n }
	
decl_base_for_body : 
    | o = object_decl	{  Ast.Body_Declaration_d_objet o }
	| TYPE id = IDENTIFIER IS t = record_type SEMICOL { Ast.Body_Definition_de_type_Article id t }
	| SUBTYPE id = IDENTIFIER IS s = type_design SEMICOL		{ Ast.Body_Declaration_de_sous_type id s }
	| use_clause	{ Ast.Body_Use_Clause }
	| p = pragma	{  Ast.Body_Pragma_Declaration p }

procs_for_body :
    | PROCEDURE n = IDENTIFIER p = formal_part_opt e = end_procs_for_body {
        match e with
        | Empty => Ast.Fun_dec n p
        | Rename ln => Ast.Fun_renames n p ln
        | Body d stms id => 
            Ast.Fun_def {|
		            Ast.f_name := id;
		            Ast.f_args := p;
		            Ast.f_local_decs := d;
		            Ast.f_instrs := stms
	            |}
        end
    }
    
end_procs_for_body :
    | IS d = list(subprog_local_decl) stmts = block_body END id = IDENTIFIER SEMICOL { Body d stmts id }
    | SEMICOL {Empty}
    | n = renames SEMICOL 	{ Rename n }

pkg_body : PACKAGE BODY name = simple_name IS d1 = list(decl_base_for_body) d2 = list(procs_for_body) END rename = c_id_opt SEMICOL	
		{ 
			(name, d1, d2)
		}


use_clause : 
	| USE SEC_OPEL SEMICOL	{ "SEC_OPEL" }

rename_decl : 
	| r = rename_unit	{ r }

rename_unit : 
	| s = subprog_spec n = renames SEMICOL	{ let (id, args) := s in Ast.Declaration_de_surnom id args n }

renames : RENAMES n = name	{ n }
	

compilation :	
	| u = comp_unit	{ u }

comp_unit : ctx = context_spec u = unit	
		{
			(ctx, u)
		}
	| u = unit
		{
				(([], false), u)
		}
	

context_spec : 
	| w = with_clause u = use_clause_opt	{ w, u }
	| l = context_spec w = with_clause u = use_clause_opt	{ let (wl, ul) := l in (wl ++ w, ul || u) }

with_clause : WITH l = separated_nonempty_list(COMMA, nom_simple_d_unite) SEMICOL	{ l }
	
nom_simple_d_unite : 
	| n = simple_name {n}
	| SEC_OPEL { "SEC_OPEL" }

use_clause_opt :	{ false }
	| use_clause_opt use_clause	{ true }
	

unit : p = pkg_decl	{ let (p0, p1) := p in Ast.Compilation_PkgDecl p0 p1 }
	| p = pkg_body	{ let (p', p2) := p in let (p0, p1) := p' in Ast.Compilation_PkgBody p0 p1 p2 }
	| s = subprog_body	{ Ast.Compilation_Body s }

