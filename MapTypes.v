Require Import List String Map ZArith.
Import ListNotations.

Module String_Equality <: EQUALITY_TYPE.
  Definition t := string.
  Definition eq := String.eqb.
  Definition eq_spec_true := String.eqb_eq.
End String_Equality.

Module List_Equality (E: EQUALITY_TYPE) <: EQUALITY_TYPE.
  Definition t := list E.t.
  Fixpoint eq l l' := match l, l' with
    | [], [] => true
    | h::t, h'::t' => andb (E.eq h h') (eq t t')
    | _, _ => false
  end.

  Lemma eq_ident: forall l, eq l l = true.
  Proof.
  induction l.
  simpl; auto.
  simpl.
  rewrite IHl.
  rewrite Bool.andb_true_r.
  apply E.eq_spec_true.
  auto.
  Qed.
  
  Lemma eq_spec_true : forall l l', eq l l' = true <-> l = l'.
  Proof.
  induction l.
  split.
  induction l'.
  auto.
  simpl.
  intros.
  inversion H.
  intros.
  rewrite <- H.
  simpl.
  auto.
  induction l'.
  simpl.
  split.
  intros.
  inversion H.
  intros.
  inversion H.
  split.
  simpl.
  intros.
  apply Bool.andb_true_iff in H.
  destruct H.
  apply E.eq_spec_true in H.
  apply IHl in H0.
  rewrite H.
  rewrite H0.
  auto.
  intros.
  rewrite H.
  apply eq_ident.
  Qed.
End List_Equality.

Module StringMapModule := EListMap(String_Equality).
Module SCompEquality := List_Equality(String_Equality).
Module SCompMapModule := EListMap(SCompEquality).

Module Nat_Equality <: EQUALITY_TYPE.
  Definition t := nat.
  Definition eq := Nat.eqb.
  Definition eq_spec_true := Nat.eqb_eq.
End Nat_Equality.

Module NatMapModule := EListMap(Nat_Equality).