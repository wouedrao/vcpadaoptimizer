(** * Coq codes *)
(** ** Dependencies *)

(* Add LoadPath "." as RegExp. *)
Require Export RegExp.Utility.
Require Export RegExp.Definitions.
Require Export RegExp.Boolean.
Require Export RegExp.Concat.
Require Import Lia.

(* Unset Standard Proposition Elimination Names. *)

(** ** [Char] *)

Lemma Char_true : forall c, (Char c) ~== (String c ""%string).
Proof.
  intro c.  simpl.
  destruct(ascii_dec c c); simpl.
    auto.
    elim n. auto.
Qed.

Lemma AnyChar_true : forall c, AnyChar ~== (String c ""%string).
Proof.
  intros. simpl. auto.
Qed.

Lemma CharRange_true : forall l h c, ((leb l c) && (leb c h))%bool = true -> (CharRange l h) ~== (String c ""%string) .
Proof.
  intros.  simpl. rewrite H. simpl. auto.
Qed.

Lemma CharRangeExcept_true : forall l h c, ((leb l c) && (leb c h))%bool = false -> (CharRangeExcept l h) ~== (String c ""%string) .
Proof.
  intros.  simpl. rewrite H. simpl. auto.
Qed.

Lemma Char_false : forall s c, s <> (String c ""%string) -> (Char c) ~!= s.
Proof.
  induction s.
    intros c Hs.  simpl.  auto.
    induction s; intros c Hs; simpl.  
      destruct(ascii_dec c a); simpl.
        rewrite e in Hs.  elim Hs.  auto. 
        auto.
      destruct(ascii_dec c a); simpl.
        eapply Empty_false.
        eapply Empty_false.
Qed.

Lemma AnyChar_false : forall s, (forall c, s <> (String c ""%string)) -> AnyChar ~!= s.
Proof.
  induction s.
    intros. simpl. auto.
    induction s. intros. contradict H. unfold not. intros. apply (H a). auto.
    simpl. intros. apply Empty_false.
Qed.

Lemma CharRange_false : forall l h s, (forall c,  ((leb l c) && (leb c h))%bool = true -> s <> (String c ""%string)) -> (CharRange l h) ~!= s.
Proof.
  induction s.
    intros. simpl. auto.
    induction s. intros. case_eq (((l <=? a)%char && (a <=? h)%char)%bool). 
      intros. assert (D := H a H0). contradiction.
      simpl. intros. rewrite H0. simpl. auto.
    intros. simpl. induction (((l <=? a)%char && (a <=? h)%char)%bool). 
      simpl. apply Empty_false.
      simpl. apply Empty_false.
Qed.

Lemma CharRangeExcept_false : forall l h s, (forall c,  ((leb l c) && (leb c h))%bool = false -> s <> (String c ""%string)) -> (CharRangeExcept l h) ~!= s.
Proof.
  induction s.
    intros. simpl. auto.
    induction s. intros. simpl. case_eq (((leb l a) && (leb a h))%bool).
      simpl. auto. 
      intros. assert (D := H a H0). contradiction.
    intros. simpl. induction (((leb l a) && (leb a h))%bool). 
      simpl. apply Empty_false.
      simpl. apply Empty_false.
Qed.
  

Add Parametric Morphism : Char with
signature ascii_eq ==> re_eq as Char_mor.
Proof.
  intros x y Hxy.  destruct Hxy.  setoid_reflexivity.
Qed.

Add Parametric Morphism : CharRange with
signature ascii_eq ==> ascii_eq ==> re_eq as CharRange_mor.
Proof.
  intros x y Hxy x0 y0 Hx0y0.  destruct Hxy.  destruct Hx0y0.  setoid_reflexivity.
Qed.

Lemma derive_Char_is_id : forall a r, derive a (Char a ++ r) =R= r.
Proof.
  intros a r.  simpl.  
  destruct(ascii_dec a a).
    setoid_rewrite Cat_left_id.  setoid_reflexivity.
    elim n.  auto.
Qed.

Lemma derive_CharRange_is_id : forall l h a r, ((l <=? a)%char && (a <=? h)%char)%bool = true -> derive a ((CharRange l h) ++ r) =R= r.
Proof.
  intros a r.  simpl. 
  intros. rewrite H.
    setoid_rewrite Cat_left_id.  setoid_reflexivity.
Qed.

Lemma derive_CharRangeExcept_is_id : forall l h a r, ((l <=? a)%char && (a <=? h)%char)%bool = false -> derive a ((CharRangeExcept l h) ++ r) =R= r.
Proof.
  intros a r.  simpl. 
  intros. rewrite H.
    setoid_rewrite Cat_left_id.  setoid_reflexivity.
Qed.

Lemma derive_AnyChar_is_id : forall a r, derive a (AnyChar ++ r) =R= r.
Proof.
  intros a r.  simpl. 
    setoid_rewrite Cat_left_id.  setoid_reflexivity.
Qed.

(** **CharRange and AnyChar equivalence** *)
Lemma ascii_bounded_0: forall a, ("000" <=? a)%char = true.
Proof.
intros.
unfold leb.
unfold Ascii.compare.
rewrite Nnat.N2Nat.inj_compare.
simpl.
induction (BinNat.N.to_nat (N_of_ascii a)); auto.
Qed.

Lemma ascii_bounded_255: forall a, (a <=? "255")%char = true.
Proof.
intros.
unfold leb.
unfold Ascii.compare.
assert (D := N_ascii_bounded a).
rewrite <- BinNat.N.compare_lt_iff in D.
rewrite Nnat.N2Nat.inj_compare in *.
apply PeanoNat.Nat.compare_lt_iff in D.
case_eq (PeanoNat.Nat.compare (BinNat.N.to_nat (N_of_ascii a))
    (BinNat.N.to_nat (N_of_ascii "255"))); auto.
intros.
simpl in H.
apply PeanoNat.Nat.compare_gt_iff in H.
lia.
Qed.

Theorem AnyChar_CharRange_equiv: AnyChar =R= (CharRange "000" "255").
Proof.
unfold re_eq.
induction s.
simpl.
auto.
simpl.
rewrite ascii_bounded_0.
rewrite ascii_bounded_255.
simpl.
auto.
Qed.

Theorem CharRange_equiv_empty: forall l h, (h <? l)%char = true -> (CharRange l h) =R= Empty.
Proof.
unfold re_eq.
induction s.
simpl.
auto.
simpl.
case_eq ((l <=? a)%char); intros; auto.
case_eq ((a <=? h)%char); intros; auto.
simpl.
unfold leb in *.
unfold ltb in *.
unfold Ascii.compare in *.
rewrite Nnat.N2Nat.inj_compare in *.
assert (PeanoNat.Nat.compare
        (BinNat.N.to_nat (N_of_ascii h))
        (BinNat.N.to_nat (N_of_ascii l)) = Lt).
induction (PeanoNat.Nat.compare
        (BinNat.N.to_nat (N_of_ascii h))
        (BinNat.N.to_nat (N_of_ascii l))); auto; inversion H.
apply PeanoNat.Nat.compare_lt_iff in H2.
case_eq (PeanoNat.Nat.compare
         (BinNat.N.to_nat (N_of_ascii l))
         (BinNat.N.to_nat (N_of_ascii a))); intros.
apply PeanoNat.Nat.compare_eq_iff in H3.
case_eq (PeanoNat.Nat.compare
         (BinNat.N.to_nat (N_of_ascii a))
         (BinNat.N.to_nat (N_of_ascii h))); intros.
apply PeanoNat.Nat.compare_eq_iff in H4.
lia.
apply PeanoNat.Nat.compare_lt_iff in H4.
lia.
rewrite H4 in H1; inversion H1.
apply PeanoNat.Nat.compare_lt_iff in H3.
case_eq (PeanoNat.Nat.compare
         (BinNat.N.to_nat (N_of_ascii a))
         (BinNat.N.to_nat (N_of_ascii h))); intros.
apply PeanoNat.Nat.compare_eq_iff in H4.
lia.
apply PeanoNat.Nat.compare_lt_iff in H4.
lia.
rewrite H4 in H1; inversion H1.
rewrite H3 in H0; inversion H0.
Qed.

Theorem CharRangeExcept_equiv_empty: forall l h, (h <? l)%char = true -> (CharRangeExcept l h) =R= AnyChar.
Proof.
unfold re_eq.
induction s.
simpl.
auto.
simpl.
case_eq ((l <=? a)%char); intros; auto.
case_eq ((a <=? h)%char); intros; auto.
simpl.
unfold leb in *.
unfold ltb in *.
unfold Ascii.compare in *.
rewrite Nnat.N2Nat.inj_compare in *.
assert (PeanoNat.Nat.compare
        (BinNat.N.to_nat (N_of_ascii h))
        (BinNat.N.to_nat (N_of_ascii l)) = Lt).
induction (PeanoNat.Nat.compare
        (BinNat.N.to_nat (N_of_ascii h))
        (BinNat.N.to_nat (N_of_ascii l))); auto; inversion H.
apply PeanoNat.Nat.compare_lt_iff in H2.
case_eq (PeanoNat.Nat.compare
         (BinNat.N.to_nat (N_of_ascii l))
         (BinNat.N.to_nat (N_of_ascii a))); intros.
apply PeanoNat.Nat.compare_eq_iff in H3.
case_eq (PeanoNat.Nat.compare
         (BinNat.N.to_nat (N_of_ascii a))
         (BinNat.N.to_nat (N_of_ascii h))); intros.
apply PeanoNat.Nat.compare_eq_iff in H4.
lia.
apply PeanoNat.Nat.compare_lt_iff in H4.
lia.
rewrite H4 in H1; inversion H1.
apply PeanoNat.Nat.compare_lt_iff in H3.
case_eq (PeanoNat.Nat.compare
         (BinNat.N.to_nat (N_of_ascii a))
         (BinNat.N.to_nat (N_of_ascii h))); intros.
apply PeanoNat.Nat.compare_eq_iff in H4.
lia.
apply PeanoNat.Nat.compare_lt_iff in H4.
lia.
rewrite H4 in H1; inversion H1.
rewrite H3 in H0; inversion H0.
Qed.

(** ** String *)
(** For simplicity, there is no [RegExp] constructor for string; however, the conversion is easy. *)

Fixpoint string_to_re (s:string):RegExp :=
match s with
| EmptyString => Eps
| String a s' => (Char a) ++ (string_to_re s')
end.

Lemma string_to_re_true : forall s:string, (string_to_re s) ~== s.
Proof.
  induction s.
    simpl.  auto.
    simpl.  destruct(ascii_dec a a).
      erewrite Cat_left_id_s.  auto.
      elim n.  auto. 
Qed.

Lemma string_to_re_false : forall s s':string, s <> s' -> (string_to_re s) ~!= s'.
Proof.
  induction s.
    intros s' Hs.  simpl.  eapply Eps_false.  auto.
    induction s'.  
      intros Hs.  simpl.  auto.
      intro Hs.  simpl.  destruct(ascii_dec a a0).
        erewrite Cat_left_id_s.  rewrite e in Hs.  eapply IHs.
        destruct(string_dec s s').
          rewrite e0 in Hs.  elim Hs. auto.
          auto.
        erewrite Cat_left_zero_s.  auto.
Qed.
