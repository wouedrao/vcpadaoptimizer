(* *********************************************************************)
(*                                                                     *)
(*                 Coqlex verified lexer generator                     *)
(*                                                                     *)
(*  Copyright 2022 Siemens Mobility SAS and Institut National de       *)
(*  Recherche en Informatique et en Automatique.                       *)
(*  All rights reserved. This file is distributed under                *)
(*  the terms of the INRIA Non-Commercial License Agreement (see the   *)
(*  LICENSE file).                                                     *)
(*                                                                     *)
(* *********************************************************************)

let copy_right = Format.sprintf {|(* *********************************************************************)
(*                                                                     *)
(*                 Coqlex verified lexer generator                     *)
(*                                                                     *)
(*  Copyright %d Siemens Mobility SAS and Institut National de       *)
(*  Recherche en Informatique et en Automatique.                       *)
(*  All rights reserved. This file is distributed under                *)
(*  the terms of the INRIA Non-Commercial License Agreement (see the   *)
(*  LICENSE file).                                                     *)
(*                                                                     *)
(* *********************************************************************)|} (1900 + (Unix.gmtime (Unix.time ())).Unix.tm_year)

open ParserUtils
open LexerUtils
open Parser
open CoqLexLexer
open Definitions
open RValues
open Unix
open Sys
open Str

let buffer_size = 8192;;
let buffer = Bytes.create buffer_size;;

let file_copy input_name output_name =
  let fd_in = openfile input_name [O_RDONLY] 0 in
  let fd_out = openfile output_name [O_WRONLY; O_CREAT; O_TRUNC] 0o777 in
  let rec copy_loop () = match read fd_in buffer 0 buffer_size with
    |  0 -> ()
    | r -> ignore (write fd_out buffer 0 r); copy_loop ()
  in
  copy_loop ();
  close fd_in;
  close fd_out;;

let set_filename (fname:string) (lexbuf: Lexing.lexbuf)  =
    lexbuf.lex_curr_p <-
        { lexbuf.lex_curr_p with pos_fname = fname }
    ; lexbuf


let create_directory ?perm:(perm = 0o777) dirname =
	if (file_exists dirname) then
			if not (is_directory dirname) then
				failwith ("Please remove your file named " ^ dirname)
			else
				()
		else
			Unix.mkdir dirname perm

module VlParser =
struct
	module M = ParserUtils.GParser(struct type t = Parser.vllStruct let default_fuel = 1000000 type storage = char list let coq_lexer = CoqLexLexer.thelexer let menhirParser = Parser.prog end);;
	let parse_from_mllexbuf = M.parse_from_mllexbuf;;
	let parse_from_coqlexbuf = M.parse_from_coqlexbuf ;;
	let mlparse_from_file path = let lexbuf = set_filename path (Lexing.from_string (filepath_to_string path)) in parse_from_mllexbuf lexbuf [];;
	let coqparse_from_file path = let lexbuf = LexerDefinition.lexbuf_from_string (char_list_of_string (filepath_to_string path)) in parse_from_coqlexbuf lexbuf [];;
end;;

let char_print h = 
    let code = Char.code h in
    if code >= 32 && code <= 126 && code <> 34 then
        Format.sprintf {| "%c"%%char |} h
    else
        Format.sprintf {| "%03i"%%char |} code
        

let rec alist_print f alist = match alist with
| [] -> Format.fprintf f ""
| h::[] -> Format.fprintf f "(Char %s)" (char_print h)
| h::t -> Format.fprintf f "(Or (Char %s) %a)" (char_print h) alist_print t

let rec aelist_print f alist = match alist with
| [] -> Format.fprintf f ""
| h::[] -> Format.fprintf f "(CharRangeExcept %s %s)" (char_print h) (char_print h)
| h::t -> Format.fprintf f "(And (CharRangeExcept %s %s) %a)" (char_print h) (char_print h) aelist_print t

let rec rlist_print f rlist = match rlist with
| [] -> Format.fprintf f ""
| MCR(s, e)::[] -> Format.fprintf f "(CharRange %s %s)" (char_print s) (char_print e)
| MCR(s, e)::t -> Format.fprintf f "(Or (CharRange %s %s) %a)" (char_print s) (char_print e) rlist_print t

let rec relist_print f rlist = match rlist with
| [] -> Format.fprintf f ""
| MCR(s, e)::[] -> Format.fprintf f "(CharRangeExcept %s %s)" (char_print s) (char_print e)
| MCR(s, e)::t -> Format.fprintf f "(And (CharRangeExcept %s %s) %a)" (char_print s) (char_print e) relist_print t

let mclass_print f mclass = match mclass with
| MCC ([], []) ->  Format.fprintf f ""
| MCC ([], rlist) ->  Format.fprintf f "%a" rlist_print rlist
| MCC (alist, []) ->  Format.fprintf f "%a" alist_print alist
| MCC (alist, rlist) ->  Format.fprintf f "(Or %a %a)" alist_print alist rlist_print rlist

let meclass_print f mclass = match mclass with
| MCC ([], []) ->  Format.fprintf f ""
| MCC ([], rlist) ->  Format.fprintf f "%a" relist_print rlist
| MCC (alist, []) ->  Format.fprintf f "%a" aelist_print alist
| MCC (alist, rlist) ->  Format.fprintf f "(And %a %a)" aelist_print alist relist_print rlist

let rec print_coq_string_b f clist = match clist with
| [] -> Format.fprintf f {|""%%string|}
| h::t -> Format.fprintf f "(String %s %a)" (char_print h) print_coq_string_b t

let char_print_b h = let code = Char.code h in code >= 32 && code <= 126 && code <> 34

let rec print_coq_string_s f clist = match clist with
| [] -> Format.fprintf f "\"%%string"
| h::t -> Format.fprintf f "%c%a" h print_coq_string_s t

let print_coq_string f clist = 
    if List.for_all char_print_b clist then Format.fprintf f "\"%a" print_coq_string_s clist
    else print_coq_string_b f clist

let rec mregex_print f regex = match regex with
| MChar c -> Format.fprintf f "Char %s" (char_print c)
| MEps -> Format.fprintf f "Eps"
| MCat (r1, r2) -> Format.fprintf f "Cat (%a) (%a)" mregex_print r1 mregex_print r2
| MOr (r1, r2) -> Format.fprintf f "Or (%a) (%a)" mregex_print r1 mregex_print r2
| MStar (r1) -> Format.fprintf f "Star (%a)" mregex_print r1
| MAnd (r1, r2) -> Format.fprintf f "And (%a) (%a)" mregex_print r1 mregex_print r2
| MMinus (r1, r2) -> Format.fprintf f "Minus (%a) (%a)" mregex_print r1 mregex_print r2
| MIdent s ->  Format.fprintf f "%s" (string_of_char_list s)
| MFromStr s ->  Format.fprintf f "RegExp.Char.string_to_re %a" print_coq_string s
| MAcceptClass c -> Format.fprintf f "%a" mclass_print c
| MNotAcceptClass c -> Format.fprintf f "%a" meclass_print c
;;

let rec process_bind_list f bind_list = match bind_list with
| [] -> Format.fprintf f ""
| (id, value)::t -> Format.fprintf f "Definition %s := %a.@.%a" (string_of_char_list id) mregex_print value process_bind_list t ;;

let id_re = Str.regexp {|[_a-zA-Z]\(\.?[_a-zA-Z0-9]\)*|}

let reg_f current_defined_lexers all_lexers ident =
    if List.mem ident current_defined_lexers then
        Printf.sprintf "(%s n)" ident
    else if List.mem ident all_lexers then
        Printf.sprintf "(%s (fuel * 5000))" ident
    else
        ident

let opt_search_forward re s pos =
  try Some(Str.search_forward re s pos) with Not_found -> None

let global_substitute expr repl_fun text =
  let rec replace accu start last_was_empty =
    let startpos = if last_was_empty then start + 1 else start in
    if startpos > String.length text then
      string_after text start :: accu
    else
      match opt_search_forward expr text startpos with
      | None ->
          string_after text start :: accu
      | Some pos ->
          let end_pos = match_end () in
          let found = String.sub text pos (end_pos-pos) in
          let repl_text = repl_fun found in
          replace (repl_text :: String.sub text start (pos-start) :: accu)
                  end_pos (end_pos = pos)
  in
    String.concat "" (List.rev (replace [] 0 false))

let extract_all_lexers_from_lg lg =
    let rec extract_all_lexers_from_lg_ acc  = function
    | [] -> acc
    | ((((s, _), _), _), _)::t -> extract_all_lexers_from_lg_ ((string_of_char_list s)::acc) t
    in
    extract_all_lexers_from_lg_ [] lg

let extract_all_lexers vll = match vll with
| (((_, _), lg), _) ->  List.concat (List.map extract_all_lexers_from_lg lg)

let rec process_regexXaction f (current_defined_lexers, all_lexers, rXa) = match rXa with
| [] -> ()
| (regex, action)::[] -> Format.fprintf f "(%a, %s)" mregex_print regex (global_substitute id_re (reg_f current_defined_lexers all_lexers) (string_of_char_list action))
| (regex, action)::t -> Format.fprintf f "(%a, %s);@ %a" mregex_print regex
    (global_substitute id_re (reg_f current_defined_lexers all_lexers) (string_of_char_list action))
    process_regexXaction (current_defined_lexers, all_lexers, t) ;;


let rec process_funXaction f (current_defined_lexers, all_lexers, rXa) = match rXa with
| [] -> ()
| (fu, action)::[] -> Format.fprintf f "(%s, %s)" (global_substitute id_re (reg_f current_defined_lexers all_lexers) (string_of_char_list fu)) (global_substitute id_re (reg_f current_defined_lexers all_lexers) (string_of_char_list action))
| (fu, action)::t -> Format.fprintf f "(%s, %s);@ %a" (Str.global_substitute id_re (reg_f current_defined_lexers all_lexers) (string_of_char_list fu)) (global_substitute id_re (reg_f current_defined_lexers all_lexers) (string_of_char_list action)) process_funXaction (current_defined_lexers, all_lexers, t) ;;

let process_t_policy_declaration f (tok_type, stor_type) =
    match tok_type, stor_type with
     | PolyToken, PolyStorage -> Format.fprintf f "{Token Storage: Set}"
     | _, PolyStorage -> Format.fprintf f "{Storage: Set}"
     | PolyToken, _ -> Format.fprintf f "{Token : Set}"
     | _, _ -> ()

let process_t_policy_generalize f (tok_type, stor_type) =
    match tok_type, stor_type with
     | CanGuestToken, CanGuestStorage -> ()
     | CanGuestToken, PolyStorage -> Format.fprintf f "(Action := semantic_action (Storage := Storage))"
     | CanGuestToken, FixedStorage stor -> Format.fprintf f "(Action := semantic_action (Storage := %s))" (string_of_char_list stor)

     | PolyToken, CanGuestStorage -> Format.fprintf f "(Action := semantic_action (Token := Token))"
     | PolyToken, PolyStorage -> Format.fprintf f "(Action := semantic_action (Token := Token) (Storage := Storage))"
     | PolyToken, FixedStorage stor -> Format.fprintf f "(Action := semantic_action (Token := Token) (Storage := %s))" (string_of_char_list stor)

     | FixedToken tok, CanGuestStorage -> Format.fprintf f "(Action := semantic_action (Token := %s))" (string_of_char_list tok)
     | FixedToken tok, PolyStorage -> Format.fprintf f "(Action := semantic_action  (Token := %s) (Storage := Storage))" (string_of_char_list tok)
     | FixedToken tok, FixedStorage stor -> Format.fprintf f "(Action := semantic_action  (Token := %s) (Storage := %s))" (string_of_char_list tok) (string_of_char_list stor)


let process_lexer f (is_fst_lexer, l_name, el_type, (regexpXaction, funXaction), tok_type, stor_type, (current_defined_lexers, all_lexers)) =
    Format.fprintf f
{code|%s %s %a fuel lexbuf storage {struct fuel} := match fuel with
| 0 => (AnalysisNoFuel lexbuf, storage)
| S n =>
  (@[<v 8>    match generalizing_elector %a %s ([@ %a],@ [%a]@ )@] (remaining_str lexbuf) with
    | Some (FunctionElected _ ac) => ac lexbuf storage
    | Some (RegexpElected cand) =>
        let (pref, suff) := string_split (electionScore cand) (remaining_str lexbuf) in
        (electionAction cand)
        {|
           s_pos := cur_pos lexbuf;
           str_val := pref;
           cur_pos := {|
                      l_number := l_number (cur_pos lexbuf);
                      c_number := c_number (cur_pos lexbuf) + electionScore cand;
                      abs_pos := abs_pos (cur_pos lexbuf) + electionScore cand |};
           remaining_str := suff |}
        storage
    | None => (AnalysisFailedEmptyToken lexbuf, storage)
    end
  )
end
|code}
    (if is_fst_lexer then "Fixpoint" else "with")
    (string_of_char_list l_name)
    process_t_policy_declaration (tok_type, stor_type)
    process_t_policy_generalize (tok_type, stor_type)
    (string_of_char_list el_type)
    process_regexXaction (current_defined_lexers, all_lexers, regexpXaction)
    process_funXaction (current_defined_lexers, all_lexers, funXaction)

let rec process_lexer_group f (is_first, group, current_defined_lexers, all_lexers) = match group with
| [] -> Format.fprintf f "."
| ((((l_name, el_type), (regexpXaction, funXaction)), tok_type), stor_type)::t -> Format.fprintf f "%a%a" process_lexer (is_first, l_name, el_type, (regexpXaction, funXaction), tok_type, stor_type, (current_defined_lexers, all_lexers)) process_lexer_group (false, t, current_defined_lexers, all_lexers)

let rec process_lexer_list_of_group f (group_list, all_lexers) = match group_list with
| [] -> ()
| h::t ->
    let current_defined_lexers = extract_all_lexers_from_lg h in
    Format.fprintf f "%a@.%a" process_lexer_group (true, h, current_defined_lexers, all_lexers)
    process_lexer_list_of_group (t, current_defined_lexers @ all_lexers)

let rec printlist f lst  = match lst with
| [] -> ()
| h::t -> Format.fprintf f "%s %a" h printlist t ;;

let extractionGenerate f (p_extract, filename_bn, list_of_group) = if p_extract then (Format.fprintf f {|
(* Extraction code *)
Require Coq.extraction.Extraction ExtrOcamlBasic ExtrOcamlChar ExtrOcamlString ExtrOcamlNatInt.
Extraction Blacklist Char String List.
Extraction "%s" lexbuf_from_string %a.|} 
    (filename_bn ^ ".ml")
    printlist (List.concat (List.map extract_all_lexers_from_lg list_of_group))) 


let codeGenerate f (((header, reg_def), list_of_group), trailler) p_extract filename_bn = Format.fprintf f {|%s

Add LoadPath "RegExpLib" as RegExp.
Require Import LexerDefinition RValues RegExp.Definitions RegExp.Char List CoqLexUtils.
Import ListNotations.

(*Header*)
%s

(*Regexp bindings*)
%a

(*Lexer definitions*)
%a

(*Trailler*)
%s

%a

|} copy_right
    (string_of_char_list header)
    process_bind_list reg_def
    process_lexer_list_of_group (list_of_group, [])
    (string_of_char_list trailler)
    extractionGenerate (p_extract, filename_bn, list_of_group)

let rec generateLexerModule f (module_name, lexer_list) = match lexer_list with
| [] -> ()
| h::t -> Format.fprintf f "let %s_ocamllexer = fun l s -> lexbuf_Adapter default_fuel %s.%s l s ;;@.let %s_coqlexer = fun l s -> coqlexbuf_Adapter default_fuel %s.%s l s ;;@.%a" h module_name h h module_name h generateLexerModule (module_name, t) ;;



let generateUtilCode f (module_name, vll) = Format.fprintf f {|%s

open Format
open Lexing
open Stream
open %s

exception UserException of string
exception NoRegexMatch
exception NoFuel
exception NoTokenSpecified

let string_of_char_list cl = String.concat "" (List.map (String.make 1) cl)

let char_list_of_string s = List.init (String.length s) (String.get s)

let to_ml_lexbuf coq_lexbuf =
    let cat = coq_lexbuf.str_val @@ coq_lexbuf.remaining_str in
 { refill_buff = (fun lexbuf -> lexbuf.lex_eof_reached <- true);
    lex_buffer =  Bytes.of_string (string_of_char_list cat);
    lex_buffer_len = List.length cat;
    lex_abs_pos = coq_lexbuf.cur_pos.abs_pos;
    lex_start_pos = 0;
    lex_curr_pos = List.length coq_lexbuf.str_val;
    lex_last_pos = 0;
    lex_last_action = 0;
    lex_mem = [||];
    lex_eof_reached = true;
    lex_start_p = {pos_fname = ""; pos_lnum  = coq_lexbuf.s_pos.l_number; pos_bol = coq_lexbuf.s_pos.c_number; pos_cnum = coq_lexbuf.s_pos.abs_pos};
    lex_curr_p = {pos_fname = ""; pos_lnum  = coq_lexbuf.cur_pos.l_number; pos_bol = coq_lexbuf.cur_pos.c_number; pos_cnum = coq_lexbuf.cur_pos.abs_pos};
  }

let set_ml_lexbuf coq_lexbuf lexbuf =
    let cat = coq_lexbuf.str_val @@ coq_lexbuf.remaining_str in
    lexbuf.lex_buffer <- Bytes.of_string (string_of_char_list cat);
    lexbuf.lex_buffer_len <- Bytes.length lexbuf.lex_buffer;
    lexbuf.lex_abs_pos <- coq_lexbuf.cur_pos.abs_pos;
    lexbuf.lex_start_pos <- 0;
    lexbuf.lex_curr_pos <- List.length coq_lexbuf.str_val;
	lexbuf.lex_start_p <- {{pos_fname = ""; pos_lnum  = coq_lexbuf.s_pos.l_number; pos_bol = coq_lexbuf.s_pos.c_number; pos_cnum = coq_lexbuf.s_pos.abs_pos} with pos_fname =
lexbuf.lex_start_p.pos_fname};
	lexbuf.lex_curr_p <- {{pos_fname = ""; pos_lnum  = coq_lexbuf.cur_pos.l_number; pos_bol = coq_lexbuf.cur_pos.c_number; pos_cnum = coq_lexbuf.cur_pos.abs_pos} with pos_fname = lexbuf.lex_curr_p.pos_fname}

let from_ml_lexbuf ml_lexbuf =
 {
    s_pos = {l_number = ml_lexbuf.lex_start_p.pos_lnum; c_number = ml_lexbuf.lex_start_p.pos_bol; abs_pos = ml_lexbuf.lex_start_p.pos_cnum};
    str_val = char_list_of_string (lexeme ml_lexbuf);
    cur_pos = {l_number = ml_lexbuf.lex_curr_p.pos_lnum; c_number = ml_lexbuf.lex_curr_p.pos_bol; abs_pos = ml_lexbuf.lex_curr_p.pos_cnum};
    remaining_str = char_list_of_string (Bytes.to_string (Bytes.sub ml_lexbuf.lex_buffer ml_lexbuf.lex_curr_pos ml_lexbuf.lex_buffer_len));
 }

let coqlexbuf_Adapter fuel coq_lexer lexbuf storage =
	match coq_lexer fuel lexbuf storage with
	| (AnalysisFailedUserRaisedError (err_mess, lexbuf), _) -> Format.printf "Error at position %%d:%%d@@." lexbuf.cur_pos.l_number lexbuf.cur_pos.c_number;
	    Stdlib.raise (UserException (string_of_char_list err_mess))
	| (AnalysisFailedEmptyToken (lexbuf), _) -> Format.printf "Error at position %%d:%%d@@." lexbuf.cur_pos.l_number lexbuf.cur_pos.c_number; Stdlib.raise NoRegexMatch
	| (AnalysisTerminated (Some tok, lexbuf), sto) -> (tok, lexbuf, sto)
	| (AnalysisTerminated (None, lexbuf), _) -> Format.printf "Error at position %%d:%%d@@." lexbuf.cur_pos.l_number lexbuf.cur_pos.c_number; Stdlib.raise NoTokenSpecified
	| (AnalysisNoFuel lexbuf, _) -> Format.printf "Error at position %%d:%%d@@." lexbuf.cur_pos.l_number lexbuf.cur_pos.c_number; Stdlib.raise NoFuel


let lexbuf_Adapter fuel coq_lexer ml_lexbuf storage =
    let coq_lexbuf = from_ml_lexbuf ml_lexbuf in
    match coq_lexer fuel coq_lexbuf storage with
	| (AnalysisFailedUserRaisedError(err_mess, lexbuf), _) -> Format.printf "Error at position %%d:%%d@@." lexbuf.cur_pos.l_number lexbuf.cur_pos.c_number; set_ml_lexbuf lexbuf ml_lexbuf; Stdlib.raise (UserException (string_of_char_list err_mess))
	| (AnalysisFailedEmptyToken (lexbuf), _) -> Format.printf "Error at position %%d:%%d@@." lexbuf.cur_pos.l_number lexbuf.cur_pos.c_number; set_ml_lexbuf lexbuf ml_lexbuf; Stdlib.raise NoRegexMatch
	| (AnalysisTerminated (Some tok, lexbuf), _) -> set_ml_lexbuf lexbuf ml_lexbuf; tok
	| (AnalysisTerminated (None, lexbuf), _) -> Format.printf "Error at position %%d:%%d@@." lexbuf.cur_pos.l_number lexbuf.cur_pos.c_number; set_ml_lexbuf lexbuf ml_lexbuf; Stdlib.raise NoTokenSpecified
	| (AnalysisNoFuel lexbuf, _) -> Format.printf "Error at position %%d:%%d@@." lexbuf.cur_pos.l_number lexbuf.cur_pos.c_number; set_ml_lexbuf lexbuf ml_lexbuf; Stdlib.raise NoFuel

let in_channel_to_string in_channel = 
	let rec in_channel_to_string_rec in_channel acc = 
		(
		  match Stdlib.input_line in_channel with
		    | exception End_of_file -> List.rev acc
		    | str -> in_channel_to_string_rec in_channel (str::acc)
		) in
	String.concat "\n" (in_channel_to_string_rec in_channel [])

let filepath_to_string path = let in_channel = Stdlib.open_in path in in_channel_to_string in_channel

let default_fuel = 50000
(*Lexers*)
%a
|} copy_right
module_name
generateLexerModule (module_name, extract_all_lexers vll)
;;



let rec generateLexerBench f lexer_list = match lexer_list with
| [] -> Format.fprintf f {|
            | _ -> failwith "Lexer not found"|}
| h::t -> Format.fprintf f {|
            | "%s" ->
                let count = ref 0 in
                let rec loop lexbuf storage =
                (
                    match %s_coqlexer lexbuf storage with
                    | (t, lb, _) when lb.remaining_str = [] -> (count := !count + 1; ())
                    | (t, lb, sto) -> (count := !count + 1; loop lb sto)
                )
                in
                let _ = Format.printf "Starting evaluation of %%s on %%s@@." !lexer_name path in
                let start = Unix.gettimeofday() in
                let _ = loop lexbuf storage_def in
                let end_ = Unix.gettimeofday() in
                Format.printf "Time = %%f (for %%d tokens)@@." (end_ -. start) !count %a|}
    h
    h
    generateLexerBench t


let mainMLGenerateUsual f (lexer_list, module_name) = Format.fprintf f {|%s

open Format
open %s
open Lib

(*Config*)
let storage_def = [] (*Change the value of storage_def to set the initial storage*)


let () =
    let usage_msg = Format.sprintf "A program to run a benchmark on a Coq-lexer.@;Usage : %%s filename -lexer_name string -default_str string@@." Sys.argv.(0) in
    let inputPath = ref None in
    let lexer_name = ref "" in
    let speclist =
        [
            ("-lexer", Arg.Set_string lexer_name, "The name of the lexer to benchmark")
        ] in
    Arg.parse speclist (fun anon -> inputPath := Some anon) usage_msg;
    match !inputPath with
    | None -> failwith "No input file"
    | Some path ->
        let _ = Format.printf "Reading input file@@." in
        let input_string = filepath_to_string path in
        let lexbuf = lexbuf_from_string (char_list_of_string input_string) in
        (match !lexer_name with %a)|} copy_right
        module_name
        generateLexerBench lexer_list ;;

let compileSHGenerate f module_name = Format.fprintf f {|#(* *********************************************************************)
#(*                                                                     *)
#(*                 Coqlex verified lexer generator                     *)
#(*                                                                     *)
#(*  Copyright %d Siemens Mobility SAS and Institut National de       *)
#(*  Recherche en Informatique et en Automatique.                       *)
#(*  All rights reserved. This file is distributed under                *)
#(*  the terms of the INRIA Non-Commercial License Agreement (see the   *)
#(*  LICENSE file).                                                     *)
#(*                                                                     *)
#(* *********************************************************************)
#!/bin/bash
coqc %s.v
ocamlopt -alert "-deprecated" unix.cmxa -cclib -lunix %s.mli %s.ml Lib.ml main.ml -o analyzer
rm -f %s.vos *.vok %s.ml %s.mli *.cmo *.cmi .*.aux *.o *.cmx *.glob 
|} (1900 + (Unix.gmtime (Unix.time ())).Unix.tm_year) module_name  module_name module_name module_name module_name module_name

;;

let () =
	begin
		let usage_msg = Format.sprintf "A program to generate a Coq-lexer .@;Usage : %s filename" Sys.argv.(0) in
		let inputPath = ref None in
		let ignore_main = ref false in
		let ignore_extract = ref false in
		let ignore_libml = ref false in
		let out_dir = ref "" in
		let speclist = [
		    ("--no-main", Arg.Set ignore_main, "Prevent the generation of main.ml and compile.sh"); 
		    ("--no-lib-ml", Arg.Set ignore_libml, "Prevent the generation of Lib.ml"); 
		    ("--no-extract", Arg.Set ignore_extract, "Prevent the generation of the extraction part in the generated Coq code"); 
		    ("-o", Arg.Set_string out_dir, "Set output directory name"); 
	    ] in
		Arg.parse speclist (fun anon -> inputPath := Some anon) usage_msg;
		
		match !inputPath with
		| None -> failwith "No input file"
		| Some path ->
		        let prog_dirname = Filename.dirname (Unix.realpath (Sys.argv.(0))) in
		        let base_name_wo_ext = Filename.remove_extension (Filename.basename path) in
		        let module_name = String.capitalize_ascii base_name_wo_ext in
				Format.printf "log------------------------@.";
		        Format.printf "Parsing %s@." path;
		        let parsed = VlParser.coqparse_from_file path in
				Format.printf "%s is parsed@." path;
				let inputfile_dirname = if !out_dir <> "" then !out_dir else (Filename.concat (Filename.dirname path) (Format.sprintf "CoqLexerGeneratedFor%s" module_name)) in
				Format.printf "Creating directory: %s@." inputfile_dirname;
				create_directory inputfile_dirname;
				Format.printf "Generating file %s.v@." module_name;
				let out_channel = open_out (Filename.concat inputfile_dirname ( module_name ^ ".v")) in
				codeGenerate (Format.formatter_of_out_channel out_channel) parsed (not !ignore_extract) module_name; close_out out_channel;
				if not !ignore_libml then
				(
				    Format.printf "Generating file Lib.ml@.";
				    let out_channel = open_out (Filename.concat inputfile_dirname ( "Lib.ml")) in
				    generateUtilCode (Format.formatter_of_out_channel out_channel) (module_name, parsed); close_out out_channel
			    );
				if not !ignore_main then
				(
				    Format.printf "Generating file compile.sh@.";
				    let out_channel = open_out (Filename.concat inputfile_dirname "compile.sh") in
				    compileSHGenerate (Format.formatter_of_out_channel out_channel) module_name; close_out out_channel;
				    chmod (Filename.concat inputfile_dirname "compile.sh") 0o740;
				    Format.printf "Generating file main.ml@.";
				    let out_channel = open_out (Filename.concat inputfile_dirname ( "main.ml")) in
				    mainMLGenerateUsual (Format.formatter_of_out_channel out_channel) (extract_all_lexers parsed, module_name); 
				    close_out out_channel
				);
				Format.printf "Copying coq files@.";
				file_copy (Filename.concat prog_dirname "LexerDefinition.vo") (Filename.concat inputfile_dirname "LexerDefinition.vo");
				file_copy (Filename.concat prog_dirname "CoqLexUtils.vo") (Filename.concat inputfile_dirname "CoqLexUtils.vo");
				file_copy (Filename.concat prog_dirname "RValues.vo") (Filename.concat inputfile_dirname "RValues.vo");
				file_copy (Filename.concat prog_dirname "MatchLenSimpl.vo") (Filename.concat inputfile_dirname "MatchLenSimpl.vo");
				file_copy (Filename.concat prog_dirname "MatchLen.vo") (Filename.concat inputfile_dirname "MatchLen.vo");
				file_copy (Filename.concat prog_dirname "ShortestLenSimpl.vo") (Filename.concat inputfile_dirname "ShortestLenSimpl.vo");
				file_copy (Filename.concat prog_dirname "ShortestLen.vo") (Filename.concat inputfile_dirname "ShortestLen.vo");
				file_copy (Filename.concat prog_dirname "RegexpSimpl.vo") (Filename.concat inputfile_dirname "RegexpSimpl.vo");
				let inputfile_lib_dirname = Filename.concat inputfile_dirname "RegExpLib" in
				create_directory inputfile_lib_dirname;
				file_copy (Filename.concat (Filename.concat prog_dirname "regexp_opt") "Boolean.vo") (Filename.concat inputfile_lib_dirname "Boolean.vo");
				file_copy (Filename.concat (Filename.concat prog_dirname "regexp_opt") "Concat.vo") (Filename.concat inputfile_lib_dirname "Concat.vo");
				file_copy (Filename.concat (Filename.concat prog_dirname "regexp_opt") "Includes.vo") (Filename.concat inputfile_lib_dirname "Includes.vo");
				file_copy (Filename.concat (Filename.concat prog_dirname "regexp_opt") "Star.vo") (Filename.concat inputfile_lib_dirname "Star.vo");
				file_copy (Filename.concat (Filename.concat prog_dirname "regexp_opt") "Char.vo") (Filename.concat inputfile_lib_dirname "Char.vo");
				file_copy (Filename.concat (Filename.concat prog_dirname "regexp_opt") "Definitions.vo") (Filename.concat inputfile_lib_dirname "Definitions.vo");
				file_copy (Filename.concat (Filename.concat prog_dirname "regexp_opt") "RegExp.vo") (Filename.concat inputfile_lib_dirname "RegExp.vo");
				file_copy (Filename.concat (Filename.concat prog_dirname "regexp_opt") "Utility.vo") (Filename.concat inputfile_lib_dirname "Utility.vo");
				Format.printf "-----------------end of log@.";
				Format.printf "The lexer generation has been successfully executed in %s@." inputfile_dirname; 
				if not !ignore_main then
				    Format.printf "Now go to %s and execute compile.sh@." inputfile_dirname
    end
