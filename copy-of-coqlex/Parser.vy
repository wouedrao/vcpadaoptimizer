(* *********************************************************************)
(*                                                                     *)
(*                 Coqlex verified lexer generator                     *)
(*                                                                     *)
(*  Copyright 2021 Siemens Mobility SAS and Institut National de       *)
(*  Recherche en Informatique et en Automatique.                       *)
(*  All rights reserved. This file is distributed under                *)
(*  the terms of the INRIA Non-Commercial License Agreement (see the   *)
(*  LICENSE file).                                                     *)
(*                                                                     *)
(* *********************************************************************)

%{
Require Import List.
Require Import Coq.Strings.Ascii Coq.Strings.String.
Import ListNotations.

Inductive MCharRange :=
| MCR : ascii -> ascii -> MCharRange.

Inductive MCharClass :=
| MCC : (list ascii) -> (list MCharRange) -> MCharClass.

Definition MCC_from_ascii_list ascii_list := MCC ascii_list [].

Definition MCC_from_MCharRange mcharrange := MCC [] [mcharrange].

Definition MCC_add_ascii_list mcc ascii_list := 
match mcc with
| MCC al mr => MCC (List.app al ascii_list) mr
end.

Definition MCC_add_MCharRange mcc mcharrange := 
match mcc with
| MCC al mr => MCC al (List.app mr mcharrange)
end.

Definition MCC_add mcc mcc2 :=
match mcc, mcc2 with
| MCC al mr, MCC al2 mr2 => MCC (List.app al al2) (List.app mr mr2)
end.

Inductive MRegExp := 
| MChar : ascii -> MRegExp
| MAcceptClass : MCharClass -> MRegExp
| MNotAcceptClass : MCharClass -> MRegExp
| MIdent : string -> MRegExp
| MCat : MRegExp -> MRegExp -> MRegExp
| MStar : MRegExp -> MRegExp
| MOr : MRegExp -> MRegExp -> MRegExp
| MAnd : MRegExp -> MRegExp -> MRegExp
| MMinus : MRegExp -> MRegExp -> MRegExp
| MFromStr : string -> MRegExp
| MEps : MRegExp.

Inductive tset_token :=
| PolyToken
| CanGuestToken
| FixedToken : string -> tset_token.

Inductive tset_storage :=
| PolyStorage
| CanGuestStorage
| FixedStorage : string -> tset_storage.

(* lexer name = elector regex {}... $(fun) {}... *)
Definition lexerData : Set := string * string * ((list (MRegExp * string)) * (list (string * string))) * tset_token * tset_storage.

(* @Group storage_type token_type lexers *)
Definition lexerGroupe : Set :=  list lexerData.

(* {header} defs lexerGroupe {trailer} *)
Definition vllStruct : Set := string * (list (string * MRegExp)) * list lexerGroupe * string.

%}

%token LPARENT RPARENT LBRACKET RBRACKET CIRCUMFLEX STAR UNDERSCORE PLUS VBAR QMARK EOF MINUS RULE AND EQ PARSE DOLLARD LET THEN SHORTEST EOF_SYMB STORAGE_POLY TOKEN_POLY
%token <ascii> CH_LIT
%token <string> BRACECONTENT IDENT STR_LIT STORAGE_SET_TYPE TOKEN_SET_TYPE

%start<vllStruct> prog
%type <list lexerGroupe> lexer_groups
%type <string> elector
%type <tset_token> tok_set
%type <tset_storage> storage_set
%type <tset_token * tset_storage> t_setting
%type <string> detect_fun
%type <MRegExp> regex term factor expression atom
%type <string> header bracecontent bracecontent_opt trailler
%type <(list (MRegExp * string)) * (list (string * string))> lexer_definition
%type <lexerData> lexer
%type <lexerGroupe> lexer_list lexers
%type <MCharClass> characterrange characterclass 
%type <string * MRegExp> regex_bind_one
%type <list (string * MRegExp)> regex_bind_list regex_bind
%%

prog : | h = header b = regex_bind lg = lexer_groups t = trailler EOF { (h, b, lg, t) }

regex_bind :  |  { [] }
              | l = regex_bind_list { l }

regex_bind_list : | b = regex_bind_one { [b] }
                  | l = regex_bind_list b = regex_bind_one { List.app l [b] }

regex_bind_one : LET i = IDENT EQ r = regex { (i, r) }

bracecontent : | c = BRACECONTENT { c }

bracecontent_opt : | { ""%string }
                   | c = bracecontent  { c }

header : | c = bracecontent_opt { c }

trailler : | c = bracecontent_opt { c }

lexer_groups : | g = lexers  { [g] }
               | l = lexer_groups THEN g = lexers { List.app l [g] }



lexers : | RULE l = lexer_list { l }

lexer_list :  | l = lexer { [l] }
              | ls = lexer_list AND l = lexer { List.app ls [l] }

tok_set : 
  | TOKEN_POLY          {PolyToken}
  | t = TOKEN_SET_TYPE  {FixedToken t}
  
storage_set : 
  | STORAGE_POLY          {PolyStorage}
  | t = STORAGE_SET_TYPE  {FixedStorage t}

t_setting : 
            {(CanGuestToken, CanGuestStorage)}
            | t = tok_set {(t, CanGuestStorage)}
            | st = storage_set {(CanGuestToken, st)}
            | t = tok_set st = storage_set {(t, st)}
            | st = storage_set t = tok_set {(t, st)}



lexer : id = IDENT EQ e = elector l = lexer_definition t = t_setting { (id, e, l, fst t, snd t) }

elector :   PARSE { "LexerDefinition.longest_match_elector"%string }
          | SHORTEST { "LexerDefinition.shortest_match_elector"%string } 

lexer_definition : r = regex c = bracecontent { ([(r, c)], []) }
                   | r = detect_fun c = bracecontent { ([], [(r, c)]) }
                   | VBAR r = regex c = bracecontent { ([(r, c)], []) }
                   | VBAR r = detect_fun c = bracecontent { ([], [(r, c)]) }
                   | l = lexer_definition VBAR r = regex c = bracecontent { (List.app (fst l) [(r, c)], (snd l)) }
                   | l = lexer_definition VBAR r = detect_fun c = bracecontent { ((fst l), List.app (snd l) [(r, c)]) }

detect_fun: DOLLARD c = bracecontent { c }
            | EOF_SYMB { "CoqLexUtils.EOF"%string }

regex :       | e = expression { e }

expression :  | t = term { t }
              | e = expression VBAR t = term {MOr e t}


term :        | f = factor { f }
              | t = term f = factor {MCat t f}
              | t = term MINUS f = factor {MMinus t f}

factor :      | a = atom  { a }
              | t = atom STAR {MStar t} 
              | t = atom PLUS {MCat t (MStar t)} 
              | t = atom QMARK {MOr MEps t} 

atom : | c = CH_LIT {MChar c}
       | UNDERSCORE { MIdent "RValues.regex_any"%string }
       | LPARENT e = expression RPARENT { e }
       | LBRACKET c = characterclass RBRACKET { MAcceptClass c }
       | LBRACKET CIRCUMFLEX c = characterclass RBRACKET { MNotAcceptClass c }
       | s = STR_LIT {MFromStr s}
       | id = IDENT { MIdent id }


characterclass :  
       | c = characterrange { c }
       | c1 = characterclass c = characterrange {MCC_add c1 c}

characterrange : 
       | c = CH_LIT { MCC_from_ascii_list [c] }
       | b = CH_LIT MINUS e = CH_LIT {MCC_from_MCharRange (MCR b e)}
