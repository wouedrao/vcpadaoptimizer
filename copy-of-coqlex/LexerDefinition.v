Add LoadPath "regexp_opt" as RegExp.

Require Import RegExp.Definitions.
Require Import Lia.
Require Import MatchLenSimpl.
Require Import ShortestLenSimpl.
Require Import Coq.Lists.List String.
Import ListNotations.
Require Import Recdef Coq.Program.Wf.

Record Position : Set := mkPosition
{
  l_number : nat;
  c_number : nat;
  abs_pos : nat
}.

Definition OPos := {| l_number := 1;  c_number := 0;  abs_pos := 0 |}.

Record Lexbuf : Set := mkLexbuf
{
  s_pos : Position
  ;str_val : string
  ;cur_pos : Position
  ;remaining_str : string
}.

Definition lexbuf_from_string str := {|
           s_pos := OPos;
           str_val := EmptyString;
           cur_pos := OPos;
           remaining_str := str |}.

Inductive LexicalAnalysisResult {Token : Set} : Set :=
| AnalysisTerminated : option Token -> Lexbuf -> LexicalAnalysisResult
| AnalysisFailedUserRaisedError : string -> Lexbuf -> LexicalAnalysisResult
| AnalysisFailedEmptyToken : Lexbuf -> LexicalAnalysisResult
| AnalysisNoFuel : Lexbuf -> LexicalAnalysisResult
.

(* Election... *)

Record RegexpElectionResult {Action : Set} : Set := mkRegexpElectionResult
{
   electionRegexp : RegExp
  ;electionAction : Action
  ;electionScore : nat
}.

(** Longest match correctness **)

Fixpoint longest_match_elector {Action : Set} lst str := match lst with
| [] => None
| (re, ac)::t => 
  (
    match match_len re str with
    | None => longest_match_elector t str
    | Some n => 
        Some (
          match longest_match_elector t str with
          | None => (mkRegexpElectionResult Action re ac n)
          | Some candidate => (if Nat.ltb n (electionScore candidate) then candidate else (mkRegexpElectionResult Action re ac n) )
          end
        )
    end
  )
end.

(*** longest_match_elector properties ***)

Lemma longest_match_elector_None {Action : Set}: forall l str, longest_match_elector (Action:=Action) l str = None <-> (forall re ac, In (re, ac) l -> match_len re str = None).
Proof.
induction l; simpl.
split; intros; auto.
inversion H0.
induction a.
intro.
case_eq (match_len a str).
split.
intros.
inversion H0.
intros.
assert ((a, b) = (a, b) \/ In (a, b) l).
left; auto.
assert (D := H0 a b H1).
rewrite H in D.
inversion D.
split.
intros.
inversion H1.
inversion H2; subst; auto.
assert (D := IHl str).
inversion D.
eapply H3 in H0; eauto.
intros.
apply IHl.
intros.
eapply H0; eauto.
Qed.

Lemma longest_match_elector_max {Action : Set}: forall l str h, longest_match_elector (Action:=Action) l str = Some h -> (forall re ac n, In (re, ac) l -> match_len re str = Some n -> (electionScore h) >= n).
Proof.
induction l; simpl.
intros.
inversion H.
induction a.
intros str h.
case_eq (match_len a str); intros n H.
case_eq (longest_match_elector l str); intros r H0.
case_eq (Nat.ltb n (electionScore r)); intros.
inversion H2; subst.
inversion H3.
inversion H5; subst.
rewrite H in H4; inversion H4; subst.
apply PeanoNat.Nat.ltb_lt in H1; lia.
eapply IHl; eauto.
inversion H2; subst.
simpl.
inversion H3.
inversion H5; subst.
rewrite H in H4; inversion H4; subst; auto.
assert (D := IHl str r H0 re ac n0 H5 H4).
apply PeanoNat.Nat.ltb_ge in H1; lia.
intros.
inversion H1.
inversion H3; subst.
rewrite H in H2; inversion H2; subst.
inversion H0; subst.
simpl.
auto.
assert (D := longest_match_elector_None (Action := Action) l str).
inversion D.
assert (D' := H4 r re ac H3).
rewrite H2 in D'; inversion D'.
intros.
inversion H0.
inversion H2; subst.
rewrite n in H1; inversion H1.
eauto.
Qed.

Lemma longest_match_elector_priority {Action : Set}: 
  forall l str re ac cand n, 
    match_len re str = Some n ->
    longest_match_elector (Action:=Action) l str = Some cand -> 
    n = (electionScore cand) ->
    longest_match_elector (Action:=Action) ((re, ac)::l) str = Some (mkRegexpElectionResult Action re ac n).
Proof.
intros.
simpl.
rewrite H, H0.
subst.
assert (Nat.ltb (electionScore cand) (electionScore cand) = false).
apply PeanoNat.Nat.ltb_ge.
lia.
rewrite H1.
auto.
Qed.

(** Shortest match elector*)
Fixpoint shortest_match_elector {Action : Set} lst str := match lst with
| [] => None
| (re, ac)::t => 
  (
    match shortest_match_len re str with
    | None => shortest_match_elector t str
    | Some n => 
        Some (
          match shortest_match_elector t str with
          | None => (mkRegexpElectionResult Action re ac n)
          | Some candidate => (if Nat.leb n (electionScore candidate) then (mkRegexpElectionResult Action re ac n) else candidate )
          end
        )
    end
  )
end.

(*** shortest_match_elector properties ***)

Lemma shortest_match_elector_None {Action : Set}: forall l str, shortest_match_elector (Action:=Action) l str = None <-> (forall re ac, In (re, ac) l -> shortest_match_len re str = None).
Proof.
induction l; simpl.
split; intros; auto.
inversion H0.
induction a.
intro.
case_eq (shortest_match_len a str).
split.
intros.
inversion H0.
intros.
assert ((a, b) = (a, b) \/ In (a, b) l).
left; auto.
assert (D := H0 a b H1).
rewrite H in D.
inversion D.
split.
intros.
inversion H1.
inversion H2; subst; auto.
assert (D := IHl str).
inversion D.
eapply H3 in H0; eauto.
intros.
apply IHl.
intros.
eapply H0; eauto.
Qed.

Lemma shortest_match_elector_min {Action : Set}: forall l str h, shortest_match_elector (Action:=Action) l str = Some h -> (forall re ac n, In (re, ac) l -> shortest_match_len re str = Some n -> (electionScore h) <= n).
Proof.
induction l; simpl.
intros.
inversion H.
induction a.
intros str h.
case_eq (shortest_match_len a str).
+ intros n H; case_eq (shortest_match_elector l str); intros.
  case_eq (Nat.leb n (electionScore r)); intros; rewrite H4 in *.
  simpl; apply PeanoNat.Nat.leb_le in H4.
  inversion H1; subst; simpl.
  inversion H2.
  inversion H5; subst.
  rewrite H in H3; inversion H3; subst; lia.
  assert (electionScore r <= n0).
  eapply IHl; eauto.
  lia.
  simpl; apply PeanoNat.Nat.leb_gt in H4.
  inversion H1; subst; simpl.
  inversion H2.
  inversion H5; subst.
  rewrite H in H3; inversion H3; subst; lia.
  eapply IHl; eauto.
  inversion H1; subst; simpl.
  inversion H2.
  inversion H4; subst.
  rewrite H in H3; inversion H3; subst; lia.
  assert (forall (re : RegExp) (ac : Action),
   In (re, ac) l -> shortest_match_len re str = None).
  apply shortest_match_elector_None; auto.
  assert (H' := H5 re ac H4).
  rewrite H3 in H'.
  inversion H'.
+ intros.
  inversion H1.
  inversion H3; subst.
  rewrite H in H2.
  inversion H2.
  eapply IHl ; eauto.
Qed.  

Lemma shortest_match_elector_priority {Action : Set}: 
  forall l str re ac cand n, 
    shortest_match_len re str = Some n ->
    shortest_match_elector (Action:=Action) l str = Some cand -> 
    n = (electionScore cand) ->
    shortest_match_elector (Action:=Action) ((re, ac)::l) str = Some (mkRegexpElectionResult Action re ac n).
Proof.
intros.
simpl.
rewrite H, H0.
subst.
assert (Nat.leb (electionScore cand) (electionScore cand) = true).
apply PeanoNat.Nat.leb_le.
lia.
rewrite H1.
auto.
Qed.

(** Generalizing elector **)
Inductive electionResult {Action : Set} : Set := 
| RegexpElected : RegexpElectionResult (Action := Action) -> electionResult
| FunctionElected : (string -> bool) -> Action -> electionResult.

Definition elector {Action : Set} : Type := 
  (list (RegExp * Action)) * (list ((string -> bool) * Action)) -> string -> option (electionResult (Action := Action)).

Fixpoint function_selector {Action : Set} (fun_list: list ((string -> bool) * Action)) str := match fun_list with
| [] => None
| (fn, ac)::t => if fn str then Some (fn, ac) else function_selector t str
end.

Definition generalizing_elector {Action : Set} (regex_elector: list (RegExp * Action) -> string -> option (RegexpElectionResult (Action:=Action))) re_fun str := match re_fun with
| (re_ac, fun_ac) => 
    (
      match function_selector fun_ac str with
      | None => 
        (
          match regex_elector re_ac str with
          | None => None
          | Some can => Some (RegexpElected can)
          end
        )
      | Some (fn, ac) => Some (FunctionElected fn ac)
      end
    )
end.

(* Election - End *)

(* String Utils *)
Fixpoint string_split n str := match str with
| EmptyString => (EmptyString, EmptyString)
| String h t => 
  (
    match n with
    | 0 => (EmptyString, String h t)
    | S n' => 
      (
        let (l, r) := string_split n' t in
        (String h l, r)
      )
    end
  )
end.

Lemma string_split_concat : forall n str l r, string_split n str = (l, r) -> append l r = str.
Proof.
induction n, str; simpl; intros.
inversion H; subst; auto.
inversion H; subst; auto.
inversion H; subst; auto.
case_eq (string_split n str); intros; rewrite H0 in *.
apply IHn in H0.
inversion H; subst.
auto.
Qed.

Lemma string_split_left : forall n str l r, string_split n str = (l, r) -> l = substring 0 n str.
Proof.
induction n, str; simpl; intros.
inversion H; subst; auto.
inversion H; subst; auto.
inversion H; subst; auto.
case_eq (string_split n str); intros; rewrite H0 in *.
apply IHn in H0.
inversion H; subst.
auto.
Qed.

Lemma string_split_right : forall n str l r, string_split n str = (l, r) -> r = substring n (length str - n) str.
Proof.
induction n, str; simpl; intros.
inversion H; subst; auto.
inversion H; subst; auto.
f_equal.
induction str; simpl; auto.
f_equal.
auto.
inversion H; subst; auto.
case_eq (string_split n str); intros; rewrite H0 in *.
apply IHn in H0.
inversion H; subst.
auto.
Qed.

Definition semantic_action {Token : Set} {Storage : Set} : Set := Lexbuf -> Storage -> LexicalAnalysisResult (Token := Token) * Storage.














