from os import listdir, path
from statistics import mean, stdev
import numpy as np
import json
import math
input_dir = "results"

def fcontent(f):
    tmp = open(f, "r")
    content = tmp.read()
    tmp.close()
    return content

def poly_eq(coeffs):
    return lambda x: sum([coeffs[i]*(x**i) for i in range(0,len(coeffs))])

def process_dir(input_dir):
    fs = listdir(input_dir)
    fs.sort(key = lambda x: int(x[:x.find(".")]))
    #print(fs)
    fswid = fs
    fs = [input_dir+"/"+f for f in fs]
    # [:int(len(fs)/2)]
    cs = [fcontent(f) for f in fs]
    js = [json.loads(c) for c in cs]

    for j in js:
        if j["rest_len"] != 0:
            print("WARNING: {} was not processed entirely".format(j["fname"]))
    return (fswid, js)

def process_set_of(input_dir):
    fs = listdir(input_dir)
    fs = [d for d in fs if path.isdir(input_dir + "/" + d)]
    fout = []
    if len(fs):
        fs0, js0 = process_dir(input_dir + "/" + fs[0])
        fout = []
        fout.append((fs[0], js0))
        for d in fs[1:]:
            fsd, jsd = process_dir(input_dir + "/" + d)
            if fsd == fs0:
                fout.append((d, jsd))
            else:
                print("WARNING: {} does not have the same set of results as {}".format(d, fs[0]))
    return fout
            
def print_token_by_sec(js, name):
    perf_arr = [j["input_len"]/mean(j["times"]) for j in js]
    print ("{} : {} ; array : {}".format(name, mean(perf_arr), perf_arr))
    

fout = process_set_of(input_dir)

for (name, js) in fout:
    print_token_by_sec(js, name)


