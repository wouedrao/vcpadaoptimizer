Add LoadPath "regexp_opt" as RegExp.
Require Import RegExp.Definitions.
Require Import RegExp.Utility.
Require Import RegexpSimpl.
Require Import ShortestLen.
Require Import Ascii.

Fixpoint shortest_match_len re s := 
match re with
| Empty => None
| Eps => Some 0
| Star _ => Some 0
| _ => 
(
  match s with
  | EmptyString => if nu re then Some 0 else None
  | String a s2 => if nu re then Some 0 else (match shortest_match_len (RegexpSimpl.derive a re) s2 with
                  | Some l => Some (S l)
                  | None => None
                  end)
  end
)
end.

Lemma shortest_match_len_empty : forall s, ShortestLen.shortest_match_len Empty s = None.
Proof.
induction s; simpl; auto.
rewrite IHs; auto.
Qed.

Lemma shortest_match_len_Eps : forall s, ShortestLen.shortest_match_len Eps s = Some 0.
Proof.
induction s; simpl; auto.
Qed.

Lemma shortest_match_len_Star : forall s r, ShortestLen.shortest_match_len (Star r) s = Some 0.
Proof.
induction s; simpl; auto.
Qed.

Lemma shortest_match_len_correct : forall str r, shortest_match_len r str = ShortestLen.shortest_match_len r str.
Proof.
induction str; simpl.
+ induction r; simpl; auto.
+ induction r; auto; try rewrite IHstr; auto; first [rewrite shortest_match_len_empty; auto | erewrite shortest_match_len_re_eq; eauto; apply derive_simpl_correct].
Qed.

Fixpoint shortest_match_len_term_rec re s term : option nat := 
match re with
| Empty => term None
| Eps => term (Some 0)
| Star _ => term (Some 0)
| _ => 
(
  match s with
  | EmptyString => term (if nu re then Some 0 else None)
  | String a s2 => if nu re then term (Some 0) else 
                      (shortest_match_len_term_rec (RegexpSimpl.derive a re) s2 
                        (fun res => match res with | Some l => term (Some (S l)) | None => term None end)
                      )
end)
end.

Theorem general_equivalence_of_shortest_match_len_and_shortest_match_len_term_rec :
  forall s re term,
    term (shortest_match_len re s) = shortest_match_len_term_rec re s term.
Proof.
induction s; simpl; auto.
induction re; auto.
intros.
rewrite <- IHs.
induction (nu re); auto.
induction re; auto.
case_eq (shortest_match_len (derive a re) s);intros; auto.
induction re; auto.
induction re; auto.
Qed.

Definition shortest_match_len_terminal_recursive re s := shortest_match_len_term_rec re s id.

Theorem general_equivalence_of_shortest_match_len_terminal_recursive_correct : 
  forall s re,
    shortest_match_len_terminal_recursive re s = shortest_match_len re s.
Proof.
unfold shortest_match_len_terminal_recursive.
intros.
rewrite <- general_equivalence_of_shortest_match_len_and_shortest_match_len_term_rec.
unfold id.
auto.
Qed.

Theorem general_equivalence_of_shortest_match_len_terminal_recursive_correct2 : 
  forall s re,
    shortest_match_len_terminal_recursive re s = ShortestLen.shortest_match_len re s.
Proof.
intros.
rewrite <- shortest_match_len_correct.
apply general_equivalence_of_shortest_match_len_terminal_recursive_correct.
Qed.













