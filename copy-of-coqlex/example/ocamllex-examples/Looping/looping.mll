
rule my_lexer = parse
    |  'b' 'a'* 'b' { 0 }
    |  'a'* { my_lexer lexbuf }
    | eof { 1 }
