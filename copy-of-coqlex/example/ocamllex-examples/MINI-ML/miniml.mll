{
    open Buffer
    
    type t_type = 
        | LET 
        | REC 
        | AND 
        | IN 
        | FUN 
        | If 
        | THEN 
        | ELSE 
        | TRUE 
        | FALSE
        | NOT
        | COMMA
        | EQ 
        | NE    
        | LE    
        | GE    
        | LT    
        | GT    
        | ARROW
        | MINUS
        | PLUS 
        | DIV  
        | MULT 
        | BOOL_AND
        | BOOL_OR
        | LPAR 
        | RPAR 
        | STR of string 
        | Nat of string 
        | Ident of string 
        | EOF
        
    let keywords =
       [ ("let",   LET);
          ("rec",   REC);
          ("and",   AND);
          ("in",   IN);
          ("fun",   FUN);
          ("if",    If);
          ("then",  THEN);
          ("else",  ELSE);
          ("true",  TRUE);
          ("false", FALSE);
          ("not",   NOT)] ;;
}

let newline_regex    = '\n' | '\r' | '\r' '\n'
let blank      = ' ' | '\t'
let lowercase  = ['a'-'z' '_']
let uppercase  = ['A'-'Z' '_']
let ident_char = ['A'-'Z' 'a'-'z' '_' '\'' '0'-'9']
let ident      = lowercase ident_char*
let constr     = uppercase ident_char*
let nat        = ['1'-'9'] ['0'-'9']* | '0'
let escaped    = "\\n" | "\\\"" | "\\?" | "\\\\" | "\\a" | "\\b" | "\\f" | "\\r" | "\\t" | "\\v"
let str_char   = [^ '"' '\\' '\n'] | escaped
let string_regex     = '"' str_char* '"'

rule comment storage = parse
  "(*"         { 
                    storage := ((lexbuf.lex_start_p), (lexbuf.lex_curr_p))::(!storage);
                    comment storage lexbuf; 
                    comment storage lexbuf
               }
| "*)"         { 
                    match !storage with
                    | [] -> failwith "Call of comment without args"
                    | h::t -> storage := t
                }
| newline_regex    { Lexing.new_line lexbuf; comment storage lexbuf }
| _            { comment storage lexbuf }
| eof          { failwith "Opened comment." }

and token_lexer = parse
  newline_regex      { Lexing.new_line lexbuf; token_lexer lexbuf }
| blank+       { token_lexer lexbuf }
| ","          { COMMA }
| "="          { EQ }
| "<>"         { NE }
| "=<"         { LE }
| ">="         { GE }
| "<"          { LT }
| ">"          { GT }
| "->"         { ARROW }
| "-"          { MINUS }
| "+"          { PLUS }
| "/"          { DIV }
| "*"          { MULT }
| "&&"         { BOOL_AND }
| "||"         { BOOL_OR }
| "("          { LPAR }
| ")"          { RPAR }
| string_regex { STR (Lexing.lexeme lexbuf) }
| "(*"         { 
                 let storage = ref [(lexbuf.lex_start_p), (lexbuf.lex_curr_p)] in
                 comment storage lexbuf; token_lexer lexbuf
               }
| nat          { Nat (Lexing.lexeme lexbuf) }
| ident        { 
                    match List.assoc_opt (Lexing.lexeme lexbuf) keywords with
                    | Some t -> t
                    | None -> (Ident (Lexing.lexeme lexbuf))
               }
               
| _       { 
                failwith ("Invalid character " ^ (Lexing.lexeme lexbuf))
          }
| eof          { EOF }
