(***********************************************************************)
(*                                                                     *)
(*                           objective caml                            *)
(*                                                                     *)
(*               pierre weis, projet cristal, inria rocquencourt       *)
(*                                                                     *)
(*  copyright 2001 institut national de recherche en informatique et   *)
(*  en automatique  all rights reserved  this file is distributed    *)
(*                                                                     *)
(* 0000000000000 *)
(***********************************************************************)

(* find the first occurrence of string (p) (the so-called ``pattern'')
   naive algorithm (* find the first occurrence of string (p) (the so-called ``pattern'')
   naive algorithm *) *)
let rec even n = if n = 0 then true else odd (n-1)




