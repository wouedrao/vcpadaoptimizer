(***********************************************************************)
(*                                                                     *)
(*                           objective caml                            *)
(*                                                                     *)
(*               pierre weis, projet cristal, inria rocquencourt       *)
(*                                                                     *)
(*  copyright 2001 institut national de recherche en informatique et   *)
(*  en automatique  all rights reserved  this file is distributed    *)
(*  only by permission                                                *)
(*                                                                     *)
(***********************************************************************)

(* find the first occurrence of string (p) (the so-called ``pattern'')
   into the string (s)
   naive algorithm (* find the first occurrence of string (p) (the so-called ``pattern'')
   into the string (s)
   naive algorithm *) *)
let rec even n = if n = 0 then true else odd (n-1)
and odd = fun n -> if n = 0 then false else even (n-1)
in even 13

let x = 4 in x + 1

let b =  1 < 2 || 1 = 2 in
let c = false || 1 < 2+3*4 && ((fun x -> x) (0 > 1))
in c

let mult x y = x * y in
  let rec fact n = if n = 0 then 1 else mult n (fact (n-1))
  in (fun x -> x + 1) (fact 5)
  
let foo, mult =
  (fun (x,(y,z)) -> x + y * z), (fun (x,y) -> x * y)
in foo (1, (mult (2,3), 4))

let x, y = 1, 2 in x + y

let rec even n = if n = 0 then true else odd (n-1)
and odd = fun n -> if n = 0 then false else even (n-1)
in even 13

let x = 4 in x + 1

let b =  1 < 2 || 1 = 2 in
let c = false || 1 < 2+3*4 && ((fun x -> x) (0 > 1))
in c

let mult x y = x * y in
  let rec fact n = if n = 0 then 1 else mult n (fact (n-1))
  in (fun x -> x + 1) (fact 5)
  
let foo, mult =
  (fun (x,(y,z)) -> x + y * z), (fun (x,y) -> x * y)
in foo (1, (mult (2,3), 4))

let x, y = 1, 2 in x + y

let rec even n = if n = 0 then true else odd (n-1)
and odd = fun n -> if n = 0 then false else even (n-1)
in even 13

let x = 4 in x + 1

let b =  1 < 2 || 1 = 2 in
let c = false || 1 < 2+3*4 && ((fun x -> x) (0 > 1))
in c

let mult x y = x * y in
  let rec fact n = if n = 0 then 1 else mult n (fact (n-1))
  in (fun x -> x + 1) (fact 5)
  
let foo, mult =
  (fun (x,(y,z)) -> x + y * z), (fun (x,y) -> x * y)
in foo (1, (mult (2,3), 4))

let x, y = 1, 2 in x + y

let rec even n = if n = 0 then true else odd (n-1)
and odd = fun n -> if n = 0 then false else even (n-1)
in even 13

let x = 4 in x + 1

let b =  1 < 2 || 1 = 2 in
let c = false || 1 < 2+3*4 && ((fun x -> x) (0 > 1))
in c

let mult x y = x * y in
  let rec fact n = if n = 0 then 1 else mult n (fact (n-1))
  in (fun x -> x + 1) (fact 5)
  
let foo, mult =
  (fun (x,(y,z)) -> x + y * z), (fun (x,y) -> x * y)
in foo (1, (mult (2,3), 4))

let x, y = 1, 2 in x + y
(***********************************************************************)
(*                                                                     *)
(*                           objective caml                            *)
(*                                                                     *)
(*               pierre weis, projet cristal, inria rocquencourt       *)
(*                                                                     *)
(*  copyright 2001 institut national de recherche en informatique et   *)
(*  en automatique  all rights reserved  this file is distributed    *)
(*  only by permission                                                *)
(*                                                                     *)
(***********************************************************************)

(* find the first occurrence of string (p) (the so-called ``pattern'')
   into the string (s)
   naive algorithm (* find the first occurrence of string (p) (the so-called ``pattern'')
   into the string (s)
   naive algorithm *) *)
let rec even n = if n = 0 then true else odd (n-1)
and odd = fun n -> if n = 0 then false else even (n-1)
in even 13

let x = 4 in x + 1

let b =  1 < 2 || 1 = 2 in
let c = false || 1 < 2+3*4 && ((fun x -> x) (0 > 1))
in c

let mult x y = x * y in
  let rec fact n = if n = 0 then 1 else mult n (fact (n-1))
  in (fun x -> x + 1) (fact 5)
  
let foo, mult =
  (fun (x,(y,z)) -> x + y * z), (fun (x,y) -> x * y)
in foo (1, (mult (2,3), 4))

let x, y = 1, 2 in x + y

let rec even n = if n = 0 then true else odd (n-1)
and odd = fun n -> if n = 0 then false else even (n-1)
in even 13

let x = 4 in x + 1

let b =  1 < 2 || 1 = 2 in
let c = false || 1 < 2+3*4 && ((fun x -> x) (0 > 1))
in c

let mult x y = x * y in
  let rec fact n = if n = 0 then 1 else mult n (fact (n-1))
  in (fun x -> x + 1) (fact 5)
  
let foo, mult =
  (fun (x,(y,z)) -> x + y * z), (fun (x,y) -> x * y)
in foo (1, (mult (2,3), 4))

let x, y = 1, 2 in x + y

let rec even n = if n = 0 then true else odd (n-1)
and odd = fun n -> if n = 0 then false else even (n-1)
in even 13

let x = 4 in x + 1

let b =  1 < 2 || 1 = 2 in
let c = false || 1 < 2+3*4 && ((fun x -> x) (0 > 1))
in c

let mult x y = x * y in
  let rec fact n = if n = 0 then 1 else mult n (fact (n-1))
  in (fun x -> x + 1) (fact 5)
  
let foo, mult =
  (fun (x,(y,z)) -> x + y * z), (fun (x,y) -> x * y)
in foo (1, (mult (2,3), 4))

let x, y = 1, 2 in x + y

let rec even n = if n = 0 then true else odd (n-1)
and odd = fun n -> if n = 0 then false else even (n-1)
in even 13

let x = 4 in x + 1

let b =  1 < 2 || 1 = 2 in
let c = false || 1 < 2+3*4 && ((fun x -> x) (0 > 1))
in c

let mult x y = x * y in
  let rec fact n = if n = 0 then 1 else mult n (fact (n-1))
  in (fun x -> x + 1) (fact 5)
  
let foo, mult =
  (fun (x,(y,z)) -> x + y * z), (fun (x,y) -> x * y)
in foo (1, (mult (2,3), 4))

let x, y = 1, 2 in x + y
(***********************************************************************)
(*                                                                     *)
(*                           objective caml                            *)
(*                                                                     *)
(*               pierre weis, projet cristal, inria rocquencourt       *)
(*                                                                     *)
(*  copyright 2001 institut national de recherche en informatique et   *)
(*  en automatique  all rights reserved  this file is distributed    *)
(*  only by permission                                                *)
(*                                                                     *)
(***********************************************************************)

(* find the first occurrence of string (p) (the so-called ``pattern'')
   into the string (s)
   naive algorithm (* find the first occurrence of string (p) (the so-called ``pattern'')
   into the string (s)
   naive algorithm *) *)
let rec even n = if n = 0 then true else odd (n-1)
and odd = fun n -> if n = 0 then false else even (n-1)
in even 13

let x = 4 in x + 1

let b =  1 < 2 || 1 = 2 in
let c = false || 1 < 2+3*4 && ((fun x -> x) (0 > 1))
in c

let mult x y = x * y in
  let rec fact n = if n = 0 then 1 else mult n (fact (n-1))
  in (fun x -> x + 1) (fact 5)
  
let foo, mult =
  (fun (x,(y,z)) -> x + y * z), (fun (x,y) -> x * y)
in foo (1, (mult (2,3), 4))

let x, y = 1, 2 in x + y

let rec even n = if n = 0 then true else odd (n-1)
and odd = fun n -> if n = 0 then false else even (n-1)
in even 13

let x = 4 in x + 1

let b =  1 < 2 || 1 = 2 in
let c = false || 1 < 2+3*4 && ((fun x -> x) (0 > 1))
in c

let mult x y = x * y in
  let rec fact n = if n = 0 then 1 else mult n (fact (n-1))
  in (fun x -> x + 1) (fact 5)
  
let foo, mult =
  (fun (x,(y,z)) -> x + y * z), (fun (x,y) -> x * y)
in foo (1, (mult (2,3), 4))

let x, y = 1, 2 in x + y

let rec even n = if n = 0 then true else odd (n-1)
and odd = fun n -> if n = 0 then false else even (n-1)
in even 13

let x = 4 in x + 1

let b =  1 < 2 || 1 = 2 in
let c = false || 1 < 2+3*4 && ((fun x -> x) (0 > 1))
in c

let mult x y = x * y in
  let rec fact n = if n = 0 then 1 else mult n (fact (n-1))
  in (fun x -> x + 1) (fact 5)
  
let foo, mult =
  (fun (x,(y,z)) -> x + y * z), (fun (x,y) -> x * y)
in foo (1, (mult (2,3), 4))

let x, y = 1, 2 in x + y

let rec even n = if n = 0 then true else odd (n-1)
and odd = fun n -> if n = 0 then false else even (n-1)
in even 13

let x = 4 in x + 1

let b =  1 < 2 || 1 = 2 in
let c = false || 1 < 2+3*4 && ((fun x -> x) (0 > 1))
in c

let mult x y = x * y in
  let rec fact n = if n = 0 then 1 else mult n (fact (n-1))
  in (fun x -> x + 1) (fact 5)
  
let foo, mult =
  (fun (x,(y,z)) -> x + y * z), (fun (x,y) -> x * y)
in foo (1, (mult (2,3), 4))

let x, y = 1, 2 in x + y
(***********************************************************************)
(*                                                                     *)
(*                           objective caml                            *)
(*                                                                     *)
(*               pierre weis, projet cristal, inria rocquencourt       *)
(*                                                                     *)
(*  copyright 2001 institut national de recherche en informatique et   *)
(*  en automatique  all rights reserved  this file is distributed    *)
(*  only by permission                                                *)
(*                                                                     *)
(***********************************************************************)

(* find the first occurrence of string (p) (the so-called ``pattern'')
   into the string (s)
   naive algorithm (* find the first occurrence of string (p) (the so-called ``pattern'')
   into the string (s)
   naive algorithm *) *)
let rec even n = if n = 0 then true else odd (n-1)
and odd = fun n -> if n = 0 then false else even (n-1)
in even 13

let x = 4 in x + 1

let b =  1 < 2 || 1 = 2 in
let c = false || 1 < 2+3*4 && ((fun x -> x) (0 > 1))
in c

let mult x y = x * y in
  let rec fact n = if n = 0 then 1 else mult n (fact (n-1))
  in (fun x -> x + 1) (fact 5)
  
let foo, mult =
  (fun (x,(y,z)) -> x + y * z), (fun (x,y) -> x * y)
in foo (1, (mult (2,3), 4))

let x, y = 1, 2 in x + y

let rec even n = if n = 0 then true else odd (n-1)
and odd = fun n -> if n = 0 then false else even (n-1)
in even 13

let x = 4 in x + 1

let b =  1 < 2 || 1 = 2 in
let c = false || 1 < 2+3*4 && ((fun x -> x) (0 > 1))
in c

let mult x y = x * y in
  let rec fact n = if n = 0 then 1 else mult n (fact (n-1))
  in (fun x -> x + 1) (fact 5)
  
let foo, mult =
  (fun (x,(y,z)) -> x + y * z), (fun (x,y) -> x * y)
in foo (1, (mult (2,3), 4))

let x, y = 1, 2 in x + y


(***********************************************************************)
(*                                                                     *)
(*                           objective caml                            *)
(*                                                                     *)
(*               pierre weis, projet cristal, inria rocquencourt       *)
(*                                                                     *)
(*  copyright 2001 institut national de recherche en informatique et   *)
(*  en automatique  all rights reserved  this file is distributed    *)
(*  only by permission                                                *)
(*                                                                     *)
(***********************************************************************)

(* find the first occurrence of string (p) (the so-called ``pattern'')
   into the string (s)
   naive algorithm (* find the first occurrence of string (p) (the so-called ``pattern'')
   into the string (s)
   naive algorithm *) *)
   
let rec even n = if n = 0 then true else odd (n-1)
and odd = fun n -> if n = 0 then false else even (n-1)
in even 13

let rec even n = if n = 0 then true else odd (n-1)
and odd = fun n -> if n = 0 then false else even (n-1)

let x = 4 in x + 1

let b =  1 < 2 || 1 = 2 in
let c = false || 1 < 2+3*4 && ((fun x -> x) (0 > 1))
in c

let mult x y = x * y in
  let rec fact n = if n = 0 then 1 else mult n (fact (n-1))
  in (fun x -> x + 1) (fact 5)
  
let foo, mult =
  (fun (x,(y,z)) -> x + y * z), (fun (x,y) -> x * y)
in foo (1, (mult (2,3), 4))

let x, y = 1, 2 in x + y

(***********************************************************************)
(*                                                                     *)
(*                           objective caml                            *)
(*                                                                     *)
(*               pierre weis, projet cristal, inria rocquencourt       *)
(*                                                                     *)
(*  copyright 2001 institut national de recherche en informatique et   *)
(*  en automatique  all rights reserved  this file is distributed    *)
(*  only by permission                                                *)
(*                                                                     *)
(***********************************************************************)

(* find the first occurrence of string (p) (the so-called ``pattern'')
   into the string (s)
   naive algorithm (* find the first occurrence of string (p) (the so-called ``pattern'')
   into the string (s)
   naive algorithm *) *)

let rec even n = if n = 0 then true else odd (n-1)
and odd = fun n -> if n = 0 then false else even (n-1)
in even 13

let x = 4 in x + 1

let b =  1 < 2 || 1 = 2 in
let c = false || 1 < 2+3*4 && ((fun x -> x) (0 > 1))
in c

let mult x y = x * y in
  let rec fact n = if n = 0 then 1 else mult n (fact (n-1))
  in (fun x -> x + 1) (fact 5)
  
let foo, mult =
  (fun (x,(y,z)) -> x + y * z), (fun (x,y) -> x * y)
in foo (1, (mult (2,3), 4))

let x, y = 1, 2 in x + y

(***********************************************************************)
(*                                                                     *)
(*                           objective caml                            *)
(*                                                                     *)
(*               pierre weis, projet cristal, inria rocquencourt       *)
(*                                                                     *)
(*  copyright 2001 institut national de recherche en informatique et   *)
(*  en automatique  all rights reserved  this file is distributed    *)
(*  only by permission                                                *)
(*                                                                     *)
(***********************************************************************)

(* find the first occurrence of string (p) (the so-called ``pattern'')
   into the string (s)
   naive algorithm (* find the first occurrence of string (p) (the so-called ``pattern'')
   into the string (s)
   naive algorithm *) *)


let rec even n = if n = 0 then true else odd (n-1)
and odd = fun n -> if n = 0 then false else even (n-1)
in even 13


(***********************************************************************)
(*                                                                     *)
(*                           objective caml                            *)
(*                                                                     *)
(*               pierre weis, projet cristal, inria rocquencourt       *)
(*                                                                     *)
(*  copyright 2001 institut national de recherche en informatique et   *)
(*  en automatique  all rights reserved  this file is distributed    *)
(*  only by permission                                                *)
(*                                                                     *)
(***********************************************************************)

(* find the first occurrence of string (p) (the so-called ``pattern'')
   into the string (s)
   naive algorithm (* find the first occurrence of string (p) (the so-called ``pattern'')
   into the string (s)
   naive algorithm *) *)
   
(***********************************************************************)
(*                                                                     *)
(*                           objective caml                            *)
(*                                                                     *)
(*               pierre weis, projet cristal, inria rocquencourt       *)
(*                                                                     *)
(*  copyright 2001 institut national de recherche en informatique et   *)
(*  en automatique  all rights reserved  this file is distributed    *)
(*  only by permission                                                *)
(*                                                                     *)
(***********************************************************************)

(* find the first occurrence of string (p) (the so-called ``pattern'')
   into the string (s)
   naive algorithm (* find the first occurrence of string (p) (the so-called ``pattern'')
   into the string (s)
   naive algorithm *) *)

let x = 4 in x + 1

let b =  1 < 2 || 1 = 2 in
let c = false || 1 < 2+3*4 && ((fun x -> x) (0 > 1))
in c

let mult x y = x * y in
  let rec fact n = if n = 0 then 1 else mult n (fact (n-1))
  in (fun x -> x + 1) (fact 5)
  
let foo, mult =
  (fun (x,(y,z)) -> x + y * z), (fun (x,y) -> x * y)
in foo (1, (mult (2,3), 4))

let x, y = 1, 2 in x + y

let rec even n = if n = 0 then true else odd (n-1)
and odd = fun n -> if n = 0 then false else even (n-1)
in even 13

let x = 4 in x + 1

let b =  1 < 2 || 1 = 2 in
let c = false || 1 < 2+3*4 && ((fun x -> x) (0 > 1))
in c

let mult x y = x * y in
  let rec fact n = if n = 0 then 1 else mult n (fact (n-1))
  in (fun x -> x + 1) (fact 5)
  
let foo, mult =
  (fun (x,(y,z)) -> x + y * z), (fun (x,y) -> x * y)
in foo (1, (mult (2,3), 4))

let x, y = 1, 2 in x + y

let rec even n = if n = 0 then true else odd (n-1)
and odd = fun n -> if n = 0 then false else even (n-1)
in even 13

let x = 4 in x + 1

let b =  1 < 2 || 1 = 2 in
let c = false || 1 < 2+3*4 && ((fun x -> x) (0 > 1))
in c

let mult x y = x * y in
  let rec fact n = if n = 0 then 1 else mult n (fact (n-1))
  in (fun x -> x + 1) (fact 5)
  
let foo, mult =
  (fun (x,(y,z)) -> x + y * z), (fun (x,y) -> x * y)
in foo (1, (mult (2,3), 4))

let x, y = 1, 2 in x + y

let rec even n = if n = 0 then true else odd (n-1)
and odd = fun n -> if n = 0 then false else even (n-1)
in even 13

let x = 4 in x + 1

let b =  1 < 2 || 1 = 2 in
let c = false || 1 < 2+3*4 && ((fun x -> x) (0 > 1))
in c

let mult x y = x * y in
  let rec fact n = if n = 0 then 1 else mult n (fact (n-1))
  in (fun x -> x + 1) (fact 5)
  
let foo, mult =
  (fun (x,(y,z)) -> x + y * z), (fun (x,y) -> x * y)
in foo (1, (mult (2,3), 4))

let x, y = 1, 2 in x + y
(***********************************************************************)
(*                                                                     *)
(*                           objective caml                            *)
(*                                                                     *)
(*               pierre weis, projet cristal, inria rocquencourt       *)
(*                                                                     *)
(*  copyright 2001 institut national de recherche en informatique et   *)
(*  en automatique  all rights reserved  this file is distributed    *)
(*  only by permission                                                *)
(*                                                                     *)
(***********************************************************************)

(* find the first occurrence of string (p) (the so-called ``pattern'')
   into the string (s)
   naive algorithm (* find the first occurrence of string (p) (the so-called ``pattern'')
   into the string (s)
   naive algorithm *) *)
let rec even n = if n = 0 then true else odd (n-1)
and odd = fun n -> if n = 0 then false else even (n-1)
in even 13

let x = 4 in x + 1

let b =  1 < 2 || 1 = 2 in
let c = false || 1 < 2+3*4 && ((fun x -> x) (0 > 1))
in c

let mult x y = x * y in
  let rec fact n = if n = 0 then 1 else mult n (fact (n-1))
  in (fun x -> x + 1) (fact 5)
  
let foo, mult =
  (fun (x,(y,z)) -> x + y * z), (fun (x,y) -> x * y)
in foo (1, (mult (2,3), 4))

let x, y = 1, 2 in x + y

let rec even n = if n = 0 then true else odd (n-1)
and odd = fun n -> if n = 0 then false else even (n-1)
in even 13

let x = 4 in x + 1

let b =  1 < 2 || 1 = 2 in
let c = false || 1 < 2+3*4 && ((fun x -> x) (0 > 1))
in c

let mult x y = x * y in
  let rec fact n = if n = 0 then 1 else mult n (fact (n-1))
  in (fun x -> x + 1) (fact 5)
  
let foo, mult =
  (fun (x,(y,z)) -> x + y * z), (fun (x,y) -> x * y)
in foo (1, (mult (2,3), 4))

let x, y = 1, 2 in x + y

let rec even n = if n = 0 then true else odd (n-1)
and odd = fun n -> if n = 0 then false else even (n-1)
in even 13

let x = 4 in x + 1

let b =  1 < 2 || 1 = 2 in
let c = false || 1 < 2+3*4 && ((fun x -> x) (0 > 1))
in c

let mult x y = x * y in
  let rec fact n = if n = 0 then 1 else mult n (fact (n-1))
  in (fun x -> x + 1) (fact 5)
  
let foo, mult =
  (fun (x,(y,z)) -> x + y * z), (fun (x,y) -> x * y)
in foo (1, (mult (2,3), 4))

let x, y = 1, 2 in x + y

let rec even n = if n = 0 then true else odd (n-1)
and odd = fun n -> if n = 0 then false else even (n-1)
in even 13

let x = 4 in x + 1

let b =  1 < 2 || 1 = 2 in
let c = false || 1 < 2+3*4 && ((fun x -> x) (0 > 1))
in c

let mult x y = x * y in
  let rec fact n = if n = 0 then 1 else mult n (fact (n-1))
  in (fun x -> x + 1) (fact 5)
  
let foo, mult =
  (fun (x,(y,z)) -> x + y * z), (fun (x,y) -> x * y)
in foo (1, (mult (2,3), 4))

let x, y = 1, 2 in x + y
(***********************************************************************)
(*                                                                     *)
(*                           objective caml                            *)
(*                                                                     *)
(*               pierre weis, projet cristal, inria rocquencourt       *)
(*                                                                     *)
(*  copyright 2001 institut national de recherche en informatique et   *)
(*  en automatique  all rights reserved  this file is distributed    *)
(*  only by permission                                                *)
(*                                                                     *)
(***********************************************************************)

(* find the first occurrence of string (p) (the so-called ``pattern'')
   into the string (s)
   naive algorithm (* find the first occurrence of string (p) (the so-called ``pattern'')
   into the string (s)
   naive algorithm *) *)
let rec even n = if n = 0 then true else odd (n-1)
and odd = fun n -> if n = 0 then false else even (n-1)
in even 13

let x = 4 in x + 1

let b =  1 < 2 || 1 = 2 in
let c = false || 1 < 2+3*4 && ((fun x -> x) (0 > 1))
in c

let mult x y = x * y in
  let rec fact n = if n = 0 then 1 else mult n (fact (n-1))
  in (fun x -> x + 1) (fact 5)
  
let foo, mult =
  (fun (x,(y,z)) -> x + y * z), (fun (x,y) -> x * y)
in foo (1, (mult (2,3), 4))

let x, y = 1, 2 in x + y

let rec even n = if n = 0 then true else odd (n-1)
and odd = fun n -> if n = 0 then false else even (n-1)
in even 13

let x = 4 in x + 1

let b =  1 < 2 || 1 = 2 in
let c = false || 1 < 2+3*4 && ((fun x -> x) (0 > 1))
in c

let mult x y = x * y in
  let rec fact n = if n = 0 then 1 else mult n (fact (n-1))
  in (fun x -> x + 1) (fact 5)
  
let foo, mult =
  (fun (x,(y,z)) -> x + y * z), (fun (x,y) -> x * y)
in foo (1, (mult (2,3), 4))

let x, y = 1, 2 in x + y

let rec even n = if n = 0 then true else odd (n-1)
and odd = fun n -> if n = 0 then false else even (n-1)
in even 13

let x = 4 in x + 1

let b =  1 < 2 || 1 = 2 in
let c = false || 1 < 2+3*4 && ((fun x -> x) (0 > 1))
in c

let mult x y = x * y in
  let rec fact n = if n = 0 then 1 else mult n (fact (n-1))
  in (fun x -> x + 1) (fact 5)
  
let foo, mult =
  (fun (x,(y,z)) -> x + y * z), (fun (x,y) -> x * y)
in foo (1, (mult (2,3), 4))

let x, y = 1, 2 in x + y

let rec even n = if n = 0 then true else odd (n-1)
and odd = fun n -> if n = 0 then false else even (n-1)
in even 13

let x = 4 in x + 1

let b =  1 < 2 || 1 = 2 in
let c = false || 1 < 2+3*4 && ((fun x -> x) (0 > 1))
in c

let mult x y = x * y in
  let rec fact n = if n = 0 then 1 else mult n (fact (n-1))
  in (fun x -> x + 1) (fact 5)
  
let foo, mult =
  (fun (x,(y,z)) -> x + y * z), (fun (x,y) -> x * y)
in foo (1, (mult (2,3), 4))

let x, y = 1, 2 in x + y
(***********************************************************************)
(*                                                                     *)
(*                           objective caml                            *)
(*                                                                     *)
(*               pierre weis, projet cristal, inria rocquencourt       *)
(*                                                                     *)
(*  copyright 2001 institut national de recherche en informatique et   *)
(*  en automatique  all rights reserved  this file is distributed    *)
(*  only by permission                                                *)
(*                                                                     *)
(***********************************************************************)

(* find the first occurrence of string (p) (the so-called ``pattern'')
   into the string (s)
   naive algorithm (* find the first occurrence of string (p) (the so-called ``pattern'')
   into the string (s)
   naive algorithm *) *)
let rec even n = if n = 0 then true else odd (n-1)
and odd = fun n -> if n = 0 then false else even (n-1)
in even 13

let x = 4 in x + 1

let b =  1 < 2 || 1 = 2 in
let c = false || 1 < 2+3*4 && ((fun x -> x) (0 > 1))
in c

let mult x y = x * y in
  let rec fact n = if n = 0 then 1 else mult n (fact (n-1))
  in (fun x -> x + 1) (fact 5)
  
let foo, mult =
  (fun (x,(y,z)) -> x + y * z), (fun (x,y) -> x * y)
in foo (1, (mult (2,3), 4))

let x, y = 1, 2 in x + y

let rec even n = if n = 0 then true else odd (n-1)
and odd = fun n -> if n = 0 then false else even (n-1)
in even 13

let x = 4 in x + 1

let b =  1 < 2 || 1 = 2 in
let c = false || 1 < 2+3*4 && ((fun x -> x) (0 > 1))
in c

let mult x y = x * y in
  let rec fact n = if n = 0 then 1 else mult n (fact (n-1))
  in (fun x -> x + 1) (fact 5)
  
let foo, mult =
  (fun (x,(y,z)) -> x + y * z), (fun (x,y) -> x * y)
in foo (1, (mult (2,3), 4))

let x, y = 1, 2 in x + y


(***********************************************************************)
(*                                                                     *)
(*                           objective caml                            *)
(*                                                                     *)
(*               pierre weis, projet cristal, inria rocquencourt       *)
(*                                                                     *)
(*  copyright 2001 institut national de recherche en informatique et   *)
(*  en automatique  all rights reserved  this file is distributed    *)
(*  only by permission                                                *)
(*                                                                     *)
(***********************************************************************)

(* find the first occurrence of string (p) (the so-called ``pattern'')
   into the string (s)
   naive algorithm (* find the first occurrence of string (p) (the so-called ``pattern'')
   into the string (s)
   naive algorithm *) *)
   
let rec even n = if n = 0 then true else odd (n-1)
and odd = fun n -> if n = 0 then false else even (n-1)
in even 13

let rec even n = if n = 0 then true else odd (n-1)
and odd = fun n -> if n = 0 then false else even (n-1)

let x = 4 in x + 1

let b =  1 < 2 || 1 = 2 in
let c = false || 1 < 2+3*4 && ((fun x -> x) (0 > 1))
in c

let mult x y = x * y in
  let rec fact n = if n = 0 then 1 else mult n (fact (n-1))
  in (fun x -> x + 1) (fact 5)
  
let foo, mult =
  (fun (x,(y,z)) -> x + y * z), (fun (x,y) -> x * y)
in foo (1, (mult (2,3), 4))

let x, y = 1, 2 in x + y

(***********************************************************************)
(*                                                                     *)
(*                           objective caml                            *)
(*                                                                     *)
(*               pierre weis, projet cristal, inria rocquencourt       *)
(*                                                                     *)
(*  copyright 2001 institut national de recherche en informatique et   *)
(*  en automatique  all rights reserved  this file is distributed    *)
(*  only by permission                                                *)
(*                                                                     *)
(***********************************************************************)

(* find the first occurrence of string (p) (the so-called ``pattern'')
   into the string (s)
   naive algorithm (* find the first occurrence of string (p) (the so-called ``pattern'')
   into the string (s)
   naive algorithm *) *)

let rec even n = if n = 0 then true else odd (n-1)
and odd = fun n -> if n = 0 then false else even (n-1)
in even 13

let x = 4 in x + 1

let b =  1 < 2 || 1 = 2 in
let c = false || 1 < 2+3*4 && ((fun x -> x) (0 > 1))
in c

let mult x y = x * y in
  let rec fact n = if n = 0 then 1 else mult n (fact (n-1))
  in (fun x -> x + 1) (fact 5)
  
let foo, mult =
  (fun (x,(y,z)) -> x + y * z), (fun (x,y) -> x * y)
in foo (1, (mult (2,3), 4))

let x, y = 1, 2 in x + y

(***********************************************************************)
(*                                                                     *)
(*                           objective caml                            *)
(*                                                                     *)
(*               pierre weis, projet cristal, inria rocquencourt       *)
(*                                                                     *)
(*  copyright 2001 institut national de recherche en informatique et   *)
(*  en automatique  all rights reserved  this file is distributed    *)
(*  only by permission                                                *)
(*                                                                     *)
(***********************************************************************)

(* find the first occurrence of string (p) (the so-called ``pattern'')
   into the string (s)
   naive algorithm (* find the first occurrence of string (p) (the so-called ``pattern'')
   into the string (s)
   naive algorithm *) *)


let rec even n = if n = 0 then true else odd (n-1)
and odd = fun n -> if n = 0 then false else even (n-1)
in even 13


(***********************************************************************)
(*                                                                     *)
(*                           objective caml                            *)
(*                                                                     *)
(*               pierre weis, projet cristal, inria rocquencourt       *)
(*                                                                     *)
(*  copyright 2001 institut national de recherche en informatique et   *)
(*  en automatique  all rights reserved  this file is distributed    *)
(*  only by permission                                                *)
(*                                                                     *)
(***********************************************************************)

(* find the first occurrence of string (p) (the so-called ``pattern'')
   into the string (s)
   naive algorithm (* find the first occurrence of string (p) (the so-called ``pattern'')
   into the string (s)
   naive algorithm *) *)
