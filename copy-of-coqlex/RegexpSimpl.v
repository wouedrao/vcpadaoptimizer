(* Add LoadPath "regexp_opt" as RegExp.  *)

Require Import RegExp.Definitions.
Require Import RegExp.Boolean.
Require Import RegExp.Char.
Require Import RegExp.Concat.
Require Import RegExp.Star.

Definition simp_cat r s :=
match r with
| Empty => Empty
| Eps => s
| x => (match s with
       | Empty => Empty
       | Eps => x
       | x0 => Cat x x0
       end)
end.

Lemma simp_cat_correct : forall l r, simp_cat l r =R= Cat l r.
Proof.
induction l; induction r; simpl;
  try first [rewrite Cat_left_zero | rewrite Cat_left_id | rewrite Cat_right_zero | rewrite Cat_right_id ];
  apply re_eq_refl.
Qed.

Definition simp_or r s :=
match r with
| Empty => s
| x => (match s with
       | Empty => x
       | x0 => Or x x0
       end)
end.

Lemma simp_or_correct : forall l r, simp_or l r =R= Or l r.
Proof.
induction l; simpl;
  induction r; simpl; try apply re_eq_refl;
  first [ rewrite Or_left_id | rewrite Or_right_id ];
  apply re_eq_refl.
Qed.

Definition simp_star r :=
match r with
 | Empty => Eps
 | Eps => Eps
 | x => Star x
end.

Lemma simp_star_correct : forall r, simp_star r =R= Star r.
Proof.
{ induction r; simpl; try apply re_eq_refl.
- rewrite matches_Star_right.
  rewrite Cat_left_zero.
  rewrite Or_right_id.
  apply re_eq_refl.
- unfold re_eq.
  induction s; simpl; auto.
  rewrite Cat_left_zero.
  apply re_eq_refl.
}
Qed.

Definition simp_and r s :=
match r with
| Empty => Empty
| x => (match s with
       | Empty => Empty
       | x0 => And x x0
       end)
end.

Lemma simp_and_correct : forall l r, simp_and l r =R= And l r.
Proof.
induction l; simpl.
unfold re_eq.
intros.
generalize dependent r.
generalize dependent s.
induction s; simpl; auto.
induction r; intros; try apply re_eq_refl.
unfold re_eq.
induction s; simpl; auto.
induction s; simpl; auto.
induction r; intros; try apply re_eq_refl.
unfold re_eq.
intros.
rewrite matches_And.
rewrite Empty_false.
induction (Char a ~= s); auto.
induction r; intros; try apply re_eq_refl.
unfold re_eq.
intros.
rewrite matches_And.
rewrite Empty_false.
induction (Cat l1 l2 ~= s); auto.
induction r; intros; try apply re_eq_refl.
unfold re_eq.
intros.
rewrite matches_And.
rewrite Empty_false.
induction (Or l1 l2 ~= s); auto.
induction r; intros; try apply re_eq_refl.
unfold re_eq.
intros.
rewrite matches_And.
rewrite Empty_false.
induction (Star l ~= s); auto.
induction r; intros; try apply re_eq_refl.
unfold re_eq.
intros.
rewrite matches_And.
rewrite Empty_false.
induction (And l1 l2 ~= s); auto.
induction r; intros; try apply re_eq_refl.
unfold re_eq.
intros.
rewrite matches_And.
rewrite Empty_false.
induction (AnyChar ~= s); auto.
induction r; intros; try apply re_eq_refl.
unfold re_eq.
intros.
rewrite matches_And.
rewrite Empty_false.
induction (CharRange a a0 ~= s); auto.
induction r; intros; try apply re_eq_refl.
unfold re_eq.
intros.
rewrite matches_And.
rewrite Empty_false.
induction (CharRangeExcept a a0 ~= s); auto.
induction r; intros; try apply re_eq_refl.
unfold re_eq.
intros.
rewrite matches_And.
rewrite Empty_false.
induction (Minus l1 l2 ~= s); auto.
Qed.

Definition simp_minus r s :=
match r with
| Empty => Empty
| x => (match s with
       | Empty => r
       | x0 => Minus x x0
       end)
end.

Lemma simp_minus_zero_left: forall l, Minus Empty l =R= Empty.
Proof.
unfold re_eq.
intros.
rewrite matches_Minus.
rewrite Empty_false.
simpl; auto.
Qed.

Lemma simp_minus_zero_right: forall l, Minus l Empty =R= l.
Proof.
unfold re_eq.
intros.
rewrite matches_Minus.
rewrite Empty_false.
simpl.
induction (l ~= s); simpl; auto.
Qed.

Lemma simp_minus_correct : forall l r, simp_minus l r =R= Minus l r.
Proof.
intros.
induction l; induction r; try rewrite simp_minus_zero_left; try rewrite simp_minus_zero_right; simpl; apply re_eq_refl.
Qed.


Fixpoint derive a (re:RegExp):RegExp :=
match re with
| Char c => match (ascii_dec c a) with
 | left _ => Eps
 | right _ => Empty
 end
| Cat r s =>
  if nu r
  then simp_or (simp_cat (derive a r) s) (derive a s)
  else simp_cat (derive a r) s
| Or r s => simp_or (derive a r) (derive a s)
| Star r => simp_cat (derive a r) (simp_star r)
| And r s => simp_and (derive a r) (derive a s)
| AnyChar => Eps
| CharRange l h => if ((l <=? a)%char && (a <=? h)%char)%bool then Eps else Empty
| CharRangeExcept l h => if ((l <=? a)%char && (a <=? h)%char)%bool then Empty else Eps
| Minus r s => simp_minus (derive a r) (derive a s)
| _ => Empty
end.

Theorem derive_simpl_correct : forall a r, derive a r =R= RegExp.Definitions.derive a r.
Proof.
{ induction r; simpl; try apply re_eq_refl.
- case_eq (nu r1); intros.
  rewrite simp_or_correct.
  rewrite simp_cat_correct.
  rewrite IHr1.
  rewrite IHr2.
  apply re_eq_refl.
  rewrite simp_cat_correct.
  rewrite IHr1.
  apply re_eq_refl.
- rewrite simp_or_correct.
  rewrite IHr1.
  rewrite IHr2.
  apply re_eq_refl.
- rewrite simp_cat_correct.
  rewrite simp_star_correct.
  rewrite IHr.
  apply re_eq_refl.
- rewrite simp_and_correct.
  rewrite IHr1.
  rewrite IHr2.
  apply re_eq_refl.
- rewrite simp_minus_correct.
  rewrite IHr1.
  rewrite IHr2.
  apply re_eq_refl.
}
Qed.





