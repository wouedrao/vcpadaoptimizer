(* *********************************************************************)
(*                                                                     *)
(*                 Coqlex verified lexer generator                     *)
(*                                                                     *)
(*  Copyright 2021 Siemens Mobility SAS and Institut National de       *)
(*  Recherche en Informatique et en Automatique.                       *)
(*  All rights reserved. This file is distributed under                *)
(*  the terms of the INRIA Non-Commercial License Agreement (see the   *)
(*  LICENSE file).                                                     *)
(*                                                                     *)
(* *********************************************************************)

open Lexing
open LexerUtils
open Parser
open LexerDefinition

let report (lexb) =
  let b = lexeme_start_p lexb in
  let e = lexeme_end_p lexb in
  let l = b.pos_lnum in
  let fc = b.pos_bol in
  let lc = e.pos_bol in
  Format.eprintf "File \"%s\", line %d, characters %d-%d : Error near \"%s\"@." e.pos_fname l fc lc (lexeme lexb) (*String.sub (Bytes.sub_string lexb.lex_buffer lexb.lex_abs_pos (lexb.lex_buffer_len - lexb.lex_abs_pos)) 0 10*)
  
let coq_report coq_lexbuf =
  Format.eprintf "Line %d, characters %d-%d : Error near \"%s\"@." coq_lexbuf.cur_pos.l_number coq_lexbuf.s_pos.c_number coq_lexbuf.cur_pos.c_number (string_of_char_list coq_lexbuf.str_val) (*String.sub (Bytes.sub_string lexb.lex_buffer lexb.lex_abs_pos (lexb.lex_buffer_len - lexb.lex_abs_pos)) 0 10*)


module GParser (M : sig type t type storage val default_fuel : int val coq_lexer : int -> coq_Lexbuf -> storage -> Parser.Aut.Gram.token coq_LexicalAnalysisResult * storage val menhirParser : int -> MenhirLibParser.Inter.buffer -> t MenhirLibParser.Inter.parse_result end) = struct
	let ocamllexer = LexerUtils.lexbuf_Adapter M.default_fuel M.coq_lexer ;;
	
	let coqlexer = LexerUtils.coqlexbuf_Adapter M.default_fuel M.coq_lexer ;;
	
	let _local_coq_lexbuf_ref = ref None
	
	let tokens_stream_from_ml_lexbuf lexbuf storage : MenhirLibParser.Inter.buffer =
		let rec compute_token_stream () =
			let loop token =
				MenhirLibParser.Inter.Buf_cons (token, Lazy.from_fun compute_token_stream)
			in loop (ocamllexer lexbuf storage)
		in
		Lazy.from_fun compute_token_stream
	
	let tokens_stream_from_coq_lexbuf coq_lexbuf storage : MenhirLibParser.Inter.buffer =
		let rec compute_token_stream coq_lexbuf storage () =
			let loop tokenXlexbufXstorage =
				let token, lexbuf, storage = tokenXlexbufXstorage in
				let _ = _local_coq_lexbuf_ref := Some lexbuf in
				MenhirLibParser.Inter.Buf_cons (token, Lazy.from_fun (compute_token_stream lexbuf storage))
			in loop (coqlexer coq_lexbuf storage)
		in
		Lazy.from_fun (compute_token_stream coq_lexbuf storage)
	
	let ocamlparser_from_mllexbuf lexbuf storage = 
		(match M.menhirParser 50 (tokens_stream_from_ml_lexbuf lexbuf storage) with
		| Fail_pr_full _ ->
		        report (lexbuf); failwith "failed!"
		| Timeout_pr ->
		        report (lexbuf); failwith "timed out!"
		| Parsed_pr (output, _) ->
		        output) ;;
	
	let coqparser_from_coqlexbuf lexbuf storage = 
		(match M.menhirParser 50 (tokens_stream_from_coq_lexbuf lexbuf storage) with
		| Fail_pr_full _ ->
		        (match !_local_coq_lexbuf_ref with
		        | None -> failwith "failed!"
		        | Some lb -> ((coq_report lb); failwith "failed!")
		        )
		| Timeout_pr ->
		        failwith "timed out!"
		| Parsed_pr (output, _) ->
		        output) ;;

	let parse_from_mllexbuf = ocamlparser_from_mllexbuf ;;
	let parse_from_coqlexbuf = coqparser_from_coqlexbuf ;;
end;;

