Add LoadPath "regexp_opt" as RegExp.

Require Import RegExp.Definitions.
Require Import Lia.
Require Import MatchLenSimpl.
Require Import LexerDefinition.
Require Import Coq.Lists.List String.
Import ListNotations.

Definition new_line {Token Storage: Set} : semantic_action (Token:=Token) (Storage:=Storage) :=
  (fun lexbuf (storage: Storage) => 
    (AnalysisTerminated None 
      (mkLexbuf 
        (s_pos lexbuf) 
        (str_val lexbuf) 
        (mkPosition
          (S (l_number (cur_pos lexbuf)))
          0
          (abs_pos (cur_pos lexbuf))
        )
        (remaining_str lexbuf)
      ), storage)).

Definition ret {Token Storage: Set} (tok: Token) : semantic_action (Token:=Token) (Storage:=Storage) := 
  (fun lexbuf (storage: Storage) => (AnalysisTerminated (Some tok) lexbuf, storage)).

Definition ret_l {Token Storage: Set} (c: string -> Token)  : semantic_action (Token:=Token) (Storage:=Storage) := 
  (fun lexbuf (storage: Storage) => (AnalysisTerminated (Some (c lexbuf.(str_val))) lexbuf, storage)).
  
Definition ret_s {Token Storage: Set} (c: Storage -> Token)  : semantic_action (Token:=Token) (Storage:=Storage) := 
  (fun lexbuf (storage: Storage) => (AnalysisTerminated (Some (c storage)) lexbuf, storage)).

Definition ret_f {Token Storage: Set} f : semantic_action (Token:=Token) (Storage:=Storage) := 
  (fun lexbuf (storage: Storage) => (AnalysisTerminated (Some (f lexbuf storage)) lexbuf, storage)).
  
Definition ignore {Token Token' Storage: Set} (lex : semantic_action (Token:=Token) (Storage:=Storage)) : semantic_action (Token:=Token') (Storage:=Storage) :=
  (fun lexbuf (storage: Storage) => (
    match lex lexbuf storage with
    | (AnalysisTerminated _ lexbuf', storage') => (AnalysisTerminated None lexbuf', storage')
    | (AnalysisFailedUserRaisedError msg lexbuf', storage') => (AnalysisFailedUserRaisedError msg lexbuf', storage')
    | (AnalysisFailedEmptyToken lexbuf', storage') => (AnalysisFailedEmptyToken lexbuf', storage')
    | (AnalysisNoFuel lexbuf', storage') => (AnalysisNoFuel lexbuf', storage')
    end )).

Definition raise {Token Storage: Set} msg : semantic_action (Token:=Token) (Storage:=Storage) :=
  (fun lexbuf (storage: Storage) => (AnalysisFailedUserRaisedError msg lexbuf, storage)).
  
Definition raise_l {Token Storage: Set} (c: string -> string) : semantic_action (Token:=Token) (Storage:=Storage) :=
  (fun lexbuf (storage: Storage) => (AnalysisFailedUserRaisedError (c lexbuf.(str_val)) lexbuf, storage)).
  
Definition raise_s {Token Storage: Set} (c: Storage -> string) : semantic_action (Token:=Token) (Storage:=Storage) :=
  (fun lexbuf (storage: Storage) => (AnalysisFailedUserRaisedError (c storage) lexbuf, storage)).

Definition store {Token Storage: Set} f : semantic_action (Token:=Token) (Storage:=Storage) :=
  (fun lexbuf (storage: Storage) => (AnalysisTerminated None lexbuf, f lexbuf storage)).
  
Definition fcat_store lexbuf storage := String.append storage (str_val lexbuf).
  
Definition fflush_store {Storage: Set} (lexbuf: Lexbuf) (storage : Storage) := EmptyString.

Definition set_storage {Token Storage: Set} s : semantic_action (Token:=Token) (Storage:=Storage) :=
  (fun lexbuf (storage: Storage) => (AnalysisTerminated None lexbuf, s)).

Definition let_in {Token Storage: Set} lex f_store : semantic_action (Token:=Token) (Storage:=Storage) :=
  (
    fun lexbuf (storage: Storage) => 
      (
        match lex lexbuf storage with
        | (AnalysisTerminated tok_opt lexbuf', storage') => (AnalysisTerminated tok_opt lexbuf', f_store tok_opt storage')
        | r => r
        end
      )
  ).
  
Fixpoint sequence {Token Storage: Set} (seq: list (semantic_action (Token:=Token) (Storage:=Storage))) lexbuf (storage: Storage) {struct seq} := 
  match seq with
  | [] => (AnalysisTerminated None lexbuf, storage)
  | f::[] => f lexbuf storage
  | h::t => 
      (
        match h lexbuf storage with
        | (AnalysisTerminated tok_opt lexbuf', storage') => sequence t lexbuf' storage'
        | r => r
        end
      )
  end.
  
Definition empty {Token Storage: Set} : semantic_action (Token:=Token) (Storage:=Storage) := 
  (fun lexbuf (storage: Storage) => (AnalysisTerminated None lexbuf, storage)).

Definition EOF str := match str with | EmptyString => true | _ => false end.

Definition default_fuel := 5000.