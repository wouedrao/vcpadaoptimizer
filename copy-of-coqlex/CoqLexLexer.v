(* *********************************************************************)
(*                                                                     *)
(*                 Coqlex verified lexer generator                     *)
(*                                                                     *)
(*  Copyright 2021 Siemens Mobility SAS and Institut National de       *)
(*  Recherche en Informatique et en Automatique.                       *)
(*  All rights reserved. This file is distributed under                *)
(*  the terms of the INRIA Non-Commercial License Agreement (see the   *)
(*  LICENSE file).                                                     *)
(*                                                                     *)
(* *********************************************************************)

Require Import LexerDefinition RValues.
Require Import Parser.
Require Import RegExp.Definitions RegExp.Char.
Require Import CoqLexUtils.
Import ListNotations.

Fixpoint action_lexer fuel lexbuf storage {struct fuel} := match fuel with
| 0 => (AnalysisNoFuel lexbuf, storage)
| S n =>
  (  
    match generalizing_elector longest_match_elector 
      (
        [
          (string_to_re "{", sequence [store fcat_store; action_lexer n; store fcat_store; action_lexer n]); 
          (string_to_re "}", ret_s BRACECONTENT); 
          (Char "010", sequence [new_line; store fcat_store; action_lexer n]); 
          (regex_any, sequence [store fcat_store; action_lexer n])
        ], 
        [
          (CoqLexUtils.EOF, raise "Un-finished Action"%string)
        ]
      ) 
      (remaining_str lexbuf) with
    | Some (FunctionElected _ ac) => ac lexbuf storage
    | Some (RegexpElected cand) =>
        let (pref, suff) := string_split (electionScore cand) (remaining_str lexbuf) in
        (electionAction cand)
        {|
           s_pos := cur_pos lexbuf;
           str_val := pref;
           cur_pos := {|
                      l_number := l_number (cur_pos lexbuf);
                      c_number := c_number (cur_pos lexbuf) + electionScore cand;
                      abs_pos := abs_pos (cur_pos lexbuf) + electionScore cand |};
           remaining_str := suff |}
        storage
        
    | None => (AnalysisFailedEmptyToken lexbuf, storage)
    end
  )
end. 

Fixpoint action_lexer_user fuel lexbuf storage {struct fuel} := match fuel with
| 0 => (AnalysisNoFuel lexbuf, storage)
| S n =>
  (  
    match generalizing_elector longest_match_elector 
      (
        [
          (string_to_re "%{", sequence [store fcat_store; action_lexer_user n; store fcat_store; action_lexer_user n]); 
          (string_to_re "}%", ret_s BRACECONTENT); 
          (Char "010", sequence [new_line; store fcat_store; action_lexer_user n]); 
          (regex_any, sequence [store fcat_store; action_lexer_user n])
        ], 
        [
          (CoqLexUtils.EOF, raise "Un-finished Action"%string)
        ]
      ) 
      (remaining_str lexbuf) with
    | Some (FunctionElected _ ac) => ac lexbuf storage
    | Some (RegexpElected cand) =>
        let (pref, suff) := string_split (electionScore cand) (remaining_str lexbuf) in
        (electionAction cand)
        {|
           s_pos := cur_pos lexbuf;
           str_val := pref;
           cur_pos := {|
                      l_number := l_number (cur_pos lexbuf);
                      c_number := c_number (cur_pos lexbuf) + electionScore cand;
                      abs_pos := abs_pos (cur_pos lexbuf) + electionScore cand |};
           remaining_str := suff |}
        storage
        
    | None => (AnalysisFailedEmptyToken lexbuf, storage)
    end
  )
end. 

Fixpoint comment {Token Storage: Set} fuel lexbuf storage {struct fuel} := match fuel with
| 0 => (AnalysisNoFuel lexbuf, storage)
| S n =>
  (  
    match generalizing_elector (Action := semantic_action (Token:=Token) (Storage:=Storage)) longest_match_elector 
      (
        [
          (string_to_re "(*", sequence [comment n; comment n]); 
          (string_to_re "*)", empty); 
          (Char "010", sequence [new_line; comment n]); 
          (regex_any, comment n);
          (string_to_re "(", comment n)
        ], 
        [
          (CoqLexUtils.EOF, raise "Un-finished Comment"%string)
        ]
      ) 
      (remaining_str lexbuf) with
    | Some (FunctionElected _ ac) => ac lexbuf storage
    | Some (RegexpElected cand) =>
        let (pref, suff) := string_split (electionScore cand) (remaining_str lexbuf) in
        (electionAction cand)
        {|
           s_pos := cur_pos lexbuf;
           str_val := pref;
           cur_pos := {|
                      l_number := l_number (cur_pos lexbuf);
                      c_number := c_number (cur_pos lexbuf) + electionScore cand;
                      abs_pos := abs_pos (cur_pos lexbuf) + electionScore cand |};
           remaining_str := suff |}
        storage
        
    | None => (AnalysisFailedEmptyToken lexbuf, storage)
    end
  )
end. 

Fixpoint storage_type_setting fuel lexbuf storage {struct fuel} := match fuel with
| 0 => (AnalysisNoFuel lexbuf, storage)
| S n =>
  (  
    match generalizing_elector longest_match_elector 
      (
        [
          (string_to_re ";", ret_s STORAGE_SET_TYPE); 
          (Char "010", sequence [new_line; store fcat_store; storage_type_setting n]); 
          (regex_any, sequence [store fcat_store; storage_type_setting n])
        ], 
        [
          (CoqLexUtils.EOF, raise "Un-finished storage_type_setting"%string)
        ]
      ) 
      (remaining_str lexbuf) with
    | Some (FunctionElected _ ac) => ac lexbuf storage
    | Some (RegexpElected cand) =>
        let (pref, suff) := string_split (electionScore cand) (remaining_str lexbuf) in
        (electionAction cand)
        {|
           s_pos := cur_pos lexbuf;
           str_val := pref;
           cur_pos := {|
                      l_number := l_number (cur_pos lexbuf);
                      c_number := c_number (cur_pos lexbuf) + electionScore cand;
                      abs_pos := abs_pos (cur_pos lexbuf) + electionScore cand |};
           remaining_str := suff |}
        storage
        
    | None => (AnalysisFailedEmptyToken lexbuf, storage)
    end
  )
end. 

Fixpoint token_type_setting fuel lexbuf storage {struct fuel} := match fuel with
| 0 => (AnalysisNoFuel lexbuf, storage)
| S n =>
  (  
    match generalizing_elector longest_match_elector 
      (
        [
          (string_to_re ";", ret_s TOKEN_SET_TYPE); 
          (Char "010", sequence [new_line; store fcat_store; token_type_setting n]); 
          (regex_any, sequence [store fcat_store; token_type_setting n])
        ], 
        [
          (CoqLexUtils.EOF, raise "Un-finished token_type_setting"%string)
        ]
      ) 
      (remaining_str lexbuf) with
    | Some (FunctionElected _ ac) => ac lexbuf storage
    | Some (RegexpElected cand) =>
        let (pref, suff) := string_split (electionScore cand) (remaining_str lexbuf) in
        (electionAction cand)
        {|
           s_pos := cur_pos lexbuf;
           str_val := pref;
           cur_pos := {|
                      l_number := l_number (cur_pos lexbuf);
                      c_number := c_number (cur_pos lexbuf) + electionScore cand;
                      abs_pos := abs_pos (cur_pos lexbuf) + electionScore cand |};
           remaining_str := suff |}
        storage
        
    | None => (AnalysisFailedEmptyToken lexbuf, storage)
    end
  )
end. 

Fixpoint thelexer fuel lexbuf storage {struct fuel} := match fuel with
| 0 => (AnalysisNoFuel lexbuf, storage)
| S n =>
  (  
    match generalizing_elector longest_match_elector 
      (
        [
          (charList_to_re [" "%char; "009"%char; "013"%char], thelexer n); 
          (string_to_re "%fix_storage_type", sequence [set_storage EmptyString; storage_type_setting (fuel * 5000)]); 
          (string_to_re "%fix_token_type", sequence [set_storage EmptyString; token_type_setting (fuel * 5000)]); 
          (Char "010", sequence [new_line; thelexer n]);
			    (Char "(", ret (LPARENT tt)); 
			    (Char ")", ret (RPARENT tt)); 
			    (Char "[", ret (LBRACKET tt)); 
			    (Char "]", ret (RBRACKET tt)); 
			    (Char "*", ret (STAR tt)); 
			    (Char "_", ret (UNDERSCORE tt)); 
			    (Char "+", ret (PLUS tt)); 
			    (Char "|", ret (VBAR tt)); 
			    (Char "?", ret (QMARK tt)); 
			    (Char "-", ret (MINUS tt)); 
			    (Char "^", ret (CIRCUMFLEX tt)); 
			    (Char "$", ret (DOLLARD tt)); 
      		(string_to_re "%polymorphic_token", ret (TOKEN_POLY tt)); 
      		(string_to_re "%polymorphic_storage", ret (STORAGE_POLY tt)); 
      		(string_to_re "EOF", ret (EOF_SYMB tt)); 
      		(string_to_re "eof", ret (EOF_SYMB tt)); 
      		(Char "=", ret (EQ tt)); 
          (string_to_re "let", ret (LET tt)); 
			    (string_to_re "rule", ret (RULE tt)); 
			    (string_to_re "and", ret (AND tt)); 
			    (string_to_re "then", ret (THEN tt)); 
			    (string_to_re "parse", ret (PARSE tt)); 
			    (string_to_re "shortest", ret (SHORTEST tt)); 
      		(identifier, ret_l IDENT); 
      		(string_regex, ret_f (fun lexbuf _ => STR_LIT (ocamlStrInterp (removeFirstAndLast (str_val lexbuf))) )); 
      		(char_regex, 
      		  (fun lexbuf storage => 
      		    match to_Char (ocamlStrInterp (removeFirstAndLast (str_val lexbuf))) with 
      		    | Some v => (AnalysisTerminated (Some (CH_LIT v)) lexbuf, storage)
      		    | None => (AnalysisFailedUserRaisedError "Impossible" lexbuf, storage) 
      		    end
    		    )
  		    ); 
      		(string_to_re "(*", sequence [set_storage EmptyString; comment (fuel * 5000); thelexer n]); 
          (string_to_re "{",  sequence [set_storage EmptyString; action_lexer (fuel * 5000)]);
      		(string_to_re "%{", sequence [set_storage EmptyString; action_lexer_user (fuel * 5000)])
        ], 
        [
          (CoqLexUtils.EOF, ret (Parser.EOF tt))
        ]
      ) 
      (remaining_str lexbuf) with
    | Some (FunctionElected _ ac) => ac lexbuf storage
    | Some (RegexpElected cand) =>
        let (pref, suff) := string_split (electionScore cand) (remaining_str lexbuf) in
        (electionAction cand)
        {|
           s_pos := cur_pos lexbuf;
           str_val := pref;
           cur_pos := {|
                      l_number := l_number (cur_pos lexbuf);
                      c_number := c_number (cur_pos lexbuf) + electionScore cand;
                      abs_pos := abs_pos (cur_pos lexbuf) + electionScore cand |};
           remaining_str := suff |}
        storage
        
    | None => (AnalysisFailedEmptyToken lexbuf, storage)
    end
  )
end. 


