Require Import PartialView ContextRule 
  Semantics_f Semantics Values Ast OptimUtils FlowOptim ContextRule TacUtils PartialViewOptimizerPropagator.
Require Import List String ZArith Lia.
Import ListNotations.

Fixpoint unloop maxIter env cond ins := 
(
  match evalSec_opel_w_f env cond with
  | Some (Bool false) => Some (ISeqEnd INull, env)
  | Some (Bool true) => 
    (
      match maxIter with
      | 0 => None
      | S n => 
        (
          match contextRuleInstruction_s default_fuel env ins with
          | None => None
          | Some env' => 
              (
                match unloop n env' cond ins with
                | Some (ins_s_unrolled, f_env) => Some (stmtAppend ins ins_s_unrolled, f_env)
                | None => None
                end
              )
           end
         )
      end
    )
  | _ => None
  end
).

Opaque contextRuleInstruction.
Opaque contextRuleInstruction_s.
Opaque contextRuleCase_instruction.
Opaque contextRuleElif_instruction.

Theorem unloop_correct : forall maxIter e cond ins res ins_s_unrolled f_env in_data,
  envpartialView e (fst in_data) ->
  unloop maxIter e cond ins = Some (ins_s_unrolled, f_env) ->
  evalInstruction in_data (ILoop cond ins) res ->
  evalInstruction_s in_data ins_s_unrolled res /\ (extractErrorType res = None -> envpartialView f_env (extractEnv res)).
Proof.
induction maxIter.
simpl.
intros.
inversion H1; subst.
assert (D := envpartialViewEvalSec_opel_w e env cond H).
apply evalSec_opel_w_f_correct in H4; rewrite H4 in *.
destruct D; rewrite H2 in *; inversion H0.
assert (D := envpartialViewEvalSec_opel_w e env cond H).
apply evalSec_opel_w_f_correct in H5; rewrite H5 in *.
destruct D; rewrite H2 in *; inversion H0.
assert (D := envpartialViewEvalSec_opel_w e env cond H).
apply evalSec_opel_w_f_correct in H6; rewrite H6 in *.
destruct D; rewrite H2 in *; inversion H0; subst.
simpl.
split; auto.
econstructor; eauto.
econstructor; eauto.
simpl.
intros.
inversion H1; subst.
assert (D := envpartialViewEvalSec_opel_w e env cond H).
apply evalSec_opel_w_f_correct in H4; rewrite H4 in *.
destruct D; rewrite H2 in *; [|inversion H0].
case_eq (contextRuleInstruction_s default_fuel e ins); intros; rewrite H3 in *; [|inversion H0].
case_eq (unloop maxIter e0 cond ins); intros; rewrite H5 in *; [|inversion H0].
induction p.
assert (extractErrorType (OK env' evn') = None) by (simpl; auto).
assert (D := contextRuleCorrect_Instruction_s (env, evn) ins (OK env' evn') H6 default_fuel e e0 H7 H H3).
simpl in D.
assert (D' := IHmaxIter e0 cond ins res a b (env', evn') D H5 H8).
destruct D'.
inversion H0; subst.
split; auto.
eapply stmtAppend_correct_continue; eauto.
assert (D := envpartialViewEvalSec_opel_w e env cond H).
apply evalSec_opel_w_f_correct in H5; rewrite H5 in *.
destruct D; rewrite H2 in *; [|inversion H0].
case_eq (contextRuleInstruction_s default_fuel e ins); intros; rewrite H3 in *; [|inversion H0].
case_eq (unloop maxIter e1 cond ins); intros; rewrite H4 in *; [|inversion H0].
induction p.
inversion H0; subst.
split; simpl; auto.
eapply stmtAppend_correct_stop; eauto.
simpl; auto.
intros.
inversion H6.
assert (D := envpartialViewEvalSec_opel_w e env cond H).
apply evalSec_opel_w_f_correct in H6; rewrite H6 in *.
destruct D; rewrite H2 in *; inversion H0; subst.
simpl.
split; auto.
econstructor; eauto.
econstructor; eauto.
Qed.

Parameter maxIter : nat.

Definition is_empty is := match is with
| ISeqEnd INull => true
| _ => false
end.

Fixpoint optimize_Instruction dup decider env i := match i with
| ILoop cond ins => 
    (
      match evalSec_opel_w_f env cond with
      | Some (Bool false) => (INull, Some env)
      | _ => 
        (
          match forgetInstruction_s ins env with
          | None => (ILoop cond ins, None)
          | Some e => (ILoop cond (remove_incorrect_begin_loop_in_list (fst (optimize_Instruction_s dup decider e ins))), Some e)
          end
        )
      end
    )
| IIf cond ins_if ins_else => 
    (IIf cond 
      (remove_incorrect_begin_loop_in_list (fst (optimize_Instruction_s dup decider env ins_if))) 
      (fst (optimize_Elif_instruction dup decider env ins_else)), 
      contextRuleInstruction default_fuel env i)
| (ICase var case) => 
    (ICase var (fst (optimize_Case_instruction dup decider env var case)), contextRuleInstruction default_fuel env i)
| _ => (i, contextRuleInstruction default_fuel env i)
end
with optimize_Elif_instruction dup decider env i := match i with
| IElse ins => (IElse (remove_incorrect_begin_loop_in_list (fst (optimize_Instruction_s dup decider env ins))), contextRuleElif_instruction default_fuel env i)
| IElif cond ins_true ins_false =>
    (IElif cond (remove_incorrect_begin_loop_in_list (fst (optimize_Instruction_s dup decider env ins_true))) (fst (optimize_Elif_instruction dup decider env ins_false)), 
      contextRuleElif_instruction default_fuel env i)
end 
with optimize_Case_instruction dup decider env var i := match i with
| ICaseFinishDefault ins_s => 
    (ICaseFinishDefault (remove_incorrect_begin_loop_in_list (fst (optimize_Instruction_s dup decider env ins_s))), contextRuleCase_instruction default_fuel env var i)
| ICaseCons (exp, ins_s) case_t => 
    (
      (ICaseCons (exp, 
        remove_incorrect_begin_loop_in_list
          (match evalExpression_f env exp with
          | Some (Int v) =>
            (
              if will_slimSet_set var env then
              (
                match slimSetIdsValue var (Int v) env with
                | Some env' => fst (optimize_Instruction_s dup decider env' ins_s) 
                | None => fst (optimize_Instruction_s dup decider env ins_s) 
                end
              )
              else
              (
                fst (optimize_Instruction_s dup decider env ins_s) 
              )
            )
          | _ => fst (optimize_Instruction_s dup decider env ins_s)
          end)) (fst (optimize_Case_instruction dup decider env var case_t))), contextRuleCase_instruction default_fuel env var i)
end
with optimize_Instruction_s dup (decider : Ast.instruction_s -> Ast.instruction_s -> bool) env i := match i with
| ISeqEnd h =>
  (
    let (h_opt, h_env) := (optimize_Instruction dup decider env h) in 
    (match h_opt with
    | ILoop cond ins_l => 
          (
            match unloop maxIter env cond ins_l with
            | Some (ins, env') => 
              if orb (negb (dup ins_l)) (is_empty ins) then
              (  
                if decider ins (ISeqEnd h_opt) then (remove_incorrect_end_iter_in_list ins, Some env') else (ISeqEnd h_opt, h_env)
              )
              else (ISeqEnd h_opt, h_env)
            | None => (ISeqEnd h_opt, h_env)
            end
          )
    | _ => (ISeqEnd h_opt, h_env)
    end)
  )
| ISeqCons h t => 
    let (h_opt, h_env) := optimize_Instruction dup decider env h in 
    (
      match h_opt with
      | ILoop cond ins_l => 
          (
            match unloop maxIter env cond ins_l with
            | Some (ins, env') => 
                let (t_opt, t_env) := optimize_Instruction_s dup decider env' t in
                let opt_insts := remove_incorrect_end_iter_in_list (stmtAppend ins t_opt) in
                if orb (negb (dup ins_l)) (is_empty ins) then
                (
                  if decider opt_insts (ISeqCons h_opt t_opt) then 
                    (opt_insts, t_env) 
                  else 
                    (ISeqCons h_opt t_opt, t_env)
                )
                else
                  (ISeqCons h_opt t_opt, t_env)
            | None => 
                (
                  match h_env with
                  | None => 
                      (ISeqCons h_opt t, None)
                  | Some env' =>
                      let (t_opt, t_env) := optimize_Instruction_s dup decider env' t in
                      (ISeqCons h_opt t_opt, t_env)
                  end
                )
            end
          )
      | _ =>
        (
          match h_env with
          | None => 
              (ISeqCons h_opt t, None)
          | Some env' =>
              let (t_opt, t_env) := optimize_Instruction_s dup decider env' t in
              (ISeqCons h_opt t_opt, t_env)
          end
        )
      end
    )
end.

Opaque forgetInstruction_s.
Opaque remove_incorrect_begin_loop_in_list remove_incorrect_end_iter_in_list.


Theorem optimize_correct : forall dup decider,
  (
    forall in_data f res,
    evalInstruction in_data f res -> forall e,
    envpartialView e (fst in_data) ->
    evalInstruction in_data (fst (optimize_Instruction dup decider e f)) res /\ 
    (forall outenv, 
      (snd (optimize_Instruction dup decider e f)) = Some outenv -> 
      extractErrorType res = None -> envpartialView outenv (extractEnv res))
  ) /\ 
  (
    forall in_data f res,
    evalInstruction_s in_data f res -> forall e,
    envpartialView e (fst in_data) ->
    evalInstruction_s in_data (fst (optimize_Instruction_s dup decider e f)) res /\ 
    (forall outenv, 
      (snd (optimize_Instruction_s dup decider e f)) = Some outenv -> 
      extractErrorType res = None -> envpartialView outenv (extractEnv res))
  ) /\ 
  (
    forall in_data f res,
    evalElif_instruction in_data f res -> forall e,
    envpartialView e (fst in_data) -> 
    evalElif_instruction in_data (fst (optimize_Elif_instruction dup decider e f)) res /\ 
    (forall outenv,  
      (snd (optimize_Elif_instruction dup decider e f)) = Some outenv -> 
      extractErrorType res = None -> envpartialView outenv (extractEnv res))
  ) /\ 
  (
    forall in_data exp f res,
    evalCase_instruction in_data exp f res -> forall e,
    envpartialView e (fst in_data) ->
    evalCase_instruction in_data exp (fst (optimize_Case_instruction dup decider e exp f)) res /\ 
    (forall outenv, 
      (snd (optimize_Case_instruction dup decider e exp f)) = Some outenv -> 
      extractErrorType res = None -> envpartialView outenv (extractEnv res)) 
  ).
Proof.
intros.
apply sem_inst_mutind; try (solve [split; [econstructor; eauto|unfold optimize_Instruction; unfold snd; intros; 
eapply contextRuleCorrect_Instruction; eauto; econstructor; eauto]]); simpl.

split.
eapply evalInstructionIIfTrue; eauto.
eapply remove_incorrect_begin_loop_in_list_correct; eauto.
eapply H; eauto.
intros.
eapply contextRuleCorrect_Instruction with (in_data:=(env, evn)) ; eauto.
eapply evalInstructionIIfTrue; eauto.
intros.
split.
eapply evalInstructionIIfFalse; eauto.
eapply H; eauto.
intros.
eapply contextRuleCorrect_Instruction with (in_data:=(env, evn)); eauto.
eapply evalInstructionIIfFalse; eauto.
intros.
split.
eapply evalInstructionICase; eauto.
eapply H; eauto.
intros.
eapply contextRuleCorrect_Instruction with (in_data:=(env, evn)); eauto.
eapply evalInstructionICase; eauto.

(*  Loop True *)
intros.
assert (D := envpartialViewEvalSec_opel_w e2 env cond H1).
apply evalSec_opel_w_f_correct in e.
rewrite e in D.
apply evalSec_opel_w_f_correct in e.
destruct D; rewrite H2.
case_eq (forgetInstruction_s ins e2); intros.
split.
eapply evalInstructionILoopTrueOk with (env':=env') (evn':=evn'); eauto.
eapply remove_incorrect_begin_loop_in_list_correct; eauto.
eapply H; eauto.
apply forgetInstruction_is_lower in H3.
eapply envpartialView_trans; eauto.
assert (extractErrorType (OK env' evn') = None) by (simpl; auto).
assert 
  (D := 
  forgetInstruction_is_lower_after_exec 
  (env, evn) 
  ins 
  (OK env' evn') e0 H4 e2 e3 H1 H3).
assert (D' := H0 e3 D).
destruct D'.
case_eq (evalSec_opel_w_f e3 cond); intros; rewrite H7 in H5.
assert (forgetInstruction_s ins e3 = Some e3).
eapply forgetInstruction_is_idempoten; eauto.
rewrite H8 in H5.
induction v; auto.
induction n; auto.
apply forgetInstruction_is_lower in H3.
assert (D' := envpartialViewEvalSec_opel_w e3 e2 cond H3).
rewrite H7, H2 in D'.
destruct D'; inversion H9.
assert (forgetInstruction_s ins e3 = Some e3).
eapply forgetInstruction_is_idempoten; eauto.
rewrite H8 in H5; auto.
assert (extractErrorType (OK env' evn') = None) by (simpl; auto).
assert 
  (D := 
  forgetInstruction_is_lower_after_exec 
  (env, evn) 
  ins 
  (OK env' evn') e0 H4 e2 e3 H1 H3).
intros.
eapply H0; eauto.
unfold snd in H5.
inversion H5; subst.
simpl.
assert (forgetInstruction_s ins outenv = Some outenv).
eapply forgetInstruction_is_idempoten; eauto.
rewrite H7.
induction (evalSec_opel_w_f outenv cond); auto.
induction a; auto.
induction n; auto.
split.
eapply evalInstructionILoopTrueOk with (env':=env') (evn':=evn'); eauto.
intros.
unfold snd in H4.
inversion H4.
case_eq (forgetInstruction_s ins e2); intros.
split.
eapply evalInstructionILoopTrueOk with (env':=env') (evn':=evn'); eauto.
eapply remove_incorrect_begin_loop_in_list_correct; eauto.
eapply H; eauto.
apply forgetInstruction_is_lower in H3.
eapply envpartialView_trans; eauto.
assert (extractErrorType (OK env' evn') = None) by (simpl; auto).
assert 
  (D := 
  forgetInstruction_is_lower_after_exec 
  (env, evn) 
  ins 
  (OK env' evn') e0 H4 e2 e3 H1 H3).
assert (D' := H0 e3 D).
destruct D'.
case_eq (evalSec_opel_w_f e3 cond); intros; rewrite H7 in *.
assert (forgetInstruction_s ins e3 = Some e3).
eapply forgetInstruction_is_idempoten; eauto.
rewrite H8 in H5.
induction v; auto.
induction n; auto.
apply forgetInstruction_is_lower in H3.
assert (D' := envpartialViewEvalSec_opel_w e3 e2 cond H3).
rewrite H7, H2 in D'.
destruct D'; inversion H9.
assert (forgetInstruction_s ins e3 = Some e3).
eapply forgetInstruction_is_idempoten; eauto.
rewrite H8 in H5.
auto.
unfold snd.
assert (extractErrorType (OK env' evn') = None) by (simpl; auto).
assert 
  (D := 
  forgetInstruction_is_lower_after_exec 
  (env, evn) 
  ins 
  (OK env' evn') e0 H4 e2 e3 H1 H3).
intros.
eapply H0; eauto.
inversion H5; subst.
simpl.
assert (forgetInstruction_s ins outenv = Some outenv).
eapply forgetInstruction_is_idempoten; eauto.
rewrite H7.
induction (evalSec_opel_w_f outenv cond); auto.
induction a; auto.
induction n; auto.
split.
eapply evalInstructionILoopTrueOk with (env':=env') (evn':=evn'); eauto.
intros.
inversion H4.

intros.
simpl.
assert (D := envpartialViewEvalSec_opel_w e2 env cond H0).
apply evalSec_opel_w_f_correct in e0; rewrite e0 in D; apply evalSec_opel_w_f_correct in e0.
destruct D; rewrite H1.
case_eq (forgetInstruction_s ins e2); intros; simpl.
split.
eapply evalInstructionILoopTrueError; eauto.
eapply remove_incorrect_begin_loop_in_list_correct; eauto.
eapply H; eauto.
apply forgetInstruction_is_lower in H2.
eapply envpartialView_trans; eauto.
intros.
inversion H4.
split.
eapply evalInstructionILoopTrueError; eauto.
intros.
inversion H4.
case_eq (forgetInstruction_s ins e2); intros; simpl.
split.
eapply evalInstructionILoopTrueError; eauto.
eapply remove_incorrect_begin_loop_in_list_correct; eauto.
eapply H; eauto.
apply forgetInstruction_is_lower in H2.
eapply envpartialView_trans; eauto.
intros.
inversion H4.
split.
eapply evalInstructionILoopTrueError; eauto.
intros.
inversion H4.

(*Loop False*)
simpl.
intros.
assert (D := envpartialViewEvalSec_opel_w e0 env cond H).
apply evalSec_opel_w_f_correct in e; rewrite e in D; apply evalSec_opel_w_f_correct in e.
destruct D; rewrite H0.
simpl.
split.
econstructor; eauto.
intros.
inversion H1; subst; auto.
case_eq (forgetInstruction_s ins e0); intros; simpl.
split.
eapply evalInstructionILoopFalse; eauto.
intros.
inversion H2; subst; auto.
apply forgetInstruction_is_lower in H1.
eapply envpartialView_trans; eauto.
split.
eapply evalInstructionILoopFalse; eauto.
intros.
inversion H2.

(*Seq*)

intros.
assert (D := H e0 H0).
destruct D.
simpl.
case_eq (optimize_Instruction dup decider e0 i); intros; rewrite H3 in *.
simpl.
induction i0; try (split; solve [econstructor; eauto | eapply H2; eauto]); inversion H1; subst.
case_eq (unloop maxIter e0 s i0); intros; [|split; solve [econstructor; eauto | eapply H2; eauto]].
induction p.
induction ((negb (dup i0) || is_empty a)%bool); [|split; solve [econstructor; eauto | eapply H2; eauto]].
induction (decider a (ISeqEnd (ILoop s i0))); simpl. 
split.
eapply remove_incorrect_end_iter_in_list_correct; eauto.
eapply unloop_correct; eauto.
intros.
inversion H5; subst.
eapply unloop_correct with (in_data := (env, evn)); eauto.
split.
econstructor; eauto.
intros.
eapply H2; eauto.
case_eq (unloop maxIter e0 s i0); intros; [|split; solve [econstructor; eauto | eapply H2; eauto]].
induction p.
induction ((negb (dup i0) || is_empty a)%bool); [|split; solve [econstructor; eauto | eapply H2; eauto]].
induction (decider a (ISeqEnd (ILoop s i0))); simpl. 
split.
eapply remove_incorrect_end_iter_in_list_correct; eauto.
eapply unloop_correct; eauto.
intros.
inversion H6.
split.
econstructor; eauto.
intros.
inversion H6.
case_eq (unloop maxIter e0 s i0); intros; [|split; solve [econstructor; eauto | eapply H2; eauto]].
induction p.
induction ((negb (dup i0) || is_empty a)%bool); [|split; solve [econstructor; eauto | eapply H2; eauto]].
induction (decider a (ISeqEnd (ILoop s i0))); simpl. 
split.
eapply remove_incorrect_end_iter_in_list_correct; eauto.
eapply unloop_correct; eauto.
intros.
inversion H5; subst.
assert (D:= unloop_correct maxIter e0 s i0 (OK env evn) a outenv (env, evn) H0 H4 H1).
apply D; simpl; auto.
split.
econstructor; eauto.
intros.
eapply H2; eauto.

intros.
case_eq (optimize_Instruction dup decider e1 h); intros.
assert (D := H e1 H1).
destruct D; simpl in *.
induction o.
rewrite H2 in *.
simpl in H4.
assert (envpartialView a env') by eauto.
case_eq (optimize_Instruction_s dup decider a t); intros.
assert (D0 := H0 a H5).
destruct D0; simpl in *.
rewrite H6 in *.
simpl in H7.
induction i; try solve [simpl; split; eauto; eapply evalInstruction_sISeqConsOk; eauto].
case_eq (unloop maxIter e1 s i); intros; try solve [simpl; split; eauto; eapply evalInstruction_sISeqConsOk; eauto].
induction p.
(* case_eq ((negb (dup a0) || is_empty a0)%bool); intros; try solve [simpl; split; eauto; eapply evalInstruction_sISeqConsOk; eauto]. *)
case_eq (optimize_Instruction_s dup decider b t); intros.
case_eq ((negb (dup i) || is_empty a0)%bool); intros; try solve [simpl; split; eauto; eapply evalInstruction_sISeqConsOk; eauto].
case_eq (decider (remove_incorrect_end_iter_in_list (stmtAppend a0 i1))
         (ISeqCons (ILoop s i) i1)); intros; try solve [simpl; split; eauto; eapply evalInstruction_sISeqConsOk; eauto].
simpl.
split.
eapply remove_incorrect_end_iter_in_list_correct; eauto.
eapply stmtAppend_correct_continue with (res:=(OK env' evn')); eauto.
eapply unloop_correct; eauto.
simpl; eauto.
assert (envpartialView b env').
eapply unloop_correct with (res:=(OK env' evn')); eauto.
simpl; eauto.
assert (D1 := H0 b H13).
rewrite H10 in D1.
simpl in D1.
destruct D1; eauto.
assert (envpartialView b env').
eapply unloop_correct with (res:=(OK env' evn')); eauto.
simpl; eauto.
assert (D1 := H0 b H13).
simpl in D1.
destruct D1; eauto.
intros.
rewrite H10 in *.
induction o0; eauto.
split.
simpl.
econstructor; eauto.
assert (envpartialView b env').
eapply unloop_correct with (res:=(OK env' evn')); eauto.
simpl; eauto.
assert (D1 := H0 b H13).
simpl in D1.
destruct D1; eauto.
rewrite H10 in *.
eauto.
simpl; intros.
assert (envpartialView b env').
eapply unloop_correct with (res:=(OK env' evn')); eauto.
simpl; eauto.
assert (D1 := H0 b H15).
simpl in D1.
destruct D1; eauto.
rewrite H10 in *.
eauto.
split.
simpl.
econstructor; eauto.
assert (envpartialView b env').
eapply unloop_correct with (res:=(OK env' evn')); eauto.
simpl; eauto.
assert (D1 := H0 b H12).
simpl in D1.
destruct D1; eauto.
rewrite H10 in *.
eauto.
simpl; intros.
assert (envpartialView b env').
eapply unloop_correct with (res:=(OK env' evn')); eauto.
simpl; eauto.
assert (D1 := H0 b H14).
simpl in D1.
destruct D1; eauto.
rewrite H10 in *.
eauto.


rewrite H2 in *.
simpl in *.
induction i; try solve [simpl; split; intros; einversion; eapply evalInstruction_sISeqConsOk; eauto].
case_eq (unloop maxIter e1 s i); try solve [simpl; split; intros; einversion; eapply evalInstruction_sISeqConsOk; eauto].
induction p.
case_eq (optimize_Instruction_s dup decider b t); intros.
assert (envpartialView b env').
eapply unloop_correct with (res:=(OK env' evn')); eauto.
simpl; eauto.
assert (D1 := H0 b H7).
destruct D1.
rewrite H5 in *; simpl in *.
case_eq ((negb (dup i) || is_empty a)%bool); intros.
case_eq (decider (remove_incorrect_end_iter_in_list (stmtAppend a i0))
         (ISeqCons (ILoop s i) i0)); intros.
simpl.
split; eauto.
eapply remove_incorrect_end_iter_in_list_correct; eauto.
eapply stmtAppend_correct_continue with (res:=(OK env' evn')); eauto.
eapply unloop_correct; eauto.
split; simpl; eauto.
eapply evalInstruction_sISeqConsOk; eauto.
split; simpl; eauto.
eapply evalInstruction_sISeqConsOk; eauto.

intros.
case_eq (optimize_Instruction dup decider e1 h); intros.
assert (D := H e1 H0).
destruct D; rewrite H1 in *; simpl in *.
induction o.
case_eq (optimize_Instruction_s dup decider a t); intros.
induction i; try solve [split; simpl; intros; einversion; eauto; eapply evalInstruction_sISeqConsError; eauto].
case_eq (unloop maxIter e1 s i); intros; try solve [split; simpl; intros; einversion; eauto; eapply evalInstruction_sISeqConsError; eauto].
induction p.
split; intros; einversion.
case_eq (optimize_Instruction_s dup decider b t); intros.
case_eq ((negb (dup i) || is_empty a0)%bool); intros.
case_eq (decider (remove_incorrect_end_iter_in_list (stmtAppend a0 i1))
         (ISeqCons (ILoop s i) i1)); intros.
simpl.
eapply remove_incorrect_end_iter_in_list_correct; eauto.
eapply stmtAppend_correct_stop; eauto; simpl; eauto.
eapply unloop_correct; eauto.
eapply evalInstruction_sISeqConsError; eauto.
eapply evalInstruction_sISeqConsError; eauto.

split; intros; einversion.
induction i; try solve [simpl; intros; eapply evalInstruction_sISeqConsError; eauto].
case_eq (unloop maxIter e1 s i); intros; try solve [simpl; intros; eapply evalInstruction_sISeqConsError; eauto].
induction p.
case_eq (optimize_Instruction_s dup decider b t); intros.
case_eq ((negb (dup i) || is_empty a)%bool); intros.
case_eq (decider (remove_incorrect_end_iter_in_list (stmtAppend a i0))
         (ISeqCons (ILoop s i) i0)); intros.
simpl.
eapply remove_incorrect_end_iter_in_list_correct; eauto.
eapply stmtAppend_correct_stop; eauto; simpl; eauto.
eapply unloop_correct; eauto.
eapply evalInstruction_sISeqConsError; eauto.
eapply evalInstruction_sISeqConsError; eauto.

intros.
assert (D := H e0 H0).
destruct D.
split; intros; eauto.
econstructor; eauto.
eapply remove_incorrect_begin_loop_in_list_correct; eauto.
eapply contextRuleCorrect_Elif_instruction with (in_data:=(env, evn)); eauto.
econstructor; eauto.

intros.
assert (D := H e1 H0).
destruct D.
split; intros; eauto.
eapply evalInstruction_sIElifTrue; eauto.
eapply remove_incorrect_begin_loop_in_list_correct; eauto.
eapply contextRuleCorrect_Elif_instruction with (in_data:=(env, evn)) ; eauto.
eapply evalInstruction_sIElifTrue; eauto.

intros.
assert (D := H e1 H0).
destruct D.
split; intros; eauto.
eapply evalInstruction_sIElifFalse; eauto.
assert (evalElif_instruction (env, evn) (IElif cond ins_true ins_false) res).
eapply evalInstruction_sIElifFalse; eauto.
eapply contextRuleCorrect_Elif_instruction with (in_data:=(env, evn)) ; eauto.

intros.
assert (D := H e0 H0).
destruct D.
split; intros; eauto.
econstructor; eauto.
eapply remove_incorrect_begin_loop_in_list_correct; eauto.
inversion H4.

intros.
assert (D := H e1 H0).
destruct D.
split; intros; eauto.
econstructor; eauto.
eapply remove_incorrect_begin_loop_in_list_correct; eauto.
inversion H4.

intros.
split.
econstructor; eauto.
eapply remove_incorrect_begin_loop_in_list_correct; eauto.
assert (D := H e1 H0).
destruct D.
case_eq (evalExpression_f e1 exp); intros.
induction v; try eauto.
assert (D := envpartialViewEvalExpression e1 env exp H0).
rewrite H3 in D.
destruct D.
apply eq_sym in H4.
apply evalExpression_f_correct in e.
assert (D := evalExpression_equal_case env var exp n e H4).
apply evalExpression_f_correct in e.
simpl in D.
case_eq (Environment.getIdsValue var env); intros; rewrite H5 in D; try einversion.
induction v; try einversion.
induction v; try einversion.
inversion D; subst.
assert (D0 := partialViewSlimsetWhenUpIsConst var e1 env (Int n) H0 H5).
case_eq (will_slimSet_set var e1); intros.
destruct D0.
rewrite H7.
eapply H1; eauto.
rewrite H7.
case_eq (forgetIdsValue var e1); intros.
assert (envpartialView e2 env).
eapply envpartialView_trans; eauto.
eapply envpartialViewForgetVsNothing; eauto.
eapply envpartialView_refl; eauto.
eapply H; eauto.
eapply H; eauto.
eapply H1; eauto.
case_eq (will_slimSet_set var e1); intros.
inversion D; subst.
case_eq (slimSetIdsValue var (Int n) e1); intros.
assert (envpartialView e2 env).
eapply partialViewSlimsetActualValue; eauto.
eapply H; eauto.
eapply H; eauto.
eapply H1; eauto.
inversion H4.
eapply H1; eauto.
intros.
assert (evalCase_instruction (env, evn) var (ICaseCons (exp, ins_s) case_t) res).
econstructor; eauto.
intros.
eapply contextRuleCorrect_Case_instruction with (in_data:=(env, evn)); eauto.

intros.
split.
eapply evalICaseConsFalse; eauto.
eapply H; eauto.
intros.
assert (evalCase_instruction (env, evn) var (ICaseCons (exp, ins_s) case_t) res).
eapply evalICaseConsFalse; eauto.
eapply contextRuleCorrect_Case_instruction with (in_data:=(env, evn)); eauto.
Qed.


Definition while_unroll_optimize dup dec ctx ins := 
  remove_incorrect_begin_loop_in_list (remove_incorrect_end_iter_in_list (remove_null_instruction_s (fst (optimize_Instruction_s dup dec ctx ins)))).

Theorem while_unroll_optimize_instruction_s_correct : contextBased_optimizer_is_correct (while_unroll_optimize).
Proof.
unfold contextBased_optimizer_is_correct.
unfold while_unroll_optimize.
intros.
eapply remove_incorrect_begin_loop_in_list_correct; eauto.
eapply remove_incorrect_end_iter_in_list_correct; eauto.
eapply remove_null_instruction_correct with (ins := (fst (optimize_Instruction_s dup deci e ins_s))); eauto.
eapply optimize_correct; eauto.
Qed.
