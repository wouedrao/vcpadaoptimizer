Require Import Map MapTypes Values Environment Event Semantics Semantics_f TacUtils.
Require Import Ast.
Require Import List String ZArith Lia Bool.
Import ListNotations.

Definition mapPartialView lower bigger := 
  forall id, 
    StringMapModule.get id lower = StringMapModule.get id bigger 
      \/ 
    ( exists v, 
      (StringMapModule.get id lower = Some (VariableValue Undefined) /\ StringMapModule.get id bigger = Some (VariableValue v)) \/ 
      (StringMapModule.get id lower = Some (ConstantValue Undefined) /\ StringMapModule.get id bigger = Some (ConstantValue v))
    ).

Lemma mapPartialView_refl: forall m,
  mapPartialView m m.
Proof.
unfold mapPartialView.
intros.
left.
auto.
Qed.

Lemma mapPartialView_trans: forall m m' m'',
  mapPartialView m m' -> mapPartialView m' m'' -> mapPartialView m m''.
Proof.
unfold mapPartialView.
intros.
assert (D := H id).
assert (D0 := H0 id).
destruct D; destruct D0.
left.
transitivity (StringMapModule.get id m'); auto.
destruct H2.
destruct H2.
destruct H2.
rewrite H1.
right.
eauto.
destruct H2.
rewrite H1.
right.
eauto.
destruct H1.
destruct H1.
destruct H1.
rewrite <- H2 in *.
eauto.
destruct H1.
rewrite H1.
rewrite <- H2.
eauto.
destruct H1.
destruct H2.
destruct H1.
destruct H2.
destruct H1.
destruct H2.
right; eauto.
destruct H1, H2.
rewrite H3 in H2.
inversion H2.
destruct H1, H2.
destruct H2.
rewrite H3 in H2.
inversion H2.
destruct H2.
right; eauto.
Qed.

Lemma mapPartialView_setV: forall lower bigger,
  mapPartialView lower bigger ->
  forall id v, 
  mapPartialView (StringMapModule.set id v lower) (StringMapModule.set id v bigger).
Proof.
unfold mapPartialView.
intros.
rewrite StringMapModule.gsspec.
rewrite StringMapModule.gsspec.
induction (String_Equality.eq id0 id); auto.
Qed.

Lemma mapPartialView_setVUndef: forall lower bigger,
  mapPartialView lower bigger ->
  forall id v, 
  mapPartialView (StringMapModule.set id (VariableValue Undefined) lower) (StringMapModule.set id (VariableValue v) bigger).
Proof.
unfold mapPartialView.
intros.
rewrite StringMapModule.gsspec.
rewrite StringMapModule.gsspec.
induction (String_Equality.eq id0 id); auto.
right.
eexists; eauto.
Qed.

Lemma mapPartialView_getSome_b2l : forall lower bigger id v',
  mapPartialView lower bigger ->
  StringMapModule.get id bigger = Some v' ->
  (exists v, StringMapModule.get id lower = Some v /\ v = v') \/ 
  (exists v, StringMapModule.get id lower = Some (VariableValue Undefined) /\ v' = (VariableValue v)) \/
  (exists v, StringMapModule.get id lower = Some (ConstantValue Undefined) /\ v' = (ConstantValue v)).
Proof.
unfold mapPartialView.
intros.
assert (D:= H id).
destruct D.
rewrite H1, H0; eauto.
destruct H1.
destruct H1, H1; rewrite H0 in H2; inversion H2; subst; eauto.
Qed.

Lemma mapPartialView_getSome_l2b : forall lower bigger id v',
  mapPartialView lower bigger ->
  StringMapModule.get id lower = Some v' ->
  exists v, StringMapModule.get id bigger = Some v.
Proof.
unfold mapPartialView.
intros.
assert (D:= H id).
destruct D.
rewrite H1 in *.
exists v'; auto.
firstorder.
Qed.

Lemma mapPartialView_getNone_l2b : forall lower bigger id,
  mapPartialView lower bigger -> StringMapModule.get id lower = None -> StringMapModule.get id bigger = None.
Proof.
unfold mapPartialView.
intros.
assert (D:= H id).
destruct D.
rewrite <- H1, H0; auto.
rewrite H0 in H1; inversion H1.
destruct H2, H2; inversion H2.
Qed.

Lemma mapPartialView_getNone_b2l : forall lower bigger id,
  mapPartialView lower bigger -> StringMapModule.get id bigger = None -> StringMapModule.get id lower = None.
Proof.
unfold mapPartialView.
intros.
assert (D:= H id).
destruct D.
rewrite <- H0, H1; auto.
rewrite H0 in H1; inversion H1.
destruct H2, H2; inversion H3.
Qed.

Definition mapSetUndefined id map := match StringMapModule.get id map with
| Some (VariableValue _) => StringMapModule.set id (VariableValue Undefined) map
| Some (ConstantValue _) => StringMapModule.set id (ConstantValue Undefined) map
| _ => map
end.
   
Lemma mapPartialView_setNothingUndef: forall lower bigger id,
  mapPartialView lower bigger ->
  mapPartialView (mapSetUndefined id lower) bigger.
Proof.
intros.
unfold mapPartialView in *.
intros.
assert (D := H id0).
destruct D.
unfold mapSetUndefined.
case_eq (StringMapModule.get id lower); auto.
induction c; intros.
rewrite StringMapModule.gsspec.
rewrite <- H0 in *.
case_eq (String_Equality.eq id0 id); auto.
intros.
apply String_Equality.eq_spec_true in H2; subst; auto.
right; eauto.
rewrite StringMapModule.gsspec.
rewrite <- H0 in *.
case_eq (String_Equality.eq id0 id); auto.
intros.
apply String_Equality.eq_spec_true in H2; subst; auto.
right; eauto.
destruct H0.
unfold mapSetUndefined.
case_eq (StringMapModule.get id lower); auto.
induction c; intros.
rewrite StringMapModule.gsspec.
case_eq (String_Equality.eq id0 id); auto.
intros.
apply String_Equality.eq_spec_true in H2; subst; auto.
right; eauto.
destruct H0, H0; eauto. 
rewrite H0 in H1; inversion H1.
rewrite StringMapModule.gsspec.
case_eq (String_Equality.eq id0 id); auto.
intros.
apply String_Equality.eq_spec_true in H2; subst; auto.
right; eauto.
destruct H0, H0; eauto. 
rewrite H0 in H1; inversion H1.
Qed.

Lemma mapPartialView_getUndefsetV: forall lower bigger id v,
  mapPartialView lower bigger ->
  StringMapModule.get id lower = Some (VariableValue Undefined) -> 
  mapPartialView lower (StringMapModule.set id (VariableValue v) bigger).
Proof.
intros.
unfold mapPartialView in *.
intros.
rewrite StringMapModule.gsspec.
case_eq (String_Equality.eq id0 id); auto.
intros.
apply String_Equality.eq_spec_true in H1.
subst.
eauto.
Qed.

Lemma mapSetIdempoten {A : Type}: forall m id (v : A), StringMapModule.get id m = Some v ->
    StringMapModule.set id v m = m.
Proof.
induction m; simpl; intros.
inversion H.
induction a.
case_eq (String_Equality.eq id a); intros; rewrite H0 in *.
inversion H; subst.
auto.
f_equal.
auto.
Qed.

Definition envpartialView env' env := 
  mapPartialView env'.(localVars) env.(localVars) /\
  mapPartialView env'.(globalVars) env.(globalVars) /\
  env'.(localfunDecls) = env.(localfunDecls) /\
  env'.(funDefs) = env.(funDefs) /\
  (forall id, 
    match StringMapModule.get id env'.(externals), StringMapModule.get id env.(externals) with
    | Some m', Some m => 
        forall i, 
          StringMapModule.get i m' = StringMapModule.get i m \/ 
          (StringMapModule.get i m' = Some (Environment.EVar (VariableValue Undefined)) /\ (StringMapModule.get i m = None \/ exists v, StringMapModule.get i m = Some (Environment.EVar (VariableValue v)))) 
          \/
          (StringMapModule.get i m' = Some (Environment.EVar (ConstantValue Undefined)) /\ (exists v, StringMapModule.get i m = Some (Environment.EVar (ConstantValue v)))) 
    | Some m', None => 
        forall i, 
          StringMapModule.get i m' = None \/ 
          StringMapModule.get i m' = Some (Environment.EVar (VariableValue Undefined))
    | None, None => True
    | _, _ => False
    end
  ).
  
Lemma envpartialView_refl : forall e, envpartialView e e.
Proof.
unfold envpartialView.
intros.
repeat (split; auto); try apply mapPartialView_refl; intros.
induction (StringMapModule.get id (externals e)); auto.
Qed.

Lemma envpartialView_trans : forall e e' e'', envpartialView e e' -> envpartialView e' e'' -> envpartialView e e''.
Proof.
unfold envpartialView.
intros.
destruct H, H0.
destruct H1, H2.
destruct H3, H4.
destruct H5, H6.
repeat (split; auto); try eapply mapPartialView_trans; eauto; intros.
etransitivity; eauto.
etransitivity; eauto.
assert (H7id := H7 id).
assert (H8id := H8 id).
case_eq (StringMapModule.get id (externals e)); intros; rewrite H9 in *.
case_eq (StringMapModule.get id (externals e')); intros; rewrite H10 in *.
case_eq (StringMapModule.get id (externals e'')); intros; rewrite H11 in *.
assert (H7idi := H7id i).
assert (H8idi := H8id i).
destruct H7idi; destruct H8idi; auto.
left; etransitivity; eauto.
destruct H13.
destruct H13.
destruct H14.
rewrite H12.
right; eauto.
rewrite H12; eauto.
rewrite H12; eauto.
rewrite <- H13; eauto.
destruct H12, H13, H12, H13, H14, H15; eauto.
rewrite H14 in H13; inversion H13.
destruct H14; rewrite H14 in H13; inversion H13.
rewrite H14 in H13; inversion H13.
rewrite H14 in H13; inversion H13.
assert (H8idi := H8id i).
assert (H7idi := H7id i).
destruct H8idi.
destruct H7idi.
rewrite H12 in *; auto.
destruct H13, H13; auto.
destruct H14.
rewrite H12 in H14; inversion H14.
destruct H7idi.
rewrite H12 in H13; auto.
destruct H13, H13; auto.
destruct H14.
rewrite H12 in H14; inversion H14.
induction (StringMapModule.get id (externals e'')); inversion H8id; auto.
induction (StringMapModule.get id (externals e')); inversion H7id.
induction (StringMapModule.get id (externals e'')); inversion H8id; auto.
Qed.

Lemma ids_select_from_value_f_Undefined : forall t, ids_select_from_value_f Undefined t = Some Undefined.
Proof.
induction t; simpl; auto.
Qed.

Lemma envpartialViewGetIdsSome : forall ids e e', 
  envpartialView e e' ->
  getIdsValue ids e = getIdsValue ids e' \/ 
  ( getIdsValue ids e = Some (Value (VariableValue Undefined)) /\ 
    ((exists v, getIdsValue ids e' = Some (Value (VariableValue v))) \/ getIdsValue ids e' = None)
  ) \/ 
  ( getIdsValue ids e = Some (Value (ConstantValue Undefined)) /\ 
    ((exists v, getIdsValue ids e' = Some (Value (ConstantValue v))) \/ getIdsValue ids e' = None)
  ).
Proof.
unfold envpartialView.
intros.
destruct H.
destruct H0.
destruct H1.
destruct H2.
unfold getIdsValue.
unfold getIdValue.
case_eq ids; intros.
auto.
case_eq l; intros.
case_eq (StringMapModule.get t (localVars e')); intros.
assert (D := mapPartialView_getSome_b2l (localVars e) (localVars e') t c H H6).
destruct D.
destruct H7.
destruct H7.
subst.
rewrite H7; eauto.
destruct H7.
destruct H7.
destruct H7.
subst.
rewrite H7; eauto.
right; left; eauto.
destruct H7.
destruct H7.
subst.
rewrite H7; eauto.
right; right; eauto.
assert (D := mapPartialView_getNone_b2l (localVars e) (localVars e') t H H6).
rewrite D.
case_eq (StringMapModule.get t (globalVars e')); intros.
assert (D0 := mapPartialView_getSome_b2l (globalVars e) (globalVars e') t c H0 H7).
destruct D0.
destruct H8.
destruct H8.
subst.
rewrite H8.
auto.
destruct H8.
destruct H8.
destruct H8.
subst.
rewrite H8; right; left; eauto.
destruct H8.
destruct H8.
subst.
rewrite H8; right; right; eauto.
assert (D0 := mapPartialView_getNone_b2l (globalVars e) (globalVars e') t H0 H7).
rewrite D0.
rewrite H2.
induction (StringMapModule.get t (funDefs e')); auto.
rewrite H1.
induction (StringMapModule.get t (localfunDecls e')); auto.
case_eq (StringMapModule.get t (localVars e')); intros.
assert (D := mapPartialView_getSome_b2l (localVars e) (localVars e') t c H H6).
destruct D.
destruct H7.
destruct H7.
subst.
rewrite H7.
auto.
destruct H7.
destruct H7.
destruct H7.
subst.
rewrite H7.
rewrite ids_select_from_value_f_Undefined; eauto.
right; left; split; eauto.
induction (ids_select_from_value_f x (t0 :: l0)); eauto.
destruct H7.
destruct H7.
subst.
rewrite H7.
rewrite ids_select_from_value_f_Undefined; eauto.
induction (ids_select_from_value_f x (t0 :: l0)); eauto.
right; right; split; eauto.
assert (D := mapPartialView_getNone_b2l (localVars e) (localVars e') t H H6).
rewrite D.
case_eq (StringMapModule.get t (globalVars e')); intros.
assert (D0 := mapPartialView_getSome_b2l (globalVars e) (globalVars e') t c H0 H7).
destruct D0.
destruct H8.
destruct H8.
subst.
rewrite H8.
auto.
destruct H8.
destruct H8.
destruct H8.
subst.
rewrite H8.
rewrite ids_select_from_value_f_Undefined.
induction (ids_select_from_value_f x (t0 :: l0)); right; eauto.
destruct H8.
destruct H8.
subst.
rewrite H8.
rewrite ids_select_from_value_f_Undefined.
induction (ids_select_from_value_f x (t0 :: l0)); right; eauto.
assert (D0 := mapPartialView_getNone_b2l (globalVars e) (globalVars e') t H0 H7).
rewrite D0.
rewrite H2.
induction (StringMapModule.get t (funDefs e')); auto.
rewrite H1.
induction (StringMapModule.get t (localfunDecls e')); auto.
assert (H3id := H3 t).
case_eq (StringMapModule.get t (externals e')); intros; rewrite H8 in *.
case_eq (StringMapModule.get t (externals e)); intros; rewrite H9 in *.
assert (H3idt0 := H3id t0).
destruct H3idt0.
rewrite H10; auto.
destruct H10.
destruct H10.
destruct H11.
rewrite H10, H11; auto.
rewrite ids_select_from_value_f_Undefined.
right; eauto.
destruct H11.
rewrite H10, H11; eauto.
rewrite ids_select_from_value_f_Undefined.
right; induction (ids_select_from_value_f x l0); eauto.
destruct H10, H11.
rewrite H10, H11; eauto.
rewrite ids_select_from_value_f_Undefined.
right; induction (ids_select_from_value_f x l0); eauto.
inversion H3id.
case_eq (StringMapModule.get t (externals e)); intros; rewrite H9 in *; auto.
assert (H3idt0 := H3id t0).
destruct H3idt0; rewrite H10; auto.
rewrite ids_select_from_value_f_Undefined.
right; eauto.
Qed.

Lemma envpartialViewSetIdValue_l2b : forall e e', 
  envpartialView e e' ->
  forall id v nenv nenv',
  setIdValue id v e = Some nenv ->
  setIdValue id v e' = Some nenv' -> envpartialView nenv nenv'.
Proof.
unfold envpartialView.
intros e e' H.
destruct H.
destruct H0.
destruct H1.
destruct H2.
unfold setIdValue.
intros.
case_eq (StringMapModule.get id (localVars e)); intros.
assert (D := mapPartialView_getSome_l2b (localVars e) (localVars e') id c H H6).
destruct D.
assert (D := mapPartialView_getSome_b2l (localVars e) (localVars e') id x H H7).
destruct D.
destruct H8.
destruct H8; subst.
rewrite H7, H8 in *.
induction x; inversion H4; inversion H5; simpl.
repeat (split; auto).
apply mapPartialView_setV; auto.
destruct H8.
destruct H8, H8; subst.
rewrite H7, H8 in *; inversion H6; inversion H4; inversion H5; subst; simpl.
repeat (split; auto).
apply mapPartialView_setV; auto.
destruct H8, H8; subst.
rewrite H7, H8 in *; inversion H6; inversion H4; inversion H5; subst; simpl.
assert (D := mapPartialView_getNone_l2b (localVars e) (localVars e') id H H6).
rewrite D, H6 in *.

case_eq (StringMapModule.get id (globalVars e)); intros.
assert (D' := mapPartialView_getSome_l2b (globalVars e) (globalVars e') id c H0 H7).
destruct D'.
assert (D' := mapPartialView_getSome_b2l (globalVars e) (globalVars e') id x H0 H8).
destruct D'.
destruct H9, H9; subst.
rewrite H8, H9 in *.
induction x; inversion H4; inversion H5; simpl.
repeat (split; auto).
apply mapPartialView_setV; auto.
destruct H9.
destruct H9, H9; subst; rewrite H8, H9 in *; inversion H7; inversion H4; inversion H5; subst; simpl.
repeat (split; auto).
apply mapPartialView_setV; auto.
destruct H9, H9; subst; rewrite H8, H9 in *; inversion H7; inversion H4; inversion H5; subst; simpl.
assert (D' := mapPartialView_getNone_l2b (globalVars e) (globalVars e') id H0 H7).
rewrite D', H7 in *.
inversion H4.
Qed.

Definition forgetIdValue id env := 
    match StringMapModule.get id env.(localVars) with
      | None => 
          (
            match StringMapModule.get id env.(globalVars) with
              | None => None
              | Some ((VariableValue _)) => 
                  Some {|
                    localVars := env.(localVars);
                    globalVars := StringMapModule.set id (VariableValue Undefined) env.(globalVars);
                    funDefs := env.(funDefs);
                    localfunDecls := env.(localfunDecls);
                    externals := env.(externals)
                  |}
              | Some ((ConstantValue _)) => Some env
            end
          )
      | Some ((VariableValue _)) => 
          Some {|
            localVars := StringMapModule.set id (VariableValue Undefined) env.(localVars);
            globalVars := env.(globalVars);
            funDefs := env.(funDefs);
            localfunDecls := env.(localfunDecls);
            externals := env.(externals)
          |}
      | Some ((ConstantValue _)) => Some env
    end.
    
Lemma setVariableUndef_setUndefined : forall m id v,
  StringMapModule.get id m = Some (VariableValue v) ->
  mapSetUndefined id m = StringMapModule.set id (VariableValue Undefined) m.
Proof.
unfold mapSetUndefined; intros.
rewrite H.
auto.
Qed.

Lemma envpartialViewSetUndefined : forall e e', 
  envpartialView e e' ->
  forall id nenv,
  forgetIdValue id e = Some nenv ->
  envpartialView nenv e'.
Proof.
unfold envpartialView.
intros e e' H.
destruct H.
destruct H0.
destruct H1.
destruct H2.
unfold forgetIdValue.
intros.
case_eq (StringMapModule.get id (localVars e)); intros; rewrite H5 in *.
induction c; inversion H4; subst.
simpl.
repeat (split; auto); simpl.
simpl.
repeat (split; auto); simpl.
erewrite <- setVariableUndef_setUndefined; eauto.
eapply mapPartialView_setNothingUndef; eauto.
case_eq (StringMapModule.get id (globalVars e)); intros; rewrite H6 in *.
induction c; inversion H4; simpl; subst.
repeat (split; auto).
repeat (split; auto).
erewrite <- setVariableUndef_setUndefined; eauto.
eapply mapPartialView_setNothingUndef; eauto.
inversion H4.
Qed.

Lemma envpartialViewSetIdUndefined_l2b : forall e e', 
  envpartialView e e' ->
  forall id v nenv nenv',
  forgetIdValue id e = Some nenv ->
  setIdValue id v e' = Some nenv' -> envpartialView nenv nenv'.
Proof.
unfold envpartialView.
intros e e' H.
destruct H.
destruct H0.
destruct H1.
destruct H2.
unfold setIdValue.
unfold forgetIdValue.
intros.
case_eq (StringMapModule.get id (localVars e)); intros.
assert (D := mapPartialView_getSome_l2b (localVars e) (localVars e') id c H H6).
destruct D.
assert (D := mapPartialView_getSome_b2l (localVars e) (localVars e') id x H H7).
destruct D.
destruct H8, H8; subst.
rewrite H7, H8 in *.
induction x; inversion H4; inversion H5; simpl.
repeat (split; auto).
apply mapPartialView_setVUndef; auto.
destruct H8.
destruct H8, H8; subst.
rewrite H7, H8 in *; inversion H4; inversion H5; simpl.
repeat (split; auto).
apply mapPartialView_setVUndef; auto.
destruct H8, H8; subst.
rewrite H7, H8 in *; inversion H4; inversion H5; simpl.
assert (D := mapPartialView_getNone_l2b (localVars e) (localVars e') id H H6).
rewrite D, H6 in *.

case_eq (StringMapModule.get id (globalVars e)); intros.
assert (D' := mapPartialView_getSome_l2b (globalVars e) (globalVars e') id c H0 H7).
destruct D'.
assert (D' := mapPartialView_getSome_b2l (globalVars e) (globalVars e') id x H0 H8).
destruct D'.
destruct H9, H9; subst.
rewrite H8, H9 in *.
induction x; inversion H4; inversion H5; simpl.
repeat (split; auto).
apply mapPartialView_setVUndef; auto.
destruct H9.
destruct H9, H9; subst.
rewrite H8, H9 in *; inversion H4; inversion H5; simpl.
repeat (split; auto).
apply mapPartialView_setVUndef; auto.
destruct H9, H9; subst.
rewrite H8, H9 in *; inversion H4; inversion H5; simpl.
assert (D' := mapPartialView_getNone_l2b (globalVars e) (globalVars e') id H0 H7).
rewrite D', H7 in *.
inversion H4.
Qed.

Definition forgetIdsValue ids env := match ids with
| [] => None
| hid::[] => forgetIdValue hid env
| hid::hid0::tid0 => 
    (
      match StringMapModule.get hid env.(localVars) with
      | None => 
        (
          match StringMapModule.get hid env.(globalVars) with
          | None =>
            (
              match StringMapModule.get hid env.(funDefs) with
              | None =>
                (
                  match StringMapModule.get hid env.(localfunDecls) with
                  | Some _ => None
                  | None => 
                      (
                        match StringMapModule.get hid env.(externals) with
                        | None => 
                            (
                              Some ({|
                                localVars := env.(localVars);
                                globalVars := env.(globalVars);
                                funDefs := env.(funDefs);
                                localfunDecls := env.(localfunDecls);
                                externals := StringMapModule.set hid (StringMapModule.set hid0 (Environment.EVar (VariableValue Undefined)) []) env.(externals)
                              |})
                            )
                        | Some lib => 
                            (
                              match StringMapModule.get hid0 lib with
                              | None | Some (Environment.EVar (VariableValue _)) => 
                                  (
                                    Some ({|
                                      localVars := env.(localVars);
                                      globalVars := env.(globalVars);
                                      funDefs := env.(funDefs);
                                      localfunDecls := env.(localfunDecls);
                                      externals := StringMapModule.set hid (StringMapModule.set hid0 (Environment.EVar (VariableValue Undefined)) lib) env.(externals)
                                    |})
                                  )
                              | Some (Environment.EVar (ConstantValue v)) => Some env
                              | Some (EFDecl _) => None
                              end
                            )
                        end
                      )
                  end
                )
              | Some _ => None
              end
            )
         | Some ((ConstantValue v)) => Some env
         | Some ((VariableValue v)) => 
            Some ({|
                localVars := env.(localVars);
                globalVars := StringMapModule.set hid (VariableValue Undefined) env.(globalVars);
                funDefs := env.(funDefs);
                localfunDecls := env.(localfunDecls);
                externals := env.(externals)
              |})
        end)
      | Some ((ConstantValue v)) => Some env
      | Some ((VariableValue v)) => 
        (
          Some ({|
            localVars := StringMapModule.set hid (VariableValue Undefined) env.(localVars);
            globalVars := env.(globalVars);
            localfunDecls := env.(localfunDecls);
            funDefs := env.(funDefs);
            externals := env.(externals)
          |})
        )
      end)
end.

Lemma envpartialViewForgetVsNothing : forall ids e e' nenv, 
  envpartialView e e' -> 
  forgetIdsValue ids e = Some nenv ->
  envpartialView nenv e'.
Proof.
unfold envpartialView.
unfold getIdsValue.
unfold forgetIdsValue.
intro.
case_eq ids.
intros.
inversion H1.
intros t l.
case_eq l.
intros.
eapply envpartialViewSetUndefined; eauto.
intros.
destruct H1.
destruct H3.
destruct H4.
destruct H5.
case_eq (StringMapModule.get t (localVars e)); intros; rewrite H7 in *.
induction c; inversion H2; subst; auto.
simpl.
repeat (split; auto).
erewrite <- setVariableUndef_setUndefined; eauto.
eapply mapPartialView_setNothingUndef; eauto.
case_eq (StringMapModule.get t (globalVars e)); intros; rewrite H8 in *.
induction c; inversion H2; subst; simpl; auto.
repeat (split; auto).
erewrite <- setVariableUndef_setUndefined; eauto.
eapply mapPartialView_setNothingUndef; eauto.
case_eq (StringMapModule.get t (funDefs e)); intros; rewrite H9 in *.
inversion H2.
case_eq (StringMapModule.get t (localfunDecls e)); intros; rewrite H10 in *.
inversion H2.
assert (D := H6 t).
case_eq (StringMapModule.get t (externals e)); intros; rewrite H11 in *.
case_eq (StringMapModule.get t (externals e')); intros; rewrite H12 in *.
assert (Dt0 := D t0).
destruct Dt0.
rewrite H13 in *.
case_eq (StringMapModule.get t0 t2); intros; rewrite H14 in *.
induction e0.
induction c; inversion H2; subst; simpl; auto.
repeat (split; auto); simpl; intros.
rewrite (StringMapModule.gsspec).
case_eq (String_Equality.eq id t); intros; auto.
apply String_Equality.eq_spec_true in H; rewrite H in *; auto.
rewrite H12.
intros.
rewrite (StringMapModule.gsspec).
case_eq (String_Equality.eq i t0); intros; auto.
apply String_Equality.eq_spec_true in H0; rewrite H0 in *; auto.
right; eauto.
apply H6.
inversion H2.
inversion H2.
simpl.
repeat (split; auto); simpl; intros.
rewrite (StringMapModule.gsspec).
case_eq (String_Equality.eq id t); intros; auto.
apply String_Equality.eq_spec_true in H15; rewrite H15 in *; auto.
rewrite H12.
intros.
rewrite (StringMapModule.gsspec).
case_eq (String_Equality.eq i t0); intros; auto.
apply String_Equality.eq_spec_true in H17; rewrite H17 in *; auto.
apply H6.
destruct H13.
destruct H13; rewrite H13 in *.
inversion H2; simpl.
repeat (split; auto); simpl; intros.
rewrite (StringMapModule.gsspec).
case_eq (String_Equality.eq id t); intros; auto.
apply String_Equality.eq_spec_true in H15; rewrite H15 in *; auto.
rewrite H12.
intros.
rewrite (StringMapModule.gsspec).
case_eq (String_Equality.eq i t0); intros; auto.
apply String_Equality.eq_spec_true in H17; rewrite H17 in *; auto.
apply H6.
destruct H13; rewrite H13 in *.
inversion H2; subst.
repeat (split; auto); simpl; intros.
assert (Dt0 := D t0).
destruct Dt0; rewrite H13 in *; inversion H2; simpl; auto.
repeat (split; auto); simpl; intros.
rewrite (StringMapModule.gsspec).
case_eq (String_Equality.eq id t); intros; auto.
apply String_Equality.eq_spec_true in H14; rewrite H14 in *; auto.
rewrite H12.
intros.
rewrite (StringMapModule.gsspec).
case_eq (String_Equality.eq i t0); intros; auto.
apply H6.
repeat (split; auto); simpl; intros.
rewrite (StringMapModule.gsspec).
case_eq (String_Equality.eq id t); intros; auto.
apply String_Equality.eq_spec_true in H14; rewrite H14 in *; auto.
rewrite H12.
intros.
rewrite (StringMapModule.gsspec).
case_eq (String_Equality.eq i t0); intros; auto.
apply H6.
inversion H2; simpl.
repeat (split; auto); simpl; intros.
case_eq (StringMapModule.get t (externals e')); intros; rewrite H12 in *; inversion D.
rewrite (StringMapModule.gsspec).
case_eq (String_Equality.eq id t); intros; auto.
apply String_Equality.eq_spec_true in H14; rewrite H14 in *; auto.
rewrite H12.
intros.
simpl.
induction (String_Equality.eq i t0); auto.
apply H6.
Qed.

Lemma envpartialViewForgetVsSet : forall ids e e' v nenv nenv', 
  envpartialView e e' -> 
  setIdsValue ids v e' = Some nenv' ->
  forgetIdsValue ids e = Some nenv ->
  envpartialView nenv nenv'.
Proof.
unfold envpartialView.
unfold setIdsValue.
unfold forgetIdsValue.
intro.
case_eq ids.
intros.
inversion H1.
intros t l.
case_eq l.
intros.
eapply envpartialViewSetIdUndefined_l2b; eauto.
intros.
destruct H1.
destruct H4.
destruct H5.
destruct H6.
rewrite H6, H5 in *.
case_eq (StringMapModule.get t (localVars e')); intros; rewrite H8 in *.
assert (H8':=H8).
eapply mapPartialView_getSome_b2l in H8'; eauto.
destruct H8'.
destruct H9, H9.
subst.
rewrite H9 in *.
induction c; [inversion H2|].
induction (ids_set_from_value_f v0 (t0 :: l0) v); inversion H2.
inversion H3; simpl.
repeat (split; auto).
eapply mapPartialView_setVUndef; eauto.
destruct H9.
destruct H9, H9.
subst.
rewrite H9 in *.
induction (ids_set_from_value_f x (t0 :: l0) v); inversion H2.
inversion H3; simpl.
repeat (split; auto).
eapply mapPartialView_setVUndef; eauto.
destruct H9, H9.
subst.
rewrite H9 in *.
inversion H2.
eapply mapPartialView_getNone_b2l in H8; eauto; rewrite H8 in *.
case_eq (StringMapModule.get t (globalVars e')); intros; rewrite H9 in *.
assert (H9':=H9).
eapply mapPartialView_getSome_b2l in H9'; eauto.
destruct H9'.
destruct H10, H10.
subst.
rewrite H10 in *.
induction c; [inversion H2|].
induction (ids_set_from_value_f v0 (t0 :: l0) v); inversion H2.
inversion H3; simpl.
repeat (split; auto).
eapply mapPartialView_setVUndef; eauto.

destruct H10.
destruct H10, H10.
subst.
rewrite H10 in *.
induction (ids_set_from_value_f x (t0 :: l0) v); inversion H2.
inversion H3; simpl.
repeat (split; auto).
eapply mapPartialView_setVUndef; eauto.
destruct H10, H10.
subst.
rewrite H10 in *.
inversion H2.
eapply mapPartialView_getNone_b2l in H9; eauto; rewrite H9 in *.
induction (StringMapModule.get t (funDefs e')); [inversion H2|].
induction (StringMapModule.get t (localfunDecls e')); [inversion H2|].
assert (D := H7 t).
case_eq (StringMapModule.get t (externals e')); intros; rewrite H10 in *.
case_eq (StringMapModule.get t (externals e)); intros; rewrite H11 in *.
assert (Dt0 := D t0).
destruct Dt0.
rewrite H12 in *.
case_eq (StringMapModule.get t0 t1); intros; rewrite H13 in *.
induction e0.
induction c; [inversion H2|].
induction (ids_set_from_value_f v0 l0 v); inversion H2; inversion H3; simpl.
repeat (split; auto); intros.
rewrite (StringMapModule.gsspec).
rewrite (StringMapModule.gsspec).
case_eq (String_Equality.eq id t); intros; auto.
apply String_Equality.eq_spec_true in H14; subst.
rewrite (StringMapModule.gsspec).
rewrite (StringMapModule.gsspec).
case_eq (String_Equality.eq i t0); intros; auto.
apply String_Equality.eq_spec_true in H; subst.
right; eauto.
apply H7.
inversion H2.
induction (ids_set_from_value_f Undefined l0 v); inversion H2; inversion H3; simpl.
repeat (split; auto); intros.
rewrite (StringMapModule.gsspec).
rewrite (StringMapModule.gsspec).
case_eq (String_Equality.eq id t); intros; auto.
apply String_Equality.eq_spec_true in H14; subst.
rewrite (StringMapModule.gsspec).
case_eq (String_Equality.eq i t0); intros; auto.
apply String_Equality.eq_spec_true in H; subst.
right; eauto.
rewrite (StringMapModule.gsspec).
rewrite StringMapModule.Xeq_refl; eauto.
rewrite (StringMapModule.gsspec).
rewrite H.
apply D.
apply H7.
destruct H12.
destruct H12, H13; rewrite H12 in *. 
rewrite H13 in *.
induction (ids_set_from_value_f Undefined l0 v); inversion H2; inversion H3; simpl.
repeat (split; auto); intros.
rewrite (StringMapModule.gsspec).
rewrite (StringMapModule.gsspec).
case_eq (String_Equality.eq id t); intros; auto.
apply String_Equality.eq_spec_true in H14; subst.
rewrite (StringMapModule.gsspec).
case_eq (String_Equality.eq i t0); intros; auto.
apply String_Equality.eq_spec_true in H; subst.
right; eauto.
rewrite (StringMapModule.gsspec).
rewrite StringMapModule.Xeq_refl; eauto.
rewrite (StringMapModule.gsspec).
rewrite H.
apply D.
apply H7.
destruct H13.
rewrite H13 in *.
induction (ids_set_from_value_f x l0 v); inversion H2; inversion H3; simpl.
repeat (split; auto); intros.
rewrite (StringMapModule.gsspec).
rewrite (StringMapModule.gsspec).
case_eq (String_Equality.eq id t); intros; auto.
apply String_Equality.eq_spec_true in H14; subst.
rewrite (StringMapModule.gsspec).
case_eq (String_Equality.eq i t0); intros; auto.
apply String_Equality.eq_spec_true in H; subst.
right; eauto.
rewrite (StringMapModule.gsspec).
rewrite StringMapModule.Xeq_refl; eauto.
rewrite (StringMapModule.gsspec).
rewrite H.
apply D.
apply H7.
destruct H12, H13; rewrite H12 in *. 
rewrite H13 in *.
induction (ids_set_from_value_f Undefined l0 v); inversion H2; inversion H3; simpl.
inversion D.
case_eq (StringMapModule.get t (externals e)); intros; rewrite H11 in *.
assert (Dt0 := D t0).
destruct Dt0.
rewrite H12 in *.
induction (ids_set_from_value_f Undefined l0 v); inversion H2; inversion H3; simpl.
repeat (split; auto); intros.
rewrite (StringMapModule.gsspec).
rewrite (StringMapModule.gsspec).
case_eq (String_Equality.eq id t); intros; auto.
apply String_Equality.eq_spec_true in H13; subst.
rewrite (StringMapModule.gsspec).
case_eq (String_Equality.eq i t0); intros; auto.
apply String_Equality.eq_spec_true in H; subst.
right; eauto.
simpl.
rewrite StringMapModule.Xeq_refl.
left; eauto.
simpl.
rewrite H.
assert (D':= D i).
destruct D'; auto.
apply H7.
rewrite H12 in *.
induction (ids_set_from_value_f Undefined l0 v); inversion H2; inversion H3; simpl.
repeat (split; auto); intros.
rewrite (StringMapModule.gsspec).
rewrite (StringMapModule.gsspec).
case_eq (String_Equality.eq id t); intros; auto.
apply String_Equality.eq_spec_true in H13; subst.
rewrite (StringMapModule.gsspec).
case_eq (String_Equality.eq i t0); intros; auto.
apply String_Equality.eq_spec_true in H; subst.
right; eauto.
simpl.
rewrite StringMapModule.Xeq_refl.
left; eauto.
simpl.
rewrite H.
assert (D':= D i).
destruct D'; auto.
apply H7.
induction (ids_set_from_value_f Undefined l0 v); inversion H2; inversion H3; simpl.
repeat (split; auto); intros.
rewrite (StringMapModule.gsspec).
rewrite (StringMapModule.gsspec).
case_eq (String_Equality.eq id t); intros; auto.
apply String_Equality.eq_spec_true in H12; subst.
simpl.
case_eq (String_Equality.eq i t0); intros; auto.
apply String_Equality.eq_spec_true in H; subst.
right; eauto.
apply H7.
Qed.

Lemma binary_op_undefined_some_iff: forall op v v', 
    (exists r, Math.binary_operation op v v' = Some r) <-> 
    Math.binary_operation op v Undefined = Some Undefined /\ 
    Math.binary_operation op Undefined v' = Some Undefined.
Proof.
intros.
unfold Math.binary_operation.
unfold Math.and.
unfold Math.or.
unfold Math.xor.
unfold Math.unary_not.
unfold Math.to_bool.
split; intros; destruct H.
induction op, v, v'; cbn in *; auto; inversion H; clear H1.
induction (Zeq_bool 0 n); auto.
induction (Zeq_bool 0 n); auto.
induction (Zeq_bool 0 n); induction (Zeq_bool 0 n0); auto.
induction (Zeq_bool 0 n); auto.
induction (Zeq_bool 0 n); inversion H.
induction (Zeq_bool 0 n); inversion H.
induction (Zeq_bool 0 n); inversion H.
induction (Zeq_bool 0 n0); auto.
induction (Zeq_bool 0 n); inversion H; auto.
induction (Zeq_bool 0 n); inversion H; auto.
induction (Zeq_bool 0 n); induction (Zeq_bool 0 n0); inversion H; auto.
induction (Zeq_bool 0 n); inversion H; auto.
induction (Zeq_bool 0 n); inversion H; auto.
induction (Zeq_bool 0 n); inversion H; auto.
induction (Zeq_bool 0 n); inversion H; auto.
induction (Zeq_bool 0 n0); inversion H; auto.
induction (Zeq_bool 0 n); inversion H; auto.
induction (Zeq_bool 0 n); inversion H; auto.
induction (Zeq_bool 0 n); induction (Zeq_bool 0 n0); inversion H; auto.
induction (Zeq_bool 0 n); inversion H; auto.
induction (Zeq_bool 0 n); inversion H; auto.
induction (Zeq_bool 0 n); inversion H; auto.
induction (Zeq_bool 0 n); inversion H; auto.
induction (Zeq_bool 0 n0); inversion H; auto.
induction n; inversion H; auto.
induction n0; inversion H; auto.
induction n0; inversion H; auto.
induction n0; inversion H; auto.
induction n; inversion H; auto.
induction n; inversion H; auto.
induction n; inversion H; auto.
induction n0; inversion H; auto.
induction n0; inversion H; auto.
induction n0; inversion H; auto.
induction n; inversion H; auto.
induction n; inversion H; auto.
induction n; inversion H; auto.
induction n0; inversion H; auto.
induction n0; inversion H; auto.
induction n0; inversion H; auto.
induction n; inversion H; auto.
induction n; inversion H; auto.
induction op, v, v'; simpl in *; auto; inversion H; inversion H0; try rewrite H2; try rewrite H3; eauto.
induction (Zeq_bool 0 n); induction (Zeq_bool 0 n0); simpl; eauto.
induction (Zeq_bool 0 n); simpl; eauto.
induction (Zeq_bool 0 n0); simpl; eauto.
rewrite H2.
rewrite H3.
induction (Zeq_bool 0 n); simpl; eauto.
induction (Zeq_bool 0 n0); simpl; eauto.
induction (Zeq_bool 0 n0); simpl; eauto.
induction (Zeq_bool 0 n); simpl; eauto.
induction (Zeq_bool 0 n0); simpl; eauto.
induction (Zeq_bool 0 n); induction (Zeq_bool 0 n0); simpl; eauto.
induction (Zeq_bool 0 n); simpl; eauto.
induction (Zeq_bool 0 n0); simpl; eauto.
induction n0; inversion H0; eauto.
induction n0; inversion H0; eauto.
induction n0; inversion H0; eauto.
Qed.

Lemma binary_op_undefined_none_iff: forall op v v', 
    (Math.binary_operation op v Undefined = None \/ Math.binary_operation op Undefined v' = None) <-> 
    Math.binary_operation op v v' = None.
Proof.
intros.
unfold Math.binary_operation.
unfold Math.and.
unfold Math.or.
unfold Math.xor.
unfold Math.unary_not.
unfold Math.to_bool.
split; intros.
destruct H.
induction op, v; simpl in *; auto; inversion H; try clear H1.
induction (Zeq_bool 0 n); simpl; inversion H.
induction (Zeq_bool 0 n); simpl; inversion H.
induction (Zeq_bool 0 n); simpl; inversion H.
induction v'; try induction n0; auto.
induction v'; try induction n0; auto.
induction v'; try induction n0; auto.
induction n; auto.
induction v'; try induction n; auto.
induction v'; try induction n0; auto.
induction v'; try induction n0; auto.
induction v'; try induction n; auto.
induction v'; try induction n; auto.
induction v'; try induction n0; auto.
induction v'; try induction n0; auto.
induction v'; try induction n; auto.
induction v'; try induction n; auto.
induction v'; try induction n; simpl in *; auto.
induction v; auto.
induction (Zeq_bool 0 n); simpl; auto.
induction op; auto.
induction op; auto; inversion H.
induction op; auto; inversion H.
induction op; auto; inversion H.
induction op; inversion H.
unfold Math.div; induction v; auto.
unfold Math.rem; induction v; auto.
unfold Math.mod'; induction v; auto.
induction op; inversion H.
induction op; inversion H.
induction op; inversion H; induction v; auto.
induction op; inversion H; induction v; auto.
induction op; inversion H; induction v; auto; induction (Zeq_bool 0 n); simpl; auto.
induction op; inversion H; induction v; auto; induction (Zeq_bool 0 n); simpl; auto.
induction op; inversion H; induction v; auto; induction (Zeq_bool 0 n); simpl; auto.

induction v; auto; try induction (Zeq_bool 0 n); induction v'; 
auto; try induction (Zeq_bool 0 n0); simpl in *; auto; 
induction op; inversion H; auto; try induction n; try induction n0; inversion H; auto.
Qed.

Lemma binary_op_undefined_some_r: forall op v, 
    Math.binary_operation op v Undefined = Some Undefined \/ Math.binary_operation op v Undefined = None.
Proof.
intros.
unfold Math.binary_operation.
unfold Math.and.
unfold Math.or.
unfold Math.xor.
unfold Math.unary_not.
unfold Math.to_bool.
induction op, v; simpl in *; auto; induction (Zeq_bool 0 n); auto.
Qed.

Lemma binary_op_undefined_some_l: forall op v, 
    Math.binary_operation op Undefined v = Some Undefined \/ Math.binary_operation op Undefined v = None.
Proof.
intros.
unfold Math.binary_operation.
unfold Math.and.
unfold Math.or.
unfold Math.xor.
unfold Math.unary_not.
unfold Math.to_bool.
induction op, v; simpl in *; auto; induction (Zeq_bool 0 n); induction n; auto.
Qed.

Lemma unary_op_undefined: forall op,
    Math.unary_operation op Undefined = Some Undefined.
Proof.
intros.
unfold Math.unary_operation.
unfold Math.unary_not.
unfold Math.to_bool.
induction op; auto.
Qed.

Theorem envpartialViewEvalExpression: forall e e' exp,
  envpartialView e e' ->
  evalExpression_f e exp = evalExpression_f e' exp \/ (evalExpression_f e exp = Some (Undefined)).
Proof.
intros.
induction exp; simpl; auto.
assert (D := envpartialViewGetIdsSome n e e' H).
destruct D.
rewrite H0.
auto.
destruct H0.
destruct H0.
rewrite H0.
destruct H1.
destruct H1.
rewrite H1.
right; auto.
rewrite H1.
auto.
destruct H0.
destruct H1. 
destruct H1.
rewrite H0, H1; eauto.
rewrite H0, H1; eauto.
destruct IHexp1, IHexp2; try rewrite H0; try rewrite H1; auto.
induction (evalExpression_f e' exp1); auto.
case_eq (evalExpression_f e' exp2).
intros.
case_eq (Math.binary_operation b a v); intros.
assert (exists r, Math.binary_operation b a v = Some r) by (exists v0; auto).
apply binary_op_undefined_some_iff in H4.
destruct H4.
rewrite H4.
auto.
apply or_comm.
apply binary_op_undefined_some_r.
intros.
apply or_comm.
apply binary_op_undefined_some_r.
case_eq (evalExpression_f e' exp2); intros.
case_eq (evalExpression_f e' exp1); intros.
case_eq (Math.binary_operation b v0 v); intros.
assert (exists r, Math.binary_operation b v0 v = Some r) by (exists v1; auto).
apply binary_op_undefined_some_iff in H5.
destruct H5.
rewrite H6.
auto.
apply or_comm.
apply binary_op_undefined_some_l.
apply or_comm.
apply binary_op_undefined_some_l.
induction (evalExpression_f e' exp1); auto.
induction b; simpl; auto.
destruct IHexp; rewrite H0.
auto.
rewrite unary_op_undefined; auto.
Qed.

Theorem slim_projectMapGlobal : forall read e e',
  mapPartialView e e' ->
  mapPartialView (StringMapModule.map (fun v => match v with | ConstantValue va => ConstantValue va | VariableValue v => VariableValue Undefined end ) e ) (projectMapGlobal read e').
Proof.
unfold mapPartialView.
intros.
rewrite StringMapModule.gmap.
assert (Hid := H id).
destruct Hid.
rewrite H0.
case_eq (StringMapModule.get id e'); auto; intros.
induction c; eauto.
apply projectMapGlobal_dst_sc with (src := read) in H1.
rewrite H1; auto.
case_eq (StringMapModule.get id read); intros.
induction c.
eapply projectMapGlobal_src_sc with (dst := e') in H2.
rewrite H2.
rewrite H1; right; eauto.
assert (H2' := H2).
eapply projectMapGlobal_src_sv with (dst := e') in H2.
assert (exists v_dst : value, StringMapModule.get id e' = Some (VariableValue v_dst)) by (eexists; eauto).
apply H2 in H3.
rewrite H3.
rewrite H2'.
right; eauto.
assert (H2' := H2).
eapply projectMapGlobal_src_none with (dst := e') in H2.
rewrite H2.
rewrite H1.
right; eauto.
assert (H1' := H1).
eapply projectMapGlobal_dst_none with (src := read) in H1.
rewrite H1, H1'; auto.
destruct H0.
destruct H0, H0.
rewrite H0.
right; eauto.
eapply projectMapGlobal_dst_sv with (src := read) in H1; eauto.
destruct H1; eauto.
rewrite H0.
eapply projectMapGlobal_dst_sc with (src := read) in H1; eauto.
Qed.


Theorem slim_projectMapExternal: forall readlib slimlib lib,
  ( forall i, 
    StringMapModule.get i slimlib = StringMapModule.get i lib \/ 
    (StringMapModule.get i slimlib = Some (Environment.EVar (VariableValue Undefined)) 
      /\ 
    (StringMapModule.get i lib = None \/ exists v, StringMapModule.get i lib = Some (Environment.EVar (VariableValue v)))) \/ 
    (StringMapModule.get i slimlib = Some (Environment.EVar (ConstantValue Undefined)) /\
     exists v, StringMapModule.get i lib = Some (Environment.EVar (ConstantValue v))
    )
  )
    -> 
    ( forall i, 
    StringMapModule.get i (StringMapModule.map 
                (fun v => match v with | Environment.EVar (VariableValue v) => Environment.EVar (VariableValue Undefined) | r => r  end ) slimlib) = StringMapModule.get i (projectMapExternalVar readlib lib) \/ 
    (StringMapModule.get i (StringMapModule.map 
                (fun v => match v with | Environment.EVar (VariableValue v) => Environment.EVar (VariableValue Undefined) | r => r  end ) slimlib) = Some (Environment.EVar (VariableValue Undefined)) 
      /\ 
    (StringMapModule.get i (projectMapExternalVar readlib lib) = None \/ exists v, StringMapModule.get i (projectMapExternalVar readlib lib) = Some (Environment.EVar (VariableValue v)))) \/
    (StringMapModule.get i (StringMapModule.map 
                (fun v => match v with | Environment.EVar (VariableValue v) => Environment.EVar (VariableValue Undefined) | r => r  end ) slimlib) = Some (Environment.EVar (ConstantValue Undefined)) 
      /\ 
    exists v, StringMapModule.get i (projectMapExternalVar readlib lib) = Some (Environment.EVar (ConstantValue v))) ) 
    .
Proof.
intros.
rewrite StringMapModule.gmap.
assert (D := H i).
destruct D.
rewrite H0; auto.
case_eq (StringMapModule.get i lib); auto; intros.
induction e; auto.
induction c; auto.
apply projectMapExternalVar_dst_sc with (src := readlib) in H1; rewrite H1; auto.
apply projectMapExternalVar_dst_sv with (src := readlib) in H1.
destruct H1.
rewrite H1.
right; eauto.
apply projectMapExternalVar_dst_f with (src := readlib) in H1; rewrite H1; eauto.
apply projectMapExternalVar_dst_none with (src := readlib) in H1; rewrite H1; eauto.
destruct H0.
destruct H0.
rewrite H0.
destruct H1.
apply projectMapExternalVar_dst_none with (src := readlib) in H1; rewrite H1; eauto.
destruct H1.
apply projectMapExternalVar_dst_sv with (src := readlib) in H1.
destruct H1.
right; eauto.
destruct H0.
rewrite H0.
destruct H1.
apply projectMapExternalVar_dst_sc with (src := readlib) in H1.
rewrite H1.
right; eauto.
Qed.


Theorem slim_projectMapLib : forall slim read orig,
(forall id, 
    match StringMapModule.get id slim, StringMapModule.get id orig with
    | Some m', Some m => 
        forall i, 
          StringMapModule.get i m' = StringMapModule.get i m \/ 
          (StringMapModule.get i m' = Some (Environment.EVar (VariableValue Undefined)) /\ (StringMapModule.get i m = None \/ exists v, StringMapModule.get i m = Some (Environment.EVar (VariableValue v)))) \/
          (StringMapModule.get i m' = Some (Environment.EVar (ConstantValue Undefined)) /\ exists v, StringMapModule.get i m = Some (Environment.EVar (ConstantValue v)))
    
    | Some m', None => 
        forall i, 
          StringMapModule.get i m' = None \/ 
          StringMapModule.get i m' = Some (Environment.EVar (VariableValue Undefined))
    | None, None => True
    | _, _ => False
    end
  ) ->
(forall id, 
    match StringMapModule.get id (StringMapModule.map 
            (fun m => 
              StringMapModule.map 
                (fun v => match v with | Environment.EVar (VariableValue v) => Environment.EVar (VariableValue Undefined) | r => r  end ) m ) slim), StringMapModule.get id (projectMapLib read orig) with
    | Some m', Some m => 
        forall i, 
          StringMapModule.get i m' = StringMapModule.get i m \/ 
          (StringMapModule.get i m' = Some (Environment.EVar (VariableValue Undefined)) /\ (StringMapModule.get i m = None \/ exists v, StringMapModule.get i m = Some (Environment.EVar (VariableValue v)))) \/
          (StringMapModule.get i m' = Some (Environment.EVar (ConstantValue Undefined)) /\ exists v, StringMapModule.get i m = Some (Environment.EVar (ConstantValue v)))
    | Some m', None => 
        forall i, 
          StringMapModule.get i m' = None \/ 
          StringMapModule.get i m' = Some (Environment.EVar (VariableValue Undefined))
    | None, None => True
    | _, _ => False
    end
  ).
Proof.
induction slim; simpl.
intros.
assert (Hid := H id).
case_eq (StringMapModule.get id orig); intros; rewrite H0 in *; inversion Hid.
rewrite projectMapLib_dst_none.
auto.
auto.
induction a.
simpl.
intros.
assert (Hid := H id).
rewrite StringMapModule.gmap.
simpl.
case_eq (String_Equality.eq id a); intros; rewrite H0 in *; auto.
case_eq (StringMapModule.get id orig); intros; rewrite H1 in *; auto.
assert (H1' := H1).
apply projectMapLib_dst_some with (src := read) in H1.
destruct H1.
rewrite H1.
intros.
rewrite StringMapModule.gmap.
assert (Di := Hid i).
destruct Di.
rewrite H2.
induction (StringMapModule.get i t); auto.
induction a0; auto.
induction c; auto.
right; eauto.
destruct H2.
destruct H2.
destruct H3.
rewrite H2,H3; eauto.
destruct H3.
rewrite H2, H3; right; eauto.
destruct H2, H3.
rewrite H2, H3.
right; eauto.
destruct H1.
destruct H1.
rewrite H2.
apply slim_projectMapExternal; eauto.
assert (H1' := H1).
apply projectMapLib_dst_none with (src := read) in H1.
rewrite H1.
intros.
rewrite StringMapModule.gmap.
assert (Di := Hid i).
destruct Di.
rewrite H2; auto.
rewrite H2; auto.

case_eq (StringMapModule.get id slim); intros; rewrite H1 in *.
case_eq (StringMapModule.get id orig); intros; rewrite H2 in *.
assert (H2' := H2).
apply projectMapLib_dst_some with (src := read) in H2.
destruct H2.
rewrite H2.
intros.
rewrite StringMapModule.gmap.
assert (Di := Hid i).
destruct Di.
rewrite H3 in *; auto.
induction (StringMapModule.get i t0); auto.
induction a0; auto.
induction c; auto.
right; eauto.
destruct H3.
destruct H3.
destruct H4.
rewrite H3.
destruct H4.
right; eauto.
rewrite H3.
right; eauto.
destruct H3.
destruct H4.
rewrite H3, H4.
right; eauto.
destruct H2.
destruct H2.
rewrite H3.
eapply slim_projectMapExternal; eauto.
assert (H2' := H2).
apply projectMapLib_dst_none with (src := read) in H2.
rewrite H2.
intros.
rewrite StringMapModule.gmap.
assert (Di := Hid i).
destruct Di; rewrite H3; eauto.
case_eq (StringMapModule.get id orig); intros; rewrite H2 in Hid; inversion Hid.
apply projectMapLib_dst_none with (src := read) in H2.
rewrite H2.
auto.
Qed.

Fixpoint slimPartialViewcopyOut_f (sig: list (ident * mode * name)) (params: list expression_or_string_lit) env := match sig, params with
| [], [] => Some {| localVars := env.(localVars);
        globalVars := StringMapModule.map 
                (fun v => match v with | ConstantValue va => ConstantValue va | VariableValue v => VariableValue Undefined end ) env.(globalVars);
        funDefs := env.(funDefs);
        localfunDecls := env.(localfunDecls);
        externals := 
          StringMapModule.map 
            (fun m => 
              StringMapModule.map 
                (fun v => match v with | Environment.EVar (VariableValue v) => Environment.EVar (VariableValue Undefined) | r => r  end ) m )
          env.(externals)  |}
| ((idSig, Ast.In, IdTyp)::tSig), (eParam::tParams) => slimPartialViewcopyOut_f tSig tParams env
| ((idSig, Ast.InOut, IdTyp)::tSig), ((Exp (EVar eParam))::tParams) =>
    (
      match slimPartialViewcopyOut_f tSig tParams env with
      | Some r_env => forgetIdsValue eParam r_env
      | None => None
      end
    )
| ((idSig, Ast.Out, IdTyp)::tSig), ((Exp (EVar eParam))::tParams) =>
    (
      match slimPartialViewcopyOut_f tSig tParams env with
      | Some r_env => forgetIdsValue eParam r_env
      | None => None
      end
    )
| _, _ => None
end.

Theorem slimPartialViewcopyOut_correct : forall sig params lenv env lenv' env' f_env,
  envpartialView lenv env ->
  copyOut_f f_env sig params env = Some env' ->
  slimPartialViewcopyOut_f sig params lenv = Some lenv' ->
  envpartialView lenv' env'.
Proof.
induction sig.
induction params; simpl; unfold envpartialView.
intros.
inversion H0.
inversion H1.
simpl.
destruct H.
destruct H2.
destruct H5.
destruct H6.
repeat (split; auto).
apply slim_projectMapGlobal; auto.
apply slim_projectMapLib; auto.
intros.
inversion H0.
simpl.
induction a.
induction a.
induction b0; intros.
induction params; eauto.
inversion H0.
induction params; eauto.
inversion H0.
clear IHparams.
case_eq (copyOut_f f_env sig params env); intros; rewrite H2 in *.
case_eq (slimPartialViewcopyOut_f sig params lenv); intros; rewrite H3 in *.
case_eq (match getIdValue a f_env with
           | Some (Value (ConstantValue v)) | Some (Value (VariableValue v)) => Some v
           | None => Some Undefined
           | _ => None
           end); intros; rewrite H4 in *.
induction a0; [|inversion H0].
induction e1; [|inversion H0|inversion H0|inversion H0].
apply envpartialViewForgetVsSet with (ids := n) (e:=e0) (e':=e) (v:=v); auto.
eapply IHsig; eauto.
induction a0; inversion H0.
induction e1; inversion H0.
induction a0; inversion H1.
induction e0; inversion H1.
induction a0; inversion H0.
induction e; inversion H0.
induction params; eauto.
inversion H0.
clear IHparams.
case_eq (copyOut_f f_env sig params env); intros; rewrite H2 in *.
case_eq (slimPartialViewcopyOut_f sig params lenv); intros; rewrite H3 in *.
case_eq (match getIdValue a f_env with
           | Some (Value (ConstantValue v)) | Some (Value (VariableValue v)) => Some v
           | None => Some Undefined
           | _ => None
           end); intros; rewrite H4 in *.
induction a0; [|inversion H0].
induction e1; [|inversion H0|inversion H0|inversion H0].
apply envpartialViewForgetVsSet with (ids := n) (e:=e0) (e':=e) (v:=v); auto.
eapply IHsig; eauto.
induction a0; inversion H0.
induction e1; inversion H0.
induction a0; inversion H1.
induction e0; inversion H1.
induction a0; inversion H0.
induction e; inversion H0.
Qed.

Lemma mapPartialView_nil_l : forall l, mapPartialView [] l <-> l = [].
Proof.
induction l.
split; auto.
intros.
apply mapPartialView_refl.
split; intros.
unfold mapPartialView in H.
induction a.
assert (Ha := H a).
destruct Ha.
simpl in H0.
rewrite StringMapModule.Xeq_refl in H0.
inversion H0.
destruct H0.
destruct H0.
destruct H0.
simpl in H0.
inversion H0.
destruct H0.
simpl in H0.
inversion H0.
inversion H.
Qed. 

Lemma mapPartialView_projetGlob : forall lenv env lenv' env',
  mapPartialView lenv env -> 
  mapPartialView lenv' env' -> 
  mapPartialView (projectMapGlobal lenv lenv') (projectMapGlobal env env').
Proof.
unfold mapPartialView.
intros.
assert (Hid := H id).
assert (H0id := H0 id).
destruct Hid.
destruct H0id.
case_eq (StringMapModule.get id lenv); intros.
induction c.
assert (StringMapModule.get id env = Some (ConstantValue v)) by (rewrite H1 in *; auto).
apply projectMapGlobal_src_sc with (dst := lenv') in H3.
apply projectMapGlobal_src_sc with (dst := env') in H4.
rewrite H3, H4; auto.
case_eq (StringMapModule.get id lenv'); intros.
induction c.
assert (StringMapModule.get id env' = Some (ConstantValue v0)) by (rewrite H2 in *; auto).
apply projectMapGlobal_dst_sc with (src := lenv) in H4.
apply projectMapGlobal_dst_sc with (src := env) in H5.
rewrite H4, H5; auto.
assert (H3' := H3).
apply projectMapGlobal_src_sv with (dst := lenv') in H3.
assert (exists v_dst : value, StringMapModule.get id lenv' = Some (VariableValue v_dst)) by eauto.
apply H3 in H5.
rewrite H5.
rewrite H1 in H3'.
rewrite H2 in H4.
assert (H4' := H4).
apply projectMapGlobal_src_sv with (dst := env') in H3'.
assert (exists v_dst : value, StringMapModule.get id env' = Some (VariableValue v_dst)) by eauto.
apply H3' in H6.
rewrite H6.
auto.
assert (StringMapModule.get id env' = None) by (rewrite H2 in *; auto).
apply projectMapGlobal_dst_none with (src := lenv) in H4.
apply projectMapGlobal_dst_none with (src := env) in H5.
rewrite H4, H5; auto.
assert (StringMapModule.get id env = None) by (rewrite H1 in *; auto).
apply projectMapGlobal_src_none with (dst := lenv') in H3.
apply projectMapGlobal_src_none with (dst := env') in H4.
rewrite H4, H3; auto.
destruct H2.
destruct H2.
destruct H2.
case_eq (StringMapModule.get id lenv); intros.
induction c.
assert (StringMapModule.get id env = Some (ConstantValue v)) by (rewrite H1 in *; auto).
apply projectMapGlobal_src_sc with (dst := lenv') in H4; rewrite H4, H2.
apply projectMapGlobal_src_sc with (dst := env') in H5; rewrite H5, H3; eauto.
assert (StringMapModule.get id env = Some (VariableValue v)) by (rewrite H1 in *; auto).
eapply projectMapGlobal_sv_sv with (src:=lenv) (dst:=lenv') in H2; eauto; erewrite H2.
eapply projectMapGlobal_sv_sv with (src:=env) (dst:=env') in H3; eauto; erewrite H3.
assert (StringMapModule.get id env = None) by (rewrite H1 in *; auto).
apply projectMapGlobal_src_none with (dst := lenv') in H4.
apply projectMapGlobal_src_none with (dst := env') in H5.
rewrite H4, H5, H2, H3; eauto.
destruct H2.
apply projectMapGlobal_dst_sc with (src := lenv) in H2.
apply projectMapGlobal_dst_sc with (src := env) in H3.
rewrite H2, H3; eauto.
destruct H1.
destruct H0id.
destruct H1, H1.
case_eq (StringMapModule.get id lenv'); intros; rewrite H4 in *; apply eq_sym in H2.
induction c.
apply projectMapGlobal_dst_sc with (src := env) in H2; rewrite H2; eauto.
apply projectMapGlobal_dst_sc with (src := lenv) in H4; rewrite H4; eauto.
eapply projectMapGlobal_sv_sv with (src:=lenv) (dst:=lenv') in H1; eauto; erewrite H1.
eapply projectMapGlobal_sv_sv with (src:=env) (dst:=env') in H3; eauto; erewrite H3.
assert (H4':=H4).
assert (H2':=H2).
apply projectMapGlobal_dst_none with (src := lenv) in H4; rewrite H4', H4 in *.
apply projectMapGlobal_dst_none with (src := env) in H2; rewrite H2', H2 in *; auto.
apply projectMapGlobal_src_sc with (dst := lenv') in H1.
apply projectMapGlobal_src_sc with (dst := env') in H3.
rewrite H1, H3; eauto.
destruct H2.
destruct H1.
destruct H1.
destruct H2.
destruct H2.
eapply projectMapGlobal_sv_sv with (src:=lenv) (dst:=lenv') in H1; eauto; erewrite H1.
eapply projectMapGlobal_sv_sv with (src:=env) (dst:=env') in H3; eauto; erewrite H3.
destruct H2.
apply projectMapGlobal_dst_sc with (src := env) in H4; rewrite H4.
apply projectMapGlobal_dst_sc with (src := lenv) in H2; rewrite H2.
eauto.
destruct H1.
apply projectMapGlobal_src_sc with (dst := lenv') in H1.
apply projectMapGlobal_src_sc with (dst := env') in H3.
rewrite H1, H3; eauto.
Qed.

(*Lemma mapPartialView_projetExternal : forall m m' m1 m1',
  (
  forall i : String_Equality.t,
    StringMapModule.get i m' = StringMapModule.get i m \/
    StringMapModule.get i m' = Some (Environment.EVar (VariableValue Undefined)) /\
    (StringMapModule.get i m = None \/
     (exists v : value, StringMapModule.get i m = Some (Environment.EVar (VariableValue v)))) \/
    StringMapModule.get i m' = Some (Environment.EVar (ConstantValue Undefined)) /\
    (exists v : value, StringMapModule.get i m = Some (Environment.EVar (ConstantValue v)))
  ) -> 
  (
  forall i : String_Equality.t,
    StringMapModule.get i m1' = StringMapModule.get i m1 \/
    StringMapModule.get i m1' = Some (Environment.EVar (VariableValue Undefined)) /\
    (StringMapModule.get i m1 = None \/
     (exists v : value, StringMapModule.get i m1 = Some (Environment.EVar (VariableValue v)))) \/
    StringMapModule.get i m1' = Some (Environment.EVar (ConstantValue Undefined)) /\
    (exists v : value, StringMapModule.get i m1 = Some (Environment.EVar (ConstantValue v)))
  ) -> 
  (
  forall i : String_Equality.t,
    StringMapModule.get i (projectMapExternalVar m' m1') = StringMapModule.get i (projectMapExternalVar m m1) \/
    StringMapModule.get i (projectMapExternalVar m' m1') = Some (Environment.EVar (VariableValue Undefined)) /\
    (StringMapModule.get i (projectMapExternalVar m m1) = None \/
     (exists v : value, StringMapModule.get i (projectMapExternalVar m m1) = Some (Environment.EVar (VariableValue v)))) \/
    StringMapModule.get i (projectMapExternalVar m' m1') = Some (Environment.EVar (ConstantValue Undefined)) /\
    (exists v : value, StringMapModule.get i (projectMapExternalVar m m1) = Some (Environment.EVar (ConstantValue v)))
  ).
Proof.
intros.
assert (Hi := H i).
assert (H0i := H0 i).
destruct Hi.
destruct H0i.
case_eq (StringMapModule.get i m1); intros; rewrite H3 in *.
induction e.
induction c.
apply projectMapExternalVar_dst_sc with (src := m') in H2.
apply projectMapExternalVar_dst_sc with (src := m) in H3.
rewrite H2, H3; eauto.
case_eq (StringMapModule.get i m); intros; rewrite H4 in *.
induction e.
induction c.
apply projectMapExternalVar_src_sc with (dst := m1') in H1.
apply projectMapExternalVar_src_sc with (dst := m1) in H4.
rewrite H2, H3, H1, H4 in *; eauto.
eapply projectMapExternalVar_sv_sv with (src := m') (dst := m1') in H1; eauto.
eapply projectMapExternalVar_sv_sv with (src := m) (dst := m1) in H4; eauto.
rewrite H1, H4; eauto.
eapply projectMapExternalVar_src_f with (dst := m1') in H1; eauto.
eapply projectMapExternalVar_src_f with (dst := m1) in H4; eauto.
rewrite H1, H4; eauto.
eapply projectMapExternalVar_src_none with (dst := m1') in H1; eauto.
eapply projectMapExternalVar_src_none with (dst := m1) in H4; eauto.
rewrite H1, H4; eauto.
eapply projectMapExternalVar_dst_f with (src := m') in H2; eauto.
eapply projectMapExternalVar_dst_f with (src := m) in H3; eauto.
rewrite H2, H3; eauto.
eapply projectMapExternalVar_dst_none with (src := m') in H2; eauto.
eapply projectMapExternalVar_dst_none with (src := m) in H3; eauto.
rewrite H2, H3; eauto.
destruct H2.
destruct H2.
destruct H3.
eapply projectMapExternalVar_dst_none with (src := m) in H3; eauto.
rewrite H3.*)
  
  
  
(* Lemma mapPartialView_projetLib: forall lext lext' uext uext',
(forall id : String_Equality.t,
  match StringMapModule.get id lext with
  | Some m' =>
      match StringMapModule.get id lext' with
      | Some m =>
          forall i : String_Equality.t,
          StringMapModule.get i m' = StringMapModule.get i m \/
          StringMapModule.get i m' = Some (Environment.EVar (VariableValue Undefined)) /\
          (StringMapModule.get i m = None \/
           (exists v : value, StringMapModule.get i m = Some (Environment.EVar (VariableValue v)))) \/
          StringMapModule.get i m' = Some (Environment.EVar (ConstantValue Undefined)) /\
          (exists v : value, StringMapModule.get i m = Some (Environment.EVar (ConstantValue v)))
      | None =>
          forall i : String_Equality.t,
          StringMapModule.get i m' = None \/
          StringMapModule.get i m' = Some (Environment.EVar (VariableValue Undefined))
      end
  | None =>
      match StringMapModule.get id lext' with
      | Some _ => False
      | None => True
      end
  end
) ->
(forall id : String_Equality.t,
  match StringMapModule.get id uext with
  | Some m' =>
      match StringMapModule.get id uext' with
      | Some m =>
          forall i : String_Equality.t,
          StringMapModule.get i m' = StringMapModule.get i m \/
          StringMapModule.get i m' = Some (Environment.EVar (VariableValue Undefined)) /\
          (StringMapModule.get i m = None \/
           (exists v : value, StringMapModule.get i m = Some (Environment.EVar (VariableValue v)))) \/
          StringMapModule.get i m' = Some (Environment.EVar (ConstantValue Undefined)) /\
          (exists v : value, StringMapModule.get i m = Some (Environment.EVar (ConstantValue v)))
      | None =>
          forall i : String_Equality.t,
          StringMapModule.get i m' = None \/
          StringMapModule.get i m' = Some (Environment.EVar (VariableValue Undefined))
      end
  | None =>
      match StringMapModule.get id uext' with
      | Some _ => False
      | None => True
      end
  end
) ->
(
  forall id : String_Equality.t,
  match StringMapModule.get id (projectMapLib lext uext) with
  | Some m' =>
      match StringMapModule.get id (projectMapLib lext' uext') with
      | Some m =>
          forall i : String_Equality.t,
          StringMapModule.get i m' = StringMapModule.get i m \/
          StringMapModule.get i m' = Some (Environment.EVar (VariableValue Undefined)) /\
          (StringMapModule.get i m = None \/
           (exists v : value, StringMapModule.get i m = Some (Environment.EVar (VariableValue v)))) \/
          StringMapModule.get i m' = Some (Environment.EVar (ConstantValue Undefined)) /\
          (exists v : value, StringMapModule.get i m = Some (Environment.EVar (ConstantValue v)))
      | None =>
          forall i : String_Equality.t,
          StringMapModule.get i m' = None \/
          StringMapModule.get i m' = Some (Environment.EVar (VariableValue Undefined))
      end
  | None =>
      match StringMapModule.get id (projectMapLib lext' uext') with
      | Some _ => False
      | None => True
      end
  end
).
Proof.
intros.
assert (Hid := H id).
assert (H0id := H0 id).
case_eq (StringMapModule.get id uext); intros; rewrite H1 in *.
case_eq (StringMapModule.get id uext'); intros; rewrite H2 in *.
assert (H1' := H1).
assert (H2' := H2).
apply projectMapLib_dst_some with (src := lext) in H1.
apply projectMapLib_dst_some with (src := lext') in H2.
destruct H1.
destruct H2.
rewrite H1, H2 in *; eauto.
destruct H2.
destruct H2.
rewrite H1, H2, H3 in *.
assert (exists x, StringMapModule.get id lext = Some x) by (induction (StringMapModule.get id lext); eauto; inversion Hid).
destruct H4.
rewrite H4 in *.
intros. *)
(* 
Theorem slimPartialViewcopyOut'_correct : forall sig params simf_env f_env sim_env env sim_out out,
  envpartialView simf_env f_env ->
  envpartialView sim_env env ->
  copyOut_f simf_env sig params sim_env = Some sim_out ->
  copyOut_f f_env sig params env = Some out ->
  envpartialView sim_out out.
Proof.
induction sig.
induction params; simpl; unfold envpartialView.
intros.
inversion H1.
inversion H2.
simpl.
destruct H, H0.
destruct H3, H6.
destruct H7, H8.
destruct H9, H10.
repeat (split; eauto).
apply mapPartialView_projetGlob; auto.
apply slim_projectMapLib; auto. *)



(* Fixpoint slimPartialViewcopyOutExternalWithSig_f (sig: list (ident * mode * name)) (params: list expression_or_string_lit) env := match sig, params with
| [], [] => Some {| localVars := env.(localVars);
        globalVars := env.(globalVars);
        funDefs := env.(funDefs);
        localfunDecls := env.(localfunDecls);
        externals := 
          StringMapModule.map 
            (fun m => 
              StringMapModule.map 
                (fun v => match v with | Environment.EVar (VariableValue v) => Environment.EVar (VariableValue Undefined) | r => r  end ) m )
          env.(externals)  |}
| ((idSig, Ast.In, IdTyp)::tSig), (eParam::tParams) => slimPartialViewcopyOutExternalWithSig_f tSig tParams env
| ((idSig, Ast.InOut, IdTyp)::tSig), ((Exp (EVar eParam))::tParams) =>
    (
      match slimPartialViewcopyOutExternalWithSig_f tSig tParams env with
      | Some r_env => forgetIdsValue eParam r_env
      | None => None
      end
    )
| ((idSig, Ast.Out, IdTyp)::tSig), ((Exp (EVar eParam))::tParams) =>
    (
      match slimPartialViewcopyOutExternalWithSig_f tSig tParams env with
      | Some r_env => forgetIdsValue eParam r_env
      | None => None
      end
    )
| _, _ => None
end.

Theorem slimPartialViewcopyOutExternalWithSig_correct : forall sig params lenv env lenv' env' f_env,
  envpartialView lenv env ->
  copyOutExternalWithSig_f f_env sig params env = Some env' ->
  slimPartialViewcopyOutExternalWithSig_f sig params lenv = Some lenv' ->
  envpartialView lenv' env'.
Proof.
induction sig.
induction params; simpl; unfold envpartialView.
intros.
inversion H0.
inversion H1.
simpl.
destruct H.
destruct H2.
destruct H5.
destruct H6.
repeat (split; auto).
apply slim_projectMapMap; auto.
intros.
inversion H0.
simpl.
induction a.
induction a.
induction b0; intros.
induction params; eauto.
inversion H0.
induction params; eauto.
inversion H0.
clear IHparams.
case_eq (copyOutExternalWithSig_f f_env sig params env); intros; rewrite H2 in *.
case_eq (slimPartialViewcopyOutExternalWithSig_f sig params lenv); intros; rewrite H3 in *.
case_eq (match getIdValue a f_env with
           | Some (Value (ConstantValue v)) | Some (Value (VariableValue v)) => Some v
           | None => Some Undefined
           | _ => None
           end); intros; rewrite H4 in *.
induction a0; [|inversion H0].
induction e1; [|inversion H0|inversion H0|inversion H0].
apply envpartialViewForgetVsSet with (ids := n) (e:=e0) (e':=e) (v:=v); auto.
eapply IHsig; eauto.
induction a0; inversion H0.
induction e1; inversion H0.
induction a0; inversion H1.
induction e0; inversion H1.
induction a0; inversion H0.
induction e; inversion H0.
induction params; eauto.
inversion H0.
clear IHparams.
case_eq (copyOutExternalWithSig_f f_env sig params env); intros; rewrite H2 in *.
case_eq (slimPartialViewcopyOutExternalWithSig_f sig params lenv); intros; rewrite H3 in *.
case_eq (match getIdValue a f_env with
           | Some (Value (ConstantValue v)) | Some (Value (VariableValue v)) => Some v
           | None => Some Undefined
           | _ => None
           end); intros; rewrite H4 in *.
induction a0; [|inversion H0].
induction e1; [|inversion H0|inversion H0|inversion H0].
apply envpartialViewForgetVsSet with (ids := n) (e:=e0) (e':=e) (v:=v); auto.
eapply IHsig; eauto.
induction a0; inversion H0.
induction e1; inversion H0.
induction a0; inversion H1.
induction e0; inversion H1.
induction a0; inversion H0.
induction e; inversion H0.
Qed. *)

Fixpoint slimPartialViewcopyOutExternalWithoutSig_f (params: list expression_or_string_lit) env := match params with
| [] => Some {| localVars := env.(localVars);
        globalVars := StringMapModule.map 
                (fun v => match v with | ConstantValue va => ConstantValue va | VariableValue v => VariableValue Undefined end ) env.(globalVars);
        funDefs := env.(funDefs);
        localfunDecls := env.(localfunDecls);
        externals := 
          StringMapModule.map 
            (fun m => 
              StringMapModule.map 
                (fun v => match v with | Environment.EVar (VariableValue v) => Environment.EVar (VariableValue Undefined) | r => r  end ) m )
          env.(externals)  |}

| ((Exp (EVar eParam))::tParams) =>
    (
      match slimPartialViewcopyOutExternalWithoutSig_f tParams env with
      | Some r_env => forgetIdsValue eParam r_env
      | None => None
      end
    )
| (_::tParams) => slimPartialViewcopyOutExternalWithoutSig_f tParams env
end.

Theorem slimPartialViewcopyOutExternalWithoutSig_correct : forall params lenv env lenv' env' f_env,
  envpartialView lenv env ->
  copyOutExternalWithoutSig_f f_env params env = Some env' ->
  slimPartialViewcopyOutExternalWithoutSig_f params lenv = Some lenv' ->
  envpartialView lenv' env'.
Proof.
induction params; simpl.
unfold envpartialView.
intros.
inversion H0.
inversion H1.
simpl.
destruct H.
destruct H2.
destruct H5.
destruct H6.
repeat (split; auto).
apply slim_projectMapGlobal; auto.
apply slim_projectMapLib; auto.
intros.
case_eq (slimPartialViewcopyOutExternalWithoutSig_f params lenv); intros; rewrite H2 in *.
case_eq (copyOutExternalWithoutSig_f f_env params env); intros; rewrite H3 in *.
induction a.
induction e1.
induction (match getIdsValue n f_env with
       | Some (Value (ConstantValue v)) | Some (Value (VariableValue v)) => Some v
       | None => Some Undefined
       | _ => None
       end); [|inversion H0].
apply envpartialViewForgetVsSet with (ids := n) (e:=e) (e':=e0) (v:=a); auto.
eauto.
inversion H0.
inversion H1.
subst.
eauto.
clear IHe1_1.
clear IHe1_2.
inversion H0.
inversion H1.
subst.
eauto.
inversion H0.
inversion H1.
subst.
eauto.
inversion H0.
inversion H1.
subst.
eauto.
induction a; inversion H0.
induction e0; inversion H0.
induction a; inversion H1.
induction e; inversion H1.
Qed.

Definition slimPartialViewexecFunction env (f: element_declararif) (params: association_d_arguments) := 
    slimPartialViewcopyOut_f f.(f_args) params env.

Theorem slimPartialViewexecFunction_correct : forall e in_data f params res slim_e,
  execFunction in_data f params res ->
  extractErrorType res = None ->
  envpartialView e (fst in_data) ->
  slimPartialViewexecFunction e f params = Some slim_e ->
  envpartialView slim_e (extractEnv res).
Proof.
induction 1; intros.
simpl.
eapply slimPartialViewcopyOut_correct with (f_env:=af_env); eauto.
simpl.
apply copyOut_f_correct; auto.
simpl in H2.
inversion H2.
Qed.

Definition slimSetIdsValue ids v env := match ids with
| [] => Some env
| [id] => setIdValue id v env
| pid::id::[] => 
    (
      match StringMapModule.get pid env.(localVars) with
      | None => 
          (
            match StringMapModule.get pid env.(globalVars) with
            | None =>
              (
                match StringMapModule.get pid env.(funDefs) with
                | None =>
                  (
                      match StringMapModule.get pid env.(localfunDecls) with
                      | None =>
                        (
                          match StringMapModule.get pid env.(externals) with
                          | Some lib => 
                            (
                              match StringMapModule.get id lib with
                              | Some (Environment.EVar (VariableValue _)) => 
                                  Some ({|
                                    localVars := env.(localVars);
                                    globalVars := env.(globalVars);
                                    localfunDecls := env.(localfunDecls);
                                    funDefs := env.(funDefs);
                                    externals := StringMapModule.set pid (StringMapModule.set id (Environment.EVar (VariableValue v)) lib) env.(externals)
                                  |}) 
                              | _ => forgetIdsValue ids env
                              end
                            )
                          | _ => forgetIdsValue ids env
                          end
                        )
                     | _ => forgetIdsValue ids env
                     end
                  )
                | _ => forgetIdsValue ids env
                end
              )
            | _ => forgetIdsValue ids env
            end
          )
        | _ => forgetIdsValue ids env
       end
    )
| _ => forgetIdsValue ids env
end.

Theorem slimSetIdsValue_correct : forall ids v e e' env env',
  envpartialView e e' ->
  setIdsValue ids v e' = Some env' ->
  slimSetIdsValue ids v e = Some env ->
  envpartialView env env'.
Proof.
intro.
case_eq ids.
simpl; intros.
inversion H1.
intros t l.
case_eq l.
simpl.
intros.
eapply envpartialViewSetIdValue_l2b; eauto.
intros.
subst.
case_eq l0; intros.
subst.
case_eq (StringMapModule.get t (localVars e)); intros.
assert (slimSetIdsValue [t; t0] v e = forgetIdsValue [t; t0] e) by (simpl; rewrite H; auto).
rewrite H0 in H3.
eapply envpartialViewForgetVsSet; eauto.
case_eq (StringMapModule.get t (globalVars e)); intros.
assert (slimSetIdsValue [t; t0] v e = forgetIdsValue [t; t0] e) by (simpl; rewrite H, H0; auto).
rewrite H4 in H3.
eapply envpartialViewForgetVsSet; eauto.
case_eq (StringMapModule.get t (funDefs e)); intros.
assert (slimSetIdsValue [t; t0] v e = forgetIdsValue [t; t0] e) by (simpl; rewrite H, H0, H4; auto).
rewrite H5 in H3.
eapply envpartialViewForgetVsSet; eauto.
case_eq (StringMapModule.get t (localfunDecls e)); intros.
assert (slimSetIdsValue [t; t0] v e = forgetIdsValue [t; t0] e) by (simpl; rewrite H, H0, H4, H5; auto).
rewrite H6 in H3.
eapply envpartialViewForgetVsSet; eauto.
case_eq (StringMapModule.get t (externals e)); intros.
case_eq (StringMapModule.get t0 t1); intros.
induction e0.
induction c.
assert (slimSetIdsValue [t; t0] v e = forgetIdsValue [t; t0] e) by (simpl; rewrite H, H0, H4, H5, H6, H7; auto).
rewrite H8 in H3; eapply envpartialViewForgetVsSet; eauto.
simpl in *.
rewrite H, H0, H4, H5, H6, H7 in H3.
inversion H3; subst.
destruct H1.
destruct H8.
destruct H9.
destruct H10.
rewrite H9, H10 in *.
apply mapPartialView_getNone_l2b with (bigger := localVars e') in H; auto.
apply mapPartialView_getNone_l2b with (bigger := globalVars e') in H0; auto.
rewrite H0, H, H4, H5 in H2.
assert (D := H11 t).
rewrite H6 in D.
case_eq (StringMapModule.get t (externals e')); intros; rewrite H12 in *.
assert (Dt0 := D t0).
destruct Dt0.
apply eq_sym in H13.
rewrite H7 in H13.
rewrite H13 in H2.
inversion H2; subst.
repeat (split; auto).
intros; simpl.
rewrite StringMapModule.gsspec.
rewrite StringMapModule.gsspec.
case_eq (String_Equality.eq id t); intros; eauto.
rewrite StringMapModule.gsspec.
rewrite StringMapModule.gsspec.
case_eq (String_Equality.eq i t0); intros; eauto.
apply H11.
destruct H13.
destruct H13.
destruct H14.
rewrite H14 in H2.
inversion H2; subst.
simpl.
repeat (split; auto).
intros.
simpl.
rewrite StringMapModule.gsspec.
rewrite StringMapModule.gsspec.
case_eq (String_Equality.eq id t); intros; eauto.
rewrite StringMapModule.gsspec.
rewrite StringMapModule.gsspec.
case_eq (String_Equality.eq i t0); intros; eauto.
apply H11.
destruct H14.
rewrite H14 in H2.
inversion H2; subst.
simpl.
repeat (split; auto).
intros.
simpl.
rewrite StringMapModule.gsspec.
rewrite StringMapModule.gsspec.
case_eq (String_Equality.eq id t); intros; eauto.
rewrite StringMapModule.gsspec.
rewrite StringMapModule.gsspec.
case_eq (String_Equality.eq i t0); intros; eauto.
apply H11.
destruct H13.
rewrite H13 in H7; inversion H7.
inversion H2; subst.
simpl.
assert (Dt0 := D t0).
destruct Dt0; rewrite H7 in H13; inversion H13; subst.
repeat (split; auto).
intros.
simpl.
rewrite StringMapModule.gsspec.
simpl.
rewrite StringMapModule.gsspec.
simpl.
case_eq (String_Equality.eq id t); intros; eauto.
rewrite StringMapModule.gsspec.
simpl.
case_eq (String_Equality.eq i t0); intros; eauto.
assert (Di := D i).
destruct Di; eauto.
apply H11.
assert (slimSetIdsValue [t; t0] v e = forgetIdsValue [t; t0] e) by (simpl; rewrite H, H0, H4, H5, H6, H7; auto).
rewrite H8 in H3; eapply envpartialViewForgetVsSet; eauto.
assert (slimSetIdsValue [t; t0] v e = forgetIdsValue [t; t0] e) by (simpl; rewrite H, H0, H4, H5, H6, H7; auto).
rewrite H8 in H3; eapply envpartialViewForgetVsSet; eauto.
assert (slimSetIdsValue [t; t0] v e = forgetIdsValue [t; t0] e) by (simpl; rewrite H, H0, H4, H5, H6; auto).
rewrite H7 in H3; eapply envpartialViewForgetVsSet; eauto.
subst.
eapply envpartialViewForgetVsSet; eauto.
Qed.

Definition slimPartialViewevalFunCall env f := match f with
| ICallDefinedFunction f_name None => 
    (
      match getIdsValue f_name env with
      | Some (DefinedFunction fn) => 
          slimPartialViewexecFunction env fn []
      | Some (FunctionDeclaration sig) => 
          slimPartialViewcopyOut_f sig [] env
      | _ => 
          slimPartialViewcopyOutExternalWithoutSig_f [] env
      end
    )
| ICallDefinedFunction f_name (Some p) => 
    (
      match getIdsValue f_name env with
      | Some (DefinedFunction fn) => 
          slimPartialViewexecFunction env fn p
      | Some (FunctionDeclaration sig) => 
          slimPartialViewcopyOut_f sig p env
      | _ => 
          slimPartialViewcopyOutExternalWithoutSig_f p env
      end
    )
| (SOInstructionCompareM status m0 m1 t) => forgetIdsValue status env
| (SOInstructionCompareR status e1 e2 e3 e4 e5 e6 t) => forgetIdsValue status env
| (SOInstructionInit var (Some e)) => 
    (
      match evalExpression_f env (EVar var), evalExpression_f env e with
      | Some (Undefined), _ => forgetIdsValue var env
      | _, Some (Undefined) => forgetIdsValue var env
      | Some t, Some v => slimSetIdsValue var (initValue t v) env
      | _, _ => forgetIdsValue var env
      end
    )
| (SOInstructionReadHardInput t e n_config no_t) => 
    (
      forgetIdsValue t env
    )
    
| (SOInstructionReadMess t valide e no_msg no_t no_fic no_bx) =>
    (
      match forgetIdsValue t env with
      | Some env' => forgetIdsValue valide env'
      | None => None
      end
    )
| (SOInstructionReadMessUnfil t valide e no_msg no_fic no_bx) =>
    (
      match forgetIdsValue t env with
      | Some env' => forgetIdsValue valide env'
      | None => None
      end
    )
| (SOInstructionWrite t i x None) => 
    (
      match evalExpression_f env (EVar t), evalExpression_f env (EVar i), evalExpression_f env (EVar x) with
      | Some (Undefined), _, _ => forgetIdsValue t env
      | _, _, Some (Undefined) => forgetIdsValue t env
      | Some vt, Some (Int vi), Some vx => 
          (
            match idxs_set_from_value_f vt [(Z.to_nat vi)] vx with
            | Some r => slimSetIdsValue t r env
            | None => forgetIdsValue t env
            end
          )
      | _, _, _ => forgetIdsValue t env
      end
    )
| (SOInstructionWrite t i j (Some x)) => 
    (
      match evalExpression_f env (EVar t), evalExpression_f env (EVar i), evalExpression_f env (EVar j), evalExpression_f env (EVar x) with
      | Some (Undefined), _, _, _ => forgetIdsValue t env
      | _, _, _, Some (Undefined) => forgetIdsValue t env
      | Some vt, Some (Int vi), Some (Int vj), Some vx => 
          (
            match idxs_set_from_value_f vt [(Z.to_nat vi); (Z.to_nat vj)] vx with
            | Some r => slimSetIdsValue t r env
            | None => forgetIdsValue t env
            end
          )
      | _, _, _, _ => forgetIdsValue t env
      end
    )
| (SOInstructionWriteHardOutput s t no_config no_t)  => 
    (
      forgetIdsValue s env
    )
| (SOInstructionWriteMess s t no_msg no_fic no_bx) =>  
    (
      forgetIdsValue s env
    )
| (SOInstructionWriteOutvar z x no_fic no_bx no_t) =>  
    (
      match evalExpression_f env (EVar x) with
      | Some (Undefined) => forgetIdsValue z env
      | Some v => slimSetIdsValue z v env
      | _ => forgetIdsValue z env
      end
    )
| (SOInstructionWriteRedund r t no_fic no_bx no_t) =>  
    (
      forgetIdsValue r env
    )
| _ => Some env
end.

(* Theorem slimPartialViewevalFunCall_correct : forall e in_data f res slim_e,
  evalFunCall in_data f res ->
  extractErrorType res = None ->
  envpartialView e (fst in_data) ->
  slimPartialViewevalFunCall e f = Some slim_e ->
  envpartialView slim_e (extractEnv res).
Proof.
induction 1; intros.
eapply slimPartialViewexecFunction_correct; eauto.
unfold slimPartialViewevalFunCall in H3.
simpl in H2.
assert (D:= envpartialViewGetIdsSome f_name e b_env H2).
destruct D.
rewrite H4 in *.
rewrite H in H3.
auto.
destruct H4.
destruct H4.
destruct H5.
destruct H5.
rewrite H5 in H.
inversion H.
rewrite H5 in H.
inversion H.
destruct H4.
destruct H4.
destruct H5.
destruct H4.
rewrite H4 in H.
inversion H.
rewrite H4 in H.
inversion H.
eapply slimPartialViewexecFunction_correct; eauto.
unfold slimPartialViewevalFunCall in H3.
simpl in H2.
assert (D:= envpartialViewGetIdsSome f_name e b_env H2).
destruct D.
rewrite H4 in *.
rewrite H in H3.
auto.
destruct H4.
destruct H4.
destruct H5.
destruct H5.
rewrite H5 in H.
inversion H.
rewrite H5 in H.
inversion H.
destruct H4.
destruct H4.
destruct H5.
destruct H4.
rewrite H4 in H.
inversion H.
rewrite H4 in H.
inversion H.
simpl.
unfold slimPartialViewevalFunCall in H4.
simpl in H4.
assert (D:= envpartialViewGetIdsSome f_name e b_env H3).
destruct D.
rewrite H5 in *.
rewrite H in H4.
rewrite copyOut_f_correct in H1.
eapply slimPartialViewcopyOut_correct with (f_env := a_env); eauto.
destruct H5, H5.
destruct H6.
destruct H6.
rewrite H6 in H; inversion H.
rewrite H6 in H; inversion H.
destruct H6.
destruct H6.
rewrite H6 in H; inversion H.
rewrite H6 in H; inversion H.
simpl in H1; inversion H1.
simpl.
simpl in H3.
unfold slimPartialViewevalFunCall in H4.
assert (D:= envpartialViewGetIdsSome f_name e b_env H3).
destruct D.
rewrite H5 in *.
rewrite H in H4.
rewrite copyOut_f_correct in H1.
eapply slimPartialViewcopyOut_correct with (f_env := a_env); eauto.
destruct H5, H5.
destruct H6.
destruct H6.
rewrite H6 in H; inversion H.
rewrite H6 in H; inversion H.
destruct H6.
destruct H6.
rewrite H6 in H; inversion H.
rewrite H6 in H; inversion H.
simpl in H1; inversion H1.
simpl.
unfold slimPartialViewevalFunCall in H4.
simpl in H3.
assert (D:= envpartialViewGetIdsSome f_name e b_env H3).
destruct D.
rewrite H5 in *.
rewrite H in H4.
eapply slimPartialViewcopyOutExternalWithoutSig_correct with (f_env := a_env); eauto.
eapply copyOutExternalWithoutSig_f_correct; eauto.
destruct H5, H5.
destruct H6.
destruct H6.
rewrite H6 in H; inversion H.
rewrite H5 in H4.
eapply slimPartialViewcopyOutExternalWithoutSig_correct with (f_env := a_env); eauto.
eapply copyOutExternalWithoutSig_f_correct; eauto.
destruct H6.
destruct H6.
rewrite H6 in H; inversion H.
rewrite H5 in H4.
eapply slimPartialViewcopyOutExternalWithoutSig_correct with (f_env := a_env); eauto.
eapply copyOutExternalWithoutSig_f_correct; eauto.
simpl in H1; inversion H1.
simpl.
unfold slimPartialViewevalFunCall in H4.
simpl in H3.
assert (D:= envpartialViewGetIdsSome f_name e b_env H3).
destruct D.
rewrite H5 in *.
rewrite H in H4.
eapply slimPartialViewcopyOutExternalWithoutSig_correct with (f_env := a_env); eauto.
eapply copyOutExternalWithoutSig_f_correct; eauto.
destruct H5, H5.
destruct H6.
destruct H6.
rewrite H6 in H; inversion H.
rewrite H5 in H4.
eapply slimPartialViewcopyOutExternalWithoutSig_correct with (f_env := a_env); eauto.
eapply copyOutExternalWithoutSig_f_correct; eauto.
destruct H6.
destruct H6.
rewrite H6 in H; inversion H.
rewrite H5 in H4.
eapply slimPartialViewcopyOutExternalWithoutSig_correct with (f_env := a_env); eauto.
eapply copyOutExternalWithoutSig_f_correct; eauto.
simpl in H1; inversion H1.
simpl in *; inversion H1; subst; auto.
simpl in *; inversion H1; subst; auto.
simpl in *; inversion H1; subst; auto.
simpl in H1; inversion H1; subst; auto.
simpl in H1; inversion H1; subst; auto.
eapply envpartialViewForgetVsSet; eauto.
eapply envpartialViewForgetVsSet; eauto.
inversion H.
simpl in *; inversion H1; subst; auto.
simpl.
unfold slimPartialViewevalFunCall in H4.
apply evalExpression_f_correct in H.
apply evalExpression_f_correct in H0.
assert (D := envpartialViewEvalExpression e b_env e0 H3).
assert (D' := envpartialViewEvalExpression e b_env (EVar var) H3).
destruct D; destruct D'; try rewrite H5, H6 in *; try rewrite H, H0 in *.
induction vvar.
eapply envpartialViewForgetVsSet; eauto.
induction v.
eapply envpartialViewForgetVsSet; eauto.
eapply slimSetIdsValue_correct; eauto.
eapply slimSetIdsValue_correct; eauto.
eapply slimSetIdsValue_correct; eauto.
eapply slimSetIdsValue_correct; eauto.
eapply slimSetIdsValue_correct; eauto.
induction v.
eapply envpartialViewForgetVsSet; eauto.
eapply slimSetIdsValue_correct; eauto.
eapply slimSetIdsValue_correct; eauto.
eapply slimSetIdsValue_correct; eauto.
eapply slimSetIdsValue_correct; eauto.
eapply slimSetIdsValue_correct; eauto.
induction v.
eapply envpartialViewForgetVsSet; eauto.
eapply slimSetIdsValue_correct; eauto.
eapply slimSetIdsValue_correct; eauto.
eapply slimSetIdsValue_correct; eauto.
eapply slimSetIdsValue_correct; eauto.
eapply slimSetIdsValue_correct; eauto.
induction v.
eapply envpartialViewForgetVsSet; eauto.
eapply slimSetIdsValue_correct; eauto.
eapply slimSetIdsValue_correct; eauto.
eapply slimSetIdsValue_correct; eauto.
eapply slimSetIdsValue_correct; eauto.
eapply slimSetIdsValue_correct; eauto.
induction v.
eapply envpartialViewForgetVsSet; eauto.
eapply slimSetIdsValue_correct; eauto.
eapply slimSetIdsValue_correct; eauto.
eapply slimSetIdsValue_correct; eauto.
eapply slimSetIdsValue_correct; eauto.
eapply slimSetIdsValue_correct; eauto.
eapply envpartialViewForgetVsSet; eauto.
rewrite H0 in H4.
induction vvar; eapply envpartialViewForgetVsSet; eauto.
eapply envpartialViewForgetVsSet; eauto.
unfold slimPartialViewevalFunCall in H1; inversion H1; simpl in H0; subst; auto.
unfold slimPartialViewevalFunCall in H1; inversion H1; simpl in H0; subst; auto.
unfold slimPartialViewevalFunCall in H1; inversion H1; simpl in H0; subst; auto.
unfold slimPartialViewevalFunCall in H5; eapply envpartialViewForgetVsSet; eauto.
unfold slimPartialViewevalFunCall in H4.
case_eq (forgetIdsValue t e); intros; rewrite H5 in *; [|inversion H4].
simpl.
assert (D := envpartialViewForgetVsSet t e b_env v e1 a_env' H3 H0 H5).
eapply envpartialViewForgetVsSet; eauto.
unfold slimPartialViewevalFunCall in H3.
case_eq (forgetIdsValue t e); intros; rewrite H4 in *; [|inversion H3].
assert (D := envpartialViewForgetVsNothing t e b_env e1 H2 H4).
eapply envpartialViewForgetVsSet; eauto.
unfold slimPartialViewevalFunCall in H4.
case_eq (forgetIdsValue t e); intros; rewrite H5 in *; [|inversion H4].
simpl.
assert (D := envpartialViewForgetVsSet t e b_env v e1 a_env' H3 H0 H5).
eapply envpartialViewForgetVsSet; eauto.
unfold slimPartialViewevalFunCall in H3.
case_eq (forgetIdsValue t e); intros; rewrite H4 in *; [|inversion H3].
assert (D := envpartialViewForgetVsNothing t e b_env e1 H2 H4).
eapply envpartialViewForgetVsSet; eauto.
assert (D := envpartialViewEvalExpression e b_env (EVar t) H5).
assert (D' := envpartialViewEvalExpression e b_env (EVar i) H5).
assert (D'' := envpartialViewEvalExpression e b_env (EVar x) H5).
rewrite evalExpression_f_correct in *.
unfold slimPartialViewevalFunCall in H6.
destruct D; destruct D'; destruct D''; try rewrite H7 in *; try rewrite H8 in *; try rewrite H9 in *; try rewrite H in *; try rewrite H0 in *; try rewrite H1 in *; rewrite idxs_set_from_value_f_correct in *; try rewrite H2 in H6.
assert (slimSetIdsValue t vt' e = Some slim_e \/ forgetIdsValue t e = Some slim_e).
induction vt, vx; auto.
destruct H10.
eapply slimSetIdsValue_correct; eauto.
eapply envpartialViewForgetVsSet; eauto.
induction vt; eapply envpartialViewForgetVsSet; eauto.
induction vt, vx; eapply envpartialViewForgetVsSet; eauto.
induction vt; eapply envpartialViewForgetVsSet; eauto.
eapply envpartialViewForgetVsSet; eauto.
eapply envpartialViewForgetVsSet; eauto.
eapply envpartialViewForgetVsSet; eauto.
eapply envpartialViewForgetVsSet; eauto.
assert (D := envpartialViewEvalExpression e b_env (EVar t) H6).
assert (D' := envpartialViewEvalExpression e b_env (EVar i) H6).
assert (D'' := envpartialViewEvalExpression e b_env (EVar j) H6).
assert (D''' := envpartialViewEvalExpression e b_env (EVar x) H6).
rewrite evalExpression_f_correct in *.
unfold slimPartialViewevalFunCall in H7.
destruct D; destruct D'; destruct D''; destruct D'''; 
try rewrite H8 in *; try rewrite H9 in *; try rewrite H10 in *; try rewrite H11 in *; 
try rewrite H in *; try rewrite H0 in *; try rewrite H1 in *; try rewrite H2 in *; 
rewrite idxs_set_from_value_f_correct in *; try rewrite H3 in H7.
assert (slimSetIdsValue t vt' e = Some slim_e \/ forgetIdsValue t e = Some slim_e).
induction vt, vx; auto.
destruct H12.
eapply slimSetIdsValue_correct; eauto.
eapply envpartialViewForgetVsSet; eauto.
induction vt; eapply envpartialViewForgetVsSet; eauto.
induction vt, vx; eapply envpartialViewForgetVsSet; eauto.
induction vt; eapply envpartialViewForgetVsSet; eauto.
induction vt, vx; eapply envpartialViewForgetVsSet; eauto.
induction vt; eapply envpartialViewForgetVsSet; eauto.
induction vt, vx; eapply envpartialViewForgetVsSet; eauto.
induction vt; eapply envpartialViewForgetVsSet; eauto.
eapply envpartialViewForgetVsSet; eauto.
eapply envpartialViewForgetVsSet; eauto.
eapply envpartialViewForgetVsSet; eauto.
eapply envpartialViewForgetVsSet; eauto.
eapply envpartialViewForgetVsSet; eauto.
eapply envpartialViewForgetVsSet; eauto.
eapply envpartialViewForgetVsSet; eauto.
eapply envpartialViewForgetVsSet; eauto.
unfold slimPartialViewevalFunCall in H3.
eapply envpartialViewForgetVsSet; eauto.
eapply envpartialViewForgetVsSet; eauto.
unfold slimPartialViewevalFunCall in H3.
rewrite evalExpression_f_correct in *.
assert (D := envpartialViewEvalExpression e b_env (EVar x) H2).
destruct D; rewrite H4 in *.
rewrite H in *.
induction v; simpl.
eapply envpartialViewForgetVsSet; eauto.
eapply slimSetIdsValue_correct; eauto.
eapply slimSetIdsValue_correct; eauto.
eapply slimSetIdsValue_correct; eauto.
eapply slimSetIdsValue_correct; eauto.
eapply slimSetIdsValue_correct; eauto.
eapply envpartialViewForgetVsSet; eauto.
unfold slimPartialViewevalFunCall in H3.
eapply envpartialViewForgetVsSet; eauto.
Qed. *)

(*
Forget List
*)

Definition isIdsforgotten ids env := match ids with
| [] => false
| hid::[] => 
    (
      match getIdValue hid env with
      | Some (Value (VariableValue Undefined)) => true
      | Some (Value (ConstantValue _)) => true
      | _ => false
      end
    )
| hid::hid0::tid0 => 
    (
      match StringMapModule.get hid env.(localVars) with
      | None => 
        (
          match StringMapModule.get hid env.(globalVars) with
          | None =>
            (
              match StringMapModule.get hid env.(funDefs) with
              | None =>
                (
                  match StringMapModule.get hid env.(localfunDecls) with
                  | Some _ => false
                  | None => 
                    (
                      match StringMapModule.get hid env.(externals) with
                      | None => false
                      | Some lib => 
                          (
                            match StringMapModule.get hid0 lib with
                            | Some (Environment.EVar (VariableValue Undefined)) => true
                            | Some (Environment.EVar (ConstantValue _)) => true
                            | _ => false
                            end
                          )
                      end
                    )
                  end
                )
              | Some _ => false
              end
            )
         | Some ((VariableValue Undefined)) => true
         | Some ((ConstantValue _)) => true
         | _ => false
        end)
      | Some ((VariableValue Undefined)) => true
      | Some ((ConstantValue _)) => true
      | _ => false
      end)
end.

Theorem correctSetIsForgotten : forall ids inenv outenv,
  forgetIdsValue ids inenv = Some outenv ->
  isIdsforgotten ids outenv = true.
Proof.
intro.
unfold forgetIdsValue.
unfold isIdsforgotten.
unfold forgetIdValue.
unfold getIdValue.
case_eq ids.
intros.
inversion H0.
intros t l.
case_eq l.
intros.
case_eq (StringMapModule.get t (localVars inenv)); intros; rewrite H2 in *.
induction c; inversion H1; simpl.
subst.
rewrite H2; auto.
rewrite StringMapModule.gsspec.
rewrite StringMapModule.Xeq_refl; auto.
case_eq (StringMapModule.get t (globalVars inenv)); intros; rewrite H3 in *.
induction c; inversion H1; simpl.
subst; rewrite H2, H3; auto.
rewrite H2.
rewrite StringMapModule.gsspec.
rewrite StringMapModule.Xeq_refl; auto.
inversion H1.
intros.
case_eq (StringMapModule.get t (localVars inenv)); intros; rewrite H2 in *.
induction c; inversion H1; subst; simpl.
rewrite H2; auto.
rewrite StringMapModule.gsspec.
rewrite StringMapModule.Xeq_refl; auto.
case_eq (StringMapModule.get t (globalVars inenv)); intros; rewrite H3 in *.
induction c; inversion H1; subst; simpl.
rewrite H2, H3; auto.
inversion H1; subst; simpl.
rewrite H2; auto.
rewrite StringMapModule.gsspec.
rewrite StringMapModule.Xeq_refl; auto.
case_eq (StringMapModule.get t (funDefs inenv)); intros; rewrite H4 in *.
inversion H1.
case_eq (StringMapModule.get t (localfunDecls inenv)); intros; rewrite H5 in *.
inversion H1.
case_eq (StringMapModule.get t (externals inenv)); intros; rewrite H6 in *.
case_eq (StringMapModule.get t0 t1); intros; rewrite H7 in *; simpl.
induction e; [| inversion H1].
induction c; inversion H1; subst; simpl.
rewrite H2, H3, H4, H5, H6, H7; auto.
rewrite H2, H3, H4, H5; auto.
rewrite StringMapModule.gsspec.
rewrite StringMapModule.Xeq_refl; auto.
rewrite StringMapModule.gsspec.
rewrite StringMapModule.Xeq_refl; auto.
inversion H1; subst; simpl.
rewrite H2, H3, H4, H5; auto.
rewrite StringMapModule.gsspec.
rewrite StringMapModule.Xeq_refl; auto.
rewrite StringMapModule.gsspec.
rewrite StringMapModule.Xeq_refl; auto.
inversion H1; subst; simpl.
rewrite H2, H3, H4, H5; auto.
rewrite StringMapModule.gsspec.
rewrite StringMapModule.Xeq_refl; auto.
simpl.
rewrite StringMapModule.Xeq_refl; auto.
Qed.

Theorem correctKeepIsForgotten : forall ids' ids inenv outenv,
  isIdsforgotten ids inenv = true ->
  forgetIdsValue ids' inenv = Some outenv ->
  isIdsforgotten ids outenv = true.
Proof.
intro.
unfold forgetIdsValue.
unfold isIdsforgotten.
unfold forgetIdValue.
unfold getIdValue.
case_eq ids'.
simpl.
intros.
inversion H1.
intros t l.
case_eq l.
simpl.
intros.
case_eq (StringMapModule.get t (localVars inenv)); intros; rewrite H3 in *.
induction c; inversion H2; simpl.
case_eq ids; intros; rewrite H4 in *.
inversion H1.
rewrite H5 in *; auto.
case_eq ids; intros; rewrite H4 in *.
inversion H1.
case_eq l0; intros; rewrite H6 in *.
rewrite StringMapModule.gsspec.
case_eq (String_Equality.eq t0 t); intros; auto.
rewrite StringMapModule.gsspec.
case_eq (String_Equality.eq t0 t); intros; auto.
case_eq (StringMapModule.get t (globalVars inenv)); intros; rewrite H4 in *.
induction c; inversion H2; simpl.
case_eq ids; intros; rewrite H5 in *.
inversion H1.
rewrite H6 in *; auto.
case_eq ids; intros; rewrite H5 in *.
inversion H1.
case_eq l0; intros; rewrite H7 in *.
rewrite StringMapModule.gsspec.
case_eq (String_Equality.eq t0 t); intros; auto.
rewrite String_Equality.eq_spec_true in H8.
subst.
rewrite H3; auto.
rewrite StringMapModule.gsspec.
case_eq (String_Equality.eq t0 t); intros; auto.
rewrite String_Equality.eq_spec_true in H8.
subst.
rewrite H3; auto.
inversion H2.
intros.
case_eq (StringMapModule.get t (localVars inenv)); intros; rewrite H3 in *.
induction c; inversion H2; simpl.
inversion H2; subst; auto.
case_eq ids; intros; rewrite H4 in *.
inversion H1.
case_eq l1; intros; rewrite H6 in *.
rewrite StringMapModule.gsspec.
case_eq (String_Equality.eq t1 t); intros; auto.
rewrite StringMapModule.gsspec.
case_eq (String_Equality.eq t1 t); intros; auto.
case_eq (StringMapModule.get t (globalVars inenv)); intros; rewrite H4 in *.
induction c; inversion H2; simpl.
subst; auto.
case_eq ids; intros; rewrite H5 in *.
inversion H1.
case_eq l1; intros; rewrite H7 in *.
rewrite StringMapModule.gsspec.
case_eq (String_Equality.eq t1 t); intros; auto.
rewrite String_Equality.eq_spec_true in H8.
subst.
rewrite H3; auto.
rewrite StringMapModule.gsspec.
case_eq (String_Equality.eq t1 t); intros; auto.
rewrite String_Equality.eq_spec_true in H8.
subst.
rewrite H3; auto.
case_eq (StringMapModule.get t (funDefs inenv)); intros; rewrite H5 in *.
inversion H2.
case_eq (StringMapModule.get t (localfunDecls inenv)); intros; rewrite H6 in *.
inversion H2.
case_eq (StringMapModule.get t (externals inenv)); intros; rewrite H7 in *.
case_eq (StringMapModule.get t0 t1); intros; rewrite H8 in *.
induction e; [|inversion H2].
induction c; inversion H2; subst; auto.
case_eq ids; intros; rewrite H in *.
inversion H1.
case_eq l; intros; rewrite H0 in *; simpl.
auto.
rewrite StringMapModule.gsspec.
case_eq (String_Equality.eq t2 t); intros; auto.
rewrite String_Equality.eq_spec_true in H9; subst.
rewrite H3, H4, H5, H6; auto.
rewrite StringMapModule.gsspec.
case_eq (String_Equality.eq t3 t0); intros; auto.
rewrite H3, H4, H5, H6, H7 in H1; auto.
inversion H2; subst; simpl.
case_eq ids; intros; rewrite H in *.
inversion H1.
case_eq l; intros; rewrite H0 in *; auto.
rewrite StringMapModule.gsspec.
case_eq (String_Equality.eq t2 t); intros; auto.
rewrite String_Equality.eq_spec_true in H9.
subst.
rewrite StringMapModule.gsspec.
rewrite H3, H4, H5, H6; auto.
case_eq (String_Equality.eq t3 t0); intros; auto.
rewrite H3, H4, H5, H6, H7 in H1; auto.
inversion H2; simpl.
case_eq ids; intros; rewrite H8 in *.
inversion H1.
case_eq l1; intros; rewrite H10 in *.
rewrite H1; auto.
rewrite StringMapModule.gsspec.
case_eq (String_Equality.eq t1 t); intros; auto.
rewrite String_Equality.eq_spec_true in H11.
subst.
simpl.
rewrite H3, H4, H5, H6; auto.
case_eq (String_Equality.eq t2 t0); intros; auto.
rewrite H3, H4, H5, H6, H7 in H1; auto.
Qed.

Theorem correctIdemPotentIsForgotten : forall ids inenv,
  isIdsforgotten ids inenv = true ->
  forgetIdsValue ids inenv = Some inenv.
Proof.
intro.
unfold forgetIdsValue.
unfold isIdsforgotten.
unfold forgetIdValue.
unfold getIdValue.
case_eq ids.
intros.
inversion H0.
intros t l.
case_eq l.
intros.
case_eq (StringMapModule.get t (localVars inenv)); intros; rewrite H2 in *.
induction c; auto.
induction v; inversion H1.
induction inenv; simpl in *; repeat (f_equal; auto).
apply mapSetIdempoten; auto.
case_eq (StringMapModule.get t (globalVars inenv)); intros; rewrite H3 in *.
induction c; auto.
induction v; inversion H1.
induction inenv; simpl in *; repeat (f_equal; auto).
apply mapSetIdempoten; auto.
case_eq (StringMapModule.get t (funDefs inenv)); intros; rewrite H4 in *.
inversion H1.
case_eq (StringMapModule.get t (localfunDecls inenv)); intros; rewrite H5 in *.
inversion H1.
inversion H1.
intros.
case_eq (StringMapModule.get t (localVars inenv)); intros; rewrite H2 in *.
induction c; auto.
induction v; inversion H1; auto.
induction inenv; simpl in *; repeat (f_equal; auto).
apply mapSetIdempoten; auto.
case_eq (StringMapModule.get t (globalVars inenv)); intros; rewrite H3 in *.
induction c; inversion H1; auto.
clear H5.
induction v; inversion H1.
induction inenv; simpl in *; repeat (f_equal; auto).
apply mapSetIdempoten; auto.
case_eq (StringMapModule.get t (funDefs inenv)); intros; rewrite H4 in *.
inversion H1.
case_eq (StringMapModule.get t (localfunDecls inenv)); intros; rewrite H5 in *.
inversion H1.
case_eq (StringMapModule.get t (externals inenv)); intros; rewrite H6 in *.
case_eq (StringMapModule.get t0 t1); intros; rewrite H7 in *.
induction e; inversion H1.
clear H9.
induction c; inversion H1; auto.
clear H9.
induction v; inversion H1.
induction inenv; simpl in *; repeat (f_equal; auto).
assert (StringMapModule.set t0 (Environment.EVar (VariableValue Undefined)) t1 = t1).
rewrite mapSetIdempoten; auto.
rewrite H8.
rewrite mapSetIdempoten; auto.
inversion H1.
inversion H1.
Qed.

Fixpoint forgetIdsValueList l e := match l with
| [] => Some e
| h::t => 
    (
      match forgetIdsValueList t e with
      | Some e' => forgetIdsValue h e'
      | _ => None
      end
    )
end.

Theorem correctKeepIsForgottenList : forall l ids inenv outenv,
  isIdsforgotten ids inenv = true ->
  forgetIdsValueList l inenv = Some outenv ->
  isIdsforgotten ids outenv = true.
Proof.
induction l.
simpl.
intros.
inversion H0.
subst.
auto.
simpl.
intros.
case_eq (forgetIdsValueList l inenv); intros; rewrite H1 in *; [|inversion H0].
assert (isIdsforgotten ids e = true).
eapply IHl; eauto.
eapply correctKeepIsForgotten; eauto.
Qed.

Theorem correctSetIsForgottenList : forall l inenv outenv,
  forgetIdsValueList l inenv = Some outenv -> 
  forall i, In i l -> isIdsforgotten i outenv = true.
Proof.
induction l.
simpl.
intros.
inversion H0.
simpl.
intros.
case_eq (forgetIdsValueList l inenv); intros; rewrite H1 in *; [|inversion H].
destruct H0.
subst.
eapply correctSetIsForgotten; eauto.
assert (isIdsforgotten i e = true).
eauto.
eapply correctKeepIsForgotten; eauto.
Qed.

Theorem correctIdemPotentIsForgottenListIn : forall l inenv,
  (forall i, In i l -> isIdsforgotten i inenv = true) -> 
  forgetIdsValueList l inenv = Some inenv.
Proof.
induction l; simpl; auto.
intros.
rewrite IHl.
assert (Ha := H a).
assert (a = a \/ In a l); auto.
apply Ha in H0.
apply correctIdemPotentIsForgotten; auto.
intros.
assert (Ha := H i).
assert (a = i \/ In i l); auto.
Qed.

Theorem correctIdemPotentIsForgottenList : forall l inenv outenv,
  forgetIdsValueList l inenv = Some outenv ->
  forgetIdsValueList l outenv = Some outenv.
Proof.
intros.
assert (D := correctSetIsForgottenList l inenv outenv H).
apply correctIdemPotentIsForgottenListIn; auto.
Qed.

Theorem envpartialViewForgetList : forall l env outenv,
  forgetIdsValueList l env = Some outenv ->
  envpartialView outenv env.
induction l.
simpl.
intros.
inversion H.
apply envpartialView_refl.
simpl.
intros.
case_eq (forgetIdsValueList l env); intros; rewrite H0 in *.
apply IHl in H0.
apply envpartialViewForgetVsNothing with (e' := e) in H.
eapply envpartialView_trans; eauto.
apply envpartialView_refl.
inversion H.
Qed.

Definition forgetGlobalsAndExternals env := {| localVars := env.(localVars);
        globalVars := StringMapModule.map 
                (fun v => match v with | ConstantValue va => ConstantValue va | VariableValue v => VariableValue Undefined end ) env.(globalVars);
        funDefs := env.(funDefs);
        localfunDecls := env.(localfunDecls);
        externals := 
          StringMapModule.map 
            (fun m => 
              StringMapModule.map 
                (fun v => match v with | Environment.EVar (VariableValue v) => Environment.EVar (VariableValue Undefined) | r => r  end ) m )
          env.(externals)  |}.

Lemma forgetGlob : forall m, StringMapModule.map
  (fun v0 : ConstOrVar =>
   match v0 with
   | ConstantValue va => ConstantValue va
   | VariableValue _ => VariableValue Undefined
   end)
  (StringMapModule.map
     (fun v0 : ConstOrVar =>
      match v0 with
      | ConstantValue va => ConstantValue va
      | VariableValue _ => VariableValue Undefined
      end) m) =
StringMapModule.map
  (fun v0 : ConstOrVar =>
   match v0 with
   | ConstantValue va => ConstantValue va
   | VariableValue _ => VariableValue Undefined
   end) m.
Proof.
induction m; simpl; auto.
induction a.
induction b.
simpl.
f_equal; auto.
simpl.
f_equal; auto.
Qed.

Lemma forgetExt : forall m, StringMapModule.map 
            (fun m => 
              StringMapModule.map 
                (fun v => match v with | Environment.EVar (VariableValue v) => Environment.EVar (VariableValue Undefined) | r => r  end ) m )
          
  (StringMapModule.map 
            (fun m => 
              StringMapModule.map 
                (fun v => match v with | Environment.EVar (VariableValue v) => Environment.EVar (VariableValue Undefined) | r => r  end ) m )
           m) =
StringMapModule.map 
            (fun m => 
              StringMapModule.map 
                (fun v => match v with | Environment.EVar (VariableValue v) => Environment.EVar (VariableValue Undefined) | r => r  end ) m )
           m.
Proof.
induction m; simpl; auto.
induction a; simpl.
f_equal; auto.
f_equal; auto.
induction b; simpl; auto.
induction a0; simpl; auto.
f_equal; auto.
f_equal; auto.
induction b0; auto.
induction c; auto.
Qed.

Lemma setForgetMap : forall m t v, 
  StringMapModule.get t m = Some (VariableValue v) ->
  StringMapModule.set t (VariableValue Undefined)
     (StringMapModule.map
        (fun v0 : ConstOrVar =>
         match v0 with
         | ConstantValue va => ConstantValue va
         | VariableValue _ => VariableValue Undefined
         end) m) = (StringMapModule.map
        (fun v0 : ConstOrVar =>
         match v0 with
         | ConstantValue va => ConstantValue va
         | VariableValue _ => VariableValue Undefined
         end) m).
Proof.
induction m; simpl.
intros.
inversion H.
induction a.
intros.
case_eq (String_Equality.eq t a); intros; rewrite H0 in *.
rewrite String_Equality.eq_spec_true in H0.
inversion H.
subst.
simpl.
rewrite StringMapModule.Xeq_refl.
auto.
simpl.
rewrite H0.
f_equal; auto.
eapply IHm; eauto.
Qed.

Lemma setForgetMap' : forall m t v, 
  StringMapModule.get t m = Some (Environment.EVar (VariableValue v)) ->
  StringMapModule.set t (Environment.EVar (VariableValue Undefined))
     (StringMapModule.map
        (fun v0 : External =>
         match v0 with
         | Environment.EVar (VariableValue _) => Environment.EVar (VariableValue Undefined)
         | _ => v0
         end) m) = (StringMapModule.map
        (fun v0 : External =>
         match v0 with
         | Environment.EVar (VariableValue _) => Environment.EVar (VariableValue Undefined)
         | _ => v0
         end) m).
Proof.
induction m; simpl.
intros.
inversion H.
induction a.
intros.
case_eq (String_Equality.eq t a); intros; rewrite H0 in *.
rewrite String_Equality.eq_spec_true in H0.
inversion H.
subst.
simpl.
rewrite StringMapModule.Xeq_refl.
auto.
simpl.
rewrite H0.
f_equal; auto.
eapply IHm; eauto.
Qed.

Lemma setForgetMap2 : forall m t t0 t1 v, 
  StringMapModule.get t m = Some t1 ->
  StringMapModule.get t0 t1 = Some (Environment.EVar (VariableValue v)) ->
  (StringMapModule.set t
     (StringMapModule.set t0 (Environment.EVar (VariableValue Undefined))
        (StringMapModule.map
           (fun v0 : External =>
            match v0 with
            | Environment.EVar (VariableValue _) => Environment.EVar (VariableValue Undefined)
            | _ => v0
            end) t1))
              (StringMapModule.map
               (fun m : StringMapModule.t External =>
                StringMapModule.map
                  (fun v0 : External =>
                   match v0 with
                   | Environment.EVar (VariableValue _) => Environment.EVar (VariableValue Undefined)
                   | _ => v0
                   end) m) m)) = 
              (StringMapModule.map
               (fun m : StringMapModule.t External =>
                StringMapModule.map
                  (fun v0 : External =>
                   match v0 with
                   | Environment.EVar (VariableValue _) => Environment.EVar (VariableValue Undefined)
                   | _ => v0
                   end) m) m).
Proof.
induction m; simpl; intros.
inversion H.
induction a; simpl.
case_eq (String_Equality.eq t a); intros; rewrite H1 in *.
f_equal.
f_equal.
inversion H.
eapply setForgetMap'; eauto.
f_equal; eauto.
Qed.

Lemma forgetGlob' : forall m, StringMapModule.map
  (fun v : External =>
      match v with
      | Environment.EVar (VariableValue _) => Environment.EVar (VariableValue Undefined)
      | _ => v
      end)
  (StringMapModule.map
     (fun v : External =>
      match v with
      | Environment.EVar (VariableValue _) => Environment.EVar (VariableValue Undefined)
      | _ => v
      end) m) =
StringMapModule.map
  (fun v : External =>
      match v with
      | Environment.EVar (VariableValue _) => Environment.EVar (VariableValue Undefined)
      | _ => v
      end) m.
Proof.
induction m; simpl; auto.
induction a.
induction b.
simpl.
induction c.
f_equal; auto.
f_equal; auto.
simpl.
f_equal; auto.
Qed.

Lemma setForgetMap'' : forall m t0,
StringMapModule.map
  (fun v : External =>
   match v with
   | Environment.EVar (VariableValue _) => Environment.EVar (VariableValue Undefined)
   | _ => v
   end)
  (StringMapModule.set t0 (Environment.EVar (VariableValue Undefined))
     (StringMapModule.map
        (fun v : External =>
         match v with
         | Environment.EVar (VariableValue _) => Environment.EVar (VariableValue Undefined)
         | _ => v
         end) m)) =
StringMapModule.set t0 (Environment.EVar (VariableValue Undefined))
  (StringMapModule.map
     (fun v : External =>
      match v with
      | Environment.EVar (VariableValue _) => Environment.EVar (VariableValue Undefined)
      | _ => v
      end) m).
Proof.
induction m; simpl; auto; intros.
induction a.
induction b.
simpl.
case_eq (String_Equality.eq t0 a); intros.
simpl.
f_equal; auto.
apply forgetGlob'.
simpl.
induction c.
f_equal; auto.
f_equal; auto.
simpl.
case_eq (String_Equality.eq t0 a); intros.
simpl.
f_equal; auto.
apply forgetGlob'.
simpl.
f_equal; auto.
Qed.

Lemma setForgetMap2' : forall m t t0 t1, 
  StringMapModule.get t m = Some t1 ->
  StringMapModule.get t0 t1 = None ->
StringMapModule.map
  (fun m : StringMapModule.t External =>
   StringMapModule.map
     (fun v : External =>
      match v with
      | Environment.EVar (VariableValue _) => Environment.EVar (VariableValue Undefined)
      | _ => v
      end) m)
  (StringMapModule.set t
     (StringMapModule.set t0 (Environment.EVar (VariableValue Undefined))
        (StringMapModule.map
           (fun v : External =>
            match v with
            | Environment.EVar (VariableValue _) => Environment.EVar (VariableValue Undefined)
            | _ => v
            end) t1))
     (StringMapModule.map
        (fun m : StringMapModule.t External =>
         StringMapModule.map
           (fun v : External =>
            match v with
            | Environment.EVar (VariableValue _) => Environment.EVar (VariableValue Undefined)
            | _ => v
            end) m) m)) =
StringMapModule.set t
  (StringMapModule.set t0 (Environment.EVar (VariableValue Undefined))
     (StringMapModule.map
        (fun v : External =>
         match v with
         | Environment.EVar (VariableValue _) => Environment.EVar (VariableValue Undefined)
         | _ => v
         end) t1))
  (StringMapModule.map
     (fun m : StringMapModule.t External =>
      StringMapModule.map
        (fun v : External =>
         match v with
         | Environment.EVar (VariableValue _) => Environment.EVar (VariableValue Undefined)
         | _ => v
         end) m) m).
Proof.
induction m; simpl; intros.
inversion H.
induction a; simpl.
case_eq (String_Equality.eq t a); intros; rewrite H1 in *; eauto.
inversion H.
simpl.
subst.
f_equal; auto.
f_equal; auto.
apply setForgetMap''.
apply forgetExt.
simpl.
f_equal; auto.
f_equal; auto.
apply forgetGlob'.
Qed.

Lemma setForgetMap2'' : forall m t t0, 
  StringMapModule.get t m = None ->
StringMapModule.map
  (fun m : StringMapModule.t External =>
   StringMapModule.map
     (fun v : External =>
      match v with
      | Environment.EVar (VariableValue _) => Environment.EVar (VariableValue Undefined)
      | _ => v
      end) m)
  (StringMapModule.set t [(t0, Environment.EVar (VariableValue Undefined))]
     (StringMapModule.map
        (fun m : StringMapModule.t External =>
         StringMapModule.map
           (fun v : External =>
            match v with
            | Environment.EVar (VariableValue _) => Environment.EVar (VariableValue Undefined)
            | _ => v
            end) m) m)) =
StringMapModule.set t [(t0, Environment.EVar (VariableValue Undefined))]
  (StringMapModule.map
     (fun m : StringMapModule.t External =>
      StringMapModule.map
        (fun v : External =>
         match v with
         | Environment.EVar (VariableValue _) => Environment.EVar (VariableValue Undefined)
         | _ => v
         end) m) m).
Proof.
induction m; simpl; auto; intros.
induction a; simpl.
case_eq (String_Equality.eq t a); intros; rewrite H0 in *; eauto.
inversion H.
simpl.
induction b; simpl; auto.
f_equal; auto.
induction a0; simpl.
induction b0; simpl.
induction c; simpl.
f_equal; auto.
f_equal; auto.
f_equal; auto.
apply forgetGlob'.
f_equal; auto.
f_equal; auto.
f_equal; auto.
apply forgetGlob'.
f_equal; auto.
f_equal; auto.
f_equal; auto.
apply forgetGlob'.
Qed.

Theorem forgetGlobalsAndExternalsForgetIdemPoten : forall a env outenv,
  forgetIdsValue a (forgetGlobalsAndExternals env) = Some outenv ->
  forgetGlobalsAndExternals outenv = outenv.
Proof.
unfold forgetIdsValue.
unfold forgetIdValue.
unfold forgetGlobalsAndExternals.
simpl.
intros.
case_eq a; simpl; intros; rewrite H0 in *.
inversion H.
case_eq l; intros; rewrite H1 in *. 
case_eq (StringMapModule.get t (localVars env)); intros; rewrite H2 in *.
induction c; inversion H; simpl.
f_equal.
apply forgetGlob.
apply forgetExt.
f_equal.
apply forgetGlob.
apply forgetExt.
rewrite StringMapModule.gmap in H.
case_eq (StringMapModule.get t (globalVars env)); intros; rewrite H3 in *.
induction c; inversion H; simpl.
f_equal.
apply forgetGlob.
apply forgetExt.
simpl.
f_equal.
erewrite setForgetMap; eauto.
apply forgetGlob.
apply forgetExt.
inversion H.
case_eq (StringMapModule.get t (localVars env)); intros; rewrite H2 in *.
induction c; inversion H; simpl.
f_equal.
apply forgetGlob.
apply forgetExt.
f_equal.
apply forgetGlob.
apply forgetExt.
rewrite StringMapModule.gmap in H.
case_eq (StringMapModule.get t (globalVars env)); intros; rewrite H3 in *.
induction c; inversion H; simpl.
f_equal.
apply forgetGlob.
apply forgetExt.
simpl.
f_equal.
erewrite setForgetMap; eauto.
apply forgetGlob.
apply forgetExt.
case_eq (StringMapModule.get t (funDefs env)); intros; rewrite H4 in *.
inversion H.
case_eq (StringMapModule.get t (localfunDecls env)); intros; rewrite H5 in *.
inversion H.
rewrite StringMapModule.gmap in H.
case_eq (StringMapModule.get t (externals env)); intros; rewrite H6 in *.
rewrite StringMapModule.gmap in H.
case_eq (StringMapModule.get t0 t1); intros; rewrite H7 in *.
induction e.
induction c; inversion H; simpl.
f_equal.
apply forgetGlob.
apply forgetExt.
f_equal.
apply forgetGlob.
erewrite setForgetMap2; eauto.
apply forgetExt.
inversion H.
inversion H; simpl.
f_equal.
apply forgetGlob.
erewrite setForgetMap2'; eauto.
inversion H; simpl.
f_equal.
apply forgetGlob.
simpl.
apply setForgetMap2''; auto.
Qed.

Theorem forgetGlobalsAndExternalsForgetListIdemPoten : forall l env outenv,
  forgetIdsValueList l (forgetGlobalsAndExternals env) = Some outenv ->
  forgetGlobalsAndExternals outenv = outenv.
Proof.
induction l; simpl.
intros.
inversion H.
unfold forgetGlobalsAndExternals.
f_equal.
simpl.
apply forgetGlob.
simpl.
apply forgetExt.
intros.
case_eq (forgetIdsValueList l (forgetGlobalsAndExternals env)); intros; rewrite H0 in *; [|inversion H].
apply IHl in H0.
rewrite <- H0 in H.
apply forgetGlobalsAndExternalsForgetIdemPoten in H.
auto.
Qed.

(* Definition forgetExternals env := {| localVars := env.(localVars);
        globalVars := env.(globalVars);
        funDefs := env.(funDefs);
        localfunDecls := env.(localfunDecls);
        externals := 
          StringMapModule.map 
            (fun m => 
              StringMapModule.map 
                (fun v => match v with | Environment.EVar (VariableValue v) => Environment.EVar (VariableValue Undefined) | r => r  end ) m )
          env.(externals)  |}. *)

(* Theorem forgetExternalsForgetIdemPoten : forall a env outenv,
  forgetIdsValue a (forgetExternals env) = Some outenv ->
  forgetExternals outenv = outenv.
Proof.
unfold forgetIdsValue.
unfold forgetIdValue.
unfold forgetExternals.
simpl.
intros.
case_eq a; simpl; intros; rewrite H0 in *.
inversion H.
case_eq l; intros; rewrite H1 in *. 
case_eq (StringMapModule.get t (localVars env)); intros; rewrite H2 in *.
induction c; inversion H; simpl.
f_equal.
apply forgetExt.
f_equal.
apply forgetExt.
case_eq (StringMapModule.get t (globalVars env)); intros; rewrite H3 in *.
induction c; inversion H; simpl.
f_equal.
apply forgetExt.
simpl.
f_equal.
apply forgetExt.
inversion H.
case_eq (StringMapModule.get t (localVars env)); intros; rewrite H2 in *.
induction c; inversion H; simpl.
f_equal.
apply forgetExt.
f_equal.
apply forgetExt.
case_eq (StringMapModule.get t (globalVars env)); intros; rewrite H3 in *.
induction c; inversion H; simpl.
f_equal.
apply forgetExt.
simpl.
f_equal.
apply forgetExt.
case_eq (StringMapModule.get t (funDefs env)); intros; rewrite H4 in *.
inversion H.
case_eq (StringMapModule.get t (localfunDecls env)); intros; rewrite H5 in *.
inversion H.
rewrite StringMapModule.gmap in H.
case_eq (StringMapModule.get t (externals env)); intros; rewrite H6 in *.
rewrite StringMapModule.gmap in H.
case_eq (StringMapModule.get t0 t1); intros; rewrite H7 in *.
induction e.
induction c; inversion H; simpl.
f_equal.
apply forgetExt.
f_equal.
erewrite setForgetMap2; eauto.
apply forgetExt.
inversion H.
inversion H; simpl.
f_equal.
erewrite setForgetMap2'; eauto.
inversion H; simpl.
f_equal.
simpl.
apply setForgetMap2''; auto.
Qed. *)

(* Theorem forgetExternalsForgetListIdemPoten : forall l env outenv,
  forgetIdsValueList l (forgetExternals env) = Some outenv ->
  forgetExternals outenv = outenv.
Proof.
induction l; simpl.
intros.
inversion H.
unfold forgetExternals.
f_equal.
simpl.
apply forgetExt.
intros.
case_eq (forgetIdsValueList l (forgetExternals env)); intros; rewrite H0 in *; [|inversion H].
apply IHl in H0.
rewrite <- H0 in H.
apply forgetExternalsForgetIdemPoten in H.
auto.
Qed.

Lemma envpartialViewforgetExternals : forall e,
    envpartialView (forgetExternals e) e.
Proof.
unfold forgetExternals.
unfold envpartialView.
simpl.
intros.
repeat (split; try apply mapPartialView_refl).
intros.
rewrite StringMapModule.gmap.
induction (StringMapModule.get id (externals e)); auto.
intros.
rewrite StringMapModule.gmap.
induction (StringMapModule.get i a); auto.
induction a0; auto.
induction c; eauto.
right; eauto.
Qed. *)

Lemma envpartialViewforgetGlobalsAndExternals : forall e,
    envpartialView (forgetGlobalsAndExternals e) e.
Proof.
unfold forgetGlobalsAndExternals.
unfold envpartialView.
simpl.
intros.
repeat (split; try apply mapPartialView_refl).
unfold mapPartialView.
intros.
rewrite StringMapModule.gmap.
induction (StringMapModule.get id (globalVars e)); auto.
induction a; auto.
right; auto; eauto.
intros.
rewrite StringMapModule.gmap.
induction (StringMapModule.get id (externals e)); auto.
intros.
rewrite StringMapModule.gmap.
induction (StringMapModule.get i a); auto.
induction a0; auto.
induction c; auto.
right; eauto.
Qed.

Fixpoint forgetcopyOutSigList (sig: list (ident * mode * name)) (params: list expression_or_string_lit) := match sig, params with
| [], [] => Some []
| ((idSig, Ast.In, IdTyp)::tSig), (eParam::tParams) => forgetcopyOutSigList tSig tParams
| ((idSig, Ast.InOut, IdTyp)::tSig), ((Exp (EVar eParam))::tParams) =>
    (
      match forgetcopyOutSigList tSig tParams with
      | Some r => Some (eParam::r)
      | None => None
      end
    )
| ((idSig, Ast.Out, IdTyp)::tSig), ((Exp (EVar eParam))::tParams) =>
    (
      match forgetcopyOutSigList tSig tParams with
      | Some r => Some (eParam::r)
      | None => None
      end
    )
| _, _ => None
end.

(*Definition forgetcopyOutExternal (sig: list (ident * mode * nom)) (params: list expression_or_string_lit) env :=
  match forgetcopyOutExternalList sig params with
  | Some l => forgetIdsValueList l (forgetGlobalsAndExternals env)
  | None => None
  end.*)

(* Theorem forgetcopyOutSigList_copyoutExternalWithSig_correct : forall sig params l lenv env lenv' env' f_env,
  envpartialView lenv env ->
  copyOutExternalWithSig_f f_env sig params env = Some env' ->
  forgetcopyOutSigList sig params = Some l ->
  forgetIdsValueList l (forgetExternals lenv) = Some lenv' ->
  envpartialView lenv' env'.
Proof.
induction sig.
induction params; simpl; unfold envpartialView.
intros.
inversion H0.
inversion H1; subst.
simpl in H2.
inversion H2.
simpl.
destruct H.
destruct H3.
destruct H5.
destruct H6.
repeat (split; auto).
apply slim_projectMapMap; auto.
intros.
inversion H0.
induction a.
induction a.
induction params; simpl.
induction b0; intros; inversion H0.
induction b0.
intros.
eapply IHsig; eauto.
induction a0; [|intros; inversion H0].
induction e; [|intros; inversion H0|intros; inversion H0|intros; inversion H0].
intros l lenv env lenv' env' f_env H.
case_eq (copyOutExternalWithSig_f f_env sig params env); [| intros; inversion H1].
case_eq (forgetcopyOutSigList sig params); [| intros; inversion H3].
case_eq (match getIdValue a f_env with
  | Some (Value (ConstantValue v)) | Some (Value (VariableValue v)) => Some v
  | None => Some Undefined
  | _ => None
  end); intros; [|inversion H3].
inversion H4; subst.
simpl in H5.
case_eq (forgetIdsValueList l0 (forgetExternals lenv)); intros; rewrite H6 in *; [|inversion H5].
eapply IHsig in H6; eauto.
apply envpartialViewForgetVsSet with (ids := n) (e:=e0) (e':=e) (v:=v); auto.
induction a0; [|intros; inversion H0].
induction e; [|intros; inversion H0|intros; inversion H0|intros; inversion H0].
intros l lenv env lenv' env' f_env H.
case_eq (copyOutExternalWithSig_f f_env sig params env); [| intros; inversion H1].
case_eq (forgetcopyOutSigList sig params); [| intros; inversion H3].
case_eq (match getIdValue a f_env with
  | Some (Value (ConstantValue v)) | Some (Value (VariableValue v)) => Some v
  | None => Some Undefined
  | _ => None
  end); intros; [|inversion H3].
inversion H4; subst.
simpl in H5.
case_eq (forgetIdsValueList l0 (forgetExternals lenv)); intros; rewrite H6 in *; [|inversion H5].
eapply IHsig in H6; eauto.
apply envpartialViewForgetVsSet with (ids := n) (e:=e0) (e':=e) (v:=v); auto.
Qed. *)

Fixpoint forgetcopyOutExternalWithoutSigList (params: list expression_or_string_lit) := match params with
| [] => []
| ((Exp (EVar eParam))::tParams) => (eParam)::(forgetcopyOutExternalWithoutSigList tParams)
| (_::tParams) => forgetcopyOutExternalWithoutSigList tParams
end.

Theorem forgetcopyOutExternalWithoutSigList_correct : forall params lenv env lenv' env' f_env,
  envpartialView lenv env ->
  copyOutExternalWithoutSig_f f_env params env = Some env' ->
  forgetIdsValueList (forgetcopyOutExternalWithoutSigList params) (forgetGlobalsAndExternals lenv) = Some lenv' ->
  envpartialView lenv' env'.
Proof.
induction params; simpl.
unfold envpartialView.
intros.
inversion H0.
inversion H1.
simpl.
destruct H.
destruct H2.
destruct H5.
destruct H6.
repeat (split; auto).
apply slim_projectMapGlobal; auto.
apply slim_projectMapLib; auto.
intros.
induction a.
induction e.
simpl in H1.
case_eq (forgetIdsValueList (forgetcopyOutExternalWithoutSigList params)
         (forgetGlobalsAndExternals lenv)); intros; rewrite H2 in *; [|inversion H1].
case_eq (copyOutExternalWithoutSig_f f_env params env); intros; rewrite H3 in *; [|inversion H0].
induction (match getIdsValue n f_env with
       | Some (Value (ConstantValue v)) | Some (Value (VariableValue v)) => Some v
       | None => Some Undefined
       | _ => None
       end); [|inversion H0].
apply envpartialViewForgetVsSet with (ids := n) (e:=e) (e':=e0) (v:=a); auto.
eauto.
eauto.
eauto.
eauto.
eauto.
Qed.

Theorem forgetcopyOutSigList_copyout_correct : forall local_f sig params l lenv env lenv' env',
  envpartialView lenv env ->
  copyOut_f local_f sig params env = Some env' ->
  forgetcopyOutSigList sig params = Some l ->
  forgetIdsValueList l (forgetGlobalsAndExternals lenv) = Some lenv' ->
  envpartialView lenv' env'.
Proof.
induction sig.
induction params; simpl; unfold envpartialView.
intros.
inversion H0.
inversion H1; subst.
simpl in H2.
inversion H2.
simpl.
destruct H.
destruct H3.
destruct H5.
destruct H6.
repeat (split; auto).
apply slim_projectMapGlobal; auto.
apply slim_projectMapLib; auto.
intros.
inversion H0.
induction a.
induction a.
induction params; simpl.
induction b0; intros; inversion H0.
induction b0.
intros.
eapply IHsig; eauto.
induction a0; [|intros; inversion H0].
induction e; [|intros; inversion H0|intros; inversion H0|intros; inversion H0].
intros l lenv env lenv' env' H.
case_eq (copyOut_f local_f sig params env); [| intros; inversion H1].
case_eq (forgetcopyOutSigList sig params); [| intros; inversion H3].
case_eq (match getIdValue a local_f with
  | Some (Value (ConstantValue v)) | Some (Value (VariableValue v)) => Some v
  | None => Some Undefined
  | _ => None
  end); intros; [|inversion H3].
inversion H4; subst.
simpl in H5.
case_eq (forgetIdsValueList l0 (forgetGlobalsAndExternals lenv)); intros; rewrite H6 in *; [|inversion H5].
eapply IHsig in H6; eauto.
apply envpartialViewForgetVsSet with (ids := n) (e:=e0) (e':=e) (v:=v); auto.
induction a0; [|intros; inversion H0].
induction e; [|intros; inversion H0|intros; inversion H0|intros; inversion H0].
intros l lenv env lenv' env' H.
case_eq (copyOut_f local_f sig params env); [| intros; inversion H1].
case_eq (forgetcopyOutSigList sig params); [| intros; inversion H3].
case_eq (match getIdValue a local_f with
  | Some (Value (ConstantValue v)) | Some (Value (VariableValue v)) => Some v
  | None => Some Undefined
  | _ => None
  end); intros; [|inversion H3].
inversion H4; subst.
simpl in H5.
case_eq (forgetIdsValueList l0 (forgetGlobalsAndExternals lenv)); intros; rewrite H6 in *; [|inversion H5].
eapply IHsig in H6; eauto.
apply envpartialViewForgetVsSet with (ids := n) (e:=e0) (e':=e) (v:=v); auto.
Qed.

Definition forgetFunCallList env f := match f with
| ICallDefinedFunction f_name None => 
    (
      match getIdsValue f_name env with
      | Some (DefinedFunction fn) => 
          forgetcopyOutSigList fn.(f_args) []
      | Some (FunctionDeclaration sig) => 
          forgetcopyOutSigList sig []
      | _ => 
          Some (forgetcopyOutExternalWithoutSigList [])
      end
    )
| ICallDefinedFunction f_name (Some p) => 
    (
      match getIdsValue f_name env with
      | Some (DefinedFunction fn) => 
          forgetcopyOutSigList fn.(f_args) p
      | Some (FunctionDeclaration sig) => 
          forgetcopyOutSigList sig p
      | _ => 
          Some (forgetcopyOutExternalWithoutSigList p)
      end
    )
| (SOInstructionCompareM status m0 m1 t) => Some [status]
| (SOInstructionCompareR status e1 e2 e3 e4 e5 e6 t) => Some [status]
| (SOInstructionInit var (Some e)) => Some [var]
| (SOInstructionReadHardInput t e n_config no_t) => Some [t]
| (SOInstructionReadMess t valide e no_msg no_t no_fic no_bx) => Some [valide; t]
| (SOInstructionReadMessUnfil t valide e no_msg no_fic no_bx) => Some [valide; t]
| (SOInstructionWrite t i x None) => Some [t]
| (SOInstructionWrite t i j (Some x)) => Some [t]
| (SOInstructionWriteHardOutput s t no_config no_t)  => Some [s]
| (SOInstructionWriteMess s t no_msg no_fic no_bx) => Some [s]
| (SOInstructionWriteOutvar z x no_fic no_bx no_t) => Some [z]
| (SOInstructionWriteRedund r t no_fic no_bx no_t) => Some [r]
| _ => Some []
end.

Definition forgetFunCallFinisher f := match f with
| ICallDefinedFunction f_name _ => forgetGlobalsAndExternals
| _ => (fun x => x)
end.

(* Definition forgetFunCallList_correct : forall e in_data f res l slim_e,
  evalFunCall in_data f res ->
  extractErrorType res = None ->
  envpartialView e (fst in_data) ->
  forgetFunCallList e f = Some l ->
  forgetIdsValueList l (forgetFunCallFinisher f e) = Some slim_e ->
  envpartialView slim_e (extractEnv res).
Proof.
induction 1; intros.
simpl in H4.
inversion H0; subst; simpl in *.
assert (D:= envpartialViewGetIdsSome f_name e b_env H2).
destruct D.
rewrite H in *.
rewrite H5 in *.
eapply forgetcopyOutSigList_copyout_correct with (local_f:= af_env); eauto.
apply copyOut_f_correct; auto.
destruct H5, H5, H6.
destruct H6.
rewrite H in H6; inversion H6.
rewrite H in H6; inversion H6.
destruct H6.
rewrite H in H6; inversion H6.
rewrite H in H6; inversion H6.
inversion H1.
inversion H0; subst; simpl in *.
assert (D:= envpartialViewGetIdsSome f_name e b_env H2).
destruct D.
rewrite H in *.
rewrite H5 in *.
eapply forgetcopyOutSigList_copyout_correct with (local_f:= af_env); eauto.
apply copyOut_f_correct; auto.
destruct H5, H5, H6.
destruct H6.
rewrite H in H6; inversion H6.
rewrite H in H6; inversion H6.
destruct H6.
rewrite H in H6; inversion H6.
rewrite H in H6; inversion H6.
inversion H1.
simpl in *.
assert (D:= envpartialViewGetIdsSome f_name e b_env H3).
destruct D.
rewrite H in *.
rewrite H6 in *.
eapply forgetcopyOutSigList_copyout_correct with (local_f:= a_env); eauto.
apply copyOut_f_correct; auto.
destruct H6, H6, H7.
destruct H7.
rewrite H in H7; inversion H7.
rewrite H in H7; inversion H7.
destruct H7.
rewrite H in H7; inversion H7.
rewrite H in H7; inversion H7.
inversion H1.
simpl in *.
assert (D:= envpartialViewGetIdsSome f_name e b_env H3).
destruct D.
rewrite H in *.
rewrite H6 in *.
eapply forgetcopyOutSigList_copyout_correct with (local_f:= a_env); eauto.
apply copyOut_f_correct; auto.
destruct H6, H6, H7.
destruct H7.
rewrite H in H7; inversion H7.
rewrite H in H7; inversion H7.
destruct H7.
rewrite H in H7; inversion H7.
rewrite H in H7; inversion H7.
inversion H1.
simpl in *.
assert (D:= envpartialViewGetIdsSome f_name e b_env H3).
destruct D.
rewrite H in *.
rewrite H6 in *.
inversion H4; subst.
eapply forgetcopyOutExternalWithoutSigList_correct with (f_env:= a_env) (params:=f_params); eauto.
apply copyOutExternalWithoutSig_f_correct; auto.
destruct H6, H6, H7.
destruct H7.
rewrite H in H7; inversion H7.
rewrite H6 in H4; inversion H4; subst.
eapply forgetcopyOutExternalWithoutSigList_correct with (f_env:= a_env) (params:=f_params); eauto.
apply copyOutExternalWithoutSig_f_correct; auto.
destruct H7; rewrite H7 in H; inversion H.
rewrite H6 in H4; inversion H4; subst.
eapply forgetcopyOutExternalWithoutSigList_correct with (f_env:= a_env) (params:=f_params); eauto.
apply copyOutExternalWithoutSig_f_correct; auto.
inversion H1.
simpl in *.
assert (D:= envpartialViewGetIdsSome f_name e b_env H3).
destruct D.
rewrite H in *.
rewrite H6 in *.
inversion H4; subst.
eapply forgetcopyOutExternalWithoutSigList_correct with (f_env:= a_env) (params:=[]); eauto.
apply copyOutExternalWithoutSig_f_correct; auto.
destruct H6, H6; rewrite H6 in H4; inversion H4; subst; eapply forgetcopyOutExternalWithoutSigList_correct with (f_env:= a_env) (params:=[]); eauto; apply copyOutExternalWithoutSig_f_correct; auto.
inversion H1.
simpl in *; inversion H1; subst; simpl in *; inversion H2; subst; auto.
simpl in *; inversion H1; subst; simpl in *; inversion H2; subst; auto.
simpl in *; inversion H1; subst; simpl in *; inversion H2; subst; auto.
simpl in *; inversion H1; subst; simpl in *; inversion H2; subst; auto.
simpl in *; inversion H1; subst; simpl in *; inversion H2; subst; auto.
simpl in H2; inversion H2 ; subst; simpl in *; eapply envpartialViewForgetVsSet; eauto.
simpl in H2; inversion H2; subst; simpl in *; eapply envpartialViewForgetVsSet; eauto.
inversion H.
simpl in *; inversion H1; subst; simpl in *; inversion H2; subst; auto.
simpl in H4; inversion H4; subst; simpl in *; eapply envpartialViewForgetVsSet; eauto.
simpl in *; inversion H1; subst; simpl in *; inversion H2; subst; auto.
simpl in *; inversion H1; subst; simpl in *; inversion H2; subst; auto.
simpl in *; inversion H1; subst; simpl in *; inversion H2; subst; auto.
simpl in H5; inversion H5; subst; simpl in *; eapply envpartialViewForgetVsSet; eauto.
simpl in H4; inversion H4; subst; simpl in *.
case_eq (forgetIdsValue t e); intros; rewrite H6 in *; [|inversion H5].
eapply envpartialViewForgetVsSet with (ids:=valide) (v:=(Bool true)) (e:=e1) (e':=a_env'); eauto.
eapply envpartialViewForgetVsSet; eauto.
simpl in H3; inversion H3; subst; simpl in *.
case_eq (forgetIdsValue t e); intros; rewrite H5 in *; [|inversion H4].
eapply envpartialViewForgetVsSet with (e:=e1); eauto.
apply envpartialViewForgetVsNothing with (ids:=t) (e:=e); eauto.
simpl in H4; inversion H4; subst; simpl in *.
case_eq (forgetIdsValue t e); intros; rewrite H6 in *; [|inversion H5].
eapply envpartialViewForgetVsSet with (ids:=valide) (v:=(Bool true)) (e:=e1) (e':=a_env'); eauto.
eapply envpartialViewForgetVsSet; eauto.
simpl in H3; inversion H3; subst; simpl in *.
case_eq (forgetIdsValue t e); intros; rewrite H5 in *; [|inversion H4].
eapply envpartialViewForgetVsSet with (e:=e1); eauto.
apply envpartialViewForgetVsNothing with (ids:=t) (e:=e); eauto.
simpl in H6; inversion H6; subst; simpl in *; eapply envpartialViewForgetVsSet; eauto.
simpl in H7; inversion H7; subst; simpl in *; eapply envpartialViewForgetVsSet; eauto.
simpl in H3; inversion H3; subst; simpl in *; eapply envpartialViewForgetVsSet; eauto.
simpl in H3; inversion H3; subst; simpl in *; eapply envpartialViewForgetVsSet; eauto.
simpl in H3; inversion H3; subst; simpl in *; eapply envpartialViewForgetVsSet; eauto.
simpl in H3; inversion H3; subst; simpl in *; eapply envpartialViewForgetVsSet; eauto.
Qed. *)

Fixpoint forgetFinisherInstruction e := match e with
| IIf cond ifs els => fun x => (forgetFinisherInstruction_s ifs (forgetFinisherElif_instruction els x))
| ICase e c => forgetFinisherCase_instruction c
| ILoop _ i => forgetFinisherInstruction_s i
| ICallDefinedFunction f_name _ => forgetGlobalsAndExternals
| _ => (fun x => x)
end 
with forgetFinisherElif_instruction e := match e with
| IElse i => forgetFinisherInstruction_s i
| IElif cond ifs els => fun x => (forgetFinisherInstruction_s ifs (forgetFinisherElif_instruction els x))
end
with forgetFinisherCase_instruction e := match e with
| ICaseFinishDefault i => forgetFinisherInstruction_s i
| ICaseCons (_, ifs) els => fun x => (forgetFinisherInstruction_s ifs (forgetFinisherCase_instruction els x))
end
with forgetFinisherInstruction_s e := match e with
| ISeqEnd i => forgetFinisherInstruction i
| ISeqCons h t => fun x => (forgetFinisherInstruction_s t ( forgetFinisherInstruction h x))
end.

Fixpoint forgetListInstruction env f := match f with
| IAssign sc _ => Some [sc]
| IIf _ iifi iel => 
    (
      match forgetListInstruction_s env iifi, forgetListElif_instruction env iel with
      | Some iffl, Some iell => Some (iffl ++ iell)
      | _, _ => None
      end
    )
| ICase _ c => forgetListCase_instruction env c
| ILoop _ c => forgetListInstruction_s env c
| ICallDefinedFunction f_name None => 
    (
      match getIdsValue f_name env with
      | Some (DefinedFunction fn) => 
          forgetcopyOutSigList fn.(f_args) []
      | Some (FunctionDeclaration sig) => 
          forgetcopyOutSigList sig []
      | _ => 
          Some (forgetcopyOutExternalWithoutSigList [])
      end
    )
| ICallDefinedFunction f_name (Some p) => 
    (
      match getIdsValue f_name env with
      | Some (DefinedFunction fn) => 
          forgetcopyOutSigList fn.(f_args) p
      | Some (FunctionDeclaration sig) => 
          forgetcopyOutSigList sig p
      | _ => 
          Some (forgetcopyOutExternalWithoutSigList p)
      end
    )
| (SOInstructionCompareM status m0 m1 t) => Some [status]
| (SOInstructionCompareR status e1 e2 e3 e4 e5 e6 t) => Some [status]
| (SOInstructionInit var (Some e)) => Some [var]
| (SOInstructionReadHardInput t e n_config no_t) => Some [t]
| (SOInstructionReadMess t valide e no_msg no_t no_fic no_bx) => Some [valide; t]
| (SOInstructionReadMessUnfil t valide e no_msg no_fic no_bx) => Some [valide; t]
| (SOInstructionWrite t i x None) => Some [t]
| (SOInstructionWrite t i j (Some x)) => Some [t]
| (SOInstructionWriteHardOutput s t no_config no_t)  => Some [s]
| (SOInstructionWriteMess s t no_msg no_fic no_bx) => Some [s]
| (SOInstructionWriteOutvar z x no_fic no_bx no_t) => Some [z]
| (SOInstructionWriteRedund r t no_fic no_bx no_t) => Some [r]
| _ => Some []
end
with forgetListElif_instruction env f := match f with
| IElse e => forgetListInstruction_s env e
| IElif _ iifi iel => 
    (
      match forgetListInstruction_s env iifi, forgetListElif_instruction env iel with
      | Some iffl, Some iell => Some (iffl ++ iell)
      | _, _ => None
      end
    )
end
with forgetListCase_instruction env f := match f with
| ICaseFinishDefault e => forgetListInstruction_s env e
| ICaseCons (_, iifi) iel => 
    (
      match forgetListInstruction_s env iifi, forgetListCase_instruction env iel with
      | Some iffl, Some iell => Some (iffl ++ iell)
      | _, _ => None
      end
    )
end
with forgetListInstruction_s env f := match f with
| ISeqEnd e => forgetListInstruction env e
| ISeqCons h t => 
    (
      match forgetListInstruction_s env t, forgetListInstruction env h with
      | Some lst, Some frt => Some (lst ++ frt)
      | _, _ => None
      end
    )
end.

Lemma setMapOrder : forall m t v,
StringMapModule.get t m = Some (VariableValue v) -> 
StringMapModule.set t (VariableValue Undefined)
  (StringMapModule.map
     (fun v0 : ConstOrVar =>
      match v0 with
      | ConstantValue va => ConstantValue va
      | VariableValue _ => VariableValue Undefined
      end) m) =
StringMapModule.map
  (fun v0 : ConstOrVar =>
   match v0 with
   | ConstantValue va => ConstantValue va
   | VariableValue _ => VariableValue Undefined
   end) (StringMapModule.set t (VariableValue Undefined) m).
Proof.
induction m.
simpl; auto.
simpl.
induction a.
intro.
case_eq (String_Equality.eq t a); simpl; auto.
intros.
inversion H0.
subst.
rewrite H; auto.
intros.
rewrite H; auto.
f_equal; auto.
eauto.
Qed.

Lemma setMapOrder' : forall m t v,
StringMapModule.get t m = Some (Environment.EVar (VariableValue v)) -> 
StringMapModule.set t (Environment.EVar (VariableValue Undefined))
  (StringMapModule.map
     (fun v0 : External =>
      match v0 with
      | Environment.EVar (VariableValue _) => Environment.EVar (VariableValue Undefined)
      | _ => v0
      end) m) =
StringMapModule.map
  (fun v0 : External =>
   match v0 with
      | Environment.EVar (VariableValue _) => Environment.EVar (VariableValue Undefined)
      | _ => v0
   end) (StringMapModule.set t (Environment.EVar (VariableValue Undefined)) m).
Proof.
induction m.
simpl; auto.
simpl.
induction a.
intro.
case_eq (String_Equality.eq t a); simpl; auto.
intros.
inversion H0.
subst.
rewrite H; auto.
intros.
rewrite H; auto.
f_equal; auto.
eauto.
Qed.

(* Here
 *)
Lemma setMapMapOrder : forall m t t0 t1 v,
StringMapModule.get t m = Some t1 ->
StringMapModule.get t0 t1 = Some (Environment.EVar (VariableValue v)) ->
StringMapModule.set t
  (StringMapModule.set t0 (Environment.EVar (VariableValue Undefined))
     (StringMapModule.map
        (fun v0 : External =>
         match v0 with
         | Environment.EVar (VariableValue _) => Environment.EVar (VariableValue Undefined)
         | _ => v0
         end) t1))
  (StringMapModule.map
     (fun m : StringMapModule.t External =>
      StringMapModule.map
        (fun v0 : External =>
         match v0 with
         | Environment.EVar (VariableValue _) => Environment.EVar (VariableValue Undefined)
         | _ => v0
         end) m) m) =
StringMapModule.map
  (fun m : StringMapModule.t External =>
   StringMapModule.map
     (fun v0 : External =>
      match v0 with
      | Environment.EVar (VariableValue _) => Environment.EVar (VariableValue Undefined)
      | _ => v0
      end) m)
  (StringMapModule.set t
     (StringMapModule.set t0 (Environment.EVar (VariableValue Undefined)) t1) 
     m).
Proof.
induction m.
simpl.
intros.
inversion H.
simpl.
induction a.
simpl.
simpl.
intro.
case_eq (String_Equality.eq t a).
intros.
inversion H0; subst.
simpl.
f_equal.
f_equal.
eapply setMapOrder'; eauto.
simpl.
simpl.
intros.
f_equal.
eauto.
Qed.

Lemma setMapOrder'' : forall m t,
StringMapModule.get t m = None -> 
StringMapModule.set t (Environment.EVar (VariableValue Undefined))
  (StringMapModule.map
     (fun v0 : External =>
      match v0 with
      | Environment.EVar (VariableValue _) => Environment.EVar (VariableValue Undefined)
      | _ => v0
      end) m) =
StringMapModule.map
  (fun v0 : External =>
   match v0 with
      | Environment.EVar (VariableValue _) => Environment.EVar (VariableValue Undefined)
      | _ => v0
   end) (StringMapModule.set t (Environment.EVar (VariableValue Undefined)) m).
Proof.
induction m.
simpl; auto.
simpl.
induction a.
intro.
case_eq (String_Equality.eq t a); simpl; auto.
intros.
inversion H0.
intros.
rewrite H; auto.
f_equal; auto.
Qed.

Lemma setMapMapOrder' : forall m t t0 t1,
StringMapModule.get t m = Some t1 ->
StringMapModule.get t0 t1 = None ->
StringMapModule.set t
  (StringMapModule.set t0 (Environment.EVar (VariableValue Undefined))
     (StringMapModule.map
        (fun v0 : External =>
         match v0 with
         | Environment.EVar (VariableValue _) => Environment.EVar (VariableValue Undefined)
         | _ => v0
         end) t1))
  (StringMapModule.map
     (fun m : StringMapModule.t External =>
      StringMapModule.map
        (fun v0 : External =>
         match v0 with
         | Environment.EVar (VariableValue _) => Environment.EVar (VariableValue Undefined)
         | _ => v0
         end) m) m) =
StringMapModule.map
  (fun m : StringMapModule.t External =>
   StringMapModule.map
     (fun v0 : External =>
      match v0 with
      | Environment.EVar (VariableValue _) => Environment.EVar (VariableValue Undefined)
      | _ => v0
      end) m)
  (StringMapModule.set t
     (StringMapModule.set t0 (Environment.EVar (VariableValue Undefined)) t1) 
     m).
Proof.
induction m.
simpl.
intros.
inversion H.
simpl.
induction a.
simpl.
simpl.
intro.
case_eq (String_Equality.eq t a).
intros.
inversion H0; subst.
simpl.
f_equal.
f_equal.
eapply setMapOrder''; eauto.
simpl.
simpl.
intros.
f_equal.
eauto.
Qed.

Lemma setMapMapOrder'' : forall m t t0,
StringMapModule.get t m = None ->
StringMapModule.set t [(t0, Environment.EVar (VariableValue Undefined))]
  (StringMapModule.map
     (fun m : StringMapModule.t External =>
      StringMapModule.map
        (fun v : External =>
         match v with
         | Environment.EVar (VariableValue _) => Environment.EVar (VariableValue Undefined)
         | _ => v
         end) m) m) =
StringMapModule.map
  (fun m : StringMapModule.t External =>
   StringMapModule.map
     (fun v : External =>
      match v with
      | Environment.EVar (VariableValue _) => Environment.EVar (VariableValue Undefined)
      | _ => v
      end) m)
  (StringMapModule.set t [(t0, Environment.EVar (VariableValue Undefined))] m).
Proof.
induction m.
simpl; auto.
simpl.
induction a.
simpl.
intro.
case_eq (String_Equality.eq t a); intros.
inversion H0.
simpl.
f_equal.
eauto.
Qed.

Lemma forgetIdsFSome : forall a f env env',
  (f = forgetGlobalsAndExternals \/ f = fun x => x) ->
  forgetIdsValue a env = Some env' ->
  forgetIdsValue a (f env) = Some (f env').
Proof.
unfold forgetIdsValue.
intros.
case_eq a; intros; rewrite H1 in *.
inversion H0.
case_eq l; intros; rewrite H2 in *.
unfold forgetIdValue in *.
case_eq (StringMapModule.get t (localVars env)); intros; rewrite H3 in *.
induction c; inversion H0; subst.
destruct H.
rewrite H.
simpl.
rewrite H3; auto.
rewrite H, H3; auto.
destruct H.
rewrite H.
simpl.
rewrite H3; auto.
rewrite H, H3; auto.
destruct H.
rewrite H.
simpl.
rewrite H3; auto.
case_eq (StringMapModule.get t (globalVars env)); intros; rewrite H4 in *.
induction c; inversion H0; subst.
rewrite StringMapModule.gmap.
rewrite H4; auto.
rewrite StringMapModule.gmap.
rewrite H4; auto.
unfold forgetGlobalsAndExternals.
simpl.
f_equal.
f_equal.
eapply setMapOrder; eauto.
inversion H0.
rewrite H; simpl.
rewrite H3; auto.
case_eq (StringMapModule.get t (localVars env)); intros; rewrite H3 in *.
induction c; inversion H0; subst.
destruct H.
rewrite H.
simpl.
rewrite H3; auto.
rewrite H, H3; auto.
destruct H.
rewrite H.
simpl.
rewrite H3; auto.
rewrite H, H3; auto.
case_eq (StringMapModule.get t (globalVars env)); intros; rewrite H4 in *.
induction c; inversion H0; subst.
destruct H.
rewrite H.
simpl.
rewrite H3.
rewrite StringMapModule.gmap.
rewrite H4; auto.
rewrite H.
simpl.
rewrite H3.
rewrite H4; auto.
destruct H.
rewrite H.
simpl.
rewrite H3.
rewrite StringMapModule.gmap.
simpl.
rewrite H4; auto.
unfold forgetGlobalsAndExternals.
simpl.
f_equal.
f_equal.
eapply setMapOrder; eauto.
rewrite H.
simpl.
rewrite H3.
rewrite H4; auto.
case_eq (StringMapModule.get t (funDefs env)); intros; rewrite H5 in *; [inversion H0|].
case_eq (StringMapModule.get t (localfunDecls env)); intros; rewrite H6 in *; [inversion H0|].
case_eq (StringMapModule.get t (externals env)); intros; rewrite H7 in *; [|inversion H0].
case_eq (StringMapModule.get t0 t1); intros; rewrite H8 in *.
induction e; [|inversion H0].
induction c; inversion H0; subst.
destruct H.
rewrite H.
simpl.
rewrite H3.
rewrite StringMapModule.gmap.
rewrite H4, H5, H6.
rewrite StringMapModule.gmap.
rewrite H7.
rewrite StringMapModule.gmap.
rewrite H8.
auto.
rewrite H.
simpl.
rewrite H3, H4, H5, H6, H7, H8; auto.
destruct H.
rewrite H.
simpl.
rewrite H3.
rewrite StringMapModule.gmap.
rewrite H4, H5, H6.
rewrite StringMapModule.gmap.
rewrite H7.
rewrite StringMapModule.gmap.
rewrite H8.
unfold forgetGlobalsAndExternals.
simpl.
f_equal.
f_equal.
eapply setMapMapOrder; eauto.
rewrite H.
rewrite H3, H4, H5, H6, H7, H8; auto.
inversion H0; subst.
destruct H.
rewrite H.
simpl.
rewrite H3.
rewrite StringMapModule.gmap.
rewrite H4, H5, H6.
rewrite StringMapModule.gmap.
rewrite H7.
rewrite StringMapModule.gmap.
rewrite H8.
unfold forgetGlobalsAndExternals.
simpl.
f_equal.
f_equal.
eapply setMapMapOrder'; eauto.
rewrite H.
rewrite H3, H4, H5, H6, H7, H8; auto.
destruct H.
rewrite H.
simpl.
rewrite H3.
rewrite StringMapModule.gmap.
rewrite H4, H5, H6.
rewrite StringMapModule.gmap.
rewrite H7.
unfold forgetGlobalsAndExternals.
simpl.
f_equal.
f_equal.
eapply setMapMapOrder''; eauto.
rewrite H.
rewrite H3, H4, H5, H6, H7; auto.
Qed.

Lemma forgetIdsFNone : forall a f env,
  (f = forgetGlobalsAndExternals \/ f = fun x => x) ->
  forgetIdsValue a env = None ->
  forgetIdsValue a (f env) = None.
Proof.
unfold forgetIdsValue.
intros.
case_eq a; intros; rewrite H1 in *; auto.
case_eq l; intros; rewrite H2 in *.
unfold forgetIdValue in *.
case_eq (StringMapModule.get t (localVars env)); intros; rewrite H3 in *.
induction c; inversion H0.
case_eq (StringMapModule.get t (globalVars env)); intros; rewrite H4 in *.
induction c; inversion H0.
destruct H.
rewrite H.
simpl.
rewrite H3.
rewrite StringMapModule.gmap.
rewrite H4; auto.
rewrite H.
simpl.
rewrite H3, H4; auto.
case_eq (StringMapModule.get t (localVars env)); intros; rewrite H3 in *.
induction c; inversion H0.
case_eq (StringMapModule.get t (globalVars env)); intros; rewrite H4 in *.
induction c; inversion H0.
case_eq (StringMapModule.get t (funDefs env)); intros; rewrite H5 in *.
destruct H.
rewrite H.
simpl.
rewrite H3.
rewrite StringMapModule.gmap.
rewrite H4, H5; auto.
rewrite H; simpl; rewrite H3, H4, H5; auto.
case_eq (StringMapModule.get t (localfunDecls env)); intros; rewrite H6 in *.
destruct H.
rewrite H.
simpl.
rewrite H3.
rewrite StringMapModule.gmap.
rewrite H4, H5, H6; auto.
rewrite H; simpl; rewrite H3, H4, H5, H6; auto.
case_eq (StringMapModule.get t (externals env)); intros; rewrite H7 in *.
case_eq (StringMapModule.get t0 t1); intros; rewrite H8 in *.
induction e.
induction c; inversion H0.
destruct H.
rewrite H.
simpl.
rewrite H3.
rewrite StringMapModule.gmap.
rewrite H4, H5, H6; auto.
rewrite StringMapModule.gmap.
rewrite H7.
rewrite StringMapModule.gmap.
rewrite H8; auto.
rewrite H; simpl; rewrite H3, H4, H5, H6, H7, H8; auto.
inversion H0.
inversion H0.
Qed.

Lemma forgetListConcat : forall l0 l1 f env env',
  (f = forgetGlobalsAndExternals \/ f = fun x => x) ->
  forgetIdsValueList l1 env = Some env' ->
  forgetIdsValueList l0 (f env') = forgetIdsValueList (l0 ++ l1) (f env).
Proof.
induction l0.
simpl.
induction l1.
simpl in *.
intros.
inversion H0; subst; auto.
intros.
simpl in H0.
simpl.
case_eq (forgetIdsValueList l1 env); intros; rewrite H1 in H0; [|inversion H0].
eapply IHl1 in H1; eauto.
rewrite <- H1.
apply eq_sym.
apply forgetIdsFSome; auto.
simpl.
intros.
case_eq (forgetIdsValueList l0 (f env')); intros.
assert (D:=IHl0 l1 f env env' H H0).
rewrite <- D.
rewrite H1.
auto.
assert (D:=IHl0 l1 f env env' H H0).
rewrite <- D.
rewrite H1.
auto.
Qed.

Lemma forgetListPartial : forall l0 f env env',
  (f = forgetGlobalsAndExternals \/ f = fun x => x) ->
  forgetIdsValueList l0 (f env) = Some env' ->
  envpartialView env' env.
Proof.
intros.
assert (envpartialView env' (f env)).
eapply (envpartialViewForgetList l0); eauto.
eapply envpartialView_trans; eauto.
destruct H.
rewrite H.
apply envpartialViewforgetGlobalsAndExternals.
rewrite H.
apply envpartialView_refl.
Qed.

Require Import Logic.FunctionalExtensionality.

Theorem simpl_GE_GE : (fun e => forgetGlobalsAndExternals (forgetGlobalsAndExternals e)) = forgetGlobalsAndExternals.
Proof.
apply functional_extensionality.
intros.
unfold forgetGlobalsAndExternals.
simpl.
intros.
f_equal; auto.
apply forgetGlob.
apply forgetExt.
Qed.

Theorem simplForgetFinisherInstruction : forall i,
  forgetFinisherInstruction i = forgetGlobalsAndExternals \/
  forgetFinisherInstruction i = fun x => x 
with simplForgetFinisherElif_instruction : forall i,
  forgetFinisherElif_instruction i = forgetGlobalsAndExternals \/
  forgetFinisherElif_instruction i = fun x => x 
with simplForgetFinisherCase_instruction : forall i,
  forgetFinisherCase_instruction i = forgetGlobalsAndExternals \/
  forgetFinisherCase_instruction i = fun x => x
with simplForgetFinisherInstruction_s : forall i,
  forgetFinisherInstruction_s i = forgetGlobalsAndExternals \/
  forgetFinisherInstruction_s i = fun x => x .
Proof.
induction i; simpl; auto.
assert (D := simplForgetFinisherElif_instruction e).
destruct D.
rewrite H.
assert (D' := simplForgetFinisherInstruction_s i).
destruct D'.
rewrite H0.
rewrite simpl_GE_GE; auto.
rewrite H0.
auto.
rewrite H; simpl; auto.
(* (* apply simplForgetFinisherCase_instruction.
apply simplForgetFinisherInstruction_s.
induction p; simpl; intros; auto.
 *) *)
induction i; simpl; intros.
apply simplForgetFinisherInstruction_s.
destruct IHi.
rewrite H.
assert (D' := simplForgetFinisherInstruction_s i).
destruct D'.
rewrite H0.
rewrite simpl_GE_GE; auto.
rewrite H0.
auto.
rewrite H; simpl; auto.

induction i; simpl; intros.
apply simplForgetFinisherInstruction_s.
induction p.
destruct IHi.
rewrite H.
assert (D' := simplForgetFinisherInstruction_s b).
destruct D'.
rewrite H0.
rewrite simpl_GE_GE; auto.
rewrite H0; auto.
rewrite H.
auto.

induction i; simpl; intros.
apply simplForgetFinisherInstruction.
destruct IHi.
rewrite H.
assert (D' := simplForgetFinisherInstruction i).
destruct D'.
rewrite H0.
rewrite simpl_GE_GE; auto.
rewrite H0; auto.
rewrite H; auto.
Qed.

Lemma partialEnvForgetListIdemPotent_FunCall : forall f e e',
  envpartialView e e' -> 
  forgetFunCallList e f = forgetFunCallList e' f.
Proof.
induction f; simpl; auto.
induction o; simpl; auto.
intros.
assert (D := envpartialViewGetIdsSome l e e' H).
destruct D.
rewrite H0; auto.
destruct H0.
destruct H0.
destruct H1.
destruct H1.
rewrite H0, H1; auto.
rewrite H0, H1; auto.
destruct H0, H1.
destruct H1.
rewrite H0, H1; auto.
rewrite H0, H1; auto.
intros.
assert (D := envpartialViewGetIdsSome l e e' H).
destruct D.
rewrite H0; auto.
destruct H0, H0, H1.
destruct H1.
rewrite H0, H1; auto.
rewrite H0, H1; auto.
destruct H1.
rewrite H0, H1; auto.
rewrite H0, H1; auto.
Qed.

Lemma partialEnvForgetListIdemPotent_Instruction : forall i e e',
  envpartialView e e' -> 
  forgetListInstruction e i = forgetListInstruction e' i
with partialEnvForgetListIdemPotent_Elif_instruction : forall i e e',
  envpartialView e e' -> 
  forgetListElif_instruction e i = forgetListElif_instruction e' i
with partialEnvForgetListIdemPotent_Case_instruction : forall i e e',
  envpartialView e e' -> 
  forgetListCase_instruction e i = forgetListCase_instruction e' i
with partialEnvForgetListIdemPotent_Instruction_s : forall i e e',
  envpartialView e e' -> 
  forgetListInstruction_s e i = forgetListInstruction_s e' i.
Proof.
induction i; simpl; auto.
intros.
erewrite partialEnvForgetListIdemPotent_Instruction_s; eauto.
erewrite partialEnvForgetListIdemPotent_Elif_instruction; eauto.

intros.
induction o; simpl; auto.
assert (D := envpartialViewGetIdsSome l e e' H).
destruct D.
rewrite H0; auto.
destruct H0.
destruct H0.
destruct H1.
destruct H1.
rewrite H0, H1; auto.
rewrite H0, H1; auto.
destruct H0, H1.
destruct H1.
rewrite H0, H1; auto.
rewrite H0, H1; auto.
assert (D := envpartialViewGetIdsSome l e e' H).
destruct D.
rewrite H0; auto.
destruct H0, H0, H1.
destruct H1.
rewrite H0, H1; auto.
rewrite H0, H1; auto.
destruct H1.
rewrite H0, H1; auto.
rewrite H0, H1; auto.

induction i; simpl.
apply partialEnvForgetListIdemPotent_Instruction_s.
intros.
erewrite partialEnvForgetListIdemPotent_Instruction_s; eauto.
erewrite IHi; eauto.
induction i; simpl.
apply partialEnvForgetListIdemPotent_Instruction_s.
induction p; intros.
erewrite partialEnvForgetListIdemPotent_Instruction_s; eauto.
erewrite IHi; eauto.
induction i; simpl.
apply partialEnvForgetListIdemPotent_Instruction.
intros.
erewrite partialEnvForgetListIdemPotent_Instruction; eauto.
erewrite IHi; eauto.
Qed.

(* Lemma partialEnvForgetFinisherIdemPotent_Instruction : forall i e e',
  envpartialView e e' -> 
  forgetFinisherInstruction i = forgetFinisherInstruction i
with partialEnvForgetFinisherIdemPotent_Elif_instruction : forall i e e',
  envpartialView e e' -> 
  forgetFinisherElif_instruction e i = forgetFinisherElif_instruction e' i
with partialEnvForgetFinisherIdemPotent_Case_instruction : forall i e e',
  envpartialView e e' -> 
  forgetFinisherCase_instruction e i = forgetFinisherCase_instruction e' i
with partialEnvForgetFinisherIdemPotent_Instruction_s : forall i e e',
  envpartialView e e' -> 
  forgetFinisherInstruction_s e i = forgetFinisherInstruction_s e' i.
Proof.
induction i; simpl; auto.
intros.
erewrite partialEnvForgetFinisherIdemPotent_Instruction_s; eauto.
erewrite partialEnvForgetFinisherIdemPotent_Elif_instruction; eauto.
intros.
induction f; simpl; auto.
assert (D := envpartialViewGetIdsSome l e e' H).
destruct D.
rewrite H0; auto.
destruct H0.
destruct H1.
destruct H1.
rewrite H0, H1; auto.
rewrite H0, H1; auto.
induction i; simpl.
apply partialEnvForgetFinisherIdemPotent_Instruction_s.
intros.
erewrite partialEnvForgetFinisherIdemPotent_Instruction_s; eauto.
erewrite IHi; eauto.
induction i; simpl.
apply partialEnvForgetFinisherIdemPotent_Instruction_s.
induction p; intros.
erewrite partialEnvForgetFinisherIdemPotent_Instruction_s; eauto.
erewrite IHi; eauto.
induction i; simpl.
apply partialEnvForgetFinisherIdemPotent_Instruction.
intros.
erewrite partialEnvForgetFinisherIdemPotent_Instruction; eauto.
erewrite IHi; eauto.
Qed. *)

Lemma forgetListConcatNone : forall l0 l1 f env,
  (f = forgetGlobalsAndExternals \/ f = fun x => x) ->
  forgetIdsValueList l1 env = None ->
  forgetIdsValueList (l0 ++ l1) (f env) = None.
Proof.
induction l0.
simpl.
induction l1.
simpl in *.
intros.
inversion H0; subst; auto.
intros.
simpl in H0.
simpl.
case_eq (forgetIdsValueList l1 env); intros; rewrite H1 in H0.
eapply forgetListConcat with (l0 := []) (f:=f) in H1.
simpl in H1.
rewrite <- H1.
apply forgetIdsFNone; auto.
auto.
eapply IHl1 in H1; eauto.
rewrite H1.
auto.
simpl.
intros.
eapply IHl0 in H0; eauto.
rewrite H0.
auto.
Qed.

Theorem simpl_GE_GE' : forall e, forgetGlobalsAndExternals (forgetGlobalsAndExternals e) = forgetGlobalsAndExternals e.
Proof.
intros.
unfold forgetGlobalsAndExternals.
simpl.
intros.
f_equal; auto.
apply forgetGlob.
apply forgetExt.
Qed.

Lemma isIdsforgottenConserved : forall f i e,
    (f = forgetGlobalsAndExternals \/ f = fun x => x) ->
    isIdsforgotten i e = true ->
    isIdsforgotten i (f e) = true.
Proof.
unfold isIdsforgotten.
intros.
case_eq i; intros; rewrite H1 in *; auto.
unfold getIdValue in *.
case_eq l; intros; rewrite H2 in *.
case_eq (StringMapModule.get t (localVars e)); intros; rewrite H3 in H0.
destruct H.
rewrite H in *; simpl.
rewrite H3; auto.
rewrite H in *; simpl; rewrite H3; auto.
case_eq (StringMapModule.get t (globalVars e)); intros; rewrite H4 in H0.
destruct H.
rewrite H in *; simpl.
rewrite H3; auto.
rewrite StringMapModule.gmap.
rewrite H4; auto.
induction c; auto.
rewrite H in *; simpl; rewrite H3, H4; auto.
case_eq (StringMapModule.get t (funDefs e)); intros; rewrite H5 in H0; [inversion H0|].
case_eq (StringMapModule.get t (localfunDecls e)); intros; rewrite H6 in H0; inversion H0.
case_eq (StringMapModule.get t (localVars e)); intros; rewrite H3 in H0.
destruct H.
rewrite H in *; simpl.
rewrite H3; auto.
rewrite H in *; simpl; rewrite H3; auto.
case_eq (StringMapModule.get t (globalVars e)); intros; rewrite H4 in H0.
destruct H.
rewrite H in *; simpl.
rewrite H3; auto.
rewrite StringMapModule.gmap.
rewrite H4; auto.
induction c; auto.
rewrite H in *; simpl; rewrite H3, H4; auto.
case_eq (StringMapModule.get t (funDefs e)); intros; rewrite H5 in H0; [inversion H0|].
case_eq (StringMapModule.get t (localfunDecls e)); intros; rewrite H6 in H0; [inversion H0|].
case_eq (StringMapModule.get t (externals e)); intros; rewrite H7 in H0; [|inversion H0].
case_eq (StringMapModule.get t0 t1); intros; rewrite H8 in H0; [|inversion H0].
case_eq e0; intros; rewrite H9 in *; [|inversion H0].
destruct H.
rewrite H in *; simpl.
rewrite H3.
rewrite StringMapModule.gmap.
rewrite H4, H5, H6.
rewrite StringMapModule.gmap.
rewrite H7.
rewrite StringMapModule.gmap.
rewrite H8.
induction c; auto.
rewrite H in *; simpl; rewrite H3, H4, H5, H6, H7, H8; auto.
Qed.

Lemma forgetListIdemPoten : forall l f env env',
    (f = forgetGlobalsAndExternals \/ f = fun x => x) ->
    forgetIdsValueList l (f env) = Some env' ->
    forgetIdsValueList l (f env') = Some env'.
Proof.
intros.
assert (D := correctSetIsForgottenList l (f env) env' H0).
assert (forall i, In i l -> isIdsforgotten i (f env') = true).
intros.
apply D in H1.
apply isIdsforgottenConserved; auto.
assert (D' := correctIdemPotentIsForgottenListIn l (f env') H1).
destruct H.
rewrite H in *.
assert (D'' := forgetGlobalsAndExternalsForgetListIdemPoten l env env' H0).
replace (Some env') with (Some (forgetGlobalsAndExternals env')); auto.
f_equal; auto.
rewrite D'.
rewrite H in *; auto.
Qed.

Theorem forgetListInstruction_correct : forall in_data f res,
  evalInstruction in_data f res ->
  extractErrorType res = None -> forall e l slim_e,
  envpartialView e (fst in_data) ->
  forgetListInstruction e f = Some l ->
  forgetIdsValueList l (forgetFinisherInstruction f e) = Some slim_e ->
  envpartialView slim_e (extractEnv res)
with forgetListElif_instruction_correct : forall in_data f res,
  evalElif_instruction in_data f res ->
  extractErrorType res = None -> forall e l slim_e,
  envpartialView e (fst in_data) ->
  forgetListElif_instruction e f = Some l ->
  forgetIdsValueList l (forgetFinisherElif_instruction f e) = Some slim_e ->
  envpartialView slim_e (extractEnv res)
with forgetListCase_instruction_correct : forall in_data c f res,
  evalCase_instruction in_data c f res ->
  extractErrorType res = None -> forall e l slim_e,
  envpartialView e (fst in_data) ->
  forgetListCase_instruction e f = Some l ->
  forgetIdsValueList l (forgetFinisherCase_instruction f e) = Some slim_e ->
  envpartialView slim_e (extractEnv res)
with forgetListInstruction_s_correct : forall in_data f res,
  evalInstruction_s in_data f res ->
  extractErrorType res = None -> forall e l slim_e,
  envpartialView e (fst in_data) ->
  forgetListInstruction_s e f = Some l ->
  forgetIdsValueList l (forgetFinisherInstruction_s f e) = Some slim_e ->
  envpartialView slim_e (extractEnv res).
Proof.
induction 1; simpl; intros; subst.
inversion H1; subst; inversion H2; subst; auto.
inversion H1; subst; inversion H2; subst; auto.
inversion H1; subst; inversion H2; subst; auto.
inversion H1; subst; inversion H2; subst; auto.
inversion H3; subst; simpl in H4.
eapply envpartialViewForgetVsSet; eauto.

(*If true*)
case_eq (forgetListInstruction_s e ins_if); intros; rewrite H5 in *; [|inversion H3].
case_eq (forgetListElif_instruction e ins_else); intros; rewrite H6 in *; [|inversion H3].
inversion H3; subst.
case_eq (forgetIdsValueList l1 (forgetFinisherElif_instruction ins_else e)); intros.
assert (D := forgetListPartial l1 (forgetFinisherElif_instruction ins_else) e e0 (simplForgetFinisherElif_instruction ins_else) H7).
case_eq (forgetIdsValueList l0 (forgetFinisherInstruction_s ins_if e0)); intros.
assert (envpartialView e0 env).
eapply envpartialView_trans; eauto.
erewrite <- partialEnvForgetListIdemPotent_Instruction_s in H5; eauto.
assert (D':=forgetListInstruction_s_correct (env, evn) ins_if res H0 H1 e0 l0 e1 H9 H5 H8).
assert (D'' := forgetListConcat l0 l1 (forgetFinisherInstruction_s ins_if) (forgetFinisherElif_instruction ins_else e) e0 (simplForgetFinisherInstruction_s ins_if) H7).
rewrite H4 in D''.
rewrite H8 in D''.
inversion D''; subst.
auto.
assert (D'' := forgetListConcat l0 l1 (forgetFinisherInstruction_s ins_if) (forgetFinisherElif_instruction ins_else e) e0 (simplForgetFinisherInstruction_s ins_if) H7).
rewrite H4 in D''.
rewrite H8 in D''.
inversion D''.
assert (D := forgetListConcatNone l0 l1 (forgetFinisherInstruction_s ins_if) (forgetFinisherElif_instruction ins_else e) (simplForgetFinisherInstruction_s ins_if) H7).
rewrite H4 in D.
inversion D.

(*if false*)
case_eq (forgetListInstruction_s e ins_if); intros; rewrite H5 in *; [|inversion H3].
case_eq (forgetListElif_instruction e ins_else); intros; rewrite H6 in *; [|inversion H3].
inversion H3; subst.
case_eq (forgetIdsValueList l1 (forgetFinisherElif_instruction ins_else e)); intros.
assert (D:=forgetListElif_instruction_correct (env, evn) ins_else res H0 H1 e l1 e0 H2 H6 H7).
case_eq (forgetIdsValueList l0 (forgetFinisherInstruction_s ins_if e0)); intros.
assert (D' := forgetListConcat l0 l1 (forgetFinisherInstruction_s ins_if) (forgetFinisherElif_instruction ins_else e) e0 (simplForgetFinisherInstruction_s ins_if) H7).
assert (envpartialView e0 e).
eapply forgetListPartial with (l0 := l1) (f := forgetFinisherElif_instruction ins_else); eauto.
apply simplForgetFinisherElif_instruction.
rewrite H8 in D'.
rewrite <- D' in H4.
inversion H4; subst.
assert (envpartialView slim_e e0).
eapply forgetListPartial with (l0 := l0) (f := forgetFinisherInstruction_s ins_if); eauto.
apply simplForgetFinisherInstruction_s.
eapply envpartialView_trans; eauto.
assert (D' := forgetListConcat l0 l1 (forgetFinisherInstruction_s ins_if) (forgetFinisherElif_instruction ins_else e) e0 (simplForgetFinisherInstruction_s ins_if) H7).
assert (envpartialView e0 e).
eapply forgetListPartial with (l0 := l1) (f := forgetFinisherElif_instruction ins_else); eauto.
apply simplForgetFinisherElif_instruction.
rewrite H4 in D'.
rewrite H8 in D'.
inversion D'.
assert (D := forgetListConcatNone l0 l1 (forgetFinisherInstruction_s ins_if) (forgetFinisherElif_instruction ins_else e) (simplForgetFinisherInstruction_s ins_if) H7).
rewrite H4 in D.
inversion D.
eapply forgetListCase_instruction_correct; eauto.

(*Loop True*)
assert (extractErrorType (OK env' evn') = None) by (simpl; auto).
assert (D:=forgetListInstruction_s_correct (env, evn) ins (OK env' evn') H0 H6 e l slim_e H3 H4 H5).
simpl in D.
simpl in IHevalInstruction.
assert (envpartialView slim_e e).
eapply forgetListPartial with (l0 := l) (f := forgetFinisherInstruction_s ins); eauto.
apply simplForgetFinisherInstruction_s.
assert (forgetListInstruction_s slim_e ins = Some l).
rewrite <- H4.
eapply partialEnvForgetListIdemPotent_Instruction_s; eauto.
assert (forgetIdsValueList l (forgetFinisherInstruction_s ins slim_e) =
     Some slim_e).
eapply forgetListIdemPoten; eauto.
apply simplForgetFinisherInstruction_s.
assert (D' := IHevalInstruction H2 slim_e l slim_e D H8 H9).
exact D'.

inversion H1.
assert (envpartialView slim_e e).
eapply forgetListPartial with (l0 := l) (f := forgetFinisherInstruction_s ins); eauto.
apply simplForgetFinisherInstruction_s.
eapply envpartialView_trans; eauto.

assert (D:= envpartialViewGetIdsSome f_name e b_env H5).
destruct D.
rewrite H in *.
rewrite H8 in *.
eapply forgetcopyOutSigList_copyout_correct with (local_f:= af_env); eauto.
apply copyOut_f_correct; auto.
destruct H8, H8, H9.
destruct H9.
rewrite H in H9; inversion H9.
rewrite H in H9; inversion H9.
destruct H9.
rewrite H in H9; inversion H9.
rewrite H in H9; inversion H9.
inversion H3.
assert (D:= envpartialViewGetIdsSome f_name e b_env H5).
destruct D.
rewrite H in *.
rewrite H8 in *.
eapply forgetcopyOutSigList_copyout_correct with (local_f:= af_env); eauto.
apply copyOut_f_correct; auto.
destruct H8, H8, H9.
destruct H9.
rewrite H in H9; inversion H9.
rewrite H in H9; inversion H9.
destruct H9.
rewrite H in H9; inversion H9.
rewrite H in H9; inversion H9.
inversion H3.
assert (D:= envpartialViewGetIdsSome f_name e b_env H3).
destruct D.
rewrite H in *.
rewrite H6 in *.
eapply forgetcopyOutSigList_copyout_correct with (local_f:= a_env); eauto.
apply copyOut_f_correct; auto.
destruct H6, H6, H7.
destruct H7.
rewrite H in H7; inversion H7.
rewrite H in H7; inversion H7.
destruct H7.
rewrite H in H7; inversion H7.
rewrite H in H7; inversion H7.
inversion H1.
assert (D:= envpartialViewGetIdsSome f_name e b_env H3).
destruct D.
rewrite H in *.
rewrite H6 in *.
eapply forgetcopyOutSigList_copyout_correct with (local_f:= a_env); eauto.
apply copyOut_f_correct; auto.
destruct H6, H6, H7.
destruct H7.
rewrite H in H7; inversion H7.
rewrite H in H7; inversion H7.
destruct H7.
rewrite H in H7; inversion H7.
rewrite H in H7; inversion H7.
inversion H1.
simpl in *.
assert (D:= envpartialViewGetIdsSome f_name e b_env H3).
destruct D.
rewrite H in *.
rewrite H6 in *.
inversion H4; subst.
eapply forgetcopyOutExternalWithoutSigList_correct with (f_env:= a_env) (params:=f_params); eauto.
apply copyOutExternalWithoutSig_f_correct; auto.
destruct H6, H6, H7.
destruct H7.
rewrite H in H7; inversion H7.
rewrite H6 in H4; inversion H4; subst.
eapply forgetcopyOutExternalWithoutSigList_correct with (f_env:= a_env) (params:=f_params); eauto.
apply copyOutExternalWithoutSig_f_correct; auto.
destruct H7; rewrite H7 in H; inversion H.
rewrite H6 in H4; inversion H4; subst.
eapply forgetcopyOutExternalWithoutSigList_correct with (f_env:= a_env) (params:=f_params); eauto.
apply copyOutExternalWithoutSig_f_correct; auto.
inversion H1.
simpl in *.
assert (D:= envpartialViewGetIdsSome f_name e b_env H3).
destruct D.
rewrite H in *.
rewrite H6 in *.
inversion H4; subst.
eapply forgetcopyOutExternalWithoutSigList_correct with (f_env:= a_env) (params:=[]); eauto.
apply copyOutExternalWithoutSig_f_correct; auto.
destruct H6, H6; rewrite H6 in H4; inversion H4; subst; eapply forgetcopyOutExternalWithoutSigList_correct with (f_env:= a_env) (params:=[]); eauto; apply copyOutExternalWithoutSig_f_correct; auto.
inversion H1.
simpl in *; inversion H1; subst; simpl in *; inversion H2; subst; auto.
simpl in *; inversion H1; subst; simpl in *; inversion H2; subst; auto.
simpl in *; inversion H1; subst; simpl in *; inversion H2; subst; auto.
simpl in *; inversion H1; subst; simpl in *; inversion H2; subst; auto.
simpl in *; inversion H1; subst; simpl in *; inversion H2; subst; auto.
simpl in H2; inversion H2 ; subst; simpl in *; eapply envpartialViewForgetVsSet; eauto.
simpl in H2; inversion H2; subst; simpl in *; eapply envpartialViewForgetVsSet; eauto.
inversion H.
simpl in *; inversion H1; subst; simpl in *; inversion H2; subst; auto.
simpl in H4; inversion H4; subst; simpl in *; eapply envpartialViewForgetVsSet; eauto.
simpl in *; inversion H1; subst; simpl in *; inversion H2; subst; auto.
simpl in *; inversion H1; subst; simpl in *; inversion H2; subst; auto.
simpl in *; inversion H1; subst; simpl in *; inversion H2; subst; auto.
simpl in H5; inversion H5; subst; simpl in *; eapply envpartialViewForgetVsSet; eauto.
simpl in H4; inversion H4; subst; simpl in *.
case_eq (forgetIdsValue t e0); intros; rewrite H6 in *; [|inversion H5].
eapply envpartialViewForgetVsSet with (ids:=valide) (v:=(Bool true)) (e:=e1) (e':=a_env'); eauto.
eapply envpartialViewForgetVsSet; eauto.
simpl in H3; inversion H3; subst; simpl in *.
case_eq (forgetIdsValue t e0); intros; rewrite H5 in *; [|inversion H4].
eapply envpartialViewForgetVsSet with (e:=e1); eauto.
apply envpartialViewForgetVsNothing with (ids:=t) (e:=e0); eauto.
simpl in H4; inversion H4; subst; simpl in *.
case_eq (forgetIdsValue t e0); intros; rewrite H6 in *; [|inversion H5].
eapply envpartialViewForgetVsSet with (ids:=valide) (v:=(Bool true)) (e:=e1) (e':=a_env'); eauto.
eapply envpartialViewForgetVsSet; eauto.
simpl in H3; inversion H3; subst; simpl in *.
case_eq (forgetIdsValue t e0); intros; rewrite H5 in *; [|inversion H4].
eapply envpartialViewForgetVsSet with (e:=e1); eauto.
apply envpartialViewForgetVsNothing with (ids:=t) (e:=e0); eauto.
simpl in H6; inversion H6; subst; simpl in *; eapply envpartialViewForgetVsSet; eauto.
simpl in H7; inversion H7; subst; simpl in *; eapply envpartialViewForgetVsSet; eauto.
simpl in H3; inversion H3; subst; simpl in *; eapply envpartialViewForgetVsSet; eauto.
simpl in H3; inversion H3; subst; simpl in *; eapply envpartialViewForgetVsSet; eauto.
simpl in H3; inversion H3; subst; simpl in *; eapply envpartialViewForgetVsSet; eauto.
simpl in H3; inversion H3; subst; simpl in *; eapply envpartialViewForgetVsSet; eauto.

(** ------ **)
induction 1; simpl; intros; subst.
eapply forgetListInstruction_s_correct; eauto.
(*If true*)
case_eq (forgetListInstruction_s e ins_true); intros; rewrite H5 in *; [|inversion H3].
case_eq (forgetListElif_instruction e ins_false); intros; rewrite H6 in *; [|inversion H3].
inversion H3; subst.
case_eq (forgetIdsValueList l1 (forgetFinisherElif_instruction ins_false e)); intros.
assert (D := forgetListPartial l1 (forgetFinisherElif_instruction ins_false) e e0 (simplForgetFinisherElif_instruction ins_false) H7).
case_eq (forgetIdsValueList l0 (forgetFinisherInstruction_s ins_true e0)); intros.
assert (envpartialView e0 env).
eapply envpartialView_trans; eauto.
erewrite <- partialEnvForgetListIdemPotent_Instruction_s in H5; eauto.
assert (D':=forgetListInstruction_s_correct (env, evn) ins_true res H0 H1 e0 l0 e1 H9 H5 H8).
assert (D'' := forgetListConcat l0 l1 (forgetFinisherInstruction_s ins_true) (forgetFinisherElif_instruction ins_false e) e0 (simplForgetFinisherInstruction_s ins_true) H7).
rewrite H4 in D''.
rewrite H8 in D''.
inversion D''; subst.
auto.
assert (D'' := forgetListConcat l0 l1 (forgetFinisherInstruction_s ins_true) (forgetFinisherElif_instruction ins_false e) e0 (simplForgetFinisherInstruction_s ins_true) H7).
rewrite H4 in D''.
rewrite H8 in D''.
inversion D''.
assert (D := forgetListConcatNone l0 l1 (forgetFinisherInstruction_s ins_true) (forgetFinisherElif_instruction ins_false e) (simplForgetFinisherInstruction_s ins_true) H7).
rewrite H4 in D.
inversion D.

(*if false*)
case_eq (forgetListInstruction_s e ins_true); intros; rewrite H5 in *; [|inversion H3].
case_eq (forgetListElif_instruction e ins_false); intros; rewrite H6 in *; [|inversion H3].
inversion H3; subst.
case_eq (forgetIdsValueList l1 (forgetFinisherElif_instruction ins_false e)); intros.
assert (D:=IHevalElif_instruction H1 e l1 e0 H2 H6 H7).
case_eq (forgetIdsValueList l0 (forgetFinisherInstruction_s ins_true e0)); intros.
assert (D' := forgetListConcat l0 l1 (forgetFinisherInstruction_s ins_true) (forgetFinisherElif_instruction ins_false e) e0 (simplForgetFinisherInstruction_s ins_true) H7).
assert (envpartialView e0 e).
eapply forgetListPartial with (l0 := l1) (f := forgetFinisherElif_instruction ins_false); eauto.
apply simplForgetFinisherElif_instruction.
rewrite H8 in D'.
rewrite <- D' in H4.
inversion H4; subst.
assert (envpartialView slim_e e0).
eapply forgetListPartial with (l0 := l0) (f := forgetFinisherInstruction_s ins_true); eauto.
apply simplForgetFinisherInstruction_s.
eapply envpartialView_trans; eauto.
assert (D' := forgetListConcat l0 l1 (forgetFinisherInstruction_s ins_true) (forgetFinisherElif_instruction ins_false e) e0 (simplForgetFinisherInstruction_s ins_true) H7).
assert (envpartialView e0 e).
eapply forgetListPartial with (l0 := l1) (f := forgetFinisherElif_instruction ins_false); eauto.
apply simplForgetFinisherElif_instruction.
rewrite H4 in D'.
rewrite H8 in D'.
inversion D'.
assert (D := forgetListConcatNone l0 l1 (forgetFinisherInstruction_s ins_true) (forgetFinisherElif_instruction ins_false e) (simplForgetFinisherInstruction_s ins_true) H7).
rewrite H4 in D.
inversion D.

(**-----------**)
induction 1; simpl; intros; subst.
inversion H0.
inversion H0.
(*Case true*)
case_eq (forgetListInstruction_s e ins_s); intros; rewrite H5 in *; [|inversion H3].
case_eq (forgetListCase_instruction e case_t); intros; rewrite H6 in *; [|inversion H3].
inversion H3; subst.
case_eq (forgetIdsValueList l1 (forgetFinisherCase_instruction case_t e)); intros.
assert (D := forgetListPartial l1 (forgetFinisherCase_instruction case_t) e e0 (simplForgetFinisherCase_instruction case_t) H7).
case_eq (forgetIdsValueList l0 (forgetFinisherInstruction_s ins_s e0)); intros.
assert (envpartialView e0 env).
eapply envpartialView_trans; eauto.
erewrite <- partialEnvForgetListIdemPotent_Instruction_s in H5; eauto.
assert (D':=forgetListInstruction_s_correct (env, evn) ins_s res H0 H1 e0 l0 e1 H9 H5 H8).
assert (D'' := forgetListConcat l0 l1 (forgetFinisherInstruction_s ins_s) (forgetFinisherCase_instruction case_t e) e0 (simplForgetFinisherInstruction_s ins_s) H7).
rewrite H4 in D''.
rewrite H8 in D''.
inversion D''; subst.
auto.
assert (D'' := forgetListConcat l0 l1 (forgetFinisherInstruction_s ins_s) (forgetFinisherCase_instruction case_t e) e0 (simplForgetFinisherInstruction_s ins_s) H7).
rewrite H4 in D''.
rewrite H8 in D''.
inversion D''.
assert (D := forgetListConcatNone l0 l1 (forgetFinisherInstruction_s ins_s) (forgetFinisherCase_instruction case_t e) (simplForgetFinisherInstruction_s ins_s) H7).
rewrite H4 in D.
inversion D.

(*if false*)
case_eq (forgetListInstruction_s e ins_s); intros; rewrite H5 in *; [|inversion H3].
case_eq (forgetListCase_instruction e case_t); intros; rewrite H6 in *; [|inversion H3].
inversion H3; subst.
case_eq (forgetIdsValueList l1 (forgetFinisherCase_instruction case_t e)); intros.
assert (D:=IHevalCase_instruction H1 e l1 e0 H2 H6 H7).
case_eq (forgetIdsValueList l0 (forgetFinisherInstruction_s ins_s e0)); intros.
assert (D' := forgetListConcat l0 l1 (forgetFinisherInstruction_s ins_s) (forgetFinisherCase_instruction case_t e) e0 (simplForgetFinisherInstruction_s ins_s) H7).
assert (envpartialView e0 e).
eapply forgetListPartial with (l0 := l1) (f := forgetFinisherCase_instruction case_t); eauto.
apply simplForgetFinisherCase_instruction.
rewrite H8 in D'.
rewrite <- D' in H4.
inversion H4; subst.
assert (envpartialView slim_e e0).
eapply forgetListPartial with (l0 := l0) (f := forgetFinisherInstruction_s ins_s); eauto.
apply simplForgetFinisherInstruction_s.
eapply envpartialView_trans; eauto.
assert (D' := forgetListConcat l0 l1 (forgetFinisherInstruction_s ins_s) (forgetFinisherCase_instruction case_t e) e0 (simplForgetFinisherInstruction_s ins_s) H7).
assert (envpartialView e0 e).
eapply forgetListPartial with (l0 := l1) (f := forgetFinisherCase_instruction case_t); eauto.
apply simplForgetFinisherCase_instruction.
rewrite H4 in D'.
rewrite H8 in D'.
inversion D'.
assert (D := forgetListConcatNone l0 l1 (forgetFinisherInstruction_s ins_s) (forgetFinisherCase_instruction case_t e) (simplForgetFinisherInstruction_s ins_s) H7).
rewrite H4 in D.
inversion D.

(**------------**)
induction 1; simpl; intros; subst.
eapply forgetListInstruction_correct; eauto.
case_eq (forgetListInstruction_s e t); intros; rewrite H5 in H3; [|inversion H3].
case_eq (forgetListInstruction e h); intros; rewrite H6 in H3; [|inversion H3].
inversion H3; subst.
case_eq (forgetIdsValueList l1 (forgetFinisherInstruction h e)); intros.
assert (envpartialView e0 e).
eapply forgetListPartial with (l0 := l1) (f := forgetFinisherInstruction h); eauto.
apply simplForgetFinisherInstruction.
case_eq (forgetIdsValueList l0 (forgetFinisherInstruction_s t e0)); intros.
assert (envpartialView e1 e0).
eapply forgetListPartial with (l0 := l0) (f := forgetFinisherInstruction_s t); eauto.
apply simplForgetFinisherInstruction_s.
assert (extractErrorType (OK env' evn') = None) by (simpl; auto).
assert (D := forgetListInstruction_correct (env, evn) h (OK env' evn') H H11 e l1 e0 H2 H6 H7).
simpl in D.
assert (forgetListInstruction_s e0 t = Some l0).
rewrite <- H5.
eapply partialEnvForgetListIdemPotent_Instruction_s; eauto.
assert (D' := IHevalInstruction_s H1 e0 l0 e1 D H12 H9).
assert (D'' := forgetListConcat 
          l0 l1 
          (forgetFinisherInstruction_s t) 
          (forgetFinisherInstruction h e) 
          e0
          (simplForgetFinisherInstruction_s t)
          H7
    ).
rewrite H4 in D''.
rewrite H9 in D''.
inversion D''; subst.
auto.
assert (D'' := forgetListConcat 
          l0 l1 
          (forgetFinisherInstruction_s t) 
          (forgetFinisherInstruction h e) 
          e0
          (simplForgetFinisherInstruction_s t)
          H7
    ).
rewrite H4 in D''.
rewrite H9 in D''.
inversion D''; subst.
assert (D := forgetListConcatNone l0 l1 (forgetFinisherInstruction_s t) (forgetFinisherInstruction h e) (simplForgetFinisherInstruction_s t) H7).
rewrite H4 in D.
inversion D.
inversion H0.
Qed.

(**Sliming**)
Lemma envpartialViewEvalSec_opel_expression_booleenne: forall e e' exp,
  envpartialView e e' ->
  evalSec_opel_expression_booleenne_f e exp = evalSec_opel_expression_booleenne_f e' exp \/ (evalSec_opel_expression_booleenne_f e exp = Some (Undefined)).
Proof.
unfold evalSec_opel_expression_booleenne_f.
induction exp; apply envpartialViewEvalExpression.
Qed.

Definition evalSec_opel_expression_d_affectation_f env e := match e with
| (SOEAffExpUMinus ids) => evalExpression_f env (Eunary NumericUnaryMinus (EVar ids))
| (SOEAffExpBinary op p0 p1) => evalExpression_f env (Ebinary (convSec_opel_binary_num_op op) (convSec_opel_parametre_to_exp p0) (convSec_opel_parametre_to_exp p1))
| (SOEAffDiv2 ids e) => evalExpression_f env 
      (Ebinary Ediv (EVar ids) (Ebinary EPow (ELit (Int_lit 2)) e))
| (SOEAffBool b) => evalSec_opel_expression_booleenne_f env b
| (SOEAffAssign ids) => evalExpression_f env (convSec_opel_parametre_to_exp ids)
| (SOEAffBuildTag ids None) => Some Undefined
| (SOEAffBuildTag ids (Some (ids', None))) => Some Undefined
| (SOEAffBuildTag ids (Some(ids', Some ids''))) => Some Undefined
| (SOEAffEncode e) => evalExpression_f env e
| (SOEAffRead ids None) => evalExpression_f env (EVar ids)
| (SOEAffRead ids (Some(ids', None))) => 
      (
        match evalExpression_f env (EVar ids), evalExpression_f env (EVar ids') with
        | Some v, Some (Int v') => idxs_select_from_value_f v [(Z.to_nat v')]
        | _, _ => None
        end
      )
| (SOEAffRead ids (Some(ids', Some ids''))) => 
      (
        match evalExpression_f env (EVar ids), evalExpression_f env (EVar ids'), evalExpression_f env (EVar ids'') with
        | Some v, Some (Int v'), Some (Int v'') => idxs_select_from_value_f v [(Z.to_nat v'); (Z.to_nat v'')]
        | _, _, _ => None
        end
      )
| SOEAffReadContext => Some Undefined
end.

Lemma evalSec_opel_expression_d_affectation_f_correct: forall e env trace v,
  evalSec_opel_expression_d_affectation (env, trace) e v ->
  ( evalSec_opel_expression_d_affectation_f env e = Some v \/ evalSec_opel_expression_d_affectation_f env e = Some Undefined ).
Proof.
unfold evalSec_opel_expression_d_affectation_f.
induction e.
intros; inversion H; subst; apply evalExpression_f_correct in H4; auto.
intros; inversion H; subst; apply evalExpression_f_correct in H6; auto.
intros; inversion H; subst; apply evalExpression_f_correct in H5; auto.
intros; inversion H; subst; apply evalSec_opel_expression_booleenne_f_correct in H4; auto.
intros; inversion H; subst; apply evalExpression_f_correct in H4; auto.
intros; induction o; auto; induction a; auto; induction b; auto.
intros; inversion H; subst; apply evalExpression_f_correct in H4; auto.
intros; inversion H; subst.
apply evalExpression_f_correct in H5; auto.
apply evalExpression_f_correct in H4, H6; rewrite H4, H6; auto.
apply idxs_select_from_value_f_correct in H7; auto.
apply evalExpression_f_correct in H4, H5, H7; rewrite H4, H5, H7; auto.
apply idxs_select_from_value_f_correct in H8; auto.
auto.
Qed.

Lemma evalSec_opel_expression_d_affectation_f_correct_iff: forall e env trace v,
  evalSec_opel_expression_d_affectation_f env e = Some v ->
  ( evalSec_opel_expression_d_affectation (env, trace) e v \/ v = Undefined ).
Proof.
unfold evalSec_opel_expression_d_affectation_f.
induction e; intros.
apply evalExpression_f_correct in H; left; econstructor; eauto.
apply evalExpression_f_correct in H; left; econstructor; eauto.
apply evalExpression_f_correct in H; left; econstructor; eauto.
apply evalSec_opel_expression_booleenne_f_correct in H; left; econstructor; eauto.
apply evalExpression_f_correct in H; left; econstructor; eauto.
induction o; inversion H; auto; clear H1; induction a; inversion H; auto; clear H1; induction b; inversion H; auto. 
apply evalExpression_f_correct in H; left; econstructor; eauto.
induction o.
induction a.
induction b.
case_eq (evalExpression_f env (EVar l)); intros; rewrite H0 in H; [|inversion H].
case_eq (evalExpression_f env (EVar a)); intros; rewrite H1 in H; [|inversion H].
case_eq (evalExpression_f env (EVar a0)); intros; rewrite H2 in H; [|inversion H].
induction v1, v2; inversion H; clear H4.
apply evalExpression_f_correct in H0, H1, H2.
apply idxs_select_from_value_f_correct in H.
left; econstructor; eauto.
induction v1; inversion H.
case_eq (evalExpression_f env (EVar l)); intros; rewrite H0 in H; [|inversion H].
case_eq (evalExpression_f env (EVar a)); intros; rewrite H1 in H; [|inversion H].
induction v1; inversion H; clear H3.
apply evalExpression_f_correct in H0, H1.
apply idxs_select_from_value_f_correct in H.
left; econstructor; eauto.
apply evalExpression_f_correct in H.
left; econstructor; eauto.
inversion H; subst; auto.
Qed.



Lemma envpartialViewEvalSec_opel_expression_d_affectation: forall e e' exp,
  envpartialView e e' ->
  evalSec_opel_expression_d_affectation_f e exp = None \/
  evalSec_opel_expression_d_affectation_f e exp = evalSec_opel_expression_d_affectation_f e' exp \/ 
  evalSec_opel_expression_d_affectation_f e exp = Some (Undefined)
  .
Proof.
unfold evalSec_opel_expression_d_affectation_f.
induction exp; try (right; apply envpartialViewEvalExpression); auto.
right; apply envpartialViewEvalSec_opel_expression_booleenne; auto.
intros.
induction o; auto; try (right; apply envpartialViewEvalExpression); auto.
induction a.
induction b.
assert (D := envpartialViewEvalExpression e e' (EVar l) H).
destruct D; rewrite H0.
assert (D' := envpartialViewEvalExpression e e' (EVar a) H).
destruct D'; rewrite H1.
assert (D'' := envpartialViewEvalExpression e e' (EVar a0) H).
destruct D''; rewrite H2; auto.
induction (evalExpression_f e' (EVar a)); auto.
induction (evalExpression_f e' (EVar l)); auto.
induction a1; auto.
induction (evalExpression_f e' (EVar l)); auto.
assert (D' := envpartialViewEvalExpression e e' (EVar a) H).
destruct D'; rewrite H1.
assert (D'' := envpartialViewEvalExpression e e' (EVar a0) H).
destruct D''; rewrite H2; auto.
induction (evalExpression_f e' (EVar a)); auto.
induction a1; auto.
induction (evalExpression_f e' (EVar a0)); auto.
induction a1; auto.
induction (evalExpression_f e' (EVar a)); auto.
induction a1; auto.
auto.
assert (D := envpartialViewEvalExpression e e' (EVar l) H).
destruct D; rewrite H0.
assert (D' := envpartialViewEvalExpression e e' (EVar a) H).
destruct D'; rewrite H1; auto.
induction (evalExpression_f e' (EVar l)); auto.
induction (evalExpression_f e (EVar a)); auto.
induction a0; auto.
Qed.

Lemma envpartialViewEvalSec_opel_w: forall e e' exp,
  envpartialView e e' ->
  evalSec_opel_w_f e exp = evalSec_opel_w_f e' exp \/ 
  evalSec_opel_w_f e exp = Some (Undefined)
  .
Proof.
unfold evalSec_opel_w_f.
induction exp.
apply envpartialViewEvalSec_opel_expression_booleenne.
intros.
apply envpartialViewEvalExpression with (exp := EVar l)in H.
destruct H; rewrite H; auto.
Qed.

Definition evalSec_opel_expression_if_f env b := match b with
| SOExpression_booleenne b => evalSec_opel_expression_booleenne_f env b
| SOt ids => 
    (
      match evalExpression_f env (EVar ids) with
      | Some v => Some (Math.to_bool v)
      | _ => None
      end
     )
| _ => Some Undefined
end.

Lemma envpartialViewEvalSec_expression_if: forall e e' evn v exp,
  envpartialView e e' ->
  (evalSec_opel_expression_if (e', evn) exp v -> evalSec_opel_expression_if_f e exp = Some v) \/ 
  evalSec_opel_expression_if_f e exp = Some (Undefined)
  .
Proof.
unfold evalSec_opel_expression_if_f.
induction exp; auto.
intros.
assert (D := envpartialViewEvalSec_opel_expression_booleenne e e' s H).
destruct D; rewrite H0; auto.
left.
intros.
inversion H1; subst.
apply evalSec_opel_expression_booleenne_f_correct; auto.
intros.
assert (D := envpartialViewEvalExpression e e' (EVar l) H).
destruct D; rewrite H0; auto.
left.
intros.
inversion H1; subst.
rewrite evalExpression_f_correct in H6.
rewrite H6.
auto.
Qed.

(* Search setIdsValue.
 *)

Lemma mapPartialViewBeqVsetLower : forall e e' t v,
  mapPartialView e e' ->
  StringMapModule.get t e' = Some (VariableValue v) ->
  mapPartialView (StringMapModule.set t (VariableValue v) e) e'.
Proof.
induction e'; simpl; intros; try einversion.
induction a.
unfold mapPartialView in *.
simpl in *.
intros.
assert (D := H id).
rewrite StringMapModule.gsspec.
case_eq (String_Equality.eq id a); intros; rewrite H1 in *; eauto.
rewrite String_Equality.eq_spec_true in H1; subst.
rewrite StringMapModule.Xeq_sym in H0.
case_eq (String_Equality.eq a t); intros; rewrite H1 in *.
inversion H0; subst; eauto.
eauto.
rewrite StringMapModule.Xeq_sym in H0.
case_eq (String_Equality.eq a t); intros; rewrite H2 in *; eauto.
rewrite String_Equality.eq_spec_true in H2; subst.
inversion H0; subst; eauto.
rewrite H1 in *; eauto.
case_eq (String_Equality.eq id t); intros; eauto.
rewrite String_Equality.eq_spec_true in H3; subst; eauto.
Qed.

Lemma mapPartialView_setNothingUndef': forall lower bigger id v,
  mapPartialView lower bigger ->
  StringMapModule.get id bigger = Some (VariableValue v) ->
  mapPartialView (StringMapModule.set id (VariableValue Undefined) lower) bigger.
Proof.
intros.
unfold mapPartialView in *.
intros.
rewrite StringMapModule.gsspec.
case_eq (String_Equality.eq id0 id); auto.
intros.
apply String_Equality.eq_spec_true in H1; subst; auto.
right; eauto.
Qed.

Lemma partialViewSlimsetWhenUpIsConst: forall var e e' v, 
  envpartialView e e' ->
  getIdsValue var e' = Some (Value (ConstantValue v)) ->
  slimSetIdsValue var v e = None \/ slimSetIdsValue var v e = forgetIdsValue var e.
Proof.
intros.
assert (D := envpartialViewGetIdsSome var e e' H).
rewrite H0 in D.
unfold getIdsValue in D.
destruct D.
clear -H1.
case_eq var; intros; subst; try einversion.
case_eq l; intros; subst; try einversion.
unfold slimSetIdsValue.
unfold getIdValue in H1.
unfold setIdValue.
case_eq (StringMapModule.get t (localVars e)); intros; rewrite H in *.
inversion H1; subst; auto.
case_eq (StringMapModule.get t (globalVars e)); intros; rewrite H0 in *; auto.
inversion H1; subst; auto.
unfold slimSetIdsValue.
case_eq (StringMapModule.get t (localVars e)); intros; rewrite H in *.
induction l0; eauto.
case_eq (StringMapModule.get t (globalVars e)); intros; rewrite H0 in *; auto.
induction l0; eauto.
case_eq (StringMapModule.get t (funDefs e)); intros; rewrite H2 in *; auto.
induction l0; eauto.
case_eq (StringMapModule.get t (localfunDecls e)); intros; rewrite H3 in *; auto.
induction l0; eauto.
case_eq (StringMapModule.get t (externals e)); intros; rewrite H4 in *; auto.
case_eq (StringMapModule.get t0 t1); intros; rewrite H5 in *; auto.
induction e0; eauto; try einversion; try induction c; try einversion; try induction (ids_select_from_value_f v0 l0); try (solve [inversion H1]);
induction l0; eauto.
induction l0; eauto.
induction l0; eauto.
destruct H1.
destruct H1.
destruct H2.
destruct H2; inversion H2.
inversion H2.
destruct H1.
clear -H1.
case_eq var; intros; subst; try einversion.
case_eq l; intros; subst; try einversion.
unfold slimSetIdsValue.
unfold getIdValue in H1.
unfold setIdValue.
case_eq (StringMapModule.get t (localVars e)); intros; rewrite H in *.
inversion H1; subst; auto.
case_eq (StringMapModule.get t (globalVars e)); intros; rewrite H0 in *; auto.
inversion H1; subst; auto.
unfold slimSetIdsValue.
case_eq (StringMapModule.get t (localVars e)); intros; rewrite H in *.
induction l0; eauto.
case_eq (StringMapModule.get t (globalVars e)); intros; rewrite H0 in *; auto.
induction l0; eauto.
case_eq (StringMapModule.get t (funDefs e)); intros; rewrite H2 in *; auto.
induction l0; eauto.
case_eq (StringMapModule.get t (localfunDecls e)); intros; rewrite H3 in *; auto.
induction l0; eauto.
case_eq (StringMapModule.get t (externals e)); intros; rewrite H4 in *; auto.
case_eq (StringMapModule.get t0 t1); intros; rewrite H5 in *; auto.
induction e0; eauto; try einversion; try induction c; try einversion; try induction (ids_select_from_value_f v0 l0); try (solve [inversion H1]);
induction l0; eauto.
induction l0; eauto.
induction l0; eauto.
Qed.


Lemma partialViewSlimsetActualValue : forall ids e e' v e0,
  envpartialView e e' ->
  getIdsValue ids e' = Some (Value (VariableValue v)) ->
  slimSetIdsValue ids v e = Some e0 ->
  envpartialView e0 e'.
Proof.
unfold envpartialView.
intro.
case_eq ids; simpl; intros; try einversion.
destruct H0, H3, H4, H5.
case_eq l; simpl; intros; rewrite H7 in *; try einversion.
unfold getIdValue in H1.
unfold setIdValue in H2.
case_eq (StringMapModule.get t (localVars e')); intros; rewrite H8 in *.
inversion H1; subst.
assert (D := mapPartialView_getSome_b2l (localVars e) (localVars e') t (VariableValue v) H0 H8).
destruct D.
destruct H, H; subst.
rewrite H in *.
inversion H2; subst.
simpl.
repeat (split; eauto).
eapply mapPartialViewBeqVsetLower; eauto.
destruct H.
destruct H, H.
inversion H7. 
subst x.
rewrite H in H2.
inversion H2; subst.
simpl.
repeat (split; eauto).
eapply mapPartialViewBeqVsetLower; eauto.
destruct H, H.
inversion H7.
assert (H9 := mapPartialView_getNone_b2l (localVars e) (localVars e') t H0 H8).
rewrite H9 in *.
case_eq (StringMapModule.get t (globalVars e')); intros; rewrite H10 in *.
inversion H1; subst.
assert (D := mapPartialView_getSome_b2l (globalVars e) (globalVars e') t (VariableValue v) H3 H10).
destruct D.
destruct H, H; subst.
rewrite H in *.
inversion H2; subst.
simpl.
repeat (split; eauto).
eapply mapPartialViewBeqVsetLower; eauto.
destruct H.
destruct H, H.
inversion H7. 
subst x.
rewrite H in H2.
inversion H2; subst.
simpl.
repeat (split; eauto).
eapply mapPartialViewBeqVsetLower; eauto.
destruct H, H.
inversion H7.
assert (H11 := mapPartialView_getNone_b2l (globalVars e) (globalVars e') t H3 H10).
rewrite H11 in *.
inversion H2.

case_eq l0; intros; rewrite H8 in *.
case_eq (StringMapModule.get t (localVars e')); intros; rewrite H9 in *.
induction c; try solve [induction (match id_select_from_value v0 t0 with
       | Some v' => ids_select_from_value_f v' []
       | None => None
       end); inversion H1].
induction (match id_select_from_value v0 t0 with
       | Some v' => ids_select_from_value_f v' []
       | None => None
       end); inversion H1; subst.
assert (D := mapPartialView_getSome_b2l (localVars e) (localVars e') t (VariableValue v0) H0 H9).
destruct D.
destruct H, H; subst.
rewrite H in *.
inversion H2; subst.
simpl.
repeat (split; eauto).
eapply mapPartialView_setNothingUndef'; eauto.
destruct H, H, H.
inversion H7; subst.
rewrite H in H2.
inversion H2; subst.
simpl.
repeat (split; eauto).
eapply mapPartialView_setNothingUndef'; eauto.
inversion H7.
assert (H10 := mapPartialView_getNone_b2l (localVars e) (localVars e') t H0 H9).
rewrite H10 in *.
case_eq (StringMapModule.get t (globalVars e')); intros; rewrite H11 in *.
induction c; try solve [induction (match id_select_from_value v0 t0 with
       | Some v' => ids_select_from_value_f v' []
       | None => None
       end); inversion H1].
induction (match id_select_from_value v0 t0 with
       | Some v' => ids_select_from_value_f v' []
       | None => None
       end); inversion H1; subst.
assert (D := mapPartialView_getSome_b2l (globalVars e) (globalVars e') t (VariableValue v0) H3 H11).
destruct D.
destruct H, H; subst.
rewrite H in *.
inversion H2; subst.
simpl.
repeat (split; eauto).
eapply mapPartialView_setNothingUndef'; eauto.
destruct H.
destruct H, H.
inversion H7. 
subst x.
rewrite H in H2.
inversion H2; subst.
simpl.
repeat (split; eauto).
eapply mapPartialView_setNothingUndef'; eauto.
destruct H, H.
inversion H7.
assert (H12 := mapPartialView_getNone_b2l (globalVars e) (globalVars e') t H3 H11).
rewrite H12 in *.
case_eq (StringMapModule.get t (funDefs e')); intros; rewrite H13 in *; try einversion.
rewrite H5, H13 in *.
case_eq (StringMapModule.get t (localfunDecls e')); intros; rewrite H14 in *; try einversion.
rewrite H4, H14 in *.
assert (H6' := H6 t).
case_eq (StringMapModule.get t (externals e)); intros; rewrite H15 in *.
case_eq (StringMapModule.get t0 t1); intros; rewrite H16 in *.
induction e1; try einversion.
induction c; eauto.
inversion H2; subst; eauto.
inversion H2; subst; simpl; repeat (split; eauto); eauto.
intros.
rewrite StringMapModule.gsspec in *.
case_eq (String_Equality.eq id t); intros; try rewrite H in *; eauto.
apply String_Equality.eq_spec_true in H; subst; auto.
case_eq (StringMapModule.get t (externals e')); intros; rewrite H in *; try einversion; eauto.
rewrite StringMapModule.gsspec in *.
case_eq (String_Equality.eq i t0); intros; try rewrite H7 in *; eauto.
apply String_Equality.eq_spec_true in H7; subst; auto.
case_eq (StringMapModule.get t0 t2); intros; rewrite H7 in *; try einversion.
induction e0; try einversion.
induction c; simpl in H1; try einversion.
inversion H1; subst; eauto.
eapply H6; eauto.
inversion H2; subst.
simpl.
repeat (split; eauto).
intros.
rewrite StringMapModule.gsspec in *.
case_eq (String_Equality.eq id t); intros; eauto.
apply String_Equality.eq_spec_true in H; subst; auto.
case_eq (StringMapModule.get t (externals e')); intros; rewrite H in *.
rewrite StringMapModule.gsspec in *.
assert (StringMapModule.get t0 t2 = None).
assert (D := H6' t0).
destruct D.
rewrite <- H7; apply eq_sym; eauto.
rewrite H16 in H7; destruct H7, H7; try einversion.
rewrite H7 in *.
inversion H1.
inversion H1.
eapply H6; eauto.
inversion H2; subst; simpl.
case_eq (StringMapModule.get t (externals e')); intros; rewrite H in H6'; inversion H6'.
rewrite H in H1; try einversion.

case_eq (StringMapModule.get t (localVars e')); intros; rewrite H9 in *.
induction c; try solve [induction (match id_select_from_value v0 t0 with
       | Some v' => ids_select_from_value_f v' (t1 :: l1)
       | None => None
       end); inversion H1].
induction (match id_select_from_value v0 t0 with
       | Some v' => ids_select_from_value_f v' (t1 :: l1)
       | None => None
       end); inversion H1; subst.
assert (D := mapPartialView_getSome_b2l (localVars e) (localVars e') t (VariableValue v0) H0 H9).
destruct D.
destruct H, H; subst.
rewrite H in *.
inversion H2; subst.
simpl.
repeat (split; eauto).
eapply mapPartialView_setNothingUndef'; eauto.
destruct H, H, H.
inversion H7; subst.
rewrite H in H2.
inversion H2; subst.
simpl.
repeat (split; eauto).
eapply mapPartialView_setNothingUndef'; eauto.
inversion H7.
assert (H10 := mapPartialView_getNone_b2l (localVars e) (localVars e') t H0 H9).
rewrite H10 in *.
case_eq (StringMapModule.get t (globalVars e')); intros; rewrite H11 in *.
induction c; try solve [induction (match id_select_from_value v0 t0 with
       | Some v' => ids_select_from_value_f v' (t1 :: l1)
       | None => None
       end); inversion H1].
induction (match id_select_from_value v0 t0 with
       | Some v' => ids_select_from_value_f v' (t1 :: l1)
       | None => None
       end); inversion H1; subst.
assert (D := mapPartialView_getSome_b2l (globalVars e) (globalVars e') t (VariableValue v0) H3 H11).
destruct D.
destruct H, H; subst.
rewrite H in *.
inversion H2; subst.
simpl.
repeat (split; eauto).
eapply mapPartialView_setNothingUndef'; eauto.
destruct H.
destruct H, H.
inversion H7. 
subst x.
rewrite H in H2.
inversion H2; subst.
simpl.
repeat (split; eauto).
eapply mapPartialView_setNothingUndef'; eauto.
destruct H, H.
inversion H7.
assert (H12 := mapPartialView_getNone_b2l (globalVars e) (globalVars e') t H3 H11).
rewrite H12 in *.
case_eq (StringMapModule.get t (funDefs e')); intros; rewrite H13 in *; try einversion.
rewrite H5, H13 in *.
case_eq (StringMapModule.get t (localfunDecls e')); intros; rewrite H14 in *; try einversion.
rewrite H4, H14 in *.
assert (H6' := H6 t).
case_eq (StringMapModule.get t (externals e)); intros; rewrite H15 in *.
case_eq (StringMapModule.get t0 t2); intros; rewrite H16 in *.
induction e1; try einversion.
induction c; eauto.
inversion H2; subst; eauto.
inversion H2; subst; simpl; repeat (split; eauto); eauto.
intros.
rewrite StringMapModule.gsspec in *.
case_eq (String_Equality.eq id t); intros; try rewrite H in *; eauto.
apply String_Equality.eq_spec_true in H; subst; auto.
case_eq (StringMapModule.get t (externals e')); intros; rewrite H in *; try einversion; eauto.
rewrite StringMapModule.gsspec in *.
case_eq (String_Equality.eq i t0); intros; try rewrite H7 in *; eauto.
apply String_Equality.eq_spec_true in H7; subst; auto.
case_eq (StringMapModule.get t0 t3); intros; rewrite H7 in *; try einversion.
right; left.
split; eauto; right; eauto.
induction e0; try einversion.
induction c; try einversion; induction (ids_select_from_value_f v1 (t1 :: l1)); inversion H1; subst; eauto.
eapply H6; eauto.
inversion H2; subst.
simpl.
repeat (split; eauto).
intros.
rewrite StringMapModule.gsspec in *.
case_eq (String_Equality.eq id t); intros; eauto.
apply String_Equality.eq_spec_true in H; subst; auto.
case_eq (StringMapModule.get t (externals e')); intros; rewrite H in *.
rewrite StringMapModule.gsspec in *.
assert (StringMapModule.get t0 t3 = None).
assert (D := H6' t0).
destruct D.
rewrite <- H7; apply eq_sym; eauto.
rewrite H16 in H7; destruct H7, H7; try einversion.
rewrite H7 in *.
inversion H1.
inversion H1.
eapply H6; eauto.
inversion H2; subst; simpl.
case_eq (StringMapModule.get t (externals e')); intros; rewrite H in H6'; inversion H6'.
rewrite H in H1; try einversion.
Qed.


(* Here
 *)
(* Here
 *)
Fixpoint slimPartialViewevalInstruction env f := match f with
| INull => Some env
| IPragma _ => Some env
| IAssign var value => 
    (
      match evalSec_opel_expression_d_affectation_f env value with
      | Some (Undefined) => forgetIdsValue var env
      | Some v => slimSetIdsValue var v env
      | _ => forgetIdsValue var env
      end
    )
 | (IIf cond ins_if ins_else) =>
    (
      match evalSec_opel_expression_if_f env cond with
      | Some (Bool true) => slimPartialViewevalInstruction_s env ins_if
      | Some (Bool false) => slimPartialViewevalElif_instruction env ins_else
      | _ => 
        (
          match forgetListInstruction env f with
          | Some l => forgetIdsValueList l (forgetFinisherInstruction f env)
          | None => None
          end
        )
      end
    )
 | (ICase var case) => slimPartialViewevalCase_instruction env var case
 | ILoop cond ins => 
    (
      match evalSec_opel_w_f env cond with
      | Some (Bool false) => Some env
      | _ => 
        (
          match forgetListInstruction env f with
          | Some l => forgetIdsValueList l (forgetFinisherInstruction f env)
          | None => None
          end
        )
      end
    )
 | SOInstructionInit var (Some e) => 
    (
      match evalExpression_f env e with
      | Some Undefined | None => 
        (
          match forgetListInstruction env f with
          | Some l => forgetIdsValueList l (forgetFinisherInstruction f env)
          | None => None
          end
        )
      | Some v =>
          (
            match evalExpression_f env (EVar var) with
            | Some Undefined | None => 
              (
                match forgetListInstruction env f with
                | Some l => forgetIdsValueList l (forgetFinisherInstruction f env)
                | None => None
                end
              )
            | Some vvar => slimSetIdsValue var (initValue vvar v) env
            end
          )
      end
    )
 | SOInstructionWrite t i x None =>
    (
      match evalExpression_f env (EVar t) with
      | Some Undefined | None => 
        (
          match forgetListInstruction env f with
          | Some l => forgetIdsValueList l (forgetFinisherInstruction f env)
          | None => None
          end
        )
      | Some vt => 
        (
          match evalExpression_f env (EVar i) with
          | Some (Int vi) => 
            (
              match evalExpression_f env (EVar x) with
              | Some Undefined | None => 
                (
                  match forgetListInstruction env f with
                  | Some l => forgetIdsValueList l (forgetFinisherInstruction f env)
                  | None => None
                  end
                )
              | Some vx => 
                (
                  match idxs_set_from_value_f vt [(Z.to_nat vi)] vx with
                  | Some vt' => slimSetIdsValue t vt' env
                  | None => None
                  end
                )
              end
            )
          | _ => 
            (
              match forgetListInstruction env f with
              | Some l => forgetIdsValueList l (forgetFinisherInstruction f env)
              | None => None
              end
            )
          end
        )
      end
    )
 | SOInstructionWrite t i j (Some x) =>
    (
      match evalExpression_f env (EVar t) with
      | Some Undefined | None => 
        (
          match forgetListInstruction env f with
          | Some l => forgetIdsValueList l (forgetFinisherInstruction f env)
          | None => None
          end
        )
      | Some vt => 
        (
          match evalExpression_f env (EVar i), evalExpression_f env (EVar j) with
          | Some (Int vi), Some (Int vj) => 
            (
              match evalExpression_f env (EVar x) with
              | Some Undefined | None => 
                (
                  match forgetListInstruction env f with
                  | Some l => forgetIdsValueList l (forgetFinisherInstruction f env)
                  | None => None
                  end
                )
              | Some vx => 
                (
                  match idxs_set_from_value_f vt [(Z.to_nat vi); (Z.to_nat vj)] vx with
                  | Some vt' => slimSetIdsValue t vt' env
                  | None => None
                  end
                )
              end
            )
          | _, _ => 
            (
              match forgetListInstruction env f with
              | Some l => forgetIdsValueList l (forgetFinisherInstruction f env)
              | None => None
              end
            )
          end
        )
      end
    )
 | SOInstructionWriteOutvar z x no_fic no_bx no_t =>
    (
      match evalExpression_f env (EVar x) with
      | Some Undefined | None => 
        (
          match forgetListInstruction env f with
          | Some l => forgetIdsValueList l (forgetFinisherInstruction f env)
          | None => None
          end
        )
      | Some v => slimSetIdsValue z v env
      end
    )
 | _ =>
    (
      match forgetListInstruction env f with
      | Some l => forgetIdsValueList l (forgetFinisherInstruction f env)
      | None => None
      end
    )
end

with slimPartialViewevalElif_instruction env f := match f with
| IElse i => slimPartialViewevalInstruction_s env i
| IElif cond ins_true ins_false => 
    (
      match evalSec_opel_expression_if_f env cond with
      | Some (Bool true) => slimPartialViewevalInstruction_s env ins_true
      | Some (Bool false) => slimPartialViewevalElif_instruction env ins_false
      | _ => 
        (
          match forgetListElif_instruction env f with
          | Some l => forgetIdsValueList l (forgetFinisherElif_instruction f env)
          | None => None
          end
        )
      end
    )
end
    
with slimPartialViewevalCase_instruction env e f := match f with
| ICaseFinishDefault ins_s => slimPartialViewevalInstruction_s env ins_s
| ICaseCons (exp, ins_s) case_t => 
    (
      match evalExpression_f env (Ebinary Equal (EVar e) exp) with
      | Some (Bool true) => slimPartialViewevalInstruction_s env ins_s
      | Some (Bool false) => slimPartialViewevalCase_instruction env e case_t
      | _ => 
        (
          match forgetListCase_instruction env f with
          | Some l => forgetIdsValueList l (forgetFinisherCase_instruction f env)
          | None => None
          end
        )
      end
    )
end
    
with slimPartialViewevalInstruction_s env f := match f with
| ISeqEnd i => slimPartialViewevalInstruction env i
| ISeqCons h t => 
    (
      match slimPartialViewevalInstruction env h with
      | Some e' => slimPartialViewevalInstruction_s e' t
      | None => None
      end
    )
end.

Theorem slimPartialViewevalInstruction_correct : forall in_data f res,
  evalInstruction in_data f res ->
  extractErrorType res = None -> forall e slim_e,
  envpartialView e (fst in_data) ->
  slimPartialViewevalInstruction e f = Some slim_e ->
  envpartialView slim_e (extractEnv res)
with slimPartialViewElif_instruction_correct : forall in_data f res,
  evalElif_instruction in_data f res ->
  extractErrorType res = None -> forall e slim_e,
  envpartialView e (fst in_data) ->
  slimPartialViewevalElif_instruction e f = Some slim_e ->
  envpartialView slim_e (extractEnv res)
with slimPartialViewCase_instruction_correct : forall in_data exp f res,
  evalCase_instruction in_data exp f res ->
  extractErrorType res = None -> forall e slim_e,
  envpartialView e (fst in_data) ->
  slimPartialViewevalCase_instruction e exp f = Some slim_e ->
  envpartialView slim_e (extractEnv res)
with slimPartialViewInstruction_s_correct : forall in_data f res,
  evalInstruction_s in_data f res ->
  extractErrorType res = None -> forall e slim_e,
  envpartialView e (fst in_data) ->
  slimPartialViewevalInstruction_s e f = Some slim_e ->
  envpartialView slim_e (extractEnv res).
Proof.
induction 1; intros; subst.
simpl in *; inversion H1; subst; auto.
simpl in *; inversion H1; subst; auto.
simpl in *; inversion H1; subst; auto.
simpl in *; inversion H1; subst; auto.
simpl in *.
assert (D := envpartialViewEvalSec_opel_expression_d_affectation e env aff H2).
apply evalSec_opel_expression_d_affectation_f_correct in H.
assert (forgetIdsValue var e = Some slim_e \/ slimSetIdsValue var v e = Some slim_e).
destruct D.
rewrite H4 in *; auto.
destruct H4; rewrite H4 in H3; auto.
destruct H; rewrite H in H3; auto.
induction v; auto.
destruct H4.
eapply envpartialViewForgetVsSet; eauto.
eapply slimSetIdsValue_correct; eauto.
assert (D := envpartialViewEvalSec_expression_if e env evn (Bool true) cond H2).
destruct D.
assert (D := H4 H).
simpl in H3.
rewrite D in H3.
eapply slimPartialViewInstruction_s_correct; eauto.
unfold slimPartialViewevalInstruction in H3.
rewrite H4 in H3.
case_eq (forgetListInstruction e (IIf cond ins_if ins_else)); intros; rewrite H5 in H3; [|inversion H3].
eapply forgetListInstruction_correct; eauto.
eapply evalInstructionIIfTrue; eauto.
assert (D := envpartialViewEvalSec_expression_if e env evn (Bool false) cond H2).
destruct D.
assert (D := H4 H).
simpl in H3.
rewrite D in H3.
eapply slimPartialViewElif_instruction_correct; eauto.
unfold slimPartialViewevalInstruction in H3.
rewrite H4 in H3.
case_eq (forgetListInstruction e (IIf cond ins_if ins_else)); intros; rewrite H5 in H3; [|inversion H3].
eapply forgetListInstruction_correct; eauto.
eapply evalInstructionIIfFalse; eauto.
simpl in H2.
eapply slimPartialViewCase_instruction_correct; eauto.
assert (D := envpartialViewEvalSec_opel_w e env cond H3).
assert (evalInstruction (env, evn) (ILoop cond ins) res).
eapply evalInstructionILoopTrueOk; eauto.
apply evalSec_opel_w_f_correct in H.
rewrite H in D.
unfold slimPartialViewevalInstruction in H4.
destruct D.
rewrite H6 in H4.
case_eq (forgetListInstruction e (ILoop cond ins)); intros; rewrite H7 in H4; [|inversion H4].
eapply forgetListInstruction_correct; eauto.
rewrite H6 in H4.
case_eq (forgetListInstruction e (ILoop cond ins)); intros; rewrite H7 in H4; [|inversion H4].
eapply forgetListInstruction_correct; eauto.
inversion H1.
assert (D := envpartialViewEvalSec_opel_w e env cond H1).
assert (evalInstruction (env, evn) (ILoop cond ins) (OK env evn)).
eapply evalInstructionILoopFalse; eauto.
apply evalSec_opel_w_f_correct in H.
rewrite H in D.
destruct D.
unfold slimPartialViewevalInstruction in H2.
rewrite H4 in H2.
inversion H2; subst; auto.
unfold slimPartialViewevalInstruction in H2.
rewrite H4 in H2.
case_eq (forgetListInstruction e (ILoop cond ins)); intros; rewrite H5 in H2; [|inversion H2].
eapply forgetListInstruction_correct; eauto.
unfold slimPartialViewevalInstruction in H6.
case_eq (forgetListInstruction e (ICallDefinedFunction f_name None)); intros; rewrite H7 in H6; [|inversion H6].
eapply forgetListInstruction_correct; eauto.
econstructor; eauto.
inversion H3.
unfold slimPartialViewevalInstruction in H6.
case_eq (forgetListInstruction e (ICallDefinedFunction f_name (Some f_params))); intros; rewrite H7 in H6; [|inversion H6].
eapply forgetListInstruction_correct; eauto.
econstructor; eauto.
inversion H3.
unfold slimPartialViewevalInstruction in H4.
case_eq (forgetListInstruction e (ICallDefinedFunction f_name (Some f_params))); intros; rewrite H5 in H4; [|inversion H4].
eapply forgetListInstruction_correct; eauto.
eapply evalCallExtFunctionWithParamsOK; eauto.
inversion H1.
unfold slimPartialViewevalInstruction in H4.
case_eq (forgetListInstruction e (ICallDefinedFunction f_name None)); intros; rewrite H5 in H4; [|inversion H4].
eapply forgetListInstruction_correct; eauto.
eapply evalCallExtFunctionNoParamsOK; eauto.
inversion H1.
unfold slimPartialViewevalInstruction in H4.
case_eq (forgetListInstruction e (ICallDefinedFunction f_name (Some f_params))); intros; rewrite H5 in H4; [|inversion H4].
eapply forgetListInstruction_correct; eauto.
eapply evalCallUnknownFunctionWithParamsOK; eauto.
inversion H1.
unfold slimPartialViewevalInstruction in H4.
case_eq (forgetListInstruction e (ICallDefinedFunction f_name None)); intros; rewrite H5 in H4; [|inversion H4].
eapply forgetListInstruction_correct; eauto.
eapply evalCallUnknownFunctionNoParamsOK; eauto.
inversion H1.

simpl in H1; inversion H1; subst; auto.
simpl in H1; inversion H1; subst; auto.
simpl in H1; inversion H1; subst; auto.
simpl in H1; inversion H1; subst; auto.
simpl in H1; inversion H1; subst; auto.
simpl in H2; eapply envpartialViewForgetVsSet; eauto.
simpl in H2; eapply envpartialViewForgetVsSet; eauto.
inversion H.
simpl in H1; inversion H1; subst; auto.
simpl in H4.
assert (D := envpartialViewEvalExpression e0 b_env e H3).
rewrite evalExpression_f_correct in H; rewrite H in D.
assert (D0 := envpartialViewEvalExpression e0 b_env (EVar var) H3).
rewrite evalExpression_f_correct in H0; rewrite H0 in D0.
assert (slimSetIdsValue var (initValue vvar v) e0 = Some slim_e \/ forgetIdsValue var e0 = Some slim_e).
destruct D, D0; simpl in H6; try rewrite H5, H6 in *; eauto.
induction v; try induction vvar; eauto.
right; induction v; eauto.
rewrite H5 in H4; eauto.
rewrite H5 in H4; eauto.
simpl.
destruct H5.
eapply slimSetIdsValue_correct; eauto.
eapply envpartialViewForgetVsSet; eauto.
simpl in H1; inversion H1; subst; auto.
simpl in H1; inversion H1; subst; auto.
simpl in H1; inversion H1; subst; auto.
simpl in H2; eapply envpartialViewForgetVsSet; eauto.
simpl in H4.
case_eq (forgetIdsValue t e0); intros; rewrite H5 in H4; [|inversion H4].
assert (envpartialView e1 a_env') by (eapply envpartialViewForgetVsSet; eauto).
eapply envpartialViewForgetVsSet; eauto.
simpl in H3.
case_eq (forgetIdsValue t e0); intros; rewrite H4 in H3; [|inversion H3].
assert (envpartialView e1 b_env) by (eapply envpartialViewForgetVsNothing; eauto).
eapply envpartialViewForgetVsSet; eauto.
simpl in H4.
case_eq (forgetIdsValue t e0); intros; rewrite H5 in H4; [|inversion H4].
assert (envpartialView e1 a_env') by (eapply envpartialViewForgetVsSet; eauto).
eapply envpartialViewForgetVsSet; eauto.
simpl in H3.
case_eq (forgetIdsValue t e0); intros; rewrite H4 in H3; [|inversion H3].
assert (envpartialView e1 b_env) by (eapply envpartialViewForgetVsNothing; eauto).
eapply envpartialViewForgetVsSet; eauto.
simpl in H6.
assert (D := envpartialViewEvalExpression e b_env (EVar t) H5).
rewrite evalExpression_f_correct in H; rewrite H in D.
assert (D0 := envpartialViewEvalExpression e b_env (EVar i) H5).
rewrite evalExpression_f_correct in H0; rewrite H0 in D0.
assert (D1 := envpartialViewEvalExpression e b_env (EVar x) H5).
rewrite evalExpression_f_correct in H1; rewrite H1 in D1.
rewrite idxs_set_from_value_f_correct in H2.
simpl in H2.
assert (slimSetIdsValue t vt' e = Some slim_e \/ forgetIdsValue t e = Some slim_e).
destruct D, D0, D1; simpl in H7; simpl in H8; simpl in H9; try rewrite H7 in H6; try rewrite H8 in H6; 
try rewrite H9 in H6; eauto.
rewrite H2 in H6.
induction vt; eauto; induction vx; eauto.
induction vt; eauto.
induction vt; eauto.
induction vt; eauto.
simpl.
destruct H7.
eapply slimSetIdsValue_correct; eauto.
eapply envpartialViewForgetVsSet; eauto.
simpl in H7.
assert (D := envpartialViewEvalExpression e b_env (EVar t) H6).
rewrite evalExpression_f_correct in H; rewrite H in D.
assert (D0 := envpartialViewEvalExpression e b_env (EVar i) H6).
rewrite evalExpression_f_correct in H0; rewrite H0 in D0.
assert (D1 := envpartialViewEvalExpression e b_env (EVar j) H6).
rewrite evalExpression_f_correct in H1; rewrite H1 in D1.
assert (D2 := envpartialViewEvalExpression e b_env (EVar x) H6).
rewrite evalExpression_f_correct in H2; rewrite H2 in D2.
rewrite idxs_set_from_value_f_correct in H3.
simpl in H3.
assert (slimSetIdsValue t vt' e = Some slim_e \/ forgetIdsValue t e = Some slim_e).
destruct D, D0, D1, D2; simpl in H8; simpl in H9; simpl in H10; simpl in H11; 
try rewrite H8 in H7; try rewrite H9 in H7; try rewrite H10 in H7; try rewrite H11 in H7; eauto.
rewrite H3 in H7.
induction vt; eauto; induction vx; eauto.
induction vt; eauto.
induction vt; eauto.
induction vt; eauto.
induction vt; eauto.
induction vt; eauto.
induction vt; eauto.
induction vt; eauto.
simpl.
destruct H8.
eapply slimSetIdsValue_correct; eauto.
eapply envpartialViewForgetVsSet; eauto.

simpl in H3; eapply envpartialViewForgetVsSet; eauto.
simpl in H3; eapply envpartialViewForgetVsSet; eauto.
simpl in H3.
assert (D := envpartialViewEvalExpression e b_env (EVar x) H2).
rewrite evalExpression_f_correct in H; rewrite H in D.
simpl in D.
destruct D; rewrite H4 in H3.
simpl.
induction v; try (solve [eapply envpartialViewForgetVsSet; eauto]); try (solve [eapply slimSetIdsValue_correct; eauto]).
simpl.
eapply envpartialViewForgetVsSet; eauto.
simpl in H3; eapply envpartialViewForgetVsSet; eauto.

induction 1; intros; subst.
simpl in H2.
eapply slimPartialViewInstruction_s_correct; eauto.
assert (D := envpartialViewEvalSec_expression_if e env evn (Bool true) cond H2).
destruct D.
assert (D := H4 H).
simpl in H3.
rewrite D in H3.
eapply slimPartialViewInstruction_s_correct; eauto.
unfold slimPartialViewevalElif_instruction in H3.
rewrite H4 in H3.
case_eq (forgetListElif_instruction e (IElif cond ins_true ins_false)); intros; rewrite H5 in H3; [|inversion H3].
eapply forgetListElif_instruction_correct; eauto.
eapply evalInstruction_sIElifTrue; eauto.
assert (D := envpartialViewEvalSec_expression_if e env evn (Bool false) cond H2).
destruct D.
assert (D := H4 H).
simpl in H3.
rewrite D in H3.
eapply slimPartialViewElif_instruction_correct; eauto.
unfold slimPartialViewevalElif_instruction in H3.
rewrite H4 in H3.
case_eq (forgetListElif_instruction e (IElif cond ins_true ins_false)); intros; rewrite H5 in H3; [|inversion H3].
assert (evalElif_instruction (env, evn) (IElif cond ins_true ins_false) res).
eapply evalInstruction_sIElifFalse; eauto.
eapply forgetListElif_instruction_correct; eauto.

induction 1; intros; subst.
inversion H0.
inversion H0.
assert (D := envpartialViewEvalExpression e env (Ebinary Equal (EVar var) exp) H2).
apply evalExpression_f_correct in H.
rewrite H in D.
destruct D.
unfold slimPartialViewevalCase_instruction in H3.
rewrite H4 in H3.
eapply slimPartialViewInstruction_s_correct; eauto.
assert (evalCase_instruction (env, evn) var (ICaseCons (exp, ins_s) case_t) res).
eapply evalICaseConsTrue; eauto.
apply evalExpression_f_correct; auto.
unfold slimPartialViewevalCase_instruction in H3.
rewrite H4 in H3.
case_eq (forgetListCase_instruction e (ICaseCons (exp, ins_s) case_t)); intros; rewrite H6 in H3; [|inversion H3].
eapply forgetListCase_instruction_correct; eauto.
assert (D := envpartialViewEvalExpression e env (Ebinary Equal (EVar var) exp) H2).
apply evalExpression_f_correct in H.
rewrite H in D.
destruct D.
unfold slimPartialViewevalCase_instruction in H3.
rewrite H4 in H3.
eapply IHevalCase_instruction; eauto.
assert (evalCase_instruction (env, evn) var (ICaseCons (exp, ins_s) case_t) res).
eapply evalICaseConsFalse; eauto.
apply evalExpression_f_correct; auto.
unfold slimPartialViewevalCase_instruction in H3.
rewrite H4 in H3.
case_eq (forgetListCase_instruction e (ICaseCons (exp, ins_s) case_t)); intros; rewrite H6 in H3; [|inversion H3].
eapply forgetListCase_instruction_correct; eauto.

induction 1; intros; subst.
simpl in H2.
eapply slimPartialViewevalInstruction_correct; eauto.
simpl in H3.
case_eq (slimPartialViewevalInstruction e h); intros; rewrite H4 in H3; [|inversion H3].
eapply slimPartialViewevalInstruction_correct in H4; eauto.
simpl in H4.
eapply IHevalInstruction_s; eauto.
inversion H0.
Qed.

Definition reasonably_smallest_environment env := {| 
        localVars := StringMapModule.map 
                (fun v => match v with | ConstantValue va => ConstantValue va | VariableValue v => VariableValue Undefined end )env.(localVars);
        globalVars := StringMapModule.map 
                (fun v => match v with | ConstantValue va => ConstantValue va | VariableValue v => VariableValue Undefined end ) env.(globalVars);
        funDefs := env.(funDefs);
        localfunDecls := env.(localfunDecls);
        externals := 
          StringMapModule.map 
            (fun m => 
              StringMapModule.map 
                (fun v => match v with | Environment.EVar (VariableValue v) => Environment.EVar (VariableValue Undefined) | r => r  end ) m )
          env.(externals)  |}.

Theorem reasonably_smallest_environment_correct : forall env,
    envpartialView (reasonably_smallest_environment env) env.
Proof.
unfold envpartialView.
simpl.
intros.
repeat (split; auto); unfold mapPartialView; intros; rewrite StringMapModule.gmap.
induction (StringMapModule.get id (localVars env)); eauto; induction a; eauto.
induction (StringMapModule.get id (globalVars env)); eauto; induction a; eauto.
induction (StringMapModule.get id (externals env)); auto.
intros.
rewrite StringMapModule.gmap.
induction (StringMapModule.get i a); auto.
induction a0; auto.
induction c; eauto.
right; eauto.
Qed.

