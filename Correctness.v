Require Import Environment Values Semantics MapTypes Event PartialView.
Require Import Ast.
Require Import List String ZArith Lia.
Import ListNotations.

Definition is_runtime_env_of e r := envpartialView (reasonably_smallest_environment e) r.

Inductive assoc_arg_value : env -> list expression_or_string_lit -> list value -> Prop :=
| assoc_arg_value_Nil : 
    forall run_env,
    assoc_arg_value run_env [] []
| assoc_arg_value_ConsExp : 
    forall run_env e v t tres,
    evalExpression run_env e v ->
    assoc_arg_value run_env t tres ->
    assoc_arg_value run_env ((Exp e)::t) (v::tres)
| assoc_arg_value_ConsSTR : 
    forall run_env e t tres,
    assoc_arg_value run_env t tres ->
    assoc_arg_value run_env ((StringLit e)::t) ((STR e)::tres)
.

(* Here
 *)

Inductive declaration : Type :=
| VarDec : String_Equality.t -> bool -> (name * list expression) -> declaration
| RecDec : String_Equality.t -> (list (String_Equality.t * name)) -> declaration
| UseDec : declaration
| STypDec : String_Equality.t -> name * list expression -> declaration
| PragDec : pragma -> declaration.

Definition prep_type_designator typ := match typ with
| TypeSimple n => (n, [])
| TypeComposeArray1 n e => (n, [e])
| TypeComposeArray2 n e e' => (n, [e; e'])
end.

Fixpoint declaration_set_of_declaration_de_base decs := match decs with
| [] => []
| (Declaration_d_objet (ConstantVar vid typ _))::t => (VarDec vid true (prep_type_designator typ))::(declaration_set_of_declaration_de_base t)
| (Declaration_d_objet (VariableVar vid typ))::t => (VarDec vid false (prep_type_designator typ))::(declaration_set_of_declaration_de_base t)
| (Definition_de_type_Article rname rbody)::t => (RecDec rname rbody)::(declaration_set_of_declaration_de_base t)
| (Declaration_de_sous_type tname typ)::t => (STypDec tname (prep_type_designator typ))::(declaration_set_of_declaration_de_base t)
| Use_Clause::t => UseDec::(declaration_set_of_declaration_de_base t)
| (Pragma_Declaration p)::t => (PragDec p)::(declaration_set_of_declaration_de_base t)
| _::t => declaration_set_of_declaration_de_base t
end.

Fixpoint declaration_set_of_declaration_1_body decs := match decs with
| [] => []
| (Body_Declaration_d_objet (ConstantVar vid typ _))::t => (VarDec vid true (prep_type_designator typ))::(declaration_set_of_declaration_1_body t)
| (Body_Declaration_d_objet (VariableVar vid typ))::t => (VarDec vid false (prep_type_designator typ))::(declaration_set_of_declaration_1_body t)
| (Body_Definition_de_type_Article rname rbody)::t => (RecDec rname rbody)::(declaration_set_of_declaration_1_body t)
| (Body_Declaration_de_sous_type tname typ)::t => (STypDec tname (prep_type_designator typ))::(declaration_set_of_declaration_1_body t)
| Body_Use_Clause::t => UseDec::(declaration_set_of_declaration_1_body t)
| (Body_Pragma_Declaration p)::t => (PragDec p)::(declaration_set_of_declaration_1_body t)
end.

Inductive include_except_prag : list declaration -> list declaration -> Prop :=
| include_except_prag_Nil : include_except_prag [] []
| include_except_prag_HeadEq : forall o t t', include_except_prag t t' -> include_except_prag (o::t) (o::t')
| include_except_prag_HeadPragma : forall o t t', include_except_prag t t' -> include_except_prag t (PragDec o::t')
.

Lemma include_except_prag_refl : forall l, include_except_prag l l.
Proof.
induction l.
apply include_except_prag_Nil.
apply include_except_prag_HeadEq; auto.
Qed.

Definition env_optimization_correct e e' :=
  StringMapModule.map_eq e.(localVars) e'.(localVars) /\
  StringMapModule.map_eq e.(globalVars) e'.(globalVars) /\
  StringMapModule.map_eq e.(localfunDecls) e'.(localfunDecls) /\
  (
    forall i,
    match StringMapModule.get i e.(funDefs), StringMapModule.get i e'.(funDefs) with
    | Some f, Some f' => forall inenv inevnt res params,
        is_runtime_env_of e inenv ->
        execFunction (inenv, inevnt) f params res -> 
        execFunction (inenv, inevnt) f' params res
    | None, None => True
    | _, _ => False
    end
  ) /\
  (
    forall i,
    match StringMapModule.get i e.(externals), StringMapModule.get i e'.(externals) with
    | Some v, Some v' => StringMapModule.map_eq v v'
    | None, None => True
    | _, _ => False
    end
  ).

Definition env_optimization_correct' smallest_env e e' :=
  StringMapModule.map_eq e.(localVars) e'.(localVars) /\
  StringMapModule.map_eq e.(globalVars) e'.(globalVars) /\
  StringMapModule.map_eq e.(localfunDecls) e'.(localfunDecls) /\
  (
    forall i,
    match StringMapModule.get i e.(funDefs), StringMapModule.get i e'.(funDefs) with
    | Some f, Some f' => forall inenv inevnt res params,
        envpartialView smallest_env inenv ->
        execFunction (inenv, inevnt) f params res -> 
        execFunction (inenv, inevnt) f' params res
    | None, None => True
    | _, _ => False
    end
  ) /\
  (
    forall i,
    match StringMapModule.get i e.(externals), StringMapModule.get i e'.(externals) with
    | Some v, Some v' => StringMapModule.map_eq v v'
    | None, None => True
    | _, _ => False
    end
  ).
  
Theorem env_optimization_correct_equiv : forall e e',
  env_optimization_correct e e' = env_optimization_correct' (reasonably_smallest_environment e) e e'.
unfold env_optimization_correct.
unfold env_optimization_correct'.
intros.
f_equal; auto.
Qed.

Theorem env_optimization_correct_refl : forall smallest_env e, env_optimization_correct' smallest_env e e.
Proof.
unfold env_optimization_correct'.
intros.
repeat (split; try apply StringMapModule.map_eq_refl); intros.
induction (StringMapModule.get i (funDefs e)); auto.
induction (StringMapModule.get i (externals e)); auto.
apply StringMapModule.map_eq_refl.
Qed.

Theorem env_optimization_correct_declareGlobalVar : forall smallest_env id value e e' de de', env_optimization_correct' smallest_env e e' -> 
  declareGlobalVar id value e = Some de -> 
  declareGlobalVar id value e' = Some de' -> 
  env_optimization_correct' smallest_env de de'
.
Proof.
unfold env_optimization_correct.
unfold declareGlobalVar.
intros.
destruct H.
destruct H2; rewrite H2 in *.
destruct H3; rewrite H3 in *.
destruct H4.
case_eq (StringMapModule.get id (globalVars e')); intros; rewrite H6 in *; [inversion H0|].
case_eq (StringMapModule.get id (funDefs e)); intros; rewrite H7 in *; [inversion H0|].
case_eq (StringMapModule.get id (funDefs e')); intros; rewrite H8 in *; [inversion H1|].
case_eq (StringMapModule.get id (localfunDecls e')); intros; rewrite H9 in *; [inversion H0|].
case_eq (StringMapModule.get id (externals e)); intros; rewrite H10 in *; inversion H0.
case_eq (StringMapModule.get id (externals e')); intros; rewrite H11 in *; inversion H1.
simpl.
repeat (split; try apply StringMapModule.map_eq_refl); auto; intros.
apply StringMapModule.map_eq_set_cons; auto.
Qed.

Theorem env_optimization_correct_defineFunction : forall f f' e e' opt_env de de', env_optimization_correct' opt_env e e' -> 
  f_name f = f_name f' ->
  (forall run_env inevnt res params,
        envpartialView opt_env run_env ->
        execFunction (run_env, inevnt) f params res -> 
        execFunction (run_env, inevnt) f' params res) ->
  defineFunction f e = Some de -> 
  defineFunction f' e' = Some de' -> 
  env_optimization_correct' opt_env de de'
.
Proof.
unfold env_optimization_correct.
unfold defineFunction.
intros.
destruct H; rewrite H in *.
rewrite H0 in *.
destruct H4; rewrite H4 in *.
destruct H5.
destruct H6.
case_eq (StringMapModule.get (f_name f') (localVars e')); intros; rewrite H8 in *; [inversion H2|].
case_eq (StringMapModule.get (f_name f') (globalVars e')); intros; rewrite H9 in *; [inversion H2|].
case_eq (StringMapModule.get (f_name f') (externals e)); intros; rewrite H10 in *; [inversion H2|].
case_eq (StringMapModule.get (f_name f') (externals e')); intros; rewrite H11 in *; [inversion H3|].
case_eq (StringMapModule.get (f_name f') (funDefs e)); intros; rewrite H12 in *; inversion H2.
case_eq (StringMapModule.get (f_name f') (funDefs e')); intros; rewrite H13 in *; inversion H3.
simpl.
repeat (split; auto).
intros.
simpl.
rewrite StringMapModule.gsspec.
rewrite StringMapModule.gsspec.
case_eq (String_Equality.eq i (f_name f')); auto.
intros; auto.
apply H6.
Qed.

Theorem env_optimization_correct_declareFunction : forall id fparams run_env e e' de de', env_optimization_correct' run_env e e' -> 
  declareFunction id fparams e = Some de -> 
  declareFunction id fparams e' = Some de' -> 
  env_optimization_correct' run_env de de'
.
Proof.
unfold env_optimization_correct.
unfold declareFunction.
intros.
destruct H; rewrite H in *.
destruct H2; rewrite H2 in *.
destruct H3; rewrite H3 in *.
destruct H4.
case_eq (StringMapModule.get id (localVars e')); intros; rewrite H6 in *; [inversion H0|].
case_eq (StringMapModule.get id (globalVars e')); intros; rewrite H7 in *; [inversion H0|].
case_eq (StringMapModule.get id (externals e)); intros; rewrite H8 in *; [inversion H0|].
case_eq (StringMapModule.get id (externals e')); intros; rewrite H9 in *; [inversion H1|].
case_eq (StringMapModule.get id (funDefs e)); intros; rewrite H10 in *; [inversion H0|].
case_eq (StringMapModule.get id (funDefs e')); intros; rewrite H11 in *; [inversion H1|].
case_eq (StringMapModule.get id (localfunDecls e')); intros; rewrite H12 in *; inversion H0; inversion H1.
simpl.
repeat (split; auto).
apply StringMapModule.map_eq_set_cons; auto.
Qed.

Definition preprocessor : Type := ((list string) * bool) -> (string * bool) -> ( (StringMapModule.t ConstOrVar) * StringMapModule.t (list (ident * mode * name)) * StringMapModule.t (StringMapModule.t External) ).     

Definition getName u := match u with
| (Compilation_Body ed) => (ed.(f_name), false)
| (Compilation_PkgDecl id decls) => (id, false)
| (Compilation_PkgBody id decls fdecls) => (id, true)
end.

Definition compilation_correct_optimization (p : preprocessor) c optimizer := match c, optimizer p c with
| (incls, unt), (incls', unt') => forall globalV declF exts globalV' declF' exts',
  ( p incls (getName unt) = (globalV, declF, exts) -> 
    p incls' (getName unt') = (globalV', declF', exts') -> 
    match unt, unt' with
    | (Compilation_Body ed), (Compilation_Body ed') => forall de de', ed.(f_name) = ed'.(f_name) /\ (
        defineFunction ed ({|
            localVars := [];
            globalVars := globalV;
            funDefs := [];
            localfunDecls := declF;
            externals := exts
          |}) = Some de -> 
        defineFunction ed' ({|
            localVars := [];
            globalVars := globalV';
            funDefs := [];
            localfunDecls := declF';
            externals := exts'
          |}) = Some de' -> 
        env_optimization_correct de de')
    | (Compilation_PkgDecl id decls), (Compilation_PkgDecl id' decls') => 
        forall env_wd env_wd',
         id = id' /\ (include_except_prag (declaration_set_of_declaration_de_base decls) (declaration_set_of_declaration_de_base decls'))
                  /\ ( evalDeclaration_de_baseList ({|
                          localVars := [];
                          globalVars := globalV;
                          funDefs := [];
                          localfunDecls := declF;
                          externals := exts
                        |}) decls env_wd ->
                       evalDeclaration_de_baseList ({|
                          localVars := [];
                          globalVars := globalV';
                          funDefs := [];
                          localfunDecls := declF';
                          externals := exts'
                        |}) decls' env_wd' ->
                       env_optimization_correct env_wd env_wd'
                       )
    | (Compilation_PkgBody id decls fdecls), (Compilation_PkgBody id' decls' fdecls') =>
        forall env_wd env_wf env_wd' env_wf',
         id = id' /\ (include_except_prag (declaration_set_of_declaration_1_body decls) (declaration_set_of_declaration_1_body decls'))
                  /\ ( evalDeclaration_1_bodyList ({|
                          localVars := [];
                          globalVars := globalV;
                          funDefs := [];
                          localfunDecls := declF;
                          externals := exts
                        |}) decls env_wd ->
                       evalDeclaration_2_bodyList env_wd fdecls env_wf ->
                       evalDeclaration_1_bodyList ({|
                          localVars := [];
                          globalVars := globalV';
                          funDefs := [];
                          localfunDecls := declF';
                          externals := exts'
                        |}) decls' env_wd' ->
                       evalDeclaration_2_bodyList env_wd' fdecls' env_wf' ->
                       env_optimization_correct env_wf env_wf'
                       )
    | _, _ => False
    end)
end.








