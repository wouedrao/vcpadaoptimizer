Require Import Map MapTypes Values Environment Event Semantics Semantics_f Correctness Determinism PartialView TacUtils Determinism.
Require Import Ast.
Require Import List String ZArith Lia Bool.
Import ListNotations.

(* Definition simplEqualityForValue v v' := match v, v' with
| Int n, Int n' => Z.eqb n n'
| Bool n, Bool n' => Bool.eqb n n'
| _, _ => false
end.

Lemma simplEqualityForValue_impl: forall e e', simplEqualityForValue e e' = true -> e = e'.
Proof.
induction e; induction e'; simpl; auto; intros; inversion H.
clear H1.
f_equal.
apply Z.eqb_eq; auto.
f_equal.
apply Bool.eqb_true_iff; auto.
Qed. *)

(* Lemma id_setIdemPotem: forall ids r v, v<>Undefined ->
  id_select_from_value r ids = Some v ->
  id_set_from_value r ids v = Some r.
Proof.
induction r; simpl.
intros.
inversion H0; subst; contradiction.
intros; inversion H0.
intros; inversion H0.
intros; inversion H0.
intros; inversion H0.
unfold record_select.
case_eq (StringMapModule.get ids r).
intros.
f_equal.
f_equal.
inversion H1; subst.
apply mapSetIdempoten; auto.
intros.
inversion H1; subst; contradiction.
Qed.

Lemma ids_setIdemPotem: forall ids r v,
  v <> Undefined ->
  ids_select_from_value_f r ids = Some v ->
  ids_set_from_value_f r ids v = Some r /\ r <> Undefined.
Proof.
induction ids; simpl; auto.
intros.
inversion H0; subst; auto.
intros.
case_eq (id_select_from_value r a); intros; rewrite H1 in H0; [|inversion H0].
apply IHids in H0; auto.
destruct H0.
rewrite H0.
split.
apply id_setIdemPotem; auto.
unfold not; intros.
rewrite H3 in H1.
simpl in H1.
inversion H1.
apply eq_sym in H5.
contradiction.
Qed. *)


(* Theorem expSetIdsIdem : forall env env' var v,
  evalExpression_f env (EVar var) = Some v -> 
  setIdsValue var v env = Some env' -> 
  env' = env.
Proof.
unfold evalExpression_f.
unfold setIdsValue.
unfold getIdsValue.
unfold getIdValue.
unfold setIdValue.
intros.
case_eq var; intros; rewrite H1 in *; [inversion H0|].
case_eq l; intros; rewrite H2 in *.
case_eq (StringMapModule.get t (localVars env)); intros; rewrite H3 in *.
induction c; [inversion H0|].
inversion H; inversion H0; subst.
induction env; simpl in *.
f_equal.
apply mapSetIdempoten; auto.
case_eq (StringMapModule.get t (globalVars env)); intros; rewrite H4 in *.
induction c; [inversion H0|].
inversion H; inversion H0; subst.
induction env; simpl in *.
f_equal.
apply mapSetIdempoten; auto.
inversion H0.
case_eq (StringMapModule.get t (localVars env)); intros; rewrite H3 in *.
induction c; [inversion H0|].

Theorem expSetIdsIdem : forall env env' var v,
  get
  evalExpression_f env (Ebinary Equal (EVar var) v) ->
  setIdsValue var v env = Some env' -> 
  env' = env. *)
  
(* Fixpoint select compvalue env case_t := match case_t with
| ICaseFinishDefault ins_s => None
| ICaseCons (exp, ins_s) case_t' => 
    (
      match evalExpression_f env (Ebinary Equal compvalue exp) with
      | Some (Bool true) => Some ins_s
      | Some (Bool false) => select compvalue env case_t'
      | _ => None
      end
    )
end.

Theorem select_correct: forall compvalue in_data case res,
  evalCase_instruction in_data compvalue case res -> forall ins env,
  envpartialView env (fst in_data) ->
  select compvalue env case = Some ins ->
  evalInstruction_s in_data ins res.
Proof.
induction 1; intros.
simpl in *; inversion H1; subst.
simpl in *; inversion H1; subst; auto.
assert (D := envpartialViewEvalExpression env0 env (Ebinary Equal compvalue exp) H1).
apply evalExpression_f_correct in H.
rewrite H in D.
unfold select in H2.
destruct D; rewrite H3 in H2; auto.
simpl in *; inversion H2; subst; auto.
inversion H2.
assert (D := envpartialViewEvalExpression env0 env (Ebinary Equal compvalue exp) H1).
apply evalExpression_f_correct in H.
rewrite H in D.
unfold select in H2.
destruct D; rewrite H3 in H2; auto.
eapply IHevalCase_instruction; eauto.
inversion H2.
Qed.

Fixpoint select_if env elif := match elif with
| IElse ins_s => Some ins_s
| IElif cond ins_true ins_false => 
    (
      match evalSec_opel_expression_if_f env cond with
      | Some (Bool true) => Some ins_true
      | Some (Bool false) => select_if env ins_false
      | _ => None
      end
    )
end.

Theorem select_if_correct: forall in_data case res,
  evalElif_instruction in_data case res -> forall ins env,
  envpartialView env (fst in_data) ->
  select_if env case = Some ins ->
  evalInstruction_s in_data ins res.
Proof.
induction 1; intros.
simpl in *; inversion H1; subst; auto.
assert (D := envpartialViewEvalSec_expression_if env0 env evn (Bool true) cond H1).
destruct D.
assert (D := H3 H).
unfold select_if in H2.
rewrite D in H2; auto.
simpl in *; inversion H2; subst; auto.
unfold select_if in H2.
rewrite H3 in H2; auto.
simpl in *; inversion H2; subst; auto.
assert (D := envpartialViewEvalSec_expression_if env0 env evn (Bool false) cond H1).
destruct D.
assert (D := H3 H).
unfold select_if in H2.
rewrite D in H2; auto.
eauto.
unfold select_if in H2.
rewrite H3 in H2; auto.
simpl in *; inversion H2; subst; auto.
Qed.

Fixpoint stmtAppend l l0 := match l with
| ISeqEnd h => ISeqCons h l0
| ISeqCons h t => ISeqCons h (stmtAppend t l0)
end.

Theorem stmtAppend_correct_stop : forall i res in_data,
  evalInstruction_s in_data i res -> forall i0 etype,
  extractErrorType res = Some etype -> 
  evalInstruction_s in_data (stmtAppend i i0) res.
Proof.
induction 1; intros.
apply equivExtractErr in H0; rewrite H0 in *.
simpl.
eapply evalInstruction_sISeqConsError; eauto.
simpl.
eapply evalInstruction_sISeqConsOk; eauto.
simpl.
eapply evalInstruction_sISeqConsError; eauto.
Qed.

Theorem stmtAppend_correct_continue : forall i res in_data,
  evalInstruction_s in_data i res -> forall i0 res',
  extractErrorType res = None -> 
  evalInstruction_s (extractEnv res, extractEvent res) i0 res' ->
  evalInstruction_s in_data (stmtAppend i i0) res'.
Proof.
induction 1; intros.
apply equivExtractNoErr in H0; rewrite H0 in H.
simpl.
eapply evalInstruction_sISeqConsOk; eauto.
simpl.
eapply evalInstruction_sISeqConsOk; eauto.
inversion H0.
Qed.

Definition forgetInstruction_s i env := 
  (
    match forgetListInstruction_s env i with
    | Some l => forgetIdsValueList l (forgetFinisherInstruction_s i env)
    | None => None
    end
  ).

Lemma forgetInstruction_is_lower : forall i env env', forgetInstruction_s i env = Some env' -> envpartialView env' env.
Proof.
unfold forgetInstruction_s.
intros.
case_eq (forgetListInstruction_s env i); intros; rewrite H0 in H; [|inversion H].
apply forgetListPartial in H; auto.
apply simplForgetFinisherInstruction_s.
Qed.

Lemma forgetInstruction_is_idempoten : forall i env env', forgetInstruction_s i env = Some env' -> forgetInstruction_s i env' = Some env'.
Proof.
intros.
assert (envpartialView env' env).
eapply forgetInstruction_is_lower; eauto.
unfold forgetInstruction_s in *.
case_eq (forgetListInstruction_s env i); intros; rewrite H1 in H; [|inversion H].
assert (forgetListInstruction_s env' i = Some l).
rewrite <- H1.
apply partialEnvForgetListIdemPotent_Instruction_s; auto.
rewrite H2.
eapply forgetListIdemPoten; eauto.
apply simplForgetFinisherInstruction_s.
Qed.

Lemma forgetInstruction_is_lower_after_exec : forall in_data f res,
evalInstruction_s in_data f res ->
  extractErrorType res = None -> forall e slim_e,
  envpartialView e (fst in_data) ->
  forgetInstruction_s f e = Some slim_e ->
  envpartialView slim_e (extractEnv res).
Proof.
unfold forgetInstruction_s.
intros.
case_eq (forgetListInstruction_s e f); intros; rewrite H3 in H2; [|inversion H2].
eapply forgetListInstruction_s_correct; eauto.
Qed.

Fixpoint optimize_Instruction env i := match i with
| ILoop cond ins => 
    (
      match evalSec_opel_w_f env cond with
      | Some (Bool false) => (INull, Some env)
      | _ => 
        (
          match forgetInstruction_s ins env with
          | None => (ILoop cond ins, None)
          | Some e => (ILoop cond (fst (optimize_Instruction_s e ins)), Some e)
          end
        )
      end
    )
| IIf cond ins_if ins_else => 
    (IIf cond (fst (optimize_Instruction_s env ins_if)) (fst (optimize_Elif_instruction env ins_else)), 
      slimPartialViewevalInstruction env i)
| (ICase var case) => 
    (ICase var (fst (optimize_Case_instruction env (EVar var) case)), slimPartialViewevalInstruction env i)
| _ => (i, slimPartialViewevalInstruction env i)
end
with optimize_Elif_instruction env i := match i with
| IElse ins => (IElse (fst (optimize_Instruction_s env ins)), slimPartialViewevalElif_instruction env i)
| IElif cond ins_true ins_false =>
    (IElif cond (fst (optimize_Instruction_s env ins_true)) (fst (optimize_Elif_instruction env ins_false)), 
      slimPartialViewevalElif_instruction env i)
end 
with optimize_Case_instruction env e i := match i with
| ICaseFinishDefault ins_s => 
    (ICaseFinishDefault (fst (optimize_Instruction_s env ins_s)), slimPartialViewevalCase_instruction env e i)
| ICaseCons (exp, ins_s) case_t => 
    (ICaseCons (exp, fst (optimize_Instruction_s env ins_s)) case_t, slimPartialViewevalCase_instruction env e i)
end
with optimize_Instruction_s env i := match i with
| ISeqEnd h => 
  (
    let h_opt := (fst (optimize_Instruction env h)) in 
    (match h_opt with
    | ICase var case => 
        (
          match select (EVar var) env case with
          | None => ISeqEnd h_opt
          | Some ins => ins
          end
        )
    | IIf cond ins_if ins_else => 
        (
          match evalSec_opel_expression_if_f env cond with
          | Some (Bool true) => ins_if
          | Some (Bool false) => 
            (
              match select_if env ins_else with
              | Some ins => ins
              | None => ISeqEnd h_opt
              end
            )
          | _ => ISeqEnd h_opt
          end
        )
    | _ => ISeqEnd h_opt
    end, slimPartialViewevalInstruction_s env i)
  ) 
| ISeqCons h t => 
  (
    let (h_opt, h_env) := optimize_Instruction env h in 
    let (t_opt, t_env) := 
      (
        match h_env with
        | Some outenv' => (optimize_Instruction_s outenv' t)
        | None => (t, None)
        end
      ) in
    (match h_opt with
    | ICase var case => 
        (
          match select (EVar var) env case with
          | None => (ISeqCons h_opt t_opt)
          | Some ins => (stmtAppend ins t_opt)
          end
        )
    | IIf cond ins_if ins_else => 
        (
          match evalSec_opel_expression_if_f env cond with
          | Some (Bool true) => (stmtAppend ins_if t_opt)
          | Some (Bool false) => 
            (
              match select_if env ins_else with
              | Some ins => (stmtAppend ins t_opt)
              | None => (ISeqCons h_opt t_opt)
              end
            )
          | _ => (ISeqCons h_opt t_opt)
          end
        )
    | ICall (SOInstructionBeginLoop e) =>
        (
          match t_opt with
          | ISeqEnd (ILoop _ _) => (ISeqCons h_opt t_opt)
          | ISeqCons (ILoop _ _) _ => (ISeqCons h_opt t_opt)
          | _ => t_opt
          end
        )
    | _ => (ISeqCons h_opt t_opt)
    end, t_env)
  )
end.

(*Lemma simplOPTWhile : forall e cond ins, (evalSec_opel_w_f e cond = Some (Bool false) /\ optimize_Instruction e (ILoop cond ins) = (INull, Some e)) \/ 
  (optimize_Instruction e (ILoop cond ins) = (ILoop cond (fst (optimize_Instruction_s e ins)), slimPartialViewevalInstruction e (ILoop cond ins))).
Proof.
intros.
unfold optimize_Instruction.
unfold fst.
induction (evalSec_opel_w_f e cond); simpl; auto.
induction a; simpl;auto.
induction n; simpl;auto.
Qed.*)

Theorem optimize_Instruction_correct : 
  forall in_data f res,
  evalInstruction in_data f res -> forall e,
  envpartialView e (fst in_data) ->
  evalInstruction in_data (fst (optimize_Instruction e f)) res /\ 
  (forall outenv, (snd (optimize_Instruction e f)) = Some outenv -> extractErrorType res = None -> envpartialView outenv (extractEnv res) )
with optimize_Elif_instruction_correct : 
  forall in_data f res,
  evalElif_instruction in_data f res -> forall e,
  envpartialView e (fst in_data) -> 
  evalElif_instruction in_data (fst (optimize_Elif_instruction e f)) res /\ 
  (forall outenv,  (snd (optimize_Elif_instruction e f)) = Some outenv -> extractErrorType res = None -> envpartialView outenv (extractEnv res) )
with optimize_Case_instruction_correct : 
  forall in_data exp f res,
  evalCase_instruction in_data exp f res -> forall e,
  envpartialView e (fst in_data) ->
  evalCase_instruction in_data exp (fst (optimize_Case_instruction e exp f)) res /\ 
  (forall outenv,  (snd (optimize_Case_instruction e exp f)) = Some outenv -> extractErrorType res = None -> envpartialView outenv (extractEnv res) )
with optimize_Instruction_s_correct : 
  forall in_data f res,
  evalInstruction_s in_data f res -> forall e,
  envpartialView e (fst in_data) ->
  evalInstruction_s in_data (fst (optimize_Instruction_s e f)) res /\ 
  (forall outenv,  (snd (optimize_Instruction_s e f)) = Some outenv -> extractErrorType res = None -> envpartialView outenv (extractEnv res) )
.
Proof.
induction 1; intros.
simpl; split; [econstructor; eauto|intros;inversion H0; subst; auto].
simpl; split; [econstructor; eauto|intros;inversion H0; subst; auto].
simpl; split; [econstructor; eauto|intros;inversion H0; subst; auto].
simpl; split; [econstructor; eauto|intros;inversion H0; subst; auto].
simpl (fst (optimize_Instruction e (IAssign var aff))).
split.
econstructor; eauto.
unfold optimize_Instruction.
unfold snd.
intros.
eapply slimPartialViewevalInstruction_correct; eauto.
econstructor; eauto.
split.
simpl.
eapply evalInstructionIIfTrue; eauto.
eapply optimize_Instruction_s_correct; eauto.
unfold optimize_Instruction.
unfold snd.
intros.
eapply slimPartialViewevalInstruction_correct; eauto.
econstructor; eauto.
split.
simpl.
eapply evalInstructionIIfFalse; eauto.
eapply optimize_Elif_instruction_correct; eauto.
unfold optimize_Instruction.
unfold snd.
intros.
eapply slimPartialViewevalInstruction_correct; eauto.
eapply evalInstructionIIfFalse; eauto.
split.
simpl.
eapply evalInstructionICase; eauto.
eapply optimize_Case_instruction_correct; eauto.
unfold optimize_Instruction.
unfold snd.
intros.
eapply slimPartialViewevalInstruction_correct; eauto.
eapply evalInstructionICase; eauto.
(*Loop True*)
assert (D := envpartialViewEvalSec_opel_w e env cond H2).
apply evalSec_opel_w_f_correct in H.
rewrite H in D.
simpl.
destruct D; rewrite H3.
case_eq (forgetInstruction_s ins e); intros; simpl.
split.
apply evalSec_opel_w_f_correct in H.
eapply evalInstructionILoopTrueOk with (env':=env') (evn':=evn'); eauto.
eapply optimize_Instruction_s_correct; eauto.
apply forgetInstruction_is_lower in H4.
eapply envpartialView_trans; eauto.
assert (extractErrorType (OK env' evn') = None) by (simpl; auto).
assert 
  (D := 
  forgetInstruction_is_lower_after_exec 
  (env, evn) 
  ins 
  (OK env' evn') H0 H5 e e0 H2 H4).
assert (D' := IHevalInstruction e0 D).
destruct D'.
simpl in H6.
case_eq (evalSec_opel_w_f e0 cond); intros; rewrite H8 in *.
assert (forgetInstruction_s ins e0 = Some e0).
eapply forgetInstruction_is_idempoten; eauto.
rewrite H9 in H6.
induction v; auto.
induction n; auto.
apply forgetInstruction_is_lower in H4.
assert (D' := envpartialViewEvalSec_opel_w e0 e cond H4).
rewrite H8, H3 in D'.
destruct D'; inversion H10.
simpl in H6.
assert (forgetInstruction_s ins e0 = Some e0).
eapply forgetInstruction_is_idempoten; eauto.
rewrite H9 in H6.
simpl in H6; auto.
assert (extractErrorType (OK env' evn') = None) by (simpl; auto).
assert 
  (D := 
  forgetInstruction_is_lower_after_exec 
  (env, evn) 
  ins 
  (OK env' evn') H0 H5 e e0 H2 H4).
intros.
eapply IHevalInstruction; eauto.
inversion H6; subst.
simpl.
assert (forgetInstruction_s ins outenv = Some outenv).
eapply forgetInstruction_is_idempoten; eauto.
rewrite H8.
induction (evalSec_opel_w_f outenv cond); auto.
induction a; auto.
induction n; auto.
split.
eapply evalInstructionILoopTrueOk with (env':=env') (evn':=evn'); eauto.
apply evalSec_opel_w_f_correct; eauto.
intros.
inversion H5.
case_eq (forgetInstruction_s ins e); intros; simpl.
split.
apply evalSec_opel_w_f_correct in H.
eapply evalInstructionILoopTrueOk with (env':=env') (evn':=evn'); eauto.
eapply optimize_Instruction_s_correct; eauto.
apply forgetInstruction_is_lower in H4.
eapply envpartialView_trans; eauto.
assert (extractErrorType (OK env' evn') = None) by (simpl; auto).
assert 
  (D := 
  forgetInstruction_is_lower_after_exec 
  (env, evn) 
  ins 
  (OK env' evn') H0 H5 e e0 H2 H4).
assert (D' := IHevalInstruction e0 D).
destruct D'.
simpl in H6.
case_eq (evalSec_opel_w_f e0 cond); intros; rewrite H8 in *.
assert (forgetInstruction_s ins e0 = Some e0).
eapply forgetInstruction_is_idempoten; eauto.
rewrite H9 in H6.
induction v; auto.
induction n; auto.
apply forgetInstruction_is_lower in H4.
assert (D' := envpartialViewEvalSec_opel_w e0 e cond H4).
rewrite H8, H3 in D'.
destruct D'; inversion H10.
simpl in H6.
assert (forgetInstruction_s ins e0 = Some e0).
eapply forgetInstruction_is_idempoten; eauto.
rewrite H9 in H6.
simpl in H6; auto.
assert (extractErrorType (OK env' evn') = None) by (simpl; auto).
assert 
  (D := 
  forgetInstruction_is_lower_after_exec 
  (env, evn) 
  ins 
  (OK env' evn') H0 H5 e e0 H2 H4).
intros.
eapply IHevalInstruction; eauto.
inversion H6; subst.
simpl.
assert (forgetInstruction_s ins outenv = Some outenv).
eapply forgetInstruction_is_idempoten; eauto.
rewrite H8.
induction (evalSec_opel_w_f outenv cond); auto.
induction a; auto.
induction n; auto.
split.
eapply evalInstructionILoopTrueOk with (env':=env') (evn':=evn'); eauto.
apply evalSec_opel_w_f_correct; eauto.
intros.
inversion H5.

simpl.
assert (D := envpartialViewEvalSec_opel_w e0 env cond H1).
apply evalSec_opel_w_f_correct in H; rewrite H in D; apply evalSec_opel_w_f_correct in H.
destruct D; rewrite H2.
case_eq (forgetInstruction_s ins e0); intros; simpl.
split.
eapply evalInstructionILoopTrueError; eauto.
eapply optimize_Instruction_s_correct; eauto.
apply forgetInstruction_is_lower in H3.
eapply envpartialView_trans; eauto.
intros.
inversion H5.
split.
eapply evalInstructionILoopTrueError; eauto.
intros.
inversion H5.
case_eq (forgetInstruction_s ins e0); intros; simpl.
split.
eapply evalInstructionILoopTrueError; eauto.
eapply optimize_Instruction_s_correct; eauto.
apply forgetInstruction_is_lower in H3.
eapply envpartialView_trans; eauto.
intros.
inversion H5.
split.
eapply evalInstructionILoopTrueError; eauto.
intros.
inversion H5.

(*Loop False*)
simpl.
assert (D := envpartialViewEvalSec_opel_w e env cond H0).
apply evalSec_opel_w_f_correct in H; rewrite H in D; apply evalSec_opel_w_f_correct in H.
destruct D; rewrite H1.
simpl.
split.
econstructor; eauto.
intros.
inversion H2; subst; auto.
case_eq (forgetInstruction_s ins e); intros; simpl.
split.
eapply evalInstructionILoopFalse; eauto.
intros.
inversion H3; subst; auto.
apply forgetInstruction_is_lower in H2.
eapply envpartialView_trans; eauto.
split.
eapply evalInstructionILoopFalse; eauto.
intros.
inversion H3; subst; auto.

(*Call*)
simpl (fst (optimize_Instruction e (ICall f))).
split.
econstructor; eauto.
unfold optimize_Instruction.
unfold snd.
intros.
eapply slimPartialViewevalInstruction_correct; eauto.
econstructor; eauto.


(***)
induction 1; intros.
simpl.
split. 
econstructor.
eapply optimize_Instruction_s_correct; eauto.
intros.
eapply slimPartialViewInstruction_s_correct; eauto.

simpl (fst (optimize_Elif_instruction e (IElif cond ins_true ins_false))).
split.
eapply evalInstruction_sIElifTrue; eauto.
eapply optimize_Instruction_s_correct; eauto.
intros.
eapply slimPartialViewElif_instruction_correct with (f := (IElif cond ins_true ins_false)); eauto.
eapply evalInstruction_sIElifTrue; eauto.
assert (D := IHevalElif_instruction e H1).
destruct D.
split; auto.
simpl.
eapply evalInstruction_sIElifFalse; eauto.
intros.
eapply slimPartialViewElif_instruction_correct with (f := (IElif cond ins_true ins_false)); eauto.
eapply evalInstruction_sIElifFalse; eauto.

(**)
induction 1; intros.
simpl (fst (optimize_Case_instruction e compvalue (ICaseFinishDefault ins_s))).
split.
eapply evalICaseFinishDefaultOk with (env':=env') (evn':=evn'); eauto.
eapply optimize_Instruction_s_correct; eauto.
intros.
inversion H2.
simpl (fst (optimize_Case_instruction e0 compvalue (ICaseFinishDefault ins_s))).
split.
eapply evalICaseFinishDefaultErr with (env':=env') (evn':=evn'); eauto.
eapply optimize_Instruction_s_correct; eauto.
intros.
inversion H2.

simpl (fst (optimize_Case_instruction e compvalue (ICaseCons (exp, ins_s) case_t))).
split.
eapply evalICaseConsTrue; eauto.
eapply optimize_Instruction_s_correct; eauto.
intros.
eapply slimPartialViewCase_instruction_correct with (f := (ICaseCons (exp, ins_s) case_t)) (exp:=compvalue); eauto.
eapply evalICaseConsTrue; eauto.
assert (D := IHevalCase_instruction e H1).
destruct D.
split; auto.
simpl.
eapply evalICaseConsFalse; eauto.
intros.
eapply slimPartialViewCase_instruction_correct with (f := (ICaseCons (exp, ins_s) case_t)) (exp:=compvalue); eauto.
eapply evalICaseConsFalse; eauto.

(**)
induction 1; intros.
split.
assert (D := optimize_Instruction_correct (env, evn) i res H e H0).
destruct D.
simpl.
case_eq (optimize_Instruction e i); intros; rewrite H3 in *.
induction i0; simpl.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
inversion H1; subst.
assert (D := envpartialViewEvalSec_expression_if e env evn (Bool true) s H0).
destruct D.
assert (D:= H4 H10).
rewrite D.
simpl; auto.
rewrite H4.
simpl; auto.
eapply evalInstruction_sISeqEnd; auto.
assert (D := envpartialViewEvalSec_expression_if e env evn (Bool false) s H0).
destruct D.
assert (D:= H4 H10).
rewrite D.
case_eq (select_if e e0); intros.
simpl.
eapply select_if_correct; eauto.
simpl; auto.
eapply evalInstruction_sISeqEnd; auto.
rewrite H4.
simpl; auto.
eapply evalInstruction_sISeqEnd; auto.
inversion H1; subst.
case_eq (select (EVar l) e c); intros.
simpl.
eapply select_correct; eauto.
simpl; auto.
eapply evalInstruction_sISeqEnd; auto.
eapply evalInstruction_sISeqEnd; auto.
eapply evalInstruction_sISeqEnd; auto.
intros.
simpl in H1.
eapply slimPartialViewevalInstruction_correct with (f := i); eauto.

simpl.
case_eq (optimize_Instruction e h); intros.
assert (D := optimize_Instruction_correct (env, evn) h (OK env' evn') H e H1).
destruct D.
rewrite H2 in *.
simpl in H3, H4.
induction o.
case_eq (optimize_Instruction_s a t); intros.
assert (envpartialView a env').
apply H4; auto.
assert (D := IHevalInstruction_s a H6).
destruct D.
rewrite H5 in *.
simpl in H7, H8.
simpl.
split; auto.
induction i.
eapply evalInstruction_sISeqConsOk; eauto.
eapply evalInstruction_sISeqConsOk; eauto.
eapply evalInstruction_sISeqConsOk; eauto.
inversion H3; subst.
assert (D := envpartialViewEvalSec_expression_if e env evn (Bool true) s H1).
destruct D.
assert (D:= H9 H15).
rewrite D.
eapply stmtAppend_correct_continue; eauto.
rewrite H9.
eapply evalInstruction_sISeqConsOk; eauto.
assert (D := envpartialViewEvalSec_expression_if e env evn (Bool false) s H1).
destruct D.
assert (D:= H9 H15).
rewrite D.
case_eq (select_if e e0); intros.
eapply select_if_correct with (case := e0) in H10; eauto.
eapply stmtAppend_correct_continue; eauto.
eapply evalInstruction_sISeqConsOk; eauto.
rewrite H9.
eapply evalInstruction_sISeqConsOk; eauto.
inversion H3; subst.
case_eq (select (EVar l) e c); intros.
eapply select_correct with (res := (OK env' evn')) in H9; eauto.
eapply stmtAppend_correct_continue; eauto.
eapply evalInstruction_sISeqConsOk; eauto.
eapply evalInstruction_sISeqConsOk; eauto.

assert (( exists e, f = SOInstructionBeginLoop e /\ ((match f with
  | SOInstructionBeginLoop _ =>
      match i0 with
      | ISeqEnd (ILoop _ _) | ISeqCons (ILoop _ _) _ => ISeqCons (ICall f) i0
      | _ => i0
      end
  | _ => ISeqCons (ICall f) i0 end) = ISeqCons (ICall f) i0 \/ 
          (match f with
  | SOInstructionBeginLoop _ =>
      match i0 with
      | ISeqEnd (ILoop _ _) | ISeqCons (ILoop _ _) _ => ISeqCons (ICall f) i0
      | _ => i0
      end
  | _ => ISeqCons (ICall f) i0 end) = i0)) \/  (match f with
  | SOInstructionBeginLoop _ =>
      match i0 with
      | ISeqEnd (ILoop _ _) | ISeqCons (ILoop _ _) _ => ISeqCons (ICall f) i0
      | _ => i0
      end
  | _ => ISeqCons (ICall f) i0 end) = ISeqCons (ICall f) i0).
induction f; auto.
left.
exists e0.
split; auto.
induction i0; auto.
induction i; auto.
induction i; auto.
destruct H9.
destruct H9.
destruct H9; subst.
destruct H10; rewrite H9.
eapply evalInstruction_sISeqConsOk; eauto.
inversion H3; subst; auto.
inversion H14; subst; auto.
rewrite H9.
eapply evalInstruction_sISeqConsOk; eauto.
simpl; split; auto.
induction i.
eapply evalInstruction_sISeqConsOk; eauto.
eapply evalInstruction_sISeqConsOk; eauto.
eapply evalInstruction_sISeqConsOk; eauto.
inversion H3; subst.
assert (D := envpartialViewEvalSec_expression_if e env evn (Bool true) s H1).
destruct D.
assert (D:= H5 H11).
rewrite D.
eapply stmtAppend_correct_continue; eauto.
rewrite H5.
eapply evalInstruction_sISeqConsOk; eauto.
assert (D := envpartialViewEvalSec_expression_if e env evn (Bool false) s H1).
destruct D.
assert (D:= H5 H11).
rewrite D.
case_eq (select_if e e0); intros.
eapply select_if_correct with (case := e0) in H6; eauto.
eapply stmtAppend_correct_continue; eauto.
eapply evalInstruction_sISeqConsOk; eauto.
rewrite H5.
eapply evalInstruction_sISeqConsOk; eauto.
inversion H3; subst.
case_eq (select (EVar l) e c); intros.
eapply select_correct with (res := (OK env' evn')) in H5; eauto.
eapply stmtAppend_correct_continue; eauto.
eapply evalInstruction_sISeqConsOk; eauto.
eapply evalInstruction_sISeqConsOk; eauto.

assert (( exists e, f = SOInstructionBeginLoop e /\ ((match f with
  | SOInstructionBeginLoop _ =>
      match t with
      | ISeqEnd (ILoop _ _) | ISeqCons (ILoop _ _) _ => ISeqCons (ICall f) t
      | _ => t
      end
  | _ => ISeqCons (ICall f) t
  end) = ISeqCons (ICall f) t \/ 
          (match f with
  | SOInstructionBeginLoop _ =>
      match t with
      | ISeqEnd (ILoop _ _) | ISeqCons (ILoop _ _) _ => ISeqCons (ICall f) t
      | _ => t
      end
  | _ => ISeqCons (ICall f) t
  end) = t)) \/  (match f with
  | SOInstructionBeginLoop _ =>
      match t with
      | ISeqEnd (ILoop _ _) | ISeqCons (ILoop _ _) _ => ISeqCons (ICall f) t
      | _ => t
      end
  | _ => ISeqCons (ICall f) t
  end) = ISeqCons (ICall f) t).
induction f; auto.
left.
exists e0.
split; auto.
induction t; auto.
induction i; auto.
induction i; auto.
destruct H5.
destruct H5.
destruct H5; subst.
destruct H6; rewrite H5.
eapply evalInstruction_sISeqConsOk; eauto.
inversion H3; subst.
inversion H10; subst.
auto.
rewrite H5.

eapply evalInstruction_sISeqConsOk; eauto.
intros.
inversion H5.
simpl.
case_eq (optimize_Instruction e0 h); intros.
assert (D := optimize_Instruction_correct (env, evn) h (Error e env' evn') H e0 H0).
destruct D.
rewrite H1 in *.
simpl in H2, H3.
induction o.
case_eq (optimize_Instruction_s a t); intros.
simpl.
split; auto.
induction i.
eapply evalInstruction_sISeqConsError; eauto.
eapply evalInstruction_sISeqConsError; eauto.
eapply evalInstruction_sISeqConsError; eauto.
inversion H2; subst.
assert (D := envpartialViewEvalSec_expression_if e0 env evn (Bool true) s H0).
destruct D.
assert (D:= H5 H11).
rewrite D.
eapply stmtAppend_correct_stop; simpl; eauto.
rewrite H5.
eapply evalInstruction_sISeqConsError; eauto.
assert (D := envpartialViewEvalSec_expression_if e0 env evn (Bool false) s H0).
destruct D.
assert (D:= H5 H11).
rewrite D.
case_eq (select_if e0 e1); intros.
eapply select_if_correct with (case := e1) in H6; eauto.
eapply stmtAppend_correct_stop; simpl; eauto.
eapply evalInstruction_sISeqConsError; eauto.
rewrite H5.
eapply evalInstruction_sISeqConsError; eauto.
inversion H2; subst.
case_eq (select (EVar l) e0 c); intros.
eapply select_correct with (res := (Error e env' evn')) in H5; eauto.
eapply stmtAppend_correct_stop; simpl; eauto.
eapply evalInstruction_sISeqConsError; eauto.
eapply evalInstruction_sISeqConsError; eauto.
assert (( exists e, f = SOInstructionBeginLoop e /\ ((match f with
  | SOInstructionBeginLoop _ =>
      match i0 with
      | ISeqEnd (ILoop _ _) | ISeqCons (ILoop _ _) _ => ISeqCons (ICall f) i0
      | _ => i0
      end
  | _ => ISeqCons (ICall f) i0 end) = ISeqCons (ICall f) i0 \/ 
          (match f with
  | SOInstructionBeginLoop _ =>
      match i0 with
      | ISeqEnd (ILoop _ _) | ISeqCons (ILoop _ _) _ => ISeqCons (ICall f) i0
      | _ => i0
      end
  | _ => ISeqCons (ICall f) i0 end) = i0)) \/  (match f with
  | SOInstructionBeginLoop _ =>
      match i0 with
      | ISeqEnd (ILoop _ _) | ISeqCons (ILoop _ _) _ => ISeqCons (ICall f) i0
      | _ => i0
      end
  | _ => ISeqCons (ICall f) i0 end) = ISeqCons (ICall f) i0).
induction f; auto.
left.
exists e1.
split; auto.
induction i0; auto.
induction i; auto.
induction i; auto.
destruct H5.
destruct H5.
destruct H5; subst.
inversion H2; subst.
inversion H10; subst.
rewrite H5.
eapply evalInstruction_sISeqConsError; eauto.
intros.
inversion H6.
simpl; split; auto.
induction i.
eapply evalInstruction_sISeqConsError; eauto.
eapply evalInstruction_sISeqConsError; eauto.
eapply evalInstruction_sISeqConsError; eauto.
inversion H2; subst.
assert (D := envpartialViewEvalSec_expression_if e0 env evn (Bool true) s H0).
destruct D.
assert (D:= H4 H10).
rewrite D.
eapply stmtAppend_correct_stop; simpl; eauto.
rewrite H4.
eapply evalInstruction_sISeqConsError; eauto.
assert (D := envpartialViewEvalSec_expression_if e0 env evn (Bool false) s H0).
destruct D.
assert (D:= H4 H10).
rewrite D.
case_eq (select_if e0 e1); intros.
eapply select_if_correct with (case := e1) in H5; eauto.
eapply stmtAppend_correct_stop; simpl; eauto.
eapply evalInstruction_sISeqConsError; eauto.
rewrite H4.
eapply evalInstruction_sISeqConsError; eauto.
inversion H2; subst.
case_eq (select (EVar l) e0 c); intros.
eapply select_correct with (res := (Error e env' evn')) in H4; eauto.
eapply stmtAppend_correct_stop; simpl; eauto.
eapply evalInstruction_sISeqConsError; eauto.
eapply evalInstruction_sISeqConsError; eauto.
assert (( exists e, f = SOInstructionBeginLoop e /\ ((match f with
  | SOInstructionBeginLoop _ =>
      match t with
      | ISeqEnd (ILoop _ _) | ISeqCons (ILoop _ _) _ => ISeqCons (ICall f) t
      | _ => t
      end
  | _ => ISeqCons (ICall f) t
  end) = ISeqCons (ICall f) t \/ 
          (match f with
  | SOInstructionBeginLoop _ =>
      match t with
      | ISeqEnd (ILoop _ _) | ISeqCons (ILoop _ _) _ => ISeqCons (ICall f) t
      | _ => t
      end
  | _ => ISeqCons (ICall f) t
  end) = t)) \/  (match f with
  | SOInstructionBeginLoop _ =>
      match t with
      | ISeqEnd (ILoop _ _) | ISeqCons (ILoop _ _) _ => ISeqCons (ICall f) t
      | _ => t
      end
  | _ => ISeqCons (ICall f) t
  end) = ISeqCons (ICall f) t).
induction f; auto.
left.
exists e1.
split; auto.
induction t; auto.
induction i; auto.
induction i; auto.
destruct H4.
destruct H4.
destruct H4; subst.
inversion H2; subst.
inversion H9.
rewrite H4.
eapply evalInstruction_sISeqConsError; eauto.
Qed. *)


Fixpoint copyInPartialView_f env (sig : list (ident * mode * name)) := match sig with
| [] => Some ({| localVars := [];
        globalVars := env.(globalVars);
        funDefs := env.(funDefs);
        localfunDecls := env.(localfunDecls);
        externals := env.(externals) |})
| ((idSig, Ast.In, _)::tSig) => 
    (
      match copyInPartialView_f env tSig with
      | Some env' => (declareLocalVar idSig (ConstantValue Undefined) env') 
      | None => None
      end
    )
| ((idSig, _, _)::tSig) => 
    (
      match copyInPartialView_f env tSig with
      | Some env' => (declareLocalVar idSig (VariableValue Undefined) env') 
      | None => None
      end
    )
end.


Lemma declareLocalVarPartialViewUndefVsV : forall id v e e' r r',
  envpartialView e e' ->
  declareLocalVar id (VariableValue Undefined) e = Some r ->
  declareLocalVar id (VariableValue v) e' = Some r' ->
  envpartialView r r'.
Proof.
unfold declareLocalVar.
unfold envpartialView.
intros.
destruct H.
destruct H2.
destruct H3.
destruct H4.
case_eq (StringMapModule.get id (localVars e)); intros; rewrite H6 in *; [inversion H0|].
case_eq (StringMapModule.get id (localVars e')); intros; rewrite H7 in *; [inversion H1|].
case_eq (StringMapModule.get id (funDefs e)); intros; rewrite H8 in *; [inversion H0|].
case_eq (StringMapModule.get id (funDefs e')); intros; rewrite H9 in *; [inversion H1|].
case_eq (StringMapModule.get id (localfunDecls e)); intros; rewrite H10 in *; [inversion H0|].
case_eq (StringMapModule.get id (localfunDecls e')); intros; rewrite H11 in *; [inversion H1|].
case_eq (StringMapModule.get id (externals e)); intros; rewrite H12 in *; inversion H0; subst; simpl.
case_eq (StringMapModule.get id (externals e')); intros; rewrite H13 in *; inversion H1; subst; simpl.
repeat (split; auto).
eapply mapPartialView_setVUndef; eauto.
Qed.

Lemma declareLocalVarPartialViewUndefCsC : forall id v e e' r r',
  envpartialView e e' ->
  declareLocalVar id (ConstantValue Undefined) e = Some r ->
  declareLocalVar id (ConstantValue v) e' = Some r' ->
  envpartialView r r'.
Proof.
unfold declareLocalVar.
unfold envpartialView.
intros.
destruct H.
destruct H2.
destruct H3.
destruct H4.
case_eq (StringMapModule.get id (localVars e)); intros; rewrite H6 in *; [inversion H0|].
case_eq (StringMapModule.get id (localVars e')); intros; rewrite H7 in *; [inversion H1|].
case_eq (StringMapModule.get id (funDefs e)); intros; rewrite H8 in *; [inversion H0|].
case_eq (StringMapModule.get id (funDefs e')); intros; rewrite H9 in *; [inversion H1|].
case_eq (StringMapModule.get id (localfunDecls e)); intros; rewrite H10 in *; [inversion H0|].
case_eq (StringMapModule.get id (localfunDecls e')); intros; rewrite H11 in *; [inversion H1|].
case_eq (StringMapModule.get id (externals e)); intros; rewrite H12 in *; inversion H0; subst; simpl.
case_eq (StringMapModule.get id (externals e')); intros; rewrite H13 in *; inversion H1; subst; simpl.
repeat (split; auto).
unfold mapPartialView in H.
unfold mapPartialView.
clear -H.
intros.
assert (D := H id0).
rewrite StringMapModule.gsspec.
rewrite StringMapModule.gsspec.
case_eq (String_Equality.eq id0 id); intros; eauto.
Qed.

Lemma declareLocalVarPartialViewVVsV : forall id v e e' r r',
  envpartialView e e' ->
  declareLocalVar id v e = Some r ->
  declareLocalVar id v e' = Some r' ->
  envpartialView r r'.
Proof.
unfold declareLocalVar.
unfold envpartialView.
intros.
destruct H.
destruct H2.
destruct H3.
destruct H4.
case_eq (StringMapModule.get id (localVars e)); intros; rewrite H6 in *; [inversion H0|].
case_eq (StringMapModule.get id (localVars e')); intros; rewrite H7 in *; [inversion H1|].
case_eq (StringMapModule.get id (funDefs e)); intros; rewrite H8 in *; [inversion H0|].
case_eq (StringMapModule.get id (funDefs e')); intros; rewrite H9 in *; [inversion H1|].
case_eq (StringMapModule.get id (localfunDecls e)); intros; rewrite H10 in *; [inversion H0|].
case_eq (StringMapModule.get id (localfunDecls e')); intros; rewrite H11 in *; [inversion H1|].
case_eq (StringMapModule.get id (externals e)); intros; rewrite H12 in *; inversion H0; subst; simpl.
case_eq (StringMapModule.get id (externals e')); intros; rewrite H13 in *; inversion H1; subst; simpl.
repeat (split; auto).
apply mapPartialView_setV; auto.
Qed.

Theorem copyInPartialView_f_correct : forall sig params e e' r r',
  envpartialView e e' ->
  copyInPartialView_f e sig = Some r ->
  copyIn_f e' sig params = Some r' -> 
  envpartialView r r'.
Proof.
induction sig.
simpl; intros.
induction params; inversion H0; inversion H1; auto.
unfold envpartialView in *; auto.
simpl.
destruct H.
destruct H2.
destruct H5.
destruct H6.
repeat (split; auto).
unfold mapPartialView.
simpl; auto.
simpl.
intros.
induction a.
induction a.
induction b0.
case_eq (copyInPartialView_f e sig); intros; rewrite H2 in *; try einversion.
induction params; try einversion.
clear IHparams.
case_eq (copyIn_f e' sig params); induction a0; intros; rewrite H3 in *; try induction (evalExpression_f e' e1); try einversion.
assert (envpartialView e0 e2) by eauto.
eapply declareLocalVarPartialViewUndefCsC; eauto.
assert (envpartialView e0 e1) by eauto.
eapply declareLocalVarPartialViewUndefCsC; eauto.
case_eq (copyInPartialView_f e sig); intros; rewrite H2 in *; try einversion.
induction params; try einversion.
clear IHparams.
induction a0; try einversion; induction e1; try einversion.
case_eq (copyIn_f e' sig params); intros; rewrite H3 in *; try einversion.
case_eq (match getIdsValue n e' with
       | Some (Value (ConstantValue v)) |
         Some (Value (VariableValue v)) => Some v
       | None => Some Undefined
       | _ => None
       end); intros; rewrite H4 in *; try einversion.
assert (envpartialView e0 e1) by eauto.
eapply declareLocalVarPartialViewUndefVsV; eauto.
case_eq (copyInPartialView_f e sig); intros; rewrite H2 in *; try einversion.
induction params; try einversion.
clear IHparams.
induction a0; try einversion; induction e1; try einversion.
case_eq (copyIn_f e' sig params); intros; rewrite H3 in *; try einversion.
case_eq (match getIdsValue n e' with
       | Some (Value (ConstantValue v)) |
         Some (Value (VariableValue v)) => Some v
       | None => Some Undefined
       | _ => None
       end); intros; rewrite H4 in *; try einversion.
assert (envpartialView e0 e1) by eauto.
eapply declareLocalVarPartialViewUndefVsV; eauto.
Qed.

Fixpoint initLocalsPartialView_f f_env obj := match obj with
| [] => Some f_env
| ((LocalDecVar (ConstantVar id typ e))::objT) => 
    (
          match evalExpression_f f_env e with
          | Some Undefined => 
            (
              match declareLocalVar id (ConstantValue Undefined) f_env with
              | Some env' => initLocalsPartialView_f env' objT
              | None => None
              end
            )
          | Some v =>
            (
              match declareLocalVar id (ConstantValue v) f_env with
              | Some env' => initLocalsPartialView_f env' objT
              | None => None
              end
            )
          | None => 
            (
              match declareLocalVar id (ConstantValue Undefined) f_env with
              | Some env' => initLocalsPartialView_f env' objT
              | None => None
              end
            )
          end
    )
| (LocalDecVar ((VariableVar id typ))::objT) => 
    (
      match declareLocalVar id (VariableValue (decValue typ)) f_env with
      | Some env' => initLocalsPartialView_f env' objT
      | None => None
      end
    )
| _::objT => initLocalsPartialView_f f_env objT
end.

Theorem initLocalsPartialView_f_correct : forall decls e e' r r',
  envpartialView e e' ->
  initLocalsPartialView_f e decls = Some r ->
  initLocals_f e' decls = Some r' -> 
  envpartialView r r'.
Proof.
induction decls; simpl.
intros.
inversion H0; subst.
inversion H1; subst; auto.
intros.
induction a.
induction o.
assert (D := envpartialViewEvalExpression e e' e0 H).
destruct D; rewrite H2 in *.
induction (evalExpression_f e' e0); [|inversion H1].
case_eq (declareLocalVar t (ConstantValue a) e'); intros; rewrite H3 in *; [|inversion H1].
case_eq (declareLocalVar t (ConstantValue a) e); intros; rewrite H4 in *.
assert (envpartialView e2 e1) by (eapply declareLocalVarPartialViewVVsV; eauto).
induction a; try rewrite H4 in *; try einversion; eauto.
induction a; try einversion; try rewrite H4 in *; try einversion.
case_eq (evalExpression_f e' e0); intros; rewrite H3 in *; try einversion.
case_eq (declareLocalVar t (ConstantValue Undefined) e); intros; rewrite H4 in *; try einversion.
case_eq (declareLocalVar t (ConstantValue v) e'); intros; rewrite H5 in *; try einversion.
assert (envpartialView e1 e2) by (eapply declareLocalVarPartialViewUndefCsC; eauto).
eauto.
case_eq (declareLocalVar t (VariableValue (decValue t0)) e); intros; rewrite H2 in *; try einversion.
case_eq (declareLocalVar t (VariableValue (decValue t0)) e'); intros; rewrite H3 in *; try einversion.
assert (envpartialView e0 e1) by (eapply declareLocalVarPartialViewVVsV; eauto).
eauto.
eauto.
Qed.

Definition contextBased_optimizer : Type := (Ast.instruction_s -> bool) -> (Ast.instruction_s -> Ast.instruction_s -> bool) -> env -> instruction_s -> instruction_s.

Definition contextBased_optimizer_is_correct (opt: contextBased_optimizer) := 
    forall ins_s env evn res, 
      evalInstruction_s (env, evn) ins_s res -> forall dup deci e, 
      envpartialView e env ->
      evalInstruction_s (env, evn) (opt dup deci e ins_s) res.
      
(* Definition optimize_Instruction_s_bis e i := fst (optimize_Instruction_s e i).

Theorem optimize_Instruction_s_bis_is_correct : contextBased_optimizer_is_correct optimize_Instruction_s_bis.
Proof.
unfold contextBased_optimizer_is_correct.
unfold optimize_Instruction_s_bis.
intros.
eapply optimize_Instruction_s_correct; eauto.
Qed. *)

Definition optimize_ed dup deci ctx ed (opt: contextBased_optimizer) := 
  {| f_name := ed.(f_name); f_args := ed.(f_args); f_local_decs := ed.(f_local_decs); f_instrs := 
      (match copyInPartialView_f ctx ed.(f_args) with
        | Some env' => 
          (
            match initLocalsPartialView_f env' ed.(f_local_decs) with
            | Some f_env => 
                opt (dup ed.(f_name)) (deci ed.(f_name)) f_env ed.(f_instrs)
            | None => 
                ed.(f_instrs)
            end
          )
        | None => ed.(f_instrs) end) |}.
        
Theorem optimize_ed_correct' : forall opt, 
      contextBased_optimizer_is_correct opt -> 
      forall dup deci de e (run_env : env) (inevnt : list event)
       (res : interpretationResult) (params : association_d_arguments),
     envpartialView (reasonably_smallest_environment de) run_env ->
     execFunction (run_env, inevnt) e params res ->
     execFunction (run_env, inevnt)
       (optimize_ed dup deci (reasonably_smallest_environment de) e opt) params
       res.
Proof.
intros.
inversion H1; subst.
unfold optimize_ed.
case_eq (copyInPartialView_f (reasonably_smallest_environment de) (f_args e)); intros; econstructor; eauto.
case_eq (initLocalsPartialView_f e0 (f_local_decs e)); intros; simpl; try econstructor; eauto.
eapply H; eauto.
apply copyIn_f_correct in H4.
assert (exists trace, initLocals (f_env, inevnt) (f_local_decs e) (f_env', trace)) by eauto.
apply initLocals_f_correct in H7.
eapply initLocalsPartialView_f_correct in H3; eauto.
eapply copyInPartialView_f_correct; eauto.
unfold optimize_ed.
case_eq (copyInPartialView_f (reasonably_smallest_environment de) (f_args e)); intros; econstructor; eauto.
case_eq (initLocalsPartialView_f e0 (f_local_decs e)); intros; simpl; try econstructor; eauto.
eapply H; eauto.
apply copyIn_f_correct in H4.
assert (exists trace, initLocals (f_env, inevnt) (f_local_decs e) (f_env', trace)) by eauto.
apply initLocals_f_correct in H6.
eapply initLocalsPartialView_f_correct in H3; eauto.
eapply copyInPartialView_f_correct; eauto.
Qed.


Theorem optimize_ed_correct : forall dup deci env in_data ed params res opt,
  contextBased_optimizer_is_correct opt ->
  envpartialView env (fst in_data) ->
  execFunction in_data ed params res -> 
  execFunction in_data (optimize_ed dup deci env ed opt) params res.
Proof.
induction 3; intros.
unfold optimize_ed.
case_eq (copyInPartialView_f (env) (f_args f)); intros.
case_eq (initLocalsPartialView_f e (f_local_decs f)); intros.
econstructor; eauto.
simpl.
apply copyIn_f_correct in H1.
assert (exists trace, initLocals (f_env, b_evn) (f_local_decs f) (f_env', trace)) by eauto.
apply initLocals_f_correct in H7.
eapply copyInPartialView_f_correct in H5; eauto.
eapply initLocalsPartialView_f_correct in H6; eauto.
simpl in H0.
econstructor; eauto.
econstructor; eauto.

unfold optimize_ed.
case_eq (copyInPartialView_f (env) (f_args f)); intros.
case_eq (initLocalsPartialView_f e (f_local_decs f)); intros.
econstructor; eauto.
simpl.
apply copyIn_f_correct in H1.
assert (exists trace, initLocals (f_env, b_evn) (f_local_decs f) (f_env', trace)) by eauto.
apply initLocals_f_correct in H6.
eapply copyInPartialView_f_correct in H4; eauto.
eapply initLocalsPartialView_f_correct in H5; eauto.
simpl in H0.
econstructor; eauto.
econstructor; eauto.
Qed.

Definition encapsuler_for_declaration_2_body dup deci lowest_run_env (opt: contextBased_optimizer) d := match d with
| Fun_def ed => Fun_def (optimize_ed dup deci lowest_run_env ed opt)
| r => r
end.

Definition encapsuler_for_declaration_2_bodyList dup deci ctx (opt: contextBased_optimizer) lib := 
  List.map (encapsuler_for_declaration_2_body dup deci ctx opt) lib.

Theorem encapsuler_for_declaration_2_body_correct : forall opt,
  contextBased_optimizer_is_correct opt -> forall dup deci dec inenv inenv' outenv outenv' run_env,
  env_optimization_correct' run_env inenv inenv' ->
  evalDeclaration_2_bodyList inenv dec outenv ->
  evalDeclaration_2_bodyList inenv' (encapsuler_for_declaration_2_bodyList dup deci run_env opt dec) outenv' ->
  env_optimization_correct' run_env outenv outenv'.
Proof.
induction dec; simpl; intros; inversion H1; inversion H2; subst; auto.
eapply IHdec with (inenv:=env'); eauto.
inversion H6; subst; simpl in *; inversion H12; simpl in *; subst.
eapply env_optimization_correct_declareFunction; eauto.
assert (D := env_optimization_correct_declareFunction fid fparams run_env inenv inenv' env'1 env'2 H0 H3 H13).
eapply env_optimization_correct_defineFunction; eauto.
eapply env_optimization_correct_defineFunction with 
  (e:=inenv)
  (e':=inenv')
  (de:=env') 
  (de':=env'0) 
  (f := ed)
  (f' := optimize_ed dup deci run_env ed opt)
  ; eauto.
intros.
eapply optimize_ed_correct; eauto.
Qed.

Definition encapsuler_for_unite (opt: contextBased_optimizer) dup deci ctx unite := match unite with
| Compilation_Body ed => Compilation_Body (optimize_ed (dup []) (deci []) ctx ed opt)
| Compilation_PkgDecl id dec => Compilation_PkgDecl id dec
| Compilation_PkgBody id dec fdec => Compilation_PkgBody id dec (encapsuler_for_declaration_2_bodyList (dup [id]) (deci [id]) ctx opt fdec)
end.

Definition encapsuler_for_compilation (opt: contextBased_optimizer) dup deci (p : preprocessor) (c: compilation) := match c with
| (incls, unt) => 
    (incls, (
      match p incls (getName unt) with
      | (globalV, declF, exts) =>
        ( 
          match unt with
          | Compilation_Body ed => 
              (
                match defineFunction ed ({|
                          localVars := [];
                          globalVars := globalV;
                          funDefs := [];
                          localfunDecls := declF;
                          externals := exts
                        |}) with
                | Some ctx_basis => encapsuler_for_unite opt dup deci (reasonably_smallest_environment ctx_basis) unt
                | None => unt
                end
              )
           | Compilation_PkgDecl id dec =>
              (
                match evalDeclaration_de_baseList_f ({|
                            localVars := [];
                            globalVars := globalV;
                            funDefs := [];
                            localfunDecls := declF;
                            externals := exts
                          |}) dec with
                | Some ctx_basis => encapsuler_for_unite opt dup deci (reasonably_smallest_environment ctx_basis) unt
                | None => unt
                end
              )
           | Compilation_PkgBody id dec fdec =>
              (
                match evalDeclaration_1_bodyList_f ({|
                            localVars := [];
                            globalVars := globalV;
                            funDefs := [];
                            localfunDecls := declF;
                            externals := exts
                          |}) dec with
                | Some ctx_basis_1 => 
                    (
                      match evalDeclaration_2_bodyList_f ctx_basis_1 fdec with
                      | Some ctx_basis => encapsuler_for_unite opt dup deci (reasonably_smallest_environment ctx_basis) unt
                      | None => unt
                      end
                    )
                | None => unt
                end
              )
           end
        )
      end))
     
end.

Lemma p_getname : forall dup deci (p : preprocessor) a b opt ctx, p a (getName (encapsuler_for_unite ctx dup deci opt b)) = p a (getName b).
Proof.
induction b; eauto.
Qed.


Theorem encapsuler_for_compilation_correct : forall opt,
  contextBased_optimizer_is_correct opt -> forall dup deci p c,
  compilation_correct_optimization p c (encapsuler_for_compilation opt dup deci).
Proof.
unfold compilation_correct_optimization.
induction c; simpl.
intros.
rewrite H0 in *.
remember ({|
           localVars := [];
           globalVars := globalV;
           funDefs := [];
           localfunDecls := declF;
           externals := exts |}) as inenv.
induction b; simpl; intros.
case_eq (defineFunction e inenv); intros; rewrite H2 in *.
rewrite p_getname in H1.
rewrite H0 in H1.
inversion H1; subst.
split; simpl; auto; intros.
inversion H3; subst.
assert (f_name e =
    f_name (optimize_ed (dup []) (deci []) (reasonably_smallest_environment de) e opt)) by (simpl; eauto).
assert (D := env_optimization_correct_defineFunction e (optimize_ed (dup []) (deci []) (reasonably_smallest_environment de) e opt)
{|
   localVars := [];
   globalVars := globalV';
   funDefs := [];
   localfunDecls := declF';
   externals := exts'
 |} 
{|
   localVars := [];
   globalVars := globalV';
   funDefs := [];
   localfunDecls := declF';
   externals := exts'
 |} (reasonably_smallest_environment de) de de' 
 (env_optimization_correct_refl (reasonably_smallest_environment de) {|
   localVars := [];
   globalVars := globalV';
   funDefs := [];
   localfunDecls := declF';
   externals := exts'
 |}) H5 (optimize_ed_correct' opt H (dup []) (deci []) de e) H2 H4
).
rewrite env_optimization_correct_equiv.
auto.
split; eauto; intros; try einversion.
case_eq (evalDeclaration_de_baseList_f inenv l); intros; rewrite H2 in *; eauto.
rewrite p_getname in H1.
rewrite H0 in H1.
inversion H1; subst.
split; eauto; intros.
split.
apply include_except_prag_refl.
intros.
fparam_determ evalDeclaration_de_baseList_f_correct H3 H4.
apply env_optimization_correct_refl.
simpl in H1.
simpl in H0.
rewrite H0 in H1; inversion H1; subst.
split; eauto; intros.
split.
apply include_except_prag_refl.
intros.
fparam_determ evalDeclaration_de_baseList_f_correct H3 H4.
apply env_optimization_correct_refl.
case_eq (evalDeclaration_1_bodyList_f inenv l); intros; rewrite H2 in *; eauto.
case_eq (evalDeclaration_2_bodyList_f e l0); intros; rewrite H3 in *; eauto.
rewrite p_getname in H1.
rewrite H0 in H1.
inversion H1; subst.
split; eauto; intros.
split.
apply include_except_prag_refl.
intros.
rewrite evalDeclaration_1_bodyList_f_correct in *.
f_determ H2 H4.
f_determ H2 H6.
rewrite evalDeclaration_2_bodyList_f_correct in *.
f_determ H3 H5.
rewrite <- evalDeclaration_2_bodyList_f_correct in *.
assert (D := encapsuler_for_declaration_2_body_correct opt H (dup [t]) (deci [t]) l0 env_wd' env_wd' env_wf env_wf' 
(reasonably_smallest_environment env_wf)
(env_optimization_correct_refl (reasonably_smallest_environment env_wf) env_wd')
H3 H7
).
auto.
rewrite H0 in H1.
inversion H1; subst.
split; eauto; intros.
split.
apply include_except_prag_refl.
intros.
rewrite evalDeclaration_1_bodyList_f_correct in *.
f_determ H2 H4.
f_determ H2 H6.
rewrite evalDeclaration_2_bodyList_f_correct in *.
rewrite H5 in H3; inversion H3.
rewrite H0 in H1.
inversion H1; subst.
split; eauto; intros.
split; intros.
apply include_except_prag_refl.
rewrite evalDeclaration_1_bodyList_f_correct in *.
f_determ H2 H3.
Qed.





