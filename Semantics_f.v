Require Import Map MapTypes Values Environment Event Semantics.
Require Import Ast.
Require Import List String ZArith Lia.
Import ListNotations.
    
Fixpoint evalExpression_f env e := match e with
| (ELit (Int_lit v)) => Some (Int v)
| (ELit (Bool_lit v)) => Some (Bool v)
| (EVar id) => (
                match getIdsValue id env with
                | Some (Value (ConstantValue v)) => Some v
                | Some (Value (VariableValue v)) => Some v
                | None => Some Undefined
                | _ => None
                end
               )
| (Ebinary op e0 e1) => 
               (
                match evalExpression_f env e0, evalExpression_f env e1 with
                | Some v0, Some v1 => Math.binary_operation op v0 v1
                | _, _ => None
                end
               )
| (Eunary op e0) => 
               (
                match evalExpression_f env e0 with
                | Some v0 => Math.unary_operation op v0
                | None => None
                end
               )
end.

Lemma evalExpression_f_correct : forall e env v, evalExpression env e v <-> evalExpression_f env e = Some v.
Proof.
induction e; simpl; intros; split; intros.
inversion H.
rewrite H2; auto.
rewrite H2; auto.
rewrite H2; auto.
case_eq (getIdsValue n env).
intros.
rewrite H0 in H.
induction v0.
induction v0.
inversion H; rewrite H2 in *; apply evalEVarConst; auto.
inversion H; rewrite H2 in *; apply evalEVarVar; auto.
inversion H.
inversion H.
intros.
rewrite H0 in H.
inversion H.
apply evalEVarNone; auto.
induction l; inversion H; auto.
induction l; inversion H; constructor.
inversion H.
apply IHe1 in H4.
apply IHe2 in H6.
rewrite H4, H6; auto.
case_eq (evalExpression_f env e1).
case_eq (evalExpression_f env e2).
intros.
rewrite H0, H1 in H.
apply IHe2 in H0.
apply IHe1 in H1.
eapply evalEbinary; eauto.
intros; rewrite H0, H1 in H; inversion H.
intros; try rewrite H0 in H; inversion H.
inversion H.
apply IHe in H3.
rewrite H3; auto.
case_eq (evalExpression_f env e); intros; rewrite H0 in H.
apply IHe in H0.
eapply evalEunary; eauto.
inversion H.
Qed.

(* Fixpoint make_call_param_list_f env params := match params with
| [] => Some []
| (StringLit exp)::t => 
                  (match make_call_param_list_f env t with
                    | Some t' => Some ((STR exp)::t')
                    | _ => None
                    end
                  )
| (Exp exp)::t => (match evalExpression_f env exp with
                    | Some exp' =>
                      (match make_call_param_list_f env t with
                        | Some t' => Some (exp'::t')
                        | _ => None
                        end
                      )
                    | _ => None
                   end
                  )
end. *)

(* Lemma make_call_param_list_f_correct : forall params env r, make_call_param_list env params r <-> make_call_param_list_f env params = Some r.
Proof.
induction params; simpl; intros; split; intros.
inversion H; auto.
inversion H; constructor.
inversion H; subst. apply evalExpression_f_correct in H3. rewrite H3. apply IHparams in H5. rewrite H5. auto. apply IHparams in H4. rewrite H4. auto.
induction a. case_eq (evalExpression_f env e); intros; rewrite H0 in H; [|inversion H]. case_eq (make_call_param_list_f env params); intros; rewrite H1 in H; [|inversion H]. apply evalExpression_f_correct in H0. apply IHparams in H1. inversion H. econstructor; eauto.
case_eq (make_call_param_list_f env params); intros; rewrite H0 in H; inversion H; subst; apply IHparams in H0; econstructor; eauto.
Qed.

Definition make_event_f env fname fparams fsig := match make_call_param_list_f env fparams with
| Some fpeval => Some 
    (mkEvent fname fparams fpeval fsig (mkenv [] env.(globalVars) env.(funDefs) env.(localfunDecls) env.(externals)))
| _ => None
end.

Lemma make_event_f_correct : forall env fname fparams fsig r, make_event env fname fparams fsig r <-> make_event_f env fname fparams fsig = Some r.
Proof.
split; intros.
+ inversion H.
  subst.
  apply make_call_param_list_f_correct in H0.
  unfold make_event_f.
  rewrite H0.
  auto.
+ unfold make_event_f in H.
  case_eq (make_call_param_list_f env fparams); intros; rewrite H0 in H; inversion H; subst.
  apply make_call_param_list_f_correct in H0.
  constructor; auto.
Qed. *)

Definition evalSec_opel_expression_booleenne_f env e := match e with
| SOEBNot ids => evalExpression_f env (Eunary BoolNot (EVar ids))
| SOEBBinaire op ids p => 
    evalExpression_f env 
      (Ebinary 
          (convSec_opel_binary_bool_op op) 
          (convSec_opel_parametre_nom_secu_to_exp ids)
          (convSec_opel_parametre_to_exp p)
      )
end.

Lemma evalSec_opel_expression_booleenne_f_correct : forall e env v, evalSec_opel_expression_booleenne env e v <-> evalSec_opel_expression_booleenne_f env e = Some v .
Proof.
unfold evalSec_opel_expression_booleenne_f.
unfold evalSec_opel_expression_booleenne.
induction e.
intros.
apply evalExpression_f_correct.
intros.
apply evalExpression_f_correct.
Qed.

Definition evalSec_opel_w_f env ex := match ex with
| SOWBool b => evalSec_opel_expression_booleenne_f env b
| SOWNSecu ids => 
    (
      match evalExpression_f env (EVar ids) with
      | Some v => Some (Math.to_bool v)
      | _ => None
      end
    )
end.

Lemma evalSec_opel_w_f_correct : forall e env v, evalSec_opel_w env e v <-> evalSec_opel_w_f env e = Some v .
Proof.
split; unfold evalSec_opel_w_f; intros.
+ induction H.
  apply evalSec_opel_expression_booleenne_f_correct; auto.
  apply evalExpression_f_correct in H.
  rewrite H.
  auto.
+ induction e.
  apply evalSec_opel_expression_booleenne_f_correct in H.
  econstructor; eauto.
  case_eq (evalExpression_f env (EVar l)); intros; rewrite H0 in H; inversion H; subst.
  apply evalExpression_f_correct in H0.
  econstructor; eauto.
Qed.

Fixpoint copyIn_f env (sig : list (ident * mode * name)) (app_params : list expression_or_string_lit) := match sig, app_params with
| [], [] => Some ({| localVars := [];
        globalVars := env.(globalVars);
        funDefs := env.(funDefs);
        localfunDecls := env.(localfunDecls);
        externals := env.(externals) |})
| ((idSig, Ast.In, IdTyp)::tSig), ((StringLit strParam)::tParams) => 
    (
      match copyIn_f env tSig tParams with
      | Some env' => (declareLocalVar idSig (ConstantValue (STR strParam)) env') 
      | None => None
      end
    )
| ((idSig, Ast.In, IdTyp)::tSig), ((Exp eParam)::tParams) => 
    (
      match copyIn_f env tSig tParams with
      | Some env' => 
        (
          match evalExpression_f env eParam with
          | Some v => (declareLocalVar idSig (ConstantValue v) env')
          | None => None
          end
        )
      | None => None
      end
    )
| ((idSig, Ast.InOut, IdTyp)::tSig), ((Exp (EVar eParam))::tParams) => 
    (
      match copyIn_f env tSig tParams with
      | Some env' => 
        (
          match evalExpression_f env (EVar eParam) with
          | Some v => (declareLocalVar idSig (VariableValue v) env')
          | None => None
          end
        )
      | None => None
      end
    )
| ((idSig, Ast.Out, IdTyp)::tSig), ((Exp (EVar eParam))::tParams) => 
    (
      match copyIn_f env tSig tParams with
      | Some env' => 
        (
          match evalExpression_f env (EVar eParam) with
          | Some v => (declareLocalVar idSig (VariableValue Undefined) env')
          | None => None
          end
        )
      | None => None
      end
    )
| _, _ => None
end.

Lemma copyIn_f_correct : forall sig params env env', copyIn env sig params env' <-> copyIn_f env sig params = Some env'.
Proof.
induction sig.
induction params.
split; intros.
inversion H.
simpl; auto.
simpl in H.
inversion H.
constructor.
split; intros.
inversion H.
simpl in H; inversion H.
induction params.
split; intros.
inversion H.
simpl in H.
induction a, a, m; inversion H.
split; intros.
inversion H.
simpl.
apply IHsig in H6.
rewrite H6; auto.
apply evalExpression_f_correct in H5.
simpl; apply IHsig in H7; rewrite H5; rewrite H7; auto.
eapply evalExpression_f_correct in H5. 
apply IHsig in H7.
simpl in *.
rewrite H5.
rewrite H7; auto.
apply IHsig in H5.
apply evalExpression_f_correct in H7.
simpl in *.
rewrite H7, H5; auto.
simpl in *.
induction a.
induction a.
induction b0.
induction a0.
case_eq (copyIn_f env sig params); intros; rewrite H0 in H; [| inversion H].
case_eq (evalExpression_f env e); intros; rewrite H1 in H; [| inversion H].
inversion H.
apply IHsig in H0.
apply evalExpression_f_correct in H1.
eapply CopyInConsExpIn; eauto.
case_eq (copyIn_f env sig params); intros; rewrite H0 in H; [| inversion H].
apply IHsig in H0.
eapply CopyInConsSTR; eauto.
induction a0; [|inversion H].
induction e; inversion H; clear H1.
case_eq (copyIn_f env sig params); intros; rewrite H0 in H; [| inversion H].
case_eq (evalExpression_f env (EVar n)); intros.
assert (H1':=H1).
apply evalExpression_f_correct in H1'.
simpl in H1.
rewrite H1 in H.
apply IHsig in H0.
eapply CopyInConsExpInOut; eauto.
simpl in H1.
rewrite H1 in H.
inversion H.
induction a0; [| inversion H].
induction e; [| inversion H| inversion H| inversion H].
case_eq (copyIn_f env sig params); intros; rewrite H0 in H; [| inversion H].
case_eq (evalExpression_f env (EVar n)); intros; assert (H1':=H1); simpl in H1; rewrite H1 in H.
apply IHsig in H0.
apply evalExpression_f_correct in H1'.
econstructor; eauto.
inversion H.
Qed.

Fixpoint copyOut_f f_env (sig : list (ident * mode * name)) (app_params : list expression_or_string_lit) c_env := match sig, app_params with
| [], [] => Some ({| localVars := c_env.(localVars);
        globalVars := projectMapGlobal f_env.(globalVars) c_env.(globalVars);
        funDefs := c_env.(funDefs);
        localfunDecls := c_env.(localfunDecls);
        externals := projectMapLib f_env.(externals) c_env.(externals) |})
| ((idSig, Ast.In, IdTyp)::tSig), (eParam::tParams) => copyOut_f f_env tSig tParams c_env
| ((idSig, Ast.InOut, IdTyp)::tSig), ((Exp (EVar eParam))::tParams) => 
    (
      match copyOut_f f_env tSig tParams c_env with
      | Some env' => 
        (
          match evalExpression_f f_env (EVar [idSig]) with
          | Some v => setIdsValue eParam v env'
          | None => None
          end
        )
      | None => None
      end
    )
| ((idSig, Ast.Out, IdTyp)::tSig), ((Exp (EVar eParam))::tParams) => 
    (
      match copyOut_f f_env tSig tParams c_env with
      | Some env' => 
        (
          match evalExpression_f f_env (EVar [idSig]) with
          | Some v => setIdsValue eParam v env'
          | None => None
          end
        )
      | None => None
      end
    )
| _, _ => None
end.

Lemma copyOut_f_correct : forall sig params env env' env'', copyOut env sig params env' env'' <-> copyOut_f env sig params env' = Some env''. 
Proof.
induction sig.
induction params.
split; intros.
inversion H.
simpl; auto.
simpl in H.
inversion H.
constructor.
split; intros; [inversion H | simpl in H; inversion H].
induction params.
split; intros; [inversion H | simpl in H].
induction a.
induction a.
induction b0; inversion H.
split; intros.
inversion H.
cbn.
apply IHsig in H7; auto.
apply evalExpression_f_correct in H8.
apply IHsig in H5.
cbn.
rewrite H5.
simpl in H8; rewrite H8; auto.
apply IHsig in H5.
apply evalExpression_f_correct in H8.
cbn.
rewrite H5.
simpl in H8; rewrite H8; auto.
induction a.
induction a.
simpl in H.
induction b0.
apply IHsig in H.
constructor; auto.
induction a0; [| inversion H].
induction e; [| inversion H| inversion H| inversion H].
case_eq (copyOut_f env sig params env'); intros; rewrite H0 in H; [| inversion H].
case_eq (evalExpression_f env (EVar [a])); intros; assert (H1':=H1); [simpl in H1; rewrite H1 in H; clear H1|simpl in H1; rewrite H1 in H; inversion H ].
apply IHsig in H0.
apply evalExpression_f_correct in H1'.
econstructor; eauto.
induction a0; [| inversion H].
induction e; [| inversion H| inversion H| inversion H].
case_eq (copyOut_f env sig params env'); intros; rewrite H0 in H; [| inversion H].
case_eq (evalExpression_f env (EVar [a])); intros; assert (H1':=H1); [simpl in H1; rewrite H1 in H; clear H1|simpl in H1; rewrite H1 in H; inversion H ].
apply IHsig in H0.
apply evalExpression_f_correct in H1'.
econstructor; eauto.
Qed.

Fixpoint initLocals_f f_env obj := match obj with
| [] => Some f_env
| ((LocalDecVar (ConstantVar id typ e))::objT) => 
    (
      match evalExpression_f f_env e with
      | Some v => 
        (
          match declareLocalVar id (ConstantValue v) f_env with
          | Some env' => initLocals_f env' objT
          | None => None
          end
        )
      | None => None
      end
    )
| ((LocalDecVar (VariableVar id typ))::objT) => 
    (
      match declareLocalVar id (VariableValue (decValue typ)) f_env with
      | Some env' => initLocals_f env' objT
      | None => None
      end
    )
| _::objT => initLocals_f f_env objT
end.

Lemma initLocals_f_correct : forall sig env trace env', (exists trace', initLocals (env, trace) sig (env', trace')) <-> initLocals_f env sig = Some env'.
Proof.
induction sig.
split; intros; [destruct H|].
inversion H; subst.
simpl; auto.
simpl in H; inversion H; subst.
exists trace.
apply initLocalsNil; eauto.
split; intros; [destruct H|].
inversion H; subst.
simpl.
apply evalExpression_f_correct in H4.
rewrite H4.
rewrite H6.
eapply IHsig; eauto.
simpl.
rewrite H5.
eapply IHsig; eauto.
simpl.
eapply IHsig; eauto.
simpl.
eapply IHsig; eauto.
simpl.
eapply IHsig; eauto.

induction a; simpl in *.
induction o; simpl in *.
case_eq (evalExpression_f env e); intros; rewrite H0 in H; [| inversion H].
case_eq (declareLocalVar t (ConstantValue v) env); intros; rewrite H1 in H; [| inversion H].
apply evalExpression_f_correct in H0; auto.
eapply IHsig in H.
destruct H.
exists x.
econstructor; eauto.
case_eq (declareLocalVar t (VariableValue (decValue t0)) env); intros; rewrite H0 in H; [| inversion H].
eapply IHsig in H.
destruct H.
exists x.
econstructor; eauto.
induction p; eapply IHsig in H; destruct H; exists x; econstructor; eauto.
Qed.

Fixpoint copyOutExternalWithoutSig_f f_env (params: list expression_or_string_lit) env := match params with
| [] => Some {| localVars := env.(localVars);
        globalVars := projectMapGlobal f_env.(globalVars) env.(globalVars);
        funDefs := env.(funDefs);
        localfunDecls := env.(localfunDecls);
        externals := projectMapLib f_env.(externals) env.(externals)  |}
| ((Exp (EVar eParam))::tParams) =>
    (
      match copyOutExternalWithoutSig_f f_env tParams env with
      | Some r_env => 
        (
          match evalExpression_f f_env (EVar eParam) with
          | Some v => setIdsValue eParam v r_env
          | None => None
          end
        )
      | None => None
      end
    )
| (_::tParams) => copyOutExternalWithoutSig_f f_env tParams env
end.

Lemma copyOutExternalWithoutSig_f_correct : forall params f_env env env', copyOutExternalWithoutSig f_env params env env' <-> copyOutExternalWithoutSig_f f_env params env = Some env'.
Proof.
induction params.
simpl.
split; intros; [ inversion H; auto| inversion H; constructor].
induction a.
induction e; simpl; intros.
split; intros.
inversion H.
apply IHparams in H2.
rewrite H2; auto.
apply evalExpression_f_correct in H4; simpl in H4; rewrite H4; auto.
case_eq (copyOutExternalWithoutSig_f f_env params env); intros; rewrite H0 in H; [| inversion H].
apply IHparams in H0.
case_eq (evalExpression_f f_env (EVar n)); intros; assert (H1':=H1); simpl in H1; rewrite H1 in H; [|inversion H].
eapply copyOutExternalWithoutSigEVar; eauto.
apply evalExpression_f_correct; auto.
split; intros; [inversion H; apply IHparams in H5; auto|apply IHparams in H; apply copyOutExternalWithoutSigELit; auto].
split; intros; [inversion H; apply IHparams in H7; auto|apply IHparams in H; apply copyOutExternalWithoutSigEbinary; auto].
split; intros; [inversion H; apply IHparams in H6; auto|apply IHparams in H; apply copyOutExternalWithoutSigEunary; auto].
split; intros; [inversion H; apply IHparams in H5; auto|apply IHparams in H; apply copyOutExternalWithoutSigSTR; auto].
Qed.

Definition evalDeclaration_de_base_f env d := match d with
| (Declaration_d_objet (ConstantVar id typ e)) => 
    (
      match evalExpression_f env e with
      | Some v => declareGlobalVar id (ConstantValue v) env
      | None => None
      end
    )
| (Declaration_d_objet (VariableVar id typ)) => declareGlobalVar id (VariableValue (decValue typ)) env
| (Declaration_de_sous_programme fid fparams) =>
    (declareFunction fid fparams env)
| (Definition_de_type_Article id def) => Some env
| (Declaration_de_sous_type id def) => Some env
| (Declaration_de_surnom fid fparams rname) =>
    (
      match declareFunction fid fparams env with
      | Some env' => defineFunction {| f_name := fid; f_args := fparams; f_local_decs := []; f_instrs := ISeqEnd((ICallDefinedFunction rname (Some (from_def_to_call_params fparams)))) |} env'
      | None => None
      end    
    ) 
| Use_Clause => Some env
| (Pragma_Declaration p) => Some env
end.

Lemma evalDeclaration_de_base_f_correct : forall params env env', evalDeclaration_de_base env params env' <-> evalDeclaration_de_base_f env params = Some env'.
Proof.
induction params; simpl.
induction o; simpl.
split; intros.
inversion H.
apply evalExpression_f_correct in H5.
rewrite H5; f_equal; auto.
case_eq (evalExpression_f env e); intros; rewrite H0 in H; inversion H; eapply EvalDeclaration_d_objetConst; eauto.
apply evalExpression_f_correct; auto.
split; intros.
inversion H.
auto.
econstructor; eauto.
split; intros.
inversion H.
f_equal; auto.
inversion H; constructor; auto.
split; intros.
inversion H; auto.
inversion H; constructor.
split; intros;inversion H; auto; constructor.
intros.
split; intros.
inversion H; subst.
rewrite H5; auto.
case_eq (declareFunction t l env); intros.
rewrite H0 in H. 
econstructor; eauto.
rewrite H0 in H; inversion H.
split; intros; inversion H; auto; constructor.
split; intros; inversion H; auto; constructor.
Qed.

Fixpoint evalDeclaration_de_baseList_f env l := match l with
| [] => Some env
| h::t =>
    (
      match evalDeclaration_de_base_f env h with
      | Some env' => evalDeclaration_de_baseList_f env' t
      | None => None
      end
    )
end.

Lemma evalDeclaration_de_baseList_f_correct : forall params env env', evalDeclaration_de_baseList env params env' <-> evalDeclaration_de_baseList_f env params = Some env'.
Proof.
induction params; simpl.
split; intros; inversion H; constructor.
split; intros.
inversion H.
apply evalDeclaration_de_base_f_correct in H3; auto.
rewrite H3.
apply IHparams; auto.
case_eq (evalDeclaration_de_base_f env a); intros; rewrite H0 in H; [| inversion H].
apply IHparams in H.
apply evalDeclaration_de_base_f_correct in H0.
econstructor; eauto.
Qed.

Definition evalDeclaration_1_body_f env d := match d with
| (Body_Declaration_d_objet (ConstantVar id typ e)) => 
    (
      match evalExpression_f env e with
      | Some v => (declareGlobalVar id (ConstantValue v) env)
      | None => None
      end
    )
| (Body_Declaration_d_objet (VariableVar id typ)) => declareGlobalVar id (VariableValue (decValue typ)) env
| (Body_Definition_de_type_Article id def) => Some env
| (Body_Declaration_de_sous_type id def) => Some env
| Body_Use_Clause => Some env
| (Body_Pragma_Declaration p) => Some env
end.

Lemma evalDeclaration_1_body_f_correct : forall params env env', evalDeclaration_1_body env params env' <-> evalDeclaration_1_body_f env params = Some env'.
Proof.
induction params; simpl.
induction o; simpl.
split; intros.
inversion H.
apply evalExpression_f_correct in H5.
rewrite H5; f_equal; auto.
case_eq (evalExpression_f env e); intros; rewrite H0 in H; inversion H; eapply EvalBody_Declaration_d_objetConst; eauto.
apply evalExpression_f_correct; auto.
split; intros.
inversion H.
auto.
econstructor; eauto.
split; intros.
inversion H.
f_equal; auto.
inversion H; constructor; auto.
split; intros.
inversion H; auto.
inversion H; constructor.
split; intros;inversion H; auto; constructor.
split; intros.
inversion H; f_equal; auto.
inversion H; constructor; auto.
Qed.

Fixpoint evalDeclaration_1_bodyList_f env l := match l with
| [] => Some env
| h::t =>
    (
      match evalDeclaration_1_body_f env h  with
      | Some env' => evalDeclaration_1_bodyList_f env' t
      | None => None
      end
    )
end.

Lemma evalDeclaration_1_bodyList_f_correct : forall params env env', evalDeclaration_1_bodyList env params env' <-> evalDeclaration_1_bodyList_f env params = Some env'.
Proof.
induction params; simpl.
split; intros; inversion H; constructor.
split; intros.
inversion H.
apply evalDeclaration_1_body_f_correct in H3.
rewrite H3.
apply IHparams; auto.
case_eq (evalDeclaration_1_body_f env a); intros; rewrite H0 in H; [|inversion H].
apply evalDeclaration_1_body_f_correct in H0.
apply IHparams in H.
econstructor; eauto.
Qed.

Definition evalDeclaration_2_body_f env d := match d with
| (Fun_dec fid fparams) =>
    (declareFunction fid fparams env)
| (Fun_renames fid fparams rname) => 
    (
      match declareFunction fid fparams env with
      | Some env' => defineFunction {| f_name := fid; f_args := fparams; f_local_decs := []; f_instrs := ISeqEnd((ICallDefinedFunction rname (Some (from_def_to_call_params fparams)))) |} env'
      | None => None
      end    
    ) 
| (Fun_def ed) => (defineFunction ed env)
end.

Lemma evalDeclaration_2_body_f_correct : forall params env env', evalDeclaration_2_body env params env' <-> evalDeclaration_2_body_f env params = Some env'.
Proof.
induction params; simpl.
split; intros.
inversion H.
f_equal; auto.
inversion H; constructor; auto.
split; intros.
inversion H; f_equal; auto.
inversion H; constructor; auto.
split; intros.
inversion H; subst.
rewrite H5; auto.
case_eq (declareFunction t l env); intros; rewrite H0 in H; try solve [inversion H].
econstructor; eauto.
Qed.

Fixpoint evalDeclaration_2_bodyList_f env l := match l with
| [] => Some env
| h::t =>
    (
      match evalDeclaration_2_body_f env h  with
      | Some env' => evalDeclaration_2_bodyList_f env' t
      | None => None
      end
    )
end.

Lemma evalDeclaration_2_bodyList_f_correct : forall params env env', evalDeclaration_2_bodyList env params env' <-> evalDeclaration_2_bodyList_f env params = Some env'.
Proof.
induction params; simpl.
split; intros; inversion H; constructor.
split; intros.
inversion H.
apply IHparams in H5.
apply evalDeclaration_2_body_f_correct in H3.
rewrite H3, H5; auto.
case_eq (evalDeclaration_2_body_f env a); intros; rewrite H0 in H; [|inversion H].
apply evalDeclaration_2_body_f_correct in H0.
apply IHparams in H.
econstructor; eauto.
Qed. 





