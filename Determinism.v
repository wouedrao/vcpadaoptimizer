Require Import Map MapTypes Values Environment Event Semantics Semantics_f.
Require Import Ast.
Require Import List String ZArith Lia.
Import ListNotations.

Lemma copyOutExternalWithoutSig_determ : forall f_env env sig env',
  copyOutExternalWithoutSig f_env env sig env' -> forall env'',
  copyOutExternalWithoutSig f_env env sig env'' ->
  env' = env''.
Proof.
intros.
apply copyOutExternalWithoutSig_f_correct in H.
apply copyOutExternalWithoutSig_f_correct in H0.
rewrite H in H0.
inversion H0; auto.
Qed.

Ltac f_determ h0 h1 := rewrite h0 in h1; inversion h1; subst; auto.

Ltac fparam_determ f h0 h1 := apply f in h0; apply f in h1; rewrite h0 in h1; inversion h1; subst; auto.

Lemma evalSec_opel_expression_if_determ : forall evn env b v,
    evalSec_opel_expression_if (env, evn) b v -> forall v',
    evalSec_opel_expression_if (env, evn) b v' ->
    v = v'.
Proof.
induction 1; intros. 
+ inversion H0; subst.
  fparam_determ evalSec_opel_expression_booleenne_f_correct H H5.
+ inversion H; subst; auto.
+ inversion H; subst; auto.
+ inversion H0; subst.
  fparam_determ evalExpression_f_correct H H5.
Qed.

Lemma evalSec_opel_expression_d_affectation_determ : forall evn env e v,
  evalSec_opel_expression_d_affectation (env, evn) e v -> forall v',
  evalSec_opel_expression_d_affectation (env, evn) e v' -> v = v'.
Proof.
induction 1; intros. 
+ inversion H0; subst. fparam_determ evalExpression_f_correct H H5.
+ inversion H0; subst. fparam_determ evalExpression_f_correct H H7.
+ inversion H0; subst. fparam_determ evalExpression_f_correct H H6.
+ inversion H0; subst. fparam_determ evalSec_opel_expression_booleenne_f_correct H H5.
+ inversion H0; subst. fparam_determ evalExpression_f_correct H H5.
+ inversion H; subst; auto. 
+ inversion H0; subst. fparam_determ evalExpression_f_correct H H5.
+ inversion H0; subst. fparam_determ evalExpression_f_correct H H5.
+ inversion H2; subst. fparam_determ evalExpression_f_correct H H7. 
  fparam_determ evalExpression_f_correct H0 H9. 
  fparam_determ idxs_select_from_value_f_correct H1 H10.
+ inversion H3; subst. 
  fparam_determ evalExpression_f_correct H H9.
  fparam_determ evalExpression_f_correct H0 H11.
  fparam_determ evalExpression_f_correct H1 H12.
  fparam_determ idxs_select_from_value_f_correct H2 H13.
+ inversion H; subst; auto.
Qed.

Lemma initLocalDeterm : forall evn env decs res res',
  initLocals (env, evn) decs res ->
  initLocals (env, evn) decs res' -> res = res'.
Proof.
induction 1; intros. 
inversion H; subst; eauto.
inversion H2; subst.
fparam_determ evalExpression_f_correct H H10.
f_determ H0 H11.
inversion H1; subst.
f_determ H H8.
inversion H0; subst; eauto.
inversion H0; subst; eauto.
inversion H0; subst; eauto.
Qed.

Lemma sem_determ : (forall data f res,
  evalInstruction data f res -> forall res',
  evalInstruction data f res' ->
  res = res') /\ (forall data f res,
  evalInstruction_s data f res -> forall res',
  evalInstruction_s data f res' ->
  res = res') /\ (forall data f res,
  evalElif_instruction data f res -> forall res',
  evalElif_instruction data f res' ->
  res = res') /\ (forall data e f res,
  evalCase_instruction data e f res -> forall res',
  evalCase_instruction data e f res' ->
  res = res')
.
Proof.
  apply sem_inst_mutind; intros.
  inversion H; subst; auto.
  inversion H; subst; auto.
  inversion H; subst; auto.
  inversion H; subst; auto.
  inversion H; subst; auto.
  eapply evalSec_opel_expression_d_affectation_determ in e; eauto; subst.
  f_determ e0 H6.
  inversion H0; subst; eauto.
  eapply evalSec_opel_expression_if_determ in e; eauto. inversion e.
  inversion H0; subst; eauto.
  eapply evalSec_opel_expression_if_determ in e; eauto. inversion e.
  inversion H0; subst; eauto.
  inversion H1; subst; eauto.
  assert (D := H (OK env'0 evn'0) H8); inversion D; subst.
  apply H0; auto.
  assert (D := H (Error e2 env'0 evn'0) H8); inversion D; subst.
  fparam_determ evalSec_opel_w_f_correct e H7.
  inversion H0; subst; auto.
  assert (D := H (OK env'0 evn'0) H7); inversion D; subst.
  fparam_determ evalSec_opel_w_f_correct e0 H6.
  inversion H; subst; auto.
  fparam_determ evalSec_opel_w_f_correct e H4.
  fparam_determ evalSec_opel_w_f_correct e H5.
  inversion H0; subst; auto.
  
  f_determ e H4.
  fparam_determ copyIn_f_correct c H5.
  eapply initLocalDeterm in i; eauto; inversion i; subst.
  assert (D := H (OK af_env0 a_evn0) H7); inversion D; subst.
  fparam_determ copyOut_f_correct c0 H9.
  f_determ e H4.
  fparam_determ copyIn_f_correct c H5.
  eapply initLocalDeterm in i; eauto; inversion i; subst.
  assert (D := H (Error err af_env0 a_evn0) H8); inversion D; subst.
  f_determ e H4.
  f_determ e H4.
  f_determ e H4.
  f_determ e H4.
  inversion H0; subst; auto.
  f_determ e H4.
  fparam_determ copyIn_f_correct c H5.
  eapply initLocalDeterm in i; eauto; inversion i; subst.
  assert (D := H (OK af_env0 a_evn0) H7); inversion D; subst.
  f_determ e H4.
  fparam_determ copyIn_f_correct c H5.
  eapply initLocalDeterm in i; eauto; inversion i; subst.
  assert (D := H (Error err0 af_env0 a_evn0) H8); inversion D; subst; auto.
  f_determ e H4.
  f_determ e H4.
  f_determ e H4.
  f_determ e H4.
  inversion H0; subst; auto.
  f_determ e H5.
  fparam_determ copyIn_f_correct c H6.
  eapply initLocalDeterm in i; eauto; inversion i; subst.
  assert (D := H (OK af_env0 a_evn0) H9); inversion D; subst.
  fparam_determ copyOut_f_correct c0 H10.
  f_determ e H5.
  fparam_determ copyIn_f_correct c H6.
  eapply initLocalDeterm in i; eauto; inversion i; subst.
  assert (D := H (Error err af_env0 a_evn0) H9); inversion D; subst.
  f_determ e H5.
  f_determ e H6.
  f_determ e H5.
  f_determ e H6.
  inversion H0; subst; auto.
  f_determ e H5.
  fparam_determ copyIn_f_correct c H6.
  eapply initLocalDeterm in i; eauto; inversion i; subst.
  assert (D := H (OK af_env0 a_evn0) H9); inversion D; subst.
  f_determ e H5.
  fparam_determ copyIn_f_correct c H6.
  eapply initLocalDeterm in i; eauto; inversion i; subst.
  assert (D := H (Error err0 af_env0 a_evn0) H9); inversion D; subst; auto.
  f_determ e H5.
  f_determ e H6.
  f_determ e H5.
  f_determ e H6.
  inversion H; subst; auto.
  f_determ e H4.
  f_determ e H4.
  f_determ e H4.
  f_determ e0 H6.
  fparam_determ copyOut_f_correct c H7.
  f_determ e H5.
  f_determ e0 H6.
  f_determ e H4.
  f_determ e H5.
  inversion H; subst; auto.
  f_determ e H4.
  f_determ e H4.
  f_determ e H4.
  f_determ e0 H6.
  f_determ e H5.
  f_determ e0 H6.
  f_determ e H4.
  f_determ e H5.
  inversion H; subst; auto.
  f_determ e H3.
  f_determ e H3.
  f_determ e H3.
  f_determ e0 H4.
  fparam_determ copyOut_f_correct c H6.
  f_determ e H3.
  f_determ e0 H5.
  f_determ e H3.
  f_determ e H3.
  inversion H; subst; auto.
  f_determ e H3.
  f_determ e H3.
  f_determ e H3.
  f_determ e0 H4.
  f_determ e H3.
  f_determ e0 H5.
  f_determ e H3.
  f_determ e H3.
  inversion H; subst; auto.
  f_determ e H4.
  f_determ e H4.
  f_determ e H4.
  f_determ e H5.
  f_determ e0 H6.
  fparam_determ copyOutExternalWithoutSig_f_correct c H7.
  f_determ e0 H6.
  inversion H; subst; auto.
  f_determ e H4.
  f_determ e H4.
  f_determ e H4.
  f_determ e H5.
  f_determ e0 H6.
  f_determ e0 H6.
  inversion H; subst.
  f_determ e H3.
  f_determ e H3.
  f_determ e H3.
  f_determ e H3.
  f_determ e0 H4.
  fparam_determ copyOutExternalWithoutSig_f_correct c H6.
  f_determ e0 H5.
  inversion H; subst.
  f_determ e H3.
  f_determ e H3.
  f_determ e H3.
  f_determ e H3.
  f_determ e0 H4.
  f_determ e0 H5.
  inversion H; subst; auto.
  inversion H; subst; auto.
  inversion H; subst; auto.
  inversion H; subst; auto.
  inversion H; subst; auto.
  inversion H; subst.
  f_determ e H7.
  inversion H; subst.
  f_determ e H11.
  inversion H; subst; auto.
  inversion H; subst; auto.
  inversion H; subst; auto.
  fparam_determ evalExpression_f_correct e0 H4.
  fparam_determ evalExpression_f_correct e1 H6.
  f_determ e2 H7.
  inversion H; subst; auto.
  inversion H; subst; auto.
  inversion H; subst; auto.
  inversion H; subst; auto.
  fparam_determ evalExpression_f_correct e1 H8.
  f_determ e2 H9.
  f_determ e3 H10.
  inversion H; subst; auto.
  f_determ e0 H10.
  f_determ e1 H11.
  f_determ e2 H12.
  f_determ e0 H10.
  inversion H; subst; auto.
  f_determ e0 H10.
  f_determ e0 H10.
  f_determ e1 H11.
  inversion H; subst; auto.
  f_determ e0 H9.
  f_determ e1 H10.
  f_determ e2 H11.
  f_determ e0 H9.
  inversion H; subst; auto.
  f_determ e0 H9.
  f_determ e0 H9.
  f_determ e1 H10.
  inversion H; subst.
  fparam_determ evalExpression_f_correct e H5.
  fparam_determ evalExpression_f_correct e0 H6.
  fparam_determ evalExpression_f_correct e1 H8.
  fparam_determ idxs_set_from_value_f_correct i0 H9.
  f_determ e2 H10.
  inversion H; subst.
  fparam_determ evalExpression_f_correct e H6. 
  fparam_determ evalExpression_f_correct e0 H7.
  fparam_determ evalExpression_f_correct e1 H9.
  fparam_determ evalExpression_f_correct e2 H10.
  fparam_determ idxs_set_from_value_f_correct i0 H11.
  f_determ e3 H12.
  inversion H; subst.
  f_determ e0 H8.
  inversion H; subst.
  f_determ e0 H9.
  inversion H; subst.
  fparam_determ evalExpression_f_correct e H8.
  f_determ e0 H9.
  inversion H; subst.
  f_determ e0 H9.
  

  inversion H0; subst; auto.
  inversion H1; subst; auto.
  assert (D := H (OK env'0 evn'0) H7); inversion D; subst.
  auto.
  assert (D := H (Error e1 env'0 evn'0) H7); inversion D; subst.
  inversion H0; subst; auto.
  assert (D := H (OK env'0 evn'0) H6); inversion D; subst.

  inversion H0; subst; auto.
  inversion H0; subst; auto.
  assert (D := evalSec_opel_expression_if_determ evn env cond (Bool true) e (Bool false) H7); inversion D.
  inversion H0; subst; auto.
  assert (D := evalSec_opel_expression_if_determ evn env cond (Bool false) e (Bool true) H7); inversion D.

  inversion H0; subst; auto.
  assert (D := H (OK env'0 evn'0) H6); inversion D; subst; auto.
  assert (D := H (Error e0 env'0 evn'0) H6); inversion D; subst.
  inversion H0; subst; auto.
  assert (D := H (OK env'0 evn'0) H6); inversion D; subst.
  inversion H0; subst; auto.
  fparam_determ evalExpression_f_correct e H8.
  inversion H0; subst; auto.
  fparam_determ evalExpression_f_correct e H8.
Qed.

(* 


Fixpoint execFunction_determ 
    data f params res (H : execFunction data f params res) res' (R : execFunction data f params res') {struct H} : res = res'
with evalFunCall_determ 
    data f res (H : evalFunCall data f res) res' (R : evalFunCall data f res') {struct H} : res = res'
with evalInstruction_determ 
    data f res (H : evalInstruction data f res) res' (R : evalInstruction data f res') {struct H} : res = res'
with evalInstruction_s_determ 
    data f res (H : evalInstruction_s data f res) res' (R : evalInstruction_s data f res') {struct H} : res = res'
with evalElif_instruction_determ 
    data f res (H : evalElif_instruction data f res) res' (R : evalElif_instruction data f res') {struct H} : res = res'
with evalCase_instruction_determ 
    data e f res (H : evalCase_instruction e data f res) res' (R : evalCase_instruction e data f res') {struct H} : res = res'
.
destruct H; inversion R; subst.
  fparam_determ copyIn_f_correct H H5.
  fparam_determ initLocals_f_correct H0 H6.
  assert (D := evalInstruction_s_determ (f_env'0, b_evn) (f_instrs f) (OK af_env a_evn) H1 (OK af_env0 a_evn0) H7); inversion D; subst. 
  fparam_determ copyOut_f_correct H2 H11.
  
  fparam_determ copyIn_f_correct H H5.
  fparam_determ initLocals_f_correct H0 H6.
  clear -H1 H10 evalInstruction_s_determ.
  assert (D := evalInstruction_s_determ (f_env'0, b_evn) (f_instrs f) (OK af_env a_evn) H1 (Error err af_env0 a_evn0) H10); inversion D; subst. 
  
  fparam_determ copyIn_f_correct H H4.
  fparam_determ initLocals_f_correct H0 H5.
  clear -H1 H6 evalInstruction_s_determ.
  assert (D := evalInstruction_s_determ (f_env'0, b_evn) (f_instrs f) (Error err af_env a_evn) H1 (OK af_env0 a_evn0) H6). 
  inversion D.
  
  fparam_determ copyIn_f_correct H H4.
  fparam_determ initLocals_f_correct H0 H5.
  clear -H1 H9 evalInstruction_s_determ.
  assert (D := evalInstruction_s_determ (f_env'0, b_evn) (f_instrs f) (Error err af_env a_evn) H1 (Error err0 af_env0 a_evn0) H9). 
  exact D.
  
  
  destruct H; inversion R; subst; eauto.
  rewrite H in H4; inversion H4; subst. 
  apply (execFunction_determ (b_env, b_evn) f0 [] res H0 res').
  exact H6.
  rewrite H in H4; inversion H4. 
  rewrite H in H4; inversion H4.
  rewrite H in H4; inversion H4.
  rewrite H in H4; inversion H4.
  rewrite H in H6; inversion H6; subst.
  apply (execFunction_determ (b_env, b_evn) f0 f_params res H0 res'). 
  exact H7.
  rewrite H in H5; inversion H5.
  rewrite H in H6; inversion H6.
  rewrite H in H5; inversion H5.
  rewrite H in H6; inversion H6.
  rewrite H in H7; inversion H7.
  f_determ H H6.
  f_determ H0 H8.
  fparam_determ copyOut_f_correct H1 H9.
  f_determ H H7.
  f_determ H0 H8.
  f_determ H H6.
  f_determ H H7.
  f_determ H H6.
  f_determ H H5.
  f_determ H0 H7.
  f_determ H H6.
  f_determ H0 H7.
  f_determ H H5.
  rewrite H in H6; inversion H6.
  f_determ H H5.
  f_determ H H5.
  f_determ H0 H6.
  fparam_determ copyOut_f_correct H1 H8.
  f_determ H H5.
  f_determ H0 H7.
  f_determ H H5.
  f_determ H H5.
  f_determ H H4.
  f_determ H H4.
  f_determ H0 H5.
  f_determ H H4.
  f_determ H0 H6.
  f_determ H H4.
  f_determ H H4.
  rewrite H in H7; inversion H7.
  f_determ H H6.
  f_determ H H7.
  f_determ H0 H8.
  fparam_determ copyOutExternalWithoutSig_f_correct H1 H9.
  f_determ H0 H8.
  f_determ H H6.
  f_determ H H5.
  f_determ H H6.
  f_determ H0 H7.
  f_determ H0 H7.
  f_determ H H5.
  f_determ H H5.
  f_determ H H5.
  f_determ H0 H6.
  fparam_determ copyOutExternalWithoutSig_f_correct H1 H8.
  f_determ H0 H7.
  f_determ H H4.
  f_determ H H4.
  f_determ H H4.
  f_determ H0 H5.
  f_determ H0 H6.
  f_determ H H7.
  f_determ H H11.
  fparam_determ evalExpression_f_correct H H6.
  fparam_determ evalExpression_f_correct H0 H8.
  f_determ H1 H9.
  fparam_determ evalExpression_f_correct H0 H11.
  f_determ H1 H12.
  f_determ H2 H13.
  f_determ H H12.
  f_determ H0 H13.
  f_determ H1 H14.
  f_determ H H12.
  f_determ H H11.
  f_determ H H11.
  f_determ H0 H12.
  f_determ H H11.
  f_determ H0 H12.
  f_determ H1 H13.
  f_determ H H11.
  f_determ H H10.
  f_determ H H10.
  f_determ H0 H11.
  fparam_determ evalExpression_f_correct H H9.
  fparam_determ evalExpression_f_correct H0 H10.
  fparam_determ evalExpression_f_correct H1 H12.
  fparam_determ idxs_set_from_value_f_correct H2 H13.
  f_determ H3 H14.
  fparam_determ evalExpression_f_correct H H11. 
  fparam_determ evalExpression_f_correct H0 H12.
  fparam_determ evalExpression_f_correct H1 H14.
  fparam_determ evalExpression_f_correct H2 H15.
  fparam_determ idxs_set_from_value_f_correct H3 H16.
  f_determ H4 H17.
  f_determ H0 H9.
  f_determ H0 H10.
  fparam_determ evalExpression_f_correct H H9.
  f_determ H0 H10.
  f_determ H0 H10.
  
  induction H; inversion R; subst; eauto.
  eapply evalSec_opel_expression_d_affectation_determ in H; eauto; subst.
  f_determ H0 H7.
  eapply evalSec_opel_expression_if_determ in H; eauto. inversion H.
  eapply evalSec_opel_expression_if_determ in H; eauto. inversion H.
  assert (D := evalInstruction_s_determ (env, evn) ins (OK env' evn') H0 (OK env'0 evn'0) H8); inversion D; subst.
  apply IHevalInstruction; exact H9.
  assert (D := evalInstruction_s_determ (env, evn) ins (OK env' evn') H0 (Error e env'0 evn'0) H8); inversion D; subst.
  fparam_determ evalSec_opel_w_f_correct H H7.
  assert (D := evalInstruction_s_determ (env, evn) ins (Error e env' evn') H0 (OK env'0 evn'0) H7); inversion D; subst.
  fparam_determ evalSec_opel_w_f_correct H H6.
  fparam_determ evalSec_opel_w_f_correct H H4.
  fparam_determ evalSec_opel_w_f_correct H H5.

  induction H; inversion R; subst; eauto.
  assert (D := evalInstruction_determ (env, evn) h (OK env' evn') H (OK env'0 evn'0) H6); inversion D; subst.
  apply IHevalInstruction_s; exact H7.
  assert (D := evalInstruction_determ (env, evn) h (OK env' evn') H (Error e env'0 evn'0) H6); inversion D.
  assert (D := evalInstruction_determ (env, evn) h (Error e env' evn') H (OK env'0 evn'0) H5); inversion D.

  destruct H; inversion R; subst; eauto.
  assert (D := evalSec_opel_expression_if_determ evn env cond (Bool true) H (Bool false) H7); inversion D.
  assert (D := evalSec_opel_expression_if_determ evn env cond (Bool false) H (Bool true) H7); inversion D.

  destruct H; inversion R; subst; eauto.
  assert (D := evalInstruction_s_determ (env, evn) ins_s (OK env' evn') H (OK env'0 evn'0) H5); inversion D; subst.
  auto.
  assert (D := evalInstruction_s_determ (env, evn) ins_s (OK env' evn') H (Error e env'0 evn'0) H5); inversion D; subst.
  assert (D := evalInstruction_s_determ (env, evn) ins_s (Error e env' evn') H (OK env'0 evn'0) H5); inversion D; subst.
  fparam_determ evalExpression_f_correct H H8.
  fparam_determ evalExpression_f_correct H H8.
Qed. *)


