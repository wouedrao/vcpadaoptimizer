#!/bin/bash -x
coqc TacUtils.v
coqc Map.v
coqc MapTypes.v
coqc Ast.v
menhir Parser.vy --coq --unused-tokens
coqc Parser.v
./copy-of-coqlex/generator Lexer.vl --no-main --no-lib-ml --no-extract -o ./ > /dev/null
coqc Lexer.v
coqc Values.v
coqc Environment.v
coqc Event.v
coqc Semantics.v
coqc Semantics_f.v
coqc Determinism.v
coqc PartialView.v
coqc Correctness.v
coqc OptimUtils.v
coqc ConvergeOptim1.v
coqc ConvergeOptim2.v
coqc ConvergeOptim3.v
coqc InlinePragma.v
coqc SyntaxOptimizerPropagator.v
coqc SyntaxOptimizers.v
coqc ContextRule.v
coqc PartialViewOptimizerPropagator.v
coqc FlowOptim.v
coqc While_unroll.v
coqc ExpressionSimplifier.v
coqc PartialViewOptimizers.v
coqc Preprocessor.v
coqc Extraction.v
#./clean_coq.sh
