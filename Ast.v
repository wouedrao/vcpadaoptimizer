Require Import MapTypes List ZArith.
Local Open Scope Z_scope.
Local Open Scope type_scope.
Import ListNotations.

Definition ident := String_Equality.t. (* <identifier> *)

Definition selected_com := list String_Equality.t. 

Definition name := selected_com. (* <name> *)

Inductive unary_op : Type :=  
| NumericUnaryMinus : unary_op
| BoolNot : unary_op
| NumericAbs : unary_op.

Inductive binary_op : Type := 
| And : binary_op
| Or : binary_op
| XOr : binary_op
| Equal : binary_op
| NEqual : binary_op
| LessThan : binary_op
| LessOrEq : binary_op
| GreaterThan : binary_op
| GreaterOrEq : binary_op
| EMult : binary_op
| EAdd : binary_op
| EMinus : binary_op
| Ediv : binary_op
| Erem : binary_op
| Emod : binary_op
| EPow : binary_op.

Inductive literal : Type := 
| Int_lit : Z -> literal
| Bool_lit : bool -> literal.

Inductive expression : Type := (* <expression> *)
| EVar : name -> expression
| ELit : literal -> expression
| Ebinary : binary_op -> expression -> expression -> expression
| Eunary : unary_op -> expression -> expression.

Inductive expression_or_string_lit : Type := (* <param> *)
| Exp : expression -> expression_or_string_lit
| StringLit : String_Equality.t -> expression_or_string_lit.

Definition association_d_arguments := list expression_or_string_lit. (* <params> *)

Inductive pragma : Type := (*VCP ADA pragmas*)
| Pragma : String_Equality.t -> association_d_arguments -> pragma
| SEC_OPEL_File : selected_com -> expression -> pragma
| SEC_OPEL_Signature : selected_com -> expression -> expression -> pragma.

Definition sec_opel_parametre_literal := literal.

Inductive sec_opel_parametre : Type := (* <lit-or-var> *)
| SEC_OPEL_Value : sec_opel_parametre_literal -> sec_opel_parametre
| SEC_OPEL_Var : name -> sec_opel_parametre.

Inductive sec_opel_binary_bool_op : Type := 
| SOLand : sec_opel_binary_bool_op
| SOLor : sec_opel_binary_bool_op
| SORless : sec_opel_binary_bool_op
| SORleq : sec_opel_binary_bool_op
| SORgreater : sec_opel_binary_bool_op
| SORgeq : sec_opel_binary_bool_op
| SOReq : sec_opel_binary_bool_op
| SORdiff : sec_opel_binary_bool_op.

Inductive sec_opel_binary_num_op : Type :=
| SOEAdd : sec_opel_binary_num_op
| SOEMinus : sec_opel_binary_num_op
| SOEMult : sec_opel_binary_num_op.

Inductive sec_opel_expression_booleenne : Type := (* <cond-exp> *)
| SOEBNot : (list String_Equality.t) -> sec_opel_expression_booleenne
| SOEBBinaire : sec_opel_binary_bool_op -> (list String_Equality.t) ->
  sec_opel_parametre -> sec_opel_expression_booleenne.

Inductive sec_opel_expression_d_affectation : Type := (* <assign-exp> *)
| SOEAffExpUMinus : (list String_Equality.t) ->
  sec_opel_expression_d_affectation
| SOEAffExpBinary : sec_opel_binary_num_op -> sec_opel_parametre ->
  sec_opel_parametre -> sec_opel_expression_d_affectation
| SOEAffDiv2 : (list String_Equality.t) -> expression ->
  sec_opel_expression_d_affectation
| SOEAffBool : sec_opel_expression_booleenne ->
  sec_opel_expression_d_affectation
| SOEAffAssign : sec_opel_parametre -> sec_opel_expression_d_affectation
| SOEAffBuildTag : (list String_Equality.t) ->
  (option ((list String_Equality.t) * (option (list String_Equality.t))))
  -> sec_opel_expression_d_affectation
| SOEAffEncode : expression -> sec_opel_expression_d_affectation
| SOEAffRead : (list String_Equality.t) ->
  (option ((list String_Equality.t) * (option (list String_Equality.t))))
  -> sec_opel_expression_d_affectation
| SOEAffReadContext : sec_opel_expression_d_affectation.

Inductive sec_opel_w : Type := (* <while-cond> *)
| SOWBool : sec_opel_expression_booleenne -> sec_opel_w
| SOWNSecu : (list String_Equality.t) -> sec_opel_w.

Inductive sec_opel_expression_if : Type := (* <if-cond> *)
| SOExpression_booleenne : sec_opel_expression_booleenne ->
  sec_opel_expression_if
| SOIs_valid : sec_opel_expression_if
| SOHard_input_valid : sec_opel_expression_if
| SOt : (list String_Equality.t) -> sec_opel_expression_if.

Inductive type_designator : Type := (* <type-designator> *)
| TypeSimple : name -> type_designator
| TypeComposeArray1 : name -> expression -> type_designator
| TypeComposeArray2 : name -> expression -> expression -> type_designator.

Inductive mode : Type := (* <mode> *)
| In : mode
| InOut : mode
| Out : mode.

Inductive obj : Type := (* <var-decl> *)
| ConstantVar : String_Equality.t -> type_designator -> expression -> obj
| VariableVar : String_Equality.t -> type_designator -> obj.

Inductive declaration_de_base : Type := (* <decl-d> *)
| Declaration_d_objet : obj -> declaration_de_base
| Declaration_de_sous_programme : String_Equality.t -> (list (String_Equality.t * mode * name)) ->
  declaration_de_base
| Definition_de_type_Article : String_Equality.t -> (list (String_Equality.t * name)) ->
  declaration_de_base
| Declaration_de_sous_type : String_Equality.t -> type_designator -> declaration_de_base
| Declaration_de_surnom : String_Equality.t -> (list (String_Equality.t * mode * name)) -> name ->
  declaration_de_base
| Use_Clause : declaration_de_base
| Pragma_Declaration : pragma -> declaration_de_base.

(*Inductive procedure_call : Type := (* <proc-call> *)
 | ICallDefinedFunction : (list String_Equality.t) ->
  (option association_d_arguments) -> procedure_call
| SOInstructionBeginLoop : expression -> procedure_call
| SOInstructionConverge : expression -> procedure_call
| SOInstructionCompareM : (list String_Equality.t) ->
  (list String_Equality.t) -> (list String_Equality.t) -> expression ->
  procedure_call
| SOInstructionCompareR : (list String_Equality.t) ->
  (list String_Equality.t) -> (list String_Equality.t) ->
  (list String_Equality.t) -> (list String_Equality.t) ->
  (list String_Equality.t) -> (list String_Equality.t) -> expression ->
  procedure_call
| SOInstructionEndBranch : expression -> procedure_call
| SOInstructionEndInit : procedure_call
| SOInstructionEndIter : procedure_call
| SOInstructionFailAlarm : procedure_call
| SOInstructionInit : (list String_Equality.t) -> (option expression) ->
  procedure_call
| SOInstructionInitopel : procedure_call
| SOInstructionNewSafe : expression -> expression -> expression -> procedure_call
| SOInstructionNewSig : (list String_Equality.t) -> expression -> expression
  -> expression -> procedure_call
| SOInstructionReadHardInput : (list String_Equality.t) ->
  (list String_Equality.t) -> expression -> expression -> procedure_call
| SOInstructionReadMess : (list String_Equality.t) ->
  (list String_Equality.t) -> (list String_Equality.t) -> expression ->
  expression -> expression -> expression -> procedure_call
| SOInstructionReadMessUnfil : (list String_Equality.t) ->
  (list String_Equality.t) -> (list String_Equality.t) -> expression ->
  expression -> expression -> procedure_call
| SOInstructionWrite : (list String_Equality.t) ->
  (list String_Equality.t) -> (list String_Equality.t) ->
  (option (list String_Equality.t)) -> procedure_call
| SOInstructionWriteHardOutput : (list String_Equality.t) ->
  (list String_Equality.t) -> expression -> expression -> procedure_call
| SOInstructionWriteMess : (list String_Equality.t) ->
  (list String_Equality.t) -> expression -> expression -> expression ->
  procedure_call
| SOInstructionWriteOutvar : (list String_Equality.t) ->
  (list String_Equality.t) -> expression -> expression -> expression ->
  procedure_call
| SOInstructionWriteRedund : (list String_Equality.t) ->
  (list String_Equality.t) -> expression -> expression -> expression ->
  procedure_call. *)

Inductive instruction : Type := (* <inst> *)
| INull : instruction
| IPragma : pragma -> instruction
| IAssign : (list String_Equality.t) -> sec_opel_expression_d_affectation ->
  instruction
| IIf : sec_opel_expression_if -> instruction_s -> elif_instruction ->
  instruction
| ICase : (list String_Equality.t) -> case_instruction -> instruction
| ILoop : sec_opel_w -> instruction_s -> instruction
| ICallDefinedFunction : (list String_Equality.t) ->
  (option association_d_arguments) -> instruction
| SOInstructionBeginLoop : expression -> instruction
| SOInstructionConverge : expression -> instruction
| SOInstructionCompareM : (list String_Equality.t) ->
  (list String_Equality.t) -> (list String_Equality.t) -> expression ->
  instruction
| SOInstructionCompareR : (list String_Equality.t) ->
  (list String_Equality.t) -> (list String_Equality.t) ->
  (list String_Equality.t) -> (list String_Equality.t) ->
  (list String_Equality.t) -> (list String_Equality.t) -> expression ->
  instruction
| SOInstructionEndBranch : expression -> instruction
| SOInstructionEndInit : instruction
| SOInstructionEndIter : instruction
| SOInstructionFailAlarm : instruction
| SOInstructionInit : (list String_Equality.t) -> (option expression) ->
  instruction
| SOInstructionInitopel : instruction
| SOInstructionNewSafe : expression -> expression -> expression -> instruction
| SOInstructionNewSig : (list String_Equality.t) -> expression -> expression
  -> expression -> instruction
| SOInstructionReadHardInput : (list String_Equality.t) ->
  (list String_Equality.t) -> expression -> expression -> instruction
| SOInstructionReadMess : (list String_Equality.t) ->
  (list String_Equality.t) -> (list String_Equality.t) -> expression ->
  expression -> expression -> expression -> instruction
| SOInstructionReadMessUnfil : (list String_Equality.t) ->
  (list String_Equality.t) -> (list String_Equality.t) -> expression ->
  expression -> expression -> instruction
| SOInstructionWrite : (list String_Equality.t) ->
  (list String_Equality.t) -> (list String_Equality.t) ->
  (option (list String_Equality.t)) -> instruction
| SOInstructionWriteHardOutput : (list String_Equality.t) ->
  (list String_Equality.t) -> expression -> expression -> instruction
| SOInstructionWriteMess : (list String_Equality.t) ->
  (list String_Equality.t) -> expression -> expression -> expression ->
  instruction
| SOInstructionWriteOutvar : (list String_Equality.t) ->
  (list String_Equality.t) -> expression -> expression -> expression ->
  instruction
| SOInstructionWriteRedund : (list String_Equality.t) ->
  (list String_Equality.t) -> expression -> expression -> expression ->
  instruction

with elif_instruction : Type := (* <else-if> + else part *)
| IElse : instruction_s -> elif_instruction
| IElif : sec_opel_expression_if -> instruction_s -> elif_instruction ->
  elif_instruction

with instruction_s : Type := (* <insts> *)
| ISeqEnd : instruction -> instruction_s
| ISeqCons : instruction -> instruction_s -> instruction_s

with case_instruction : Type := (* <case-v> + others part *)
| ICaseFinishDefault : instruction_s -> case_instruction
| ICaseCons : (expression * instruction_s) -> case_instruction ->
  case_instruction.

Inductive f_local_dec : Type := 
| LocalDecVar : obj -> f_local_dec
| LocalDecPragma : pragma -> f_local_dec.

Record element_declararif := (* <proc-def> *) { 
  f_name : String_Equality.t;
  f_args : list (String_Equality.t * mode * name);
  f_local_decs : list f_local_dec;
  f_instrs : instruction_s }.

Inductive declaration_1_body : Type := (* <b-1> *)
| Body_Declaration_d_objet : obj -> declaration_1_body
| Body_Definition_de_type_Article : String_Equality.t -> (list (String_Equality.t * name)) ->
  declaration_1_body
| Body_Declaration_de_sous_type : String_Equality.t -> type_designator -> declaration_1_body
| Body_Use_Clause : declaration_1_body
| Body_Pragma_Declaration : pragma -> declaration_1_body.

Inductive declaration_2_body : Type := (* <b-2> *)
| Fun_def : element_declararif -> declaration_2_body
| Fun_dec : String_Equality.t -> (list (String_Equality.t * mode * name)) -> declaration_2_body
| Fun_renames : String_Equality.t -> (list (String_Equality.t * mode * name)) -> name ->
  declaration_2_body.

Inductive unit : Type := (* <unit> *)
| Compilation_Body : element_declararif -> unit (*Main files*)
| Compilation_PkgDecl : String_Equality.t -> (list declaration_de_base) -> unit (*header files*)
| Compilation_PkgBody : String_Equality.t -> (list declaration_1_body) -> (*header implementation files*)
  (list declaration_2_body) -> unit.

Definition compilation := (* <comp> *) ((list String_Equality.t) * bool) * unit.
