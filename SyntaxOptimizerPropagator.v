Require Import Map MapTypes Values Environment Event Semantics Semantics_f Correctness PartialView.
Require Import Ast.
Require Import List String ZArith Lia.
Import ListNotations.

Definition syntax_optimizer : Type := (Ast.instruction_s -> bool) -> (Ast.instruction_s -> Ast.instruction_s -> bool) -> instruction_s -> instruction_s.

Definition syntax_optimizer_is_correct (opt: syntax_optimizer) := 
    forall ins_s indata res, 
      evalInstruction_s indata ins_s res -> forall dup dec,
      evalInstruction_s indata (opt dup dec ins_s) res.

Definition syntax_optimizer_encapsuler_for_element_declararif (opt: syntax_optimizer) dup dec ed :=
    {| f_name := ed.(f_name); f_args := ed.(f_args); f_local_decs := ed.(f_local_decs); f_instrs := opt (dup ed.(f_name)) (dec ed.(f_name)) ed.(f_instrs) |}.

Theorem syntax_optimizer_encap_ed_correct :
    forall opt ed params indata res, 
      syntax_optimizer_is_correct opt ->
      execFunction indata ed params res -> forall dup dec,
      execFunction indata (syntax_optimizer_encapsuler_for_element_declararif opt dup dec ed) params res.
Proof.
intros.
induction H0.
eapply execFunctionSucced; eauto.
simpl.
apply H.
auto.
eapply execFunctionFailed; eauto.
simpl.
apply H.
auto.
Qed.

Theorem syntax_optimizer_encap_ed_correct' :
    forall opt, syntax_optimizer_is_correct opt -> forall dup dec ed opt_env run_env run_trace res params, 
      envpartialView opt_env run_env ->
      execFunction (run_env, run_trace) ed params res -> 
      execFunction (run_env, run_trace) (syntax_optimizer_encapsuler_for_element_declararif opt dup dec ed) params res.
Proof.
intros.
eapply syntax_optimizer_encap_ed_correct; eauto.
Qed.

Definition syntax_optimizer_encapsuler_for_declaration_2_body (opt: syntax_optimizer) dup dec d := match d with
| Fun_def ed => Fun_def (syntax_optimizer_encapsuler_for_element_declararif opt dup dec ed)
| r => r
end. 

Theorem syntax_optimizer_encapsuler_for_declaration_2_body_correct' : forall opt,
  syntax_optimizer_is_correct opt -> forall dup deci dec run_env inenv outenv inenv' outenv',
  env_optimization_correct' run_env inenv inenv' ->
  evalDeclaration_2_bodyList inenv dec outenv ->
  evalDeclaration_2_bodyList inenv' (List.map (syntax_optimizer_encapsuler_for_declaration_2_body opt dup deci) dec) outenv' ->
  env_optimization_correct' run_env outenv outenv'.
Proof.
induction dec; simpl; intros; inversion H1; inversion H2; subst; auto.
eapply IHdec with (inenv:=env'); eauto.
inversion H6; subst; simpl in *; inversion H12; simpl in *; subst.
eapply env_optimization_correct_declareFunction; eauto.
assert (D := env_optimization_correct_declareFunction fid fparams run_env inenv inenv' env'1 env'2 H0 H3 H13).
eapply env_optimization_correct_defineFunction; eauto.
eapply env_optimization_correct_defineFunction with 
  (e:=inenv)
  (e':=inenv')
  (de:=env') 
  (de':=env'0) 
  (f := ed)
  (f' := syntax_optimizer_encapsuler_for_element_declararif opt dup deci ed)
  ; eauto.
intros.
eapply syntax_optimizer_encap_ed_correct'; eauto.
Qed.

Definition syntax_optimizer_encapsuler_for_unite (opt: syntax_optimizer) dup deci unite := match unite with
| Compilation_Body ed => Compilation_Body (syntax_optimizer_encapsuler_for_element_declararif opt (dup []) (deci []) ed)
| Compilation_PkgDecl id dec => Compilation_PkgDecl id dec
| Compilation_PkgBody id dec fdec => Compilation_PkgBody id dec (List.map (syntax_optimizer_encapsuler_for_declaration_2_body opt (dup [id]) (deci [id])) fdec)
end.

Theorem syntax_optimizer_encapsuler_for_unite_correct : forall opt,
  syntax_optimizer_is_correct opt -> forall dup dec b inenv,
  (match b with
    | Compilation_Body ed =>
        match syntax_optimizer_encapsuler_for_unite opt dup dec b with
        | Compilation_Body ed' =>
            forall de de', ed.(f_name) = ed'.(f_name) /\ (
            defineFunction ed inenv = Some de -> 
            defineFunction ed' inenv = Some de' -> 
            env_optimization_correct de de')
        | _ => False
        end
    | Compilation_PkgDecl id decls =>
        match syntax_optimizer_encapsuler_for_unite opt dup dec b with
        | Compilation_PkgDecl id' decls' =>
            forall env_wd env_wd' : env,
            id = id' /\
            (include_except_prag (declaration_set_of_declaration_de_base decls) (declaration_set_of_declaration_de_base decls')) /\
            (evalDeclaration_de_baseList inenv decls env_wd ->
             evalDeclaration_de_baseList inenv decls' env_wd' ->
             env_optimization_correct env_wd env_wd')
        | _ => False
        end
    | Compilation_PkgBody id decls fdecls =>
        match syntax_optimizer_encapsuler_for_unite opt dup dec b with
        | Compilation_PkgBody id' decls' fdecls' =>
            forall env_wd env_wf env_wd' env_wf' : env,
            id = id' /\
            (include_except_prag (declaration_set_of_declaration_1_body decls) (declaration_set_of_declaration_1_body decls')) /\
            (evalDeclaration_1_bodyList inenv decls env_wd ->
             evalDeclaration_2_bodyList env_wd fdecls env_wf ->
             evalDeclaration_1_bodyList inenv decls' env_wd' ->
             evalDeclaration_2_bodyList env_wd' fdecls' env_wf' ->
             env_optimization_correct env_wf env_wf')
        | _ => False
        end
    end).
Proof.
induction b; simpl; intros.
split; eauto.
intros.
eapply env_optimization_correct_defineFunction with 
  (e:=inenv)
  (e':=inenv)
  (de:=de) 
  (de':=de') 
  (f := e)
  (f' := syntax_optimizer_encapsuler_for_element_declararif opt (dup []) (dec []) e)
  ; eauto.
intros.
apply env_optimization_correct_refl.
eapply syntax_optimizer_encap_ed_correct'; eauto.
split; auto.
split.
apply include_except_prag_refl.
intros.
eapply evalDeclaration_de_baseList_f_correct in H0.
eapply evalDeclaration_de_baseList_f_correct in H1.
rewrite H0 in H1; inversion H1; subst.
apply env_optimization_correct_refl.
split; auto.
split.
apply include_except_prag_refl.
intros.
eapply evalDeclaration_1_bodyList_f_correct in H0.
eapply evalDeclaration_1_bodyList_f_correct in H2.
rewrite H0 in H2; inversion H2; subst.
eapply syntax_optimizer_encapsuler_for_declaration_2_body_correct'; eauto.
apply env_optimization_correct_refl.
Qed.

Definition syntax_optimizer_encapsuler_for_compilation (opt: syntax_optimizer) dup dec (p : preprocessor) (c: compilation) := match c with
| (incls, unt) => (incls, (syntax_optimizer_encapsuler_for_unite opt dup dec unt))
end.

Lemma p_getname : forall dup dec (p : preprocessor) a b (opt:syntax_optimizer), p a (getName (syntax_optimizer_encapsuler_for_unite opt dup dec b)) = p a (getName b).
Proof.
induction b; eauto.
Qed.

Theorem syntax_optimizer_encapsuler_for_compilation_correct : forall dup dec opt p,
  syntax_optimizer_is_correct opt -> forall c,
  compilation_correct_optimization p c (syntax_optimizer_encapsuler_for_compilation opt dup dec).
Proof.
unfold compilation_correct_optimization.
induction c; simpl.
intros.
assert (D := p_getname dup dec p a b opt).
rewrite D in H1.
rewrite H0 in H1.
inversion H1; subst.
eapply (syntax_optimizer_encapsuler_for_unite_correct); auto.
Qed.

(* Definition syntax_optimizer_is_correct (opt: syntax_optimizer) := 
    forall ins_s indata res, 
      evalInstruction_s indata ins_s res -> forall dup dec,
      evalInstruction_s indata (opt dup dec ins_s) res. *)

Definition syntax_optimizer_dec_only 
  (opt: (Ast.instruction_s -> Ast.instruction_s -> bool) -> instruction_s -> instruction_s)
  (dup: Ast.instruction_s -> bool) 
  (dec: Ast.instruction_s -> Ast.instruction_s -> bool) := opt dec.

Theorem syntax_optimizer_dec_only_is_correct: forall opt, (forall ins_s indata res, 
      evalInstruction_s indata ins_s res -> forall dec,
      evalInstruction_s indata (opt dec ins_s) res) -> syntax_optimizer_is_correct (syntax_optimizer_dec_only opt).
Proof.
intros.
unfold syntax_optimizer_is_correct.
unfold syntax_optimizer_dec_only.
intros.
auto.
Qed.








