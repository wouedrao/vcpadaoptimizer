Require Import List String ZArith Ast MapTypes.
Import ListNotations.

Inductive value: Type :=
  | Undefined
  | Int (n : Z)
  | Bool (n : bool)
  | STR (n : String_Equality.t)
  | ArrayV : NatMapModule.t value -> value -> value
  | RecordV (r : StringMapModule.t value). 
  
Inductive ConstOrVar :=
| ConstantValue : value -> ConstOrVar
| VariableValue : value -> ConstOrVar.

Module Math.
  Definition add (v1 v2: value): option value :=
    match v1, v2 with
    | Int v1', Int v2' => Some (Int (v1' + v2'))
    | Undefined, Int _ => Some Undefined
    | Int _, Undefined => Some Undefined
    | Undefined, Undefined => Some Undefined
    | _, _ => None
    end.

  Definition sub (v1 v2: value): option value :=
    match v1, v2 with
    | Int v1', Int v2' => Some (Int (v1' - v2'))
    | Undefined, Int _ => Some Undefined
    | Int _, Undefined => Some Undefined
    | Undefined, Undefined => Some Undefined
    | _, _ => None
    end.

  Definition mul (v1 v2: value): option value :=
    match v1, v2 with
    | Int v1', Int v2' => Some (Int (v1' * v2'))
    (* | Undefined, Int 0 => Some (Int 0) 
    | Int 0, Undefined => Some (Int 0) *)
    | Undefined, Int _ => Some Undefined
    | Int _, Undefined => Some Undefined
    | Undefined, Undefined => Some Undefined
    | _, _ => None
    end.

  Definition div (v1 v2: value): option value :=
    match v1, v2 with
    | _, Int 0 => None
    | Int v1', Int v2' => Some (Int (Z.quot v1' v2'))
    | Undefined, Int _ => Some Undefined
    | Int _, Undefined => Some Undefined
    | Undefined, Undefined => Some Undefined
    | _, _ => None
    end.

  Definition rem (v1 v2: value): option value :=
    match v1, v2 with
    | _, Int 0 => None
    | Int v1', Int v2' => Some (Int (Z.rem v1' v2'))
    | Undefined, Int _ => Some Undefined
    | Int _, Undefined => Some Undefined
    | Undefined, Undefined => Some Undefined
    | _, _ => None
    end.

  Definition pow (v1 v2: value): option value :=
    match v1, v2 with
    | Int v1', Int v2' => Some (Int (Z.pow v1' v2'))
    | Undefined, Int _ => Some Undefined
    | Int _, Undefined => Some Undefined
    | Undefined, Undefined => Some Undefined
    | _, _ => None
    end.

  Definition mod' (v1 v2: value): option value :=
    match v1, v2 with
    | _, Int 0 => None
    | Int v1', Int v2' => Some (Int (Z.modulo v1' v2'))
    | Undefined, Int _ => Some Undefined
    | Int _, Undefined => Some Undefined
    | Undefined, Undefined => Some Undefined
    | _, _ => None
    end.
    
  Definition to_bool v := 
    match v with
    | Int v => if Zeq_bool 0 v then (Bool false) else (Bool true)
    | v => v
    end.

  Definition and (v1 v2: value): option value :=
    match to_bool v1, to_bool v2 with
    (* | Bool false, Undefined => Some (Bool false)
    | Undefined, Bool false => Some (Bool false) *)
    | Bool v1', Bool v2' => Some (Bool (andb v1' v2'))
    | Undefined, Bool _ => Some Undefined
    | Bool _, Undefined => Some Undefined
    | Undefined, Undefined => Some Undefined
    | _, _ => None
    end.

  Definition or (v1 v2: value): option value :=
    match to_bool v1, to_bool v2 with
    (* | Bool true, Undefined => Some (Bool true)
    | Undefined, Bool true => Some (Bool true) *)
    | Bool v1', Bool v2' => Some (Bool (orb v1' v2'))
    | Undefined, Bool _ => Some Undefined
    | Bool _, Undefined => Some Undefined
    | Undefined, Undefined => Some Undefined
    | _, _ => None
    end.

  Definition xor (v1 v2: value): option value :=
    match to_bool v1, to_bool v2 with
    | Bool v1', Bool v2' => Some (Bool (xorb v1' v2'))
    | Undefined, Bool _ => Some Undefined
    | Bool _, Undefined => Some Undefined
    | Undefined, Undefined => Some Undefined
    | _, _ => None
    end.
  
  Definition eq (v1 v2: value): option value :=
    match v1, v2 with
    | Int v1', Int v2' => Some (Bool (Zeq_bool v1' v2'))
    | Undefined, Int _ => Some Undefined
    | Int _, Undefined => Some Undefined
    | Undefined, Undefined => Some Undefined
    | _, _ => None
    end.

  Definition ne (v1 v2: value): option value :=
    match v1, v2 with
    | Int v1', Int v2' => Some (Bool (Zneq_bool v1' v2'))
    | Undefined, Int _ => Some Undefined
    | Int _, Undefined => Some Undefined
    | Undefined, Undefined => Some Undefined
    | _, _ => None
    end.

  Definition gt (v1 v2: value): option value :=
    match v1, v2 with
    | Int v1', Int v2' => Some (Bool (Zgt_bool v1' v2'))
    | Undefined, Int _ => Some Undefined
    | Int _, Undefined => Some Undefined
    | Undefined, Undefined => Some Undefined
    | _, _ => None
    end.

  Definition ge (v1 v2: value): option value :=
    match v1, v2 with
    | Int v1', Int v2' => Some (Bool (Zge_bool v1' v2'))
    | Undefined, Int _ => Some Undefined
    | Int _, Undefined => Some Undefined
    | Undefined, Undefined => Some Undefined
    | _, _ => None
    end.

  Definition lt (v1 v2: value): option value :=
    match v1, v2 with
    | Int v1', Int v2' => Some (Bool (Zlt_bool v1' v2'))
    | Undefined, Int _ => Some Undefined
    | Int _, Undefined => Some Undefined
    | Undefined, Undefined => Some Undefined
    | _, _ => None
    end.

  Definition le (v1 v2: value): option value :=
    match v1, v2 with
    | Int v1', Int v2' => Some (Bool (Zle_bool v1' v2'))
    | Undefined, Int _ => Some Undefined
    | Int _, Undefined => Some Undefined
    | Undefined, Undefined => Some Undefined
    | _, _ => None
    end.

  Definition unary_not (v: value): option value :=
    match to_bool v with
    | Bool v' => Some (Bool (negb v'))
    | Undefined => Some Undefined
    | _ => None
    end.

  Definition unary_plus (v: value): option value :=
    match v with
    | Int v' => Some v
    | Undefined => Some Undefined
    | _ => None
    end.

  Definition unary_minus (v: value): option value :=
    match v with
    | Int v' => Some (Int (Z.opp v'))
    | Undefined => Some Undefined
    | _ => None
    end.

  Definition unary_abs (v: value): option value :=
    match v with
    | Int v' => Some (Int (Z.abs v'))
    | Undefined => Some Undefined
    | _ => None
    end.

  Definition binary_operation (op: binary_op) (v1: value) (v2: value): option value :=
    match op with
    | Equal => eq v1 v2
    | XOr => xor v1 v2
    | NEqual => ne v1 v2
    | GreaterThan => gt v1 v2
    | GreaterOrEq => ge v1 v2
    | LessThan => lt v1 v2
    | LessOrEq => le v1 v2
    | And => and v1 v2
    | Or => or v1 v2
    | EAdd => add v1 v2
    | EMinus => sub v1 v2
    | EMult => mul v1 v2
    | Ediv => div v1 v2
    | Emod => mod' v1 v2
    | Erem => rem v1 v2
    | EPow => pow v1 v2
    end.

  Definition unary_operation (op: unary_op) (v: value): option value :=
    match op with
    | BoolNot => unary_not v
    | NumericUnaryMinus => unary_minus v
    | NumericAbs => unary_abs v
    end.

End Math.




