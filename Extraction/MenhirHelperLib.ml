open Parser
open LexerDefinition
open CoqlexHelperLib
open Ast

  
let coq_report coq_lexbuf =
  Format.printf "(EE) Line %d, characters %d-%d : Unexpected token \"%s\"@." coq_lexbuf.cur_pos.l_number coq_lexbuf.s_pos.c_number coq_lexbuf.cur_pos.c_number ((String.of_seq (List.to_seq coq_lexbuf.str_val)))


module GParser (M : sig val coq_lexer : int -> coq_Lexbuf -> int -> Parser.Aut.Gram.token coq_LexicalAnalysisResult * int val menhirParser : int -> MenhirLibParser.Inter.buffer -> Ast.compilation MenhirLibParser.Inter.parse_result end) = struct

	let coqlexer = CoqlexHelperLib.coqlexbuf_Adapter CoqlexHelperLib.default_fuel M.coq_lexer 
	
	let _local_coq_lexbuf_ref = ref None
	
	let tokens_stream_from_coq_lexbuf coq_lexbuf : MenhirLibParser.Inter.buffer =
		let rec compute_token_stream coq_lexbuf () =
			let loop tokenXlexbufXstorage =
				let token, lexbuf, storage = tokenXlexbufXstorage in
				let _ = _local_coq_lexbuf_ref := Some lexbuf in
				MenhirLibParser.Inter.Buf_cons (token, Lazy.from_fun (compute_token_stream lexbuf))
			in loop (coqlexer coq_lexbuf 0)
		in
		Lazy.from_fun (compute_token_stream coq_lexbuf)
	
	
	let coqparser_from_coqlexbuf lexbuf = 
		(match M.menhirParser 50 (tokens_stream_from_coq_lexbuf lexbuf) with
		| Fail_pr_full _ ->
		        (match !_local_coq_lexbuf_ref with
		        | None -> failwith "failed!"
		        | Some lb -> ((coq_report lb); failwith "failed!")
		        )
		| Timeout_pr ->
		        failwith "timed out!"
		| Parsed_pr (output, _) ->
		        output) ;;

	let parse_from_coqlexbuf = coqparser_from_coqlexbuf
end;;
