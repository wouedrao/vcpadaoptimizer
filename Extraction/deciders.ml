open Ast
open Consts
open Hashtbl
open Float
open Str
open Table_helper

(** End branch decider **)

let rec count_end_branch_instruction = function
| IIf (_, if_ins_s, els_instrs) -> (count_end_branch_instruction_s if_ins_s) +  (count_end_branch_elif_instruction els_instrs)
| ICase (_, case_instrs) -> count_end_branch_case_instruction case_instrs
| SOInstructionEndBranch _ -> 1
| _ -> 0

and count_end_branch_elif_instruction = function
| IElse ins_s -> count_end_branch_instruction_s ins_s
| IElif (_, ins_s, elif_ins) -> (count_end_branch_instruction_s ins_s) + (count_end_branch_elif_instruction elif_ins)

and count_end_branch_instruction_s = function
| ISeqEnd i -> count_end_branch_instruction i
| ISeqCons (i, is) -> Int.max (count_end_branch_instruction i) (count_end_branch_instruction_s is)

and count_end_branch_case_instruction = function
| ICaseFinishDefault i -> count_end_branch_instruction_s i
| ICaseCons (( _, is ), cs) -> (count_end_branch_instruction_s is) + (count_end_branch_case_instruction cs)

let end_branch_decider is_final is_init = let c = count_end_branch_instruction_s is_final in (*Format.printf "Count end_brand %d@." c;*) c <= Consts.max_end_branch
let end_branch_decider_with_filename filename = end_branch_decider

(** Size decider **)
let size_map = Hashtbl.create 200

let init (filename: string) size = Hashtbl.add size_map filename size

let estimateSizeOf_ident id = List.length id

let rec estimateSizeOf_composant_select = function
| [] -> 0
| h::[] -> List.length h 
| h::t -> (estimateSizeOf_ident h) + 1 + (estimateSizeOf_composant_select t)

let estimateSizeOf_nom = estimateSizeOf_composant_select

let estimateSizeOf_unary_op = function
| NumericUnaryMinus -> 1
| BoolNot -> 3
| NumericAbs -> 3

let estimateSizeOf_binary_op = function
| And -> 3
| Or -> 2
| XOr -> 3
| Equal -> 1
| NEqual -> 2
| LessThan -> 1
| LessOrEq -> 2
| GreaterThan -> 1
| GreaterOrEq -> 2
| EMult -> 1
| EAdd -> 1
| EMinus -> 1
| Ediv -> 1
| Erem -> 3
| Emod -> 3
| EPow -> 2

let estimateSizeOf_literal = function
| Int_lit v -> Float.to_int (Float.ceil(Float.log10( Float.of_int (1 + v) )))
| Bool_lit b -> 5

let rec estimateSizeOf_expression = function
| EVar v -> estimateSizeOf_nom v
| ELit v -> estimateSizeOf_literal v
| Ebinary (op, exp, exp0) -> (estimateSizeOf_expression exp) + (estimateSizeOf_binary_op op) + (estimateSizeOf_expression exp0)
| Eunary (op, exp) -> 3 + (estimateSizeOf_unary_op op) + (estimateSizeOf_expression exp)

let estimateSizeOf_expression_or_string_lit = function
| Exp e -> estimateSizeOf_expression e
| StringLit s -> 2 + (estimateSizeOf_ident s)


let rec estimateSizeOf_positionnal_args = function
| [] -> 0
| h::t -> (estimateSizeOf_expression_or_string_lit h) + (match t with | [] -> 0 | _ -> 2 + (estimateSizeOf_positionnal_args t))
		

let estimateSizeOf_association_d_arguments args = 2 + (estimateSizeOf_positionnal_args args)

let estimateSizeOf_pragma = function
| Pragma(id, arg_assoc) -> 	8 + (estimateSizeOf_ident id) + (estimateSizeOf_association_d_arguments arg_assoc)
| SEC_OPEL_File (sc, e) -> 20 + (estimateSizeOf_composant_select sc) + (estimateSizeOf_expression e)
| SEC_OPEL_Signature (sc, e, e0) -> 27 + (estimateSizeOf_composant_select sc) + (estimateSizeOf_expression e) + (estimateSizeOf_expression e0)

let estimateSizeOf_sec_opel_parametre_literal = estimateSizeOf_literal

let estimateSizeOf_sec_opel_parametre_nom_secu = estimateSizeOf_composant_select

let estimateSizeOf_sec_opel_parametre = function
| SEC_OPEL_Value v -> estimateSizeOf_sec_opel_parametre_literal v
| SEC_OPEL_Var v -> estimateSizeOf_nom v

let sec_opel_binary_num_op op = 1

let estimateSizeOf_sec_opel_expression_booleenne = function
| SOEBNot sc -> 4 + (estimateSizeOf_composant_select sc)
| SOEBBinaire (SOLand, sc, p) -> 5 + (estimateSizeOf_composant_select sc) + (estimateSizeOf_sec_opel_parametre p)
| SOEBBinaire (SOLor, sc, p) -> 4 + (estimateSizeOf_composant_select sc) + (estimateSizeOf_sec_opel_parametre p)
| SOEBBinaire (SORless, sc, p) -> 3 + (estimateSizeOf_composant_select sc) + (estimateSizeOf_sec_opel_parametre p)
| SOEBBinaire (SORleq, sc, p) -> 4 + (estimateSizeOf_composant_select sc) + (estimateSizeOf_sec_opel_parametre p)
| SOEBBinaire (SORgreater, sc, p) -> 3 + (estimateSizeOf_composant_select sc) + (estimateSizeOf_sec_opel_parametre p)
| SOEBBinaire (SORgeq, sc, p) -> 4 + (estimateSizeOf_composant_select sc) + (estimateSizeOf_sec_opel_parametre p)
| SOEBBinaire (SOReq, sc, p) -> 9 + (estimateSizeOf_composant_select sc) + (estimateSizeOf_sec_opel_parametre p)
| SOEBBinaire (SORdiff, sc, p) -> 8 + (estimateSizeOf_composant_select sc) + (estimateSizeOf_sec_opel_parametre p)


let estimateSizeOf_sec_opel_expression_d_affectation = function
| SOEAffExpUMinus sc -> 2 + (estimateSizeOf_composant_select sc)
| SOEAffExpBinary (op, p, p0) -> 2 + (estimateSizeOf_sec_opel_parametre p) + (sec_opel_binary_num_op op) + (estimateSizeOf_sec_opel_parametre p0)
| SOEAffDiv2 (pns, e) -> 8 + (estimateSizeOf_sec_opel_parametre_nom_secu pns) +  (estimateSizeOf_expression e)
| SOEAffBool eb -> estimateSizeOf_sec_opel_expression_booleenne eb
| SOEAffAssign p -> 8 + (estimateSizeOf_sec_opel_parametre p)
| SOEAffBuildTag (pns, None) -> 11 + (estimateSizeOf_sec_opel_parametre_nom_secu pns)
| SOEAffBuildTag (pns, Some (pns0, None )) -> 13 + (estimateSizeOf_sec_opel_parametre_nom_secu pns) + (estimateSizeOf_sec_opel_parametre_nom_secu pns0)
| SOEAffBuildTag (pns, Some (pns0, Some pns1 )) -> 15 + (estimateSizeOf_sec_opel_parametre_nom_secu pns) + (estimateSizeOf_sec_opel_parametre_nom_secu pns0) + (estimateSizeOf_sec_opel_parametre_nom_secu pns1)
| SOEAffEncode e -> 8 + (estimateSizeOf_expression e)
| SOEAffRead (pns, None) -> 6 + (estimateSizeOf_sec_opel_parametre_nom_secu pns)
| SOEAffRead (pns, Some (pns0, None)) ->  8 + (estimateSizeOf_sec_opel_parametre_nom_secu pns) + (estimateSizeOf_sec_opel_parametre_nom_secu pns0)
| SOEAffRead (pns, Some (pns0, Some pns1)) -> 10 + (estimateSizeOf_sec_opel_parametre_nom_secu pns) + (estimateSizeOf_sec_opel_parametre_nom_secu pns0) + (estimateSizeOf_sec_opel_parametre_nom_secu pns1)
| SOEAffReadContext -> 12

let estimateSizeOf_sec_opel_w = function 
| SOWBool eb -> 3 + (estimateSizeOf_sec_opel_expression_booleenne eb)
| SOWNSecu pns -> 3 + (estimateSizeOf_sec_opel_parametre_nom_secu pns)

let estimateSizeOf_sec_opel_expression_if = function
| SOExpression_booleenne eb -> estimateSizeOf_sec_opel_expression_booleenne eb
| SOIs_valid -> 8
| SOHard_input_valid -> 16
| SOt pns -> 3 + (estimateSizeOf_sec_opel_parametre_nom_secu pns)


let rec estimateSizeOf_instruction = function
| INull -> 6
| IPragma p -> 2 + (estimateSizeOf_pragma p)
| IAssign (pns, eaff) -> 5 + (estimateSizeOf_sec_opel_parametre_nom_secu pns) + (estimateSizeOf_sec_opel_expression_d_affectation eaff)
| IIf (eif, ins_s, elif_i) -> 18 + (estimateSizeOf_sec_opel_expression_if eif) + (estimateSizeOf_instruction_s ins_s) + (estimateSizeOf_elif_instruction elif_i)
| ICase (pns, case_ins) -> 22 + (estimateSizeOf_sec_opel_parametre_nom_secu pns) + (estimateSizeOf_case_instruction case_ins)
| ILoop (sw, ins_s) -> 22 + (estimateSizeOf_sec_opel_w sw) + (estimateSizeOf_instruction_s ins_s)
| ICallDefinedFunction (pns, None) -> 1 + (estimateSizeOf_sec_opel_parametre_nom_secu pns)
| ICallDefinedFunction (pns, Some args) -> 1 + (estimateSizeOf_sec_opel_parametre_nom_secu pns) + (estimateSizeOf_association_d_arguments args)
| SOInstructionBeginLoop e -> 13 + (estimateSizeOf_expression e)
| SOInstructionConverge e -> 11 + (estimateSizeOf_expression e)
| SOInstructionCompareM (pns, pns0, pns1, e) -> 25 + (estimateSizeOf_sec_opel_parametre_nom_secu pns) + (estimateSizeOf_sec_opel_parametre_nom_secu pns0) + (estimateSizeOf_sec_opel_parametre_nom_secu pns1) + (estimateSizeOf_expression e)
| SOInstructionCompareR (pns, pns0, pns1, pns2, pns3, pns4, pns5, e) -> 44 + (estimateSizeOf_sec_opel_parametre_nom_secu pns) + (estimateSizeOf_sec_opel_parametre_nom_secu pns0) + (estimateSizeOf_sec_opel_parametre_nom_secu pns1) + (estimateSizeOf_sec_opel_parametre_nom_secu pns2) + (estimateSizeOf_sec_opel_parametre_nom_secu pns3) + (estimateSizeOf_sec_opel_parametre_nom_secu pns4) + (estimateSizeOf_sec_opel_parametre_nom_secu pns5) + (estimateSizeOf_expression e)
| SOInstructionEndBranch e -> 13 + (estimateSizeOf_expression e)
| SOInstructionEndInit -> 9
| SOInstructionEndIter -> 14
| SOInstructionFailAlarm -> 14
| SOInstructionInit (pns, None) -> 7 + (estimateSizeOf_sec_opel_parametre_nom_secu pns)
| SOInstructionInit (pns, Some e) -> 9 + (estimateSizeOf_sec_opel_parametre_nom_secu pns) + (estimateSizeOf_expression e)
| SOInstructionInitopel -> 9
| SOInstructionNewSafe (e, e0, e1) ->  23 + (estimateSizeOf_expression e) + (estimateSizeOf_expression e0) + (estimateSizeOf_expression e1)
| SOInstructionNewSig ( pns, e, e0, e1) -> 22 + (estimateSizeOf_sec_opel_parametre_nom_secu pns) + (estimateSizeOf_expression e) + (estimateSizeOf_expression e0) + (estimateSizeOf_expression e1)
| SOInstructionReadHardInput (pns, pns0, e, e0) ->  24 + (estimateSizeOf_sec_opel_parametre_nom_secu pns) + (estimateSizeOf_sec_opel_parametre_nom_secu pns0) + (estimateSizeOf_expression e) + (estimateSizeOf_expression e0)
| SOInstructionReadMess (pns, pns0, pns1, e, e0, e1, e2) -> 24 + (estimateSizeOf_sec_opel_parametre_nom_secu pns) + (estimateSizeOf_sec_opel_parametre_nom_secu pns0) + (estimateSizeOf_sec_opel_parametre_nom_secu pns1) + (estimateSizeOf_expression e) + (estimateSizeOf_expression e0) + (estimateSizeOf_expression e1) + (estimateSizeOf_expression e2)
| SOInstructionReadMessUnfil (pns, pns0, pns1, e, e0, e1) -> 33 + (estimateSizeOf_sec_opel_parametre_nom_secu pns) + (estimateSizeOf_sec_opel_parametre_nom_secu pns0) + (estimateSizeOf_sec_opel_parametre_nom_secu pns1) + (estimateSizeOf_expression e) + (estimateSizeOf_expression e0) + (estimateSizeOf_expression e1)
| SOInstructionWrite (pns, pns0, pns1, None) -> 12 + (estimateSizeOf_sec_opel_parametre_nom_secu pns) + (estimateSizeOf_sec_opel_parametre_nom_secu pns0) + (estimateSizeOf_sec_opel_parametre_nom_secu pns1)
| SOInstructionWrite (pns, pns0, pns1, Some pns2) -> 14 + (estimateSizeOf_sec_opel_parametre_nom_secu pns) + (estimateSizeOf_sec_opel_parametre_nom_secu pns0) + (estimateSizeOf_sec_opel_parametre_nom_secu pns1) + (estimateSizeOf_sec_opel_parametre_nom_secu pns2)
| SOInstructionWriteHardOutput (pns, pns0, e, e0) ->  26 + (estimateSizeOf_sec_opel_parametre_nom_secu pns) + (estimateSizeOf_sec_opel_parametre_nom_secu pns0) + (estimateSizeOf_expression e) + (estimateSizeOf_expression e0)
| SOInstructionWriteMess (pns, pns0, e, e0, e1) ->  21 + (estimateSizeOf_sec_opel_parametre_nom_secu pns) + (estimateSizeOf_sec_opel_parametre_nom_secu pns0) + (estimateSizeOf_expression e) + (estimateSizeOf_expression e0) + (estimateSizeOf_expression e1)
| SOInstructionWriteOutvar (pns, pns0, e, e0, e1) ->  23 + (estimateSizeOf_sec_opel_parametre_nom_secu pns) + (estimateSizeOf_sec_opel_parametre_nom_secu pns0) + (estimateSizeOf_expression e) + (estimateSizeOf_expression e0) + (estimateSizeOf_expression e1)
| SOInstructionWriteRedund (pns, pns0, e, e0, e1) ->  30 + (estimateSizeOf_sec_opel_parametre_nom_secu pns) + (estimateSizeOf_sec_opel_parametre_nom_secu pns0) + (estimateSizeOf_expression e) + (estimateSizeOf_expression e0) + (estimateSizeOf_expression e1)

and estimateSizeOf_elif_instruction = function
| IElse ins_s -> 5 + (estimateSizeOf_instruction_s ins_s)
| IElif (eif, ins_s, elif_i) -> 13 + (estimateSizeOf_sec_opel_expression_if eif) + (estimateSizeOf_instruction_s ins_s) + (estimateSizeOf_elif_instruction elif_i)

and estimateSizeOf_instruction_s = function
| ISeqEnd i -> 100 + (estimateSizeOf_instruction i)
| ISeqCons (i, is) -> 100 + (estimateSizeOf_instruction i) + (estimateSizeOf_instruction_s is)

and estimateSizeOf_case_instruction = function
| ICaseFinishDefault is -> 15 + (estimateSizeOf_instruction_s is)
| ICaseCons ( (e, is), case_s ) -> 10 + (estimateSizeOf_expression e) + (estimateSizeOf_instruction_s is) + (estimateSizeOf_case_instruction case_s)

let max_size_decider filename is_init is_final = 
    let current_size = Hashtbl.find size_map filename in
    let size_of_init = estimateSizeOf_instruction_s is_init in
    let size_if_accepted = current_size + (estimateSizeOf_instruction_s is_final) - size_of_init in
    (*Format.printf "Tailles: %d %d %d@." current_size size_if_accepted Consts.max_size_file;*)
    if size_if_accepted < Consts.max_size_file then
        (
            Hashtbl.add size_map filename size_if_accepted;
            true        
        )
    else
        (
            false
        )

let opt_stat = Hashtbl.create 200


let inc_accepted_opt_in_stat filename = 
    let tot, acc, sz, eb, cp, du = 
        (
            match Hashtbl.find_opt opt_stat filename with
            | None -> (0, 0, 0, 0, 0, 0)
            | Some v -> v
        ) in
    Hashtbl.replace opt_stat filename (tot + 1, acc + 1, sz, eb, cp, du)

let inc_refused_sz_opt_in_stat filename = 
    let tot, acc, sz, eb, cp, du = 
        (
            match Hashtbl.find_opt opt_stat filename with
            | None -> (0, 0, 0, 0, 0, 0)
            | Some v -> v
        ) in
    Hashtbl.replace opt_stat filename (tot + 1, acc, sz + 1, eb, cp, du)

let inc_refused_eb_opt_in_stat filename = 
    let tot, acc, sz, eb, cp, du = 
        (
            match Hashtbl.find_opt opt_stat filename with
            | None -> (0, 0, 0, 0, 0, 0)
            | Some v -> v
        ) in
    Hashtbl.replace opt_stat filename (tot + 1, acc, sz, eb + 1, cp, du)

let inc_refused_cp_opt_in_stat filename = 
    let tot, acc, sz, eb, cp, du = 
        (
            match Hashtbl.find_opt opt_stat filename with
            | None -> (0, 0, 0, 0, 0, 0)
            | Some v -> v
        ) in
    Hashtbl.replace opt_stat filename (tot + 1, acc, sz, eb, cp + 1, du)

let inc_refused_du_opt_in_stat filename = 
    let tot, acc, sz, eb, cp, du = 
        (
            match Hashtbl.find_opt opt_stat filename with
            | None -> (0, 0, 0, 0, 0, 0)
            | Some v -> v
        ) in
    Hashtbl.replace opt_stat filename (tot + 1, acc, sz, eb, cp, du + 1)

let print_stats () =
    let (sz, eb, cp, du, tot) = Hashtbl.fold (fun _ (tot0, _, sz0, eb0, cp0, du0) (sz, eb, cp, du, tot) -> (sz + sz0, eb + eb0, cp + cp0, du + du0, tot + tot0)) opt_stat (0, 0, 0, 0, 0) in
    Format.printf "(II) Total opt: %d; Refused for size: %d; Refused for end-branch: %d; Refused for sig-computation: %d; Refused for duplication: %d\n" tot sz eb cp du

let ops_decider filename pkg_name pname is_final is_init =
    if end_branch_decider is_final is_init then
    (
        if not (compensableProblemInstruction_s LVarSetModule.empty (List.map (fun x -> (String.of_seq (List.to_seq x))) pkg_name) (String.of_seq (List.to_seq pname)) is_final) then
        (
            if max_size_decider filename is_init is_final then
            (
                inc_accepted_opt_in_stat filename;
                true
            )
            else
            (
                inc_refused_sz_opt_in_stat filename;
                false
            )
        )
        else
        (
            inc_refused_cp_opt_in_stat filename;
            false
        )
    )
    else 
    (
        inc_refused_eb_opt_in_stat filename;
        false
    )


let ins_s_non_duplic filename pkg_name pname is = if instruction_s_non_duplic_query (List.map (fun x -> (String.of_seq (List.to_seq x))) pkg_name) is then (inc_refused_du_opt_in_stat filename; true) else false


(** Inline decider **)
let rec icounter_instruction = function
| IIf (eif, ins_s, elif_i) -> (Int.max (icounter_instruction_s ins_s) (icounter_elif_instruction elif_i))
| ILoop (sw, ins_s) -> (icounter_instruction_s ins_s)
| ICase (pns, case_ins) -> (icounter_case_instruction case_ins)
| _ -> 1

and icounter_elif_instruction = function
| IElse ins_s -> icounter_instruction_s ins_s
| IElif (eif, ins_s, elif_i) -> Int.max (icounter_instruction_s ins_s) (icounter_elif_instruction elif_i)

and icounter_instruction_s = function
| ISeqEnd i -> icounter_instruction i
| ISeqCons (i, is) -> (icounter_instruction i) + (icounter_instruction_s is)

and icounter_case_instruction = function
| ICaseFinishDefault is -> icounter_instruction_s is
| ICaseCons ( (e, is), case_s ) -> Int.max (icounter_instruction_s is) (icounter_case_instruction case_s)

let contains s1 s2 =
    let re = Str.regexp_string s2 in
    try 
       ignore (Str.search_forward re s1 0); 
       true
    with Not_found -> false

let name_based_discrim pname = contains (String.lowercase_ascii (String.of_seq (List.to_seq pname))) "lire"

let inline_decide body_finder pkgname pname pargs = 
    match body_finder (String.of_seq (List.to_seq pkgname)) pname with
    | Some ({ f_name = _; f_args = _; f_local_decs = []; f_instrs = is }) -> icounter_instruction_s is <= Consts.num_inst_to_inline
    | Some _ -> false
    | None -> name_based_discrim pname
















