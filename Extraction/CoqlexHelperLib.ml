open Format
open Lexing
open Stream
open Lexer
open LexerDefinition

exception UserException of string
exception NoRegexMatch
exception NoFuel
exception NoTokenSpecified

let prefix size clist =
    let rec prefix' size clist acc = match clist with
    | [] -> (String.of_seq (List.to_seq (List.rev acc)))
    | h::t -> if size > 0 then prefix' (size-1) t (h::acc) else (String.of_seq (List.to_seq (List.rev acc))) in
    prefix' size clist []

let coqlexbuf_Adapter fuel coq_lexer lexbuf storage =
	match coq_lexer fuel lexbuf storage with
	| (AnalysisFailedUserRaisedError (err_mess, lexbuf), _) -> 
	    Format.printf "(EE) Line %d, characters %d-%d : Error raised after \"%s\"@." lexbuf.cur_pos.l_number lexbuf.s_pos.c_number lexbuf.cur_pos.c_number (String.of_seq (List.to_seq lexbuf.str_val));
	    Stdlib.raise (UserException ((String.of_seq (List.to_seq err_mess))))
	| (AnalysisFailedEmptyToken (lexbuf), _) -> 
	    Format.printf "(EE) Line %d, character %d : Unexpected sequence of characters \"%s\"@." lexbuf.cur_pos.l_number lexbuf.cur_pos.c_number (prefix 5 lexbuf.remaining_str);
	    Stdlib.raise NoRegexMatch
	| (AnalysisTerminated (Some tok, lexbuf), sto) -> (tok, lexbuf, sto)
	| (AnalysisTerminated (None, lexbuf), _) -> 
	    Format.printf "(EE) Line %d, characters %d-%d : Error no token returned after \"%s\"@." lexbuf.cur_pos.l_number lexbuf.s_pos.c_number lexbuf.cur_pos.c_number (String.of_seq (List.to_seq lexbuf.str_val));
	    Stdlib.raise NoTokenSpecified
	| (AnalysisNoFuel lexbuf, _) -> 
	   Format.printf "(EE) Line %d, character %d : Fuel missing near \"%s\"@." lexbuf.cur_pos.l_number lexbuf.cur_pos.c_number (prefix 5 lexbuf.remaining_str);
	    Stdlib.raise NoFuel

(* let from_ml_lexbuf ml_lexbuf =
 {
    s_pos = {l_number = ml_lexbuf.lex_start_p.pos_lnum; c_number = ml_lexbuf.lex_start_p.pos_bol; abs_pos = ml_lexbuf.lex_start_p.pos_cnum};
    str_val = (lexeme ml_lexbuf);
    cur_pos = {l_number = ml_lexbuf.lex_curr_p.pos_lnum; c_number = ml_lexbuf.lex_curr_p.pos_bol; abs_pos = ml_lexbuf.lex_curr_p.pos_cnum};
    remaining_str = (Bytes.to_string (Bytes.sub ml_lexbuf.lex_buffer ml_lexbuf.lex_curr_pos ml_lexbuf.lex_buffer_len));
 }

let set_ml_lexbuf coq_lexbuf lexbuf =
    let cat = coq_lexbuf.str_val ^ coq_lexbuf.remaining_str in
    lexbuf.lex_buffer <- Bytes.of_string (cat);
    lexbuf.lex_buffer_len <- Bytes.length lexbuf.lex_buffer;
    lexbuf.lex_abs_pos <- coq_lexbuf.cur_pos.abs_pos;
    lexbuf.lex_start_pos <- 0;
    lexbuf.lex_curr_pos <- String.length coq_lexbuf.str_val;
	lexbuf.lex_start_p <- {{pos_fname = ""; pos_lnum  = coq_lexbuf.s_pos.l_number; pos_bol = coq_lexbuf.s_pos.c_number; pos_cnum = coq_lexbuf.s_pos.abs_pos} with pos_fname =
lexbuf.lex_start_p.pos_fname};
	lexbuf.lex_curr_p <- {{pos_fname = ""; pos_lnum  = coq_lexbuf.cur_pos.l_number; pos_bol = coq_lexbuf.cur_pos.c_number; pos_cnum = coq_lexbuf.cur_pos.abs_pos} with pos_fname = lexbuf.lex_curr_p.pos_fname}

let lexbuf_Adapter fuel coq_lexer storage ml_lexbuf =
    let coq_lexbuf = from_ml_lexbuf ml_lexbuf in
    match coq_lexer fuel coq_lexbuf storage with
	| (AnalysisFailedUserRaisedError(err_mess, lexbuf), _) -> 
	    Format.printf "(EE) Line %d, characters %d-%d : Error raised after \"%s\"@." lexbuf.cur_pos.l_number lexbuf.s_pos.c_number lexbuf.cur_pos.c_number lexbuf.str_val;
	    set_ml_lexbuf lexbuf ml_lexbuf; 
	    Stdlib.raise (UserException (err_mess))
	| (AnalysisFailedEmptyToken (lexbuf), _) -> 
	    Format.printf "(EE) Line %d, character %d : Unexpected sequence of characters \"%s\"@." lexbuf.cur_pos.l_number lexbuf.cur_pos.c_number (String.sub lexbuf.remaining_str 0 5);
	    set_ml_lexbuf lexbuf ml_lexbuf; 
	    Stdlib.raise NoRegexMatch
	| (AnalysisTerminated (Some tok, lexbuf), _) -> set_ml_lexbuf lexbuf ml_lexbuf; tok
	| (AnalysisTerminated (None, lexbuf), _) -> 
	    Format.printf "(EE) Line %d, characters %d-%d : Error no token returned after \"%s\"@." lexbuf.cur_pos.l_number lexbuf.s_pos.c_number lexbuf.cur_pos.c_number lexbuf.str_val;
	    set_ml_lexbuf lexbuf ml_lexbuf; 
	    Stdlib.raise NoTokenSpecified
	| (AnalysisNoFuel lexbuf, _) -> 
	    Format.printf "(EE) Line %d, character %d : Fuel missing near \"%s\"@." lexbuf.cur_pos.l_number lexbuf.cur_pos.c_number (String.sub lexbuf.remaining_str 0 5);
	    set_ml_lexbuf lexbuf ml_lexbuf; 
	    Stdlib.raise NoFuel
*)
let default_fuel = 50000
