open Ast
open MapTypes
open Format

let print_ident fmt id = Format.fprintf fmt "%s" (String.of_seq (List.to_seq id))

let rec print_composant_select fmt = function
| [] -> ()
| h::[] -> Format.fprintf fmt "%s" (String.of_seq (List.to_seq h)) 
| h::t -> Format.fprintf fmt "%s.%a" (String.of_seq (List.to_seq h)) print_composant_select t

let print_nom = print_composant_select

let print_unary_op fmt = function
| NumericUnaryMinus -> Format.fprintf fmt "-"
| BoolNot -> Format.fprintf fmt "not"
| NumericAbs -> Format.fprintf fmt "abs"

let print_binary_op fmt = function
| And -> Format.fprintf fmt "and"
| Or -> Format.fprintf fmt "or"
| XOr -> Format.fprintf fmt "xor"
| Equal -> Format.fprintf fmt "="
| NEqual -> Format.fprintf fmt "/="
| LessThan -> Format.fprintf fmt "<"
| LessOrEq -> Format.fprintf fmt "<="
| GreaterThan -> Format.fprintf fmt ">"
| GreaterOrEq -> Format.fprintf fmt ">="
| EMult -> Format.fprintf fmt "*"
| EAdd -> Format.fprintf fmt "+"
| EMinus -> Format.fprintf fmt "-"
| Ediv -> Format.fprintf fmt "/"
| Erem -> Format.fprintf fmt "rem"
| Emod -> Format.fprintf fmt "mod"
| EPow -> Format.fprintf fmt "**"

let print_literal fmt = function
| Int_lit v -> Format.fprintf fmt "%d" v
| Bool_lit b -> Format.fprintf fmt "%s" (if b then "True" else "False")

let rec print_expression fmt = function
| EVar v -> Format.fprintf fmt "%a" print_nom v
| ELit v -> Format.fprintf fmt "%a" print_literal v
| Ebinary (op, exp, exp0) -> Format.fprintf fmt "(%a %a %a)" print_expression exp print_binary_op op print_expression exp0
| Eunary (op, exp) -> Format.fprintf fmt "(%a %a)" print_unary_op op print_expression exp

let print_expression_or_string_lit fmt = function
| Exp e -> Format.fprintf fmt "%a" print_expression e
| StringLit s -> Format.fprintf fmt "\"%s\"" (String.of_seq (List.to_seq s ))


let rec print_positionnal_args fmt = function
| [] -> ()
| h::t -> Format.fprintf fmt "%a%a" print_expression_or_string_lit h 
			(fun fmt t0 -> if t0 = [] then () else Format.fprintf fmt ",@ %a" print_positionnal_args t0) t
 

let print_association_d_arguments fmt args = Format.fprintf fmt "(%a)" print_positionnal_args args

let print_pragma fmt = function
| Pragma(id, arg_assoc) -> 	Format.fprintf fmt "@[pragma %s %a@]" (String.of_seq (List.to_seq id)) print_association_d_arguments arg_assoc
| SEC_OPEL_File (sc, e) -> Format.fprintf fmt "@[pragma ops_file (%a, %a)@]" print_composant_select sc print_expression e
| SEC_OPEL_Signature (sc, e, e0) -> Format.fprintf fmt "@[pragma ops_signature (%a, %a, %a)@]" print_composant_select sc print_expression e print_expression e0

let print_sec_opel_parametre_literal = print_literal

let print_sec_opel_parametre_nom_secu = print_composant_select

let print_sec_opel_parametre fmt = function
| SEC_OPEL_Value v -> Format.fprintf fmt "%a" print_sec_opel_parametre_literal v
| SEC_OPEL_Var v -> Format.fprintf fmt "%a" print_nom v

let sec_opel_binary_num_op fmt = function
| SOEAdd -> Format.fprintf fmt "+"
| SOEMinus -> Format.fprintf fmt "-"
| SOEMult -> Format.fprintf fmt "*"

let print_sec_opel_expression_booleenne fmt = function
| SOEBNot sc -> Format.fprintf fmt "not %a" print_composant_select sc
| SOEBBinaire (SOLand, sc, p) -> Format.fprintf fmt "%a and %a" print_composant_select sc print_sec_opel_parametre p
| SOEBBinaire (SOLor, sc, p) -> Format.fprintf fmt "%a or %a" print_composant_select sc print_sec_opel_parametre p
| SOEBBinaire (SORless, sc, p) -> Format.fprintf fmt "%a < %a" print_composant_select sc print_sec_opel_parametre p
| SOEBBinaire (SORleq, sc, p) -> Format.fprintf fmt "%a <= %a" print_composant_select sc print_sec_opel_parametre p
| SOEBBinaire (SORgreater, sc, p) -> Format.fprintf fmt "%a > %a" print_composant_select sc print_sec_opel_parametre p
| SOEBBinaire (SORgeq, sc, p) -> Format.fprintf fmt "%a >= %a" print_composant_select sc print_sec_opel_parametre p
| SOEBBinaire (SOReq, sc, p) -> Format.fprintf fmt "equal(%a, %a)" print_composant_select sc print_sec_opel_parametre p
| SOEBBinaire (SORdiff, sc, p) -> Format.fprintf fmt "diff(%a, %a)" print_composant_select sc print_sec_opel_parametre p


let print_sec_opel_expression_d_affectation fmt = function
| SOEAffExpUMinus sc -> Format.fprintf fmt "- %a" print_composant_select sc
| SOEAffExpBinary (op, p, p0) -> Format.fprintf fmt "%a %a %a" print_sec_opel_parametre p sec_opel_binary_num_op op print_sec_opel_parametre p0
| SOEAffDiv2 (pns, e) -> Format.fprintf fmt "div2(%a, %a)" print_sec_opel_parametre_nom_secu pns print_expression e
| SOEAffBool eb -> Format.fprintf fmt "%a" print_sec_opel_expression_booleenne eb
| SOEAffAssign p -> Format.fprintf fmt "assign(%a)" print_sec_opel_parametre p
| SOEAffBuildTag (pns, None) -> Format.fprintf fmt "build_tag(%a)" print_sec_opel_parametre_nom_secu pns
| SOEAffBuildTag (pns, Some (pns0, None )) -> Format.fprintf fmt "build_tag(%a, %a)" print_sec_opel_parametre_nom_secu pns print_sec_opel_parametre_nom_secu pns0
| SOEAffBuildTag (pns, Some (pns0, Some pns1 )) -> Format.fprintf fmt "build_tag(%a, %a, %a)" print_sec_opel_parametre_nom_secu pns print_sec_opel_parametre_nom_secu pns0 print_sec_opel_parametre_nom_secu pns1
| SOEAffEncode e -> Format.fprintf fmt "encode(%a)" print_expression e
| SOEAffRead (pns, None) -> Format.fprintf fmt "read(%a)" print_sec_opel_parametre_nom_secu pns
| SOEAffRead (pns, Some (pns0, None)) ->  Format.fprintf fmt "read(%a, %a)" print_sec_opel_parametre_nom_secu pns print_sec_opel_parametre_nom_secu pns0
| SOEAffRead (pns, Some (pns0, Some pns1)) -> Format.fprintf fmt "read(%a, %a, %a)" print_sec_opel_parametre_nom_secu pns print_sec_opel_parametre_nom_secu pns0 print_sec_opel_parametre_nom_secu pns1
| SOEAffReadContext -> Format.fprintf fmt "Read_Context"

let print_sec_opel_w fmt = function 
| SOWBool eb -> Format.fprintf fmt "w(%a)" print_sec_opel_expression_booleenne eb
| SOWNSecu pns -> Format.fprintf fmt "w(%a)" print_sec_opel_parametre_nom_secu pns

let print_sec_opel_expression_if fmt = function
| SOExpression_booleenne eb -> Format.fprintf fmt "%a" print_sec_opel_expression_booleenne eb
| SOIs_valid -> Format.fprintf fmt "is_valid"
| SOHard_input_valid -> Format.fprintf fmt "hard_input_valid"
| SOt pns -> Format.fprintf fmt "t(%a)" print_sec_opel_parametre_nom_secu pns

(* let print_function_call fmt = function *)


let rec print_instruction fmt = function
| INull -> Format.fprintf fmt "null ;"
| IPragma p -> Format.fprintf fmt "%a ;" print_pragma p
| IAssign (pns, eaff) -> Format.fprintf fmt "%a := %a;" print_sec_opel_parametre_nom_secu pns print_sec_opel_expression_d_affectation eaff
| IIf (eif, ins_s, elif_i) -> Format.fprintf fmt "if %a then@;<0 4>@[<v 0>%a @]@,%a@,end if;" print_sec_opel_expression_if eif print_instruction_s ins_s print_elif_instruction elif_i
| ICase (pns, case_ins) -> Format.fprintf fmt "case c(%a) is@;<0 4>@[<v 0>%a @]@,end case;" print_sec_opel_parametre_nom_secu pns print_case_instruction case_ins
| ILoop (sw, ins_s) -> Format.fprintf fmt "while %a loop@;<0 4>@[<v 0>%a @]@,end loop;" print_sec_opel_w sw print_instruction_s ins_s
| ICallDefinedFunction (pns, None) -> Format.fprintf fmt "%a;" print_sec_opel_parametre_nom_secu pns
| ICallDefinedFunction (pns, Some args) -> Format.fprintf fmt "%a%a;" print_sec_opel_parametre_nom_secu pns print_association_d_arguments args
| SOInstructionBeginLoop e -> Format.fprintf fmt "begin_loop(%a);" print_expression e
| SOInstructionConverge e -> Format.fprintf fmt "converge(%a);" print_expression e
| SOInstructionCompareM (pns, pns0, pns1, e) -> Format.fprintf fmt "compare_messages(%a, %a, %a, %a);" print_sec_opel_parametre_nom_secu pns print_sec_opel_parametre_nom_secu pns0 print_sec_opel_parametre_nom_secu pns1 print_expression e
| SOInstructionCompareR (pns, pns0, pns1, pns2, pns3, pns4, pns5, e) -> Format.fprintf fmt "compare_reintegration_array(%a, %a, %a, %a, %a, %a, %a, %a);" print_sec_opel_parametre_nom_secu pns print_sec_opel_parametre_nom_secu pns0 print_sec_opel_parametre_nom_secu pns1 print_sec_opel_parametre_nom_secu pns2 print_sec_opel_parametre_nom_secu pns3 print_sec_opel_parametre_nom_secu pns4 print_sec_opel_parametre_nom_secu pns5 print_expression e
| SOInstructionEndBranch e -> Format.fprintf fmt "end_branch(%a);" print_expression e
| SOInstructionEndInit -> Format.fprintf fmt "end_init;"
| SOInstructionEndIter -> Format.fprintf fmt "end_iteration;"
| SOInstructionFailAlarm -> Format.fprintf fmt "failure_alarm;"
| SOInstructionInit (pns, None) -> Format.fprintf fmt "init(%a);" print_sec_opel_parametre_nom_secu pns
| SOInstructionInit (pns, Some e) -> Format.fprintf fmt "init(%a, %a);" print_sec_opel_parametre_nom_secu pns print_expression e
| SOInstructionInitopel -> Format.fprintf fmt "initopel;"
| SOInstructionNewSafe (e, e0, e1) ->  Format.fprintf fmt "new_safe_context(%a, %a, %a);" print_expression e print_expression e0 print_expression e1
| SOInstructionNewSig ( pns, e, e0, e1) -> Format.fprintf fmt "new_signature(%a, %a, %a, %a);" print_sec_opel_parametre_nom_secu pns print_expression e print_expression e0 print_expression e1
| SOInstructionReadHardInput (pns, pns0, e, e0) ->  Format.fprintf fmt "read_hard_input(%a, %a, %a, %a);" print_sec_opel_parametre_nom_secu pns print_sec_opel_parametre_nom_secu pns0 print_expression e print_expression e0
| SOInstructionReadMess (pns, pns0, pns1, e, e0, e1, e2) -> Format.fprintf fmt "read_mess(%a, %a, %a, %a, %a, %a, %a);" print_sec_opel_parametre_nom_secu pns print_sec_opel_parametre_nom_secu pns0 print_sec_opel_parametre_nom_secu pns1 print_expression e print_expression e0 print_expression e1 print_expression e2
| SOInstructionReadMessUnfil (pns, pns0, pns1, e, e0, e1) -> Format.fprintf fmt " read_mess_unfiltered(%a, %a, %a, %a, %a, %a);" print_sec_opel_parametre_nom_secu pns print_sec_opel_parametre_nom_secu pns0 print_sec_opel_parametre_nom_secu pns1 print_expression e print_expression e0 print_expression e1
| SOInstructionWrite (pns, pns0, pns1, None) -> Format.fprintf fmt "write(%a, %a, %a);" print_sec_opel_parametre_nom_secu pns print_sec_opel_parametre_nom_secu pns0 print_sec_opel_parametre_nom_secu pns1
| SOInstructionWrite (pns, pns0, pns1, Some pns2) -> Format.fprintf fmt "write(%a, %a, %a, %a);" print_sec_opel_parametre_nom_secu pns print_sec_opel_parametre_nom_secu pns0 print_sec_opel_parametre_nom_secu pns1 print_sec_opel_parametre_nom_secu pns2
| SOInstructionWriteHardOutput (pns, pns0, e, e0) ->  Format.fprintf fmt "write_hard_output(%a, %a, %a, %a);" print_sec_opel_parametre_nom_secu pns print_sec_opel_parametre_nom_secu pns0 print_expression e print_expression e0
| SOInstructionWriteMess (pns, pns0, e, e0, e1) ->  Format.fprintf fmt "write_mess(%a, %a, %a, %a, %a);" print_sec_opel_parametre_nom_secu pns print_sec_opel_parametre_nom_secu pns0 print_expression e print_expression e0 print_expression e1
| SOInstructionWriteOutvar (pns, pns0, e, e0, e1) ->  Format.fprintf fmt "write_outvar(%a, %a, %a, %a, %a);" print_sec_opel_parametre_nom_secu pns print_sec_opel_parametre_nom_secu pns0 print_expression e print_expression e0 print_expression e1
| SOInstructionWriteRedund (pns, pns0, e, e0, e1) ->  Format.fprintf fmt "write_redund_array(%a, %a, %a, %a, %a);" print_sec_opel_parametre_nom_secu pns print_sec_opel_parametre_nom_secu pns0 print_expression e print_expression e0 print_expression e1

and print_elif_instruction fmt = function
| IElse ins_s -> Format.fprintf fmt "else@;<0 4>@[<v 0>%a@]" print_instruction_s ins_s 
| IElif (eif, ins_s, elif_i) -> Format.fprintf fmt "elsif %a then@;<0 4>@[<v 0>%a @]@,%a" print_sec_opel_expression_if eif print_instruction_s ins_s print_elif_instruction elif_i

and print_instruction_s fmt = function
| ISeqEnd i -> Format.fprintf fmt "%a" print_instruction i
| ISeqCons (i, is) -> Format.fprintf fmt "%a @,%a" print_instruction i print_instruction_s is

and print_case_instruction fmt = function
| ICaseFinishDefault is -> Format.fprintf fmt "when others =>@;<0 4>@[<v 0>%a@]" print_instruction_s is
| ICaseCons ( (e, is), case_s ) -> Format.fprintf fmt "when %a =>@;<0 4>@[<v 0>%a @]@,%a" print_expression e print_instruction_s is print_case_instruction case_s
(*| ICaseCons ( (None, is), case_s ) -> Format.fprintf fmt "when others =>@;<0 4>@[<v 0>%a @]@,%a" print_instruction_s is print_case_instruction case_s*)




let rec print_attrs_article fmt = function
| [] -> ()
| (idl, ty)::[] -> Format.fprintf fmt "%s : %a;" (String.of_seq (List.to_seq idl)) print_nom ty
| (idl, ty)::t -> Format.fprintf fmt "%s : %a; @,%a" (String.of_seq (List.to_seq idl)) print_nom ty print_attrs_article t

let rec print_name_list_for_type_tab fmt = function
| [] -> ()
| h::[] -> Format.fprintf fmt "%a" print_nom h
| h::t -> Format.fprintf fmt "%a, %a" print_nom h print_name_list_for_type_tab t

let print_mode fmt = function
| In -> Format.fprintf fmt "in"
| InOut -> Format.fprintf fmt "in out"
| Out -> Format.fprintf fmt "out"

let rec print_args_for_sub_prog fmt = function
| [] -> ()
| ((idl, mode), ty)::[] -> Format.fprintf fmt "%s : %a %a" (String.of_seq (List.to_seq idl)) print_mode mode print_nom ty
| ((idl, mode), ty)::t -> Format.fprintf fmt "%s : %a %a; %a" (String.of_seq (List.to_seq idl)) print_mode mode print_nom ty print_args_for_sub_prog t

let print_type_designator fmt = function 
| TypeSimple n -> Format.fprintf fmt "%a" print_nom n
| TypeComposeArray1 (n, e) -> Format.fprintf fmt "%a (%a)" print_nom n print_expression e
| TypeComposeArray2 (n, e0, e1) -> Format.fprintf fmt "%a (%a, %a)" print_nom n print_expression e0 print_expression e1

let print_obj fmt = function 
| ConstantVar(id, ty, e) -> Format.fprintf fmt "%s: constant %a := %a" (String.of_seq (List.to_seq id)) print_type_designator ty print_expression e
| VariableVar(id, ty) -> Format.fprintf fmt "%s: %a" (String.of_seq (List.to_seq id)) print_type_designator ty


let print_declaration_de_base fmt = function
| Declaration_d_objet o -> Format.fprintf fmt "%a ;" print_obj o
| Definition_de_type_Article (id, attrs) -> Format.fprintf fmt "type %s is record@;<0 4>@[<v 0>%a @]@,end record ;" (String.of_seq (List.to_seq id)) print_attrs_article attrs
| Declaration_de_sous_type (id, ty) -> Format.fprintf fmt "subtype %s is %a ;" (String.of_seq (List.to_seq id)) print_type_designator ty
| Declaration_de_surnom (id, args, n) -> 
    Format.fprintf fmt "procedure %s%a renames %a;" (String.of_seq (List.to_seq id))
    (fun fmt args -> (match args with | [] -> () | _ -> Format.fprintf fmt " (%a)" print_args_for_sub_prog args )) args print_nom n
| Declaration_de_sous_programme (id, args) -> Format.fprintf fmt "procedure %s%a ;" (String.of_seq (List.to_seq id)) (fun fmt args -> (match args with | [] -> () | _ -> Format.fprintf fmt " (%a)" print_args_for_sub_prog args )) args
(* | Declaration_de_nombre (id, e) -> Format.fprintf fmt "%s : constant := %a ;"  id print_expression e *)
| Use_Clause -> Format.fprintf fmt "use SEC_OPEL ;"
| Pragma_Declaration p -> Format.fprintf fmt "%a ;" print_pragma p

let rec print_decl_list fmt = function
| [] -> ()
| (LocalDecVar h)::[] -> Format.fprintf fmt "%a ;" print_obj h
| (LocalDecPragma h)::[] -> Format.fprintf fmt "%a ;" print_pragma h
| (LocalDecVar h)::t -> Format.fprintf fmt "%a ;@;%a" print_obj h print_decl_list t
| (LocalDecPragma h)::t -> Format.fprintf fmt "%a ;@;%a" print_pragma h print_decl_list t

let print_element_declararif fmt = function
{
	f_name = id;
	f_args = args;
	f_local_decs = decls_list ;
	f_instrs = ins_s;
} -> (
        match decls_list with
        | [] -> Format.fprintf fmt "@,procedure %s%a is begin@;<0 4>@[<v 0>%a @]@,end %s;@," (String.of_seq (List.to_seq id)) (fun fmt args -> (match args with | [] -> () | _ -> Format.fprintf fmt " (%a)" print_args_for_sub_prog args )) args print_instruction_s ins_s (String.of_seq (List.to_seq id))
        | _ -> Format.fprintf fmt "@,procedure %s%a is@;<0 4>@[<v 0>%a @]@,begin@;<0 4>@[<v 0>%a @]@,end %s;@," (String.of_seq (List.to_seq id)) (fun fmt args -> (match args with | [] -> () | _ -> Format.fprintf fmt " (%a)" print_args_for_sub_prog args )) args print_decl_list decls_list print_instruction_s ins_s (String.of_seq (List.to_seq id))
    )




let rec print_declsbase_list fmt = function
| [] -> ()
| h::t -> Format.fprintf fmt "%a @,%a" print_declaration_de_base h print_declsbase_list t

let print_declaration_1_body fmt = function
| Body_Declaration_d_objet o -> Format.fprintf fmt "%a ;" print_obj o
| Body_Definition_de_type_Article (id, attrs) -> Format.fprintf fmt "type %s is record@;<0 4>@[<v 0>%a @]@,end record ;" (String.of_seq (List.to_seq id)) print_attrs_article attrs
| Body_Declaration_de_sous_type (id, ty) -> Format.fprintf fmt "subtype %s is %a ;" (String.of_seq (List.to_seq id)) print_type_designator ty
(* | Body_Declaration_de_nombre (id, e) -> Format.fprintf fmt "%s : constant := %a ;"  id print_expression e *)
| Body_Use_Clause -> Format.fprintf fmt "use SEC_OPEL ;"
| Body_Pragma_Declaration p -> Format.fprintf fmt "%a ;" print_pragma p

let print_declaration_2_body fmt = function
| Fun_def e -> print_element_declararif fmt e
| Fun_dec (id, args) -> Format.fprintf fmt "procedure %s%a ;" (String.of_seq (List.to_seq id)) (fun fmt args -> (match args with | [] -> () | _ -> Format.fprintf fmt " (%a)" print_args_for_sub_prog args )) args
| Fun_renames (id, args, n) -> Format.fprintf fmt "procedure %s%a renames %a;" (String.of_seq (List.to_seq id)) (fun fmt args -> (match args with | [] -> () | _ -> Format.fprintf fmt " (%a)" print_args_for_sub_prog args )) args print_nom n

let rec print_declaration_1_body_list fmt = function
| [] -> ()
| h::[] -> Format.fprintf fmt "%a" print_declaration_1_body h
| h::t -> Format.fprintf fmt "%a @,%a" print_declaration_1_body h print_declaration_1_body_list t

let rec print_declaration_2_body_list fmt = function
| [] -> ()
| h::[] -> Format.fprintf fmt "%a" print_declaration_2_body h
| h::t -> Format.fprintf fmt "%a @,%a" print_declaration_2_body h print_declaration_2_body_list t


let print_unite fmt = function
| Compilation_Body dec -> Format.fprintf fmt "%a" print_element_declararif dec
| Compilation_PkgDecl (id, decllist) -> 
	Format.fprintf fmt "package %s is@;<0 4>@[<v 0>%a @]@,end %s;" (String.of_seq (List.to_seq id)) print_declsbase_list decllist (String.of_seq (List.to_seq id))
| Compilation_PkgBody (id, declslist, declslist2) -> Format.fprintf fmt "package body %s is@;<0 4>@[<v 0>%a@]@;<0 4>@[<v 0>%a @]@,end %s;" (String.of_seq (List.to_seq id)) print_declaration_1_body_list declslist print_declaration_2_body_list declslist2 (String.of_seq (List.to_seq id))

let rec print_with fmt = function
| [] -> ()
| h::t -> Format.fprintf fmt "with %s; @,%a" (String.of_seq (List.to_seq h)) print_with t

let print_compilation fmt = function
| (idlist, true), unite -> Format.fprintf fmt "@[<v 0>%a @,use SEC_OPEL; @,%a@]@." print_with idlist print_unite unite
| (idlist, false), unite -> Format.fprintf fmt "@[<v 0>%a @,%a@]@." print_with idlist print_unite unite



