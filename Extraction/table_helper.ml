open Ast
open Scanf

let rec find_table_arg = function
| [] -> false
| (_, _, [ty])::t -> (String.lowercase_ascii (String.of_seq (List.to_seq ty)) = "table") || find_table_arg t
| (_, _, [_; ty])::t -> (String.lowercase_ascii (String.of_seq (List.to_seq ty)) = "table") || find_table_arg t
| _::t -> find_table_arg t

let rec analyse_args_for_table_exp = function
| EVar [n] -> (String.uppercase_ascii (String.of_seq (List.to_seq n)) = "M") || (try Scanf.sscanf (String.uppercase_ascii (String.of_seq (List.to_seq n))) "LT%d" (fun _ -> true) with e -> false) || (try Scanf.sscanf (String.uppercase_ascii (String.of_seq (List.to_seq n))) "T%d" (fun _ -> true) with e -> false)
| EVar _ -> false
| ELit _ -> false
| Ebinary (_, e, e0) -> (analyse_args_for_table_exp e) || (analyse_args_for_table_exp e0)
| Eunary (_, e) -> analyse_args_for_table_exp e

let rec analyse_args_for_table_list = function
| [] -> false
| (Exp e)::t -> (analyse_args_for_table_exp e) || (analyse_args_for_table_list t)
| (StringLit _)::t -> analyse_args_for_table_list t 

let analyse_args_for_table = function
| None -> false
| Some args -> analyse_args_for_table_list args

let rec instruction_contain_table = function
| INull -> false
| IPragma _ -> false
| IAssign _ -> false
| IIf (_, if_ins_s, els_instrs) -> (instruction_s_contain_table if_ins_s) || (elif_instruction_contain_table els_instrs)
| ICase (_, case_instrs) -> case_instruction_contain_table case_instrs
| ILoop (_, instr_s) -> instruction_s_contain_table instr_s
| ICallDefinedFunction (id, args) -> analyse_args_for_table args
| SOInstructionBeginLoop _ -> true
| SOInstructionConverge _ -> true
| SOInstructionCompareM _ -> true
| SOInstructionCompareR _ -> true
| SOInstructionEndBranch _ -> false
| SOInstructionEndInit -> false
| SOInstructionEndIter -> false
| SOInstructionFailAlarm -> false
| SOInstructionInit _ -> false
| SOInstructionInitopel -> false
| SOInstructionNewSafe _ -> true
| SOInstructionNewSig _ -> true
| SOInstructionReadHardInput _ -> true
| SOInstructionReadMess _ -> true (*A voir: Id_Msg non duplicable*)
| SOInstructionReadMessUnfil _ -> true (*A voir: Id_Msg non duplicable*)
| SOInstructionWrite _ -> false
| SOInstructionWriteHardOutput _ -> true
| SOInstructionWriteMess _ -> true (*A voir: Id_Msg non duplicable*)
| SOInstructionWriteOutvar _ -> true
| SOInstructionWriteRedund _ -> true

and elif_instruction_contain_table = function
| IElse ins_s -> instruction_s_contain_table ins_s
| IElif (_, ins_s, elif_ins) -> (instruction_s_contain_table ins_s) || (elif_instruction_contain_table elif_ins)

and instruction_s_contain_table = function
| ISeqEnd i -> instruction_contain_table i
| ISeqCons (i, is) -> (instruction_contain_table i) || (instruction_s_contain_table is)

and case_instruction_contain_table = function
| ICaseFinishDefault i -> instruction_s_contain_table i
| ICaseCons (( _, is ), cs) -> (instruction_s_contain_table is) || (case_instruction_contain_table cs)


let rec needs_converge = function
| ISeqEnd(IIf(_, _, _)) -> true
| ISeqEnd(ICase(_, _)) -> true
| ISeqEnd(_) -> false
| ISeqCons(h, t) -> needs_converge t


(********* Experimental ******)
(** Pour une function, on doit connaitre ses variables locales, si elle fait appel à un converge ou read_mess **)
(** Pour une liste d'instructions, il faut savoir que pour les variables non compensable, une modification dans une branche empeche son utilisation sans ré-affectation**)

(** Data struct **)
(** <pkg-name . fname> -> sig **)
(** <pkg-name . fname> -> list of local vars **)
(** <pkg-name . fname> -> bool (true for non-duplicable, false for duplicable) **)

let f2Sig = Hashtbl.create 200
let f2LocVars = Hashtbl.create 200
let f2NonDulplicable = Hashtbl.create 200

let rec instruction_non_duplic = function
| INull -> false
| IPragma _ -> false
| IAssign _ -> false
| IIf (_, if_ins_s, els_instrs) -> (instruction_s_non_duplic if_ins_s) || (elif_instruction_non_duplic els_instrs)
| ICase (_, case_instrs) -> case_instruction_non_duplic case_instrs
| ILoop (_, instr_s) -> instruction_s_non_duplic instr_s
| ICallDefinedFunction (id, args) -> 
    ( match Hashtbl.find_opt f2NonDulplicable (String.concat "." (List.map (fun x -> (String.of_seq (List.to_seq x))) id)) with
      | None -> (** Verifier si cela arrive **) 
            (
                (analyse_args_for_table args)
            )
      | Some b -> b
    )
| SOInstructionBeginLoop _ -> true
| SOInstructionConverge _ -> true
| SOInstructionCompareM _ -> true
| SOInstructionCompareR _ -> true
| SOInstructionEndBranch _ -> false
| SOInstructionEndInit -> false
| SOInstructionEndIter -> false
| SOInstructionFailAlarm -> false
| SOInstructionInit _ -> false
| SOInstructionInitopel -> false
| SOInstructionNewSafe _ -> true
| SOInstructionNewSig _ -> true
| SOInstructionReadHardInput _ -> true
| SOInstructionReadMess _ -> true (*A voir: Id_Msg non duplicable*)
| SOInstructionReadMessUnfil _ -> true (*A voir: Id_Msg non duplicable*)
| SOInstructionWrite _ -> false
| SOInstructionWriteHardOutput _ -> true
| SOInstructionWriteMess _ -> true (*A voir: Id_Msg non duplicable*)
| SOInstructionWriteOutvar _ -> true
| SOInstructionWriteRedund _ -> true

and elif_instruction_non_duplic = function
| IElse ins_s -> instruction_s_non_duplic ins_s
| IElif (_, ins_s, elif_ins) -> (instruction_s_non_duplic ins_s) || (elif_instruction_non_duplic elif_ins)

and instruction_s_non_duplic = function
| ISeqEnd i -> instruction_non_duplic i
| ISeqCons (i, is) -> (instruction_non_duplic i) || (instruction_s_non_duplic is)

and case_instruction_non_duplic = function
| ICaseFinishDefault i -> instruction_s_non_duplic i
| ICaseCons (( _, is ), cs) -> (instruction_s_non_duplic is) || (case_instruction_non_duplic cs)

module LVarSetModule = Set.Make(String)


let add_sc_to_set lv_set acc = function
| [vid] -> if LVarSetModule.mem (String.of_seq (List.to_seq vid)) lv_set then LVarSetModule.add (String.of_seq (List.to_seq vid)) acc else acc
| _ -> acc

let rm_sc_to_set lv_set acc = function
| [vid] -> LVarSetModule.remove (String.of_seq (List.to_seq vid)) acc
| _ -> acc

let rec last_modif_in_branchInstruction pkg_name cands in_branch acc = function
| IAssign (sc, _) -> if in_branch then add_sc_to_set cands acc sc else rm_sc_to_set cands acc sc
| IIf (_, iifi, iel) ->
    last_modif_in_branchElif_instruction pkg_name cands (last_modif_in_branchInstruction_s pkg_name cands true acc iifi) iel
| ICase (_, c) -> last_modif_in_branchCase_instruction pkg_name cands acc c
| ILoop (_, c) -> last_modif_in_branchInstruction_s pkg_name cands true acc c
| ICallDefinedFunction (f_name, o) ->
    let fid = String.concat "." (match f_name with | [id] -> (pkg_name @ [(String.of_seq (List.to_seq id))]) | _ -> (List.map (fun x -> (String.of_seq (List.to_seq x))) f_name)) in
    List.fold_right (if in_branch then LVarSetModule.add else LVarSetModule.remove)
        (match o with
            | Some p ->
             (
                match Hashtbl.find_opt f2Sig fid with
                | None -> (** Vérifier si cela arrive **) List.filter_map (fun p -> match p with | Exp (EVar [vid]) -> if (LVarSetModule.mem (String.of_seq (List.to_seq vid)) cands) then Some (String.of_seq (List.to_seq vid)) else None | _ -> None) p
                | Some s -> List.filter_map (fun (e, m) -> match e, m with | Exp (EVar [vid]), InOut | Exp (EVar [vid]), Out -> if (LVarSetModule.mem (String.of_seq (List.to_seq vid)) cands) then Some (String.of_seq (List.to_seq vid)) else None | _ -> None)
                    (List.combine p s)
             )
            | None -> []
        ) acc
| SOInstructionCompareM (sc, _, _, _) -> if in_branch then add_sc_to_set cands acc sc else rm_sc_to_set cands acc sc
| SOInstructionCompareR (sc, _, _, _, _, _, _, _) -> if in_branch then add_sc_to_set cands acc sc else rm_sc_to_set cands acc sc
| SOInstructionInit (sc, o) -> if in_branch then add_sc_to_set cands acc sc else rm_sc_to_set cands acc sc
| SOInstructionReadHardInput (sc, _, _, _) -> if in_branch then add_sc_to_set cands acc sc else rm_sc_to_set cands acc sc
| SOInstructionReadMess (t0, valide, _, _, _, _, _) -> if in_branch then add_sc_to_set cands (add_sc_to_set cands acc t0) valide else rm_sc_to_set cands (rm_sc_to_set cands acc t0) valide
| SOInstructionReadMessUnfil (t0, valide, _, _, _, _) ->  if in_branch then add_sc_to_set cands (add_sc_to_set cands acc t0) valide else rm_sc_to_set cands (rm_sc_to_set cands acc t0) valide
| SOInstructionWrite (t0, _, _, _) ->  if in_branch then add_sc_to_set cands acc t0 else rm_sc_to_set cands acc t0
| SOInstructionWriteHardOutput (t0, _, _, _) ->  if in_branch then add_sc_to_set cands acc t0 else rm_sc_to_set cands acc t0
| SOInstructionWriteMess (t0, _, _, _, _) -> if in_branch then add_sc_to_set cands acc t0 else rm_sc_to_set cands acc t0
| SOInstructionWriteOutvar (t0, _, _, _, _) -> if in_branch then add_sc_to_set cands acc t0 else rm_sc_to_set cands acc t0
| SOInstructionWriteRedund (t0, _, _, _, _) -> if in_branch then add_sc_to_set cands acc t0 else rm_sc_to_set cands acc t0
| _ -> acc

and last_modif_in_branchElif_instruction pkg_name cands acc = function
| IElse e -> last_modif_in_branchInstruction_s pkg_name cands true acc e
| IElif (_, iifi, iel) ->
    last_modif_in_branchElif_instruction pkg_name cands (last_modif_in_branchInstruction_s pkg_name cands true acc iifi) iel

and last_modif_in_branchCase_instruction pkg_name cands acc = function
| ICaseFinishDefault e -> last_modif_in_branchInstruction_s pkg_name cands true acc e
| ICaseCons (p, iel) ->
  let (_, iifi) = p in
  last_modif_in_branchCase_instruction pkg_name cands (last_modif_in_branchInstruction_s pkg_name cands true acc iifi) iel

and last_modif_in_branchInstruction_s pkg_name cands in_branch acc = function
| ISeqEnd e -> last_modif_in_branchInstruction pkg_name cands in_branch acc e
| ISeqCons (h, t0) -> last_modif_in_branchInstruction_s pkg_name cands in_branch (last_modif_in_branchInstruction pkg_name cands in_branch acc h) t0




let analyse_ed pkg_name = function
| { f_name = fname;
    f_args = fsig;
    f_local_decs = decs;
    f_instrs = insts } -> 
        let ident = (String.concat "." (pkg_name @ [(String.of_seq (List.to_seq fname))])) in 
        Hashtbl.add f2Sig ident (List.map (fun ((i, m), t) -> m) fsig);
        let param_out = List.filter_map (fun ((x, m), _) -> match m with | InOut | Out -> Some (String.of_seq (List.to_seq x)) | _ -> None ) fsig in
        let param_out_set = List.fold_right LVarSetModule.add param_out LVarSetModule.empty in
        let compensables = last_modif_in_branchInstruction_s pkg_name param_out_set false LVarSetModule.empty insts in        
        Hashtbl.add f2LocVars ident 
            (
                LVarSetModule.filter (fun x -> not (LVarSetModule.mem x compensables)) param_out_set, 
                List.fold_right LVarSetModule.add (List.filter_map (fun x -> match x with | LocalDecVar (VariableVar(i, _)) -> Some (String.of_seq (List.to_seq i)) | _ -> None) decs) LVarSetModule.empty
            );
        Hashtbl.add f2NonDulplicable ident ((instruction_s_non_duplic insts) (*|| ((find_table_arg fsig_))*) )
        
let analyse_d pkg_name = function
| Declaration_de_sous_programme(fname, fsig) -> 
    let ident = (String.concat "." (pkg_name @ [(String.of_seq (List.to_seq fname))])) in 
    Hashtbl.add f2Sig ident (List.map (fun ((i, m), t) -> m) fsig)
| Declaration_de_surnom (fname, fsig, rname) -> 
    let ident = (String.concat "." (pkg_name @ [(String.of_seq (List.to_seq fname))])) in 
    let fsig_ = List.map (fun ((i, m), t) -> (i, m, t)) fsig in
    Hashtbl.add f2Sig ident (List.map (fun ((i, m), t) -> m) fsig);
    Hashtbl.add f2NonDulplicable ident 
    ( match Hashtbl.find_opt f2NonDulplicable (String.concat "." (match rname with | [id] -> (pkg_name @ [(String.of_seq (List.to_seq id))]) | _ -> List.map (fun x -> (String.of_seq (List.to_seq x))) rname)) with
      | None -> (** Verifier si cela arrive **) 
            (
                (find_table_arg fsig_)
            )
      | Some b -> b
    )
| _ -> ()
    
let analyse_d2 pkg_name = function
| Fun_def ed -> analyse_ed pkg_name ed
| Fun_dec (fname, fsig) -> 
    let ident = (String.concat "." (pkg_name @ [(String.of_seq (List.to_seq fname))])) in 
    Hashtbl.add f2Sig ident (List.map (fun ((i, m), t) -> m) fsig)
| Fun_renames (fname, fsig, rname) -> 
    let ident = (String.concat "." (pkg_name @ [(String.of_seq (List.to_seq fname))])) in 
    let fsig_ = List.map (fun ((i, m), t) -> (i, m, t)) fsig in
    Hashtbl.add f2Sig ident (List.map (fun ((i, m), t) -> m) fsig);
    Hashtbl.add f2NonDulplicable ident 
    ( match Hashtbl.find_opt f2NonDulplicable (String.concat "." (match rname with | [id] -> (pkg_name @ [(String.of_seq (List.to_seq id))]) | _ -> List.map (fun x -> (String.of_seq (List.to_seq x))) rname)) with
      | None -> (** Verifier si cela arrive **) 
            (
                (find_table_arg fsig_)
            )
      | Some b -> b
    )

let analyse_unit = function
| Compilation_Body ed -> analyse_ed [] ed
| Compilation_PkgDecl (pkg_name, ds) -> List.iter (analyse_d [(String.of_seq (List.to_seq pkg_name))]) ds
| Compilation_PkgBody (pkg_name, ds1, ds2) -> List.iter (analyse_d2 [(String.of_seq (List.to_seq pkg_name))]) ds2

let analyse_ast a = analyse_unit (snd a)


let rec instruction_non_duplic_query pkg_name = function
| INull -> false
| IPragma _ -> false
| IAssign _ -> false
| IIf (_, if_ins_s, els_instrs) -> (instruction_s_non_duplic_query pkg_name if_ins_s) || (elif_instruction_non_duplic_query pkg_name els_instrs)
| ICase (_, case_instrs) -> case_instruction_non_duplic_query pkg_name case_instrs
| ILoop (_, instr_s) -> instruction_s_non_duplic_query pkg_name instr_s
| ICallDefinedFunction ([id], args) -> 
    ( match Hashtbl.find_opt f2NonDulplicable (String.concat "." (pkg_name @ [(String.of_seq (List.to_seq id))])) with
      | None -> (** Verifier si cela arrive **) 
            (
                (analyse_args_for_table args) 
            )
      | Some b -> b
    )
| ICallDefinedFunction (id, args) -> 
    ( match Hashtbl.find_opt f2NonDulplicable (String.concat "." (List.map (fun x -> (String.of_seq (List.to_seq x))) id)) with
      | None -> (** Verifier si cela arrive **) 
            (
                (analyse_args_for_table args) 
            )
      | Some b -> b
    )
| SOInstructionBeginLoop _ -> true
| SOInstructionConverge _ -> true
| SOInstructionCompareM _ -> true
| SOInstructionCompareR _ -> true
| SOInstructionEndBranch _ -> false
| SOInstructionEndInit -> false
| SOInstructionEndIter -> false
| SOInstructionFailAlarm -> false
| SOInstructionInit _ -> false
| SOInstructionInitopel -> false
| SOInstructionNewSafe _ -> true
| SOInstructionNewSig _ -> true
| SOInstructionReadHardInput _ -> true
| SOInstructionReadMess _ -> true (*A voir: Id_Msg non duplicable*)
| SOInstructionReadMessUnfil _ -> true (*A voir: Id_Msg non duplicable*)
| SOInstructionWrite _ -> false
| SOInstructionWriteHardOutput _ -> true
| SOInstructionWriteMess _ -> true (*A voir: Id_Msg non duplicable*)
| SOInstructionWriteOutvar _ -> true
| SOInstructionWriteRedund _ -> true

and elif_instruction_non_duplic_query pkg_name = function
| IElse ins_s -> instruction_s_non_duplic_query pkg_name ins_s
| IElif (_, ins_s, elif_ins) -> (instruction_s_non_duplic_query pkg_name ins_s) || (elif_instruction_non_duplic_query pkg_name elif_ins)

and instruction_s_non_duplic_query pkg_name = function
| ISeqEnd i -> instruction_non_duplic_query pkg_name i
| ISeqCons (i, is) -> (instruction_non_duplic_query pkg_name i) || (instruction_s_non_duplic_query pkg_name is)

and case_instruction_non_duplic_query pkg_name = function
| ICaseFinishDefault i -> instruction_s_non_duplic_query pkg_name i
| ICaseCons (( _, is ), cs) -> (instruction_s_non_duplic_query pkg_name is) || (case_instruction_non_duplic_query pkg_name cs)


(** Variable compensable **)

(** Modified variables **)

let rec modified_varsInstruction pkg_name lv_set acc = function
| IAssign (sc, _) -> add_sc_to_set lv_set acc sc
| IIf (_, iifi, iel) ->
    modified_varsElif_instruction pkg_name lv_set (modified_varsInstruction_s pkg_name lv_set acc iifi) iel
| ICase (_, c) -> modified_varsCase_instruction pkg_name lv_set acc c
| ILoop (_, c) -> modified_varsInstruction_s pkg_name lv_set acc c
| ICallDefinedFunction (f_name, o) ->
    let fid = String.concat "." (match f_name with | [id] -> (pkg_name @ [(String.of_seq (List.to_seq id))]) | _ -> List.map (fun x -> (String.of_seq (List.to_seq x))) f_name) in
    List.fold_right LVarSetModule.add
        (match o with
            | Some p ->
             (
                match Hashtbl.find_opt f2Sig fid with
                | None -> (** Vérifier si cela arrive **) List.filter_map (fun p -> match p with | Exp (EVar [vid]) -> if LVarSetModule.mem (String.of_seq (List.to_seq vid)) lv_set then Some (String.of_seq (List.to_seq vid)) else None | _ -> None) p
                | Some s -> List.filter_map (fun (e, m) -> match e, m with | Exp (EVar [vid]), InOut | Exp (EVar [vid]), Out -> if LVarSetModule.mem (String.of_seq (List.to_seq vid)) lv_set then Some (String.of_seq (List.to_seq vid)) else None | _ -> None)
                    (List.combine p s)
             )
            | None -> []
        ) acc
| SOInstructionCompareM (sc, _, _, _) -> add_sc_to_set lv_set acc sc
| SOInstructionCompareR (sc, _, _, _, _, _, _, _) -> add_sc_to_set lv_set acc sc
| SOInstructionInit (sc, o) -> add_sc_to_set lv_set acc sc
| SOInstructionReadHardInput (t0, _, _, _) -> add_sc_to_set lv_set acc t0
| SOInstructionReadMess (t0, valide, _, _, _, _, _) -> add_sc_to_set lv_set (add_sc_to_set lv_set acc t0) valide
| SOInstructionReadMessUnfil (t0, valide, _, _, _, _) ->  add_sc_to_set lv_set (add_sc_to_set lv_set acc t0) valide
| SOInstructionWrite (t0, _, _, _) ->  add_sc_to_set lv_set acc t0
| SOInstructionWriteHardOutput (t0, _, _, _) ->  add_sc_to_set lv_set acc t0
| SOInstructionWriteMess (t0, _, _, _, _) -> add_sc_to_set lv_set acc t0
| SOInstructionWriteOutvar (t0, _, _, _, _) -> add_sc_to_set lv_set acc t0
| SOInstructionWriteRedund (t0, _, _, _, _) -> add_sc_to_set lv_set acc t0
| _ -> acc

and modified_varsElif_instruction pkg_name lv_set acc = function
| IElse e -> modified_varsInstruction_s pkg_name lv_set acc e
| IElif (_, iifi, iel) ->
    modified_varsElif_instruction pkg_name lv_set (modified_varsInstruction_s pkg_name lv_set acc iifi) iel

and modified_varsCase_instruction pkg_name lv_set acc = function
| ICaseFinishDefault e -> modified_varsInstruction_s pkg_name lv_set acc e
| ICaseCons (p, iel) ->
  let (_, iifi) = p in
  modified_varsCase_instruction pkg_name lv_set (modified_varsInstruction_s pkg_name lv_set acc iifi) iel

and modified_varsInstruction_s pkg_name lv_set acc = function
| ISeqEnd e -> modified_varsInstruction pkg_name lv_set acc e
| ISeqCons (h, t0) -> modified_varsInstruction_s pkg_name lv_set (modified_varsInstruction pkg_name lv_set acc h) t0
  



(** Read **)
let rec dangerous_read_in_expression forbidden_set = function
| EVar [vid] -> LVarSetModule.mem (String.of_seq (List.to_seq vid)) forbidden_set
| Ebinary(_, e0, e1) -> (dangerous_read_in_expression forbidden_set e0) || (dangerous_read_in_expression forbidden_set e1)
| Eunary(_, e) -> dangerous_read_in_expression forbidden_set e
| _ -> false

let dangerous_read_in_expression_booleenne forbidden_set = function
| SOEBNot [vid] -> LVarSetModule.mem (String.of_seq (List.to_seq vid)) forbidden_set
| SOEBBinaire (_, [vid], SEC_OPEL_Var [vid0]) -> 
    (LVarSetModule.mem (String.of_seq (List.to_seq vid)) forbidden_set) || (LVarSetModule.mem (String.of_seq (List.to_seq vid0)) forbidden_set)
| SOEBBinaire (_, [vid], _) -> (LVarSetModule.mem (String.of_seq (List.to_seq vid)) forbidden_set)
| SOEBBinaire (_, _, SEC_OPEL_Var [vid]) -> (LVarSetModule.mem (String.of_seq (List.to_seq vid)) forbidden_set)
| _ -> false

let dangerous_read_in_expression_d_affectation forbidden_set = function
| SOEAffExpUMinus [vid] -> (LVarSetModule.mem (String.of_seq (List.to_seq vid)) forbidden_set)
| SOEAffExpBinary (_, SEC_OPEL_Var [vid], SEC_OPEL_Var [vid0]) -> 
    (LVarSetModule.mem (String.of_seq (List.to_seq vid)) forbidden_set) || (LVarSetModule.mem (String.of_seq (List.to_seq vid0)) forbidden_set)
| SOEAffExpBinary (_, SEC_OPEL_Var [vid], _) -> (LVarSetModule.mem (String.of_seq (List.to_seq vid)) forbidden_set)
| SOEAffExpBinary (_, _, SEC_OPEL_Var [vid]) -> (LVarSetModule.mem (String.of_seq (List.to_seq vid)) forbidden_set)
| SOEAffDiv2([vid], e) -> (LVarSetModule.mem (String.of_seq (List.to_seq vid)) forbidden_set) || (dangerous_read_in_expression forbidden_set e)
| SOEAffDiv2(_, e) -> (dangerous_read_in_expression forbidden_set e)
| SOEAffBool b ->  dangerous_read_in_expression_booleenne forbidden_set b
| SOEAffAssign (SEC_OPEL_Var [vid]) -> (LVarSetModule.mem (String.of_seq (List.to_seq vid)) forbidden_set)
| SOEAffEncode e -> (dangerous_read_in_expression forbidden_set e)

| SOEAffBuildTag ([vid], Some([vid0], Some [vid1])) ->
    (LVarSetModule.mem (String.of_seq (List.to_seq vid)) forbidden_set) || (LVarSetModule.mem (String.of_seq (List.to_seq vid0)) forbidden_set) || (LVarSetModule.mem (String.of_seq (List.to_seq vid1)) forbidden_set)
| SOEAffBuildTag ([vid], Some(_, Some [vid0])) ->
    (LVarSetModule.mem (String.of_seq (List.to_seq vid)) forbidden_set) || (LVarSetModule.mem (String.of_seq (List.to_seq vid0)) forbidden_set)
| SOEAffBuildTag ([vid], Some([vid0], _)) ->
    (LVarSetModule.mem (String.of_seq (List.to_seq vid)) forbidden_set) || (LVarSetModule.mem (String.of_seq (List.to_seq vid0)) forbidden_set)
| SOEAffBuildTag ([vid], _) -> (LVarSetModule.mem (String.of_seq (List.to_seq vid)) forbidden_set)
| SOEAffBuildTag (_, Some([vid], Some[vid0])) ->
    (LVarSetModule.mem (String.of_seq (List.to_seq vid)) forbidden_set) || (LVarSetModule.mem (String.of_seq (List.to_seq vid0)) forbidden_set)
| SOEAffBuildTag (_, Some([vid], _)) -> (LVarSetModule.mem (String.of_seq (List.to_seq vid)) forbidden_set)
| SOEAffBuildTag (_, Some(_, Some [vid])) -> (LVarSetModule.mem (String.of_seq (List.to_seq vid)) forbidden_set)

| SOEAffRead ([vid], Some([vid0], Some [vid1])) ->
    (LVarSetModule.mem (String.of_seq (List.to_seq vid)) forbidden_set) || (LVarSetModule.mem (String.of_seq (List.to_seq vid0)) forbidden_set) || (LVarSetModule.mem (String.of_seq (List.to_seq vid1)) forbidden_set)
| SOEAffRead ([vid], Some(_, Some [vid0])) ->
    (LVarSetModule.mem (String.of_seq (List.to_seq vid)) forbidden_set) || (LVarSetModule.mem (String.of_seq (List.to_seq vid0)) forbidden_set)
| SOEAffRead ([vid], Some([vid0], _)) ->
    (LVarSetModule.mem (String.of_seq (List.to_seq vid)) forbidden_set) || (LVarSetModule.mem (String.of_seq (List.to_seq vid0)) forbidden_set)
| SOEAffRead ([vid], _) -> (LVarSetModule.mem (String.of_seq (List.to_seq vid)) forbidden_set)
| SOEAffRead (_, Some([vid], Some[vid0])) ->
    (LVarSetModule.mem (String.of_seq (List.to_seq vid)) forbidden_set) || (LVarSetModule.mem (String.of_seq (List.to_seq vid0)) forbidden_set)
| SOEAffRead (_, Some([vid], _)) -> (LVarSetModule.mem (String.of_seq (List.to_seq vid)) forbidden_set)
| SOEAffRead (_, Some(_, Some [vid])) -> (LVarSetModule.mem (String.of_seq (List.to_seq vid)) forbidden_set)

| _ -> false


let dangerous_read_in_w forbidden_set = function
| SOWBool b ->  dangerous_read_in_expression_booleenne forbidden_set b
| SOWNSecu [vid] -> (LVarSetModule.mem (String.of_seq (List.to_seq vid)) forbidden_set)
| _ -> false


let dangerous_read_in_expression_if forbidden_set = function
| SOExpression_booleenne b -> dangerous_read_in_expression_booleenne forbidden_set b
| SOt [vid] -> (LVarSetModule.mem (String.of_seq (List.to_seq vid)) forbidden_set)
| _ -> false


let rec dangerous_read_Instruction forbidden_set pkg_name fun_name = function
| IAssign (vid, e) -> (dangerous_read_in_expression_d_affectation forbidden_set e)
| IIf (c, iifi, iel) ->
    (dangerous_read_in_expression_if forbidden_set c) || (dangerous_read_Instruction_s forbidden_set pkg_name fun_name iifi) || (dangerous_read_Elif_instruction forbidden_set pkg_name fun_name iel)
| ICase (sc, c) -> 
    (match sc with | [vid] -> LVarSetModule.mem (String.of_seq (List.to_seq vid)) forbidden_set | _ -> false) || (dangerous_read_Case_instruction forbidden_set pkg_name fun_name c)
| ILoop (w, c) -> 
    (dangerous_read_in_w forbidden_set w) || (dangerous_read_Instruction_s forbidden_set pkg_name fun_name c) 
| ICallDefinedFunction (f_name, o) ->
  let fid = String.concat "." (match f_name with | [id] -> (pkg_name @ [(String.of_seq (List.to_seq id))]) | _ -> List.map (fun x -> (String.of_seq (List.to_seq x))) f_name) in
  (match o with
   | Some p ->
     (
        match Hashtbl.find_opt f2Sig fid with
        | None -> (** Vérifier si cela arrive **) (List.exists (fun p -> match p with | Exp (EVar [pid]) -> LVarSetModule.mem (String.of_seq (List.to_seq pid)) forbidden_set | _ -> false) p)
        | Some s -> (List.exists2 (fun m p -> match m, p with | InOut, Exp (EVar [pid]) | In, Exp (EVar [pid]) -> LVarSetModule.mem (String.of_seq (List.to_seq pid)) forbidden_set | _ -> false) s p)
     )
   | None -> false
  )
| SOInstructionCompareM (_, p0, p1, p2) -> 
    List.exists (dangerous_read_in_expression forbidden_set) [EVar p0; EVar p1; p2]
| SOInstructionCompareR (_, p0, p1, p2, p3, p4, p5, p6) -> 
    List.exists (dangerous_read_in_expression forbidden_set) [EVar p0; EVar p1; EVar p2; EVar p3; EVar p4; EVar p5; p6]
| SOInstructionInit (_, Some e) -> dangerous_read_in_expression forbidden_set e
| SOInstructionReadHardInput(p0, p1, p2, p3) ->
    List.exists (dangerous_read_in_expression forbidden_set) [EVar p0; EVar p1; p2; p3]
| SOInstructionReadMess (t0, _, p0, p1, p2, p3, p4) ->
    List.exists (dangerous_read_in_expression forbidden_set) [EVar t0; EVar p0; p1; p2; p3; p4]
| SOInstructionReadMessUnfil (t0, _, p0, p1, p2, p3) ->
    List.exists (dangerous_read_in_expression forbidden_set) [EVar t0; EVar p0; p1; p2; p3]
| SOInstructionWrite (_, p0, p1, Some p2) -> 
    List.exists (dangerous_read_in_expression forbidden_set) [EVar p0; EVar p1; EVar p2]
| SOInstructionWrite (_, p0, p1, _) -> 
    List.exists (dangerous_read_in_expression forbidden_set) [EVar p0; EVar p1]
| SOInstructionWriteHardOutput (_, p0, p1, p2) -> 
    List.exists (dangerous_read_in_expression forbidden_set) [EVar p0; p1; p2]
| SOInstructionWriteMess (_, p0, p1, p2, p3) -> 
    List.exists (dangerous_read_in_expression forbidden_set) [EVar p0; p1; p2; p3]
| SOInstructionWriteOutvar (_, p0, p1, p2, p3) -> 
    List.exists (dangerous_read_in_expression forbidden_set) [EVar p0; p1; p2; p3]
| SOInstructionWriteRedund (_, p0, p1, p2, p3) -> 
    List.exists (dangerous_read_in_expression forbidden_set) [EVar p0; p1; p2; p3]
| _ -> false

and dangerous_read_Elif_instruction forbidden_set pkg_name fun_name = function
| IElse e -> dangerous_read_Instruction_s forbidden_set pkg_name fun_name e
| IElif (c, iifi, iel) ->
    (dangerous_read_in_expression_if forbidden_set c) || (dangerous_read_Instruction_s forbidden_set pkg_name fun_name iifi) || (dangerous_read_Elif_instruction forbidden_set pkg_name fun_name iel)


and dangerous_read_Case_instruction forbidden_set pkg_name fun_name = function
| ICaseFinishDefault e -> dangerous_read_Instruction_s forbidden_set pkg_name fun_name e
| ICaseCons (p, iel) ->
  let (c, iifi) = p in
  (dangerous_read_in_expression forbidden_set c) || (dangerous_read_Instruction_s forbidden_set pkg_name fun_name iifi) || (dangerous_read_Case_instruction forbidden_set pkg_name fun_name iel)


and dangerous_read_Instruction_s forbidden_set pkg_name fun_name = function
| ISeqEnd e -> dangerous_read_Instruction forbidden_set pkg_name fun_name e
| ISeqCons (h, t) ->
  (dangerous_read_Instruction forbidden_set pkg_name fun_name h) ||
  (
    let params_nc, local_nc = (
            match Hashtbl.find_opt f2LocVars (String.concat "." (pkg_name @ [fun_name])) with
            | None -> (** Vérifier si cela arrive **) LVarSetModule.empty, LVarSetModule.empty
            | Some lv -> lv
         ) in
    let forbidden_set_ = modified_varsInstruction pkg_name (LVarSetModule.union params_nc local_nc) LVarSetModule.empty h in
    match h with
    | IIf _ | ICase _ | ILoop _ -> 
        dangerous_read_Instruction_s (LVarSetModule.union forbidden_set_ forbidden_set) pkg_name fun_name t
    | _ -> dangerous_read_Instruction_s (LVarSetModule.diff forbidden_set forbidden_set_) pkg_name fun_name t
  )
  
let compensableProblemInstruction_s forbidden_set pkg_name fun_name is =
     (dangerous_read_Instruction_s forbidden_set pkg_name fun_name is) || 
     (
        let params_nc, local_nc = (
            match Hashtbl.find_opt f2LocVars (String.concat "." (pkg_name @ [fun_name])) with
            | None -> (** Vérifier si cela arrive **) LVarSetModule.empty, LVarSetModule.empty
            | Some lv -> lv
         ) in
         last_modif_in_branchInstruction_s pkg_name params_nc false LVarSetModule.empty is <> LVarSetModule.empty
     )
         
         




















