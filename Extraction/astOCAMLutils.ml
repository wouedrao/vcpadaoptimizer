open Ast
open Format
open Unix
open Sys
open Stdlib
open Filename
open Environment
open Semantics
open Semantics_f
open Pretty_printer
open Values

let is_correct_args_list_opt = function
	| None -> true
	| Some arg_list -> 
		(
			let rec is_positionnal_arg_list = function
				| [] -> true
				| h::t -> (
					match h with
						|None, _ -> is_positionnal_arg_list t
						|Some _, _ -> false
				) in
			let rec is_named_arg_list = function
				| [] -> true
				| h::t -> (
					match h with
						|Some _, _ -> is_named_arg_list t
						|None, _ -> false
				) in
			(is_positionnal_arg_list arg_list) || (is_named_arg_list arg_list)
		)
	  

(*
	Pretty_printer utils
*)	

let fprintf_cond print_elem elem fmt cond =
	if cond then
		Format.fprintf fmt "%a" print_elem elem
	else
		()

let fprintf_break_cond fmt cond = 
	fprintf_cond (Format.fprintf) "@;" fmt cond	


let fprintf_list ?box:(box="") ?opening:(opening="") ?sep:(sep="") ?closing:(closing="") ?break:(break=false) ?breakdelim:(breakdelim=true) print_elem fmt li = 
	let rec print_elem_list fmt = function
			| [] -> ()
			| x::[] -> Format.fprintf fmt "%a" print_elem x
			| x::y::l -> Format.fprintf fmt "%a%s%a%a" print_elem x sep fprintf_break_cond break print_elem_list (y::l) in
	
	match li with 
		| [] -> ()
		| x::l -> 	if box = "" then 
						Format.fprintf fmt "%s%a%a%a%s" opening fprintf_break_cond ((opening <> "") && breakdelim) print_elem_list li fprintf_break_cond ((closing <> "") && breakdelim ) closing
					else
						Format.fprintf fmt "@[<%s>%s%a%a%a%s@]" box opening fprintf_break_cond ((opening <> "") && breakdelim) print_elem_list li fprintf_break_cond ((closing <> "") && breakdelim) closing

	
let sprintf_list ?box:(box="") opening string_of_elem sep closing li = 
	let b = Buffer.create 10 in
	let fmt = Format.formatter_of_buffer b in
	fprintf_list ~box:(box) ~opening:(opening) string_of_elem ~sep:(sep) ~closing:(closing) fmt li ;
	Format.pp_print_flush fmt () ;
	Buffer.contents b
	

let fprintf_string fmt str =
	Format.fprintf fmt "@[<h>%s@]" str


let fprintf_preceded ?box:(box="h") ?breakbop:(breakbop=false) ?opening:(opening="") ?breakaop:(breakaop=true) print_elem fmt el =
	if box = "" then
		Format.fprintf fmt "%a%s%a%a" fprintf_break_cond breakbop opening fprintf_break_cond (opening <> "" && breakaop) print_elem el
	else
		Format.fprintf fmt "@[<%s>%a%s%a%a@]" box fprintf_break_cond breakbop opening fprintf_break_cond (opening <> "" && breakaop) print_elem el

let fprintf_followed ?box:(box="h") ?breakbcl:(breakbcl=true) ?closing:(closing=")") ?breakacl:(breakacl=false) print_elem fmt el =
	if box = "" then
		Format.fprintf fmt "%a%a%s%a" print_elem el fprintf_break_cond (closing <> "" && breakbcl) closing fprintf_break_cond breakacl
	else
		Format.fprintf fmt "@[<%s>%a%a%s%a@]" box print_elem el fprintf_break_cond (closing <> "" && breakbcl) closing fprintf_break_cond breakacl


let fprintf_delimited ?box:(box="h") ?breakbop:(breakbop=false) ?opening:(opening="(") ?closing:(closing=")") ?breakacl:(breakacl=false) print_elem fmt el = 
	if box = "" then
		Format.fprintf fmt "%a%s%a%a%a%s%a" fprintf_break_cond breakbop opening fprintf_break_cond (opening = "") print_elem el fprintf_break_cond (closing = "") closing fprintf_break_cond breakacl
	else
		Format.fprintf fmt "@[<%s>%a%s%a%a%a%s%a@]" box fprintf_break_cond breakbop opening fprintf_break_cond (opening = "") print_elem el fprintf_break_cond (closing = "") closing fprintf_break_cond breakacl

let fprintf_in_box ?box:(box="v4") ?breakatbegin:(breakatbegin=false) print_elem fmt el = 
	Format.fprintf fmt "@[<%s>%a%a@]" box fprintf_break_cond breakatbegin print_elem el
	
let fprintf_optional print_elem fmt = function
	| Some elem -> Format.fprintf fmt "%a" print_elem elem
	| None -> ()

let toString func arg = 
	let b = Buffer.create 10 in
	let fmt = Format.formatter_of_buffer b in
	Format.fprintf fmt "%a" func arg ;
	Format.pp_print_flush fmt () ;
	Buffer.contents b
(* format_of_string (Format.sprintf "Hello %s %d" "euh" 5) error *)
(* format_of_string "Hello euh 5";; no error *)

(*
	Unix term utils
*)	
let with_cbreak f x =
  let term_init = Unix.tcgetattr Unix.stdin in
  let term_cbreak = { term_init with Unix.c_icanon = false } in
  Unix.tcsetattr Unix.stdin Unix.TCSANOW term_cbreak;
  try
    let result = f x in
    Unix.tcsetattr Unix.stdin Unix.TCSADRAIN term_init;
    result
  with e ->
    Unix.tcsetattr Unix.stdin Unix.TCSADRAIN term_init;
    raise e

let prompt ?block:(block=false) message =
	Format.printf "@[<h>%s@]@." message ;
	if block then
		Stdlib.input_char Stdlib.stdin
	else
		with_cbreak Stdlib.input_char Stdlib.stdin

let visual_bel () = ignore (Sys.command "tput flash")
	

(*
	Unix filename utils
*)
let generate_filename ?from:(from = "") ?ext:(ext = ".ada") () =
	let candidate = 
		if from = "" then
			let time = Unix.time () in
			let {Unix.tm_sec=seconds; tm_min=minutes; tm_hour=hours;
				tm_mday=day_of_month; tm_mon=month; tm_year=year;
				tm_wday=wday; tm_yday=yday; tm_isdst=isdst} =
		  	Unix.localtime time in			 
		  	Format.sprintf "%04d%02d%02d_%02d%02d%02d%s"
				(year + 1900) (month + 1) day_of_month hours minutes seconds ext
		else
			from in
	let without_extention = Filename.remove_extension candidate in
	let extension = Filename.extension  candidate in
	let possible_result =  without_extention ^ extension in
	if not (Sys.file_exists possible_result) then
		possible_result
	else
		let rec generateUniqueBis without_extention extension count = 
			let possible_result = Format.sprintf "%s(%d)%s" without_extention count extension in
			if not (Sys.file_exists possible_result) then
				possible_result
			else
				generateUniqueBis without_extention extension (count + 1) in
		generateUniqueBis without_extention extension 1 

(*
	Unix Utils
*)
let create_directory ?perm:(perm = 0o777) dirname = 
	try
		Unix.mkdir dirname perm
	with e -> ()

let in_channel_to_string in_channel = 
	let rec in_channel_to_string_rec in_channel acc = 
		(
			try 
				in_channel_to_string_rec in_channel ((Stdlib.input_line in_channel)::acc)
			with End_of_file ->
				(List.rev acc) 
		) in
	String.concat "\n" (in_channel_to_string_rec in_channel [])


(*
	Transform Utils
*)

let utransform_optionnal func = function
	| None -> None
	| Some v -> Some (func v)


(*let remove e li = 
    let rec remove_acc e acc = function
    | [] -> List.rev acc
    | h::t -> if e = h then remove_acc e acc t else remove_acc e (h::acc) t in
    remove_acc e [] li

let assoc_add k v map = 
    let rec assoc_add_acc k v map acc = 
        match map with
        | [] -> (k, v)::(List.rev acc)
        | (k0, v0)::t -> if k0 = k then List.rev_append acc ((k, v)::t) else assoc_add_acc k v t ((k0, v0)::acc)
    in
    assoc_add_acc k v map []

let assoc_put k v map = 
    let rec assoc_put_acc k v map acc = 
        match map with
        | [] -> Some (List.rev acc)
        | (k0, v0)::t -> if k0 = k then Some (List.rev_append acc ((k, v)::t)) else assoc_put_acc k v t ((k0, v0)::acc)
    in
    assoc_put_acc k v map []

let assoc_fusion map map0 =
    let rec assoc_fusion_acc map map0 acc =
        match map with
        | [] -> List.rev acc
        | (k, v)::t -> 
            (
                match List.assoc_opt k map0 with
                | Some v0 -> if v = v0 then assoc_fusion_acc t map0 ((k,v)::acc) else assoc_fusion_acc t map0 acc
                | None -> assoc_fusion_acc t map0 acc
            )
    in
    assoc_fusion_acc map map0 []

let file_list path = Sys.readdir path |> Array.to_list |> List.map (Filename.concat path)

let dirname path = if Sys.is_directory path then path else Filename.dirname path*)

let remove_suffixe suffix str = if Filename.check_suffix str suffix then String.sub str 0 (String.length str - String.length suffix) else str

let buffer_size = 8192;;
let buffer = Bytes.create buffer_size;;

let file_copy input_name output_name =
  let fd_in = openfile input_name [O_RDONLY] 0 in
  let fd_out = openfile output_name [O_WRONLY; O_CREAT; O_TRUNC] 0o666 in
  let rec copy_loop () = match read fd_in buffer 0 buffer_size with
    |  0 -> ()
    | r -> ignore (write fd_out buffer 0 r); copy_loop ()
  in
  copy_loop ();
  close fd_in;
  close fd_out;;

let is_secu cmpunit = match cmpunit with
| ((_, true), _) -> true
| ((_, false), unt) -> 
    (
        match unt with
        | Compilation_Body _ -> false
        | Compilation_PkgDecl (_, d) -> List.exists (fun d -> d = Use_Clause) d
        | Compilation_PkgBody (_, d, _) -> List.exists (fun d -> d = Body_Use_Clause) d
    )

let rec force_evalDeclaration_de_baseList_f env l = match l with
| [] -> env
| (Declaration_d_objet (ConstantVar (id, typ, e)))::t -> 
    (
      match evalExpression_f env e with
      | Some v -> 
        (
            match declareGlobalVar id (ConstantValue v) env with
            | Some env2 -> force_evalDeclaration_de_baseList_f env2 t
            | _ -> force_evalDeclaration_de_baseList_f env t
        )
      | None -> force_evalDeclaration_de_baseList_f env t
    )
| (Declaration_d_objet (VariableVar(id, typ)))::t -> 
    (
        match declareGlobalVar id (VariableValue (decValue typ)) env with
        | Some env2 -> force_evalDeclaration_de_baseList_f env2 t
        | _ -> force_evalDeclaration_de_baseList_f env t
    )
| _::t -> force_evalDeclaration_de_baseList_f env t

let force_eval_unit prep_env c = match c with
| Compilation_Body ed -> defineFunction ed prep_env
| Compilation_PkgDecl (_, dec) -> Some (force_evalDeclaration_de_baseList_f prep_env dec)
| Compilation_PkgBody (_, b1, b2) -> evalDeclaration_1_bodyList_f prep_env b1

let rec print_value fmt v =  match v with
| Undefined -> Format.fprintf fmt "undef"
| Int v -> Format.fprintf fmt "%d" v
| Bool v -> Format.fprintf fmt "%b" v
| STR v -> Format.fprintf fmt "\"%s\"" (String.of_seq (List.to_seq v))
| ArrayV (vlist, default) -> Format.fprintf fmt "["; List.iter (fun (idx, v) -> Format.fprintf fmt "(%d : %a); " idx print_value v) vlist; Format.fprintf fmt "(default : %a)]" print_value default
| RecordV vrec -> Format.fprintf fmt "{"; List.iter (fun (idx, v) -> Format.fprintf fmt "(%s : %a); " (String.of_seq (List.to_seq idx)) print_value v) vrec; Format.fprintf fmt "}" 

let print_constOrVar fmt v = match v with
| ConstantValue v -> Format.fprintf fmt "constant %a" print_value v
| VariableValue v -> Format.fprintf fmt "%a" print_value v

let print_external fmt = function
| EVar v -> print_constOrVar fmt v
| EFDecl v -> Format.fprintf fmt "%a" (fun fmt args -> (match args with | [] -> () | _ -> Format.fprintf fmt " (%a)" print_args_for_sub_prog args )) v

let rec print_external_content pkid fmt exts = match exts with
| [] -> ()
| (eid, ev)::[] -> Format.fprintf fmt "%s.%s %a" pkid eid print_external ev
| (eid, ev)::t -> Format.fprintf fmt "%s.%s %a@ %a" pkid eid print_external ev (print_external_content pkid) t


let rec is_inlined pname decs = match decs with
| [] -> false
| (Pragma_Declaration(Pragma(pragname, pragargs)))::t -> ((String.uppercase_ascii (String.of_seq (List.to_seq pragname)) = "INLINE" && (List.exists (fun arg -> arg = (Exp(EVar [pname]))) pragargs)) || (is_inlined pname t))
| _::t -> is_inlined pname t





