open Format
open LexerDefinition
open Parser
open Lexer
open Stdlib
open Printf
open Filename
open Pretty_printer
open Environment
open Preprocessor
open AstOCAMLutils
open SyntaxOptimizers
open PartialViewOptimizers
open Deciders
open Table_helper

(** Output directory **)
let output_dir = ref "source_opt" 

(** closing read and write channels **)
let close_channels input_channel output_channel = begin if input_channel <> Stdlib.stdin then Stdlib.close_in input_channel ; if output_channel <> Stdlib.stdout then Stdlib.close_out output_channel ; end 

(** The VCP ADA parser **)
module ADAParser =
struct
	module M = MenhirHelperLib.GParser(struct let coq_lexer = Lexer.token_stream let menhirParser = Parser.goal_symbol end);;
	let parse = M.parse_from_coqlexbuf
end;;

(** The tested Code printer **)
let code_print_checked ast filename =
	let buff = Buffer.create 16 in
	let fmt = Format.formatter_of_buffer buff in
	Format.fprintf fmt "%a" print_compilation ast;
	Format.pp_print_flush fmt () ;
	let content = Buffer.contents buff in
	let newlexbuf = LexerDefinition.lexbuf_from_string (List.of_seq (String.to_seq content)) in
	Buffer.reset buff;
	try
		let ast0 = ADAParser.parse newlexbuf in
		if ast <> ast0 then 
		(
            let basename = Filename.basename filename in
            let mbasename = basename ^ ".output" in
            let mbasename2 = basename ^ ".content" in
            Format.printf "(EE) pretty-printer error for %s@." filename; 
            Format.printf "(EE) Debug information can be found in files PrintterDebug/%s and PrintterDebug/%s@." basename mbasename; 
            let output_c = Stdlib.open_out (Filename.concat "PrintterDebug" basename) in
            Format.fprintf (Format.formatter_of_out_channel output_c) "%a" print_compilation ast;
            Stdlib.close_out output_c;
            let output_c = Stdlib.open_out (Filename.concat "PrintterDebug" mbasename) in
            Format.fprintf (Format.formatter_of_out_channel output_c) "%a" print_compilation ast0;
            Stdlib.close_out output_c;
            let output_c = Stdlib.open_out (Filename.concat "PrintterDebug" mbasename2) in
            Format.fprintf (Format.formatter_of_out_channel output_c) "%s" content;
            Stdlib.close_out output_c;
			failwith "(EE) Pretty_printer bug detected" 
        )
		else 
			content
	with e -> raise e

(** From filename to AST **)
let generate_ast filename =
    let channel = Stdlib.open_in filename in
    let original_content = AstOCAMLutils.in_channel_to_string channel in
    Stdlib.close_in channel ;
    let lexbuf = LexerDefinition.lexbuf_from_string (List.of_seq (String.to_seq original_content)) in
    try 
        let cmpunit = ADAParser.parse lexbuf in
        analyse_ast cmpunit;
        if not (is_secu cmpunit) then
        (
            Format.printf "(WW) %s is not Vital@." filename; 
            Format.printf "(WW) Copying : %s to %s@." filename !output_dir; 
            let basename = Filename.basename filename in
            AstOCAMLutils.file_copy filename (Filename.concat !output_dir basename)
        )
        else
        (
            let in_length = String.length original_content in
            (*Format.printf "Taille: %d@." in_length;*)
            Deciders.init filename in_length
        );
        Some cmpunit
    with e -> 
        (
            Format.printf "(EE) %s@." (Printexc.to_string e); 
            Format.printf "(EE) Was not able to load : %s@." filename; 
            Format.printf "(WW) Copying : %s to %s@." filename !output_dir; 
            let basename = Filename.basename filename in
            AstOCAMLutils.file_copy filename (Filename.concat !output_dir basename);
            None
        )


(** Topological search **)
exception CycleFound of string list

let spec_ext = ref ".ads"
let body_ext = ref ".adb"

let dfs graph visited start_node = 
  let rec explore path visited node = 
    if List.mem node path    then raise (CycleFound path) else
    if List.mem node visited then visited else     
      let new_path = node :: path in 
      let edges    = try List.assoc node graph with Not_found -> Format.printf "(WW) Spec. file \"%s\" not found and ignored\n" (node ^ !spec_ext); [] in
      let visited  = List.fold_left (explore new_path) visited edges in
      node :: visited
  in explore [] visited start_node

let toposort graph = 
  List.fold_left (fun visited (node,_) -> dfs graph visited node) [] graph
  
(** Building <filename, AST> map **)
let rec build_ast_map_bis file_list acc = match file_list with
| [] -> List.rev acc
| hpath::tpath -> 
    (
        match generate_ast hpath with
        | None -> build_ast_map_bis tpath acc
        | Some c -> build_ast_map_bis tpath ((hpath, c)::acc)
    )
   
let build_ast_map file_list = build_ast_map_bis file_list []

(** from <with statements> to <filepaths> **)
let rec from_include_instruction_to_file include_str acc = function
| [] -> acc
| filename::t -> 
    if String.lowercase_ascii (AstOCAMLutils.remove_suffixe !body_ext (AstOCAMLutils.remove_suffixe !spec_ext (Filename.basename filename))) = String.lowercase_ascii include_str && Filename.check_suffix filename !spec_ext then
        (from_include_instruction_to_file include_str (filename::acc) t)
    else
        (from_include_instruction_to_file include_str acc t)

(** from <filename, AST> map to <filename, deps> maps  **)
let build_dependence_graph ast_map = 
    let keys = List.map (fun (k, v) -> k) ast_map in
    let dep_graph = List.map 
        (fun (path, ast) -> 
            (
                let deps = List.map (fun x -> (String.of_seq (List.to_seq x))) (fst (fst ast)) in
                let possible_spec_name = (AstOCAMLutils.remove_suffixe !body_ext path) ^ !spec_ext in
                let final_deps = List.fold_left (fun mem elem -> from_include_instruction_to_file elem mem keys) (if Filename.check_suffix path !body_ext && List.mem possible_spec_name keys && possible_spec_name <> path then [possible_spec_name] else []) deps in
                
                (path, final_deps)
            )
        ) ast_map
    in
    dep_graph

(** Sorting filenames from dep_graph : <filename, deps> maps **)
let sort_compilation_units dep_graph =
    List.rev (toposort dep_graph)

(** From <with-statement> and <filename, env> map to <env> **)
let rec getEnvFromInclude inc map = match map with
| [] -> { 
    localVars = []; 
    globalVars = []; 
    funDefs = []; 
    localfunDecls = []; 
    externals = [] }
| (filename, env)::t -> 
    if String.lowercase_ascii (AstOCAMLutils.remove_suffixe !body_ext (AstOCAMLutils.remove_suffixe !spec_ext (Filename.basename filename))) = String.lowercase_ascii (String.of_seq (List.to_seq inc)) && Filename.check_suffix filename !spec_ext then env else getEnvFromInclude inc t

(** From <with-statements> and <filename, env> map to <env> **)
let rec getExternals_acc prep_env_map incs acc = match incs with
| [] -> List.rev acc
| hinc::tinc -> 
    (
        let env = getEnvFromInclude hinc prep_env_map in
        getExternals_acc prep_env_map tinc ((hinc, (Preprocessor.fromVisibleVarsToExt (env.globalVars)) @ (fromVisibleFunsToExt (env.localfunDecls)))::acc)
    )

let getExternals prep_env_map incs = getExternals_acc prep_env_map incs []

(** Generating <filename, preprocessed env> map **)
let rec generateAccessibleEnv ast_map order acc = match order with
| [] -> List.rev acc
| h_order::t_order -> 
    let ((h_incs, use), h_unit) = List.assoc h_order ast_map in
    let h_exts = getExternals acc h_incs in
    let pre_env = 
        (if Filename.check_suffix h_order !body_ext then 
            (
                let env = getEnvFromInclude (List.of_seq (String.to_seq (AstOCAMLutils.remove_suffixe !body_ext h_order))) acc in 
                {env  with externals = Preprocessor.fusionExternal env.externals h_exts}
            
            )
        else
            (
                { localVars = []; globalVars = []; funDefs = []; localfunDecls = []; externals = h_exts }
            )
        ) in
    (
        if is_secu ((h_incs, use), h_unit) then
            match Preprocessor.eval_unit pre_env h_unit with
            | None -> failwith (Format.sprintf "(EE) Cannot eval %s (secu)" h_order)
            | Some eenv -> generateAccessibleEnv ast_map t_order ((h_order, eenv)::acc)
        else
            match force_eval_unit pre_env h_unit with
            | None -> failwith (Format.sprintf "(EE) Cannot eval %s (non-secu)" h_order)
            | Some eenv -> generateAccessibleEnv ast_map t_order ((h_order, eenv)::acc)
    )

    
(* let rec getEnvFromDecId dec_id map = match map with
| [] -> failwith (Format.sprintf "(EE) %s not found in prep_map" dec_id)
| (filename, env)::t -> 
    if String.lowercase_ascii (AstOCAMLutils.remove_suffixe !body_ext (AstOCAMLutils.remove_suffixe !spec_ext (Filename.basename filename))) = String.lowercase_ascii dec_id && Filename.check_suffix filename !spec_ext then env else getEnvFromDecId dec_id t *)

(** Preprocessing **)
let rec getEnvFromBodyId body_id map = match map with
| [] -> failwith (Format.sprintf "(EE) %s not found in prep_map" body_id)
| (filename, env)::t -> 
    if String.lowercase_ascii (AstOCAMLutils.remove_suffixe !body_ext (AstOCAMLutils.remove_suffixe !spec_ext (Filename.basename filename))) = String.lowercase_ascii body_id && Filename.check_suffix filename !body_ext then env else getEnvFromBodyId body_id t

let preprocessor preprocessed_envs (incls, _) (pkg_name, is_dec_body) = 
    let dec_prep_env = getEnvFromInclude pkg_name preprocessed_envs in
    if not is_dec_body then
        (
            (([], []), dec_prep_env.externals)
        )
    else
        (
            let body_prep_env = getEnvFromBodyId (String.of_seq (List.to_seq pkg_name)) preprocessed_envs in
            ((dec_prep_env.globalVars, dec_prep_env.localfunDecls), body_prep_env.externals)
            
        )

(** Getting the list of source files **)
let process_file_list directory =
    Sys.readdir directory
    |> Array.to_list
    |> List.filter (fun x -> Filename.check_suffix x !spec_ext || Filename.check_suffix x !body_ext)
    |> List.map (fun x -> Filename.concat directory x)
    
(** Optimizing type 1: using file name to find deciders and duplication detectors **)
let rec optimize_project_ast_with_dfilename opt_ast_map preprocessed_map ast_map proc dup decider = function
| [] -> 
        Format.printf "(II) --------------------@."; 
        Format.printf "(II) End of optimizations@."; 
        Format.printf "(II) --------------------@."; 
        opt_ast_map
| hfile::tfile -> 
    Format.printf "(II) Optimizing %s@." hfile;
    let ast = List.assoc hfile ast_map in
    let ast_opt = (proc (dup hfile) (decider hfile)) (preprocessor preprocessed_map) ast in
    if ast <> ast_opt then Format.printf "(II) Detected in %s (%s)@." (Filename.basename hfile) hfile;
    optimize_project_ast_with_dfilename ((hfile, ast_opt)::opt_ast_map) preprocessed_map ast_map proc dup decider tfile

(** Optimizing type 2: deciders and duplication detectors are handled by the optimizer (proc) **)
let rec optimize_project_ast opt_ast_map preprocessed_map ast_map proc = function
| [] -> 
        Format.printf "(II) --------------------@."; 
        Format.printf "(II) End of optimizations@."; 
        Format.printf "(II) --------------------@."; 
        opt_ast_map
| hfile::tfile -> 
    Format.printf "(II) Optimizing %s@." hfile;
    let ast = List.assoc hfile ast_map in
    let ast_opt = proc (preprocessor preprocessed_map) ast in
    if ast <> ast_opt then Format.printf "(II) Detected in %s (%s)@." (Filename.basename hfile) hfile;
    optimize_project_ast ((hfile, ast_opt)::opt_ast_map) preprocessed_map ast_map proc tfile

(** Generating optimized source code **)
let rec write_optimized_ast opt_ast_map ast_map = function
| [] -> 
        Format.printf "(II) ----------------------@."; 
        Format.printf "(II) End of file generation@."; 
        Format.printf "(II) ----------------------@."
| hfile::tfile -> 
    let ast = List.assoc hfile ast_map in
    let ast_opt = List.assoc hfile opt_ast_map in
    let code = code_print_checked ast_opt hfile in 
    let basename = Filename.basename hfile in
    if ast <> ast_opt then
        Format.printf "(II) %s has been optimized\n" basename
    else
        Format.printf "(II) No optimization opportunity found in %s\n" basename ;
    let ofilename = Filename.concat !output_dir basename in
    let output_channel_opt = Stdlib.open_out ofilename in
    Printf.fprintf output_channel_opt "%s\n" code;
    Stdlib.close_out output_channel_opt;
    write_optimized_ast opt_ast_map ast_map tfile

(** Helper for inline decider **)
let rec find_body map pkgname pname = match map with
| [] -> None
| (filename, ast)::t -> 
    if String.lowercase_ascii (AstOCAMLutils.remove_suffixe !body_ext (AstOCAMLutils.remove_suffixe !spec_ext (Filename.basename filename))) = String.lowercase_ascii pkgname && Filename.check_suffix filename !body_ext then 
        (
            match ast with
            | (_, Ast.Compilation_PkgBody (_, _, l)) ->
                let candidates = List.filter_map (fun x -> match x with | Ast.Fun_def ed -> if ed.f_name = pname then Some ed else None  | _ -> None) l in
                if List.length candidates = 1 then
                    Some (List.hd candidates)
                else None
            | _ -> None
        )
    else 
        find_body t pkgname pname
    

let () = 
	begin
	let usage_msg = Format.sprintf "(EE) A program to optimize a project. Usage : %s path_to_input_directory" Sys.argv.(0) in
	let inputList = ref [] in
	(*let max_conv_to_delete = ref 50 in*)
	Arg.parse [("-spec", Arg.Set_string spec_ext, "Specify the extension of specification files"); ("-body", Arg.Set_string body_ext, "Specify the extension of body files"); ("-o", Arg.Set_string output_dir, "Specify the output directory")] (fun anon -> inputList := (!inputList @ [anon]) ) usage_msg;
	match !inputList with
		| [ in_path ] -> 
		    Format.printf "(II) Loading project file list@."; 
		    let file_list = process_file_list in_path in
		    (*Format.printf "[";
		    List.iter (fun x -> Format.printf "%s; " x) file_list;
		    Format.printf "] (%d)" (List.length file_list);
		    Format.printf "@;";*)
    	    Format.printf "(II) Building ast-map@."; 
		    create_directory !output_dir;
		    let start = Unix.gettimeofday() in
            let ast_map = build_ast_map file_list in
            let end_ = Unix.gettimeofday() in
            Format.printf "(II) Parsing time %f@." (end_ -. start);
		    (*Format.printf "[";
		    List.iter (fun (x, _) -> Format.printf "%s; " x) ast_map;
		    Format.printf "] (%d)" (List.length ast_map);
		    Format.printf "@;"*)
		    Format.printf "(II) Building dependency graph@."; 
		    let dep_graph = build_dependence_graph ast_map in
            (*Format.printf "deps: {\n"; 
		    Format.printf "{\n";
            List.iter (fun (id, li) -> Format.printf "\t %s : [" id; List.iter (fun v -> Format.printf "\n\t\t\t%s;" v) li; Format.printf "\n\t\t];\n") dep_graph;
            Format.printf "\n}@;"*)
            Format.printf "(II) Sorting dependencies\n"; 
		    let sorted = sort_compilation_units dep_graph in
            (*Format.printf "[";
		    List.iter (fun x -> Format.printf "%s; " x) sorted;
		    Format.printf "] (%d)" (List.length sorted);
		    Format.printf "@;";*)
		    let preprocessed_envs = generateAccessibleEnv ast_map sorted [] in
		    (*print_env (List.assoc "/home/palexo/Dev/TestPSC/sources_ada_x86/sec_opel_pentium_mscax100_tfc_es_commun.2.ada" preprocessed_envs)*)
		    let set_of_secu_codes = List.filter (fun x -> is_secu (List.assoc x ast_map)) sorted in
		    (*Format.printf "-----------------------\n";
		    Format.printf "Applying convergeOptim1\n";
		    Format.printf "-----------------------\n";
		    let opt_ast_map = optimize_project_ast_with_decider [] preprocessed_envs ast_map convergeOptim1 end_branch_decider_with_filename set_of_secu_codes in*)
		    
		    Format.printf "(II) -----------------------------\n";
		    Format.printf "(II) Applying expression-simpOptim\n";
		    Format.printf "(II) -----------------------------\n";
		    let opt_ast_map = optimize_project_ast [] preprocessed_envs ast_map expression_simpl_optimizer set_of_secu_codes in
		    
		    Format.printf "(II) -----------------------\n";
		    Format.printf "(II) Applying flowOptim\n";
		    Format.printf "(II) -----------------------\n";
		    let opt_ast_map = optimize_project_ast_with_dfilename [] preprocessed_envs opt_ast_map flow_optimizer ins_s_non_duplic ops_decider set_of_secu_codes in 
		    
		    Format.printf "(II) --------------------------------\n";
		    Format.printf "(II) Applying while-full-unroll-optim\n";
		    Format.printf "(II) --------------------------------\n";
		    let opt_ast_map = optimize_project_ast_with_dfilename [] preprocessed_envs opt_ast_map while_full_unroll_optimizer ins_s_non_duplic ops_decider set_of_secu_codes in 
		    
		    Format.printf "(II) -----------------------\n";
		    Format.printf "(II) Applying convergeOptim1\n";
		    Format.printf "(II) -----------------------\n";
		    let opt_ast_map = optimize_project_ast_with_dfilename [] preprocessed_envs opt_ast_map convergeOptim1 ins_s_non_duplic ops_decider set_of_secu_codes in
		    
		    Format.printf "(II) -----------------------\n";
		    Format.printf "(II) Applying convergeOptim2\n";
		    Format.printf "(II) -----------------------\n";
		    let opt_ast_map = optimize_project_ast_with_dfilename [] preprocessed_envs opt_ast_map convergeOptim2 ins_s_non_duplic ops_decider set_of_secu_codes in
		    
		    Format.printf "(II) -----------------------\n";
		    Format.printf "(II) Applying convergeOptim3\n";
		    Format.printf "(II) -----------------------\n";
		    let opt_ast_map = optimize_project_ast_with_dfilename [] preprocessed_envs opt_ast_map convergeOptim3 ins_s_non_duplic ops_decider set_of_secu_codes in
		    
		    Format.printf "(II) -----------------------------\n";
		    Format.printf "(II) Applying expression-simpOptim\n";
		    Format.printf "(II) -----------------------------\n";
		    let opt_ast_map = optimize_project_ast [] preprocessed_envs opt_ast_map expression_simpl_optimizer set_of_secu_codes in
		    
		    Format.printf "(II) -----------------------------\n";
		    Format.printf "(II) Applying Inlining\n";
		    Format.printf "(II) -----------------------------\n";
		    (*let opt_ast_map = optimize_project_ast_with_decider [] preprocessed_envs opt_ast_map pragmaInlineOptim () set_of_secu_codes in*)
		    let opt_ast_map = optimize_project_ast [] preprocessed_envs opt_ast_map (pragmaInlineOptim (Deciders.inline_decide (find_body opt_ast_map))) set_of_secu_codes in
		    
		    Format.printf "(II) -----------------------\n";
		    Format.printf "(II) Writting output files\n";
		    Format.printf "(II) -----------------------\n";
		    write_optimized_ast opt_ast_map ast_map set_of_secu_codes;
		    let end_ = Unix.gettimeofday() in
            Format.printf "(II) Total time %f@." (end_ -. start);
		    
		    Format.printf "(II) -----------------------\n";
		    Format.printf "(II) OPS Stats\n";
		    Format.printf "(II) -----------------------\n";
		    print_stats ();
		| _ -> failwith usage_msg
	end
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
