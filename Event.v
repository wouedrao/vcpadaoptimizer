Require Import List String Map Values Ast Environment.
Import ListNotations.
Local Open Scope Z_scope.
Local Open Scope type_scope.

Record event:= mkEvent {
  fun_name: selected_com
; fun_call_params: list expression_or_string_lit
; fun_call_params_eval: list (option value)
; fun_sig: option (list (ident * mode * name))
; env_at_call_wo_locals: env
}.