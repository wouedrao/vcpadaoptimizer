Require Import List.
Import ListNotations.
Require Export Setoid.
Require Export Relation_Definitions.

Module Type MAP.
  Parameter key_t: Type.
  Parameter key_eq: key_t -> key_t -> bool.
  Parameter t: Type -> Type.
  Parameter init: forall (A: Type), t A.
  Parameter get : forall (A: Type), key_t -> t A -> option A.
  Parameter set: forall (A: Type), key_t -> A -> t A -> t A.
  Parameter keys: forall (A: Type), t A -> list key_t.
  Parameter resides : forall (A: Type), key_t -> t A -> bool.
  Axiom resides_true_iff : forall (A: Type) (k : key_t) (m : t A), resides A k m = true <-> In k (keys A m).
  Parameter update: forall (A: Type), key_t -> A -> t A -> option (t A).
  Parameter unset: forall (A: Type), key_t -> t A -> t A.
  Axiom resides_update : forall (A: Type) (k : key_t) (v : A) (m : t A), resides A k m = true <-> exists m', update A k v m = Some m'.
  Axiom resides_get : forall (A: Type) (k : key_t) (m : t A), resides A k m = true <-> exists m', get A k m = Some m'.
  Axiom gi:
    forall (A: Type) (i: key_t) (x: A), get A i (init A) = None.
  Axiom gss:
    forall (A: Type) (i: key_t) (x: A) (m: t A), get A i (set A i x m) = Some x.
  Axiom gso:
    forall (A: Type) (i j: key_t) (x: A) (m: t A),
    key_eq i j = false -> get A i (set A j x m) = get A i m.
  Axiom gsspec:
    forall (A: Type) (i j: key_t) (x: A) (m: t A),
    get A i (set A j x m) = if key_eq i j then Some x else get A i m.
  Axiom gsident:
    forall (A: Type) (i j: key_t) (m: t A) (v : A), (get A i m) = Some v ->  get A j (set A i v m) = get A j m.
  Axiom unsetspec:
    forall (A: Type) (m: t A) (i j: key_t),
    get A i (unset A j m) = if key_eq i j then None else get A i m.
  Parameter map: forall (A B: Type), (A -> B) -> t A -> t B.
  Axiom gmap:
    forall (A B: Type) (f: A -> B) (i: key_t) (m: t A),
    get B i (map A B f m) = (match get A i m with | None => None | Some v => Some (f v) end).
  Axiom keysspec : forall (A: Type) (m: t A) (k : key_t), In k (keys A m) <-> exists v, get A k m = Some v.
  Axiom updatekspec1 : forall (A: Type) (m: t A) (k : key_t) (v : A), In k (keys A m) -> exists m', update A k v m = Some m'.
  Axiom updatekspec2 : forall (A: Type) (m m': t A) (k : key_t) (v : A), update A k v m = Some m' -> In k (keys A m) .
  Axiom updatevspec : forall (A: Type) (m m': t A) (k i: key_t) (v : A), update A k v m = Some m' -> (get A i m') = if key_eq k i then Some v else (get A i m).
  Axiom update_set : forall (A: Type) (m: t A) (k: key_t), (exists v, get A k m = Some v) -> (forall v, update A k v m = Some (set A k v m)).
  Definition map_eq {A : Type} (m m': t A) := forall i, get A i m = get A i m'.
  Axiom map_eq_init : forall (A : Type), map_eq (init A) (init A).
  Axiom map_eq_set_cons : forall (A : Type) (m m' : t A) k v, map_eq m m' -> map_eq (set A k v m) (set A k v m').
  Axiom map_eq_update : forall (A : Type) (m' m mu mu': t A) k v, map_eq m m' -> (update A k v m) = Some mu -> (update A k v m') = Some mu' -> map_eq mu mu'.
End MAP.


Module Type EQUALITY_TYPE.
  Parameter t: Type.
  Parameter eq: t -> t -> bool.
  Axiom eq_spec_true : forall a b, eq a b = true <-> a = b.
End EQUALITY_TYPE.

Require Import Classical.

Module EListMap(X: EQUALITY_TYPE) <: MAP.
  
  Definition key_t := X.t.
  
  Definition key_eq := X.eq.
  
  Lemma Xeq_refl : forall a, X.eq a a = true.
  Proof.
  intros.
  rewrite X.eq_spec_true; auto.
  Qed.

  Lemma eq_spec_false : forall a b, X.eq a b = false <-> a <> b.
  Proof.
  intros.
  split.
  intros.
  destruct (classic (a <> b)); auto.
  intuition.
  apply X.eq_spec_true in H1.
  rewrite H1 in H.
  discriminate H.
  intros.
  destruct (classic (X.eq a b = false)); auto.
  apply Bool.not_false_iff_true in H0.
  apply X.eq_spec_true in H0.
  contradict H.
  auto.
  Qed.

  Lemma Xeq_sym : forall a b, X.eq a b = X.eq b a.
  Proof.
  intros.
  case_eq (X.eq a b); intros; apply eq_sym; try apply X.eq_spec_true in H; try apply X.eq_spec_true; auto.
  apply eq_spec_false in H; apply eq_spec_false; auto.
  Qed.

  Lemma Xeq_trans : forall a b c, X.eq a b = true -> X.eq b c = true -> X.eq a c = true.
  Proof.
  intros; apply X.eq_spec_true.
  apply X.eq_spec_true in H.
  apply X.eq_spec_true in H0.
  transitivity b; auto.
  Qed.

  Definition t (A: Type) := list (X.t * A).
  
  Definition init (A: Type) : t A := [].
  
  Fixpoint set {A : Type} (k : X.t) (v : A) (m : t A) := match m with
  | nil => [(k, v)]
  | (tk, tv)::t => if X.eq k tk then ( (tk, v)::t ) else ( (tk, tv)::(set k v t) )
  end.
  
  Fixpoint unset {A : Type} (k : X.t) (m : t A) := match m with
  | nil => []
  | (tk, tv)::t => if X.eq k tk then ( unset k t ) else ( (tk, tv)::(unset k t) )
  end.
  
  Fixpoint get {A : Type} (k : X.t) (m : t A) := match m with
  | nil => None
  | (tk, tv)::t => if X.eq k tk then ( Some tv ) else ( (get k t) )
  end.

  Fixpoint keys {A : Type} (m : t A) := match m with
  | nil => nil
  | (tk, tv)::t => tk::(keys t)
  end.

  Fixpoint resides {A : Type} (k : X.t) (m : t A) := match m with
  | nil => false
  | (tk, _)::t => if X.eq k tk then true else resides k t
  end.

  Lemma resides_true_iff : forall (A: Type) (k : X.t) (m : t A), resides k m = true <-> In k (keys m).
  Proof.
  split.
  induction m.
  simpl.
  discriminate.
  simpl.
  induction a.
  simpl.
  case_eq (X.eq k a).
  intros.
  rewrite Xeq_sym in H.
  apply X.eq_spec_true in H.
  left.
  auto.
  intros.
  right.
  auto.
  induction m.
  simpl.
  auto.
  simpl.
  induction a.
  simpl.
  intros.
  induction H.
  apply X.eq_spec_true in H.
  rewrite Xeq_sym in H.
  rewrite H.
  auto.
  induction (X.eq k a); auto.
  Qed.
 
  Definition size { A : Type } (m : t A) := List.length m.

  Fixpoint update { A : Type } k v (m : t A) := match m with
  | nil => None
  | (tk, tv)::t => if X.eq k tk then Some ( (tk, v)::t ) else 
    ( match update k v t with
      | Some t' => Some ( (tk, tv)::t' )
      | None => None
      end
    )
  end.

  Lemma gi:
    forall (A: Type) (i: X.t) (x: A), get i (init A) = None.
  Proof.
  auto.
  Qed.

  Lemma gss:
    forall (A: Type) (i: X.t) (x: A) (m: t A), get i (set i x m) = (Some x).
  Proof.
  induction m; simpl.
  rewrite Xeq_refl; auto.
  induction a; simpl.
  case_eq (X.eq i a); intros; simpl; try rewrite H; auto.
  Qed.

  Lemma gso:
    forall (A: Type) (i j: key_t) (x: A) (m: t A),
    X.eq i j = false -> get i (set j x m) = get i m.
  Proof.
  induction m; simpl; auto.
  intros.
  rewrite H; auto.
  induction a.
  intros.
  case_eq (X.eq j a).
  simpl.
  intros.
  apply X.eq_spec_true in H0.
  rewrite H0 in *.
  rewrite H.
  auto.
  intros.
  case_eq (X.eq i a).
  intros.
  simpl.
  rewrite H1.
  auto.
  intros.
  simpl.
  rewrite H1.
  apply IHm.
  auto.
  Qed.

  Lemma gsspec:
    forall (A: Type) (i j: key_t) (x: A) (m: t A),
    get i (set j x m) = if X.eq i j then (Some x) else get i m.
  Proof.
  induction m; simpl. 
  auto.
  induction a.
  case_eq (X.eq j a); simpl.
  intros.
  case_eq (X.eq i a); intros.
  assert (X.eq i j = true).
  eapply Xeq_trans; eauto.  
  rewrite Xeq_sym.
  auto.
  rewrite H1.
  auto.
  apply X.eq_spec_true in H.
  rewrite <- H in H0.
  rewrite H0.
  auto.
  intros.
  case_eq (X.eq i a); simpl; auto.
  intros.
  apply X.eq_spec_true in H0.
  rewrite <- H0 in H.
  rewrite Xeq_sym in H. 
  rewrite H.
  auto.
  Qed.

  Lemma gsident:
    forall (A: Type) (i j: X.t) (m: t A) v, (get i m) = Some v -> get j (set i v m) = get j m.
  Proof.
  induction m; simpl; try discriminate; auto.
  induction a.
  case_eq (X.eq i a).
  intros; simpl; auto.
  inversion H0.
  auto.
  simpl.
  intros.
  rewrite IHm; auto.
  Qed.

  Lemma unsetspec:
    forall (A: Type) (m: t A) (i j: key_t),
    get i (unset j m) = if X.eq i j then None else get i m.
  Proof.
  induction m.
  simpl.
  intros.
  induction (X.eq i j); auto.
  intros.
  simpl.
  induction a.
  case_eq (X.eq j a); auto.
  rewrite IHm.
  case_eq (X.eq i j); auto.
  intros.
  assert (X.eq i a = false).
  apply X.eq_spec_true in H0.
  rewrite H0 in *; auto.
  rewrite H1; auto.
  simpl.
  case_eq (X.eq i a); auto.
  intros.
  apply X.eq_spec_true in H.
  rewrite H in *.
  rewrite Xeq_sym.
  rewrite H0; auto.
  Qed.
  
  
  Fixpoint map {A B : Type} (f : A -> B) (m : t A) := match m with
  | nil => nil
  | (tk, tv)::t => (tk, f tv)::(map f t)
  end.
  
  Lemma gmap:
    forall (A B: Type) (f: A -> B) (i: key_t) (m: t A),
    get i (map f m) = (match get i m with | None => None | Some v => Some (f v) end).
  Proof.
  induction m; simpl; auto.
  induction a.
  simpl.
  induction (X.eq i a); auto.
  Qed.

  Lemma keysspec : forall {A: Type} (m: t A) (k : X.t), In k (keys m) <-> exists v, get k m = Some v.
  Proof.
  induction m; simpl.
  split.
  intuition.
  intros.
  destruct H.
  inversion H.
  induction a.
  simpl.
  split.
  intros.
  destruct H.
  rewrite H.
  rewrite Xeq_refl.
  exists b; auto.
  induction (X.eq k a).
  exists b; auto.
  apply IHm in H; auto.
  intros.
  case_eq (X.eq k a); intros.
  apply X.eq_spec_true in H0; auto.
  rewrite H0 in H; right; apply IHm; auto.
  Qed.
  
  Lemma updatekspec1 : forall (A: Type) (m: t A) (k : X.t) (v : A), In k (keys m) -> exists m', update k v m = Some m'.
  Proof.
  induction m; simpl; intros; try inversion H.
  induction a.
  simpl in H.
  destruct H.
  rewrite H.
  rewrite Xeq_refl.
  exists ((k, v) :: m); auto.
  induction (X.eq k a).
  exists ((a, v) :: m); auto.
  apply (IHm k v) in H; auto.
  destruct H.
  rewrite H.
  exists ((a, b) :: x); auto.
  Qed.
  
  Lemma updatekspec2 : forall (A: Type) (m m': t A) (k : X.t) (v : A), update k v m = Some m' -> In k (keys m).
  Proof.
  induction m; simpl; intros; try inversion H.
  induction a.
  simpl.
  case_eq (X.eq k a); intros.
  apply X.eq_spec_true in H0; auto.
  rewrite H0 in H1.
  case_eq (update k v m); intros.
  apply IHm in H2; auto.
  rewrite H2 in H1; inversion H1.
  Qed.
  
  Lemma updatevspec : forall (A: Type) (m m': t A) (k i: X.t) (v : A), update k v m = Some m' -> (get i m') = if X.eq k i then Some v else (get i m).
  Proof.
  induction m; simpl; intros; try discriminate H.
  induction a.
  case_eq (X.eq k a); intros; rewrite H0 in H.
  inversion H.
  simpl.
  case_eq (X.eq i a); intros.
  assert (X.eq k i = true).
  apply X.eq_spec_true in H0.
  apply X.eq_spec_true in H1.
  apply X.eq_spec_true.
  apply eq_sym in H1.
  transitivity a; auto.
  rewrite H3.
  auto.
  assert (X.eq k i = false).
  apply X.eq_spec_true in H0.
  rewrite H0.
  rewrite Xeq_sym.
  auto.
  rewrite H3; auto.
  case_eq (update k v m); intros; rewrite H1 in H.
  inversion H.
  simpl.
  case_eq (X.eq i a); intros.
  apply X.eq_spec_true in H2.
  rewrite <- H2 in H0.
  rewrite H0.
  auto.
  apply IHm.
  auto.
  inversion H.
  Qed.

  Definition map_eq {A : Type} (m m': t A) := forall i, get i m = get i m'.

  Lemma map_eq_refl {A : Type} : reflexive (t A) map_eq.
  Proof.
    unfold reflexive. intro x. unfold map_eq. intro i.  auto.
  Qed.

  Lemma map_eq_sym {A : Type} : symmetric (t A) map_eq.
  Proof.
    unfold symmetric. intros x y H.  unfold map_eq in *.
    intros i.  eauto.
  Qed.

  Lemma map_eq_trans {A : Type} : transitive (t A) map_eq.
  Proof.
    unfold transitive.  intros x y z.  unfold map_eq in *. intros Hxy Hyz s.
    transitivity (get s y); eauto.
  Qed.

  Add Parametric Relation {A : Type}: (t A) map_eq
    reflexivity proved by map_eq_refl
    symmetry proved by map_eq_sym
    transitivity proved by map_eq_trans
    as MAP_setoid.

  Lemma map_eq_init {A : Type} : map_eq (init A) (init A).
  Proof.
  apply map_eq_refl.
  Qed.

  Lemma map_eq_set_cons {A : Type} : forall (m m' : t A) k v, map_eq m m' -> map_eq (set k v m) (set k v m').
  Proof.
  intros.
  unfold map_eq.
  intros.
  rewrite gsspec.
  rewrite gsspec.
  induction (X.eq i k); auto.
  Qed.

  Lemma map_eq_cons {A : Type} : forall (m m' : t A) k v, map_eq m m' -> map_eq ((k, v)::m) ((k, v)::m').
  Proof.
  unfold map_eq.
  simpl.
  intros.
  rewrite H; auto.
  Qed.

  Lemma map_eq_update {A : Type} : forall (m' m mu mu': t A) k v, map_eq m m' -> (update k v m) = Some mu -> (update k v m') = Some mu' -> map_eq mu mu'.
  Proof.
  intros.
  unfold map_eq.
  intros.
  apply (updatevspec A m mu k i) in H0.
  apply (updatevspec A m' mu' k i) in H1.
  rewrite H0.
  rewrite H1.
  induction (X.eq k i); auto.
  Qed.

  Lemma resides_update {A : Type} : forall (k : X.t) (v : A) (m : t A), resides k m = true <-> exists m', update k v m = Some m'.
  Proof.
  split; intros.
  apply resides_true_iff in H.
  apply updatekspec1; auto.
  apply resides_true_iff.
  destruct H.
  apply updatekspec2 in H.
  auto.
  Qed.

  Lemma resides_get {A : Type} : forall (k : key_t) (m : t A), resides k m = true <-> exists m', get k m = Some m'.
  Proof.
  split; intros.
  apply resides_true_iff in H.
  apply keysspec; auto.
  apply keysspec in H.
  apply resides_true_iff; auto.
  Qed.

  Lemma update_set : forall (A: Type) (m: t A) (k: key_t), (exists v, get k m = Some v) -> (forall v, update k v m = Some (set k v m)).
  Proof.
  induction m.
  cbn.
  intros.
  destruct H.
  inversion H.
  cbn.
  intros.
  induction a.
  induction (X.eq k a).
  auto.
  eapply IHm in H.
  erewrite H.
  auto.
  Qed.

End EListMap.

