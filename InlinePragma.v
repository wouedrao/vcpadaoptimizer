Require Import MapTypes Semantics Semantics_f TacUtils Correctness Ast.
Require Import Ast.
Require Import List String ZArith Lia Bool.
Import ListNotations.

Parameter is_inlined : String_Equality.t -> list declaration_de_base -> bool.

Fixpoint put_pragma_inline validator d := match d with
| [] => []
| (Declaration_de_sous_programme pname pargs)::t =>
    if (validator pname pargs) && (negb (is_inlined pname d)) then
      (Declaration_de_sous_programme pname pargs)::(Pragma_Declaration (Pragma "Inline"%string [Exp (EVar [pname])]))::(put_pragma_inline validator t)
    else
      (Declaration_de_sous_programme pname pargs)::(put_pragma_inline validator t)
(* | (Declaration_de_surnom pname pargs rname)::t =>
    if (validator pname pargs) && (negb (is_inlined pname d)) then
      (Declaration_de_surnom pname pargs rname)::(Pragma_Declaration (Pragma "Inline"%string [Exp (EVar [pname])]))::(put_pragma_inline t)
    else
      (Declaration_de_surnom pname pargs rname)::(put_pragma_inline t) *)
| h::t => h::(put_pragma_inline validator t)
end.

Lemma put_pragma_inline_correct: forall validator d inenv, evalDeclaration_de_baseList_f inenv d = evalDeclaration_de_baseList_f inenv (put_pragma_inline validator d).
Proof.
induction d; auto; intros.
induction a; simpl; try rewrite IHd; eauto.
induction o.
induction (evalExpression_f inenv e); eauto.
induction (Environment.declareGlobalVar t (Values.ConstantValue a) inenv); eauto.
induction (Environment.declareGlobalVar t (Values.VariableValue (decValue t0)) inenv); eauto.
induction (validator t l && negb (is_inlined t (Declaration_de_sous_programme t l :: d))); simpl; eauto.
induction (Environment.declareFunction t l inenv); eauto.
induction (Environment.declareFunction t l inenv); eauto.
induction (validator t l && negb (is_inlined t (Declaration_de_surnom t l n :: d))); simpl; eauto.
induction (Environment.declareFunction t l inenv); eauto.
induction (Environment.defineFunction
    {|
      f_name := t;
      f_args := l;
      f_local_decs := [];
      f_instrs :=
        ISeqEnd (ICallDefinedFunction n (Some (from_def_to_call_params l)))
    |} a); eauto.
induction (Environment.declareFunction t l inenv); eauto.
induction (Environment.defineFunction
    {|
      f_name := t;
      f_args := l;
      f_local_decs := [];
      f_instrs :=
        ISeqEnd (ICallDefinedFunction n (Some (from_def_to_call_params l)))
    |} a); eauto.
Qed.

Lemma put_pragma_include : forall validator l, include_except_prag (declaration_set_of_declaration_de_base l)
  (declaration_set_of_declaration_de_base (put_pragma_inline validator l)).
Proof.
induction l; simpl; try solve [apply include_except_prag_HeadEq; eauto].
apply include_except_prag_Nil.
induction a; simpl; try solve [apply include_except_prag_HeadEq; eauto]; auto.
induction o; simpl; try solve [apply include_except_prag_HeadEq; eauto]; auto.
induction (validator t l0 && negb (is_inlined t (Declaration_de_sous_programme t l0 :: l))).
simpl.
apply include_except_prag_HeadPragma.
auto.
simpl.
auto.
Qed.

Definition put_pragma_inline_in_compilation validator (p : preprocessor) (c: compilation) := match c with
| (incls, unt) => 
    (incls, 
      ( 
          match unt with
          | Compilation_Body ed => Compilation_Body ed
          | Compilation_PkgDecl id dec => Compilation_PkgDecl id (put_pragma_inline (validator id) dec)
          | Compilation_PkgBody id dec fdec => Compilation_PkgBody id dec fdec
      end)
    )
end.






















