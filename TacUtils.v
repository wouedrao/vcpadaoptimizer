

Ltac einversion :=
        repeat match goal with
           | [ H : _ = _ |- _ ] => solve [inversion H]
         end.
