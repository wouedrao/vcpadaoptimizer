(* *********************************************************************)
(*                                                                     *)
(*                 Coqlex verified lexer generator                     *)
(*                                                                     *)
(*  Copyright 2021 Siemens Mobility SAS and Institut National de       *)
(*  Recherche en Informatique et en Automatique.                       *)
(*  All rights reserved. This file is distributed under                *)
(*  the terms of the INRIA Non-Commercial License Agreement (see the   *)
(*  LICENSE file).                                                     *)
(*                                                                     *)
(* *********************************************************************)

Add LoadPath "RegExpLib" as RegExp.
Require Import SyntaxOptimizers PartialViewOptimizers Preprocessor InlinePragma Parser Lexer LexerDefinition.
Require Import ExtrOcamlBasic.
Require Import ExtrOcamlString.
Require Import ExtrOcamlZInt.
Require Import ExtrOcamlIntConv.
Require Import ExtrOcamlNatInt.
(* Extract Inlined Constant ConvergeOptim2.instruction_contain_table => "Table_helper.instruction_contain_table". *)
Extract Inlined Constant List.map => "List.map".
Extract Inlined Constant While_unroll.maxIter => "Consts.maxIter".
Extract Inlined Constant FlowOptim.default_fuel => "Consts.default_fuel".
Extract Inlined Constant InlinePragma.is_inlined => "AstOCAMLutils.is_inlined".
(* Extract Inlined Constant InlinePragma.validator => "Deciders.inline_decide". *)
Extraction Blacklist String List Main Char Int.
Extraction Language OCaml.
Cd "Extraction".
Separate Extraction convergeOptim1 convergeOptim2 convergeOptim3 flow_optimizer while_full_unroll_optimizer expression_simpl_optimizer extract fusionExternal eval_unit pragmaInlineOptim Lexer.token_stream Parser.goal_symbol lexbuf_from_string.
Cd "..".
