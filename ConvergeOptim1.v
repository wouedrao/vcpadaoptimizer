Require Import Map MapTypes Values Environment Event Semantics Semantics_f OptimUtils.
Require Import Ast.
Require Import List String ZArith Lia.
Import ListNotations.

Fixpoint optimize_instruction 
  (decider : Ast.instruction_s -> Ast.instruction_s -> bool)
  (i : Ast.instruction) : Ast.instruction :=
  match i with
  | Ast.INull => Ast.INull
  | Ast.IPragma p => Ast.IPragma p
  | Ast.IAssign sc aff => Ast.IAssign sc aff
  | Ast.IIf cond ins_s els_s => (Ast.IIf cond (optimize_instruction_s decider ins_s) (optimize_elif_instruction decider els_s))
  | Ast.ICase sc case_s => Ast.ICase sc (optimize_case_instruction decider case_s)
  | Ast.ILoop w ins_s => Ast.ILoop w (optimize_instruction_s decider ins_s)
  | _ => i
  end

with optimize_elif_instruction 
  (decider : Ast.instruction_s -> Ast.instruction_s -> bool)
  (function_parameter : Ast.elif_instruction) : Ast.elif_instruction :=
  match function_parameter with
  | Ast.IElse ins_s => Ast.IElse (optimize_instruction_s decider ins_s)
  | Ast.IElif cond ins_s els_s => 
      Ast.IElif cond (optimize_instruction_s decider ins_s) (optimize_elif_instruction decider els_s)
  end

with optimize_instruction_s 
  (decider : Ast.instruction_s -> Ast.instruction_s -> bool)
  (function_parameter : Ast.instruction_s) : Ast.instruction_s :=
  match function_parameter with
  | Ast.ISeqEnd ins => Ast.ISeqEnd (optimize_instruction decider ins)
  | Ast.ISeqCons h t =>
      let h_opt := optimize_instruction decider h in 
      let t_opt := optimize_instruction_s decider t in
      let default_result := (Ast.ISeqCons h_opt t_opt) in
      (
        match t_opt with
        | (Ast.ISeqCons (Ast.SOInstructionConverge _) (Ast.ISeqEnd (SOInstructionEndBranch _))) =>
              (
                if decider (Ast.ISeqEnd h_opt) default_result then
                  (Ast.ISeqEnd h_opt)
                else
                  default_result
              )
        | _ => default_result
        end
      )
  end

with optimize_case_instruction 
  (decider : Ast.instruction_s -> Ast.instruction_s -> bool)
  (function_parameter : Ast.case_instruction) : Ast.case_instruction :=
  match function_parameter with
  | Ast.ICaseFinishDefault ins_s => Ast.ICaseFinishDefault (optimize_instruction_s decider ins_s)
  | Ast.ICaseCons (exp, ins_s) case_s => Ast.ICaseCons (exp, optimize_instruction_s decider ins_s) 
        (optimize_case_instruction decider case_s)
  end.
  
(* Fixpoint optimize_instruction_correct indata ins res d
  (H : evalInstruction indata ins res) {struct ins} : forall opt,
  opt = optimize_instruction d ins ->
  evalInstruction indata opt res

with optimize_instruction_s_correct indata ins res d
  (H : evalInstruction_s indata ins res) {struct ins} : forall opt,
  opt = optimize_instruction_s d ins ->
  evalInstruction_s indata opt res

with optimize_elif_instruction_correct indata ins res d
  (H : evalElif_instruction indata ins res) {struct ins} : forall opt,
  opt = optimize_elif_instruction d ins ->
  evalElif_instruction indata opt res

with optimize_case_instruction_correct e indata ins res d
  (H : evalCase_instruction indata e ins res) {struct ins} : forall opt,
  opt = optimize_case_instruction d ins ->
  evalCase_instruction indata e opt res.
Proof.
induction ins; simpl; intros; subst; try exact H.
inversion H; subst.
eapply evalInstructionIIfTrue; eauto.
eapply evalInstructionIIfFalse; eauto.
inversion H; subst.
eapply evalInstructionICase; eauto.
inversion H; subst.
eapply evalInstructionILoopTrueOk; eauto.
app *)

(* Lemma replace_loop_body': forall i i',
  (forall m res, evalInstruction_s m i res -> evalInstruction_s m i' res) ->
  (forall c m res p p', p = (ILoop c i) -> p' = (ILoop c i') -> evalInstruction m p res -> evalInstruction m p' res).
Proof.
intros.
induction H2; inversion H0; subst.
econstructor; eauto.
eapply evalInstructionILoopTrueError; eauto.
eapply evalInstructionILoopFalse; eauto.
Qed.

Lemma replace_loop_body: forall i i',
  (forall m res, evalInstruction_s m i res -> evalInstruction_s m i' res) ->
  (forall c m res, evalInstruction m (ILoop c i) res -> evalInstruction m (ILoop c i') res).
Proof.
intros.
eapply replace_loop_body'; eauto.
Qed.

Lemma optimize_instruction_correct : forall ins indata res d,
  evalInstruction indata ins res -> forall opt,
  opt = optimize_instruction d ins ->
  evalInstruction indata opt res

with optimize_instruction_s_correct : forall ins indata res d,
  evalInstruction_s indata ins res -> forall opt,
  opt = optimize_instruction_s d ins ->
  evalInstruction_s indata opt res

with optimize_elif_instruction_correct : forall ins indata res d,
  evalElif_instruction indata ins res -> forall opt,
  opt = optimize_elif_instruction d ins ->
  evalElif_instruction indata opt res

with optimize_case_instruction_correct : forall ins e indata res d,
  evalCase_instruction indata e ins res -> forall opt,
  opt = optimize_case_instruction d ins ->
  evalCase_instruction indata e opt res.
Proof.
induction ins; simpl; intros; subst; try exact H; inversion H; subst.
eapply evalInstructionIIfTrue; eauto.
eapply evalInstructionIIfFalse; eauto.
eapply evalInstructionICase; eauto.
eapply evalInstructionILoopTrueOk; eauto.
apply replace_loop_body with (i := i).
intros.
eapply optimize_instruction_s_correct; eauto.
exact H6.
eapply evalInstructionILoopTrueError; eauto.
eapply evalInstructionILoopFalse; eauto.

induction ins; simpl; intros; try exact H; inversion H.
subst.
eapply evalInstruction_sISeqEnd; eauto.
subst ins i res0 indata.
assert ( opt = ISeqCons (optimize_instruction d h) (optimize_instruction_s d t)
    \/
      (exists v v', optimize_instruction_s d t = ISeqCons (ICall (SOInstructionConverge v)) (ISeqEnd (ICall (SOInstructionEndBranch v')))) 
    ).
induction (optimize_instruction_s d t); auto.
clear IHi.
induction i; auto.
induction p; auto.
induction i0; eauto.
induction i; eauto.
induction p; eauto.
destruct H1.

rewrite H1.
eapply evalInstruction_sISeqConsOk with (env':=env') (evn':=evn'); auto.
eapply optimize_instruction_correct with (ins := h) (d:=d); auto.
eapply IHins.
exact H6.
eauto.

destruct H1.
destruct H1.
rewrite H1 in H0.
apply eq_sym in H1.
assert (D := IHins (env', evn') res d H6 (ISeqCons (ICall (SOInstructionConverge x)) (ISeqEnd (ICall (SOInstructionEndBranch x0)))) H1).
inversion D; subst.
inversion H9; subst.
inversion H7; subst.
inversion H10; subst.
inversion H8; subst.
inversion H11; subst.
induction (d (ISeqEnd (optimize_instruction d h))
         (ISeqCons (optimize_instruction d h)
            (ISeqCons (ICall (SOInstructionConverge x)) (ISeqEnd (ICall (SOInstructionEndBranch x0)))))); subst.
econstructor.
eapply optimize_instruction_correct with (ins := h) (d:=d); auto.
eapply evalInstruction_sISeqConsOk with (env':=env'0) (evn':=evn'0); auto.
eapply optimize_instruction_correct with (ins := h) (d:=d); auto.
inversion H9; subst.
inversion H7; subst.
subst ins i res indata.
assert ( opt = ISeqCons (optimize_instruction d h) (optimize_instruction_s d t)
    \/
      (exists v v', optimize_instruction_s d t = ISeqCons (ICall (SOInstructionConverge v)) (ISeqEnd (ICall (SOInstructionEndBranch v')))) 
    ).
induction (optimize_instruction_s d t); auto.
clear IHi.
induction i; auto.
induction p; auto.
induction i0; eauto.
induction i; eauto.
induction p; eauto.
destruct H1.
rewrite H1.
eapply evalInstruction_sISeqConsError; auto.
eapply optimize_instruction_correct with (ins := h) (d:=d); auto.
destruct H1, H1.
rewrite H1 in H0.
induction (d (ISeqEnd (optimize_instruction d h))
         (ISeqCons (optimize_instruction d h)
            (ISeqCons (ICall (SOInstructionConverge x)) (ISeqEnd (ICall (SOInstructionEndBranch x0)))))); subst.
econstructor.
eapply optimize_instruction_correct with (ins := h) (d:=d); auto.
eapply evalInstruction_sISeqConsError; auto.
eapply optimize_instruction_correct with (ins := h) (d:=d); auto.

induction ins; simpl; intros; subst; try exact H; inversion H; subst.
econstructor.
eapply optimize_instruction_s_correct; eauto.
eapply evalInstruction_sIElifTrue; eauto.
eapply evalInstruction_sIElifFalse. 
exact H5.
apply (IHins (env, evn) res d).
exact H6.
auto.

induction ins; simpl; intros; subst; try exact H; inversion H; subst.
econstructor.
eapply optimize_instruction_s_correct; eauto.
eapply evalICaseFinishDefaultErr.
eapply optimize_instruction_s_correct; eauto.
eapply evalICaseConsTrue.
exact H4.
eapply optimize_instruction_s_correct; eauto.
eapply evalICaseConsFalse.
exact H4.
eapply IHins; eauto.
Qed. *)

(* Here
 *)
Lemma optimize_inst_correct: (forall indata ins res,
  evalInstruction indata ins res -> forall d opt,
  opt = optimize_instruction d ins ->
  evalInstruction indata opt res) /\ (forall indata ins res,
  evalInstruction_s indata ins res -> forall d opt,
  opt = optimize_instruction_s d ins ->
  evalInstruction_s indata opt res) /\ (forall indata ins res,
  evalElif_instruction indata ins res -> forall d opt,
  opt = optimize_elif_instruction d ins ->
  evalElif_instruction indata opt res) /\ (forall indata e ins res,
  evalCase_instruction indata e ins res -> forall d opt,
  opt = optimize_case_instruction d ins ->
  evalCase_instruction indata e opt res).
Proof.
apply sem_inst_mutind; intros.
subst; apply evalInstructionINull.
subst; apply evalInstructionIPragmaPragma; auto.
subst; apply evalInstructionIPragmaFile; auto.
subst; apply evalInstructionIPragmaSig; auto.
subst; eapply evalInstructionIAssign; eauto.
subst; eapply evalInstructionIIfTrue; eauto.
subst; eapply evalInstructionIIfFalse; eauto.
subst; eapply evalInstructionICase; eauto.
simpl in H1, H0.
subst; eapply evalInstructionILoopTrueOk; eauto.
simpl in H, H0.
subst; eapply evalInstructionILoopTrueError; eauto.
subst; eapply evalInstructionILoopFalse; eauto.

simpl in *.
subst; econstructor; eauto.
subst; econstructor; eauto.
subst; econstructor; eauto.
subst; econstructor; eauto.
subst; eapply evalCallExtFunctionWithParamsOK; eauto.
subst; eapply evalCallExtFunctionWithParamsError; eauto.
subst; eapply evalCallExtFunctionNoParamsOK; eauto.
subst; eapply evalCallExtFunctionNoParamsError; eauto.
subst; eapply evalCallUnknownFunctionWithParamsOK; eauto.
subst; eapply evalCallUnknownFunctionWithParamsError; eauto.
subst; eapply evalCallUnknownFunctionNoParamsOK; eauto.
subst; eapply evalCallUnknownFunctionNoParamsError; eauto.
subst; econstructor; eauto.
subst; econstructor; eauto.
subst; econstructor; eauto.
subst; econstructor; eauto.
subst; econstructor; eauto.
subst; econstructor; eauto.
subst; econstructor; eauto.
subst; econstructor; eauto.
subst; econstructor; eauto.
subst; econstructor; eauto.
subst; econstructor; eauto.
subst; econstructor; eauto.
subst; econstructor; eauto.
subst; econstructor; eauto.
subst; econstructor; eauto.
subst; eapply evalSOInstructionReadMessFalse; eauto.
subst; econstructor; eauto.
subst; eapply evalSOInstructionReadMessUnfilFalse; eauto.
subst; econstructor; eauto.
subst; econstructor; eauto.
subst; econstructor; eauto.
subst; econstructor; eauto.
subst; econstructor; eauto.
subst; econstructor; eauto.

subst; econstructor; eauto.
simpl in *.
assert ( opt = ISeqCons (optimize_instruction d h) (optimize_instruction_s d t)
    \/
      (exists v v', optimize_instruction_s d t = ISeqCons (SOInstructionConverge v) (ISeqEnd (SOInstructionEndBranch v')))
    ).
induction (optimize_instruction_s d t); auto.
clear IHi.
induction i; auto.
induction i0; eauto.
induction i; eauto.
destruct H2.

rewrite H2.
eapply evalInstruction_sISeqConsOk with (env':=env') (evn':=evn'); auto.
eapply H; auto.
eapply H0; eauto.

destruct H2.
destruct H2.
rewrite H2 in H1.
apply eq_sym in H2.
assert (D := H0 d (ISeqCons (SOInstructionConverge x) (ISeqEnd (SOInstructionEndBranch x0))) H2).
inversion D; subst.
inversion H8; subst.
inversion H9; subst.
inversion H6; subst.
induction (d (ISeqEnd (optimize_instruction d h))
         (ISeqCons (optimize_instruction d h)
            (ISeqCons (SOInstructionConverge x) (ISeqEnd (SOInstructionEndBranch x0))))); subst.
econstructor; eauto.
eapply evalInstruction_sISeqConsOk with (env':=env'0) (evn':=evn'0); auto.
eapply H; eauto.
inversion H8; subst.
simpl in *.
assert ( opt = ISeqCons (optimize_instruction d h) (optimize_instruction_s d t)
    \/
      (exists v v', optimize_instruction_s d t = ISeqCons (SOInstructionConverge v) (ISeqEnd (SOInstructionEndBranch v'))) 
    ).
induction (optimize_instruction_s d t); auto.
clear IHi.
induction i; auto.
induction i0; eauto.
induction i; eauto.
destruct H1.
rewrite H1.
eapply evalInstruction_sISeqConsError; eauto.
destruct H1, H1.
rewrite H1 in H0.
induction (d (ISeqEnd (optimize_instruction d h))
         (ISeqCons (optimize_instruction d h)
            (ISeqCons (SOInstructionConverge x) (ISeqEnd (SOInstructionEndBranch x0))))); subst.
econstructor; eauto.
eapply evalInstruction_sISeqConsError; eauto.

subst; econstructor; eauto.
subst; econstructor; eauto.
subst; eapply evalInstruction_sIElifFalse; eauto. 

subst; econstructor; eauto.
subst; eapply evalICaseFinishDefaultErr; eauto.
subst; eapply evalICaseConsTrue; eauto.
subst; eapply evalICaseConsFalse; eauto.
Qed.

Definition convergeoptim1_optimize d ins := fst (optimize_end_branch_number_instruction_s 0 (optimize_instruction_s d ins)).

Theorem convergeoptim1_optimize_instruction_s_correct : forall indata ins res d,
  evalInstruction_s indata ins res -> forall opt,
  opt = convergeoptim1_optimize d ins ->
  evalInstruction_s indata opt res.
Proof.
intros.
unfold convergeoptim1_optimize in *.
assert (evalInstruction_s indata (optimize_instruction_s d ins) res) by (eapply optimize_inst_correct; eauto).
subst; eapply optimize_end_branch_number_instruction_s_correct; eauto.
Qed.

(* Fixpoint optimize_instruction_correct indata ins res d
  (H : evalInstruction indata ins res) {struct H} : forall opt,
  opt = optimize_instruction d ins ->
  evalInstruction indata opt res

with optimize_instruction_s_correct indata ins res d
  (H : evalInstruction_s indata ins res) {struct H} : forall opt,
  opt = optimize_instruction_s d ins ->
  evalInstruction_s indata opt res

with optimize_elif_instruction_correct indata ins res d
  (H : evalElif_instruction indata ins res) {struct H} : forall opt,
  opt = optimize_elif_instruction d ins ->
  evalElif_instruction indata opt res

with optimize_case_instruction_correct e indata ins res d
  (H : evalCase_instruction indata e ins res) {struct H} : forall opt,
  opt = optimize_case_instruction d ins ->
  evalCase_instruction indata e opt res.
Proof.
destruct H; intros; simpl in *; subst.
apply evalInstructionINull.
apply evalInstructionIPragmaPragma; auto.
apply evalInstructionIPragmaFile; auto.
apply evalInstructionIPragmaSig; auto.
eapply evalInstructionIAssign; eauto.
eapply evalInstructionIIfTrue; eauto.
eapply evalInstructionIIfFalse; eauto.
eapply evalInstructionICase; eauto.
eapply evalInstructionILoopTrueOk; eauto.
apply (optimize_instruction_correct (env', evn') (ILoop cond ins) res d H1).
simpl; auto.
eapply evalInstructionILoopTrueError; eauto.
eapply evalInstructionILoopFalse; eauto.
eapply evalInstructionICall; eauto.

destruct H; intros; simpl in *.
subst.
eapply evalInstruction_sISeqEnd; eauto.
assert ( opt = ISeqCons (optimize_instruction d h) (optimize_instruction_s d t)
    \/
      (exists v v', optimize_instruction_s d t = ISeqCons (ICall (SOInstructionConverge v)) (ISeqEnd (ICall (SOInstructionEndBranch v')))) 
    ).
induction (optimize_instruction_s d t); auto.
clear IHi.
induction i; auto.
induction p; auto.
induction i0; eauto.
induction i; eauto.
induction p; eauto.
destruct H2.

rewrite H2.
eapply evalInstruction_sISeqConsOk with (env':=env') (evn':=evn'); auto.
eapply optimize_instruction_correct with (ins := h) (d:=d); auto.
apply optimize_instruction_s_correct with (ins := t) (d:=d).
exact H0.
auto.

destruct H2.
destruct H2.
rewrite H2 in H1.
apply eq_sym in H2.
assert (D := optimize_instruction_s_correct (env', evn') t res d H0 (ISeqCons (ICall (SOInstructionConverge x)) (ISeqEnd (ICall (SOInstructionEndBranch x0)))) H2).
inversion D; subst.
inversion H8; subst.
inversion H6; subst.
inversion H9; subst.
inversion H7; subst.
inversion H10; subst.
induction (d (ISeqEnd (optimize_instruction d h))
         (ISeqCons (optimize_instruction d h)
            (ISeqCons (ICall (SOInstructionConverge x)) (ISeqEnd (ICall (SOInstructionEndBranch x0)))))); subst.
econstructor.
eapply optimize_instruction_correct with (ins := h) (d:=d); auto.
eapply evalInstruction_sISeqConsOk with (env':=env'0) (evn':=evn'0); auto.
eapply optimize_instruction_correct with (ins := h) (d:=d); auto.
inversion H8; subst.
inversion H6; subst.
assert ( opt = ISeqCons (optimize_instruction d h) (optimize_instruction_s d t)
    \/
      (exists v v', optimize_instruction_s d t = ISeqCons (ICall (SOInstructionConverge v)) (ISeqEnd (ICall (SOInstructionEndBranch v')))) 
    ).
induction (optimize_instruction_s d t); auto.
clear IHi.
induction i; auto.
induction p; auto.
induction i0; eauto.
induction i; eauto.
induction p; eauto.
destruct H1.
rewrite H1.
eapply evalInstruction_sISeqConsError; auto.
eapply optimize_instruction_correct with (ins := h) (d:=d); auto.
destruct H1, H1.
rewrite H1 in H0.
induction (d (ISeqEnd (optimize_instruction d h))
         (ISeqCons (optimize_instruction d h)
            (ISeqCons (ICall (SOInstructionConverge x)) (ISeqEnd (ICall (SOInstructionEndBranch x0)))))); subst.
econstructor.
eapply optimize_instruction_correct with (ins := h) (d:=d); auto.
eapply evalInstruction_sISeqConsError; auto.
eapply optimize_instruction_correct with (ins := h) (d:=d); auto.

destruct H; intros; simpl in *; subst.
econstructor.
eapply optimize_instruction_s_correct; eauto.
eapply evalInstruction_sIElifTrue; eauto.
eapply evalInstruction_sIElifFalse. 
exact H.
apply (optimize_elif_instruction_correct (env, evn) ins_false res d H0).
auto.

destruct H; intros; simpl in *; subst.
econstructor.
eapply optimize_instruction_s_correct; eauto.
eapply evalICaseFinishDefaultErr.
eapply optimize_instruction_s_correct; eauto.
eapply evalICaseConsTrue.
exact H.
eapply optimize_instruction_s_correct; eauto.
eapply evalICaseConsFalse.
exact H.
apply (optimize_case_instruction_correct compvalue (env, evn) case_t res d H0).
auto.
Qed. *)





