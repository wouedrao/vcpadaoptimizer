Require Import Map MapTypes Values Environment Event Semantics Semantics TacUtils.
Require Import Ast.
Require Import List String ZArith Lia.
Import ListNotations.
Local Open Scope Z_scope.


Fixpoint optimize_end_branch_number_instruction c_no
  (i : Ast.instruction) : Ast.instruction * Z  :=
  match i with
  | Ast.IIf cond ins_s els_s => 
      let (n_ins_s, current_n) := optimize_end_branch_number_instruction_s c_no ins_s in
	    let (n_els_s, current_n') := optimize_end_branch_number_elif_instruction current_n els_s in
      (Ast.IIf cond n_ins_s n_els_s, current_n')
  | Ast.ICase sc case_s => 
      let (n_ins_s, current_n) := optimize_end_branch_number_case_instruction c_no case_s in
      (Ast.ICase sc n_ins_s, current_n)
  | Ast.ILoop w ins_s => 
      let (n_ins_s, current_n) := optimize_end_branch_number_instruction_s c_no ins_s in
      (Ast.ILoop w n_ins_s, current_n)
  | i => (i, c_no)
  end
with optimize_end_branch_number_elif_instruction 
  c_no
  (i : Ast.elif_instruction) : Ast.elif_instruction * Z :=
  match i with
  | Ast.IElse ins_s => 
    let (n_ins_s, current_n) := optimize_end_branch_number_instruction_s c_no ins_s in
    (Ast.IElse n_ins_s, current_n)
  | Ast.IElif cond ins_s els_s => 
    let (n_ins_s, current_n) := optimize_end_branch_number_instruction_s c_no ins_s in
    let (n_els_s, current_n') := optimize_end_branch_number_elif_instruction current_n els_s in
    (Ast.IElif cond n_ins_s n_els_s, current_n')
  end
with optimize_end_branch_number_instruction_s c_no i : Ast.instruction_s * Z := 
  match i with
	| ISeqEnd (SOInstructionEndBranch _) => (ISeqEnd (SOInstructionEndBranch (ELit (Int_lit c_no))), (c_no + 1))
	| ISeqEnd ins => 
		let (n_ins, current_n) := optimize_end_branch_number_instruction c_no ins in
		(ISeqEnd n_ins, current_n)
	| ISeqCons ins ins_s => 
		let (n_ins, _) := optimize_end_branch_number_instruction 0 ins in
		let (n_ins_s, current_n) := optimize_end_branch_number_instruction_s c_no ins_s in
		(ISeqCons n_ins n_ins_s, current_n)
  end
with optimize_end_branch_number_case_instruction c_no i : Ast.case_instruction * Z := 
  match i with
	| ICaseFinishDefault ( ins_s ) => 
		let (n_ins_s, current_n) := optimize_end_branch_number_instruction_s c_no ins_s in
		(ICaseFinishDefault n_ins_s, current_n)
	| ICaseCons (c,  ins_s) case_s => 
		let (n_ins_s, current_n) := optimize_end_branch_number_instruction_s c_no ins_s in
		let (n_case_s, current_n) := optimize_end_branch_number_case_instruction current_n case_s in
		(ICaseCons (c, n_ins_s) n_case_s, current_n)
  end
  .
  
(*Compute (fst (optimize_end_branch_number_instruction_s 0 
  (ISeqEnd 
    (Ast.IIf SOIs_valid 
      (ISeqEnd 
        (Ast.IIf SOIs_valid 
          (ISeqEnd (ICall (SOInstructionEndBranch (ELit (Int_lit 0))))) 
          (Ast.IElse (ISeqEnd (ICall (SOInstructionEndBranch (ELit (Int_lit 0))))))
        )
      )
      (Ast.IElse 
        (ISeqEnd 
          (Ast.IIf SOIs_valid 
            (ISeqEnd (ICall (SOInstructionEndBranch (ELit (Int_lit 0))))) 
            (Ast.IElse (ISeqEnd (ICall (SOInstructionEndBranch (ELit (Int_lit 0))))))
          )
        )
      )
    )
  )
  )).*)

Lemma optimize_end_branch_number_instruction_correct :
  forall indata ins res,
  evalInstruction indata ins res -> forall d,
  evalInstruction indata (fst (optimize_end_branch_number_instruction d ins)) res

with optimize_end_branch_number_instruction_s_correct :
  forall indata ins res,
  evalInstruction_s indata ins res -> forall d,
  evalInstruction_s indata (fst (optimize_end_branch_number_instruction_s d ins)) res

with optimize_elif_instruction_correct :
  forall indata ins res,
  evalElif_instruction indata ins res -> forall d,
  evalElif_instruction indata (fst (optimize_end_branch_number_elif_instruction d ins)) res

with optimize_case_instruction_correct :
  forall e indata ins res,
  evalCase_instruction indata e ins res -> forall d,
  evalCase_instruction indata e (fst (optimize_end_branch_number_case_instruction d ins)) res.
Proof.
induction 1; intros; simpl in *; subst.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
case_eq (optimize_end_branch_number_instruction_s d ins_if); intros.
case_eq (optimize_end_branch_number_elif_instruction z ins_else); intros.
simpl.
eapply evalInstructionIIfTrue; eauto.
assert (i = fst (optimize_end_branch_number_instruction_s d ins_if)) by (rewrite H1; auto).
rewrite H3.
eapply optimize_end_branch_number_instruction_s_correct with (d:=d); eauto.
case_eq (optimize_end_branch_number_instruction_s d ins_if); intros.
case_eq (optimize_end_branch_number_elif_instruction z ins_else); intros.
simpl.
eapply evalInstructionIIfFalse; eauto.
assert (e = fst (optimize_end_branch_number_elif_instruction z ins_else)) by (rewrite H2; auto).
rewrite H3.
eapply optimize_elif_instruction_correct with (d:=z); eauto.
case_eq (optimize_end_branch_number_case_instruction d case); intros.
simpl.
econstructor; eauto.
assert (c = fst (optimize_end_branch_number_case_instruction d case)) by (rewrite H0; auto).
rewrite H1.
eapply optimize_case_instruction_correct with (d:=d); eauto.
case_eq (optimize_end_branch_number_instruction_s d ins); intros.
assert (D := optimize_end_branch_number_instruction_s_correct (env, evn) ins (OK env' evn') H0 d).
rewrite H2 in *.
simpl in *.
eapply evalInstructionILoopTrueOk; eauto.
assert (D' := IHevalInstruction d).
rewrite H2 in *.
auto.
case_eq (optimize_end_branch_number_instruction_s d ins); intros.
assert (D := optimize_end_branch_number_instruction_s_correct (env, evn) ins (Error e env' evn') H0 d).
rewrite H1 in *.
simpl in *.
eapply evalInstructionILoopTrueError; eauto.
case_eq (optimize_end_branch_number_instruction_s d ins); intros.
simpl.
eapply evalInstructionILoopFalse; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
eapply evalCallExtFunctionWithParamsOK; eauto.
eapply evalCallExtFunctionWithParamsError; eauto.
eapply evalCallExtFunctionNoParamsOK; eauto.
eapply evalCallExtFunctionNoParamsError; eauto.
eapply evalCallUnknownFunctionWithParamsOK; eauto.
eapply evalCallUnknownFunctionWithParamsError; eauto.
eapply evalCallUnknownFunctionNoParamsOK; eauto.
eapply evalCallUnknownFunctionNoParamsError; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
eapply evalSOInstructionReadMessFalse; eauto.
econstructor; eauto.
subst; eapply evalSOInstructionReadMessUnfilFalse; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.

induction 1; intros; simpl in *; subst.
case_eq (optimize_end_branch_number_instruction d i); intros.
assert (D := optimize_end_branch_number_instruction_correct (env, evn) i res H d).
rewrite H0 in *.
simpl in *.
induction i; simpl; try (eapply evalInstruction_sISeqEnd; eauto).
simpl in H0; inversion H0; subst.
inversion H; subst.
econstructor; eauto.
case_eq (optimize_end_branch_number_instruction 0 h); intros.
assert (D := optimize_end_branch_number_instruction_correct (env, evn) h (OK env' evn') H 0).
case_eq (optimize_end_branch_number_instruction_s d t); intros.
assert (D' := IHevalInstruction_s d).
rewrite H1, H2 in *.
simpl in *.
eapply evalInstruction_sISeqConsOk; eauto.
assert (D := optimize_end_branch_number_instruction_correct (env, evn) h (Error e env' evn') H 0).
case_eq (optimize_end_branch_number_instruction 0 h); intros; rewrite H0 in *.
case_eq (optimize_end_branch_number_instruction_s d t); intros.
simpl in *.
eapply evalInstruction_sISeqConsError; eauto.

induction 1; intros; simpl in *; subst.
assert (D := optimize_end_branch_number_instruction_s_correct (env, evn) i res H d).
case_eq (optimize_end_branch_number_instruction_s d i); intros; rewrite H0 in *.
simpl in *.
econstructor; eauto.
assert (D := optimize_end_branch_number_instruction_s_correct (env, evn) ins_true res H0 d).
case_eq (optimize_end_branch_number_instruction_s d ins_true); intros; rewrite H1 in *.
induction (optimize_end_branch_number_elif_instruction z ins_false); simpl in *.
eapply evalInstruction_sIElifTrue; eauto.
case_eq (optimize_end_branch_number_instruction_s d ins_true); intros.
assert (D := IHevalElif_instruction z).
case_eq (optimize_end_branch_number_elif_instruction z ins_false); intros; rewrite H2 in *; simpl in *.
eapply evalInstruction_sIElifFalse; eauto.

induction 1; intros; simpl in *; subst.
assert (D := optimize_end_branch_number_instruction_s_correct (env, evn) ins_s (OK env' evn') H d).
case_eq (optimize_end_branch_number_instruction_s d ins_s); intros; rewrite H0 in *; simpl in *.
eapply evalICaseFinishDefaultOk; eauto.
assert (D := optimize_end_branch_number_instruction_s_correct (env, evn) ins_s (Error e env' evn') H d).
case_eq (optimize_end_branch_number_instruction_s d ins_s); intros; rewrite H0 in *; simpl in *.
eapply evalICaseFinishDefaultErr; eauto.
assert (D := optimize_end_branch_number_instruction_s_correct (env, evn) ins_s res H0 d).
case_eq (optimize_end_branch_number_instruction_s d ins_s); intros; rewrite H1 in *; simpl in *.
case_eq (optimize_end_branch_number_case_instruction z case_t); intros.
simpl in *.
eapply evalICaseConsTrue; eauto.
case_eq (optimize_end_branch_number_instruction_s d ins_s); intros.
assert (D := IHevalCase_instruction z).
case_eq (optimize_end_branch_number_case_instruction z case_t); intros; rewrite H2 in *; simpl in *.
eapply evalICaseConsFalse; eauto.
Qed.

Fixpoint remove_incorrect_end_iter_instruction (i : Ast.instruction) :=
  match i with
  | Ast.IIf cond ins_s els_s => 
      Ast.IIf cond (remove_incorrect_end_iter_instruction_s false ins_s) (remove_incorrect_end_iter_elif_instruction els_s)
  | Ast.ICase sc case_s => 
      Ast.ICase sc (remove_incorrect_end_iter_case_instruction case_s)
  | Ast.ILoop w ins_s => 
      Ast.ILoop w (remove_incorrect_end_iter_instruction_s true ins_s)
  | i => i
  end
with remove_incorrect_end_iter_elif_instruction (i : Ast.elif_instruction) :=
  match i with
  | Ast.IElse ins_s => Ast.IElse (remove_incorrect_end_iter_instruction_s false ins_s)
  | Ast.IElif cond ins_s els_s => Ast.IElif cond (remove_incorrect_end_iter_instruction_s false ins_s) (remove_incorrect_end_iter_elif_instruction els_s)
  end
with remove_incorrect_end_iter_instruction_s in_loop i := 
  match i with
	| ISeqEnd SOInstructionEndIter => if in_loop then ISeqEnd SOInstructionEndIter else ISeqEnd INull
	| ISeqEnd h => ISeqEnd (remove_incorrect_end_iter_instruction h)
	| ISeqCons SOInstructionEndIter t => remove_incorrect_end_iter_instruction_s in_loop t
  | ISeqCons ins ins_s => ISeqCons (remove_incorrect_end_iter_instruction ins) (remove_incorrect_end_iter_instruction_s in_loop ins_s)
  end
with remove_incorrect_end_iter_case_instruction i := 
  match i with
	| ICaseFinishDefault ins_s => ICaseFinishDefault (remove_incorrect_end_iter_instruction_s false ins_s)
	| ICaseCons (c,  ins_s) case_s => ICaseCons (c,  remove_incorrect_end_iter_instruction_s false ins_s) (remove_incorrect_end_iter_case_instruction case_s)
  end
  .

Lemma remove_incorrect_end_iter_instruction_correct :
  forall indata ins res,
  evalInstruction indata ins res -> 
  evalInstruction indata (remove_incorrect_end_iter_instruction ins) res

with remove_incorrect_end_iter_instruction_s_correct :
  forall indata ins res,
  evalInstruction_s indata ins res -> forall in_loop,
  evalInstruction_s indata (remove_incorrect_end_iter_instruction_s in_loop ins) res

with remove_incorrect_end_iter_elif_instruction_correct :
  forall indata ins res,
  evalElif_instruction indata ins res -> 
  evalElif_instruction indata (remove_incorrect_end_iter_elif_instruction ins) res

with remove_incorrect_end_iter_case_instruction_correct :
  forall e indata ins res,
  evalCase_instruction indata e ins res -> 
  evalCase_instruction indata e (remove_incorrect_end_iter_case_instruction ins) res.
Proof.
induction 1; intros; simpl in *; subst.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
eapply evalInstructionIIfFalse; eauto.
econstructor; eauto.
econstructor; eauto.
eapply evalInstructionILoopTrueError; eauto.
eapply evalInstructionILoopFalse; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
eapply evalCallExtFunctionWithParamsOK; eauto.
eapply evalCallExtFunctionWithParamsError; eauto.
eapply evalCallExtFunctionNoParamsOK; eauto.
eapply evalCallExtFunctionNoParamsError; eauto.
eapply evalCallUnknownFunctionWithParamsOK; eauto.
eapply evalCallUnknownFunctionWithParamsError; eauto.
eapply evalCallUnknownFunctionNoParamsOK; eauto.
eapply evalCallUnknownFunctionNoParamsError; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
eapply evalSOInstructionReadMessFalse; eauto.
econstructor; eauto.
subst; eapply evalSOInstructionReadMessUnfilFalse; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.

induction 1; intros; simpl in *; subst.
induction i; try (eapply evalInstruction_sISeqEnd; eauto).
induction in_loop; eapply evalInstruction_sISeqEnd; eauto.
inversion H; subst.
econstructor; eauto.
induction h; try (eapply evalInstruction_sISeqConsOk; eauto).
inversion H; subst; eauto.
induction h; try (eapply evalInstruction_sISeqConsError; eauto).
inversion H; subst.

induction 1; intros; simpl in *; subst.
econstructor; eauto.
econstructor; eauto.
eapply evalInstruction_sIElifFalse; eauto.

induction 1; intros; simpl in *; subst.
econstructor; eauto.
eapply evalICaseFinishDefaultErr; eauto.
eapply evalICaseConsTrue; eauto.
eapply evalICaseConsFalse; eauto.
Qed.

Fixpoint remove_lastnull_instruction (i : Ast.instruction) :=
  match i with
  | Ast.IIf cond ins_s els_s => 
      Ast.IIf cond (remove_lastnull_instruction_s ins_s) (remove_lastnull_elif_instruction els_s)
  | Ast.ICase sc case_s => 
      Ast.ICase sc (remove_lastnull_case_instruction case_s)
  | Ast.ILoop w ins_s => 
      Ast.ILoop w (remove_lastnull_instruction_s ins_s)
  | i => i
  end
with remove_lastnull_elif_instruction (i : Ast.elif_instruction) :=
  match i with
  | Ast.IElse ins_s => Ast.IElse (remove_lastnull_instruction_s ins_s)
  | Ast.IElif cond ins_s els_s => Ast.IElif cond (remove_lastnull_instruction_s ins_s) (remove_lastnull_elif_instruction els_s)
  end
with remove_lastnull_instruction_s i := 
  match i with
	| ISeqEnd h => ISeqEnd (remove_lastnull_instruction h)
  | ISeqCons ins ins_s => 
    let t_opt := remove_lastnull_instruction_s ins_s in
    match t_opt with
    | ISeqEnd INull => ISeqEnd (remove_lastnull_instruction ins)
    | _ => ISeqCons (remove_lastnull_instruction ins) t_opt
    end
  end
with remove_lastnull_case_instruction i := 
  match i with
	| ICaseFinishDefault ins_s => ICaseFinishDefault (remove_lastnull_instruction_s ins_s)
	| ICaseCons (c,  ins_s) case_s => ICaseCons (c,  remove_lastnull_instruction_s ins_s) (remove_lastnull_case_instruction case_s)
  end
  .

Lemma remove_lastnull_instruction_correct :
  forall indata ins res,
  evalInstruction indata ins res -> 
  evalInstruction indata (remove_lastnull_instruction ins) res

with remove_lastnull_instruction_s_correct :
  forall indata ins res,
  evalInstruction_s indata ins res ->
  evalInstruction_s indata (remove_lastnull_instruction_s ins) res

with remove_lastnull_elif_instruction_correct :
  forall indata ins res,
  evalElif_instruction indata ins res -> 
  evalElif_instruction indata (remove_lastnull_elif_instruction ins) res

with remove_lastnull_case_instruction_correct :
  forall e indata ins res,
  evalCase_instruction indata e ins res -> 
  evalCase_instruction indata e (remove_lastnull_case_instruction ins) res.
Proof.
induction 1; intros; simpl in *; subst.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
eapply evalInstructionIIfFalse; eauto.
econstructor; eauto.
econstructor; eauto.
eapply evalInstructionILoopTrueError; eauto.
eapply evalInstructionILoopFalse; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
eapply evalCallExtFunctionWithParamsOK; eauto.
eapply evalCallExtFunctionWithParamsError; eauto.
eapply evalCallExtFunctionNoParamsOK; eauto.
eapply evalCallExtFunctionNoParamsError; eauto.
eapply evalCallUnknownFunctionWithParamsOK; eauto.
eapply evalCallUnknownFunctionWithParamsError; eauto.
eapply evalCallUnknownFunctionNoParamsOK; eauto.
eapply evalCallUnknownFunctionNoParamsError; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
eapply evalSOInstructionReadMessFalse; eauto.
econstructor; eauto.
subst; eapply evalSOInstructionReadMessUnfilFalse; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.

induction 1; intros; simpl in *; subst.
econstructor; eauto.
induction (remove_lastnull_instruction_s t); try (eapply evalInstruction_sISeqConsOk; eauto).
induction i; try (eapply evalInstruction_sISeqConsOk; eauto).
inversion IHevalInstruction_s; subst.
inversion H5; subst.
econstructor; eauto.
induction (remove_lastnull_instruction_s t); try (eapply evalInstruction_sISeqConsError; eauto).
induction i; try (eapply evalInstruction_sISeqConsError; eauto).
eapply evalInstruction_sISeqEnd; eauto.

induction 1; intros; simpl in *; subst.
econstructor; eauto.
econstructor; eauto.
eapply evalInstruction_sIElifFalse; eauto.

induction 1; intros; simpl in *; subst.
econstructor; eauto.
eapply evalICaseFinishDefaultErr; eauto.
eapply evalICaseConsTrue; eauto.
eapply evalICaseConsFalse; eauto.
Qed.

Fixpoint remove_incorrect_end_branch_in_list l := match l with
| ISeqEnd h => ISeqEnd h
| ISeqCons (SOInstructionEndBranch _) (ISeqEnd (SOInstructionConverge _)) => ISeqEnd INull
| ISeqCons (SOInstructionEndBranch _) (ISeqCons (SOInstructionConverge _) t0) => remove_incorrect_end_branch_in_list t0
| ISeqCons (SOInstructionEndBranch _) t0 => remove_incorrect_end_branch_in_list t0
| ISeqCons h t0 => ISeqCons h (remove_incorrect_end_branch_in_list t0)
end.

Lemma remove_incorrect_end_branch_in_list_correct : forall indata ins res,
  evalInstruction_s indata ins res ->
  evalInstruction_s indata (remove_incorrect_end_branch_in_list ins) res.
Proof.
induction 1; simpl in *; intros; eauto; try (solve [econstructor; eauto]).
induction h; eauto; try (solve [econstructor; eauto]).
induction t; eauto; try (solve [econstructor; eauto]).
inversion H; subst.
induction i; eauto; try (solve [econstructor; eauto]).
inversion H0; subst.
inversion H5; subst.
econstructor; eauto.
econstructor; eauto.
clear IHt.
inversion H; subst.
simpl in *.
induction i; eauto; try (solve [econstructor; eauto]).
inversion IHevalInstruction_s; subst.
inversion H6; subst; auto.
inversion H6; subst; auto.
induction h; eauto; try (solve [econstructor; eauto]).
inversion H; subst.
Qed.

Fixpoint remove_incorrect_end_iter_in_list l := match l with
| ISeqEnd h => ISeqEnd h
| ISeqCons (SOInstructionEndIter) (ISeqEnd (SOInstructionConverge _)) => ISeqEnd INull
| ISeqCons (SOInstructionEndIter) (ISeqCons (SOInstructionConverge _) t0) => remove_incorrect_end_iter_in_list t0
| ISeqCons (SOInstructionEndIter) t0 => remove_incorrect_end_iter_in_list t0
| ISeqCons h t0 => ISeqCons h (remove_incorrect_end_iter_in_list t0)
end.

Lemma remove_incorrect_end_iter_in_list_correct : forall indata ins res,
  evalInstruction_s indata ins res ->
  evalInstruction_s indata (remove_incorrect_end_iter_in_list ins) res.
Proof.
induction 1; simpl in *; intros; eauto; try (solve [econstructor; eauto]).
induction h; eauto; try (solve [econstructor; eauto]).
induction t; eauto; try (solve [econstructor; eauto]).
inversion H; subst.
induction i; eauto; try (solve [econstructor; eauto]).
inversion H0; subst.
inversion H5; subst.
econstructor; eauto.
econstructor; eauto.
clear IHt.
inversion H; subst.
simpl in *.
induction i; eauto; try (solve [econstructor; eauto]).
inversion IHevalInstruction_s; subst.
inversion H6; subst; auto.
inversion H6; subst; auto.
induction h; eauto; try (solve [econstructor; eauto]).
inversion H; subst.
Qed.

Fixpoint remove_incorrect_begin_loop_in_list l := match l with
| ISeqEnd h => ISeqEnd h
| ISeqCons (SOInstructionBeginLoop e) (ISeqEnd (ILoop cond ins)) => ISeqCons (SOInstructionBeginLoop e) (ISeqEnd (ILoop cond ins))
| ISeqCons (SOInstructionBeginLoop e) (ISeqCons (ILoop cond ins) t0) => 
    ISeqCons (SOInstructionBeginLoop e) (ISeqCons (ILoop cond ins) (remove_incorrect_begin_loop_in_list t0))
| ISeqCons (SOInstructionBeginLoop e) t0 => remove_incorrect_begin_loop_in_list t0
| ISeqCons h t0 => ISeqCons h (remove_incorrect_begin_loop_in_list t0)
end.


Lemma remove_incorrect_begin_loop_in_list_correct : forall indata ins res,
  evalInstruction_s indata ins res ->
  evalInstruction_s indata (remove_incorrect_begin_loop_in_list ins) res.
Proof.
induction 1; simpl in *; intros; eauto; try (solve [econstructor; eauto]).
induction h; eauto; try (solve [econstructor; eauto]).
induction t; eauto; try (solve [econstructor; eauto]).
inversion H; subst.
induction i; eauto; try (solve [econstructor; eauto]).
clear IHt.
inversion H; subst.
simpl in *.
induction i; eauto; try (solve [econstructor; eauto]).
induction h; eauto; try (solve [econstructor; eauto]).
inversion H; subst.
Qed.

Fixpoint remove_incorrect_converge_in_list (need_converge:bool) l := match l with
| ISeqEnd (SOInstructionConverge e) => if need_converge then ISeqEnd (SOInstructionConverge e) else ISeqEnd INull
| ISeqEnd h => ISeqEnd h
| ISeqCons (SOInstructionConverge e) t => 
    if need_converge then ISeqCons (SOInstructionConverge e) (remove_incorrect_converge_in_list false t) else (remove_incorrect_converge_in_list false t)
| ISeqCons (IIf cond ins_if ins_else) t => 
    ISeqCons (IIf cond ins_if ins_else) (remove_incorrect_converge_in_list true t)
| ISeqCons (ICase var case) t => 
    ISeqCons (ICase var case) (remove_incorrect_converge_in_list true t)
| ISeqCons h t => 
    ISeqCons h (remove_incorrect_converge_in_list false t)
end.


Lemma remove_incorrect_converge_in_list_correct : forall indata ins res,
  evalInstruction_s indata ins res -> forall b,
  evalInstruction_s indata (remove_incorrect_converge_in_list b ins) res.
Proof.
induction 1; simpl in *; intros; eauto; try (solve [econstructor; eauto]).
induction i; eauto; try (solve [econstructor; eauto]).
induction b; eauto; econstructor; eauto.
inversion H; subst; econstructor; eauto.
induction h; eauto; try (solve [econstructor; eauto]).
inversion H; subst; eauto.
induction b; eauto; econstructor; eauto.
induction h; eauto; try (solve [econstructor; eauto]).
inversion H.
Qed.

Fixpoint remove_null_instruction (i : Ast.instruction) :=
  match i with
  | Ast.IIf cond ins_s els_s => 
      Ast.IIf cond (remove_null_instruction_s ins_s) (remove_null_elif_instruction els_s)
  | Ast.ICase sc case_s => 
      Ast.ICase sc (remove_null_case_instruction case_s)
  | Ast.ILoop w ins_s => 
      Ast.ILoop w (remove_null_instruction_s ins_s)
  | i => i
  end
with remove_null_elif_instruction (i : Ast.elif_instruction) :=
  match i with
  | Ast.IElse ins_s => Ast.IElse (remove_null_instruction_s ins_s)
  | Ast.IElif cond ins_s els_s => Ast.IElif cond (remove_null_instruction_s ins_s) (remove_null_elif_instruction els_s)
  end
with remove_null_instruction_s i := 
  match i with
	| ISeqEnd h => ISeqEnd (remove_null_instruction h)
	| ISeqCons INull ins_s => remove_null_instruction_s ins_s
  | ISeqCons ins ins_s => 
    let h_opt := remove_null_instruction ins in
    let t_opt := remove_null_instruction_s ins_s in
    match t_opt with
    | ISeqEnd INull => ISeqEnd h_opt
    | _ => ISeqCons h_opt t_opt
    end
  end
with remove_null_case_instruction i := 
  match i with
	| ICaseFinishDefault ins_s => ICaseFinishDefault (remove_null_instruction_s ins_s)
	| ICaseCons (c,  ins_s) case_s => ICaseCons (c,  remove_null_instruction_s ins_s) (remove_null_case_instruction case_s)
  end
  .

Require Import Classical.

Lemma remove_null_instruction_correct :
  (forall indata ins res,
  evalInstruction indata ins res -> forall opt,
  opt = remove_null_instruction ins ->
  evalInstruction indata opt res) /\ 
  (forall indata ins res,
  evalInstruction_s indata ins res -> forall opt,
  opt = remove_null_instruction_s ins ->
  evalInstruction_s indata opt res) /\ 
  (forall indata ins res,
  evalElif_instruction indata ins res -> forall opt,
  opt = remove_null_elif_instruction ins ->
  evalElif_instruction indata opt res) /\ 
  (forall indata e ins res,
  evalCase_instruction indata e ins res -> forall opt,
  opt = remove_null_case_instruction ins ->
  evalCase_instruction indata e opt res).
Proof.
apply sem_inst_mutind; intros; simpl in *; eauto; try (solve [subst; econstructor; eauto]).
assert (
        (h = INull /\ opt = remove_null_instruction_s t) \/ 
        (remove_null_instruction_s t = ISeqEnd INull /\ opt = ISeqEnd (remove_null_instruction h)) \/
        (opt = ISeqCons (remove_null_instruction h) (remove_null_instruction_s t))
       ).
destruct (classic (h = INull)); subst; eauto; right.
destruct (classic (remove_null_instruction_s t = ISeqEnd INull)). 
left; split; eauto.
rewrite H1.
induction h; eauto.
right.
case_eq (remove_null_instruction_s t); intros.
rewrite H3 in *.
induction h; induction i; try (solve [contradict H2; auto]); try (solve [contradict H1; auto]); auto.
induction h; try (solve [contradict H2; auto]); auto.
destruct H2.
destruct H2; subst.
inversion e; subst; eauto.
destruct H2.
destruct H2. 
rewrite H3.
assert (D := H0 (remove_null_instruction_s t) (eq_refl (remove_null_instruction_s t))).
rewrite H2 in D.
inversion D; subst.
inversion H8; subst.
solve [econstructor; eauto].
rewrite H2.
solve [econstructor; eauto].
assert (
        (h = INull /\ opt = remove_null_instruction_s t) \/ 
        (remove_null_instruction_s t = ISeqEnd INull /\ opt = ISeqEnd (remove_null_instruction h)) \/
        (opt = ISeqCons (remove_null_instruction h) (remove_null_instruction_s t))
       ).
destruct (classic (h = INull)); subst; eauto; right.
destruct (classic (remove_null_instruction_s t = ISeqEnd INull)). 
left; split; eauto.
rewrite H0.
induction h; eauto.
right.
case_eq (remove_null_instruction_s t); intros.
rewrite H2 in *.
induction h; induction i; try (solve [contradict H1; auto]); try (solve [contradict H2; auto]); auto.
induction h; try (solve [contradict H1; auto]); auto.
destruct H1.
destruct H1.
subst.
inversion e0.
destruct H1.
destruct H1.
rewrite H2.
solve [econstructor; eauto].
rewrite H1.
solve [econstructor; eauto].
Qed.












