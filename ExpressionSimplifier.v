Require Import Map MapTypes Values Environment Semantics Semantics_f TacUtils PartialView Correctness ContextRule FlowOptim PartialViewOptimizerPropagator.
Require Import Ast.
Require Import List String ZArith Lia Bool.
Import ListNotations.

Fixpoint simpl_exp ctx exp := match evalExpression_f ctx exp with
| Some (Int v) => ELit (Int_lit v)
| Some (Bool v) => ELit (Bool_lit v)
| _ => 
    (
      match exp with
      | Ebinary op e e' => Ebinary op (simpl_exp ctx e) (simpl_exp ctx e')
      | Eunary op e => Eunary op (simpl_exp ctx e)
      | _ => exp
      end
    )
end.

Theorem simpl_exp_correct: forall e e',
  envpartialView e e' -> forall exp,
  evalExpression_f e' (simpl_exp e exp) = evalExpression_f e' exp.
Proof.
induction exp; intros.
simpl.
assert (D := envpartialViewEvalExpression e e' (EVar n) H).
case_eq (evalExpression_f e (EVar n)); intros; simpl in H0; rewrite H0; auto.
induction v; eauto; simpl in *; rewrite H0 in *; destruct D; try (solve [rewrite <- H1; auto]);
try (solve [inversion H1]).
induction l; eauto.
simpl.
case_eq (evalExpression_f e exp1); intros.
assert (D := envpartialViewEvalExpression e e' exp1 H).
rewrite H0 in *.
case_eq (evalExpression_f e exp2); intros.
assert (D0 := envpartialViewEvalExpression e e' exp2 H).
rewrite H1 in *.
destruct D; try rewrite <- H2 in *.
destruct D0; try rewrite <- H3 in *.
case_eq (Math.binary_operation b v v0); intros; simpl; eauto.
induction v1; try solve [simpl; rewrite IHexp1; rewrite IHexp2; auto]; simpl; auto.
rewrite IHexp1.
rewrite IHexp2.
auto.
inversion H3; subst.
assert (D := binary_op_undefined_some_r b v).
destruct D; rewrite H4; simpl; rewrite IHexp1; rewrite IHexp2; auto.
inversion H2; subst.
destruct D0; try rewrite <- H3 in *; try rewrite <- H0.
assert (D := binary_op_undefined_some_l b v0).
destruct D; rewrite H4; simpl; rewrite IHexp1; rewrite IHexp2; auto.
inversion H2; inversion H3; subst.
assert (D := binary_op_undefined_some_l b Undefined).
destruct D; rewrite H4; simpl; rewrite IHexp1; rewrite IHexp2; auto.
destruct D; try rewrite <- H2 in *.
simpl.
rewrite IHexp1; rewrite IHexp2; auto.
simpl.
rewrite IHexp1; rewrite IHexp2; auto.
simpl.
rewrite IHexp1; rewrite IHexp2; auto.
simpl.
assert (D := envpartialViewEvalExpression e e' exp H).
case_eq (evalExpression_f e exp); intros; rewrite H0 in *.
destruct D; try rewrite <- H1 in *.
case_eq (Math.unary_operation u v); intros; simpl; eauto.
induction v0; simpl; try rewrite IHexp; auto; rewrite H2; auto.
rewrite IHexp; auto; rewrite H2; auto.
inversion H1; subst.
rewrite unary_op_undefined.
simpl.
rewrite IHexp; auto.
simpl.
rewrite IHexp; auto.
Qed.

(* sec_opel_expression_booleenne ::= 
 not sec_opel_parametre_nom_secu 
 | sec_opel_parametre_nom_secu sec_opel_operateur_relationnel sec_opel_parametre 
 | sec_opel_parametre_nom_secu sec_opel_operateur_logique sec_opel_parametre_nom_secu 
 | sec_opel_diff => diff ( sec_opel_parametre_nom_secu, sec_opel_parametre ) 
 | sec_opel_equal => equal ( sec_opel_parametre_nom_secu, sec_opel_parametre )   *)

Definition switch_sec_opel_binary_bool_op op := match op with
| SOLand => SOLand
| SOLor => SOLor
| SORless => SORgreater
| SORleq => SORgeq
| SORgreater => SORless
| SORgeq => SORleq
| SOReq => SOReq
| SORdiff => SORdiff
end.

Theorem switch_sec_opel_binary_bool_op_correct: forall env op p p',
  evalSec_opel_expression_booleenne_f env (SOEBBinaire op p (SEC_OPEL_Var p')) =
  evalSec_opel_expression_booleenne_f env (SOEBBinaire (switch_sec_opel_binary_bool_op op) p' (SEC_OPEL_Var p)).
Proof.
induction op; simpl; intros; induction (match getIdsValue p env with
  | Some (Value (ConstantValue v)) | Some (Value (VariableValue v)) => Some v
  | None => Some Undefined
  | _ => None
  end);
induction (match getIdsValue p' env with
  | Some (Value (ConstantValue v)) | Some (Value (VariableValue v)) => Some v
  | None => Some Undefined
  | _ => None
  end); auto.

unfold Math.and.
induction (Math.to_bool a); induction (Math.to_bool a0); auto; f_equal; f_equal; apply andb_comm.

unfold Math.or.
induction (Math.to_bool a); induction (Math.to_bool a0); auto; f_equal; f_equal; apply orb_comm.

unfold Math.lt.
unfold Math.gt.
induction a; induction a0; auto; f_equal; f_equal; apply eq_sym; apply Z.gtb_ltb.

unfold Math.le.
unfold Math.ge.
induction a; induction a0; auto; f_equal; f_equal; apply eq_sym; apply Z.geb_leb.

unfold Math.lt.
unfold Math.gt.
induction a; induction a0; auto; f_equal; f_equal; apply Z.gtb_ltb.

unfold Math.le.
unfold Math.ge.
induction a; induction a0; auto; f_equal; f_equal; apply Z.geb_leb.

unfold Math.eq.
induction a; induction a0; auto; f_equal; f_equal. 
case_eq (Zeq_bool n n0); intros.
apply Zeq_is_eq_bool in H.
apply eq_sym in H.
apply Zeq_is_eq_bool in H.
rewrite H; auto.
case_eq (Zeq_bool n0 n); auto; intros.
apply Zeq_is_eq_bool in H0.
apply eq_sym in H0.
apply Zeq_is_eq_bool in H0.
rewrite H0 in H; inversion H.

unfold Math.ne.
induction a; induction a0; auto; f_equal; f_equal. 
unfold Zneq_bool.
rewrite Z.compare_antisym.
case_eq ((n0 ?= n)%Z); intros; auto.
Qed.

Lemma simpl_exp_var : forall exp ctx d, simpl_exp ctx exp = (EVar d) -> exp = EVar d.
Proof.
induction exp; simpl; eauto.
intros. 
induction (match getIdsValue n ctx with
      | Some (Value (ConstantValue v)) | Some (Value (VariableValue v)) => Some v
      | None => Some Undefined
      | _ => None
      end); inversion H; try induction a; inversion H; eauto.
intros.
induction l; inversion H.
intros.
induction (match evalExpression_f ctx exp1 with
      | Some v0 =>
          match evalExpression_f ctx exp2 with
          | Some v1 => Math.binary_operation b v0 v1
          | None => None
          end
      | None => None
      end); inversion H; induction a; inversion H.
intros.
induction (match evalExpression_f ctx exp with
      | Some v0 => Math.unary_operation u v0
      | None => None
      end); inversion H; induction a; inversion H.
Qed.

Lemma simpl_exp_var_poss : forall ctx d, 
  simpl_exp ctx (EVar d) = (EVar d) \/ 
  (exists v, simpl_exp ctx (EVar d) = ELit (Int_lit v)) \/ 
  (exists v, simpl_exp ctx (EVar d) = ELit (Bool_lit v)).
Proof.
unfold simpl_exp.
intros.
induction (evalExpression_f ctx (EVar d)); eauto.
induction a; eauto.
Qed.

Lemma simpl_exp_var_poss2 : forall ctx p, 
  (exists v, p = SEC_OPEL_Var v /\ simpl_exp ctx (convSec_opel_parametre_to_exp p) = (EVar v)) \/ 
  (exists v, simpl_exp ctx (convSec_opel_parametre_to_exp p) = ELit (Int_lit v)) \/ 
  (exists v, simpl_exp ctx (convSec_opel_parametre_to_exp p) = ELit (Bool_lit v)).
Proof.
induction p; simpl; eauto.
induction s; simpl; eauto.
induction (match getIdsValue n ctx with
     | Some (Value (ConstantValue v2)) |
       Some (Value (VariableValue v2)) => Some v2
     | None => Some Undefined
     | _ => None
     end); simpl; eauto.
induction a; eauto.
Qed.

Lemma evalSec_opel_expression_booleenne_f_bin_replace : forall env op p1 p2 p1' p2',
  evalExpression_f env (convSec_opel_parametre_nom_secu_to_exp p1) = evalExpression_f env (convSec_opel_parametre_nom_secu_to_exp p1') ->
  evalExpression_f env (convSec_opel_parametre_to_exp p2) = evalExpression_f env (convSec_opel_parametre_to_exp p2') ->
  evalSec_opel_expression_booleenne_f env (SOEBBinaire op p1 p2) = evalSec_opel_expression_booleenne_f env (SOEBBinaire op p1' p2').
Proof.
unfold evalSec_opel_expression_booleenne_f.
intros.
simpl in *.
rewrite H, H0.
auto.
Qed.

Definition accepted_op op := match op with
| SOLand | SOLor => false
| _ => true
end.

Opaque accepted_op.


Definition simpl_exp_bool ctx exp := match exp with
| SOEBBinaire op p1 p2 => 
  (
    if accepted_op op then
      match simpl_exp ctx (EVar p1), simpl_exp ctx (convSec_opel_parametre_to_exp p2) with
      | ELit (Int_lit vp1), EVar idp2 => SOEBBinaire (switch_sec_opel_binary_bool_op op) idp2 (SEC_OPEL_Value (Int_lit vp1))
      | ELit (Bool_lit vp1), EVar idp2 => SOEBBinaire (switch_sec_opel_binary_bool_op op) idp2 (SEC_OPEL_Value (Bool_lit vp1))
      | _, ELit (Int_lit vp) => SOEBBinaire op p1 (SEC_OPEL_Value (Int_lit vp))
      | _, ELit (Bool_lit vp) => SOEBBinaire op p1 (SEC_OPEL_Value (Bool_lit vp))
      | _, _ => exp
      end
    else exp
  )
| _ => exp
end.

Lemma evalExpression_f_bin_repl: forall e op e1 e2 e1' e2',
   evalExpression_f e e1 = evalExpression_f e e1' -> 
   evalExpression_f e e2 = evalExpression_f e e2' -> 
  evalExpression_f e
    (Ebinary op e1 e2) =
  evalExpression_f e
    (Ebinary op e1' e2').
Proof.
intros.
simpl.
rewrite H, H0.
auto.
Qed.

Theorem switch_evalExpression_f_correct: forall env op p p',
  evalExpression_f env (Ebinary (convSec_opel_binary_bool_op op) p p') =
  evalExpression_f env (Ebinary (convSec_opel_binary_bool_op (switch_sec_opel_binary_bool_op op)) p' p).
Proof.
induction op; simpl; intros; case_eq (evalExpression_f env p); case_eq (evalExpression_f env p'); intros; auto.
unfold Math.and.
induction (Math.to_bool v); induction (Math.to_bool v0); auto; f_equal; f_equal; apply andb_comm.

unfold Math.or.
induction (Math.to_bool v); induction (Math.to_bool v0); auto; f_equal; f_equal; apply orb_comm.

unfold Math.lt.
unfold Math.gt.
induction v; induction v0; auto; f_equal; f_equal; apply eq_sym; apply Z.gtb_ltb.

unfold Math.le.
unfold Math.ge.
induction v; induction v0; auto; f_equal; f_equal; apply eq_sym; apply Z.geb_leb.

unfold Math.lt.
unfold Math.gt.
induction v; induction v0; auto; f_equal; f_equal; apply Z.gtb_ltb.

unfold Math.le.
unfold Math.ge.
induction v; induction v0; auto; f_equal; f_equal; apply Z.geb_leb.

unfold Math.eq.
induction v; induction v0; auto; f_equal; f_equal. 
case_eq (Zeq_bool n n0); intros.
apply Zeq_is_eq_bool in H1.
apply eq_sym in H1.
apply Zeq_is_eq_bool in H1.
rewrite H1; auto.
case_eq (Zeq_bool n0 n); auto; intros.
apply Zeq_is_eq_bool in H2.
apply eq_sym in H2.
apply Zeq_is_eq_bool in H2.
rewrite H2 in H1; inversion H1.

unfold Math.ne.
induction v; induction v0; auto; f_equal; f_equal. 
unfold Zneq_bool.
rewrite Z.compare_antisym.
case_eq ((n ?= n0)%Z); intros; auto.
Qed.

Opaque evalExpression_f simpl_exp.

Theorem switch_repl_evalExpression_f_correct: forall env op p p' p0 p0',
  evalExpression_f env p = evalExpression_f env p0 -> 
  evalExpression_f env p' = evalExpression_f env p0' -> 
  evalExpression_f env (Ebinary (convSec_opel_binary_bool_op op) p p') =
  evalExpression_f env (Ebinary (convSec_opel_binary_bool_op (switch_sec_opel_binary_bool_op op)) p0' p0).
Proof.
intros.
erewrite evalExpression_f_bin_repl; eauto.
eapply switch_evalExpression_f_correct; eauto.
Qed.


Theorem simpl_exp_bool_correct: forall e e',
  envpartialView e e' -> forall exp,
  evalSec_opel_expression_booleenne_f e' (simpl_exp_bool e exp) = evalSec_opel_expression_booleenne_f e' exp.
Proof.
intros.
case_eq exp; intros; simpl; eauto.
case_eq (accepted_op s); intros; eauto.
assert (D := simpl_exp_var_poss e l).
assert (D0 := simpl_exp_var_poss2 e s0).
destruct D.
rewrite H2.
destruct D0.
destruct H3, H3.
subst.
rewrite H4.
simpl.
auto.
destruct H3, H3.
rewrite H3.
simpl.
eapply evalExpression_f_bin_repl; eauto.
assert (D := simpl_exp_correct e e' H (convSec_opel_parametre_to_exp s0)).
rewrite H3 in *.
rewrite <- D.
unfold convSec_opel_parametre_literal_to_exp.
auto.
rewrite H3.
eapply evalSec_opel_expression_booleenne_f_bin_replace; eauto.
simpl.
assert (D := simpl_exp_correct e e' H (convSec_opel_parametre_to_exp s0)).
rewrite H3 in *.
rewrite <- D.
simpl.
unfold convSec_opel_parametre_literal_to_exp.
auto.
destruct H2, H2.
destruct D0.
destruct H3, H3.
subst.
rewrite H2.
rewrite H4.
simpl.
apply eq_sym.
eapply switch_repl_evalExpression_f_correct; eauto.
assert (D := simpl_exp_correct e e' H (convSec_opel_parametre_nom_secu_to_exp l)).
rewrite <- D.
unfold convSec_opel_parametre_nom_secu_to_exp.
rewrite H2.
unfold convSec_opel_parametre_literal_to_exp.
auto.
rewrite H2.
destruct H3, H3.
rewrite H3.
eapply evalExpression_f_bin_repl; eauto.
assert (D := simpl_exp_correct e e' H (convSec_opel_parametre_to_exp s0)).
rewrite <- D.
rewrite H3.
unfold convSec_opel_parametre_to_exp.
unfold convSec_opel_parametre_literal_to_exp.
auto.
rewrite H3.
eapply evalExpression_f_bin_repl; eauto.
assert (D := simpl_exp_correct e e' H (convSec_opel_parametre_to_exp s0)).
rewrite <- D.
rewrite H3.
unfold convSec_opel_parametre_to_exp.
unfold convSec_opel_parametre_literal_to_exp.
auto.
rewrite H2.
destruct D0.
destruct H3, H3; subst.
rewrite H4.
apply eq_sym.
eapply switch_repl_evalExpression_f_correct; eauto.
assert (D := simpl_exp_correct e e' H (convSec_opel_parametre_nom_secu_to_exp l)).
unfold convSec_opel_parametre_nom_secu_to_exp in *.
rewrite H2 in D.
rewrite <- D.
unfold convSec_opel_parametre_to_exp.
unfold convSec_opel_parametre_literal_to_exp.
auto.
destruct H3, H3.
rewrite H3.
simpl.
eapply evalExpression_f_bin_repl; eauto.
assert (D := simpl_exp_correct e e' H (convSec_opel_parametre_to_exp s0)).
rewrite H3 in D.
rewrite <- D.
unfold convSec_opel_parametre_literal_to_exp; auto.
rewrite H3.
simpl.
eapply evalExpression_f_bin_repl; eauto.
assert (D := simpl_exp_correct e e' H (convSec_opel_parametre_to_exp s0)).
rewrite H3 in D.
rewrite <- D.
unfold convSec_opel_parametre_literal_to_exp; auto.
Qed.

(* sec_opel_expression_d_affectation ::= 
 not sec_opel_parametre_nom_secu 
 | - sec_opel_parametre_nom_secu 
 | sec_opel_parametre_nom_secu + sec_opel_parametre 
 | sec_opel_parametre_nom_secu - sec_opel_parametre 
 | sec_opel_parametre_litteral - sec_opel_parametre_nom_secu 
 | sec_opel_parametre_nom_non_secu - sec_opel_parametre_nom_secu 
 | sec_opel_parametre_nom_secu * sec_opel_parametre 
 | sec_opel_parametre_nom_secu sec_opel_operateur_relationnel sec_opel_parametre 
 | sec_opel_parametre_nom_secu sec_opel_operateur_logique sec_opel_parametre_nom_secu 
 | sec_opel_assign 
 | sec_opel_build_tag 
 | sec_opel_diff 
 | sec_opel_div2 
 | sec_opel_equal 
 | sec_opel_encode 
 | sec_opel_read 
 | sec_opel_read_context  *)

Definition simpl_sec_opel_expression_d_affectation ctx exp := match exp with
| SOEAffAssign (SEC_OPEL_Var ids) => 
  (
    match simpl_exp ctx (EVar ids) with
    | ELit (Int_lit v) => (SOEAffAssign (SEC_OPEL_Value (Int_lit v)))
    | ELit (Bool_lit v) => (SOEAffAssign (SEC_OPEL_Value (Bool_lit v)))
    | _ => exp
    end
  )
| SOEAffExpUMinus ids => 
  (
    match simpl_exp ctx (Eunary NumericUnaryMinus (EVar ids)) with
    | ELit (Int_lit v) => (SOEAffAssign (SEC_OPEL_Value (Int_lit v)))
    | _ => exp
    end
  )
| SOEAffEncode e => 
  (
    match simpl_exp ctx e with
    | ELit (Int_lit v) => (SOEAffAssign (SEC_OPEL_Value (Int_lit v)))
    | ELit (Bool_lit v) => (SOEAffAssign (SEC_OPEL_Value (Bool_lit v)))
    | _ => exp
    end
  )
| SOEAffBool e => SOEAffBool (simpl_exp_bool ctx e)
| SOEAffExpBinary op p p' => 
  (
    match simpl_exp ctx (Ebinary (convSec_opel_binary_num_op op) (convSec_opel_parametre_to_exp p) (convSec_opel_parametre_to_exp p')) with
    | ELit (Int_lit v) => (SOEAffAssign (SEC_OPEL_Value (Int_lit v)))
    | ELit (Bool_lit v) => (SOEAffAssign (SEC_OPEL_Value (Bool_lit v)))
    | _ => exp
    end
  )
| (SOEAffRead ids (Some(ids', None))) => 
  (
    match evalExpression_f ctx (EVar ids) with
    | Some Undefined | None => (SOEAffRead ids (Some(ids', None))) 
    | Some v =>
      (
        match evalExpression_f ctx (EVar ids') with
        | Some (Int v') => 
          (
            match idxs_select_from_value_f v [(Z.to_nat v')] with
            | Some (Int v'') => (SOEAffAssign (SEC_OPEL_Value (Int_lit v'')))
            | Some (Bool v'') => (SOEAffAssign (SEC_OPEL_Value (Bool_lit v'')))
            | _ => (SOEAffRead ids (Some(ids', None)))
            end
          )
        | _ => (SOEAffRead ids (Some(ids', None)))
        end
      )
    end
  )
| (SOEAffRead ids (Some(ids', Some ids''))) => 
  (
    match evalExpression_f ctx (EVar ids) with
    | Some Undefined | None => (SOEAffRead ids (Some(ids', Some ids''))) 
    | Some v =>
      (
        match evalExpression_f ctx (EVar ids') with
        | Some (Int v') => 
          (
            match evalExpression_f ctx (EVar ids'') with
            | Some (Int v'') => 
              (
                match idxs_select_from_value_f v [(Z.to_nat v'); (Z.to_nat v'')] with
                | Some (Int v0) => (SOEAffAssign (SEC_OPEL_Value (Int_lit v0)))
                | Some (Bool v0) => (SOEAffAssign (SEC_OPEL_Value (Bool_lit v0)))
                | _ => (SOEAffRead ids (Some(ids', Some ids'')))
                end
              )
            | _ => (SOEAffRead ids (Some(ids', Some ids'')))
            end
          )
        | _ => (SOEAffRead ids (Some(ids', Some ids'')))
        end
      )
    end
  )
| _ => exp
end.

Opaque idxs_select_from_value_f.

Theorem simpl_sec_opel_expression_d_affectation_correct: forall e e',
  envpartialView e e' -> forall trace exp v,
  evalSec_opel_expression_d_affectation (e', trace) exp v ->
  evalSec_opel_expression_d_affectation (e', trace) (simpl_sec_opel_expression_d_affectation e exp) v.
Proof.
intros.
inversion H0; subst; simpl; try solve [econstructor; eauto].
apply evalExpression_f_correct in H5.
rewrite <- simpl_exp_correct with (e:=e) in H5; eauto.
case_eq (simpl_exp e (Eunary NumericUnaryMinus (EVar ids))); eauto; intros; rewrite H1 in *.
induction l; eauto.
econstructor; eauto.
unfold convSec_opel_parametre_to_exp.
unfold convSec_opel_parametre_literal_to_exp.
apply evalExpression_f_correct; auto.

apply evalExpression_f_correct in H5.
rewrite <- simpl_exp_correct with (e:=e) in H5; eauto.
case_eq (simpl_exp e
      (Ebinary (convSec_opel_binary_num_op op)
         (convSec_opel_parametre_to_exp p0)
         (convSec_opel_parametre_to_exp p1))); eauto; intros; rewrite H1 in *.
induction l; eauto.
econstructor; eauto.
unfold convSec_opel_parametre_to_exp.
unfold convSec_opel_parametre_literal_to_exp.
apply evalExpression_f_correct; auto.
econstructor; eauto.
unfold convSec_opel_parametre_to_exp.
unfold convSec_opel_parametre_literal_to_exp.
apply evalExpression_f_correct; auto.

apply evalSec_opel_expression_booleenne_f_correct in H5.
rewrite <- simpl_exp_bool_correct with (e:=e) in H5; eauto.
apply evalSec_opel_expression_booleenne_f_correct in H5.
econstructor; eauto.

induction ids; eauto.
apply evalExpression_f_correct in H5.
rewrite <- simpl_exp_correct with (e:=e) in H5; eauto.
apply evalExpression_f_correct in H5.
unfold convSec_opel_parametre_to_exp in H5.
unfold convSec_opel_parametre_nom_secu_to_exp in H5.
case_eq (simpl_exp e (EVar n)); eauto; intros; rewrite H1 in *.
induction l; eauto.
econstructor; eauto.
econstructor; eauto.

apply evalExpression_f_correct in H5.
rewrite <- simpl_exp_correct with (e:=e) in H5; eauto.
case_eq (simpl_exp e e0); eauto; intros; rewrite H1 in *.
induction l; eauto.
econstructor; eauto.
unfold convSec_opel_parametre_to_exp.
unfold convSec_opel_parametre_literal_to_exp.
apply evalExpression_f_correct; auto.
econstructor; eauto.
unfold convSec_opel_parametre_to_exp.
unfold convSec_opel_parametre_literal_to_exp.
apply evalExpression_f_correct; auto.

apply evalExpression_f_correct in H3.
apply evalExpression_f_correct in H4.
apply idxs_select_from_value_f_correct in H7.
assert (D := envpartialViewEvalExpression e e' (EVar ids) H).
rewrite H3 in D.
assert (D0 := envpartialViewEvalExpression e e' (EVar ids') H).
rewrite H4 in D0.
destruct D; rewrite H1.
destruct D0; rewrite H2.
simpl in H7.
rewrite H7.
apply evalExpression_f_correct in H3.
apply evalExpression_f_correct in H4.
apply idxs_select_from_value_f_correct in H7.
induction v0; try solve [econstructor; eauto];
induction v; econstructor; eauto; unfold convSec_opel_parametre_to_exp;
unfold convSec_opel_parametre_literal_to_exp; eauto; econstructor; eauto.
apply evalExpression_f_correct in H3.
apply evalExpression_f_correct in H4.
apply idxs_select_from_value_f_correct in H7.
induction v0; try solve [econstructor; eauto].
apply evalExpression_f_correct in H3.
apply evalExpression_f_correct in H4.
apply idxs_select_from_value_f_correct in H7.
eauto; econstructor.

apply evalExpression_f_correct in H3.
apply evalExpression_f_correct in H4.
apply evalExpression_f_correct in H5.
apply idxs_select_from_value_f_correct in H8.
assert (D := envpartialViewEvalExpression e e' (EVar ids) H).
rewrite H3 in D.
assert (D0 := envpartialViewEvalExpression e e' (EVar ids') H).
rewrite H4 in D0.
assert (D1 := envpartialViewEvalExpression e e' (EVar ids'') H).
rewrite H5 in D1.
apply evalExpression_f_correct in H3.
apply evalExpression_f_correct in H4.
apply evalExpression_f_correct in H5.
destruct D; rewrite H1.
destruct D0; rewrite H2.
destruct D1; rewrite H6.
rewrite H8.
apply idxs_select_from_value_f_correct in H8.
induction v0; try solve [econstructor; eauto];
induction v; econstructor; eauto; unfold convSec_opel_parametre_to_exp;
unfold convSec_opel_parametre_literal_to_exp; eauto; econstructor; eauto.
induction v0; eauto.
induction v0; eauto.
auto.
Qed.

Definition simpl_sec_opel_w ctx exp := match exp with
| SOWBool e => SOWBool (simpl_exp_bool ctx e)
| _ => exp
end.

Theorem simpl_sec_opel_w_correct: forall e e',
  envpartialView e e' -> forall exp,
  evalSec_opel_w_f e' (simpl_sec_opel_w e exp) = evalSec_opel_w_f e' exp.
Proof.
induction exp; eauto.
simpl.
eapply simpl_exp_bool_correct; eauto.
Qed.

Definition simpl_sec_opel_expression_if ctx exp := match exp with
| SOExpression_booleenne e => SOExpression_booleenne (simpl_exp_bool ctx e)
| _ => exp
end.

Theorem simpl_sec_opel_expression_if_correct: forall e e',
  envpartialView e e' -> forall trace exp v,
  evalSec_opel_expression_if (e', trace) exp v ->
  evalSec_opel_expression_if (e', trace) (simpl_sec_opel_expression_if e exp) v.
Proof.
intros.
inversion H0; subst; auto.
eapply EvalSOExpression_booleenne; eauto.
eapply evalSec_opel_expression_booleenne_f_correct in H5.
eapply evalSec_opel_expression_booleenne_f_correct.
rewrite simpl_exp_bool_correct; eauto.
Qed.

(* Propagator *)

(* Definition simpl_type_designator ctx exp := match exp with
| TypeSimple n => TypeSimple n
| TypeComposeArray1 n e => TypeComposeArray1 n (simpl_exp ctx e)
| TypeComposeArray2 n e e' => TypeComposeArray2 n (simpl_exp ctx e) (simpl_exp ctx e')
end. *)

Definition simpl_obj ctx exp := match exp with
| ConstantVar id typ e => ConstantVar id typ (simpl_exp ctx e)
| VariableVar id typ => VariableVar id typ
end.

Definition simpl_declaration_de_base ctx elem := match elem with
| Declaration_d_objet obj => Declaration_d_objet (simpl_obj ctx obj)
| _ => elem
end.

Lemma simpl_declaration_de_base_correct : forall ctx env r_env d,
    envpartialView ctx env ->
    evalDeclaration_de_base env d r_env -> 
    evalDeclaration_de_base env (simpl_declaration_de_base ctx d) r_env.
Proof.
induction 2; subst; simpl; try solve [econstructor; eauto]; eauto.
econstructor; eauto.
apply evalExpression_f_correct.
apply evalExpression_f_correct in H0.
rewrite <- H0.
apply simpl_exp_correct; eauto.
Qed.

Lemma envpartialViewDeclareGlobalVar : forall e e' de de' id v,
  envpartialView e e' ->
  declareGlobalVar id v e = Some de -> 
  declareGlobalVar id v e' = Some de' -> 
  envpartialView de de'.
Proof.
unfold envpartialView.
unfold declareGlobalVar.
intros.
destruct H.
destruct H2.
destruct H3.
destruct H4.
case_eq (StringMapModule.get id (globalVars e)); intros; rewrite H6 in *; try einversion.
apply mapPartialView_getNone_l2b with (bigger := (globalVars e')) in H6; eauto.
rewrite H6 in *.
rewrite H4 in *.
rewrite H3 in *.
case_eq (StringMapModule.get id (funDefs e')); intros; rewrite H7 in *; try einversion.
case_eq (StringMapModule.get id (localfunDecls e')); intros; rewrite H8 in *; try einversion.
case_eq (StringMapModule.get id (externals e)); intros; rewrite H9 in *; inversion H0.
case_eq (StringMapModule.get id (externals e')); intros; rewrite H10 in *; inversion H1.
simpl.
repeat (split; auto).
eapply mapPartialView_setV; eauto.
Qed.

Lemma mapPartialView_setCUndef: forall lower bigger,
  mapPartialView lower bigger ->
  forall id v, 
  mapPartialView (StringMapModule.set id (ConstantValue Undefined) lower) (StringMapModule.set id (ConstantValue v) bigger).
Proof.
unfold mapPartialView.
intros.
rewrite StringMapModule.gsspec.
rewrite StringMapModule.gsspec.
induction (String_Equality.eq id0 id); auto.
right.
eexists; eauto.
Qed.

Lemma envpartialViewDeclareGlobalVarConstUndef : forall e e' de de' id v,
  envpartialView e e' ->
  declareGlobalVar id (ConstantValue Undefined) e = Some de -> 
  declareGlobalVar id (ConstantValue v) e' = Some de' -> 
  envpartialView de de'.
Proof.
unfold envpartialView.
unfold declareGlobalVar.
intros.
destruct H.
destruct H2.
destruct H3.
destruct H4.
case_eq (StringMapModule.get id (globalVars e)); intros; rewrite H6 in *; try einversion.
apply mapPartialView_getNone_l2b with (bigger := (globalVars e')) in H6; eauto.
rewrite H6 in *.
rewrite H4 in *.
rewrite H3 in *.
case_eq (StringMapModule.get id (funDefs e')); intros; rewrite H7 in *; try einversion.
case_eq (StringMapModule.get id (localfunDecls e')); intros; rewrite H8 in *; try einversion.
case_eq (StringMapModule.get id (externals e)); intros; rewrite H9 in *; inversion H0.
case_eq (StringMapModule.get id (externals e')); intros; rewrite H10 in *; inversion H1.
simpl.
repeat (split; auto).
eapply mapPartialView_setCUndef; eauto.
Qed. 

Lemma envpartialViewDeclareFunction: forall e e' de de' fid fparams,
  envpartialView e e' ->
  declareFunction fid fparams e = Some de -> 
  declareFunction fid fparams e' = Some de' -> 
  envpartialView de de'.
Proof.
unfold envpartialView.
unfold declareFunction.
intros.
destruct H.
destruct H2.
destruct H3.
destruct H4.
case_eq (StringMapModule.get fid (localVars e)); intros; rewrite H6 in *; try einversion.
apply mapPartialView_getNone_l2b with (bigger := (localVars e')) in H6; eauto; rewrite H6 in *; try einversion.
case_eq (StringMapModule.get fid (globalVars e)); intros; rewrite H7 in *; try einversion.
apply mapPartialView_getNone_l2b with (bigger := (globalVars e')) in H7; eauto; rewrite H7 in *; try einversion.
rewrite H4 in *.
rewrite H3 in *.
case_eq (StringMapModule.get fid (externals e)); intros; rewrite H8 in *; try einversion.
case_eq (StringMapModule.get fid (externals e')); intros; rewrite H9 in *; try einversion.
case_eq (StringMapModule.get fid (funDefs e')); intros; rewrite H10 in *; try einversion.
case_eq (StringMapModule.get fid (localfunDecls e')); intros; rewrite H11 in *; inversion H0; inversion H1.
simpl.
repeat (split; auto).
Qed.

Lemma envpartialViewDefineFunction: forall e e' de de' ed,
  envpartialView e e' ->
  defineFunction ed e = Some de -> 
  defineFunction ed e' = Some de' -> 
  envpartialView de de'.
Proof.
unfold envpartialView.
unfold defineFunction.
intros.
destruct H.
destruct H2.
destruct H3.
destruct H4.
case_eq (StringMapModule.get (f_name ed) (localVars e)); intros; rewrite H6 in *; try einversion.
apply mapPartialView_getNone_l2b with (bigger := (localVars e')) in H6; eauto; rewrite H6 in *; try einversion.
case_eq (StringMapModule.get (f_name ed) (globalVars e)); intros; rewrite H7 in *; try einversion.
apply mapPartialView_getNone_l2b with (bigger := (globalVars e')) in H7; eauto; rewrite H7 in *; try einversion.
rewrite H4 in *.
rewrite H3 in *.
case_eq (StringMapModule.get (f_name ed) (externals e)); intros; rewrite H8 in *; try einversion.
case_eq (StringMapModule.get (f_name ed) (externals e')); intros; rewrite H9 in *; try einversion.
case_eq (StringMapModule.get (f_name ed) (funDefs e')); intros; rewrite H10 in *; inversion H0; inversion H1.
simpl.
repeat (split; auto).
Qed.

Lemma simpl_declaration_de_base_partialEnv : forall ctx env r_env c_env d,
    envpartialView ctx env ->
    evalDeclaration_de_base env d r_env -> 
    evalDeclaration_de_base ctx d c_env ->
    envpartialView c_env r_env.
Proof.
induction 2; intros.
inversion H2; subst.
apply evalExpression_f_correct in H0, H8.
assert (D := envpartialViewEvalExpression ctx env e H).
rewrite H0, H8 in *.
destruct D; inversion H3; subst.
eapply envpartialViewDeclareGlobalVar; eauto.
eapply envpartialViewDeclareGlobalVarConstUndef; eauto.
inversion H1; subst.
eapply envpartialViewDeclareGlobalVar; eauto.
inversion H1; subst.
eapply envpartialViewDeclareFunction; eauto.
inversion H0; subst; auto.
inversion H0; subst; auto.
inversion H2; subst.
eapply envpartialViewDefineFunction with (e:=env'0); eauto.
eapply envpartialViewDeclareFunction; eauto.
inversion H0; subst; auto.
inversion H0; subst; auto.
Qed.

Fixpoint simpl_declaration_de_baseList ctx l := match l with
| [] => []
| h::t => (simpl_declaration_de_base ctx h)::
  (
    match evalDeclaration_de_base_f ctx h with
    | Some ctx' => simpl_declaration_de_baseList ctx' t
    | _ => t
    end
  )
end.

Lemma include_except_prag_simpl_declaration_de_baseList : forall l ctx,
  include_except_prag (declaration_set_of_declaration_de_base l) (declaration_set_of_declaration_de_base (simpl_declaration_de_baseList ctx l)).
Proof.
induction l; intros; simpl.
apply include_except_prag_Nil.
induction a; simpl; try solve [apply include_except_prag_HeadEq; eauto]; try solve [apply include_except_prag_HeadEq; apply include_except_prag_refl].
induction o; simpl; try solve [apply include_except_prag_HeadEq; eauto]; try solve [apply include_except_prag_HeadEq; apply include_except_prag_refl].
induction (evalExpression_f ctx e); simpl; try solve [apply include_except_prag_HeadEq; eauto]; try solve [apply include_except_prag_HeadEq; apply include_except_prag_refl].
induction (declareGlobalVar t (ConstantValue a) ctx); simpl; try solve [apply include_except_prag_HeadEq; eauto]; try solve [apply include_except_prag_HeadEq; apply include_except_prag_refl].
induction (declareGlobalVar t (VariableValue (decValue t0)) ctx); simpl; try solve [apply include_except_prag_HeadEq; eauto]; try solve [apply include_except_prag_HeadEq; apply include_except_prag_refl].
induction (declareFunction t l0 ctx); simpl; try solve [apply include_except_prag_HeadEq; eauto]; try solve [apply include_except_prag_HeadEq; apply include_except_prag_refl].
eauto.
apply include_except_prag_refl.
induction (declareFunction t l0 ctx); try solve [apply include_except_prag_refl].
induction (defineFunction
         {|
           f_name := t;
           f_args := l0;
           f_local_decs := [];
           f_instrs :=
             ISeqEnd (ICallDefinedFunction n (Some (from_def_to_call_params l0)))
         |} a); try solve [apply include_except_prag_refl].
auto.
Qed.

Lemma simpl_declaration_de_baseList_equiv: forall d ctx env r_env,
    envpartialView ctx env ->
    evalDeclaration_de_baseList env d r_env -> 
    evalDeclaration_de_baseList env (simpl_declaration_de_baseList ctx d) r_env.
Proof.
induction d; simpl.
intros; auto.
intros.
inversion H0; subst.
case_eq (evalDeclaration_de_base_f ctx a); intros.
apply evalDeclaration_de_base_f_correct in H1.
apply simpl_declaration_de_base_partialEnv with (env:=env) (r_env:=env') in H1; eauto.
assert (D := simpl_declaration_de_base_correct ctx env env' a H H4).
econstructor; eauto.
econstructor; eauto.
eapply simpl_declaration_de_base_correct; eauto.
Qed.

Definition simpl_declaration_1_body ctx elem := match elem with
| Body_Declaration_d_objet obj => Body_Declaration_d_objet (simpl_obj ctx obj)
| _ => elem
end.

Fixpoint simpl_declaration_1_bodyList ctx l := match l with
| [] => []
| h::t => (simpl_declaration_1_body ctx h)::
  (
    match evalDeclaration_1_body_f ctx h with
    | Some ctx' => simpl_declaration_1_bodyList ctx' t
    | _ => t
    end
  )
end.

Lemma include_except_prag_simpl_declaration_1_bodyList : forall l ctx,
  include_except_prag (declaration_set_of_declaration_1_body l) (declaration_set_of_declaration_1_body (simpl_declaration_1_bodyList ctx l)).
Proof.
induction l; intros; simpl.
apply include_except_prag_Nil.
induction a; simpl; try solve [apply include_except_prag_HeadEq; eauto]; try solve [apply include_except_prag_HeadEq; apply include_except_prag_refl].
induction o; simpl; try solve [apply include_except_prag_HeadEq; eauto]; try solve [apply include_except_prag_HeadEq; apply include_except_prag_refl].
induction (evalExpression_f ctx e); simpl; try solve [apply include_except_prag_HeadEq; eauto]; try solve [apply include_except_prag_HeadEq; apply include_except_prag_refl].
induction (declareGlobalVar t (ConstantValue a) ctx); simpl; try solve [apply include_except_prag_HeadEq; eauto]; try solve [apply include_except_prag_HeadEq; apply include_except_prag_refl].
induction (declareGlobalVar t (VariableValue (decValue t0)) ctx); simpl; try solve [apply include_except_prag_HeadEq; eauto]; try solve [apply include_except_prag_HeadEq; apply include_except_prag_refl].
Qed.

Lemma simpl_declaration_1_body_correct : forall ctx env r_env d,
    envpartialView ctx env ->
    evalDeclaration_1_body env d r_env -> 
    evalDeclaration_1_body env (simpl_declaration_1_body ctx d) r_env.
Proof.
induction 2; subst; simpl; try solve [econstructor; eauto]; eauto.
econstructor; eauto.
apply evalExpression_f_correct.
apply evalExpression_f_correct in H0.
rewrite <- H0.
apply simpl_exp_correct; eauto.
Qed.

Lemma simpl_declaration_1_body_partialEnv : forall ctx env r_env c_env d,
    envpartialView ctx env ->
    evalDeclaration_1_body env d r_env -> 
    evalDeclaration_1_body ctx d c_env ->
    envpartialView c_env r_env.
Proof.
induction 2; intros.
inversion H2; subst.
apply evalExpression_f_correct in H0, H8.
assert (D := envpartialViewEvalExpression ctx env e H).
rewrite H0, H8 in *.
destruct D; inversion H3; subst.
eapply envpartialViewDeclareGlobalVar; eauto.
eapply envpartialViewDeclareGlobalVarConstUndef; eauto.
inversion H1; subst.
eapply envpartialViewDeclareGlobalVar; eauto.
inversion H0; subst; auto.
inversion H0; subst; auto.
inversion H0; subst; auto.
inversion H0; subst; auto.
Qed.

Lemma simpl_declaration_1_bodyList_equiv: forall d ctx env r_env,
    envpartialView ctx env ->
    evalDeclaration_1_bodyList env d r_env -> 
    evalDeclaration_1_bodyList env (simpl_declaration_1_bodyList ctx d) r_env.
Proof.
induction d; simpl.
intros; auto.
intros.
inversion H0; subst.
case_eq (evalDeclaration_1_body_f ctx a); intros.
apply evalDeclaration_1_body_f_correct in H1.
apply simpl_declaration_1_body_partialEnv with (env:=env) (r_env:=env') in H1; eauto.
assert (D := simpl_declaration_1_body_correct ctx env env' a H H4).
econstructor; eauto.
econstructor; eauto.
eapply simpl_declaration_1_body_correct; eauto.
Qed.

Fixpoint simpl_initLocals ctx ini := match ini with
| [] => []
| ((LocalDecVar (ConstantVar id typ e))::objT) => 
    (LocalDecVar (ConstantVar id typ (simpl_exp ctx e)))::
    (
      match evalExpression_f ctx e with
      | Some v => 
        (
          match declareLocalVar id (ConstantValue v) ctx with
          | Some env' => simpl_initLocals env' objT
          | None => objT
          end
        )
      | None => objT
      end
    )
| ((LocalDecVar (VariableVar id typ))::objT) => 
    (LocalDecVar (VariableVar id typ))::
    (
      match declareLocalVar id (VariableValue (decValue typ)) ctx with
      | Some env' => simpl_initLocals env' objT
      | None => objT
      end
    )
| h::objT => h::(simpl_initLocals ctx objT)
end.

Lemma simpl_initLocals_equiv : forall d ctx env trace r_env,
    envpartialView ctx env ->
    initLocals (env, trace) d r_env -> 
    initLocals (env, trace) (simpl_initLocals ctx d) r_env.
Proof.
induction d; simpl; auto.
intros.
induction a; inversion H0; subst.
econstructor; eauto.
apply evalExpression_f_correct; eauto.
apply evalExpression_f_correct in H5; eauto.
rewrite <- H5.
apply simpl_exp_correct; eauto.
apply evalExpression_f_correct in H5; eauto.
assert (D := envpartialViewEvalExpression ctx env e H).
rewrite H5 in *.
destruct D; rewrite H1.
case_eq (declareLocalVar id (ConstantValue v) ctx); eauto; intros.
eapply partialViewDeclareVC in H7; eauto.
case_eq (declareLocalVar id (ConstantValue Undefined) ctx); eauto; intros.
eapply partialViewDeclareVUndefinedC in H7; eauto.
eapply initLocalsConsVar with (env0:=env1); eauto.
case_eq (declareLocalVar id (VariableValue (decValue typ)) ctx); eauto; intros.
eapply partialViewDeclareV in H6; eauto.
econstructor; eauto.
econstructor; eauto.
econstructor; eauto.
Qed.

Fixpoint optimize_Instruction env i := match i with
| ILoop cond ins => 
    (
      match evalSec_opel_w_f env cond with
      | Some (Bool false) => (INull, Some env)
      | _ => 
        (
          match forgetInstruction_s ins env with
          | None => (ILoop cond ins, None)
          | Some e => (ILoop (simpl_sec_opel_w e cond) (fst (optimize_Instruction_s e ins)), Some e)
          end
        )
      end
    )
| IIf cond ins_if ins_else => 
    (IIf (simpl_sec_opel_expression_if env cond) (fst (optimize_Instruction_s env ins_if)) (fst (optimize_Elif_instruction env ins_else)), 
      contextRuleInstruction default_fuel env i)
| (ICase var case) => 
    (ICase var (fst (optimize_Case_instruction env var case)), contextRuleInstruction default_fuel env i)
| IAssign vid v => (IAssign vid (simpl_sec_opel_expression_d_affectation env v), contextRuleInstruction default_fuel env i)
| SOInstructionBeginLoop e => (SOInstructionBeginLoop (simpl_exp env e), Some env)
| SOInstructionConverge e => (SOInstructionConverge (simpl_exp env e), Some env)
| SOInstructionEndBranch e => (SOInstructionEndBranch (simpl_exp env e), Some env)
| _ => (i, contextRuleInstruction default_fuel env i)
end
with optimize_Elif_instruction env i := match i with
| IElse ins => (IElse (fst (optimize_Instruction_s env ins)), contextRuleElif_instruction default_fuel env i)
| IElif cond ins_true ins_false =>
    (IElif cond (fst (optimize_Instruction_s env ins_true)) (fst (optimize_Elif_instruction env ins_false)), 
      contextRuleElif_instruction default_fuel env i)
end 
with optimize_Case_instruction env var i := match i with
| ICaseFinishDefault ins_s => 
    (ICaseFinishDefault (fst (optimize_Instruction_s env ins_s)), contextRuleCase_instruction default_fuel env var i)
| ICaseCons (exp, ins_s) case_t => 
    (
      (ICaseCons (simpl_exp env exp, 
      (match evalExpression_f env exp with
      | Some (Int v) =>
        (
          if will_slimSet_set var env then
          (
            match slimSetIdsValue var (Int v) env with
            | Some env' => fst (optimize_Instruction_s env' ins_s) 
            | None => fst (optimize_Instruction_s env ins_s) 
            end
          )
          else
          (
            fst (optimize_Instruction_s env ins_s) 
          )
        )
      | _ => fst (optimize_Instruction_s env ins_s)
      end)) (fst (optimize_Case_instruction env var case_t))), contextRuleCase_instruction default_fuel env var i)
end
with optimize_Instruction_s env i := match i with
| ISeqEnd h => 
  (
    let (h_opt, h_env) := (optimize_Instruction env h) in 
    (ISeqEnd h_opt, h_env)
  ) 
| ISeqCons h t => 
  (
    let (h_opt, h_env) := optimize_Instruction env h in 
    let (t_opt, t_env) := 
      (
        match h_env with
        | Some outenv' => (optimize_Instruction_s outenv' t)
        | None => (t, None)
        end
      ) in
    (ISeqCons h_opt t_opt, t_env)
  )
end.

Opaque contextRuleInstruction.
Opaque contextRuleInstruction_s.
Opaque contextRuleCase_instruction.
Opaque contextRuleElif_instruction.
Opaque evalSec_opel_expression_if_f.

Theorem optimize_correct : 
  (
    forall in_data f res,
    evalInstruction in_data f res -> forall e,
    envpartialView e (fst in_data) ->
    evalInstruction in_data (fst (optimize_Instruction e f)) res /\ 
    (forall outenv, 
      (snd (optimize_Instruction e f)) = Some outenv -> 
      extractErrorType res = None -> envpartialView outenv (extractEnv res))
  ) /\ 
  (
    forall in_data f res,
    evalInstruction_s in_data f res -> forall e,
    envpartialView e (fst in_data) ->
    evalInstruction_s in_data (fst (optimize_Instruction_s e f)) res /\ 
    (forall outenv, 
      (snd (optimize_Instruction_s e f)) = Some outenv -> 
      extractErrorType res = None -> envpartialView outenv (extractEnv res))
  ) /\ 
  (
    forall in_data f res,
    evalElif_instruction in_data f res -> forall e,
    envpartialView e (fst in_data) -> 
    evalElif_instruction in_data (fst (optimize_Elif_instruction e f)) res /\ 
    (forall outenv,  
      (snd (optimize_Elif_instruction e f)) = Some outenv -> 
      extractErrorType res = None -> envpartialView outenv (extractEnv res))
  ) /\ 
  (
    forall in_data exp f res,
    evalCase_instruction in_data exp f res -> forall e,
    envpartialView e (fst in_data) ->
    evalCase_instruction in_data exp (fst (optimize_Case_instruction e exp f)) res /\ 
    (forall outenv, 
      (snd (optimize_Case_instruction e exp f)) = Some outenv -> 
      extractErrorType res = None -> envpartialView outenv (extractEnv res)) 
  ).
Proof.
apply sem_inst_mutind; try (solve [split; [econstructor; eauto|unfold optimize_Instruction; unfold snd; intros; 
eapply contextRuleCorrect_Instruction; eauto; econstructor; eauto]]).

split.
simpl.
econstructor; eauto.
eapply simpl_sec_opel_expression_d_affectation_correct; eauto.
intros.
assert (evalInstruction (env, evn) (IAssign var aff) (OK env_f evn)) by (econstructor; eauto).
simpl in H0.
eapply contextRuleCorrect_Instruction with (in_data:=(env, evn)) ; eauto.

simpl.
split.
eapply evalInstructionIIfTrue; eauto.
eapply simpl_sec_opel_expression_if_correct; eauto.
eapply H; eauto.
intros.
eapply contextRuleCorrect_Instruction with (in_data:=(env, evn)) ; eauto.
eapply evalInstructionIIfTrue; eauto.
intros.
simpl.
split.
eapply evalInstructionIIfFalse; eauto.
eapply simpl_sec_opel_expression_if_correct; eauto.
eapply H; eauto.
intros.
eapply contextRuleCorrect_Instruction with (in_data:=(env, evn)); eauto.
eapply evalInstructionIIfFalse; eauto.
intros.
simpl.
split.
eapply evalInstructionICase; eauto.
eapply H; eauto.
intros.
eapply contextRuleCorrect_Instruction with (in_data:=(env, evn)); eauto.
eapply evalInstructionICase; eauto.

(*  Loop True *)
simpl.
intros.
assert (D := envpartialViewEvalSec_opel_w e2 env cond H1).
apply evalSec_opel_w_f_correct in e.
rewrite e in D.
apply evalSec_opel_w_f_correct in e.
destruct D; rewrite H2.
case_eq (forgetInstruction_s ins e2); intros.

simpl.
split.
eapply evalInstructionILoopTrueOk with (env':=env') (evn':=evn'); eauto.
eapply evalSec_opel_w_f_correct.
eapply evalSec_opel_w_f_correct in e.
rewrite <- e.
eapply simpl_sec_opel_w_correct; eauto.
apply forgetInstruction_is_lower in H3.
eapply envpartialView_trans; eauto.
eapply H; eauto.
apply forgetInstruction_is_lower in H3.
eapply envpartialView_trans; eauto.
assert (extractErrorType (OK env' evn') = None) by (simpl; auto).
assert 
  (D := 
  forgetInstruction_is_lower_after_exec 
  (env, evn) 
  ins 
  (OK env' evn') e0 H4 e2 e3 H1 H3).
assert (D' := H0 e3 D).
destruct D'.
simpl in *.
case_eq (evalSec_opel_w_f e3 cond); intros; rewrite H7 in *.
assert (forgetInstruction_s ins e3 = Some e3).
eapply forgetInstruction_is_idempoten; eauto.
rewrite H8 in *.
induction v; auto.
induction n; auto.
apply forgetInstruction_is_lower in H3.
assert (D' := envpartialViewEvalSec_opel_w e3 e2 cond H3).
rewrite H7, H2 in D'.
destruct D'; inversion H9.
assert (forgetInstruction_s ins e3 = Some e3).
eapply forgetInstruction_is_idempoten; eauto.
rewrite H8 in H5; auto.
assert (extractErrorType (OK env' evn') = None) by (simpl; auto).
assert 
  (D := 
  forgetInstruction_is_lower_after_exec 
  (env, evn) 
  ins 
  (OK env' evn') e0 H4 e2 e3 H1 H3).
intros.
eapply H0; eauto.
unfold snd in H5.
inversion H5; subst.
simpl.
assert (forgetInstruction_s ins outenv = Some outenv).
eapply forgetInstruction_is_idempoten; eauto.
rewrite H7.
induction (evalSec_opel_w_f outenv cond); auto.
induction a; auto.
induction n; auto.
split.
eapply evalInstructionILoopTrueOk with (env':=env') (evn':=evn'); eauto.
intros.
unfold snd in H4.
inversion H4.
case_eq (forgetInstruction_s ins e2); intros.
split.
eapply evalInstructionILoopTrueOk with (env':=env') (evn':=evn'); eauto.
apply evalSec_opel_w_f_correct in e.
apply evalSec_opel_w_f_correct.
rewrite <- e.
eapply simpl_sec_opel_w_correct; eauto.
apply forgetInstruction_is_lower in H3.
eapply envpartialView_trans; eauto.
eapply H; eauto.
apply forgetInstruction_is_lower in H3.
eapply envpartialView_trans; eauto.
assert (extractErrorType (OK env' evn') = None) by (simpl; auto).
assert 
  (D := 
  forgetInstruction_is_lower_after_exec 
  (env, evn) 
  ins 
  (OK env' evn') e0 H4 e2 e3 H1 H3).
assert (D' := H0 e3 D).
destruct D'.
case_eq (evalSec_opel_w_f e3 cond); intros; rewrite H7 in *.
assert (forgetInstruction_s ins e3 = Some e3).
eapply forgetInstruction_is_idempoten; eauto.
rewrite H8 in H5.
induction v; auto.
induction n; auto.
apply forgetInstruction_is_lower in H3.
assert (D' := envpartialViewEvalSec_opel_w e3 e2 cond H3).
rewrite H7, H2 in D'.
destruct D'; inversion H9.
assert (forgetInstruction_s ins e3 = Some e3).
eapply forgetInstruction_is_idempoten; eauto.
rewrite H8 in H5.
auto.
unfold snd.
assert (extractErrorType (OK env' evn') = None) by (simpl; auto).
assert 
  (D := 
  forgetInstruction_is_lower_after_exec 
  (env, evn) 
  ins 
  (OK env' evn') e0 H4 e2 e3 H1 H3).
intros.
eapply H0; eauto.
inversion H5; subst.
simpl.
assert (forgetInstruction_s ins outenv = Some outenv).
eapply forgetInstruction_is_idempoten; eauto.
rewrite H7.
induction (evalSec_opel_w_f outenv cond); auto.
induction a; auto.
induction n; auto.
split.
eapply evalInstructionILoopTrueOk with (env':=env') (evn':=evn'); eauto.
intros.
inversion H4.

intros.
simpl.
assert (D := envpartialViewEvalSec_opel_w e2 env cond H0).
apply evalSec_opel_w_f_correct in e0; rewrite e0 in D; apply evalSec_opel_w_f_correct in e0.
destruct D; rewrite H1.
case_eq (forgetInstruction_s ins e2); intros; simpl.
split.
eapply evalInstructionILoopTrueError; eauto.
apply evalSec_opel_w_f_correct in e0.
apply evalSec_opel_w_f_correct.
rewrite <- e0.
eapply simpl_sec_opel_w_correct; eauto.
apply forgetInstruction_is_lower in H2.
eapply envpartialView_trans; eauto.
eapply H; eauto.
apply forgetInstruction_is_lower in H2.
eapply envpartialView_trans; eauto.
intros.
inversion H4.
split.
eapply evalInstructionILoopTrueError; eauto.
intros.
inversion H4.
case_eq (forgetInstruction_s ins e2); intros; simpl.
split.
eapply evalInstructionILoopTrueError; eauto.
apply evalSec_opel_w_f_correct in e0.
apply evalSec_opel_w_f_correct.
rewrite <- e0.
eapply simpl_sec_opel_w_correct; eauto.
apply forgetInstruction_is_lower in H2.
eapply envpartialView_trans; eauto.
eapply H; eauto.
apply forgetInstruction_is_lower in H2.
eapply envpartialView_trans; eauto.
intros.
inversion H4.
split.
eapply evalInstructionILoopTrueError; eauto.
intros.
inversion H4.

(*Loop False*)
simpl.
intros.
assert (D := envpartialViewEvalSec_opel_w e0 env cond H).
apply evalSec_opel_w_f_correct in e; rewrite e in D; apply evalSec_opel_w_f_correct in e.
destruct D; rewrite H0.
simpl.
split.
econstructor; eauto.
intros.
inversion H1; subst; auto.
case_eq (forgetInstruction_s ins e0); intros; simpl.
split.
eapply evalInstructionILoopFalse; eauto.
apply evalSec_opel_w_f_correct in e.
apply evalSec_opel_w_f_correct.
rewrite <- e.
eapply simpl_sec_opel_w_correct; eauto.
apply forgetInstruction_is_lower in H1.
eapply envpartialView_trans; eauto.
intros.
inversion H2; subst; auto.
apply forgetInstruction_is_lower in H1.
eapply envpartialView_trans; eauto.
split.
eapply evalInstructionILoopFalse; eauto.
intros.
inversion H2.

(*  BeginLoop *)
simpl.
intros.
split.
econstructor; eauto.
intros.
inversion H0; subst; auto.

(*  Converge *)
simpl.
intros.
split.
econstructor; eauto.
intros.
inversion H0; subst; auto.

(*  EndBranch *)
simpl.
intros.
split.
econstructor; eauto.
intros.
inversion H0; subst; auto.

(*Seq*)
simpl.
intros.
assert (D := H e0 H0).
case_eq (optimize_Instruction e0 i); intros; rewrite H1 in *.
simpl in *.
split.
econstructor; eauto.
eapply D; eauto.
eapply D; eauto.

simpl.
intros.
assert (D := H e1 H1).
case_eq (optimize_Instruction e1 h); intros; rewrite H2 in *.
simpl in *.
induction o.
case_eq (optimize_Instruction_s a t); intros.
simpl in *.
split.
eapply evalInstruction_sISeqConsOk; eauto.
eapply D; eauto.
assert (envpartialView a env').
eapply D; eauto.
assert (D0 := H0 a H4).
rewrite H3 in *.
simpl in *.
eapply D0; eauto.
assert (envpartialView a env').
eapply D; eauto.
assert (D0 := H0 a H4).
rewrite H3 in *.
simpl in *.
eapply D0; eauto.
simpl.
split.
eapply evalInstruction_sISeqConsOk; eauto.
eapply D; eauto.
intros; einversion.

intros.
split; [|intros; einversion].
simpl.
assert (D := H e1 H0).
case_eq (optimize_Instruction e1 h); intros; rewrite H1 in *; simpl in *.
induction o; simpl; [case_eq (optimize_Instruction_s a t); intros|] ; eapply evalInstruction_sISeqConsError; eauto; eapply D.

simpl.
intros.
assert (D := H e0 H0).
destruct D.
split; intros; eauto.
econstructor; eauto.
eapply contextRuleCorrect_Elif_instruction with (in_data:=(env, evn)); eauto.
econstructor; eauto.

simpl.
intros.
assert (D := H e1 H0).
destruct D.
split; intros; eauto.
econstructor; eauto.
eapply contextRuleCorrect_Elif_instruction with (in_data:=(env, evn)); eauto.
econstructor; eauto.

simpl.
intros.
assert (D := H e1 H0).
destruct D.
split; intros; eauto.
eapply evalInstruction_sIElifFalse; eauto.
assert (evalElif_instruction (env, evn) (IElif cond ins_true ins_false) res).
eapply evalInstruction_sIElifFalse; eauto.
eapply contextRuleCorrect_Elif_instruction with (in_data:=(env, evn)) ; eauto.

simpl.
intros.
assert (D := H e0 H0).
destruct D.
split; intros; eauto.
econstructor; eauto.
inversion H4.

simpl.
intros.
assert (D := H e1 H0).
destruct D.
split; intros; eauto.
econstructor; eauto.
inversion H4.

simpl.
intros.
split.
econstructor; eauto.
eapply evalExpression_f_correct in e.
eapply evalExpression_f_correct.
rewrite <- e.
eapply evalExpression_f_bin_repl; eauto.
eapply simpl_exp_correct; eauto.
assert (D := H e1 H0).
destruct D.
case_eq (evalExpression_f e1 exp); intros.
induction v; eauto.
assert (D := envpartialViewEvalExpression e1 env exp H0).
rewrite H3 in D.
destruct D.
apply eq_sym in H4.
apply evalExpression_f_correct in e.
assert (D := evalExpression_equal_case env var exp n e H4).
apply evalExpression_f_correct in e.
Transparent evalExpression_f.
simpl in D.
case_eq (getIdsValue var env); intros; rewrite H5 in D; try einversion.
induction v; try einversion.
induction v; try einversion.
inversion D; subst.
assert (D0 := partialViewSlimsetWhenUpIsConst var e1 env (Int n) H0 H5).
case_eq (will_slimSet_set var e1); intros.
destruct D0.
rewrite H7.
eapply H1; eauto.
rewrite H7.
case_eq (forgetIdsValue var e1); intros.
assert (envpartialView e2 env).
eapply envpartialView_trans; eauto.
eapply envpartialViewForgetVsNothing; eauto.
eapply envpartialView_refl; eauto.
eapply H; eauto.
eapply H; eauto.
eapply H1; eauto.
case_eq (will_slimSet_set var e1); intros.
inversion D; subst.
case_eq (slimSetIdsValue var (Int n) e1); intros.
assert (envpartialView e2 env).
eapply partialViewSlimsetActualValue; eauto.
eapply H; eauto.
eapply H; eauto.
eapply H1; eauto.
inversion H4.
eapply H1; eauto.
unfold snd.
intros.
assert (evalCase_instruction (env, evn) var (ICaseCons (exp, ins_s) case_t) res).
econstructor; eauto.
intros.
eapply contextRuleCorrect_Case_instruction with (in_data:=(env, evn)); eauto.

intros.
split.
eapply evalICaseConsFalse; eauto.
eapply evalExpression_f_correct in e.
eapply evalExpression_f_correct.
rewrite <- e.
eapply evalExpression_f_bin_repl; eauto.
eapply simpl_exp_correct; eauto.
eapply H; eauto.
intros.
assert (evalCase_instruction (env, evn) var (ICaseCons (exp, ins_s) case_t) res).
eapply evalICaseConsFalse; eauto.
simpl in H1.
eapply contextRuleCorrect_Case_instruction with (in_data:=(env, evn)); eauto.
Opaque evalExpression_f.
Qed.

Definition exp_simpl_optimize ctx ins := fst (optimize_Instruction_s ctx ins).

(* Theorem exp_simpl_optimize_instruction_s_correct : contextBased_optimizer_is_correct exp_simpl_optimize.
Proof.
unfold contextBased_optimizer_is_correct.
unfold exp_simpl_optimize.
intros.
eapply optimize_correct; eauto.
Qed. *)

Definition simpl_ed ctx ed := 
  {|  f_name := ed.(f_name); 
      f_args := ed.(f_args); f_local_decs := 
        (match copyInPartialView_f ctx ed.(f_args) with
         | Some env' => simpl_initLocals env' ed.(f_local_decs)
         | _ => ed.(f_local_decs)
         end
        );
      f_instrs := 
        (match copyInPartialView_f ctx ed.(f_args) with
          | Some env' => 
            (
              match initLocalsPartialView_f env' ed.(f_local_decs) with
              | Some f_env => 
                  fst (optimize_Instruction_s f_env ed.(f_instrs))
              | None => 
                  ed.(f_instrs)
              end
            )
          | None => ed.(f_instrs) end) |}.

Theorem simpl_ed_correct' : forall de e (run_env : env) inevnt
       (res : interpretationResult) (params : association_d_arguments),
     envpartialView (reasonably_smallest_environment de) run_env ->
     execFunction (run_env, inevnt) e params res ->
     execFunction (run_env, inevnt)
       (simpl_ed (reasonably_smallest_environment de) e) params
       res.
Proof.
intros.
inversion H0; subst.
unfold simpl_ed.
case_eq (copyInPartialView_f (reasonably_smallest_environment de) (f_args e)); intros; econstructor; eauto.
simpl.
eapply simpl_initLocals_equiv; eauto.
apply copyIn_f_correct in H3.
eapply copyInPartialView_f_correct; eauto.
simpl.
case_eq (initLocalsPartialView_f e0 (f_local_decs e)); intros; simpl; try econstructor; eauto.
eapply optimize_correct; eauto.
apply copyIn_f_correct in H3.
assert (exists trace, initLocals (f_env, inevnt) (f_local_decs e) (f_env', trace)) by eauto.
apply initLocals_f_correct in H6.
eapply initLocalsPartialView_f_correct in H2; eauto.
eapply copyInPartialView_f_correct; eauto.
unfold simpl_ed.
case_eq (copyInPartialView_f (reasonably_smallest_environment de) (f_args e)); intros; econstructor; eauto.
simpl.
eapply simpl_initLocals_equiv; eauto.
apply copyIn_f_correct in H3.
eapply copyInPartialView_f_correct; eauto.
simpl.
case_eq (initLocalsPartialView_f e0 (f_local_decs e)); intros; simpl; try econstructor; eauto.
eapply optimize_correct; eauto.
apply copyIn_f_correct in H3.
assert (exists trace, initLocals (f_env, inevnt) (f_local_decs e) (f_env', trace)) by eauto.
apply initLocals_f_correct in H5.
eapply initLocalsPartialView_f_correct in H2; eauto.
eapply copyInPartialView_f_correct; eauto.
Qed.

Theorem simpl_ed_correct : forall env in_data ed params res,
  envpartialView env (fst in_data) ->
  execFunction in_data ed params res -> 
  execFunction in_data (simpl_ed env ed) params res.
Proof.
induction 2; intros.
unfold simpl_ed.
case_eq (copyInPartialView_f (env) (f_args f)); intros.
econstructor; eauto.
simpl.
eapply simpl_initLocals_equiv; eauto.
apply copyIn_f_correct in H0.
eapply copyInPartialView_f_correct; eauto.
simpl.
case_eq (initLocalsPartialView_f e (f_local_decs f)); intros; simpl; try econstructor; eauto.
eapply optimize_correct; eauto.
apply copyIn_f_correct in H0.
assert (exists trace, initLocals (f_env, b_evn) (f_local_decs f) (f_env', trace)) by eauto.
apply initLocals_f_correct in H6.
eapply initLocalsPartialView_f_correct in H5; eauto.
eapply copyInPartialView_f_correct; eauto.
econstructor; eauto.

unfold simpl_ed.
case_eq (copyInPartialView_f (env) (f_args f)); intros.
econstructor; eauto.
simpl.
eapply simpl_initLocals_equiv; eauto.
apply copyIn_f_correct in H0.
eapply copyInPartialView_f_correct; eauto.
simpl.
case_eq (initLocalsPartialView_f e (f_local_decs f)); intros; simpl; try econstructor; eauto.
eapply optimize_correct; eauto.
apply copyIn_f_correct in H0.
assert (exists trace, initLocals (f_env, b_evn) (f_local_decs f) (f_env', trace)) by eauto.
apply initLocals_f_correct in H5.
eapply initLocalsPartialView_f_correct in H4; eauto.
eapply copyInPartialView_f_correct; eauto.
econstructor; eauto.
Qed.

Definition simpl_declaration_2_body lowest_run_env d := match d with
| Fun_def ed => Fun_def (simpl_ed lowest_run_env ed)
| r => r
end.

Definition simpl_declaration_2_bodyList ctx lib := 
  List.map (simpl_declaration_2_body ctx) lib.

Theorem simpl_declaration_2_bodyList_correct : forall dec inenv inenv' outenv outenv' run_env,
  env_optimization_correct' run_env inenv inenv' ->
  evalDeclaration_2_bodyList inenv dec outenv ->
  evalDeclaration_2_bodyList inenv' (simpl_declaration_2_bodyList run_env dec) outenv' ->
  env_optimization_correct' run_env outenv outenv'.
Proof.
induction dec; simpl; intros; inversion H0; inversion H1; subst; auto.
eapply IHdec with (inenv:=env'); eauto.
inversion H5; subst; simpl in *; inversion H11; simpl in *; subst.
eapply env_optimization_correct_declareFunction; eauto.
assert (D := env_optimization_correct_declareFunction fid fparams run_env inenv inenv' env'1 env'2 H H2 H12).
eapply env_optimization_correct_defineFunction; eauto.
eapply env_optimization_correct_defineFunction with 
  (e:=inenv)
  (e':=inenv')
  (de:=env') 
  (de':=env'0) 
  (f := ed)
  (f' := simpl_ed run_env ed)
  ; eauto.
intros.
eapply simpl_ed_correct; eauto.
Qed.

Definition simpl_compilation (p : preprocessor) (c: compilation) := match c with
| (incls, unt) => 
    (incls, (
      match p incls (getName unt) with
      | (globalV, declF, exts) =>
        ( 
          match unt with
          | Compilation_Body ed => 
              (
                match defineFunction ed ({|
                          localVars := [];
                          globalVars := globalV;
                          funDefs := [];
                          localfunDecls := declF;
                          externals := exts
                        |}) with
                | Some ctx_basis => Compilation_Body (simpl_ed (reasonably_smallest_environment ctx_basis) ed) 
                | None => unt
                end
              )
           | Compilation_PkgDecl id dec =>
              ( Compilation_PkgDecl id (simpl_declaration_de_baseList ({|
                            localVars := [];
                            globalVars := globalV;
                            funDefs := [];
                            localfunDecls := declF;
                            externals := exts
                          |}) dec)
              )
           | Compilation_PkgBody id dec fdec =>
              ( Compilation_PkgBody id (simpl_declaration_1_bodyList ({|
                            localVars := [];
                            globalVars := globalV;
                            funDefs := [];
                            localfunDecls := declF;
                            externals := exts
                          |}) dec) 
                          (
                            match evalDeclaration_1_bodyList_f ({|
                                        localVars := [];
                                        globalVars := globalV;
                                        funDefs := [];
                                        localfunDecls := declF;
                                        externals := exts
                                      |}) dec with
                            | Some ctx_basis_1 => 
                                (
                                  match evalDeclaration_2_bodyList_f ctx_basis_1 fdec with
                                  | Some ctx_basis => simpl_declaration_2_bodyList (reasonably_smallest_environment ctx_basis) fdec
                                  | None => fdec
                                  end
                                )
                            | None => fdec
                            end
                         )
            )
      end)
    end))
     
end.

(* Lemma p_getname : forall (p : preprocessor) a b opt ctx, p a (getName (encapsuler_for_unite ctx opt b)) = p a (getName b).
Proof.
induction b; eauto.
Qed. *)














