Require Import PartialView Semantics Semantics_f Ast Environment Values Ast TacUtils MapTypes Map.
Require Import List String ZArith Lia.
Require Import Classical.
Import ListNotations.

Lemma partialViewDeclareVC : forall e e' e1 e1' vid v, 
envpartialView e e' ->
declareLocalVar vid (ConstantValue v) e = Some e1 -> 
declareLocalVar vid (ConstantValue v) e' = Some e1' -> 
envpartialView e1 e1'.
Proof.
unfold envpartialView.
unfold declareLocalVar.
intros.
destruct H.
destruct H2.
destruct H3.
destruct H4.
case_eq (MapTypes.StringMapModule.get vid (localVars e)); intros; rewrite H6 in *; try einversion.
eapply mapPartialView_getNone_l2b in H6; eauto; rewrite H6 in *; try einversion.
case_eq (MapTypes.StringMapModule.get vid (funDefs e)); intros; rewrite H4, H7 in *; try einversion.
case_eq (MapTypes.StringMapModule.get vid (localfunDecls e)); intros; rewrite H3, H8 in *; try einversion.
case_eq (MapTypes.StringMapModule.get vid (externals e)); intros; rewrite H9 in *; try einversion.
case_eq (MapTypes.StringMapModule.get vid (externals e')); intros; rewrite H10 in *; try einversion.
inversion H0; inversion H1; subst; repeat (split; eauto); simpl.
apply mapPartialView_setV; auto.
Qed.

Lemma mapPartialView_setVCUndef: forall lower bigger,
  mapPartialView lower bigger ->
  forall id v, 
  mapPartialView (StringMapModule.set id (ConstantValue Undefined) lower) (StringMapModule.set id (ConstantValue v) bigger).
Proof.
unfold mapPartialView.
intros.
rewrite StringMapModule.gsspec.
rewrite StringMapModule.gsspec.
induction (String_Equality.eq id0 id); auto.
right.
eexists; eauto.
Qed.

Lemma partialViewDeclareVUndefinedC : forall e e' e1 e1' vid v, 
envpartialView e e' ->
declareLocalVar vid (ConstantValue Undefined) e = Some e1 -> 
declareLocalVar vid (ConstantValue v) e' = Some e1' -> 
envpartialView e1 e1'.
Proof.
unfold envpartialView.
unfold declareLocalVar.
intros.
destruct H.
destruct H2.
destruct H3.
destruct H4.
case_eq (MapTypes.StringMapModule.get vid (localVars e)); intros; rewrite H6 in *; try einversion.
eapply mapPartialView_getNone_l2b in H6; eauto; rewrite H6 in *; try einversion.
case_eq (MapTypes.StringMapModule.get vid (funDefs e)); intros; rewrite H4, H7 in *; try einversion.
case_eq (MapTypes.StringMapModule.get vid (localfunDecls e)); intros; rewrite H3, H8 in *; try einversion.
case_eq (MapTypes.StringMapModule.get vid (externals e)); intros; rewrite H9 in *; try einversion.
case_eq (MapTypes.StringMapModule.get vid (externals e')); intros; rewrite H10 in *; try einversion.
inversion H0; inversion H1; subst; repeat (split; eauto); simpl.
apply mapPartialView_setVCUndef; auto.
Qed.

Lemma partialViewDeclareV : forall e e' e1 e1' vid v, 
envpartialView e e' ->
declareLocalVar vid (VariableValue v) e = Some e1 -> 
declareLocalVar vid (VariableValue v) e' = Some e1' -> 
envpartialView e1 e1'.
Proof.
unfold envpartialView.
unfold declareLocalVar.
intros.
destruct H.
destruct H2.
destruct H3.
destruct H4.
case_eq (MapTypes.StringMapModule.get vid (localVars e)); intros; rewrite H6 in *; try einversion.
eapply mapPartialView_getNone_l2b in H6; eauto; rewrite H6 in *; try einversion.
case_eq (MapTypes.StringMapModule.get vid (funDefs e)); intros; rewrite H4, H7 in *; try einversion.
case_eq (MapTypes.StringMapModule.get vid (localfunDecls e)); intros; rewrite H3, H8 in *; try einversion.
case_eq (MapTypes.StringMapModule.get vid (externals e)); intros; rewrite H9 in *; try einversion.
case_eq (MapTypes.StringMapModule.get vid (externals e')); intros; rewrite H10 in *; try einversion.
inversion H0; inversion H1; subst; repeat (split; eauto); simpl.
apply mapPartialView_setV; auto.
Qed.

Lemma partialViewDeclareVUndefined : forall e e' e1 e1' vid v, 
envpartialView e e' ->
declareLocalVar vid (VariableValue Undefined) e = Some e1 -> 
declareLocalVar vid (VariableValue v) e' = Some e1' -> 
envpartialView e1 e1'.
Proof.
unfold envpartialView.
unfold declareLocalVar.
intros.
destruct H.
destruct H2.
destruct H3.
destruct H4.
case_eq (MapTypes.StringMapModule.get vid (localVars e)); intros; rewrite H6 in *; try einversion.
eapply mapPartialView_getNone_l2b in H6; eauto; rewrite H6 in *; try einversion.
case_eq (MapTypes.StringMapModule.get vid (funDefs e)); intros; rewrite H4, H7 in *; try einversion.
case_eq (MapTypes.StringMapModule.get vid (localfunDecls e)); intros; rewrite H3, H8 in *; try einversion.
case_eq (MapTypes.StringMapModule.get vid (externals e)); intros; rewrite H9 in *; try einversion.
case_eq (MapTypes.StringMapModule.get vid (externals e')); intros; rewrite H10 in *; try einversion.
inversion H0; inversion H1; subst; repeat (split; eauto); simpl.
apply mapPartialView_setVUndef; auto.
Qed.

Lemma partialViewEnvCopyIn : forall sig params lin lout upin upout,
  envpartialView lin upin ->
  copyIn_f lin sig params = Some lout ->
  copyIn_f upin sig params = Some upout ->
  envpartialView lout upout.
Proof.
induction sig, params; simpl; intros; try einversion; inversion H0; inversion H1; clear H3; clear H4; subst; eauto.
destruct H.
destruct H2.
destruct H3.
destruct H4.
unfold envpartialView.
repeat (split; eauto).
simpl.
apply mapPartialView_refl.
induction a, a, m; try einversion.
induction a, a, m.
induction e.
case_eq (copyIn_f lin sig params); intros; rewrite H2 in H0; [|inversion H0].
case_eq (copyIn_f upin sig params); intros; rewrite H3 in H1; [|inversion H1].
assert (D := envpartialViewEvalExpression lin upin e H).
destruct D.
rewrite H4 in *.
case_eq (evalExpression_f upin e); intros; rewrite H5 in *; inversion H0; inversion H1; subst.
assert ( envpartialView e0 e1) by eauto.
eapply partialViewDeclareVC; eauto.
rewrite H4 in H0.
case_eq (evalExpression_f upin e); intros; rewrite H5 in *; inversion H0; inversion H1; subst.
assert ( envpartialView e0 e1) by eauto.
eapply partialViewDeclareVUndefinedC; eauto.
case_eq (copyIn_f lin sig params); intros; rewrite H2 in H0; [|inversion H0].
case_eq (copyIn_f upin sig params); intros; rewrite H3 in H1; [|inversion H1].
assert ( envpartialView e e0) by eauto.
eapply partialViewDeclareVC; eauto.
induction e; try einversion.
induction e; try einversion.
assert (D := envpartialViewEvalExpression lin upin (EVar n) H).
destruct D.
simpl in H2.
rewrite H2 in *.
clear H2.
case_eq (copyIn_f lin sig params); intros; rewrite H2 in H0; [|inversion H0].
case_eq (copyIn_f upin sig params); intros; rewrite H3 in H1; [|inversion H1].
case_eq (getIdsValue n upin); intros; rewrite H4 in *; try einversion; subst.
induction v; try einversion.
induction v; try einversion.
assert ( envpartialView e e0) by eauto.
eapply partialViewDeclareV; eauto.
assert ( envpartialView e e0) by eauto.
eapply partialViewDeclareV; eauto.
assert ( envpartialView e e0) by eauto.
eapply partialViewDeclareV; eauto.
simpl in H2.
rewrite H2 in *.
clear H2.
case_eq (copyIn_f lin sig params); intros; rewrite H2 in H0; [|inversion H0].
case_eq (copyIn_f upin sig params); intros; rewrite H3 in H1; [|inversion H1].
case_eq (getIdsValue n upin); intros; rewrite H4 in *; try einversion; subst.
induction v; try einversion.
induction v; try einversion.
assert ( envpartialView e e0) by eauto.
eapply partialViewDeclareVUndefined; eauto.
assert ( envpartialView e e0) by eauto.
eapply partialViewDeclareVUndefined; eauto.
assert ( envpartialView e e0) by eauto.
eapply partialViewDeclareV; eauto.
induction e; try einversion.
induction e; try einversion.
assert (D := envpartialViewEvalExpression lin upin (EVar n) H).
destruct D.
simpl in H2.
rewrite H2 in *.
clear H2.
case_eq (copyIn_f lin sig params); intros; rewrite H2 in H0; [|inversion H0].
case_eq (copyIn_f upin sig params); intros; rewrite H3 in H1; [|inversion H1].
case_eq (getIdsValue n upin); intros; rewrite H4 in *; try einversion; subst.
induction v; try einversion.
induction v; try einversion.
assert ( envpartialView e e0) by eauto.
eapply partialViewDeclareV; eauto.
assert ( envpartialView e e0) by eauto.
eapply partialViewDeclareV; eauto.
assert ( envpartialView e e0) by eauto.
eapply partialViewDeclareV; eauto.
simpl in H2.
rewrite H2 in *.
clear H2.
case_eq (copyIn_f lin sig params); intros; rewrite H2 in H0; [|inversion H0].
case_eq (copyIn_f upin sig params); intros; rewrite H3 in H1; [|inversion H1].
case_eq (getIdsValue n upin); intros; rewrite H4 in *; try einversion; subst.
induction v; try einversion.
induction v; try einversion.
assert ( envpartialView e e0) by eauto.
eapply partialViewDeclareVUndefined; eauto.
assert ( envpartialView e e0) by eauto.
eapply partialViewDeclareVUndefined; eauto.
assert ( envpartialView e e0) by eauto.
eapply partialViewDeclareV; eauto.
Qed.
  
Lemma partialViewEnvInitLocals : forall objs lin lout upin upout,
  envpartialView lin upin ->
  initLocals_f lin objs = Some lout ->
  initLocals_f upin objs = Some upout ->
  envpartialView lout upout.
Proof.
induction objs; simpl; intros. 
inversion H0; inversion H1; subst; eauto.
induction a.
induction o.
assert (D := envpartialViewEvalExpression lin upin e H).
destruct D.
rewrite H2 in *.
case_eq (evalExpression_f upin e); intros; rewrite H3 in *; try einversion.
case_eq (evalExpression_f upin e); intros; rewrite H3 in *; try einversion.
case_eq (declareLocalVar t (ConstantValue v) lin); intros; rewrite H5 in *; try einversion.
case_eq (declareLocalVar t (ConstantValue v) upin); intros; rewrite H6 in *; try einversion.
assert (envpartialView e0 e1) by (eapply partialViewDeclareVC; eauto).
eapply IHobjs; eauto.
rewrite H2 in *.
case_eq (evalExpression_f upin e); intros; rewrite H3 in *; try einversion.
case_eq (declareLocalVar t (ConstantValue Undefined) lin); intros; rewrite H4 in *; try einversion.
case_eq (declareLocalVar t (ConstantValue v) upin); intros; rewrite H5 in *; try einversion.
assert (envpartialView e0 e1) by (eapply partialViewDeclareVUndefinedC; eauto).
eapply IHobjs; eauto.
case_eq (declareLocalVar t (VariableValue (decValue t0)) lin); intros; rewrite H2 in *; try einversion.
case_eq (declareLocalVar t (VariableValue (decValue t0)) upin); intros; rewrite H3 in *; try einversion.
assert (envpartialView e e0) by (eapply partialViewDeclareV; eauto).
eapply IHobjs; eauto.
eauto.
Qed.


Fixpoint projectMapExternalVarC src dst := match dst with
| [] => []
| (h, Environment.EVar (VariableValue Undefined))::t => (h, Environment.EVar (VariableValue Undefined))::(projectMapExternalVarC src t)
| (h, Environment.EVar (VariableValue v))::t => (h, Environment.EVar (VariableValue (match StringMapModule.get h src with | Some (Environment.EVar (VariableValue vsrc)) => vsrc | _ => v end)))::(projectMapExternalVarC src t)
| (h, v)::t => (h, v)::(projectMapExternalVarC src t)
end.

(* Lemma projectMapExternalVarC_src_sc : forall dst src id v_src,
  StringMapModule.get id src = Some (Environment.EVar (ConstantValue v_src)) -> 
  StringMapModule.get id (projectMapExternalVarC src dst) = StringMapModule.get id dst.
Proof.
induction dst; simpl; intros; auto.
induction a.
induction b; try induction c; simpl.
induction (String_Equality.eq id a); eauto.
induction v; simpl; case_eq (String_Equality.eq id a); intros;
try apply String_Equality.eq_spec_true in H0; subst; intros; try rewrite H; eauto.
induction (String_Equality.eq id a); eauto.
Qed.

Lemma projectMapExternalVarC_src_f : forall dst src id v_src,
  StringMapModule.get id src = Some (Environment.EFDecl v_src) -> 
  StringMapModule.get id (projectMapExternalVarC src dst) = StringMapModule.get id dst.
Proof.
induction dst; simpl; intros; auto.
induction a.
induction b; try induction c; simpl.
induction (String_Equality.eq id a); eauto.
induction v; simpl; case_eq (String_Equality.eq id a); intros;
try apply String_Equality.eq_spec_true in H0; subst; intros; try rewrite H; eauto.
induction (String_Equality.eq id a); eauto.
Qed.

Lemma projectMapExternalVarC_dst_none : forall dst src id,
  StringMapModule.get id dst = None -> 
  StringMapModule.get id (projectMapExternalVarC src dst) = None.
Proof.
induction dst; simpl; intros; auto.
induction a.
induction b; try induction c; simpl.
induction (String_Equality.eq id a); eauto.
induction v; simpl; case_eq (String_Equality.eq id a); intros; rewrite H0 in *; try einversion; intros;
try apply String_Equality.eq_spec_true in H0; subst; intros; try rewrite H; eauto.
induction (String_Equality.eq id a); eauto.
Qed.

Lemma projectMapExternalVarC_dst_sc : forall dst src id v_src,
  StringMapModule.get id dst = Some (Environment.EVar (ConstantValue v_src)) -> 
  StringMapModule.get id (projectMapExternalVarC src dst) = Some (Environment.EVar (ConstantValue v_src)).
Proof.
induction dst; simpl; intros; auto.
induction a.
induction b; try induction c; simpl.
induction (String_Equality.eq id a); eauto.
induction v; simpl; case_eq (String_Equality.eq id a); intros; rewrite H0 in *; try einversion; try solve [inversion H]; intros;
try apply String_Equality.eq_spec_true in H0; subst; intros; try rewrite H; eauto.
induction (String_Equality.eq id a); eauto.
Qed.

Lemma projectMapExternalVarC_dst_f : forall dst src id v_src,
  StringMapModule.get id dst = Some (Environment.EFDecl v_src) -> 
  StringMapModule.get id (projectMapExternalVarC src dst) = Some (Environment.EFDecl v_src).
Proof.
induction dst; simpl; intros; auto.
induction a.
induction b; try induction c; simpl.
induction (String_Equality.eq id a); eauto.
induction v; simpl; case_eq (String_Equality.eq id a); intros; rewrite H0 in *; try einversion; try solve [inversion H]; intros;
try apply String_Equality.eq_spec_true in H0; subst; intros; try rewrite H; eauto.
induction (String_Equality.eq id a); eauto.
Qed.

Lemma projectMapExternalVarC_dst_sv : forall dst src id v_src,
  StringMapModule.get id dst = Some (Environment.EVar (VariableValue v_src)) -> 
  exists v, StringMapModule.get id (projectMapExternalVarC src dst) = Some (Environment.EVar (VariableValue v)).
Proof.
induction dst; simpl; intros; auto.
inversion H.
induction a.
induction b; try induction c; simpl.
induction (String_Equality.eq id a); eauto.
induction v; simpl; case_eq (String_Equality.eq id a); intros; rewrite H0 in *; try einversion; try solve [inversion H]; intros;
try apply String_Equality.eq_spec_true in H0; subst; intros; try rewrite H; eauto.
induction (String_Equality.eq id a); eauto.
Qed. *)

Fixpoint projectMapLibC src (dst: StringMapModule.t (StringMapModule.t External)) : StringMapModule.t (StringMapModule.t External) := match dst with
| [] => []
| (libname, libcontent)::t => (libname, match StringMapModule.get libname src with | Some libcontent' => projectMapExternalVarC libcontent' libcontent | _ => libcontent end)::(projectMapLibC src t)
end.

(* Lemma projectMapLibC_dst_none: forall dst src id,
  StringMapModule.get id dst = None ->
  StringMapModule.get id (projectMapLibC src dst) = None.
Proof.
induction dst; auto; simpl.
induction a.
intros.
case_eq (String_Equality.eq id a); intros; rewrite H0 in *; auto.
inversion H.
simpl.
rewrite H0.
auto.
Qed. *)

Lemma projectMapLibC_dst_some: forall dst src id lib,
  StringMapModule.get id dst = Some lib ->
  (StringMapModule.get id (projectMapLibC src dst) = Some lib \/
    exists lib', StringMapModule.get id src = Some lib' /\ 
    StringMapModule.get id (projectMapLibC src dst) = Some (projectMapExternalVarC lib' lib))
  .
Proof.
induction dst; auto; simpl.
induction a.
simpl.
intros src id e.
case_eq (String_Equality.eq id a); auto; intros.
inversion H0; subst.
case_eq (StringMapModule.get a src); auto.
intros.
right; auto.
apply String_Equality.eq_spec_true in H; subst.
eauto.
Qed.

Fixpoint copyOut_f2 f_env (sig : list (ident * mode * name)) (app_params : list expression_or_string_lit) c_env := match sig, app_params with
| [], [] => Some ({| localVars := c_env.(localVars);
        globalVars := projectMapGlobal f_env.(globalVars) c_env.(globalVars);
        funDefs := c_env.(funDefs);
        localfunDecls := c_env.(localfunDecls);
        externals := projectMapLibC f_env.(externals) c_env.(externals) |})
| ((idSig, Ast.In, IdTyp)::tSig), (eParam::tParams) => copyOut_f2 f_env tSig tParams c_env
| ((idSig, Ast.InOut, IdTyp)::tSig), ((Exp (EVar eParam))::tParams) => 
    (
      match copyOut_f2 f_env tSig tParams c_env with
      | Some env' => 
        (
          match evalExpression_f f_env (EVar [idSig]) with
          | Some Undefined => forgetIdsValue eParam env'
          | Some v => slimSetIdsValue eParam v env'
          | None => None
          end
        )
      | None => None
      end
    )
| ((idSig, Ast.Out, IdTyp)::tSig), ((Exp (EVar eParam))::tParams) => 
    (
      match copyOut_f2 f_env tSig tParams c_env with
      | Some env' => 
        (
          match evalExpression_f f_env (EVar [idSig]) with
          | Some Undefined => forgetIdsValue eParam env'
          | Some v => slimSetIdsValue eParam v env'
          | None => None
          end
        )
      | None => None
      end
    )
| _, _ => None
end.

(* Lemma projectExtVOr: forall i x t,
  StringMapModule.get i (projectMapExternalVar x t) = 
  StringMapModule.get i t \/
  (StringMapModule.get i (projectMapExternalVar x t) = 
  (StringMapModule.get i x)
  /\ (exists v v', 
        StringMapModule.get i t = Some (Environment.EVar (VariableValue v)) /\ 
        StringMapModule.get i x = Some (Environment.EVar (VariableValue v')))
  ).
Proof.
induction t; eauto.
simpl.
induction a.
simpl.
induction b; simpl.
induction c; simpl.
case_eq (String_Equality.eq i a); intros; eauto.
case_eq (String_Equality.eq i a); intros; simpl; eauto.
apply String_Equality.eq_spec_true in H; subst.
induction (StringMapModule.get a x); eauto.
induction a0; eauto.
induction c; eauto.
right; eauto.
case_eq (String_Equality.eq i a); intros; eauto.
Qed. *)

Lemma projectExtCVOr: forall i x t,
  StringMapModule.get i (projectMapExternalVarC x t) = 
  StringMapModule.get i t \/
  StringMapModule.get i (projectMapExternalVarC x t) = 
  StringMapModule.get i x.
Proof.
induction t; eauto.
simpl.
induction a.
simpl.
induction b; simpl.
induction c; simpl.
case_eq (String_Equality.eq i a); intros; eauto.
case_eq (String_Equality.eq i a); intros; simpl; eauto.
apply String_Equality.eq_spec_true in H; subst.
induction v; simpl; rewrite StringMapModule.Xeq_refl; eauto; 
induction (StringMapModule.get a x); eauto;
induction a0; eauto; induction c; eauto.
induction v; simpl; rewrite H; eauto.
case_eq (String_Equality.eq i a); intros; eauto.
Qed.

(* Theorem projectMapExternalVarCeq: forall dst src id,
  StringMapModule.get id (projectMapExternalVarC src dst) = StringMapModule.get id dst \/
  (
    StringMapModule.get id (projectMapExternalVarC src dst) = StringMapModule.get id src /\
    (exists v, StringMapModule.get id dst = Some (Environment.EVar (VariableValue v)) /\ v <> Undefined) /\
    (exists v, StringMapModule.get id src = Some (Environment.EVar (VariableValue v))) 
  ).
Proof.
induction dst; simpl; eauto.
induction a.
intros.
induction b; simpl; eauto.
induction c; simpl; eauto.
case_eq (String_Equality.eq id a); intros; eauto.
induction v; simpl; eauto.
case_eq (String_Equality.eq id a); intros; eauto.
case_eq (String_Equality.eq id a); intros; eauto;
apply String_Equality.eq_spec_true in H; subst; eauto;
induction (StringMapModule.get a src); eauto; induction a0; eauto; induction c; eauto; right; split; eauto;
split; eauto;
exists (Int n); split; eauto; unfold not; intros; inversion H.
case_eq (String_Equality.eq id a); intros; eauto;
apply String_Equality.eq_spec_true in H; subst; eauto;
induction (StringMapModule.get a src); eauto; induction a0; eauto; induction c; eauto; right; split; eauto;
split; eauto;
exists (Bool n); split; eauto; unfold not; intros; inversion H.
case_eq (String_Equality.eq id a); intros; eauto;
apply String_Equality.eq_spec_true in H; subst; eauto;
induction (StringMapModule.get a src); eauto; induction a0; eauto; induction c; eauto; right; split; eauto;
split; eauto;
exists (STR n); split; eauto; unfold not; intros; inversion H.
case_eq (String_Equality.eq id a); intros; eauto;
apply String_Equality.eq_spec_true in H; subst; eauto;
induction (StringMapModule.get a src); eauto; induction a0; eauto; induction c; eauto; right; split; eauto;
split; eauto;
exists (ArrayV t v); split; eauto; unfold not; intros; inversion H.
case_eq (String_Equality.eq id a); intros; eauto;
apply String_Equality.eq_spec_true in H; subst; eauto;
induction (StringMapModule.get a src); eauto; induction a0; eauto; induction c; eauto; right; split; eauto;
split; eauto;
exists (RecordV r); split; eauto; unfold not; intros; inversion H.
case_eq (String_Equality.eq id a); intros; eauto.
Qed.


Theorem projectMapLibCeq : forall dst src id id0 lib plib,
  StringMapModule.get id (projectMapLibC src dst) = Some lib ->
  StringMapModule.get id dst = Some plib -> 
  (
    ( 
      StringMapModule.get id0 lib = StringMapModule.get id0 plib
    ) 
    \/ 
    (
      exists slib, StringMapModule.get id src = Some slib /\ 
      StringMapModule.get id0 lib = StringMapModule.get id0 slib /\ 
      (exists v, StringMapModule.get id0 plib = Some (Environment.EVar (VariableValue v)) /\ v <> Undefined) /\
      (exists v, StringMapModule.get id0 slib = Some (Environment.EVar (VariableValue v)))
    )
  ).
Proof.
induction dst; simpl; intros; try einversion.
induction a.
simpl in H.
case_eq (String_Equality.eq id a); intros; rewrite H1 in *; eauto.
rewrite String_Equality.eq_spec_true in H1; subst.
case_eq (StringMapModule.get a src); intros; rewrite H1 in H; inversion H; eauto.
inversion H0; subst.
assert (D := projectMapExternalVarCeq plib t id0).
destruct D; eauto.
subst.
inversion H0; subst; eauto.
Qed.

Theorem projectMapLibCeq' : forall dst src id plib,
  StringMapModule.get id dst = Some plib -> 
  exists lib, 
  (
      StringMapModule.get id (projectMapLibC src dst) = Some lib 
  ) /\
  ( forall id0,
    ( 
      StringMapModule.get id0 lib = StringMapModule.get id0 plib
    ) 
    \/ 
    (
      exists slib, StringMapModule.get id src = Some slib /\ 
      StringMapModule.get id0 lib = StringMapModule.get id0 slib /\ 
      (exists v, StringMapModule.get id0 plib = Some (Environment.EVar (VariableValue v)) /\ v <> Undefined) /\
      (exists v, StringMapModule.get id0 slib = Some (Environment.EVar (VariableValue v)))
    )
  ).
Proof.
intros.
assert (exists lib, StringMapModule.get id (projectMapLibC src dst) = Some lib).
assert (D := projectMapLibC_dst_some dst src id plib H).
destruct D; eauto.
destruct H0, H0; eauto.
destruct H0.
exists x; split; intros; try eapply projectMapLibCeq; eauto.
Qed.


Theorem projectMapExternalVareq: forall dst src id,
  StringMapModule.get id (projectMapExternalVar src dst) = StringMapModule.get id dst \/
  (
    StringMapModule.get id (projectMapExternalVar src dst) = StringMapModule.get id src /\
    (exists v, StringMapModule.get id dst = Some (Environment.EVar (VariableValue v))) /\
    (exists v, StringMapModule.get id src = Some (Environment.EVar (VariableValue v))) 
  ).
Proof.
induction dst; simpl; eauto.
induction a.
intros.
induction b; simpl; eauto.
induction c; simpl; eauto.
case_eq (String_Equality.eq id a); intros; eauto.
case_eq (String_Equality.eq id a); intros; eauto.
apply String_Equality.eq_spec_true in H; subst; eauto;
induction (StringMapModule.get a src); eauto; induction a0; eauto; induction c; eauto; right; split; eauto;
split; eauto;
exists (Int n); split; eauto; unfold not; intros; inversion H.
case_eq (String_Equality.eq id a); intros; eauto;
apply String_Equality.eq_spec_true in H; subst; eauto;
induction (StringMapModule.get a src); eauto; induction a0; eauto; induction c; eauto; right; split; eauto;
split; eauto;
exists (Bool n); split; eauto; unfold not; intros; inversion H.
Qed.


Theorem projectMapLibeq : forall dst src id id0 lib plib,
  StringMapModule.get id (projectMapLib src dst) = Some lib ->
  StringMapModule.get id dst = Some plib -> 
  (
    ( 
      StringMapModule.get id0 lib = StringMapModule.get id0 plib
    ) 
    \/ 
    (
      exists slib, StringMapModule.get id src = Some slib /\
      StringMapModule.get id0 lib = StringMapModule.get id0 slib /\ 
      (exists v, StringMapModule.get id0 plib = Some (Environment.EVar (VariableValue v))) /\
      (exists v, StringMapModule.get id0 slib = Some (Environment.EVar (VariableValue v)))
    )
  ).
Proof.
induction dst; simpl; intros; try einversion.
induction a.
simpl in H.
case_eq (String_Equality.eq id a); intros; rewrite H1 in *; eauto.
rewrite String_Equality.eq_spec_true in H1; subst.
inversion H0; subst; eauto.
case_eq (StringMapModule.get a src); intros; rewrite H1 in H; inversion H; eauto.
inversion H; subst.
assert (D := projectMapExternalVareq plib t id0).
destruct D; eauto.
Qed.

Theorem projectMapLibeq' : forall dst src id plib,
  StringMapModule.get id dst = Some plib -> 
  exists lib, 
  (
      StringMapModule.get id (projectMapLib src dst) = Some lib 
  ) /\
  ( forall id0,
    ( 
      StringMapModule.get id0 lib = StringMapModule.get id0 plib
    ) 
    \/ 
    (
      exists slib, StringMapModule.get id src = Some slib /\ 
      StringMapModule.get id0 lib = StringMapModule.get id0 slib /\ 
      (exists v, StringMapModule.get id0 plib = Some (Environment.EVar (VariableValue v))) /\
      (exists v, StringMapModule.get id0 slib = Some (Environment.EVar (VariableValue v)))
    )
  ).
Proof.
intros.
assert (exists lib, StringMapModule.get id (projectMapLib src dst) = Some lib).
assert (D := projectMapLib_dst_some dst src id plib H).
destruct D; eauto.
destruct H0, H0; eauto.
destruct H0.
exists x; split; intros; try eapply projectMapLibeq; eauto.
Qed. *)

Lemma projectMapExternalVar_sim : forall dst src r id,
  StringMapModule.get id src = r -> 
  StringMapModule.get id dst = r -> 
  StringMapModule.get id (projectMapExternalVar src dst) = r.
Proof.
induction dst; simpl; eauto.
induction a.
induction b; try induction c; simpl; eauto.
intros; induction (String_Equality.eq id a); eauto.
intros; case_eq (String_Equality.eq id a); intros; rewrite H1 in *; eauto.
rewrite String_Equality.eq_spec_true in H1; subst.
rewrite <- H0; eauto.
intros; induction (String_Equality.eq id a); eauto.
Qed.

Lemma proj_right : forall e e' e0 e0' i,
      ( 
       StringMapModule.get i e = StringMapModule.get i e' \/
       StringMapModule.get i e = Some (Environment.EVar (VariableValue Undefined)) /\
       (StringMapModule.get i e' = None \/
        (exists v : value, StringMapModule.get i e' = Some (Environment.EVar (VariableValue v)))) \/
       StringMapModule.get i e = Some (Environment.EVar (ConstantValue Undefined)) /\
       (exists v : value, StringMapModule.get i e' = Some (Environment.EVar (ConstantValue v)))
      ) -> 
      ( 
       StringMapModule.get i e0 = StringMapModule.get i e0' \/
       StringMapModule.get i e0 = Some (Environment.EVar (VariableValue Undefined)) /\
       (StringMapModule.get i e0' = None \/
        (exists v : value, StringMapModule.get i e0' = Some (Environment.EVar (VariableValue v)))) \/
       StringMapModule.get i e0 = Some (Environment.EVar (ConstantValue Undefined)) /\
       (exists v : value, StringMapModule.get i e0' = Some (Environment.EVar (ConstantValue v)))
      ) -> StringMapModule.get i (projectMapExternalVarC e0 e) = StringMapModule.get i e 
        -> 
      ( 
       StringMapModule.get i e = StringMapModule.get i (projectMapExternalVar e0' e') \/
       StringMapModule.get i e = Some (Environment.EVar (VariableValue Undefined)) /\
       (StringMapModule.get i (projectMapExternalVar e0' e') = None \/
        (exists v : value, StringMapModule.get i (projectMapExternalVar e0' e') = Some (Environment.EVar (VariableValue v)))) \/
       StringMapModule.get i e = Some (Environment.EVar (ConstantValue Undefined)) /\
       (exists v : value, StringMapModule.get i (projectMapExternalVar e0' e') = Some (Environment.EVar (ConstantValue v)))
      ).
Proof.
induction e; simpl; intros.
destruct H.
apply eq_sym in H.
left.
apply eq_sym.
eapply projectMapExternalVar_dst_none; eauto.
destruct H.
destruct H.
inversion H.
destruct H.
inversion H.
induction a.
induction b.
induction c; simpl in *.
case_eq (String_Equality.eq i a); intros; rewrite H2 in *; eauto.
rewrite String_Equality.eq_spec_true in H2; subst.
destruct H.
apply eq_sym in H.
left.
apply eq_sym.
eapply projectMapExternalVar_dst_sc; eauto.
destruct H.
destruct H.
inversion H.
destruct H.
inversion H; subst; eauto.
right.
right.
split; eauto.
destruct H2.
assert (D := projectMapExternalVar_dst_sc e' e0' a x H2).
eauto.

induction v; simpl in *.
case_eq (String_Equality.eq i a); intros; rewrite H2 in *; eauto.
rewrite String_Equality.eq_spec_true in H2; subst.
destruct H.
apply eq_sym in H.
right; left; split; eauto; right; eapply projectMapExternalVar_dst_sv; eauto.
destruct H.
destruct H.
right; left; split; eauto.
destruct H2.
left.
eapply projectMapExternalVar_dst_none; eauto.
destruct H2.
right.
eapply projectMapExternalVar_dst_sv; eauto.
destruct H; inversion H.

case_eq (String_Equality.eq i a); intros; rewrite H2 in *; eauto.
rewrite String_Equality.eq_spec_true in H2; subst.
assert (StringMapModule.get a e' = Some (Environment.EVar (VariableValue (Int n))))
by (destruct H; [rewrite H; eauto | destruct H; destruct H; inversion H]).
case_eq (StringMapModule.get a e0); intros; rewrite H3 in H1.
induction e1.
induction c.
rewrite H3 in H0.
assert (exists v, StringMapModule.get a e0' = Some (Environment.EVar (ConstantValue v)))
by ( destruct H0; [eauto|destruct H0, H0; eauto; inversion H0]).
destruct H4.
left.
erewrite projectMapExternalVar_src_sc; eauto.
inversion H1; subst.
rewrite H3 in *.
assert (StringMapModule.get a e0' = Some (Environment.EVar (VariableValue (Int n))))
by (destruct H0; [eauto|destruct H0, H0; eauto; inversion H0]).
left; apply eq_sym; eapply projectMapExternalVar_sim; eauto.
assert (StringMapModule.get a e0' = Some (EFDecl l))
by (destruct H0; rewrite H3 in *; eauto; destruct H0, H0; eauto; inversion H0).
left; apply eq_sym; rewrite <- H2; eapply projectMapExternalVar_src_f; eauto.
assert (StringMapModule.get a e0' = None)
by (destruct H0; rewrite H3 in *; eauto; destruct H0, H0; eauto; inversion H0).
left; apply eq_sym; rewrite <- H2; eapply projectMapExternalVar_src_none; eauto.

case_eq (String_Equality.eq i a); intros; rewrite H2 in *; eauto.
rewrite String_Equality.eq_spec_true in H2; subst.
assert (StringMapModule.get a e' = Some (Environment.EVar (VariableValue (Bool n))))
by (destruct H; [rewrite H; eauto | destruct H; destruct H; inversion H]).
case_eq (StringMapModule.get a e0); intros; rewrite H3 in H1.
induction e1.
induction c.
rewrite H3 in H0.
assert (exists v, StringMapModule.get a e0' = Some (Environment.EVar (ConstantValue v)))
by ( destruct H0; [eauto|destruct H0, H0; eauto; inversion H0]).
destruct H4.
left.
erewrite projectMapExternalVar_src_sc; eauto.
inversion H1; subst.
rewrite H3 in *.
assert (StringMapModule.get a e0' = Some (Environment.EVar (VariableValue (Bool n))))
by (destruct H0; [eauto|destruct H0, H0; eauto; inversion H0]).
left; apply eq_sym; eapply projectMapExternalVar_sim; eauto.
assert (StringMapModule.get a e0' = Some (EFDecl l))
by (destruct H0; rewrite H3 in *; eauto; destruct H0, H0; eauto; inversion H0).
left; apply eq_sym; rewrite <- H2; eapply projectMapExternalVar_src_f; eauto.
assert (StringMapModule.get a e0' = None)
by (destruct H0; rewrite H3 in *; eauto; destruct H0, H0; eauto; inversion H0).
left; apply eq_sym; rewrite <- H2; eapply projectMapExternalVar_src_none; eauto.

case_eq (String_Equality.eq i a); intros; rewrite H2 in *; eauto.
rewrite String_Equality.eq_spec_true in H2; subst.
assert (StringMapModule.get a e' = Some (Environment.EVar (VariableValue (STR n))))
by (destruct H; [rewrite H; eauto | destruct H; destruct H; inversion H]).
case_eq (StringMapModule.get a e0); intros; rewrite H3 in H1.
induction e1.
induction c.
rewrite H3 in H0.
assert (exists v, StringMapModule.get a e0' = Some (Environment.EVar (ConstantValue v)))
by ( destruct H0; [eauto|destruct H0, H0; eauto; inversion H0]).
destruct H4.
left.
erewrite projectMapExternalVar_src_sc; eauto.
inversion H1; subst.
rewrite H3 in *.
assert (StringMapModule.get a e0' = Some (Environment.EVar (VariableValue (STR n))))
by (destruct H0; [eauto|destruct H0, H0; eauto; inversion H0]).
left; apply eq_sym; eapply projectMapExternalVar_sim; eauto.
assert (StringMapModule.get a e0' = Some (EFDecl l))
by (destruct H0; rewrite H3 in *; eauto; destruct H0, H0; eauto; inversion H0).
left; apply eq_sym; rewrite <- H2; eapply projectMapExternalVar_src_f; eauto.
assert (StringMapModule.get a e0' = None)
by (destruct H0; rewrite H3 in *; eauto; destruct H0, H0; eauto; inversion H0).
left; apply eq_sym; rewrite <- H2; eapply projectMapExternalVar_src_none; eauto.

case_eq (String_Equality.eq i a); intros; rewrite H2 in *; eauto.
rewrite String_Equality.eq_spec_true in H2; subst.
assert (StringMapModule.get a e' = Some (Environment.EVar (VariableValue (ArrayV t v))))
by (destruct H; [rewrite H; eauto | destruct H; destruct H; inversion H]).
case_eq (StringMapModule.get a e0); intros; rewrite H3 in H1.
induction e1.
induction c.
rewrite H3 in H0.
assert (exists v, StringMapModule.get a e0' = Some (Environment.EVar (ConstantValue v)))
by ( destruct H0; [eauto|destruct H0, H0; eauto; inversion H0]).
destruct H4.
left.
erewrite projectMapExternalVar_src_sc; eauto.
inversion H1; subst.
rewrite H3 in *.
assert (StringMapModule.get a e0' = Some (Environment.EVar (VariableValue (ArrayV t v))))
by (destruct H0; [eauto|destruct H0, H0; eauto; inversion H0]).
left; apply eq_sym; eapply projectMapExternalVar_sim; eauto.
assert (StringMapModule.get a e0' = Some (EFDecl l))
by (destruct H0; rewrite H3 in *; eauto; destruct H0, H0; eauto; inversion H0).
left; apply eq_sym; rewrite <- H2; eapply projectMapExternalVar_src_f; eauto.
assert (StringMapModule.get a e0' = None)
by (destruct H0; rewrite H3 in *; eauto; destruct H0, H0; eauto; inversion H0).
left; apply eq_sym; rewrite <- H2; eapply projectMapExternalVar_src_none; eauto.

case_eq (String_Equality.eq i a); intros; rewrite H2 in *; eauto.
rewrite String_Equality.eq_spec_true in H2; subst.
assert (StringMapModule.get a e' = Some (Environment.EVar (VariableValue (RecordV r))))
by (destruct H; [rewrite H; eauto | destruct H; destruct H; inversion H]).
case_eq (StringMapModule.get a e0); intros; rewrite H3 in H1.
induction e1.
induction c.
rewrite H3 in H0.
assert (exists v, StringMapModule.get a e0' = Some (Environment.EVar (ConstantValue v)))
by ( destruct H0; [eauto|destruct H0, H0; eauto; inversion H0]).
destruct H4.
left.
erewrite projectMapExternalVar_src_sc; eauto.
inversion H1; subst.
rewrite H3 in *.
assert (StringMapModule.get a e0' = Some (Environment.EVar (VariableValue (RecordV r))))
by (destruct H0; [eauto|destruct H0, H0; eauto; inversion H0]).
left; apply eq_sym; eapply projectMapExternalVar_sim; eauto.
assert (StringMapModule.get a e0' = Some (EFDecl l))
by (destruct H0; rewrite H3 in *; eauto; destruct H0, H0; eauto; inversion H0).
left; apply eq_sym; rewrite <- H2; eapply projectMapExternalVar_src_f; eauto.
assert (StringMapModule.get a e0' = None)
by (destruct H0; rewrite H3 in *; eauto; destruct H0, H0; eauto; inversion H0).
left; apply eq_sym; rewrite <- H2; eapply projectMapExternalVar_src_none; eauto.

simpl in *.
case_eq (String_Equality.eq i a); intros; rewrite H2 in *; eauto.
rewrite String_Equality.eq_spec_true in H2; subst.
left.
apply eq_sym.
assert (StringMapModule.get a e' = Some (EFDecl l))
by (destruct H; [eauto|destruct H, H; eauto; inversion H]).
eapply projectMapExternalVar_dst_f; eauto.
Qed.

Lemma proj_left : forall e e' e0 e0' i,
      ( 
       StringMapModule.get i e = StringMapModule.get i e' \/
       StringMapModule.get i e = Some (Environment.EVar (VariableValue Undefined)) /\
       (StringMapModule.get i e' = None \/
        (exists v : value, StringMapModule.get i e' = Some (Environment.EVar (VariableValue v)))) \/
       StringMapModule.get i e = Some (Environment.EVar (ConstantValue Undefined)) /\
       (exists v : value, StringMapModule.get i e' = Some (Environment.EVar (ConstantValue v)))
      ) -> 
      ( 
       StringMapModule.get i e0 = StringMapModule.get i e0' \/
       StringMapModule.get i e0 = Some (Environment.EVar (VariableValue Undefined)) /\
       (StringMapModule.get i e0' = None \/
        (exists v : value, StringMapModule.get i e0' = Some (Environment.EVar (VariableValue v)))) \/
       StringMapModule.get i e0 = Some (Environment.EVar (ConstantValue Undefined)) /\
       (exists v : value, StringMapModule.get i e0' = Some (Environment.EVar (ConstantValue v)))
      ) -> StringMapModule.get i (projectMapExternalVarC e0 e) = StringMapModule.get i e0 
        -> 
      ( 
       StringMapModule.get i e0 = StringMapModule.get i (projectMapExternalVar e0' e') \/
       StringMapModule.get i e0 = Some (Environment.EVar (VariableValue Undefined)) /\
       (StringMapModule.get i (projectMapExternalVar e0' e') = None \/
        (exists v : value, StringMapModule.get i (projectMapExternalVar e0' e') = Some (Environment.EVar (VariableValue v)))) \/
       StringMapModule.get i e0 = Some (Environment.EVar (ConstantValue Undefined)) /\
       (exists v : value, StringMapModule.get i (projectMapExternalVar e0' e') = Some (Environment.EVar (ConstantValue v)))
      ).
Proof.
induction e; simpl; intros.
rewrite <- H1.
assert (StringMapModule.get i e' = None) by (
rewrite <- H1 in *; destruct H; eauto; destruct H, H; inversion H).
left.
apply eq_sym.
eapply projectMapExternalVar_dst_none; eauto.
induction a.
induction b.
induction c; simpl in *.
case_eq (String_Equality.eq i a); intros; rewrite H2 in *; eauto.
rewrite String_Equality.eq_spec_true in H2; subst.
destruct H.
apply eq_sym in H.
left.
apply eq_sym.
rewrite <- H1.
eapply projectMapExternalVar_dst_sc; eauto.
destruct H.
destruct H.
inversion H.
destruct H.
inversion H; subst; eauto.
rewrite <- H1.
right.
right.
split; eauto.
destruct H2.
assert (D := projectMapExternalVar_dst_sc e' e0' a x H2).
eauto.

induction v; simpl in *.
case_eq (String_Equality.eq i a); intros; rewrite H2 in *; eauto.
rewrite String_Equality.eq_spec_true in H2; subst.
destruct H.
apply eq_sym in H.
right; left; split; eauto; right; eapply projectMapExternalVar_dst_sv; eauto.
destruct H.
destruct H.
right; left; split; eauto.
destruct H2.
left.
eapply projectMapExternalVar_dst_none; eauto.
destruct H2.
right.
eapply projectMapExternalVar_dst_sv; eauto.
destruct H; inversion H.

case_eq (String_Equality.eq i a); intros; rewrite H2 in *; eauto.
rewrite String_Equality.eq_spec_true in H2; subst.
assert (StringMapModule.get a e' = Some (Environment.EVar (VariableValue (Int n))))
by (destruct H; [rewrite H; eauto | destruct H; destruct H; inversion H]).
case_eq (StringMapModule.get a e0); intros; rewrite H3 in H1.
induction e1.
induction c.
inversion H1.
rewrite H3 in H0.
destruct H0.
apply eq_sym in H0.
left.
apply eq_sym.
eapply projectMapExternalVar_sv_sv; eauto.
destruct H0, H0; inversion H0; subst.
destruct H4.
right; left; split; eauto; right; eapply projectMapExternalVar_dst_sv; eauto.
right; left; split; eauto; right; eapply projectMapExternalVar_dst_sv; eauto.
inversion H1.
inversion H1.

case_eq (String_Equality.eq i a); intros; rewrite H2 in *; eauto.
rewrite String_Equality.eq_spec_true in H2; subst.
assert (StringMapModule.get a e' = Some (Environment.EVar (VariableValue (Bool n))))
by (destruct H; [rewrite H; eauto | destruct H; destruct H; inversion H]).
case_eq (StringMapModule.get a e0); intros; rewrite H3 in H1.
induction e1.
induction c.
inversion H1.
rewrite H3 in H0.
destruct H0.
apply eq_sym in H0.
left.
apply eq_sym.
eapply projectMapExternalVar_sv_sv; eauto.
destruct H0, H0; inversion H0; subst.
destruct H4.
right; left; split; eauto; right; eapply projectMapExternalVar_dst_sv; eauto.
right; left; split; eauto; right; eapply projectMapExternalVar_dst_sv; eauto.
inversion H1.
inversion H1.

case_eq (String_Equality.eq i a); intros; rewrite H2 in *; eauto.
rewrite String_Equality.eq_spec_true in H2; subst.
assert (StringMapModule.get a e' = Some (Environment.EVar (VariableValue (STR n))))
by (destruct H; [rewrite H; eauto | destruct H; destruct H; inversion H]).
case_eq (StringMapModule.get a e0); intros; rewrite H3 in H1.
induction e1.
induction c.
inversion H1.
rewrite H3 in H0.
destruct H0.
apply eq_sym in H0.
left.
apply eq_sym.
eapply projectMapExternalVar_sv_sv; eauto.
destruct H0, H0; inversion H0; subst.
destruct H4.
right; left; split; eauto; right; eapply projectMapExternalVar_dst_sv; eauto.
right; left; split; eauto; right; eapply projectMapExternalVar_dst_sv; eauto.
inversion H1.
inversion H1.

case_eq (String_Equality.eq i a); intros; rewrite H2 in *; eauto.
rewrite String_Equality.eq_spec_true in H2; subst.
assert (StringMapModule.get a e' = Some (Environment.EVar (VariableValue (ArrayV t v))))
by (destruct H; [rewrite H; eauto | destruct H; destruct H; inversion H]).
case_eq (StringMapModule.get a e0); intros; rewrite H3 in H1.
induction e1.
induction c.
inversion H1.
rewrite H3 in H0.
destruct H0.
apply eq_sym in H0.
left.
apply eq_sym.
eapply projectMapExternalVar_sv_sv; eauto.
destruct H0, H0; inversion H0; subst.
destruct H4.
right; left; split; eauto; right; eapply projectMapExternalVar_dst_sv; eauto.
right; left; split; eauto; right; eapply projectMapExternalVar_dst_sv; eauto.
inversion H1.
inversion H1.

case_eq (String_Equality.eq i a); intros; rewrite H2 in *; eauto.
rewrite String_Equality.eq_spec_true in H2; subst.
assert (StringMapModule.get a e' = Some (Environment.EVar (VariableValue (RecordV r))))
by (destruct H; [rewrite H; eauto | destruct H; destruct H; inversion H]).
case_eq (StringMapModule.get a e0); intros; rewrite H3 in H1.
induction e1.
induction c.
inversion H1.
rewrite H3 in H0.
destruct H0.
apply eq_sym in H0.
left.
apply eq_sym.
eapply projectMapExternalVar_sv_sv; eauto.
destruct H0, H0; inversion H0; subst.
destruct H4.
right; left; split; eauto; right; eapply projectMapExternalVar_dst_sv; eauto.
right; left; split; eauto; right; eapply projectMapExternalVar_dst_sv; eauto.
inversion H1.
inversion H1.

simpl in *.
case_eq (String_Equality.eq i a); intros; rewrite H2 in *; eauto.
rewrite String_Equality.eq_spec_true in H2; subst.
left.
apply eq_sym.
assert (StringMapModule.get a e' = Some (EFDecl l))
by (destruct H; [eauto|destruct H, H; eauto; inversion H]).
rewrite <- H1.
eapply projectMapExternalVar_dst_f; eauto.
Qed.

Lemma proj_inf : forall e e' e0 e0' i,
      ( 
       StringMapModule.get i e = StringMapModule.get i e' \/
       StringMapModule.get i e = Some (Environment.EVar (VariableValue Undefined)) /\
       (StringMapModule.get i e' = None \/
        (exists v : value, StringMapModule.get i e' = Some (Environment.EVar (VariableValue v)))) \/
       StringMapModule.get i e = Some (Environment.EVar (ConstantValue Undefined)) /\
       (exists v : value, StringMapModule.get i e' = Some (Environment.EVar (ConstantValue v)))
      ) -> 
      ( 
       StringMapModule.get i e0 = StringMapModule.get i e0' \/
       StringMapModule.get i e0 = Some (Environment.EVar (VariableValue Undefined)) /\
       (StringMapModule.get i e0' = None \/
        (exists v : value, StringMapModule.get i e0' = Some (Environment.EVar (VariableValue v)))) \/
       StringMapModule.get i e0 = Some (Environment.EVar (ConstantValue Undefined)) /\
       (exists v : value, StringMapModule.get i e0' = Some (Environment.EVar (ConstantValue v)))
      ) -> 
      ( 
       StringMapModule.get i (projectMapExternalVarC e0 e) = StringMapModule.get i (projectMapExternalVar e0' e') \/
       StringMapModule.get i (projectMapExternalVarC e0 e) = Some (Environment.EVar (VariableValue Undefined)) /\
       (StringMapModule.get i (projectMapExternalVar e0' e') = None \/
        (exists v : value, StringMapModule.get i (projectMapExternalVar e0' e') = Some (Environment.EVar (VariableValue v)))) \/
       StringMapModule.get i (projectMapExternalVarC e0 e) = Some (Environment.EVar (ConstantValue Undefined)) /\
       (exists v : value, StringMapModule.get i (projectMapExternalVar e0' e') = Some (Environment.EVar (ConstantValue v)))
      ).
Proof.
intros.
assert (D := projectExtCVOr i e0 e).
destruct D; rewrite H1.
eapply proj_right; eauto.
eapply proj_left; eauto.
Qed.

Lemma projectMapLib_src_dst_some : forall dst src id e e',
  StringMapModule.get id dst = Some e ->
  StringMapModule.get id src = Some e' ->
  StringMapModule.get id (projectMapLib src dst) = Some (projectMapExternalVar e' e)
  .
Proof.
induction dst; simpl; eauto.
intros.
inversion H.
induction a.
simpl.
intros src id.
case_eq (String_Equality.eq id a); eauto.
intros.
rewrite String_Equality.eq_spec_true in H; subst.
inversion H0; subst; rewrite H1; eauto.
Qed.

Lemma projectMapLibC_src_dst_some : forall dst src id e e',
  StringMapModule.get id dst = Some e ->
  StringMapModule.get id src = Some e' ->
  StringMapModule.get id (projectMapLibC src dst) = Some (projectMapExternalVarC e' e)
  .
Proof.
induction dst; simpl; eauto.
intros.
inversion H.
induction a.
simpl.
intros src id.
case_eq (String_Equality.eq id a); eauto.
intros.
rewrite String_Equality.eq_spec_true in H; subst.
inversion H0; subst; rewrite H1; eauto.
Qed.

Lemma projectMapLibC_src_none: forall dst src id,
  StringMapModule.get id src = None ->
  StringMapModule.get id (projectMapLibC src dst) = StringMapModule.get id dst.
Proof.
induction dst; auto; simpl.
induction a; simpl.
intros.
case_eq (String_Equality.eq id a); intros; auto.
rewrite String_Equality.eq_spec_true in H0; subst.
rewrite H; eauto.
Qed.

Lemma projectMapLib_src_none: forall dst src id,
  StringMapModule.get id src = None ->
  StringMapModule.get id (projectMapLib src dst) = StringMapModule.get id dst.
Proof.
induction dst; auto; simpl.
induction a; simpl.
intros.
case_eq (String_Equality.eq id a); intros; auto.
rewrite String_Equality.eq_spec_true in H0; subst.
rewrite H; eauto.
Qed.

Lemma projectMapExternalVarC_src_none : forall dst src id,
  StringMapModule.get id src = None ->
  StringMapModule.get id (projectMapExternalVarC src dst) = StringMapModule.get id dst.
Proof.
induction dst; simpl; eauto.
induction a.
induction b; eauto.
induction c; simpl; eauto.
intros.
case_eq (String_Equality.eq id a); intros; auto.
induction v; simpl; intros; case_eq (String_Equality.eq id a); intros; auto;
rewrite String_Equality.eq_spec_true in H0; subst; rewrite H; auto.
simpl.
intros.
case_eq (String_Equality.eq id a); intros; auto.
Qed.

Lemma projectMapExternalVarC_some_undef: forall dst src i, 
    StringMapModule.get i (projectMapExternalVarC src dst) =
     Some (Environment.EVar (VariableValue Undefined)) -> 
    StringMapModule.get i dst = Some (Environment.EVar (VariableValue Undefined)) \/
    (StringMapModule.get i src = Some (Environment.EVar (VariableValue Undefined)) /\ (exists v,
    StringMapModule.get i dst = Some (Environment.EVar (VariableValue v)) /\ v <> Undefined)
    ).
Proof.
induction dst; simpl; eauto.
induction a.
induction b; eauto; simpl.
induction c; simpl; eauto.
intros.
case_eq (String_Equality.eq i a); intros; rewrite H0 in *; eauto.
induction v; simpl; intros src i; case_eq (String_Equality.eq i a); intros; eauto;
rewrite String_Equality.eq_spec_true in H; subst; case_eq (StringMapModule.get a src); intros; rewrite H in *; 
try induction e; eauto; try induction c; eauto; inversion H0.
right; split; eauto; unfold not; exists (Int n); split; eauto; intros; inversion H1.
right; split; eauto; unfold not; exists (Bool n); split; eauto; intros; inversion H1.
right; split; eauto; unfold not; exists (STR n); split; eauto; intros; inversion H1.
right; split; eauto; unfold not; exists (ArrayV t v); split; eauto; intros; inversion H1.
right; split; eauto; unfold not; exists (RecordV r); split; eauto; intros; inversion H1.
intros src i; case_eq (String_Equality.eq i a); intros; eauto.
Qed.

Lemma inf_v_not_undef : forall e e' i v,
      ( 
       StringMapModule.get i e = StringMapModule.get i e' \/
       StringMapModule.get i e = Some (Environment.EVar (VariableValue Undefined)) /\
       (StringMapModule.get i e' = None \/
        (exists v : value, StringMapModule.get i e' = Some (Environment.EVar (VariableValue v)))) \/
       StringMapModule.get i e = Some (Environment.EVar (ConstantValue Undefined)) /\
       (exists v : value, StringMapModule.get i e' = Some (Environment.EVar (ConstantValue v)))
      ) -> StringMapModule.get i e = Some (Environment.EVar (VariableValue v)) -> v <> Undefined ->
      StringMapModule.get i e = StringMapModule.get i e'.
Proof.
unfold not.
intros.
destruct H; eauto.
destruct H, H; rewrite H in H0; inversion H0.
apply eq_sym in H4.
apply H1 in H4; inversion H4.
Qed.

Lemma inf_c_not_undef : forall e e' i v,
      ( 
       StringMapModule.get i e = StringMapModule.get i e' \/
       StringMapModule.get i e = Some (Environment.EVar (VariableValue Undefined)) /\
       (StringMapModule.get i e' = None \/
        (exists v : value, StringMapModule.get i e' = Some (Environment.EVar (VariableValue v)))) \/
       StringMapModule.get i e = Some (Environment.EVar (ConstantValue Undefined)) /\
       (exists v : value, StringMapModule.get i e' = Some (Environment.EVar (ConstantValue v)))
      ) -> StringMapModule.get i e = Some (Environment.EVar (ConstantValue v)) -> v <> Undefined ->
      StringMapModule.get i e = StringMapModule.get i e'.
Proof.
unfold not.
intros.
destruct H; eauto.
destruct H, H; rewrite H in H0; inversion H0.
apply eq_sym in H4.
apply H1 in H4; inversion H4.
Qed.

Lemma projectMapExternalVarC_dst_none: forall dst src i,
  StringMapModule.get i dst = None ->
  StringMapModule.get i (projectMapExternalVarC src dst) = None.
Proof.
induction dst; simpl; eauto.
induction a.
induction b; simpl; try induction c; simpl; intros src i; case_eq (String_Equality.eq i a); eauto; intros;
try einversion; eauto.
induction v; simpl; rewrite H; eauto.
Qed.

Lemma projectMapExternalVarC_dst_vundef: forall dst src i,
  StringMapModule.get i dst = Some (Environment.EVar (VariableValue Undefined)) ->
  StringMapModule.get i (projectMapExternalVarC src dst) = Some (Environment.EVar (VariableValue Undefined)).
Proof.
induction dst; simpl; eauto.
induction a.
induction b; simpl; try induction c; simpl; intros src i; case_eq (String_Equality.eq i a); simpl; eauto; intros;
try einversion; inversion H0; subst; simpl; try rewrite H; eauto.
induction v; simpl; rewrite H0; rewrite H; eauto.
Qed.

Lemma projectMapLibC_dst_none: forall dst src i,
  StringMapModule.get i dst = None ->
  StringMapModule.get i (projectMapLibC src dst) = None.
Proof.
induction dst; simpl; eauto.
induction a; simpl.
intros src i.
case_eq (String_Equality.eq i a); intros; [inversion H0|eauto].
Qed.

Lemma partialViewEnvCopyOut : forall sig params lin lout flin fupin upin upout,
  envpartialView lin upin ->
  envpartialView flin fupin ->
  copyOut_f2 flin sig params lin = Some lout ->
  copyOut_f fupin sig params upin = Some upout ->
  envpartialView lout upout.
Proof.
induction sig, params; intros; try einversion.
simpl in *. 
inversion H1; inversion H2; subst; simpl.
destruct H, H0.
destruct H3, H4.
destruct H5, H6.
destruct H7, H8.
repeat (split; eauto).
simpl.
apply mapPartialView_projetGlob; auto.
simpl in *.
clear -H10 H9.
intros.
assert (H9id := H9 id).
assert (H10id := H10 id).
clear H9 H10.
case_eq (StringMapModule.get id (externals lin)); intros; rewrite H in *.
case_eq (StringMapModule.get id (externals upin)); intros; rewrite H0 in *.
case_eq (StringMapModule.get id (externals flin)); intros; rewrite H1 in *.
case_eq (StringMapModule.get id (externals fupin)); intros; rewrite H2 in *.
assert (H3:= projectMapLibC_src_dst_some (externals lin) (externals flin) id t t1 H H1).
rewrite H3.
assert (H4:= projectMapLib_src_dst_some (externals upin) (externals fupin) id t0 t2 H0 H2).
rewrite H4.
intros.
eapply proj_inf; eauto.
assert (H3:= projectMapLibC_src_dst_some (externals lin) (externals flin) id t t1 H H1).
rewrite H3.
erewrite projectMapLib_src_none; eauto.
rewrite H0.
intros.
assert (H10idi := H10id i).
destruct H10idi.
rewrite projectMapExternalVarC_src_none; eauto.
assert (D := projectExtCVOr i t1 t).
destruct D; rewrite H5; eauto.
rewrite H4 in H5.
assert (D := projectMapExternalVarC_some_undef t t1 i H5).
destruct D.
rewrite <- H6 in H4; rewrite H4; eauto.
destruct H6.
destruct H7, H7.
right.
left.
split; eauto.
right.
assert (D := inf_v_not_undef t t0 i x (H9id i) H7 H8).
rewrite <- D, H7; eauto.
case_eq (StringMapModule.get id (externals fupin)); intros; rewrite H2 in H10id; inversion H10id.
rewrite projectMapLibC_src_none; eauto.
rewrite projectMapLib_src_none; eauto.
rewrite H, H0; eauto.
rewrite projectMapLib_dst_none; eauto.
case_eq (StringMapModule.get id (externals flin)); intros; rewrite H1 in *.
assert (H3:= projectMapLibC_src_dst_some (externals lin) (externals flin) id t t0 H H1).
rewrite H3.
intros.
assert (H9idi := H9id i).
destruct H9idi.
rewrite projectMapExternalVarC_dst_none; eauto.
rewrite projectMapExternalVarC_dst_vundef; eauto.
rewrite projectMapLibC_src_none; eauto; rewrite H; eauto.
case_eq (StringMapModule.get id (externals upin)); intros; rewrite H0 in H9id; inversion H9id.
rewrite projectMapLib_dst_none; eauto.
rewrite projectMapLibC_dst_none; eauto.
simpl in *.
inversion H1.
simpl in *.
induction a.
induction a.
induction b0; inversion H1.
simpl in *.
induction a.
induction a.
induction b0; eauto.
induction e; try einversion.
induction e; try einversion.
case_eq (copyOut_f2 flin sig params lin); intros; rewrite H3 in H1; try einversion.
case_eq (copyOut_f fupin sig params upin); intros; rewrite H4 in H2; try einversion.
assert (D := envpartialViewEvalExpression flin fupin (EVar [a]) H0).
destruct D.
simpl in H5.
rewrite H5 in *.
case_eq (match getIdValue a fupin with
       | Some (Value (ConstantValue v)) | Some (Value (VariableValue v)) => Some v
       | None => Some Undefined
       | _ => None
       end); intros; rewrite H6 in *.
assert (envpartialView e e0) by eauto.
induction v; try solve [eapply slimSetIdsValue_correct; eauto]; try eapply envpartialViewForgetVsSet; eauto.
inversion H1.
simpl in H5.
rewrite H5 in H1.
assert (envpartialView e e0) by eauto.
induction (match getIdValue a fupin with
       | Some (Value (ConstantValue v)) | Some (Value (VariableValue v)) => Some v
       | None => Some Undefined
       | _ => None
       end); try solve [inversion H2]; try solve [eapply envpartialViewForgetVsSet; eauto].
induction e; try einversion.
induction e; try einversion.
case_eq (copyOut_f2 flin sig params lin); intros; rewrite H3 in H1; try einversion.
case_eq (copyOut_f fupin sig params upin); intros; rewrite H4 in H2; try einversion.
assert (D := envpartialViewEvalExpression flin fupin (EVar [a]) H0).
destruct D.
simpl in H5.
rewrite H5 in *.
case_eq (match getIdValue a fupin with
       | Some (Value (ConstantValue v)) | Some (Value (VariableValue v)) => Some v
       | None => Some Undefined
       | _ => None
       end); intros; rewrite H6 in *.
assert (envpartialView e e0) by eauto.
induction v; try solve [eapply slimSetIdsValue_correct; eauto]; try eapply envpartialViewForgetVsSet; eauto.
inversion H1.
simpl in H5.
rewrite H5 in H1.
assert (envpartialView e e0) by eauto.
induction (match getIdValue a fupin with
       | Some (Value (ConstantValue v)) | Some (Value (VariableValue v)) => Some v
       | None => Some Undefined
       | _ => None
       end); try solve [inversion H2]; try solve [eapply envpartialViewForgetVsSet; eauto].
Qed.




Fixpoint contextRuleInstruction n env f := match n with
| 0 => slimPartialViewevalInstruction env f
| S n => 
  ( 
    match f with
    | ICallDefinedFunction f_name None =>
      (
        match getIdsValue f_name env with
        | Some (DefinedFunction f0) => 
          (
            match copyIn_f env f0.(f_args) [] with
            | Some env0 =>
              (
                match initLocals_f env0 f0.(f_local_decs) with
                | Some env1 => 
                  (
                    match contextRuleInstruction_s n env1 f0.(f_instrs) with
                    | Some env2 => copyOut_f2 env2 f0.(f_args) [] env 
                    | _ => slimPartialViewevalInstruction env f
                    end
                  )
                | _ => slimPartialViewevalInstruction env f
                end
              )
            | _ => slimPartialViewevalInstruction env f
            end
          )
        | _ => slimPartialViewevalInstruction env f
        end
      )
    | ICallDefinedFunction f_name (Some f_params) =>
      (
        match getIdsValue f_name env with
        | Some (DefinedFunction f0) => 
          (
            match copyIn_f env f0.(f_args) f_params with
            | Some env0 =>
              (
                match initLocals_f env0 f0.(f_local_decs) with
                | Some env1 => 
                  (
                    match contextRuleInstruction_s n env1 f0.(f_instrs) with
                    | Some env2 => copyOut_f2 env2 f0.(f_args) f_params env 
                    | _ => slimPartialViewevalInstruction env f
                    end
                  )
                | _ => slimPartialViewevalInstruction env f
                end
              )
            | _ => slimPartialViewevalInstruction env f
            end
          )
        | _ => slimPartialViewevalInstruction env f
        end
      )
    | (IIf cond ins_if ins_else) =>
      (
        match evalSec_opel_expression_if_f env cond with
        | Some (Bool true) => contextRuleInstruction_s n env ins_if
        | Some (Bool false) => contextRuleElif_instruction n env ins_else
        | _ => slimPartialViewevalInstruction env f
        end
      )
   | (ICase var case) => contextRuleCase_instruction n env var case
   | ILoop cond ins => 
      (
        match evalSec_opel_w_f env cond with
        | Some (Bool false) => Some env
        | Some (Bool true) => 
          (
            match contextRuleInstruction_s n env ins with
            | Some env0 => contextRuleInstruction n env0 (ILoop cond ins)
            | _ => slimPartialViewevalInstruction env f
            end
          )
        | _ => slimPartialViewevalInstruction env f
        end
      )
    | _ => slimPartialViewevalInstruction env f
    end
 )
 end 
with contextRuleElif_instruction fuel env f := match fuel with
| 0 => slimPartialViewevalElif_instruction env f
| S n => 
  (
    match f with
    | IElse i => contextRuleInstruction_s n env i
    | IElif cond ins_true ins_false => 
        (
          match evalSec_opel_expression_if_f env cond with
          | Some (Bool true) => contextRuleInstruction_s n env ins_true
          | Some (Bool false) => contextRuleElif_instruction n env ins_false
          | _ => slimPartialViewevalElif_instruction env f
          end
        )
    end
  )
end    
with contextRuleCase_instruction fuel env e f := match fuel with
| 0 => slimPartialViewevalCase_instruction env e f
| S n =>
  (
    match f with
    | ICaseFinishDefault ins_s => contextRuleInstruction_s n env ins_s
    | ICaseCons (exp, ins_s) case_t => 
        (
          match evalExpression_f env (Ebinary Equal (Ast.EVar e) exp) with
          | Some (Bool true) => contextRuleInstruction_s n env ins_s
          | Some (Bool false) => contextRuleCase_instruction n env e case_t
          | _ => slimPartialViewevalCase_instruction env e f
          end
        )
    end
  )
end
with contextRuleInstruction_s fuel env f := match fuel with
| 0 => slimPartialViewevalInstruction_s env f
| S n =>
  (
    match f with
    | ISeqEnd i => contextRuleInstruction n env i
    | ISeqCons h t => 
        (
          match contextRuleInstruction n env h with
          | Some e' => contextRuleInstruction_s n e' t
          | None => slimPartialViewevalInstruction_s env f
          end
        )
    end
  )
end.

Theorem contextRuleCorrect:
  (forall in_data f res,
  evalInstruction in_data f res ->
  forall n e slim_e, extractErrorType res = None -> 
  envpartialView e (fst in_data) ->
  contextRuleInstruction n e f = Some slim_e ->
  envpartialView slim_e (extractEnv res)) /\ 
  (forall in_data f res,
  evalInstruction_s in_data f res ->
  forall n e slim_e, extractErrorType res = None -> 
  envpartialView e (fst in_data) ->
  contextRuleInstruction_s n e f = Some slim_e ->
  envpartialView slim_e (extractEnv res)) /\ 
  (forall in_data f res,
  evalElif_instruction in_data f res ->
  forall n e slim_e, extractErrorType res = None -> 
  envpartialView e (fst in_data) ->
  contextRuleElif_instruction n e f = Some slim_e ->
  envpartialView slim_e (extractEnv res)) /\ 
  (forall in_data exp f res,
  evalCase_instruction in_data exp f res ->
  forall n e slim_e, extractErrorType res = None -> 
  envpartialView e (fst in_data) ->
  contextRuleCase_instruction n e exp f = Some slim_e ->
  envpartialView slim_e (extractEnv res)).
Proof.
apply sem_inst_mutind; induction n; intros; unfold contextRuleInstruction in H1; try einversion; 
try solve [inversion H1; subst; eauto]; 
try solve [eapply slimPartialViewevalInstruction_correct; eauto; econstructor; eauto].
assert (D := envpartialViewEvalSec_expression_if e1 env evn (Bool true) cond H1).
destruct D.
assert (D := H3 e).
simpl in H2.
rewrite D in *.
eauto.
unfold contextRuleInstruction in H2.
rewrite H3 in H2.
try solve [eapply slimPartialViewevalInstruction_correct; eauto; econstructor; eauto].
assert (D := envpartialViewEvalSec_expression_if e1 env evn (Bool false) cond H1).
destruct D.
assert (D := H3 e).
simpl in H2.
rewrite D in *.
eauto.
unfold contextRuleInstruction in H2.
rewrite H3 in H2.
try solve [eapply slimPartialViewevalInstruction_correct; eauto; econstructor; eauto].
assert (evalInstruction (env, evn) (ILoop cond ins) res) by (econstructor; eauto).
unfold contextRuleInstruction in H3.
eapply slimPartialViewevalInstruction_correct; eauto.
apply evalSec_opel_w_f_correct in e.
assert (D := envpartialViewEvalSec_opel_w e2 env cond H2).
destruct D.
rewrite e in H4.
case_eq (contextRuleInstruction_s n e2 ins); intros.
simpl in H3.
rewrite H4 in H3.
rewrite H5 in H3.
eauto.
assert (evalInstruction (env, evn) (ILoop cond ins) res) by
(eapply evalInstructionILoopTrueOk; eauto; eapply evalSec_opel_w_f_correct; eauto).
unfold contextRuleInstruction in H3.
rewrite H4 in H3.
unfold contextRuleInstruction_s in H5.
rewrite H5 in H3.
eapply slimPartialViewevalInstruction_correct; eauto.
assert (evalInstruction (env, evn) (ILoop cond ins) res) by
(eapply evalInstructionILoopTrueOk; eauto; eapply evalSec_opel_w_f_correct; eauto).
unfold contextRuleInstruction in H3.
rewrite H4 in H3.
eapply slimPartialViewevalInstruction_correct; eauto.
apply evalSec_opel_w_f_correct in e.
assert (D := envpartialViewEvalSec_opel_w e0 env cond H0).
destruct D.
rewrite e in H2.
rewrite H2 in H1.
inversion H1; subst; eauto.
rewrite H2 in H1.
eapply slimPartialViewevalInstruction_correct; eauto.
eapply evalInstructionILoopFalse; eauto; eapply evalSec_opel_w_f_correct; eauto.
assert (D := envpartialViewGetIdsSome f_name e1 b_env H1).
destruct D.
rewrite e in H3.
case_eq (copyIn_f e1 (f_args f) []); intros.
case_eq (initLocals_f e2 (f_local_decs f)); intros.
case_eq (contextRuleInstruction_s n e3 (f_instrs f)); intros.
case_eq (copyOut_f2 e4 (f_args f) [] e1); intros.
simpl in H2.
rewrite H3, H4, H5, H6, H7 in H2; inversion H2; subst.
assert (envpartialView e2 f_env) by (eapply partialViewEnvCopyIn; eauto; eapply copyIn_f_correct; eauto).
assert (envpartialView e3 f_env') by (eapply partialViewEnvInitLocals; eauto; eapply initLocals_f_correct; eauto).
assert (envpartialView e4 af_env) by (eauto).
apply copyOut_f_correct in c0.
simpl.
simpl in H1.
apply (partialViewEnvCopyOut (f_args f) [] e1 slim_e e4 af_env b_env a_env); eauto.
unfold contextRuleInstruction in H2.
unfold contextRuleInstruction_s in H6.
rewrite H3, H4, H5, H6, H7 in H2.
inversion H2.
unfold contextRuleInstruction in H2.
unfold contextRuleInstruction_s in H6.
rewrite H3, H4, H5, H6 in H2.
eapply slimPartialViewevalInstruction_correct; eauto; econstructor; eauto.
unfold contextRuleInstruction in H2.
rewrite H3, H4, H5 in H2.
eapply slimPartialViewevalInstruction_correct; eauto; econstructor; eauto.
unfold contextRuleInstruction in H2.
rewrite H3, H4 in H2.
eapply slimPartialViewevalInstruction_correct; eauto; econstructor; eauto.
destruct H3, H3.
destruct H4; [destruct H4|]; rewrite e in H4; inversion H4.
destruct H4; [destruct H4|]; rewrite e in H4; inversion H4.

assert (D := envpartialViewGetIdsSome f_name e1 b_env H1).
destruct D.
rewrite e in H3.
case_eq (copyIn_f e1 (f_args f) f_params); intros.
case_eq (initLocals_f e2 (f_local_decs f)); intros.
case_eq (contextRuleInstruction_s n e3 (f_instrs f)); intros.
case_eq (copyOut_f2 e4 (f_args f) f_params e1); intros.
simpl in H2.
rewrite H3, H4, H5, H6, H7 in H2; inversion H2; subst.
assert (envpartialView e2 f_env) by (eapply partialViewEnvCopyIn; eauto; eapply copyIn_f_correct; eauto).
assert (envpartialView e3 f_env') by (eapply partialViewEnvInitLocals; eauto; eapply initLocals_f_correct; eauto).
assert (envpartialView e4 af_env) by (eauto).
apply copyOut_f_correct in c0.
simpl.
simpl in H1.
apply (partialViewEnvCopyOut (f_args f) f_params e1 slim_e e4 af_env b_env a_env); eauto.
unfold contextRuleInstruction in H2.
unfold contextRuleInstruction_s in H6.
rewrite H3, H4, H5, H6, H7 in H2.
inversion H2.
unfold contextRuleInstruction in H2.
unfold contextRuleInstruction_s in H6.
rewrite H3, H4, H5, H6 in H2.
eapply slimPartialViewevalInstruction_correct; eauto; econstructor; eauto.
unfold contextRuleInstruction in H2.
rewrite H3, H4, H5 in H2.
eapply slimPartialViewevalInstruction_correct; eauto; econstructor; eauto.
unfold contextRuleInstruction in H2.
rewrite H3, H4 in H2.
eapply slimPartialViewevalInstruction_correct; eauto; econstructor; eauto.
destruct H3, H3.
destruct H4; [destruct H4|]; rewrite e in H4; inversion H4.
destruct H4; [destruct H4|]; rewrite e in H4; inversion H4.

assert (D := envpartialViewGetIdsSome f_name e1 b_env H0).
destruct D.
rewrite e in H2.
rewrite H2 in H1.
eapply slimPartialViewevalInstruction_correct; eauto; eapply evalCallExtFunctionWithParamsOK; eauto.
destruct H2, H2.
destruct H3; [destruct H3|]; rewrite e in H3; inversion H3.
destruct H3; [destruct H3|]; rewrite e in H3; inversion H3.

assert (D := envpartialViewGetIdsSome f_name e1 b_env H0).
destruct D.
rewrite e in H2.
rewrite H2 in H1.
eapply slimPartialViewevalInstruction_correct; eauto; eapply evalCallExtFunctionNoParamsOK; eauto.
destruct H2, H2.
destruct H3; [destruct H3|]; rewrite e in H3; inversion H3.
destruct H3; [destruct H3|]; rewrite e in H3; inversion H3.

assert (D := envpartialViewGetIdsSome f_name e1 b_env H0).
destruct D.
rewrite e in H2.
rewrite H2 in H1.
eapply slimPartialViewevalInstruction_correct; eauto; eapply evalCallUnknownFunctionWithParamsOK; eauto.
destruct H2, H2.
destruct H3; [destruct H3|]; rewrite e in H3; inversion H3.
rewrite H2 in H1.
eapply slimPartialViewevalInstruction_correct; eauto; eapply evalCallUnknownFunctionWithParamsOK; eauto.
destruct H3; [destruct H3|]; rewrite e in H3; inversion H3.
rewrite H2 in H1.
eapply slimPartialViewevalInstruction_correct; eauto; eapply evalCallUnknownFunctionWithParamsOK; eauto.

assert (D := envpartialViewGetIdsSome f_name e1 b_env H0).
destruct D.
rewrite e in H2.
rewrite H2 in H1.
eapply slimPartialViewevalInstruction_correct; eauto; eapply evalCallUnknownFunctionNoParamsOK; eauto.
destruct H2, H2.
destruct H3; [destruct H3|]; rewrite e in H3; inversion H3.
rewrite H2 in H1.
eapply slimPartialViewevalInstruction_correct; eauto; eapply evalCallUnknownFunctionNoParamsOK; eauto.
destruct H3; [destruct H3|]; rewrite e in H3; inversion H3.
rewrite H2 in H1.
eapply slimPartialViewevalInstruction_correct; eauto; eapply evalCallUnknownFunctionNoParamsOK; eauto.

unfold contextRuleInstruction_s in H3.
assert (evalInstruction_s (env, evn) (ISeqCons h t) res) by (econstructor; eauto).
eapply slimPartialViewInstruction_s_correct; eauto; econstructor; eauto.
simpl in H3.
case_eq (contextRuleInstruction n e1 h); intros; rewrite H4 in H3.
eauto.
case_eq (slimPartialViewevalInstruction e1 h); intros; rewrite H5 in H3; [|inversion H3].
eapply slimPartialViewInstruction_s_correct; eauto. 
assert (extractErrorType (OK env' evn') = None) by (simpl; eauto).
assert (D := slimPartialViewevalInstruction_correct (env, evn) h (OK env' evn') e H6 e1 e2 H2 H5).
auto.

simpl in H2.
eapply slimPartialViewInstruction_s_correct; eauto; econstructor; eauto.
unfold contextRuleElif_instruction in H2.
assert (evalElif_instruction (env, evn) (IElif cond ins_true ins_false) res) by (econstructor; eauto).
eapply slimPartialViewElif_instruction_correct; eauto.
assert (D := envpartialViewEvalSec_expression_if e1 env evn (Bool true) cond H1).
destruct D.
assert (H4 := H3 e).
unfold contextRuleElif_instruction in H2.
rewrite H4 in H2.
eauto.
unfold contextRuleElif_instruction in H2.
rewrite H3 in H2.
assert (evalElif_instruction (env, evn) (IElif cond ins_true ins_false) res) by (econstructor; eauto).
eapply slimPartialViewElif_instruction_correct; eauto.
unfold contextRuleElif_instruction in H2.
assert (evalElif_instruction (env, evn) (IElif cond ins_true ins_false) res) by (econstructor; eauto).
eapply slimPartialViewElif_instruction_correct; eauto.
assert (D := envpartialViewEvalSec_expression_if e1 env evn (Bool false) cond H1).
destruct D.
assert (H4 := H3 e).
unfold contextRuleElif_instruction in H2.
rewrite H4 in H2.
eauto.
unfold contextRuleElif_instruction in H2.
rewrite H3 in H2.
assert (evalElif_instruction (env, evn) (IElif cond ins_true ins_false) res) by (econstructor; eauto).
eapply slimPartialViewElif_instruction_correct; eauto.

unfold contextRuleCase_instruction in H2.
assert (evalCase_instruction (env, evn) var (ICaseCons (exp, ins_s) case_t) res) by (econstructor; eauto).
eapply slimPartialViewCase_instruction_correct; eauto.
apply evalExpression_f_correct in e.
assert (D := envpartialViewEvalExpression e1 env (Ebinary Equal (EVar var) exp) H1).
rewrite e in D.
apply evalExpression_f_correct in e.
destruct D.
unfold contextRuleCase_instruction in H2.
rewrite H3 in H2.
eauto.
unfold contextRuleCase_instruction in H2.
rewrite H3 in H2.
assert (evalCase_instruction (env, evn) var (ICaseCons (exp, ins_s) case_t) res) by (econstructor; eauto).
eapply slimPartialViewCase_instruction_correct; eauto.
unfold contextRuleCase_instruction in H2.
assert (evalCase_instruction (env, evn) var (ICaseCons (exp, ins_s) case_t) res) by (econstructor; eauto).
eapply slimPartialViewCase_instruction_correct; eauto.
apply evalExpression_f_correct in e.
assert (D := envpartialViewEvalExpression e1 env (Ebinary Equal (EVar var) exp) H1).
rewrite e in D.
apply evalExpression_f_correct in e.
destruct D.
unfold contextRuleCase_instruction in H2.
rewrite H3 in H2.
eauto.
unfold contextRuleCase_instruction in H2.
rewrite H3 in H2.
assert (evalCase_instruction (env, evn) var (ICaseCons (exp, ins_s) case_t) res) by (econstructor; eauto).
eapply slimPartialViewCase_instruction_correct; eauto.
Qed.

Lemma contextRuleCorrect_Instruction: forall in_data f res,
  evalInstruction in_data f res ->
  forall n e slim_e, extractErrorType res = None -> 
  envpartialView e (fst in_data) ->
  contextRuleInstruction n e f = Some slim_e ->
  envpartialView slim_e (extractEnv res).
Proof.
eapply contextRuleCorrect; eauto.
Qed.

Lemma contextRuleCorrect_Instruction_s: forall in_data f res,
  evalInstruction_s in_data f res ->
  forall n e slim_e, extractErrorType res = None -> 
  envpartialView e (fst in_data) ->
  contextRuleInstruction_s n e f = Some slim_e ->
  envpartialView slim_e (extractEnv res).
Proof.
eapply contextRuleCorrect; eauto.
Qed.

Lemma contextRuleCorrect_Elif_instruction: forall in_data f res,
  evalElif_instruction in_data f res ->
  forall n e slim_e, extractErrorType res = None -> 
  envpartialView e (fst in_data) ->
  contextRuleElif_instruction n e f = Some slim_e ->
  envpartialView slim_e (extractEnv res).
Proof.
eapply contextRuleCorrect; eauto.
Qed.

Lemma contextRuleCorrect_Case_instruction: forall in_data exp f res,
  evalCase_instruction in_data exp f res ->
  forall n e slim_e, extractErrorType res = None -> 
  envpartialView e (fst in_data) ->
  contextRuleCase_instruction n e exp f = Some slim_e ->
  envpartialView slim_e (extractEnv res).
Proof.
eapply contextRuleCorrect; eauto.
Qed.









