Require Import Correctness Ast Semantics Semantics_f PartialView TacUtils Environment PartialViewOptimizerPropagator FlowOptim While_unroll ExpressionSimplifier.
Require Import List String ZArith Lia Bool.
Import ListNotations.

Definition flow_optimizer := encapsuler_for_compilation flow_optimize.

Theorem flow_optimizer_correct : forall dup dec p c,
  compilation_correct_optimization p c (flow_optimizer dup dec).
Proof.
intros.
apply encapsuler_for_compilation_correct.
exact flow_optimize_instruction_s_correct.
Qed.

Definition while_full_unroll_optimizer := encapsuler_for_compilation (while_unroll_optimize).

Theorem while_full_unroll_optimizer_correct : forall dup dec p c,
  compilation_correct_optimization p c (while_full_unroll_optimizer dup dec).
Proof.
intros.
apply encapsuler_for_compilation_correct.
apply while_unroll_optimize_instruction_s_correct.
Qed.

Definition expression_simpl_optimizer := simpl_compilation.

Theorem simpl_compilation_correct : forall p c,
  compilation_correct_optimization p c simpl_compilation.
Proof.
unfold compilation_correct_optimization.
induction c; simpl.
intros.
rewrite H in *.
remember ({|
           localVars := [];
           globalVars := globalV;
           funDefs := [];
           localfunDecls := declF;
           externals := exts |}) as inenv.
induction b; simpl; intros.
case_eq (defineFunction e inenv); intros; rewrite H1 in *.
simpl.
split; simpl; auto; intros.
assert (f_name e =
    f_name (simpl_ed (reasonably_smallest_environment de) e)) by (simpl; eauto).
simpl in H, H0; rewrite H in H0; inversion H0; subst.
inversion H2; subst.
assert (D := env_optimization_correct_defineFunction e (simpl_ed (reasonably_smallest_environment de) e)
{|
   localVars := [];
   globalVars := globalV';
   funDefs := [];
   localfunDecls := declF';
   externals := exts'
 |} 
{|
   localVars := [];
   globalVars := globalV';
   funDefs := [];
   localfunDecls := declF';
   externals := exts'
 |} (reasonably_smallest_environment de) de de' 
 (env_optimization_correct_refl (reasonably_smallest_environment de) {|
   localVars := [];
   globalVars := globalV';
   funDefs := [];
   localfunDecls := declF';
   externals := exts'
 |}) H4 (simpl_ed_correct' de e) H1 H3
).
rewrite env_optimization_correct_equiv.
auto.
split; eauto; intros; try einversion.
split; eauto.
split.
apply include_except_prag_simpl_declaration_de_baseList.
intros.
simpl in H0, H.
rewrite H0 in H; inversion H; subst.
assert (evalDeclaration_de_baseList
       {|
         localVars := [];
         globalVars := globalV;
         funDefs := [];
         localfunDecls := declF;
         externals := exts
       |}
       (simpl_declaration_de_baseList
          {|
            localVars := [];
            globalVars := globalV;
            funDefs := [];
            localfunDecls := declF;
            externals := exts
          |} l) env_wd).
eapply simpl_declaration_de_baseList_equiv; eauto.
apply envpartialView_refl.
apply evalDeclaration_de_baseList_f_correct in H3, H2.
rewrite H2 in H3; inversion H3; subst.
rewrite env_optimization_correct_equiv.
apply env_optimization_correct_refl.
split; eauto.
split.
apply include_except_prag_simpl_declaration_1_bodyList.
intros.
simpl in H0, H.
rewrite H0 in H; inversion H; subst.
assert (evalDeclaration_1_bodyList
       {|
         localVars := [];
         globalVars := globalV;
         funDefs := [];
         localfunDecls := declF;
         externals := exts
       |}
       (simpl_declaration_1_bodyList
          {|
            localVars := [];
            globalVars := globalV;
            funDefs := [];
            localfunDecls := declF;
            externals := exts
          |} l) env_wd).
eapply simpl_declaration_1_bodyList_equiv; eauto.
apply envpartialView_refl.
apply evalDeclaration_1_bodyList_f_correct in H3, H5.
rewrite H5 in H3; inversion H3; subst.
apply evalDeclaration_1_bodyList_f_correct in H1.
rewrite H1 in H4.
case_eq (evalDeclaration_2_bodyList_f env_wd' l0); intros; rewrite H6 in *; eauto.
rewrite <- evalDeclaration_2_bodyList_f_correct in *.
eapply simpl_declaration_2_bodyList_correct with (inenv':=env_wd'); eauto.
apply env_optimization_correct_refl.
rewrite evalDeclaration_2_bodyList_f_correct in *.
rewrite H6 in H2; inversion H2; subst.
auto.
rewrite evalDeclaration_2_bodyList_f_correct in *.
rewrite H2 in H6; einversion.
Qed.