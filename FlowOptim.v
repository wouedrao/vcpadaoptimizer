Require Import Map MapTypes Values Environment Event Semantics Semantics_f Correctness Determinism 
PartialView TacUtils Determinism ContextRule OptimUtils PartialViewOptimizerPropagator.
Require Import Ast.
Require Import List String ZArith Lia Bool.
Import ListNotations.


Fixpoint select var env case_t := match case_t with
| ICaseFinishDefault ins_s => None
| ICaseCons (exp, ins_s) case_t' => 
    (
      match evalExpression_f env (Ebinary Equal (EVar var) exp) with
      | Some (Bool true) => Some ins_s
      | Some (Bool false) => select var env case_t'
      | _ => None
      end
    )
end.

Theorem select_correct: forall var in_data case res,
  evalCase_instruction in_data var case res -> forall ins env,
  envpartialView env (fst in_data) ->
  select var env case = Some ins ->
  evalInstruction_s in_data ins res.
Proof.
induction 1; intros.
simpl in *; inversion H1; subst.
simpl in *; inversion H1; subst; auto.
assert (D := envpartialViewEvalExpression env0 env (Ebinary Equal (EVar var) exp) H1).
apply evalExpression_f_correct in H.
rewrite H in D.
unfold select in H2.
destruct D; rewrite H3 in H2; auto.
simpl in *; inversion H2; subst; auto.
inversion H2.
assert (D := envpartialViewEvalExpression env0 env (Ebinary Equal (EVar var) exp) H1).
apply evalExpression_f_correct in H.
rewrite H in D.
unfold select in H2.
destruct D; rewrite H3 in H2; auto.
eapply IHevalCase_instruction; eauto.
inversion H2.
Qed.

Fixpoint select_if env elif := match elif with
| IElse ins_s => Some ins_s
| IElif cond ins_true ins_false => 
    (
      match evalSec_opel_expression_if_f env cond with
      | Some (Bool true) => Some ins_true
      | Some (Bool false) => select_if env ins_false
      | _ => None
      end
    )
end.

Theorem select_if_correct: forall in_data case res,
  evalElif_instruction in_data case res -> forall ins env,
  envpartialView env (fst in_data) ->
  select_if env case = Some ins ->
  evalInstruction_s in_data ins res.
Proof.
induction 1; intros.
simpl in *; inversion H1; subst; auto.
assert (D := envpartialViewEvalSec_expression_if env0 env evn (Bool true) cond H1).
destruct D.
assert (D := H3 H).
unfold select_if in H2.
rewrite D in H2; auto.
simpl in *; inversion H2; subst; auto.
unfold select_if in H2.
rewrite H3 in H2; auto.
simpl in *; inversion H2; subst; auto.
assert (D := envpartialViewEvalSec_expression_if env0 env evn (Bool false) cond H1).
destruct D.
assert (D := H3 H).
unfold select_if in H2.
rewrite D in H2; auto.
eauto.
unfold select_if in H2.
rewrite H3 in H2; auto.
simpl in *; inversion H2; subst; auto.
Qed.

Fixpoint stmtAppend l l0 := match l with
| ISeqEnd h => ISeqCons h l0
| ISeqCons h t => ISeqCons h (stmtAppend t l0)
end.

Theorem stmtAppend_correct_stop : forall i res in_data,
  evalInstruction_s in_data i res -> forall i0 etype,
  extractErrorType res = Some etype -> 
  evalInstruction_s in_data (stmtAppend i i0) res.
Proof.
induction 1; intros.
apply equivExtractErr in H0; rewrite H0 in *.
simpl.
eapply evalInstruction_sISeqConsError; eauto.
simpl.
eapply evalInstruction_sISeqConsOk; eauto.
simpl.
eapply evalInstruction_sISeqConsError; eauto.
Qed.

Theorem stmtAppend_correct_continue : forall i res in_data,
  evalInstruction_s in_data i res -> forall i0 res',
  extractErrorType res = None -> 
  evalInstruction_s (extractEnv res, extractEvent res) i0 res' ->
  evalInstruction_s in_data (stmtAppend i i0) res'.
Proof.
induction 1; intros.
apply equivExtractNoErr in H0; rewrite H0 in H.
simpl.
eapply evalInstruction_sISeqConsOk; eauto.
simpl.
eapply evalInstruction_sISeqConsOk; eauto.
inversion H0.
Qed.

Definition forgetInstruction_s i env := 
  (
    match forgetListInstruction_s env i with
    | Some l => forgetIdsValueList l (forgetFinisherInstruction_s i env)
    | None => None
    end
  ).

Lemma forgetInstruction_is_lower : forall i env env', forgetInstruction_s i env = Some env' -> envpartialView env' env.
Proof.
unfold forgetInstruction_s.
intros.
case_eq (forgetListInstruction_s env i); intros; rewrite H0 in H; [|inversion H].
apply forgetListPartial in H; auto.
apply simplForgetFinisherInstruction_s.
Qed.

Lemma forgetInstruction_is_idempoten : forall i env env', forgetInstruction_s i env = Some env' -> forgetInstruction_s i env' = Some env'.
Proof.
intros.
assert (envpartialView env' env).
eapply forgetInstruction_is_lower; eauto.
unfold forgetInstruction_s in *.
case_eq (forgetListInstruction_s env i); intros; rewrite H1 in H; [|inversion H].
assert (forgetListInstruction_s env' i = Some l).
rewrite <- H1.
apply partialEnvForgetListIdemPotent_Instruction_s; auto.
rewrite H2.
eapply forgetListIdemPoten; eauto.
apply simplForgetFinisherInstruction_s.
Qed.

Lemma forgetInstruction_is_lower_after_exec : forall in_data f res,
evalInstruction_s in_data f res ->
  extractErrorType res = None -> forall e slim_e,
  envpartialView e (fst in_data) ->
  forgetInstruction_s f e = Some slim_e ->
  envpartialView slim_e (extractEnv res).
Proof.
unfold forgetInstruction_s.
intros.
case_eq (forgetListInstruction_s e f); intros; rewrite H3 in H2; [|inversion H2].
eapply forgetListInstruction_s_correct; eauto.
Qed.

Parameter default_fuel : nat.

Definition will_slimSet_set var env := match var with
| [] | _::_::_::_ => false
| [_] => true
| pid::id::[] => 
    (
      match StringMapModule.get pid env.(localVars) with
      | None => 
          (
            match StringMapModule.get pid env.(globalVars) with
            | None =>
              (
                match StringMapModule.get pid env.(funDefs) with
                | None =>
                  (
                      match StringMapModule.get pid env.(localfunDecls) with
                      | None =>
                        (
                          match StringMapModule.get pid env.(externals) with
                          | Some lib => 
                            (
                              match StringMapModule.get id lib with
                              | Some (Environment.EVar (VariableValue _)) => true
                              | _ => false
                              end
                            )
                          | _ => false
                          end
                        )
                     | _ => false
                     end
                  )
                | _ => false
                end
              )
            | _ => false
            end
          )
        | _ => false
       end
    )
end.

Opaque will_slimSet_set.
Opaque forgetInstruction_s.

Fixpoint optimize_Instruction env i := match i with
| ILoop cond ins => 
    (
      match evalSec_opel_w_f env cond with
      | Some (Bool false) => (INull, Some env)
      | _ => 
        (
          match forgetInstruction_s ins env with
          | None => (ILoop cond ins, None)
          | Some e => (ILoop cond (fst (optimize_Instruction_s e ins)), Some e)
          end
        )
      end
    )
| IIf cond ins_if ins_else => 
    (IIf cond (fst (optimize_Instruction_s env ins_if)) (fst (optimize_Elif_instruction env ins_else)), 
      contextRuleInstruction default_fuel env i)
| (ICase var case) => 
    (ICase var (fst (optimize_Case_instruction env var case)), contextRuleInstruction default_fuel env i)
| _ => (i, contextRuleInstruction default_fuel env i)
end
with optimize_Elif_instruction env i := match i with
| IElse ins => (IElse (fst (optimize_Instruction_s env ins)), contextRuleElif_instruction default_fuel env i)
| IElif cond ins_true ins_false =>
    (IElif cond (fst (optimize_Instruction_s env ins_true)) (fst (optimize_Elif_instruction env ins_false)), 
      contextRuleElif_instruction default_fuel env i)
end 
with optimize_Case_instruction env var i := match i with
| ICaseFinishDefault ins_s => 
    (ICaseFinishDefault (fst (optimize_Instruction_s env ins_s)), contextRuleCase_instruction default_fuel env var i)
| ICaseCons (exp, ins_s) case_t => 
    (
      (ICaseCons (exp, 
      (match evalExpression_f env exp with
      | Some (Int v) =>
        (
          if will_slimSet_set var env then
          (
            match slimSetIdsValue var (Int v) env with
            | Some env' => fst (optimize_Instruction_s env' ins_s) 
            | None => fst (optimize_Instruction_s env ins_s) 
            end
          )
          else
          (
            fst (optimize_Instruction_s env ins_s) 
          )
        )
      | _ => fst (optimize_Instruction_s env ins_s)
      end)) (fst (optimize_Case_instruction env var case_t))), contextRuleCase_instruction default_fuel env var i)
end
with optimize_Instruction_s env i := match i with
| ISeqEnd h => 
  (
    let h_opt := (fst (optimize_Instruction env h)) in 
    (match h_opt with
    | ICase var case => 
        (
          match select var env case with
          | None => ISeqEnd h_opt
          | Some ins => ins
          end
        )
    | IIf cond ins_if ins_else => 
        (
          match evalSec_opel_expression_if_f env cond with
          | Some (Bool true) => ins_if
          | Some (Bool false) => 
            (
              match select_if env ins_else with
              | Some ins => ins
              | None => ISeqEnd h_opt
              end
            )
          | _ => ISeqEnd h_opt
          end
        )
    | _ => ISeqEnd h_opt
    end, contextRuleInstruction_s default_fuel env i)
  ) 
| ISeqCons h t => 
  (
    let (h_opt, h_env) := optimize_Instruction env h in 
    let (t_opt, t_env) := 
      (
        match h_env with
        | Some outenv' => (optimize_Instruction_s outenv' t)
        | None => (t, None)
        end
      ) in
    (match h_opt with
    | ICase var case => 
        (
          match select var env case with
          | None => (ISeqCons h_opt t_opt)
          | Some ins => remove_incorrect_end_branch_in_list (stmtAppend ins t_opt)
          end
        )
    | IIf cond ins_if ins_else => 
        (
          match evalSec_opel_expression_if_f env cond with
          | Some (Bool true) => remove_incorrect_end_branch_in_list (stmtAppend ins_if t_opt)
          | Some (Bool false) => 
            (
              match select_if env ins_else with
              | Some ins => remove_incorrect_end_branch_in_list (stmtAppend ins t_opt)
              | None => (ISeqCons h_opt t_opt)
              end
            )
          | _ => (ISeqCons h_opt t_opt)
          end
        )
    | _ => remove_incorrect_begin_loop_in_list (ISeqCons h_opt t_opt)
    end, t_env)
  )
end.

Opaque contextRuleInstruction.
Opaque contextRuleInstruction_s.
Opaque contextRuleCase_instruction.
Opaque contextRuleElif_instruction.
Opaque remove_incorrect_end_branch_in_list.
Opaque remove_incorrect_begin_loop_in_list.
Opaque evalSec_opel_expression_if_f.

Theorem evalExpression_equal_case: forall env var exp v,
  evalExpression_f env (Ebinary Equal (EVar var) exp) = Some (Bool true) ->
  evalExpression_f env exp = Some (Int v) ->
  evalExpression_f env (EVar var) = Some (Int v)
  .
Proof.
unfold evalExpression_f.
intros.
case_eq (getIdsValue var env); intros; rewrite H1 in *.
induction v0; try einversion.
induction v0; try einversion.
rewrite H0 in H.
unfold Math.binary_operation in H.
unfold Math.eq in H.
induction v0; try einversion.
inversion H; subst.
apply Zeq_bool_eq in H3; subst; auto.
rewrite H0 in H.
unfold Math.binary_operation in H.
unfold Math.eq in H.
induction v0; try einversion.
inversion H; subst.
apply Zeq_bool_eq in H3; subst; auto.
rewrite H0 in H.
unfold Math.binary_operation in H.
unfold Math.eq in H.
inversion H.
Qed.

Theorem optimize_correct : 
  (
    forall in_data f res,
    evalInstruction in_data f res -> forall e,
    envpartialView e (fst in_data) ->
    evalInstruction in_data (fst (optimize_Instruction e f)) res /\ 
    (forall outenv, 
      (snd (optimize_Instruction e f)) = Some outenv -> 
      extractErrorType res = None -> envpartialView outenv (extractEnv res))
  ) /\ 
  (
    forall in_data f res,
    evalInstruction_s in_data f res -> forall e,
    envpartialView e (fst in_data) ->
    evalInstruction_s in_data (fst (optimize_Instruction_s e f)) res /\ 
    (forall outenv, 
      (snd (optimize_Instruction_s e f)) = Some outenv -> 
      extractErrorType res = None -> envpartialView outenv (extractEnv res))
  ) /\ 
  (
    forall in_data f res,
    evalElif_instruction in_data f res -> forall e,
    envpartialView e (fst in_data) -> 
    evalElif_instruction in_data (fst (optimize_Elif_instruction e f)) res /\ 
    (forall outenv,  
      (snd (optimize_Elif_instruction e f)) = Some outenv -> 
      extractErrorType res = None -> envpartialView outenv (extractEnv res))
  ) /\ 
  (
    forall in_data exp f res,
    evalCase_instruction in_data exp f res -> forall e,
    envpartialView e (fst in_data) ->
    evalCase_instruction in_data exp (fst (optimize_Case_instruction e exp f)) res /\ 
    (forall outenv, 
      (snd (optimize_Case_instruction e exp f)) = Some outenv -> 
      extractErrorType res = None -> envpartialView outenv (extractEnv res)) 
  ).
Proof.
apply sem_inst_mutind; try (solve [split; [econstructor; eauto|unfold optimize_Instruction; unfold snd; intros; 
eapply contextRuleCorrect_Instruction; eauto; econstructor; eauto]]); simpl.

split.
eapply evalInstructionIIfTrue; eauto.
eapply H; eauto.
intros.
eapply contextRuleCorrect_Instruction with (in_data:=(env, evn)) ; eauto.
eapply evalInstructionIIfTrue; eauto.
intros.
split.
eapply evalInstructionIIfFalse; eauto.
eapply H; eauto.
intros.
eapply contextRuleCorrect_Instruction with (in_data:=(env, evn)); eauto.
eapply evalInstructionIIfFalse; eauto.
intros.
split.
eapply evalInstructionICase; eauto.
eapply H; eauto.
intros.
eapply contextRuleCorrect_Instruction with (in_data:=(env, evn)); eauto.
eapply evalInstructionICase; eauto.

(*  Loop True *)
intros.
assert (D := envpartialViewEvalSec_opel_w e2 env cond H1).
apply evalSec_opel_w_f_correct in e.
rewrite e in D.
apply evalSec_opel_w_f_correct in e.
destruct D; rewrite H2.
case_eq (forgetInstruction_s ins e2); intros.
split.
eapply evalInstructionILoopTrueOk with (env':=env') (evn':=evn'); eauto.
eapply H; eauto.
apply forgetInstruction_is_lower in H3.
eapply envpartialView_trans; eauto.
assert (extractErrorType (OK env' evn') = None) by (simpl; auto).
assert 
  (D := 
  forgetInstruction_is_lower_after_exec 
  (env, evn) 
  ins 
  (OK env' evn') e0 H4 e2 e3 H1 H3).
assert (D' := H0 e3 D).
destruct D'.
case_eq (evalSec_opel_w_f e3 cond); intros; rewrite H7 in H5.
assert (forgetInstruction_s ins e3 = Some e3).
eapply forgetInstruction_is_idempoten; eauto.
rewrite H8 in H5.
induction v; auto.
induction n; auto.
apply forgetInstruction_is_lower in H3.
assert (D' := envpartialViewEvalSec_opel_w e3 e2 cond H3).
rewrite H7, H2 in D'.
destruct D'; inversion H9.
assert (forgetInstruction_s ins e3 = Some e3).
eapply forgetInstruction_is_idempoten; eauto.
rewrite H8 in H5; auto.
assert (extractErrorType (OK env' evn') = None) by (simpl; auto).
assert 
  (D := 
  forgetInstruction_is_lower_after_exec 
  (env, evn) 
  ins 
  (OK env' evn') e0 H4 e2 e3 H1 H3).
intros.
eapply H0; eauto.
unfold snd in H5.
inversion H5; subst.
simpl.
assert (forgetInstruction_s ins outenv = Some outenv).
eapply forgetInstruction_is_idempoten; eauto.
rewrite H7.
induction (evalSec_opel_w_f outenv cond); auto.
induction a; auto.
induction n; auto.
split.
eapply evalInstructionILoopTrueOk with (env':=env') (evn':=evn'); eauto.
intros.
unfold snd in H4.
inversion H4.
case_eq (forgetInstruction_s ins e2); intros.
split.
eapply evalInstructionILoopTrueOk with (env':=env') (evn':=evn'); eauto.
eapply H; eauto.
apply forgetInstruction_is_lower in H3.
eapply envpartialView_trans; eauto.
assert (extractErrorType (OK env' evn') = None) by (simpl; auto).
assert 
  (D := 
  forgetInstruction_is_lower_after_exec 
  (env, evn) 
  ins 
  (OK env' evn') e0 H4 e2 e3 H1 H3).
assert (D' := H0 e3 D).
destruct D'.
case_eq (evalSec_opel_w_f e3 cond); intros; rewrite H7 in *.
assert (forgetInstruction_s ins e3 = Some e3).
eapply forgetInstruction_is_idempoten; eauto.
rewrite H8 in H5.
induction v; auto.
induction n; auto.
apply forgetInstruction_is_lower in H3.
assert (D' := envpartialViewEvalSec_opel_w e3 e2 cond H3).
rewrite H7, H2 in D'.
destruct D'; inversion H9.
assert (forgetInstruction_s ins e3 = Some e3).
eapply forgetInstruction_is_idempoten; eauto.
rewrite H8 in H5.
auto.
unfold snd.
assert (extractErrorType (OK env' evn') = None) by (simpl; auto).
assert 
  (D := 
  forgetInstruction_is_lower_after_exec 
  (env, evn) 
  ins 
  (OK env' evn') e0 H4 e2 e3 H1 H3).
intros.
eapply H0; eauto.
inversion H5; subst.
simpl.
assert (forgetInstruction_s ins outenv = Some outenv).
eapply forgetInstruction_is_idempoten; eauto.
rewrite H7.
induction (evalSec_opel_w_f outenv cond); auto.
induction a; auto.
induction n; auto.
split.
eapply evalInstructionILoopTrueOk with (env':=env') (evn':=evn'); eauto.
intros.
inversion H4.

intros.
simpl.
assert (D := envpartialViewEvalSec_opel_w e2 env cond H0).
apply evalSec_opel_w_f_correct in e0; rewrite e0 in D; apply evalSec_opel_w_f_correct in e0.
destruct D; rewrite H1.
case_eq (forgetInstruction_s ins e2); intros; simpl.
split.
eapply evalInstructionILoopTrueError; eauto.
eapply H; eauto.
apply forgetInstruction_is_lower in H2.
eapply envpartialView_trans; eauto.
intros.
inversion H4.
split.
eapply evalInstructionILoopTrueError; eauto.
intros.
inversion H4.
case_eq (forgetInstruction_s ins e2); intros; simpl.
split.
eapply evalInstructionILoopTrueError; eauto.
eapply H; eauto.
apply forgetInstruction_is_lower in H2.
eapply envpartialView_trans; eauto.
intros.
inversion H4.
split.
eapply evalInstructionILoopTrueError; eauto.
intros.
inversion H4.

(*Loop False*)
simpl.
intros.
assert (D := envpartialViewEvalSec_opel_w e0 env cond H).
apply evalSec_opel_w_f_correct in e; rewrite e in D; apply evalSec_opel_w_f_correct in e.
destruct D; rewrite H0.
simpl.
split.
econstructor; eauto.
intros.
inversion H1; subst; auto.
case_eq (forgetInstruction_s ins e0); intros; simpl.
split.
eapply evalInstructionILoopFalse; eauto.
intros.
inversion H2; subst; auto.
apply forgetInstruction_is_lower in H1.
eapply envpartialView_trans; eauto.
split.
eapply evalInstructionILoopFalse; eauto.
intros.
inversion H2.

(*Seq*)

intros.
split.
assert (D := H e0 H0).
destruct D.
simpl.
case_eq (optimize_Instruction e0 i); intros; rewrite H3 in *.
simpl.
induction i0; try (solve [econstructor; eauto]); inversion H1; subst.
assert (D := envpartialViewEvalSec_expression_if e0 env evn (Bool true) s H0).
destruct D.
assert (D := H4 H10).
rewrite D.
auto.
rewrite H4.
econstructor; eauto.
assert (D := envpartialViewEvalSec_expression_if e0 env evn (Bool false) s H0).
destruct D.
assert (D := H4 H10).
rewrite D.
case_eq (select_if e0 e1); intros.
eapply select_if_correct; eauto.
eapply evalInstruction_sISeqEnd; auto.
rewrite H4.
eapply evalInstruction_sISeqEnd; auto.
case_eq (select l e0 c); intros.
eapply select_correct; eauto.
eapply evalInstruction_sISeqEnd; auto.
intros.
eapply contextRuleCorrect_Instruction_s with (in_data:=(env, evn)); eauto.
econstructor; eauto.

simpl.
intros.
case_eq (optimize_Instruction e1 h); intros.
assert (D := H e1 H1).
destruct D.
rewrite H2 in *.
simpl in H3, H4.
induction o.
case_eq (optimize_Instruction_s a t); intros.
assert (envpartialView a env').
eapply H4; eauto.
assert (D := H0 a H6).
destruct D.
rewrite H5 in *.
simpl in H7, H8.
simpl.
split; auto.
induction i; try (solve 
  [eapply remove_incorrect_begin_loop_in_list_correct; eauto;
  eapply evalInstruction_sISeqConsOk; eauto
    ]).
inversion H3; subst.
assert (D := envpartialViewEvalSec_expression_if e1 env evn (Bool true) s H1).
destruct D.
assert (D:= H9 H15).
rewrite D.
eapply remove_incorrect_end_branch_in_list_correct; eauto.
eapply stmtAppend_correct_continue; eauto.
rewrite H9.
eapply evalInstruction_sISeqConsOk; eauto.
assert (D := envpartialViewEvalSec_expression_if e1 env evn (Bool false) s H1).
destruct D.
assert (D:= H9 H15).
rewrite D.
case_eq (select_if e1 e2); intros.
eapply select_if_correct in H10; eauto.
eapply remove_incorrect_end_branch_in_list_correct; eauto.
eapply stmtAppend_correct_continue; eauto.
eapply evalInstruction_sISeqConsOk; eauto.
rewrite H9.
eapply evalInstruction_sISeqConsOk; eauto.
inversion H3; subst.
case_eq (select l e1 c); intros.
eapply select_correct with (res := (OK env' evn')) in H9; eauto.
eapply remove_incorrect_end_branch_in_list_correct; eauto.
eapply stmtAppend_correct_continue; eauto.
eapply evalInstruction_sISeqConsOk; eauto.
simpl.
split.
induction i; try (solve 
  [eapply remove_incorrect_begin_loop_in_list_correct; eauto; 
  eapply evalInstruction_sISeqConsOk; eauto
    ]).
inversion H3; subst.
assert (D := envpartialViewEvalSec_expression_if e1 env evn (Bool true) s H1).
destruct D.
assert (D:= H5 H11).
rewrite D.
eapply remove_incorrect_end_branch_in_list_correct; eauto.
eapply stmtAppend_correct_continue; eauto.
rewrite H5.
eapply evalInstruction_sISeqConsOk; eauto.
assert (D := envpartialViewEvalSec_expression_if e1 env evn (Bool false) s H1).
destruct D.
assert (D:= H5 H11).
rewrite D.
case_eq (select_if e1 e2); intros.
eapply select_if_correct in H6; eauto.
eapply remove_incorrect_end_branch_in_list_correct; eauto.
eapply stmtAppend_correct_continue; eauto.
eapply evalInstruction_sISeqConsOk; eauto.
rewrite H5.
eapply evalInstruction_sISeqConsOk; eauto.
inversion H3; subst.
case_eq (select l e1 c); intros.
eapply select_correct with (res := (OK env' evn')) in H5; eauto.
eapply remove_incorrect_end_branch_in_list_correct; eauto.
eapply stmtAppend_correct_continue; eauto.
eapply evalInstruction_sISeqConsOk; eauto.
intros.
inversion H5.

intros.
assert (D := H e1 H0).
destruct D.
case_eq (optimize_Instruction e1 h); intros; rewrite H3 in *.
induction o.
case_eq (optimize_Instruction_s a t); intros.
simpl.
split.
simpl in H1.
induction i; try (solve 
  [eapply remove_incorrect_begin_loop_in_list_correct; eauto; eapply evalInstruction_sISeqConsError; eauto]).
inversion H1; subst.
assert (D := envpartialViewEvalSec_expression_if e1 env evn (Bool true) s H0).
destruct D.
assert (D := H5 H11).
rewrite D.
eapply remove_incorrect_end_branch_in_list_correct; eauto.
eapply stmtAppend_correct_stop; simpl; eauto.
rewrite H5.
eapply evalInstruction_sISeqConsError; eauto.
assert (D := envpartialViewEvalSec_expression_if e1 env evn (Bool false) s H0).
destruct D.
assert (D := H5 H11).
rewrite D.
case_eq (select_if e1 e2); intros.
eapply select_if_correct in H6; eauto.
eapply remove_incorrect_end_branch_in_list_correct; eauto.
eapply stmtAppend_correct_stop; simpl; eauto.
eapply evalInstruction_sISeqConsError; eauto.
rewrite H5.
eapply evalInstruction_sISeqConsError; eauto.
case_eq (select l e1 c); intros.
eapply select_correct with (res := (Error e env' evn')) (in_data:=(env, evn)) in H5; eauto.
eapply remove_incorrect_end_branch_in_list_correct; eauto.
eapply stmtAppend_correct_stop; simpl; eauto.
inversion H1; subst; eauto.
eapply evalInstruction_sISeqConsError; eauto.
intros.
inversion H6.
split; intros.
simpl.
induction i; try (solve 
  [eapply remove_incorrect_begin_loop_in_list_correct; eauto; eapply evalInstruction_sISeqConsError; eauto]).
inversion H1; subst.
assert (D := envpartialViewEvalSec_expression_if e1 env evn (Bool true) s H0).
destruct D.
assert (D := H4 H10).
rewrite D.
eapply remove_incorrect_end_branch_in_list_correct; eauto.
eapply stmtAppend_correct_stop; simpl; eauto.
rewrite H4.
eapply evalInstruction_sISeqConsError; eauto.
assert (D := envpartialViewEvalSec_expression_if e1 env evn (Bool false) s H0).
destruct D.
assert (D := H4 H10).
rewrite D.
case_eq (select_if e1 e2); intros.
eapply select_if_correct in H5; eauto.
eapply remove_incorrect_end_branch_in_list_correct; eauto.
eapply stmtAppend_correct_stop; simpl; eauto.
eapply evalInstruction_sISeqConsError; eauto.
rewrite H4.
eapply evalInstruction_sISeqConsError; eauto.
case_eq (select l e1 c); intros.
eapply select_correct with (res := (Error e env' evn')) (in_data:=(env, evn)) in H4; eauto.
eapply remove_incorrect_end_branch_in_list_correct; eauto.
eapply stmtAppend_correct_stop; simpl; eauto.
inversion H1; subst; eauto.
eapply evalInstruction_sISeqConsError; eauto.
inversion H5.

intros.
assert (D := H e0 H0).
destruct D.
split; intros; eauto.
econstructor; eauto.
eapply contextRuleCorrect_Elif_instruction with (in_data:=(env, evn)); eauto.
econstructor; eauto.

intros.
assert (D := H e1 H0).
destruct D.
split; intros; eauto.
econstructor; eauto.
eapply contextRuleCorrect_Elif_instruction with (in_data:=(env, evn)); eauto.
econstructor; eauto.

intros.
assert (D := H e1 H0).
destruct D.
split; intros; eauto.
eapply evalInstruction_sIElifFalse; eauto.
assert (evalElif_instruction (env, evn) (IElif cond ins_true ins_false) res).
eapply evalInstruction_sIElifFalse; eauto.
eapply contextRuleCorrect_Elif_instruction with (in_data:=(env, evn)) ; eauto.

intros.
assert (D := H e0 H0).
destruct D.
split; intros; eauto.
econstructor; eauto.
inversion H4.

intros.
assert (D := H e1 H0).
destruct D.
split; intros; eauto.
econstructor; eauto.
inversion H4.

intros.
split.
econstructor; eauto.
assert (D := H e1 H0).
destruct D.
case_eq (evalExpression_f e1 exp); intros.
induction v; try eauto.
assert (D := envpartialViewEvalExpression e1 env exp H0).
rewrite H3 in D.
destruct D.
apply eq_sym in H4.
apply evalExpression_f_correct in e.
assert (D := evalExpression_equal_case env var exp n e H4).
apply evalExpression_f_correct in e.
simpl in D.
case_eq (getIdsValue var env); intros; rewrite H5 in D; try einversion.
induction v; try einversion.
induction v; try einversion.
inversion D; subst.
assert (D0 := partialViewSlimsetWhenUpIsConst var e1 env (Int n) H0 H5).
case_eq (will_slimSet_set var e1); intros.
destruct D0.
rewrite H7.
eapply H1; eauto.
rewrite H7.
case_eq (forgetIdsValue var e1); intros.
assert (envpartialView e2 env).
eapply envpartialView_trans; eauto.
eapply envpartialViewForgetVsNothing; eauto.
eapply envpartialView_refl; eauto.
eapply H; eauto.
eapply H; eauto.
eapply H1; eauto.
case_eq (will_slimSet_set var e1); intros.
inversion D; subst.
case_eq (slimSetIdsValue var (Int n) e1); intros.
assert (envpartialView e2 env).
eapply partialViewSlimsetActualValue; eauto.
eapply H; eauto.
eapply H; eauto.
eapply H1; eauto.
inversion H4.
eapply H1; eauto.
unfold snd.
intros.
assert (evalCase_instruction (env, evn) var (ICaseCons (exp, ins_s) case_t) res).
econstructor; eauto.
intros.
eapply contextRuleCorrect_Case_instruction with (in_data:=(env, evn)); eauto.

intros.
split.
eapply evalICaseConsFalse; eauto.
eapply H; eauto.
intros.
assert (evalCase_instruction (env, evn) var (ICaseCons (exp, ins_s) case_t) res).
eapply evalICaseConsFalse; eauto.
eapply contextRuleCorrect_Case_instruction with (in_data:=(env, evn)); eauto.
Qed.

Definition flow_optimize (dup : Ast.instruction_s -> bool) (dec : Ast.instruction_s -> Ast.instruction_s -> bool) ctx ins := fst (optimize_end_branch_number_instruction_s 0 (remove_null_instruction_s (fst (optimize_Instruction_s ctx ins)))).

Theorem flow_optimize_instruction_s_correct : contextBased_optimizer_is_correct flow_optimize.
Proof.
unfold contextBased_optimizer_is_correct.
unfold flow_optimize.
intros.
eapply optimize_end_branch_number_instruction_s_correct; eauto.
eapply remove_null_instruction_correct with (ins := (fst (optimize_Instruction_s e ins_s))); eauto.
eapply optimize_correct; eauto.
Qed.



