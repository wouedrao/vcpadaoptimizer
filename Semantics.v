Require Import Map MapTypes Values Environment Event.
Require Import Ast.
Require Import List String ZArith Lia.
Import ListNotations.

Inductive evalExpression : env -> expression -> value -> Prop :=
| evalELitInt : forall v env, evalExpression env (ELit (Int_lit v)) (Int v)
| evalELitBool : forall v env, evalExpression env (ELit (Bool_lit v)) (Bool v)
| evalEVarConst : forall id env v, getIdsValue id env = Some (Value (ConstantValue v)) ->
    evalExpression env (EVar id) v
| evalEVarVar : forall id env v, getIdsValue id env = Some (Value (VariableValue v)) ->
    evalExpression env (EVar id) v
| evalEVarNone : forall id env, getIdsValue id env = None ->
    evalExpression env (EVar id) Undefined
| evalEbinary : forall e0 e1 v0 v1 op v env, evalExpression env e0 v0 ->
    evalExpression env e1 v1 ->
    Math.binary_operation op v0 v1 = Some v ->
    evalExpression env (Ebinary op e0 e1) v
| evalEunary : forall e0 v0 op v env, evalExpression env e0 v0 ->
    Math.unary_operation op v0 = Some v ->
    evalExpression env (Eunary op e0) v.
    
Parameter make_event : env -> selected_com -> list expression_or_string_lit -> option (list (ident * mode * name)) -> event.
    
(* Inductive make_call_param_list : env -> list expression_or_string_lit -> list value -> Prop :=
| make_call_param_list_nil: forall env, make_call_param_list env [] []
| make_call_param_list_exp: forall env exp v te tv, 
      evalExpression env exp v ->
      make_call_param_list env te tv ->
      make_call_param_list env ((Exp exp)::te) (v::tv)
| make_call_param_list_str: forall env exp te tv, 
      make_call_param_list env te tv ->
      make_call_param_list env ((StringLit exp)::te) ((STR exp)::tv).
      
Inductive make_event : 
  env -> 
  selected_com -> 
  list expression_or_string_lit -> 
  option (list (ident * mode * name)) -> 
  event -> Prop :=
| make_event_do: forall env fname fparams fpeval fsig,
    make_call_param_list env fparams fpeval ->
    make_event env fname fparams fsig 
      (mkEvent fname fparams fpeval fsig 
        (mkenv [] env.(globalVars) env.(funDefs) env.(localfunDecls) env.(externals))). *)
    
(*Lemma evalExpressionInjective : forall env exp v v', evalExpression env exp v -> evalExpression env exp v' -> v = v'.
Proof.
intros.
generalize dependent v'.
induction H.
intros.
inversion H0; auto.
intros.
inversion H0; auto.
intros; inversion H0; rewrite H3 in H; inversion H; auto.
intros; inversion H0; rewrite H3 in H; inversion H; auto.
intros; inversion H0; rewrite H3 in H; inversion H; auto.
intros.
inversion H2.
apply IHevalExpression1 in H7.
apply IHevalExpression2 in H9.
rewrite H7 in *.
rewrite H9 in *.
rewrite H10 in H1.
inversion H1.
auto.
intros.
inversion H1.
apply IHevalExpression in H5.
rewrite H5 in *.
rewrite H7 in H0.
inversion H0; auto.
Qed.*)

Definition convSec_opel_parametre_nom_secu_to_exp id := (EVar id).

Definition convSec_opel_parametre_literal_to_exp id := (ELit id).

Definition convSec_opel_parametre_to_exp p := match p with
| SEC_OPEL_Value lit => convSec_opel_parametre_literal_to_exp lit
| SEC_OPEL_Var ids => convSec_opel_parametre_nom_secu_to_exp ids
end.

Definition convSec_opel_binary_num_op op := match op with 
| SOEAdd => EAdd
| SOEMinus => EMinus
| SOEMult => EMult
end.

Definition convSec_opel_binary_bool_op op := match op with 
| SOLand => And
| SOLor => Or
| SORless => LessThan
| SORleq => LessOrEq
| SORgreater => GreaterThan
| SORgeq => GreaterOrEq
| SOReq => Equal
| SORdiff => NEqual
end.

Definition evalSec_opel_expression_booleenne env e v:= match e with
| SOEBNot ids => evalExpression env (Eunary BoolNot (EVar ids)) v
| SOEBBinaire op ids p => 
    evalExpression env 
      (Ebinary 
          (convSec_opel_binary_bool_op op) 
          (convSec_opel_parametre_nom_secu_to_exp ids)
          (convSec_opel_parametre_to_exp p)
      )
      v
end.

Parameter ops_build_tag : env * list event -> (list String_Equality.t) -> (option ((list String_Equality.t) * (option (list String_Equality.t)))) -> value.
Parameter ops_new_safe_context : env * list event -> value.

Inductive evalSec_opel_expression_d_affectation : env * list event -> sec_opel_expression_d_affectation -> value -> Prop :=
| EvalSOEAffExpUMinus : forall ids env trace v, 
    evalExpression env (Eunary NumericUnaryMinus (EVar ids)) v ->
    evalSec_opel_expression_d_affectation (env, trace) (SOEAffExpUMinus ids) v
| EvalSOEAffExpBinary : forall op p0 p1 env trace v, 
    evalExpression env (Ebinary (convSec_opel_binary_num_op op) (convSec_opel_parametre_to_exp p0) (convSec_opel_parametre_to_exp p1)) v ->
    evalSec_opel_expression_d_affectation (env, trace) (SOEAffExpBinary op p0 p1) v
| EvalSOEAffDiv2 : forall ids e env trace v, 
    evalExpression env 
      (Ebinary Ediv (EVar ids) (Ebinary EPow (ELit (Int_lit 2)) e)) v ->
    evalSec_opel_expression_d_affectation (env, trace) (SOEAffDiv2 ids e) v
| EvalSOEAffBool : forall b v env trace, 
    evalSec_opel_expression_booleenne env b v ->
    evalSec_opel_expression_d_affectation (env, trace) (SOEAffBool b) v
| EvalSOEAffAssign : forall ids v env trace, 
    evalExpression env (convSec_opel_parametre_to_exp ids) v ->
    evalSec_opel_expression_d_affectation (env, trace) (SOEAffAssign ids) v
| EvalSOEAffBuildTag : forall ids params env trace, evalSec_opel_expression_d_affectation (env, trace) (SOEAffBuildTag ids params) (ops_build_tag (env, trace) ids params)  (*marquage d'un message série G95*)
| EvalSOEAffEncode: forall e v env trace, 
    evalExpression env e v ->
    evalSec_opel_expression_d_affectation (env, trace) (SOEAffEncode e) v
| EvalSOEAffRead_D0: forall ids v env trace, 
    evalExpression env (EVar ids) v -> (*Acquisition des Entrées scalaire non marqué*)
    evalSec_opel_expression_d_affectation (env, trace) (SOEAffRead ids None) v
| EvalSOEAffRead_D1: forall ids ids' v v' vf env trace, (*Definition variable*)
    evalExpression env (EVar ids) v ->
    evalExpression env (EVar ids') (Int v') ->
    idxs_select_from_value v [(Z.to_nat v')] vf ->
    evalSec_opel_expression_d_affectation (env, trace) (SOEAffRead ids (Some(ids', None))) vf
| EvalSOEAffRead_D2: forall ids ids' ids'' v v' v'' vf env trace, 
    evalExpression env (EVar ids) v ->
    evalExpression env (EVar ids') (Int v') ->
    evalExpression env (EVar ids'') (Int v'') ->
    idxs_select_from_value v [(Z.to_nat v'); (Z.to_nat v'')] vf ->
    evalSec_opel_expression_d_affectation (env, trace) (SOEAffRead ids (Some(ids', Some ids''))) vf
| EvalSOEAffReadContext: forall env trace,
    evalSec_opel_expression_d_affectation (env, trace) SOEAffReadContext (ops_new_safe_context (env, trace)) (*Voir doc*)
.

Inductive evalSec_opel_w : env -> sec_opel_w -> value -> Prop :=  
| EvalSOWBool : forall env b v,
    evalSec_opel_expression_booleenne env b v ->
    evalSec_opel_w env (SOWBool b) v
| SOWNSecu : forall env ids v,
    evalExpression env (EVar ids) v ->
    evalSec_opel_w env (SOWNSecu ids) (Math.to_bool v).

Parameter ops_is_valid : list event -> bool.

Inductive evalSec_opel_expression_if : env * list event -> sec_opel_expression_if -> value -> Prop :=
| EvalSOExpression_booleenne :
    forall evn b env v,
      evalSec_opel_expression_booleenne env b v -> 
      evalSec_opel_expression_if (env, evn) (SOExpression_booleenne b) v
| EvalSOIs_valid : forall evn env,
    evalSec_opel_expression_if (env, evn) SOIs_valid (Bool (ops_is_valid evn))
| EvalSOHard_input_valid : forall evn env,
    evalSec_opel_expression_if (env, evn) SOHard_input_valid (Bool (ops_is_valid evn))
| EvalSOt : forall ids evn env v,
    evalExpression env (EVar ids) v ->
    evalSec_opel_expression_if (env, evn) (SOt ids) (Math.to_bool v).
    

(* Fixpoint generateUndefinedArray1D n := match n with 
| 0 => []
| S n => (n, Undefined)::(generateUndefinedArray1D n)
end.

(* Compute (generateUndefinedArray1D 5). *)

Fixpoint generateUndefinedArray2D n m := match n with 
| 0 => []
| S n => (n, ArrayV (generateUndefinedArray1D m))::(generateUndefinedArray2D n m)
end.

(* Compute (generateUndefinedArray2D 5 2). *)

(* Compute (Z.to_nat (-5)). *)

Inductive generateUndefined : env -> type_designator -> value -> Prop := 
| generateUndefinedTypeSimple : forall env t, generateUndefined env (TypeSimple t) Undefined
| generateUndefinedTypeComposeArray1 : forall e v env t,
    evalExpression env e (Int v) ->
    generateUndefined env (TypeComposeArray1 t e) (ArrayV (generateUndefinedArray1D (Z.to_nat v)))
| generateUndefinedTypeComposeArray2 : forall e v e0 v0 env t,
    evalExpression env e (Int v) ->
    evalExpression env e0 (Int v0) ->
    generateUndefined env (TypeComposeArray2 t e e0) (ArrayV (generateUndefinedArray2D (Z.to_nat v) (Z.to_nat v0))). *)

(* Fixpoint setUndefined v := match v with
| Undefined => Undefined
| Int _ => Undefined
| Bool _ => Undefined
| STR _ => Undefined
| ArrayV a => (
                let fix setUndefinedComp vi' := match vi' with
                  | [] => []
                  | (hk, hv)::t => (hk, setUndefined hv)::(setUndefinedComp t)
                end in
                ArrayV (setUndefinedComp a)
              )
| RecordV a => (
                let fix setUndefinedComp vi' := match vi' with
                  | [] => []
                  | (hk, hv)::t => (hk, setUndefined hv)::(setUndefinedComp t)
                end in
                RecordV (setUndefinedComp a)
              )
end. *)

Inductive copyIn : env -> list (ident * mode * name) -> list expression_or_string_lit -> env -> Prop :=
| CopyInNil: forall env, copyIn env [] [] {| localVars := [];
        globalVars := env.(globalVars);
        funDefs := env.(funDefs);
        localfunDecls := env.(localfunDecls);
        externals := env.(externals)  |}
| CopyInConsSTR: forall env' env'' env idSig IdTyp tSig strParam tParams, 
    copyIn env tSig tParams env' ->
    declareLocalVar idSig (ConstantValue (STR strParam)) env' = Some env'' ->
    copyIn env ((idSig, Ast.In, IdTyp)::tSig) ((StringLit strParam)::tParams) env'' 
| CopyInConsExpIn: forall v env' env'' env idSig IdTyp tSig eParam tParams, 
    evalExpression env eParam v ->
    copyIn env tSig tParams env' ->
    declareLocalVar idSig (ConstantValue v) env' = Some env'' ->
    copyIn env ((idSig, Ast.In, IdTyp)::tSig) ((Exp eParam)::tParams) env''
| CopyInConsExpInOut: forall v env' env'' env idSig IdTyp tSig eParam tParams, 
    evalExpression env (EVar eParam) v ->
    copyIn env tSig tParams env' ->
    declareLocalVar idSig (VariableValue v) env' = Some env'' ->
    copyIn env ((idSig, Ast.InOut, IdTyp)::tSig) ((Exp (EVar eParam))::tParams) env''
| CopyInConsExpOut: forall v env' env'' env idSig IdTyp tSig eParam tParams,
    copyIn env tSig tParams env' ->
    evalExpression env (EVar eParam) v ->
    declareLocalVar idSig (VariableValue Undefined) env' = Some env'' ->
    copyIn env ((idSig, Ast.Out, IdTyp)::tSig) ((Exp (EVar eParam))::tParams) env''
.

(*
    Projection...
*)

Fixpoint projectMapGlobal src dst := match dst with
| [] => []
| (h, ConstantValue v)::t => (h, ConstantValue v)::(projectMapGlobal src t)
| (h, VariableValue v)::t => (h, VariableValue (match StringMapModule.get h src with | Some (VariableValue vsrc) => vsrc | _ => v end))::(projectMapGlobal src t)
end.

Lemma projectMapGlobal_src_sv : forall dst src id v_src,
  StringMapModule.get id src = Some (VariableValue v_src) -> 
  StringMapModule.get id (projectMapGlobal src dst) = StringMapModule.get id src <->
  exists v_dst, StringMapModule.get id dst = Some (VariableValue v_dst) .
Proof.
induction dst; simpl; intros.
split; intros.
rewrite H in H0.
inversion H0.
destruct H0.
inversion H0.
induction a.
induction b.
simpl.
case_eq (String_Equality.eq id a); intros.
split; intros.
apply String_Equality.eq_spec_true in H0; subst.
rewrite H in H1.
inversion H1.
destruct H1.
inversion H1.
eapply IHdst; eauto.
simpl.
rewrite H.
case_eq (String_Equality.eq id a); intros.
apply String_Equality.eq_spec_true in H0; subst.
rewrite H.
split; eauto.
rewrite <- H.
eapply IHdst; eauto.
Qed.

Lemma projectMapGlobal_src_none : forall dst src id,
  StringMapModule.get id src = None -> 
  StringMapModule.get id (projectMapGlobal src dst) = StringMapModule.get id dst.
Proof.
induction dst; simpl; intros; auto.
induction a.
induction b; simpl.
induction (String_Equality.eq id a); auto.
case_eq (String_Equality.eq id a); intros; auto.
apply String_Equality.eq_spec_true in H0; subst.
rewrite H; auto.
Qed.

Lemma projectMapGlobal_dst_none : forall dst src id,
  StringMapModule.get id dst = None -> 
  StringMapModule.get id (projectMapGlobal src dst) = StringMapModule.get id dst.
Proof.
induction dst; simpl; intros; auto.
induction a.
induction b; simpl.
induction (String_Equality.eq id a); auto.
case_eq (String_Equality.eq id a); intros; auto.
apply String_Equality.eq_spec_true in H0; subst.
rewrite StringMapModule.Xeq_refl in H.
inversion H.
rewrite H0 in *; eauto.
Qed.

Lemma projectMapGlobal_src_sc : forall dst src id v_src,
  StringMapModule.get id src = Some (ConstantValue v_src) -> 
  StringMapModule.get id (projectMapGlobal src dst) = StringMapModule.get id dst.
Proof.
induction dst; simpl; intros; auto.
induction a.
induction b; simpl.
induction (String_Equality.eq id a); eauto.
case_eq (String_Equality.eq id a); intros; eauto.
apply String_Equality.eq_spec_true in H0; subst.
rewrite H; auto.
Qed.

Lemma projectMapGlobal_dst_sc : forall dst src id v_src,
  StringMapModule.get id dst = Some (ConstantValue v_src) -> 
  StringMapModule.get id (projectMapGlobal src dst) = Some (ConstantValue v_src).
Proof.
induction dst; simpl; intros; auto.
induction a.
induction b; simpl.
induction (String_Equality.eq id a); eauto.
case_eq (String_Equality.eq id a); intros; eauto.
apply String_Equality.eq_spec_true in H0; subst.
rewrite StringMapModule.Xeq_refl in H.
inversion H.
rewrite H0 in *; eauto.
Qed.

Lemma projectMapGlobal_dst_sv : forall dst src id v_src,
  StringMapModule.get id dst = Some (VariableValue v_src) -> 
  exists v_src', StringMapModule.get id (projectMapGlobal src dst) = Some (VariableValue v_src').
Proof.
induction dst; simpl; intros; auto.
inversion H.
induction a.
induction b; simpl.
induction (String_Equality.eq id a); eauto.
case_eq (String_Equality.eq id a); intros; eauto.
rewrite H0 in H; eauto.
Qed.

Lemma projectMapGlobal_sv_sv : forall dst src id v_src v_src',
  StringMapModule.get id src = Some (VariableValue v_src) -> 
  StringMapModule.get id dst = Some (VariableValue v_src') -> 
  StringMapModule.get id (projectMapGlobal src dst) = Some (VariableValue v_src).
Proof.
induction dst; simpl; intros; auto.
inversion H0.
induction a.
induction b; try induction c; simpl.
induction (String_Equality.eq id a); eauto.
inversion H0.
case_eq (String_Equality.eq id a); intros; rewrite H1 in *; eauto.
apply String_Equality.eq_spec_true in H1; subst.
inversion H0; subst.
rewrite H; auto.
Qed.

Lemma projectMapGlobal_src_nil : forall dst,
  projectMapGlobal [] dst = dst.
Proof.
induction dst; auto.
simpl.
induction a.
induction b; rewrite IHdst; auto.
Qed.

Fixpoint projectMapExternalVar src dst := match dst with
| [] => []
| (h, Environment.EVar (VariableValue v))::t => (h, Environment.EVar (VariableValue (match StringMapModule.get h src with | Some (Environment.EVar (VariableValue vsrc)) => vsrc | _ => v end)))::(projectMapExternalVar src t)
| (h, v)::t => (h, v)::(projectMapExternalVar src t)
end.

Lemma projectMapExternalVar_src_sv : forall dst src id v_src,
  StringMapModule.get id src = Some (Environment.EVar (VariableValue v_src)) -> 
  StringMapModule.get id (projectMapExternalVar src dst) = StringMapModule.get id src <->
  exists v_dst, StringMapModule.get id dst = Some (Environment.EVar (VariableValue v_dst)).
Proof.
induction dst; simpl; intros.
split; intros.
rewrite H in H0.
inversion H0.
destruct H0.
inversion H0.
induction a.
induction b.
induction c.
simpl.
case_eq (String_Equality.eq id a); intros.
split; intros.
apply String_Equality.eq_spec_true in H0; subst.
rewrite H in H1.
inversion H1.
destruct H1.
inversion H1.
eapply IHdst; eauto.
simpl.
rewrite H.
case_eq (String_Equality.eq id a); intros.
apply String_Equality.eq_spec_true in H0; subst.
rewrite H.
split; eauto.
rewrite <- H.
eapply IHdst; eauto.
simpl.
case_eq (String_Equality.eq id a); intros.
apply String_Equality.eq_spec_true in H0; subst.
split; intros.
rewrite H in H0.
inversion H0.
destruct H0.
inversion H0.
eauto.
Qed.

Lemma projectMapExternalVar_src_none : forall dst src id,
  StringMapModule.get id src = None -> 
  StringMapModule.get id (projectMapExternalVar src dst) = StringMapModule.get id dst.
Proof.
induction dst; simpl; intros; auto.
induction a.
induction b; try induction c; simpl.
induction (String_Equality.eq id a); auto.
case_eq (String_Equality.eq id a); intros; auto.
apply String_Equality.eq_spec_true in H0; subst.
rewrite H; auto.
induction (String_Equality.eq id a); auto.
Qed.

Lemma projectMapExternalVar_src_sc : forall dst src id v_src,
  StringMapModule.get id src = Some (Environment.EVar (ConstantValue v_src)) -> 
  StringMapModule.get id (projectMapExternalVar src dst) = StringMapModule.get id dst.
Proof.
induction dst; simpl; intros; auto.
induction a.
induction b; try induction c; simpl.
induction (String_Equality.eq id a); eauto.
case_eq (String_Equality.eq id a); intros; eauto.
apply String_Equality.eq_spec_true in H0; subst.
rewrite H; auto.
induction (String_Equality.eq id a); eauto.
Qed.

Lemma projectMapExternalVar_src_f : forall dst src id v_src,
  StringMapModule.get id src = Some (Environment.EFDecl v_src) -> 
  StringMapModule.get id (projectMapExternalVar src dst) = StringMapModule.get id dst.
Proof.
induction dst; simpl; intros; auto.
induction a.
induction b; try induction c; simpl.
induction (String_Equality.eq id a); eauto.
case_eq (String_Equality.eq id a); intros; eauto.
apply String_Equality.eq_spec_true in H0; subst.
rewrite H; auto.
induction (String_Equality.eq id a); eauto.
Qed.

Lemma projectMapExternalVar_dst_none : forall dst src id,
  StringMapModule.get id dst = None -> 
  StringMapModule.get id (projectMapExternalVar src dst) = None.
Proof.
induction dst; simpl; intros; auto.
induction a.
induction b; try induction c; simpl.
induction (String_Equality.eq id a); auto.
induction (String_Equality.eq id a); auto.
inversion H.
induction (String_Equality.eq id a); auto.
Qed.

Lemma projectMapExternalVar_dst_sc : forall dst src id v_src,
  StringMapModule.get id dst = Some (Environment.EVar (ConstantValue v_src)) -> 
  StringMapModule.get id (projectMapExternalVar src dst) = Some (Environment.EVar (ConstantValue v_src)).
Proof.
induction dst; simpl; intros; auto.
induction a.
induction b; try induction c; simpl.
induction (String_Equality.eq id a); eauto.
case_eq (String_Equality.eq id a); intros; eauto.
rewrite H0 in *.
inversion H.
rewrite H0 in *.
auto.
case_eq (String_Equality.eq id a); intros; eauto.
rewrite H0 in *; inversion H.
rewrite H0 in *; auto; inversion H.
Qed.

Lemma projectMapExternalVar_dst_f : forall dst src id v_src,
  StringMapModule.get id dst = Some (Environment.EFDecl v_src) -> 
  StringMapModule.get id (projectMapExternalVar src dst) = Some (Environment.EFDecl v_src).
Proof.
induction dst; simpl; intros; auto.
induction a.
induction b; try induction c; simpl.
induction (String_Equality.eq id a); eauto.
case_eq (String_Equality.eq id a); intros; eauto.
rewrite H0 in *.
inversion H.
rewrite H0 in *.
auto.
case_eq (String_Equality.eq id a); intros; eauto.
rewrite H0 in *; inversion H; subst; auto.
rewrite H0 in *; auto; inversion H.
Qed.

Lemma projectMapExternalVar_dst_sv : forall dst src id v_src,
  StringMapModule.get id dst = Some (Environment.EVar (VariableValue v_src)) -> 
  exists v, StringMapModule.get id (projectMapExternalVar src dst) = Some (Environment.EVar (VariableValue v)).
Proof.
induction dst; simpl; intros; auto.
inversion H.
induction a.
induction b; try induction c; simpl.
induction (String_Equality.eq id a); eauto.
case_eq (String_Equality.eq id a); intros; eauto.
rewrite H0 in *.
eauto.
case_eq (String_Equality.eq id a); intros; eauto.
rewrite H0 in *; inversion H.
rewrite H0 in *; eauto.
Qed.

Lemma projectMapExternalVar_sv_sv : forall dst src id v_src v_src',
  StringMapModule.get id src = Some (Environment.EVar (VariableValue v_src)) -> 
  StringMapModule.get id dst = Some (Environment.EVar (VariableValue v_src')) -> 
  StringMapModule.get id (projectMapExternalVar src dst) = Some (Environment.EVar (VariableValue v_src)).
Proof.
induction dst; simpl; intros; auto.
inversion H0.
induction a.
induction b; try induction c; simpl.
induction (String_Equality.eq id a); eauto.
inversion H0.
case_eq (String_Equality.eq id a); intros; rewrite H1 in *; eauto.
apply String_Equality.eq_spec_true in H1; subst.
inversion H0; subst.
rewrite H; auto.
case_eq (String_Equality.eq id a); intros; rewrite H1 in *; eauto.
inversion H0.
Qed.


Fixpoint projectMapLib src (dst: StringMapModule.t (StringMapModule.t External)) : StringMapModule.t (StringMapModule.t External) := match dst with
| [] => []
| (libname, libcontent)::t => (libname, match StringMapModule.get libname src with | Some libcontent' => projectMapExternalVar libcontent' libcontent | _ => libcontent end)::(projectMapLib src t)
end.

Lemma projectMapLib_dst_none: forall dst src id,
  StringMapModule.get id dst = None ->
  StringMapModule.get id (projectMapLib src dst) = None.
Proof.
induction dst; auto; simpl.
induction a.
intros.
case_eq (String_Equality.eq id a); intros; rewrite H0 in *; auto.
inversion H.
simpl.
rewrite H0.
auto.
Qed.

Lemma projectMapLib_dst_some: forall dst src id e,
  StringMapModule.get id dst = Some e ->
  (StringMapModule.get id (projectMapLib src dst) = Some e \/
    exists e', StringMapModule.get id src = Some e' /\ StringMapModule.get id (projectMapLib src dst) = Some (projectMapExternalVar e' e))
  .
Proof.
induction dst; auto; simpl.
induction a.
simpl.
intros src id e.
case_eq (String_Equality.eq id a); auto; intros.
inversion H0; subst.
case_eq (StringMapModule.get a src); auto.
intros.
right; auto.
apply String_Equality.eq_spec_true in H; subst.
eauto.
Qed.


Inductive copyOut : env -> list (ident * mode * name) -> list expression_or_string_lit -> env -> env -> Prop :=
| CopyOutNil: forall f_env c_env, copyOut f_env [] [] c_env {| localVars := c_env.(localVars);
        globalVars := projectMapGlobal f_env.(globalVars) c_env.(globalVars);
        funDefs := c_env.(funDefs);
        localfunDecls := c_env.(localfunDecls);
        externals := projectMapLib f_env.(externals) c_env.(externals)  |}
| CopyOutIn: forall f_env c_env env idSig IdTyp tSig eParam tParams, 
    copyOut f_env tSig tParams c_env env ->
    copyOut f_env ((idSig, Ast.In, IdTyp)::tSig) (eParam::tParams) c_env env
| CopyOutInOut: forall env' v f_env c_env env idSig IdTyp tSig eParam tParams, 
    copyOut f_env tSig tParams c_env env' ->
    evalExpression f_env (EVar [idSig]) v ->
    setIdsValue eParam v env' = Some env  ->
    copyOut f_env ((idSig, Ast.InOut, IdTyp)::tSig) ((Exp (EVar eParam))::tParams) c_env env
| CopyOutOut: forall env' v f_env c_env env idSig IdTyp tSig eParam tParams, 
    copyOut f_env tSig tParams c_env env' ->
    evalExpression f_env (EVar [idSig]) v ->
    setIdsValue eParam v env' = Some env  ->
    copyOut f_env ((idSig, Ast.Out, IdTyp)::tSig) ((Exp (EVar eParam))::tParams) c_env env
.

Definition decValue typ := match typ with
| TypeSimple _ => Undefined
| TypeComposeArray1 _ _ => ArrayV [] Undefined
| TypeComposeArray2 _ _ _ => ArrayV [] (ArrayV [] Undefined)
end.

Inductive initLocals : env * list event -> list f_local_dec -> env * list event -> Prop :=
| initLocalsNil : forall env trace, 
    initLocals (env, trace) [] (env, trace)
| initLocalsConsConst : forall env trace v env0 res id typ e objT,
    evalExpression env e v -> 
    declareLocalVar id (ConstantValue v) env = Some env0 ->
    initLocals (env0, trace) objT res ->
    initLocals (env, trace) ((LocalDecVar (ConstantVar id typ e))::objT) res
| initLocalsConsVar : forall env trace env0 res id typ objT,
    declareLocalVar id (VariableValue (decValue typ)) env = Some env0 ->
    initLocals (env0, trace) objT res ->
    initLocals (env, trace) ((LocalDecVar (VariableVar id typ))::objT) res
| initLocalsPragmaCustom : forall env trace id e objT res,
    initLocals (env, (make_event env [id] e None)::trace) objT res ->
    initLocals (env, trace) ((LocalDecPragma (Pragma id e))::objT) res
| initLocalsPragmaFile : forall env trace p0 p1 objT res,
    initLocals (env, (make_event env ["ops_file"%string] [(Exp (EVar p0)); (Exp p1)] None)::trace) objT res ->
    initLocals (env, trace) ((LocalDecPragma (SEC_OPEL_File p0 p1))::objT) res
| initLocalsPragmaSig : forall env trace p0 p1 p2 objT res,
    initLocals (env, (make_event env ["ops_signature"%string] [(Exp (EVar p0)); (Exp p1); (Exp p2)] None)::trace) objT res ->
    initLocals (env, trace) ((LocalDecPragma (SEC_OPEL_Signature p0 p1 p2))::objT) res.
    
(* Inductive copyOutExternalWithSig : env -> list (ident * mode * nom) -> list expression_or_string_lit -> env -> env -> Prop :=
| copyOutExternalWithSigNil : forall f_env env,
    copyOutExternalWithSig f_env [] [] env {| localVars := env.(localVars);
        globalVars := env.(globalVars);
        funDefs := env.(funDefs);
        localfunDecls := env.(localfunDecls);
        externals := projectMapMap f_env.(externals) env.(externals)  |}
| copyOutExternalWithSigIn : forall f_env env r_env idSig IdTyp tSig eParam tParams,
    copyOutExternalWithSig f_env tSig tParams env r_env ->
    copyOutExternalWithSig f_env ((idSig, Ast.In, IdTyp)::tSig) (eParam::tParams) env r_env
| copyOutExternalWithSigInOut : forall f_env v r_env r_env' env idSig IdTyp tSig eParam tParams,
    copyOutExternalWithSig f_env tSig tParams env r_env ->
    evalExpression f_env (EVar [idSig]) v ->
    setIdsValue eParam v r_env = Some r_env' ->
    copyOutExternalWithSig f_env ((idSig, Ast.InOut, IdTyp)::tSig) ((Exp (EVar eParam))::tParams) env r_env'
| copyOutExternalWithSigOut : forall f_env v r_env r_env' env idSig IdTyp tSig eParam tParams,
    copyOutExternalWithSig f_env tSig tParams env r_env ->
    evalExpression f_env (EVar [idSig]) v ->
    setIdsValue eParam v r_env = Some r_env' ->
    copyOutExternalWithSig f_env ((idSig, Ast.Out, IdTyp)::tSig) ((Exp (EVar eParam))::tParams) env r_env'
. *)

Inductive copyOutExternalWithoutSig : env -> list expression_or_string_lit -> env -> env -> Prop :=
| copyOutExternalWithoutSigNil : forall f_env env,
    copyOutExternalWithoutSig f_env [] env {| localVars := env.(localVars);
        globalVars := projectMapGlobal f_env.(globalVars) env.(globalVars);
        funDefs := env.(funDefs);
        localfunDecls := env.(localfunDecls);
        externals := projectMapLib f_env.(externals) env.(externals) |}
| copyOutExternalWithoutSigSTR : forall f_env env r_env eParam tParams,
    copyOutExternalWithoutSig f_env tParams env r_env ->
    copyOutExternalWithoutSig f_env ((StringLit eParam)::tParams) env r_env
| copyOutExternalWithoutSigELit : forall f_env env r_env eParam tParams,
    copyOutExternalWithoutSig f_env tParams env r_env ->
    copyOutExternalWithoutSig f_env ((Exp (ELit eParam))::tParams) env r_env
| copyOutExternalWithoutSigEbinary : forall f_env env r_env eParam0 eParam1 eParam2 tParams,
    copyOutExternalWithoutSig f_env tParams env r_env ->
    copyOutExternalWithoutSig f_env ((Exp (Ebinary eParam0 eParam1 eParam2))::tParams) env r_env
| copyOutExternalWithoutSigEunary : forall f_env env r_env eParam0 eParam1 tParams,
    copyOutExternalWithoutSig f_env tParams env r_env ->
    copyOutExternalWithoutSig f_env ((Exp (Eunary eParam0 eParam1))::tParams) env r_env
| copyOutExternalWithoutSigEVar : forall f_env v r_env r_env' env eParam tParams,
    copyOutExternalWithoutSig f_env tParams env r_env ->
    evalExpression f_env (EVar eParam) v ->
    setIdsValue eParam v r_env = Some r_env' ->
    copyOutExternalWithoutSig f_env ((Exp (EVar eParam))::tParams) env r_env'
.       

Definition initValue vi vf := match vi with
| ArrayV _ _ => ArrayV [] vf
| _ => vf
end.

Inductive errorType: Type :=
| Failure
| HorsCode.

Inductive interpretationResult :=
| OK : env -> list event -> interpretationResult
| Error : errorType -> env -> list event -> interpretationResult.

(* Compute (initValueSimple (ArrayV (generateUndefinedArray2D 5 2)) (Bool false)). *)

(*OPS I/O*)
Parameter opsCompareMessage : (list String_Equality.t) -> (list String_Equality.t) -> env -> list event -> bool.
Axiom opsCompareMessage_refl : forall m env evn, opsCompareMessage m m env evn = true.

Parameter opsCompareReintegrationArray : 
  (list String_Equality.t) -> (list String_Equality.t) ->
  (list String_Equality.t) -> (list String_Equality.t) ->
  (list String_Equality.t) -> (list String_Equality.t) -> env -> list event -> bool.

Parameter opsReadHardInput : (list String_Equality.t) -> expression -> env -> list event -> (list bool).

Fixpoint setListBool cnum list v := match list with
| [] => Some v
| h::t => 
  (
    match idx_set_from_value v cnum (Bool h) with
    | Some v' => setListBool (cnum + 1) t v'
    | None => None
    end
  )
end.

Definition extractEnv r := match r with
| OK env evlist => env
| Error typ env evlist => env
end.

Definition extractEvent r := match r with
| OK env evlist => evlist
| Error typ env evlist => evlist
end.

Definition extractErrorType r := match r with
| OK env evlist => None
| Error typ env evlist => Some typ
end.

Lemma equivExtractNoErr : forall x, 
  extractErrorType x = None <->
  x = (OK (extractEnv x) (extractEvent x)).
Proof.
induction x; simpl.
split; auto.
split; intros; inversion H.
Qed.

Lemma equivExtractErr : forall x typ, 
  extractErrorType x = Some typ <->
  x = (Error typ (extractEnv x) (extractEvent x)).
Proof.
induction x; simpl; intros.
split; intros; inversion H.
split; intros; inversion H; auto.
Qed.

Parameter opsReadMess : (list String_Equality.t) -> expression -> expression -> env -> list event -> (bool * value).
Parameter opsReadMessUnfiltered : (list String_Equality.t) -> expression -> env -> list event -> (bool * value).
Parameter writeHardOutput : (list String_Equality.t) -> expression -> env -> list event -> value.
Parameter writeMess : (list String_Equality.t) -> expression -> env -> list event -> value.
Parameter writeRedund : (list String_Equality.t) -> env -> list event -> value.

Parameter runExternal : env * list event -> (list String_Equality.t) -> association_d_arguments -> interpretationResult.
(* Axiom runExternal_determ : forall indata fname params res res',
  runExternal indata fname params res -> 
  runExternal indata fname params res' -> 
  res = res'.
 *)
Axiom runExternal_keepTrace : forall indata fname params res,
  runExternal indata fname params = res -> 
  exists add, extractEvent res = add ++ (snd indata).

(* Inductive execFunction : env * list event -> element_declararif -> association_d_arguments -> interpretationResult -> Prop :=
| execFunctionSucced : forall b_env b_evn f_params f af_env f_env f_env' a_env a_evn,
    copyIn b_env f.(f_args) f_params f_env ->
    initLocals f_env f.(f_local_decs) f_env' ->
    evalInstruction_s (f_env', b_evn) f.(f_instrs) (OK af_env a_evn) -> 
    copyOut af_env f.(f_args) f_params b_env a_env ->
    execFunction (b_env, b_evn) f f_params (OK a_env a_evn)
| execFunctionFailed : forall b_env b_evn f_params f af_env f_env f_env' a_evn err,
    copyIn b_env f.(f_args) f_params f_env ->
    initLocals f_env f.(f_local_decs) f_env' ->
    evalInstruction_s (f_env', b_evn) f.(f_instrs) (Error err af_env a_evn) -> 
    execFunction (b_env, b_evn) f f_params (Error err af_env a_evn) 
    
with evalFunCall : env * list event -> procedure_call -> interpretationResult -> Prop := *)

Inductive evalInstruction : env * list event -> instruction -> interpretationResult -> Prop :=
| evalInstructionINull: forall env evn, evalInstruction (env, evn) INull (OK env evn)
| evalInstructionIPragmaPragma : forall env evn id e, 
    evalInstruction (env, evn) (IPragma (Pragma id e)) (OK env ((make_event env [id] e None)::evn))
| evalInstructionIPragmaFile : forall env evn p0 p1 , 
    evalInstruction (env, evn) (IPragma (SEC_OPEL_File p0 p1)) (OK env ((make_event env ["ops_file"%string] [(Exp (EVar p0)); (Exp p1)] None)::evn))
| evalInstructionIPragmaSig : forall env evn p0 p1 p2, 
    evalInstruction (env, evn) (IPragma (SEC_OPEL_Signature p0 p1 p2)) (OK env ((make_event env ["ops_signature"%string] [(Exp (EVar p0)); (Exp p1); (Exp p2)] None)::evn))
| evalInstructionIAssign: forall var aff v env env_f evn, 
    evalSec_opel_expression_d_affectation (env, evn) aff v ->
    setIdsValue var v env = Some env_f -> 
    evalInstruction (env, evn) (IAssign var aff) (OK env_f evn)
| evalInstructionIIfTrue: forall cond ins_if ins_else env evn res, 
    evalSec_opel_expression_if (env, evn) cond (Bool true) ->
    evalInstruction_s (env, evn) ins_if res ->
    evalInstruction (env, evn) (IIf cond ins_if ins_else) res
| evalInstructionIIfFalse: forall cond ins_if ins_else env evn res, 
    evalSec_opel_expression_if (env, evn) cond (Bool false) ->
    evalElif_instruction (env, evn) ins_else res ->
    evalInstruction (env, evn) (IIf cond ins_if ins_else) res
| evalInstructionICase: forall var env evn case res, 
    evalCase_instruction (env, evn) var case res ->
    evalInstruction (env, evn) (ICase var case) res
| evalInstructionILoopTrueOk: forall cond ins env evn res env' evn', 
    evalSec_opel_w env cond (Bool true) ->
    evalInstruction_s (env, evn) ins (OK env' evn') ->
    evalInstruction (env', evn') (ILoop cond ins) res ->
    evalInstruction (env, evn) (ILoop cond ins) res
| evalInstructionILoopTrueError: forall cond ins env evn e env' evn', 
    evalSec_opel_w env cond (Bool true) ->
    evalInstruction_s (env, evn) ins (Error e env' evn') ->
    evalInstruction (env, evn) (ILoop cond ins) (Error e env' evn')
| evalInstructionILoopFalse: forall cond ins env evn, 
    evalSec_opel_w env cond (Bool false) ->
    evalInstruction (env, evn) (ILoop cond ins) (OK env evn)

| evalCallDefinedFunctionNoParamsSucceed : forall b_env b_evn b_evn' f_name f af_env f_env f_env' a_env a_evn,
    getIdsValue f_name b_env = Some (DefinedFunction f) ->
    copyIn b_env f.(f_args) [] f_env ->
    initLocals (f_env, b_evn) f.(f_local_decs) (f_env', b_evn') ->
    evalInstruction_s (f_env', b_evn') f.(f_instrs) (OK af_env a_evn) -> 
    copyOut af_env f.(f_args) [] b_env a_env ->
    evalInstruction (b_env, b_evn) (ICallDefinedFunction f_name None) (OK a_env a_evn)

| evalCallDefinedFunctionNoParamsError : forall b_env b_evn b_evn' f_name f af_env f_env f_env' a_evn err,
    getIdsValue f_name b_env = Some (DefinedFunction f) ->
    copyIn b_env f.(f_args) [] f_env ->
    initLocals (f_env, b_evn) f.(f_local_decs) (f_env', b_evn') ->
    evalInstruction_s (f_env', b_evn') f.(f_instrs) (Error err af_env a_evn) -> 
    evalInstruction (b_env, b_evn) (ICallDefinedFunction f_name None) (Error err af_env a_evn)

| evalCallDefinedFunctionSucceed : forall b_env b_evn b_evn' f_name f af_env f_env f_env' a_env a_evn f_params,
    getIdsValue f_name b_env = Some (DefinedFunction f) ->
    copyIn b_env f.(f_args) f_params f_env ->
    initLocals (f_env, b_evn) f.(f_local_decs) (f_env', b_evn') ->
    evalInstruction_s (f_env', b_evn') f.(f_instrs) (OK af_env a_evn) -> 
    copyOut af_env f.(f_args) f_params b_env a_env ->
    evalInstruction (b_env, b_evn) (ICallDefinedFunction f_name (Some f_params)) (OK a_env a_evn)

| evalCallDefinedFunctionError : forall b_env b_evn b_evn' f_name f af_env f_env f_env' a_evn err f_params,
    getIdsValue f_name b_env = Some (DefinedFunction f) ->
    copyIn b_env f.(f_args) f_params f_env ->
    initLocals (f_env, b_evn) f.(f_local_decs) (f_env', b_evn') ->
    evalInstruction_s (f_env', b_evn') f.(f_instrs) (Error err af_env a_evn) -> 
    evalInstruction (b_env, b_evn) (ICallDefinedFunction f_name (Some f_params)) (Error err af_env a_evn)
    
| evalCallExtFunctionWithParamsOK : forall b_env b_evn f_name f_params f_sig a_env f_env a_evn,
    getIdsValue f_name b_env = Some (FunctionDeclaration f_sig) ->
    runExternal (b_env, ((make_event b_env f_name f_params (Some f_sig))::b_evn)) f_name f_params = (OK a_env a_evn) ->
    copyOut a_env f_sig f_params b_env f_env ->
    evalInstruction (b_env, b_evn) (ICallDefinedFunction f_name (Some f_params)) (OK f_env a_evn)
| evalCallExtFunctionWithParamsError : forall b_env b_evn f_name f_params f_sig a_env a_evn etype,
    getIdsValue f_name b_env = Some (FunctionDeclaration f_sig) ->
    runExternal (b_env, ((make_event b_env f_name f_params (Some f_sig))::b_evn)) f_name f_params = (Error etype a_env a_evn) ->
    evalInstruction (b_env, b_evn) (ICallDefinedFunction f_name (Some f_params)) (Error etype a_env a_evn)
| evalCallExtFunctionNoParamsOK : forall b_env b_evn f_name f_sig a_env f_env a_evn,
    getIdsValue f_name b_env = Some (FunctionDeclaration f_sig) ->
    runExternal (b_env, ((make_event b_env f_name [] (Some f_sig))::b_evn)) f_name [] = (OK a_env a_evn) ->
    copyOut a_env f_sig [] b_env f_env ->
    evalInstruction (b_env, b_evn) (ICallDefinedFunction f_name None) (OK f_env a_evn)
| evalCallExtFunctionNoParamsError : forall b_env b_evn f_name f_sig a_env a_evn etype,
    getIdsValue f_name b_env = Some (FunctionDeclaration f_sig) ->
    runExternal (b_env, ((make_event b_env f_name [] (Some f_sig))::b_evn)) f_name [] = (Error etype a_env a_evn) ->
    evalInstruction (b_env, b_evn) (ICallDefinedFunction f_name None) (Error etype a_env a_evn)

| evalCallUnknownFunctionWithParamsOK : forall b_env b_evn f_name f_params a_env a_evn f_env,
    getIdsValue f_name b_env = None ->
    runExternal (b_env, ((make_event b_env f_name f_params None)::b_evn)) f_name f_params = (OK a_env a_evn) ->
    copyOutExternalWithoutSig a_env f_params b_env f_env ->
    evalInstruction (b_env, b_evn) (ICallDefinedFunction f_name (Some f_params)) (OK f_env a_evn)
| evalCallUnknownFunctionWithParamsError : forall b_env b_evn f_name f_params a_env a_evn etype,
    getIdsValue f_name b_env = None ->
    runExternal (b_env, ((make_event b_env f_name f_params None)::b_evn)) f_name f_params = (Error etype a_env a_evn) ->
    evalInstruction (b_env, b_evn) (ICallDefinedFunction f_name (Some f_params)) (Error etype a_env a_evn)
| evalCallUnknownFunctionNoParamsOK : forall b_env b_evn f_name a_env a_evn f_env,
    getIdsValue f_name b_env = None ->
    runExternal (b_env, ((make_event b_env f_name [] None)::b_evn)) f_name [] = (OK a_env a_evn) ->
    copyOutExternalWithoutSig a_env [] b_env f_env ->
    evalInstruction (b_env, b_evn) (ICallDefinedFunction f_name None) (OK f_env a_evn)
| evalCallUnknownFunctionNoParamsError : forall b_env b_evn f_name a_env a_evn etype,
    getIdsValue f_name b_env = None ->
    runExternal (b_env, ((make_event b_env f_name [] None)::b_evn)) f_name [] = (Error etype a_env a_evn) ->
    evalInstruction (b_env, b_evn) (ICallDefinedFunction f_name None) (Error etype a_env a_evn)

| evalSOInstructionBeginLoop : forall e b_env b_evn,
    evalInstruction (b_env, b_evn) (SOInstructionBeginLoop e) (OK b_env b_evn) (*Ignored*)
| evalSOInstructionConverge : forall e b_env b_evn,
    evalInstruction (b_env, b_evn) (SOInstructionConverge e) (OK b_env b_evn) (*Ignored*)
| evalSOInstructionEndBranch : forall e b_env b_evn,
    evalInstruction (b_env, b_evn) (SOInstructionEndBranch e) (OK b_env b_evn) (*Ignored*)
| evalSOInstructionEndInit : forall b_env b_evn,
    evalInstruction (b_env, b_evn) SOInstructionEndInit (OK b_env ((make_event b_env ["end_init"%string] [] None)::b_evn)) (*logged*)
| evalSOInstructionEndIter : forall b_env b_evn,
    evalInstruction (b_env, b_evn) SOInstructionEndIter (OK b_env b_evn) (*Ignored*)
| evalSOInstructionCompareM: forall b_env b_evn a_env status m0 m1 t,
    setIdsValue status (Bool (opsCompareMessage m0 m1 b_env b_evn)) b_env = Some a_env ->
    evalInstruction (b_env, b_evn) (SOInstructionCompareM status m0 m1 t) (OK a_env b_evn)
| evalSOInstructionCompareR: forall b_env b_evn a_env status e1 e2 e3 e4 e5 e6 t,
    setIdsValue status (Bool (opsCompareReintegrationArray e1 e2 e3 e4 e5 e6 b_env b_evn)) b_env = Some a_env ->
    evalInstruction (b_env, b_evn) (SOInstructionCompareR status e1 e2 e3 e4 e5 e6 t) (OK a_env b_evn)
| evalSOInstructionFailAlarm : forall b_env b_evn ,
    evalInstruction (b_env, b_evn) SOInstructionFailAlarm (Error Failure b_env ((make_event b_env ["failure_alarm"%string] [] None)::b_evn))
| evalSOInstructionInitNone : forall b_env b_evn var, 
    evalInstruction (b_env, b_evn) (SOInstructionInit var None) (OK b_env ((make_event b_env ["init"%string] [(Exp (EVar var))] None)::b_evn))
| evalSOInstructionInitSome : forall v vvar b_env a_env b_evn var e,
    evalExpression b_env e v -> 
    evalExpression b_env (EVar var) vvar -> 
    setIdsValue var (initValue vvar v) b_env = Some a_env ->
    evalInstruction (b_env, b_evn) (SOInstructionInit var (Some e)) (OK a_env ((make_event b_env ["init"%string] [(Exp (EVar var))] None)::b_evn))
| evalSOInstructionInitopel : forall b_env b_evn,
    evalInstruction (b_env, b_evn) SOInstructionInitopel (OK b_env ((make_event b_env ["initopel"%string] [] None)::b_evn)) 
| evalSOInstructionNewSafe: forall b_env b_evn e1 e2 t,
    evalInstruction (b_env, b_evn) (SOInstructionNewSafe e1 e2 t) (OK b_env ((make_event b_env ["new_safe_context"%string] [(Exp e1); (Exp e2)] None)::b_evn)) (*Logged*)
| evalSOInstructionNewSig: forall b_env b_evn x e1 e2 t,
    evalInstruction (b_env, b_evn) (SOInstructionNewSig x e1 e2 t) (OK b_env ((make_event b_env ["new_signature"%string] [(Exp (EVar x)); (Exp e1); (Exp e2)] None)::b_evn)) (*Logged*)
| evalSOInstructionReadHardInput: forall v b_env a_env b_evn t e n_config no_t blist tval,
    opsReadHardInput e n_config b_env b_evn = blist ->
    evalExpression b_env (EVar t) v -> 
    setListBool 0 blist v = Some tval ->
    setIdsValue t tval b_env = Some a_env ->
    evalInstruction (b_env, b_evn) (SOInstructionReadHardInput t e n_config no_t) (OK a_env ((make_event b_env ["read_hard_input"%string] [(Exp (EVar t)); (Exp (EVar e)); (Exp n_config)] None)::b_evn)) (*Logged*)
| evalSOInstructionReadMessTrue: forall v b_env a_env' a_env b_evn t valide e no_msg no_t no_fic no_bx,
    opsReadMess e no_msg no_t b_env b_evn = (true, v) ->
    setIdsValue t v b_env = Some a_env' ->
    setIdsValue valide (Bool true) a_env' = Some a_env ->
    evalInstruction (b_env, b_evn) (SOInstructionReadMess t valide e no_msg no_t no_fic no_bx) (OK a_env ((make_event b_env ["read_mess"%string] [(Exp (EVar t)); (Exp (EVar valide)); (Exp (EVar e)); (Exp no_msg); (Exp no_fic); (Exp no_bx)] None)::b_evn)) (*Logged*)
| evalSOInstructionReadMessFalse: forall v b_env a_env b_evn t valide e no_msg no_t no_fic no_bx,
    opsReadMess e no_msg no_t b_env b_evn = (false, v) ->
    setIdsValue valide (Bool false) b_env = Some a_env ->
    evalInstruction (b_env, b_evn) (SOInstructionReadMess t valide e no_msg no_t no_fic no_bx) (OK a_env ((make_event b_env ["read_mess"%string] [(Exp (EVar t)); (Exp (EVar valide)); (Exp (EVar e)); (Exp no_msg); (Exp no_fic); (Exp no_bx)] None)::b_evn)) (*Logged*)
| evalSOInstructionReadMessUnfilTrue: forall v b_env a_env' a_env b_evn t valide e no_msg no_fic no_bx,
    opsReadMessUnfiltered e no_msg b_env b_evn = (true, v) ->
    setIdsValue t v b_env = Some a_env' ->
    setIdsValue valide (Bool true) a_env' = Some a_env ->
    evalInstruction (b_env, b_evn) (SOInstructionReadMessUnfil t valide e no_msg no_fic no_bx) (OK a_env ((make_event b_env ["read_mess_unfiltered"%string] [(Exp (EVar t)); (Exp (EVar valide)); (Exp (EVar e)); (Exp no_msg); (Exp no_fic); (Exp no_bx)] None)::b_evn)) (*Logged*)
| evalSOInstructionReadMessUnfilFalse: forall v b_env a_env b_evn t valide e no_msg no_fic no_bx,
    opsReadMessUnfiltered e no_msg b_env b_evn = (false, v) ->
    setIdsValue valide (Bool false) b_env = Some a_env ->
    evalInstruction (b_env, b_evn) (SOInstructionReadMessUnfil t valide e no_msg no_fic no_bx) (OK a_env ((make_event b_env ["read_mess_unfiltered"%string] [(Exp (EVar t)); (Exp (EVar valide)); (Exp (EVar e)); (Exp no_msg); (Exp no_fic); (Exp no_bx)] None)::b_evn)) (*Logged*)

| evalSOInstructionWrite1: forall b_env a_env b_evn t vt vt' i vi x vx,
    evalExpression b_env (EVar t) vt ->
    evalExpression b_env (EVar i) (Int vi) ->
    evalExpression b_env (EVar x) vx ->
    idxs_set_from_value vt [(Z.to_nat vi)] vx vt' ->
    setIdsValue t vt' b_env = Some a_env ->
    evalInstruction (b_env, b_evn) (SOInstructionWrite t i x None) (OK a_env b_evn)

| evalSOInstructionWrite2: forall b_env a_env b_evn t vt vt' i vi j vj x vx,
    evalExpression b_env (EVar t) vt ->
    evalExpression b_env (EVar i) (Int vi) ->
    evalExpression b_env (EVar j) (Int vj) ->
    evalExpression b_env (EVar x) vx ->
    idxs_set_from_value vt [(Z.to_nat vi); (Z.to_nat vj)] vx vt' ->
    setIdsValue t vt' b_env = Some a_env ->
    evalInstruction (b_env, b_evn) (SOInstructionWrite t i j (Some x)) (OK a_env b_evn)
    
| evalSOInstructionWriteHardOutput: forall v b_env a_env b_evn s t no_config no_t,
    writeHardOutput t no_config b_env b_evn = v ->
    setIdsValue s v b_env = Some a_env ->
    evalInstruction (b_env, b_evn) (SOInstructionWriteHardOutput s t no_config no_t) (OK a_env ((make_event b_env ["write_hard_output"%string] [(Exp (EVar s)); (Exp (EVar t)); (Exp no_config)] None)::b_evn)) (*Logged*)
    
| evalSOInstructionWriteMess: forall v b_env a_env b_evn s t no_msg no_fic no_bx,
    writeMess t no_msg b_env b_evn = v ->
    setIdsValue s v b_env = Some a_env ->
    evalInstruction (b_env, b_evn) (SOInstructionWriteMess s t no_msg no_fic no_bx) (OK a_env ((make_event b_env ["write_mess"%string] [(Exp (EVar s)); (Exp (EVar t)); (Exp no_msg); (Exp no_fic); (Exp no_bx)] None)::b_evn)) (*Logged*)
    
| evalSOInstructionWriteOutvar: forall v b_env a_env b_evn z x no_fic no_bx no_t,
    evalExpression b_env (EVar x) v -> 
    setIdsValue z v b_env = Some a_env ->
    evalInstruction (b_env, b_evn) (SOInstructionWriteOutvar z x no_fic no_bx no_t) (OK a_env ((make_event b_env ["write_outvar"%string] [(Exp (EVar z)); (Exp (EVar x)); (Exp no_fic); (Exp no_bx)] None)::b_evn)) (*Logged*)
    
| evalSOInstructionWriteRedund: forall v b_env a_env b_evn r t no_fic no_bx no_t,
    writeRedund t b_env b_evn = v ->
    setIdsValue r v b_env = Some a_env ->
    evalInstruction (b_env, b_evn) (SOInstructionWriteRedund r t no_fic no_bx no_t) (OK a_env ((make_event b_env ["write_redund_array"%string] [(Exp (EVar r)); (Exp (EVar t)); (Exp no_fic); (Exp no_bx)] None)::b_evn)) (*Logged*)

with evalInstruction_s : env * list event -> instruction_s -> interpretationResult -> Prop :=
| evalInstruction_sISeqEnd: forall i env evn res,
    evalInstruction (env, evn) i res ->
    evalInstruction_s (env, evn) (ISeqEnd i) res
| evalInstruction_sISeqConsOk: forall h t env evn env' evn' res,
    evalInstruction (env, evn) h (OK env' evn') ->
    evalInstruction_s (env', evn') t res ->
    evalInstruction_s (env, evn) (ISeqCons h t) res
| evalInstruction_sISeqConsError: forall h t env evn env' evn' e,
    evalInstruction (env, evn) h (Error e env' evn') ->
    evalInstruction_s (env, evn) (ISeqCons h t) (Error e env' evn')

with evalElif_instruction : env * list event -> elif_instruction -> interpretationResult -> Prop :=
| evalInstruction_sIElse: forall i env evn res,
    evalInstruction_s (env, evn) i res ->
    evalElif_instruction (env, evn) (IElse i) res
| evalInstruction_sIElifTrue: forall cond ins_true ins_false env evn res,
    evalSec_opel_expression_if (env, evn) cond (Bool true) ->
    evalInstruction_s (env, evn) ins_true res ->
    evalElif_instruction (env, evn) (IElif cond ins_true ins_false) res
| evalInstruction_sIElifFalse: forall cond ins_true ins_false env evn res,
    evalSec_opel_expression_if (env, evn) cond (Bool false) ->
    evalElif_instruction (env, evn) ins_false res ->
    evalElif_instruction (env, evn) (IElif cond ins_true ins_false) res
    
with evalCase_instruction : env * list event -> list String_Equality.t -> case_instruction -> interpretationResult -> Prop :=
| evalICaseFinishDefaultOk: forall env evn var ins_s env' evn',
    evalInstruction_s (env, evn) ins_s (OK env' evn') ->
    evalCase_instruction (env, evn) var (ICaseFinishDefault ins_s) (Error HorsCode env' evn')
| evalICaseFinishDefaultErr: forall env evn var ins_s e env' evn',
    evalInstruction_s (env, evn) ins_s (Error e env' evn') ->
    evalCase_instruction (env, evn) var (ICaseFinishDefault ins_s) (Error e env' evn')
| evalICaseConsTrue: forall env evn var ins_s exp case_t res,
    evalExpression env (Ebinary Equal (EVar var) exp) (Bool true) ->
    evalInstruction_s (env, evn) ins_s res ->
    evalCase_instruction (env, evn) var (ICaseCons (exp, ins_s) case_t) res
| evalICaseConsFalse: forall env evn var ins_s exp case_t res,
    evalExpression env (Ebinary Equal (EVar var) exp) (Bool false) ->
    evalCase_instruction (env, evn) var case_t res ->
    evalCase_instruction (env, evn) var (ICaseCons (exp, ins_s) case_t) res
.

(* Hint Constructors execFunction evalFunCall evalInstruction evalInstruction_s evalElif_instruction evalCase_instruction : sem_hints. *)

Scheme sem_evalInstruction := Induction for evalInstruction Sort Prop
  with sem_evalInstruction_s := Induction for evalInstruction_s Sort Prop
  with sem_evalElif_instruction := Induction for evalElif_instruction Sort Prop
  with sem_evalCase_instruction := Induction for evalCase_instruction Sort Prop.
Combined Scheme sem_inst_mutind from 
  sem_evalInstruction, sem_evalInstruction_s, sem_evalElif_instruction, sem_evalCase_instruction.


Inductive execFunction : env * list event -> element_declararif -> association_d_arguments -> interpretationResult -> Prop :=
| execFunctionSucced : forall b_env b_evn b_evn' f_params f af_env f_env f_env' a_env a_evn,
    copyIn b_env f.(f_args) f_params f_env ->
    initLocals (f_env, b_evn) f.(f_local_decs) (f_env', b_evn') ->
    evalInstruction_s (f_env', b_evn') f.(f_instrs) (OK af_env a_evn) -> 
    copyOut af_env f.(f_args) f_params b_env a_env ->
    execFunction (b_env, b_evn) f f_params (OK a_env a_evn)
| execFunctionFailed : forall b_env b_evn' b_evn f_params f af_env f_env f_env' a_evn err,
    copyIn b_env f.(f_args) f_params f_env ->
    initLocals (f_env, b_evn) f.(f_local_decs) (f_env', b_evn') ->
    evalInstruction_s (f_env', b_evn') f.(f_instrs) (Error err af_env a_evn) -> 
    execFunction (b_env, b_evn) f f_params (Error err af_env a_evn).

Definition evalElement_declararifAsMain env ed res :=
  execFunction (env, []) ed [] res.

Inductive evalUnit : env -> unit -> interpretationResult -> Prop :=
| evalCompilation_Body : forall ed env res,
    evalElement_declararifAsMain env ed res ->
    evalUnit env (Compilation_Body ed) res.

Fixpoint from_def_to_call_params (params : list (String_Equality.t * mode * name)) : association_d_arguments := match params with
| [] => []
| (id, _, _)::t => ((Exp (EVar [id]))::(from_def_to_call_params t))
end.

Inductive evalDeclaration_de_base : env -> declaration_de_base -> env -> Prop :=
| EvalDeclaration_d_objetConst : forall env v env' id typ e,
    evalExpression env e v -> 
    declareGlobalVar id (ConstantValue v) env = Some env' ->
    evalDeclaration_de_base env (Declaration_d_objet (ConstantVar id typ e)) env'
| EvalDeclaration_d_objetVar : forall env env' id typ,
    declareGlobalVar id (VariableValue (decValue typ)) env = Some env' ->
    evalDeclaration_de_base env (Declaration_d_objet (VariableVar id typ)) env'
| EvalDeclaration_de_sous_programme : forall env env' fid fparams, 
    declareFunction fid fparams env = Some env' ->
    evalDeclaration_de_base env (Declaration_de_sous_programme fid fparams) env'
| EvalDefinition_de_type_Article : forall env id def,
    evalDeclaration_de_base env (Definition_de_type_Article id def) env (*Type non géré*)
| EvalDeclaration_de_sous_type : forall env id def,
    evalDeclaration_de_base env (Declaration_de_sous_type id def) env (*Type non géré*)
| EvalDeclaration_de_surnom : forall env env' env'' fid fparams rname,
    declareFunction fid fparams env = Some env' ->
    defineFunction {| f_name := fid; f_args := fparams; f_local_decs := []; f_instrs := ISeqEnd((ICallDefinedFunction rname (Some (from_def_to_call_params fparams)))) |} env' = Some env'' ->
    evalDeclaration_de_base env (Declaration_de_surnom fid fparams rname) env''
| EvalUse_Clause : forall env, evalDeclaration_de_base env Use_Clause env (*Use non géré*)
| EvalPragma_Declaration : forall p env, evalDeclaration_de_base env (Pragma_Declaration p) env (*pragma non géré*)
.    

Inductive evalDeclaration_de_baseList : env -> list declaration_de_base -> env -> Prop :=
| evalDeclaration_de_baseListNil : forall env, evalDeclaration_de_baseList env [] env
| evalDeclaration_de_baseListCons : forall h t env env' env'',
    evalDeclaration_de_base env h env' -> 
    evalDeclaration_de_baseList env' t env'' ->
    evalDeclaration_de_baseList env (h::t) env''
.

Inductive evalDeclaration_1_body : env -> declaration_1_body -> env -> Prop :=
| EvalBody_Declaration_d_objetConst : forall env v env' id typ e,
    evalExpression env e v -> 
    declareGlobalVar id (ConstantValue v) env = Some env' ->
    evalDeclaration_1_body env (Body_Declaration_d_objet (ConstantVar id typ e)) env'
| EvalBody_Declaration_d_objetVar : forall env env' id typ,
    declareGlobalVar id (VariableValue (decValue typ)) env = Some env' ->
    evalDeclaration_1_body env (Body_Declaration_d_objet (VariableVar id typ)) env'
| EvalBody_Definition_de_type_Article : forall env id def,
    evalDeclaration_1_body env (Body_Definition_de_type_Article id def) env (*Type non géré*)
| EvalBody_Declaration_de_sous_type : forall env id def,
    evalDeclaration_1_body env (Body_Declaration_de_sous_type id def) env (*Type non géré*)
| EvalBody_Use_Clause : forall env, evalDeclaration_1_body env Body_Use_Clause env (*Use non géré*)
| EvalBody_Pragma_Declaration : forall p env, evalDeclaration_1_body env (Body_Pragma_Declaration p) env (*pragma non géré*)
.

Inductive evalDeclaration_1_bodyList : env -> list declaration_1_body -> env -> Prop :=
| evalDeclaration_1_bodyListNil : forall env, evalDeclaration_1_bodyList env [] env
| evalDeclaration_1_bodyListCons : forall h t env env' env'', 
    evalDeclaration_1_body env h env' ->
    evalDeclaration_1_bodyList env' t env'' ->
    evalDeclaration_1_bodyList env (h::t) env''
.

Inductive evalDeclaration_2_body : env -> declaration_2_body -> env -> Prop :=
| EvalFun_dec : forall env env' fid fparams, 
    declareFunction fid fparams env = Some env' ->
    evalDeclaration_2_body env (Fun_dec fid fparams) env'
| EvalFun_renames : forall env env' env'' fid fparams rname,
    declareFunction fid fparams env = Some env' ->
    defineFunction {| f_name := fid; f_args := fparams; f_local_decs := []; f_instrs := ISeqEnd((ICallDefinedFunction rname (Some (from_def_to_call_params fparams)))) |} env' = Some env'' ->
    evalDeclaration_2_body env (Fun_renames fid fparams rname) env''
| EvalFun_def : forall env env' ed, (*Acces incomplet aux fichiers*)
    defineFunction ed env = Some env' ->
    evalDeclaration_2_body env (Fun_def ed) env'
.

Inductive evalDeclaration_2_bodyList : env -> list declaration_2_body -> env -> Prop :=
| evalDeclaration_2_bodyListNil : forall env, evalDeclaration_2_bodyList env [] env
| evalDeclaration_2_bodyListCons : forall h t env env' env'', 
    evalDeclaration_2_body env h env' ->
    evalDeclaration_2_bodyList env' t env'' ->
    evalDeclaration_2_bodyList env (h::t) env''
.