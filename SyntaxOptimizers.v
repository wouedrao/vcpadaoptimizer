Require Import MapTypes Correctness ConvergeOptim1 ConvergeOptim2 ConvergeOptim3 SyntaxOptimizerPropagator InlinePragma Semantics_f.
Require Import List String ZArith Lia.
Local Open Scope Z_scope.
Import ListNotations.

Definition convergeOptim1 := 
  syntax_optimizer_encapsuler_for_compilation (syntax_optimizer_dec_only ConvergeOptim1.convergeoptim1_optimize).
  
Theorem convergeOptim1_correct: forall p dup dec c,
  compilation_correct_optimization p c (convergeOptim1 dup dec).
Proof.
intros.
apply syntax_optimizer_encapsuler_for_compilation_correct.
apply syntax_optimizer_dec_only_is_correct.
intros.
eapply convergeoptim1_optimize_instruction_s_correct; eauto.
Qed.

Definition convergeOptim2 := 
  syntax_optimizer_encapsuler_for_compilation (ConvergeOptim2.convergeoptim2_optimize).
  
Theorem convergeOptim2_correct: forall p dup dec c,
  compilation_correct_optimization p c (convergeOptim2 dup dec).
Proof.
intros.
apply syntax_optimizer_encapsuler_for_compilation_correct.
unfold syntax_optimizer_is_correct.
intros.
eapply convergeoptim2_optimize_instruction_s_correct; eauto.
Qed.

Definition convergeOptim3 := 
  syntax_optimizer_encapsuler_for_compilation (ConvergeOptim3.convergeoptim3_optimize).
  
Theorem convergeOptim3_correct: forall p dup dec c,
  compilation_correct_optimization p c (convergeOptim3 dup dec).
Proof.
intros.
apply syntax_optimizer_encapsuler_for_compilation_correct.
unfold syntax_optimizer_is_correct.
intros.
eapply convergeoptim3_optimize_instruction_s_correct; eauto.
Qed.

Definition pragmaInlineOptim validator := put_pragma_inline_in_compilation validator.

Theorem put_pragma_inline_in_compilation_correct_optimization : forall validator p c,
  compilation_correct_optimization p c (pragmaInlineOptim validator).
Proof.
unfold compilation_correct_optimization.
unfold pragmaInlineOptim.
induction c; simpl.
induction b; eauto; intros; split; eauto.
rewrite H in H0; inversion H0; subst.
intros.
rewrite H1 in H2; inversion H2; subst.
rewrite env_optimization_correct_equiv.
eapply env_optimization_correct_refl.
split.
apply put_pragma_include.
simpl in *.
rewrite H in H0; inversion H0; subst.
intros.
apply evalDeclaration_de_baseList_f_correct in H1, H2.
assert (D := put_pragma_inline_correct (validator t) l {|
         Environment.localVars := [];
         Environment.globalVars := globalV';
         Environment.funDefs := [];
         Environment.localfunDecls := declF';
         Environment.externals := exts'
       |}).
rewrite H1, H2 in D.
inversion D; subst.
rewrite env_optimization_correct_equiv.
eapply env_optimization_correct_refl.
split.
apply include_except_prag_refl.
intros.
rewrite H in H0; inversion H0; subst.
eapply evalDeclaration_1_bodyList_f_correct in H1, H3.
eapply evalDeclaration_2_bodyList_f_correct in H2, H4.
rewrite H1 in H3; inversion H3; subst.
rewrite H2 in H4; inversion H4; subst.
rewrite env_optimization_correct_equiv.
eapply env_optimization_correct_refl.
Qed.
