Require Import Map MapTypes Values Environment Event Semantics Semantics_f Correctness TacUtils OptimUtils.
Require Import Ast.
Require Import List String ZArith Lia.
Import ListNotations.

(* Parameter instruction_s_non_dup : Ast.instruction_s -> bool. *)

Fixpoint split_instrs (function_parameter : Ast.instruction_s) v
  : option (Ast.instruction_s * Ast.instruction_s) :=
  match function_parameter with
  | Ast.ISeqEnd (Ast.SOInstructionConverge _) => 
      Some ((Ast.ISeqEnd INull), Ast.ISeqEnd (Ast.SOInstructionConverge v))
  | Ast.ISeqEnd _ => None
  | Ast.ISeqCons (Ast.SOInstructionConverge _) t => 
      Some ((Ast.ISeqEnd INull), Ast.ISeqCons (Ast.SOInstructionConverge v) t)
  | Ast.ISeqCons h t =>
    match split_instrs t v with
      | None => None
      | Some (nh, nt) => Some ((Ast.ISeqCons h nh), nt)
      end
  end.

Lemma evalInstructions_INullFirst : forall ins_s in_data res,
  evalInstruction_s in_data ins_s res <->
  evalInstruction_s in_data (ISeqCons INull ins_s) res.
Proof.
split; intros.
induction in_data.
eapply evalInstruction_sISeqConsOk; eauto.
eapply evalInstructionINull; eauto.
inversion H; subst.
inversion H3; subst; auto.
inversion H4.
Qed.

Lemma evalInstructions_IConvergeFirst : forall ins_s in_data res v,
  evalInstruction_s in_data ins_s res <->
  evalInstruction_s in_data (ISeqCons (Ast.SOInstructionConverge v) ins_s) res.
Proof.
split; intros.
induction in_data.
eapply evalInstruction_sISeqConsOk; eauto.
eapply evalSOInstructionConverge; eauto.
inversion H; subst.
inversion H3; subst; auto.
inversion H4.
Qed.

Lemma evalInstructions_IEndBranchFirst : forall ins_s in_data res v,
  evalInstruction_s in_data ins_s res <->
  evalInstruction_s in_data (ISeqCons (SOInstructionEndBranch v) ins_s) res.
Proof.
split; intros.
induction in_data.
eapply evalInstruction_sISeqConsOk; eauto.
eapply evalSOInstructionEndBranch; eauto.
inversion H; subst.
inversion H3; subst; auto.
inversion H4.
Qed.

Lemma split_instrs_correct_terminates_correct : forall ins_s in_data res part1 part2 v,
   split_instrs ins_s v = Some (part1, part2) ->
   evalInstruction_s in_data ins_s res -> (exists intr_env intr_evn,
   evalInstruction_s in_data part1 (OK intr_env intr_evn) /\ evalInstruction_s (intr_env, intr_evn) part2 res) 
   \/ (exists e, extractErrorType res = Some e /\ evalInstruction_s in_data part1 res).
Proof.
intros.
generalize dependent part1.
generalize dependent part2.
induction H0; intros; simpl in *.
induction i; inversion H0; subst.
inversion H; subst.
left.
exists env. exists evn.
split; econstructor; eauto; econstructor; eauto.
case_eq (split_instrs t v); intros; rewrite H2 in H1.
induction p.
apply IHevalInstruction_s in H2.
assert ((exists v, h = SOInstructionConverge v) \/ (part1, part2) = (ISeqCons h a, b)).
induction h; inversion H1; subst; auto.
left; exists e; auto.
destruct H3.
destruct H3.
rewrite H3 in *.
inversion H1.
left.
exists env; exists evn.
split.
eapply evalInstruction_sISeqEnd; eauto.
eapply evalInstructionINull; eauto.
eapply evalInstruction_sISeqConsOk; eauto.
inversion H; subst; econstructor; eauto.
inversion H3; subst.
destruct H2.
left.
destruct H2.
exists x.
destruct H2.
exists x0.
destruct H2.
split; eauto.
eapply evalInstruction_sISeqConsOk; eauto.
right.
destruct H2.
destruct H2.
exists x.
split; eauto.
eapply evalInstruction_sISeqConsOk; eauto.
assert (exists v, h = SOInstructionConverge v). 
induction h; inversion H1; subst; auto.
eexists; eauto.
destruct H3.
subst.
inversion H1; subst.
left.
exists env.
exists evn.
split.
eapply evalInstruction_sISeqEnd; eauto.
eapply evalInstructionINull; eauto.
eapply evalInstruction_sISeqConsOk; eauto.
inversion H; subst; econstructor; eauto.
right.
exists e; split ;auto.
assert ((exists s, part1 = ISeqCons h s)).
case_eq h; intros; rewrite H1 in *; eauto; try inversion H; rewrite <- H1 in H0; induction (split_instrs t v); inversion H0; induction a; inversion H0; subst; eauto.
destruct H1.
subst.
eapply evalInstruction_sISeqConsError; eauto.
Qed.

Lemma split_instrs_correct_terminates_OK_iff : forall ins_s in_data res part1 part2 intr_env intr_evn v,
   split_instrs ins_s v = Some (part1, part2) ->
   evalInstruction_s in_data part1 (OK intr_env intr_evn) ->
   evalInstruction_s (intr_env, intr_evn) part2 res ->
   evalInstruction_s in_data ins_s res.
Proof.
induction ins_s; induction in_data; simpl; intros.
induction i; inversion H; subst.
inversion H0; subst; inversion H6; subst; inversion H1; subst; inversion H7; subst; econstructor; eauto; econstructor; eauto.
case_eq (split_instrs ins_s v); intros; [induction p|]; rewrite H2 in *.
induction i; try solve [inversion H; subst; inversion H0; subst; eapply evalInstruction_sISeqConsOk with (env':= env') (evn':=evn'); eauto].
inversion H; subst.
eapply evalInstruction_sISeqConsOk with (env':= a) (evn':=b); eauto.
econstructor; eauto.
inversion H0; subst.
inversion H7; subst.
inversion H1; subst.
inversion H9; subst.
auto.
inversion H9.
induction i; inversion H; subst.
inversion H0; subst.
inversion H7; subst.
inversion H1; subst.
inversion H9; subst.
eapply evalInstruction_sISeqConsOk; eauto.
econstructor; eauto.
inversion H9.
Qed.

Lemma split_instrs_correct_terminates_Error_iff : forall ins_s in_data part1 part2 e intr_env intr_evn v,
   split_instrs ins_s v = Some (part1, part2) ->
   evalInstruction_s in_data part1 (Error e intr_env intr_evn) -> 
   evalInstruction_s in_data ins_s (Error e intr_env intr_evn).
Proof.
induction ins_s; induction in_data; simpl; intros.
induction i; inversion H; subst.
inversion H0.
inversion H5.
case_eq (split_instrs ins_s v); intros; [induction p|]; rewrite H1 in *.
induction i; try solve [inversion H; subst; inversion H0; subst; [eapply evalInstruction_sISeqConsOk with (env':= env') (evn':=evn'); eauto | eapply evalInstruction_sISeqConsError; eauto]].
inversion H; subst.
inversion H0.
inversion H6.
induction i; inversion H; subst.
inversion H0.
inversion H6.
Qed.

Fixpoint insert_at_the_end_instruction 
  (instr_to_insert : Ast.instruction_s)
  (function_parameter : Ast.instruction) : option Ast.instruction :=
  match function_parameter with
  | Ast.IIf cond if_ins_s els_s =>
    match
      ((insert_at_the_end_instruction_s   instr_to_insert
        if_ins_s),
        (insert_at_the_end_elif_instruction instr_to_insert els_s))
      with
    | (None, _) => None
    | (_, None) => None
    | (Some if_ins_s, Some else_ins) => Some (Ast.IIf cond if_ins_s else_ins)
    end
  | Ast.ICase sc case_s =>
    match
      insert_at_the_end_case_instruction   instr_to_insert
        case_s with
    | None => None
    | Some case_s_ => Some (Ast.ICase sc case_s_)
    end
  | _ => None
  end

with insert_at_the_end_instruction_s 
  (instr_to_insert : Ast.instruction_s)
  (i_s : Ast.instruction_s) : option Ast.instruction_s :=
  match i_s with
  | Ast.ISeqEnd (Ast.SOInstructionEndBranch _) =>
      Some instr_to_insert
  | Ast.ISeqEnd i =>
    match insert_at_the_end_instruction   instr_to_insert i with
    | None => None
    | Some i_ => Some (Ast.ISeqEnd i_)
    end
  | Ast.ISeqCons h t =>
    match insert_at_the_end_instruction_s   instr_to_insert t
      with
    | None => None
    | Some t_ => Some (Ast.ISeqCons h t_)
    end
  end

with insert_at_the_end_elif_instruction 
  (instr_to_insert : Ast.instruction_s)
  (function_parameter : Ast.elif_instruction) : option Ast.elif_instruction :=
  match function_parameter with
  | Ast.IElse ins_s =>
    match
      insert_at_the_end_instruction_s   instr_to_insert ins_s
      with
    | None => None
    | Some ins_s_ => Some (Ast.IElse ins_s_)
    end
  | Ast.IElif cond ins_s els_s =>
    match
      ((insert_at_the_end_instruction_s   instr_to_insert ins_s),
        (insert_at_the_end_elif_instruction instr_to_insert els_s))
      with
    | (None, _) => None
    | (_, None) => None
    | (Some ins_s_, Some els_s_) => Some (Ast.IElif cond ins_s_ els_s_)
    end
  end

with insert_at_the_end_case_instruction 
  (instr_to_insert : Ast.instruction_s)
  (function_parameter : Ast.case_instruction) : option Ast.case_instruction :=
  match function_parameter with
  | Ast.ICaseFinishDefault ins_s => Some (Ast.ICaseFinishDefault ins_s)
  | Ast.ICaseCons (c, ins_s) case_s =>
    match
      ((insert_at_the_end_instruction_s   instr_to_insert ins_s),
        (insert_at_the_end_case_instruction instr_to_insert case_s))
      with
    | (None, _) => None
    | (_, None) => None
    | (Some ins_s_, Some case_s_) => Some (Ast.ICaseCons (c, ins_s_) case_s_)
    end
  end.



Theorem insert_at_the_end_OK : 
  (forall in_data init initout,
   evalInstruction in_data init initout -> forall insert res f_ins,
   extractErrorType initout = None -> 
   insert_at_the_end_instruction insert init = Some f_ins ->
   evalInstruction_s (extractEnv initout, extractEvent initout) insert res -> 
   evalInstruction in_data f_ins res) /\ 

  (forall in_data init initout,
   evalInstruction_s in_data init initout -> forall insert res f_ins,
   extractErrorType initout = None -> 
   insert_at_the_end_instruction_s insert init = Some f_ins ->
   evalInstruction_s (extractEnv initout, extractEvent initout) insert res -> 
   evalInstruction_s in_data f_ins res) /\ 

  (forall in_data init initout,
   evalElif_instruction in_data init initout ->
   forall insert res f_ins,
   extractErrorType initout = None -> 
   insert_at_the_end_elif_instruction insert init = Some f_ins ->
   evalInstruction_s (extractEnv initout, extractEvent initout) insert res -> 
   evalElif_instruction in_data f_ins res) /\ 

  (forall in_data e init initout,
   evalCase_instruction in_data e init initout ->
   forall insert res f_ins,
   extractErrorType initout = None -> 
   insert_at_the_end_case_instruction insert init = Some f_ins ->
   evalInstruction_s (extractEnv initout, extractEvent initout) insert res -> 
   evalCase_instruction in_data e f_ins res).
Proof.
apply sem_inst_mutind; intros; simpl in *; try einversion.
case_eq (insert_at_the_end_instruction_s insert ins_if); intros; rewrite H3 in H1; try solve [inversion H1].
case_eq (insert_at_the_end_elif_instruction insert ins_else); intros; rewrite H4 in H1; try solve [inversion H1]; subst.
inversion H1; subst; eapply evalInstructionIIfTrue; eauto.
case_eq (insert_at_the_end_instruction_s insert ins_if); intros; rewrite H3 in H1; try solve [inversion H1].
case_eq (insert_at_the_end_elif_instruction insert ins_else); intros; rewrite H4 in H1; try solve [inversion H1]; subst.
inversion H1; subst; eapply evalInstructionIIfFalse; eauto.
case_eq (insert_at_the_end_case_instruction insert case); intros; rewrite H3 in H1; inversion H1; subst.
eapply evalInstructionICase; eauto.
case_eq (insert_at_the_end_instruction insert i); intros; rewrite H3 in H1; try solve [inversion H1]; subst.
assert ( (exists v, i = SOInstructionEndBranch v /\ f_ins = insert) \/ f_ins = (ISeqEnd i0)).
induction i; inversion H1; subst; eauto.
destruct H4.
destruct H4.
destruct H4.
rewrite H4 in *.
inversion e; subst; eauto.
rewrite H4.
apply evalInstruction_sISeqEnd.
eapply H; eauto.
assert (exists v, i = SOInstructionEndBranch v /\ f_ins = insert).
induction i; inversion H1; subst; eauto.
destruct H4.
destruct H4.
rewrite H4 in *.
inversion e; subst; eauto.
case_eq (insert_at_the_end_instruction_s insert t); intros; rewrite H4 in H2; inversion H2; subst; eauto.
apply evalInstruction_sISeqConsOk with (env':=env') (evn':=evn'); eauto.

case_eq (insert_at_the_end_instruction_s insert i); intros; rewrite H3 in H1; inversion H1; subst; eauto.
apply evalInstruction_sIElse; eauto.
case_eq (insert_at_the_end_instruction_s insert ins_true); intros; rewrite H3 in H1; inversion H1; subst.
clear H5.
case_eq (insert_at_the_end_elif_instruction insert ins_false); intros; rewrite H4 in H1; inversion H1; subst.
eapply evalInstruction_sIElifTrue; eauto.
case_eq (insert_at_the_end_instruction_s insert ins_true); intros; rewrite H3 in H1; inversion H1; subst.
clear H5.
case_eq (insert_at_the_end_elif_instruction insert ins_false); intros; rewrite H4 in H1; inversion H1; subst.
eapply evalInstruction_sIElifFalse; eauto.

case_eq (insert_at_the_end_instruction_s insert ins_s); intros; rewrite H3 in H1; inversion H1; subst.
clear H5.
case_eq (insert_at_the_end_case_instruction insert case_t); intros; rewrite H4 in H1; inversion H1; subst.
eapply evalICaseConsTrue; eauto.
case_eq (insert_at_the_end_instruction_s insert ins_s); intros; rewrite H3 in H1; inversion H1; subst.
clear H5.
case_eq (insert_at_the_end_case_instruction insert case_t); intros; rewrite H4 in H1; inversion H1; subst.
eapply evalICaseConsFalse; eauto.
Qed.

Theorem insert_at_the_end_Err : 
  (forall in_data init initout,
   evalInstruction in_data init initout ->
   forall insert e f_ins, extractErrorType initout = Some e -> 
   insert_at_the_end_instruction insert init = Some f_ins ->
   evalInstruction in_data f_ins initout) /\ 

  (forall in_data init initout,
   evalInstruction_s in_data init initout ->
   forall insert e f_ins, extractErrorType initout = Some e -> 
   insert_at_the_end_instruction_s insert init = Some f_ins ->
   evalInstruction_s in_data f_ins initout) /\ 

  (forall in_data init initout,
   evalElif_instruction in_data init initout ->
   forall insert e f_ins, extractErrorType initout = Some e -> 
   insert_at_the_end_elif_instruction insert init = Some f_ins ->
   evalElif_instruction in_data f_ins initout) /\ 

  (forall in_data ex init initout,
   evalCase_instruction in_data ex init initout ->
   forall insert e f_ins, extractErrorType initout = Some e -> 
   insert_at_the_end_case_instruction insert init = Some f_ins ->
   evalCase_instruction in_data ex f_ins initout).
Proof.
apply sem_inst_mutind; intros; simpl in *; try einversion.
case_eq (insert_at_the_end_instruction_s insert ins_if); intros; rewrite H2 in H1; inversion H1.
clear H4.
case_eq (insert_at_the_end_elif_instruction insert ins_else); intros; rewrite H3 in H1; inversion H1; subst.
eapply evalInstructionIIfTrue; eauto.
case_eq (insert_at_the_end_instruction_s insert ins_if); intros; rewrite H2 in H1; inversion H1.
clear H4.
case_eq (insert_at_the_end_elif_instruction insert ins_else); intros; rewrite H3 in H1; inversion H1; subst.
eapply evalInstructionIIfFalse; eauto.
case_eq (insert_at_the_end_case_instruction insert case); intros; rewrite H2 in H1; inversion H1; subst.
eapply evalInstructionICase; eauto.
case_eq (insert_at_the_end_instruction insert i); intros; rewrite H2 in H1; inversion H1; subst.
clear H4.
assert ( (exists v, i = SOInstructionEndBranch v /\ f_ins = insert) \/ f_ins = (ISeqEnd i0)).
induction i; inversion H1; subst; eauto.
destruct H3.
destruct H3.
destruct H3.
rewrite H3 in *.
inversion e; subst; simpl in H0; einversion.
subst.
apply evalInstruction_sISeqEnd; eauto.
clear H4.
assert (exists v, i = SOInstructionEndBranch v /\ f_ins = insert).
induction i; inversion H1; subst; eauto.
destruct H3.
destruct H3.
subst.
inversion e; subst; simpl in H0; einversion.
case_eq (insert_at_the_end_instruction_s insert t); intros; rewrite H3 in H2; inversion H2; subst.
apply evalInstruction_sISeqConsOk with (env':=env') (evn':=evn'); eauto.
case_eq (insert_at_the_end_instruction_s insert t); intros; rewrite H2 in H1; inversion H1; subst.
eapply evalInstruction_sISeqConsError; eauto.
case_eq (insert_at_the_end_instruction_s insert i); intros; rewrite H2 in H1; inversion H1; subst.
apply evalInstruction_sIElse; eauto.
case_eq (insert_at_the_end_instruction_s insert ins_true); intros; rewrite H2 in H1; inversion H1; subst.
clear H4.
case_eq (insert_at_the_end_elif_instruction insert ins_false); intros; rewrite H3 in H1; inversion H1; subst.
eapply evalInstruction_sIElifTrue; eauto.
case_eq (insert_at_the_end_instruction_s insert ins_true); intros; rewrite H2 in H1; inversion H1; subst.
clear H4.
case_eq (insert_at_the_end_elif_instruction insert ins_false); intros; rewrite H3 in H1; inversion H1; subst.
eapply evalInstruction_sIElifFalse; eauto.
inversion H1; subst.
eapply evalICaseFinishDefaultOk; eauto.
inversion H1; subst.
eapply evalICaseFinishDefaultErr; eauto.
case_eq (insert_at_the_end_instruction_s insert ins_s); intros; rewrite H2 in H1; inversion H1; subst.
clear H4.
case_eq (insert_at_the_end_case_instruction insert case_t); intros; rewrite H3 in H1; inversion H1; subst.
eapply evalICaseConsTrue; eauto.
case_eq (insert_at_the_end_instruction_s insert ins_s); intros; rewrite H2 in H1; inversion H1; subst.
clear H4.
case_eq (insert_at_the_end_case_instruction insert case_t); intros; rewrite H3 in H1; inversion H1; subst.
eapply evalICaseConsFalse; eauto.
Qed.

Fixpoint optimize_instruction instruction_s_non_dup
  (decider : Ast.instruction_s -> Ast.instruction_s -> bool)
  (i : Ast.instruction) : Ast.instruction :=
  match i with
  | Ast.INull => Ast.INull
  | Ast.IPragma p => Ast.IPragma p
  | Ast.IAssign sc aff => Ast.IAssign sc aff
  | Ast.IIf cond ins_s els_s => (Ast.IIf cond (optimize_instruction_s instruction_s_non_dup decider ins_s) (optimize_elif_instruction instruction_s_non_dup decider els_s))
  | Ast.ICase sc case_s => Ast.ICase sc (optimize_case_instruction instruction_s_non_dup decider case_s)
  | Ast.ILoop w ins_s => Ast.ILoop w (optimize_instruction_s instruction_s_non_dup decider ins_s)
  | _ => i
  end

with optimize_elif_instruction instruction_s_non_dup
  (decider : Ast.instruction_s -> Ast.instruction_s -> bool)
  (function_parameter : Ast.elif_instruction) : Ast.elif_instruction :=
  match function_parameter with
  | Ast.IElse ins_s => Ast.IElse (optimize_instruction_s instruction_s_non_dup decider ins_s)
  | Ast.IElif cond ins_s els_s => 
      Ast.IElif cond (optimize_instruction_s instruction_s_non_dup decider ins_s) (optimize_elif_instruction instruction_s_non_dup decider els_s)
  end

with optimize_instruction_s instruction_s_non_dup
  (decider : Ast.instruction_s -> Ast.instruction_s -> bool)
  (function_parameter : Ast.instruction_s) : Ast.instruction_s :=
  match function_parameter with
  | Ast.ISeqEnd ins => Ast.ISeqEnd (optimize_instruction instruction_s_non_dup decider ins)
  | Ast.ISeqCons h t =>
      let h_opt := optimize_instruction instruction_s_non_dup decider h in 
      let t_opt := optimize_instruction_s instruction_s_non_dup decider t in
      let default_result := (Ast.ISeqCons h_opt t_opt) in
      (
        match t_opt with
        | (Ast.ISeqCons (Ast.SOInstructionConverge v) tt_opt) =>
              (
                match split_instrs tt_opt v with
                | None => default_result
                | Some (ih, it) =>
                    (
                        match insert_at_the_end_instruction ih h_opt with
                        | Some inserted => 
                            let new_result := Ast.ISeqCons inserted it in
                            if negb (instruction_s_non_dup ih) then
                            (
                              if decider new_result default_result then
                                new_result
                              else
                                default_result
                            )
                            else
                              default_result
                        | None => default_result
                        end
                    )
                end
              )
        | _ => default_result
        end
      )
  end

with optimize_case_instruction instruction_s_non_dup
  (decider : Ast.instruction_s -> Ast.instruction_s -> bool)
  (function_parameter : Ast.case_instruction) : Ast.case_instruction :=
  match function_parameter with
  | Ast.ICaseFinishDefault ins_s => Ast.ICaseFinishDefault (optimize_instruction_s instruction_s_non_dup decider ins_s)
  | Ast.ICaseCons (exp, ins_s) case_s => Ast.ICaseCons (exp, optimize_instruction_s instruction_s_non_dup decider ins_s) 
        (optimize_case_instruction instruction_s_non_dup decider case_s)
  end.

Lemma optimize_correct :
  (forall indata ins res,
  evalInstruction indata ins res -> forall instruction_s_non_dup d opt,
  opt = optimize_instruction instruction_s_non_dup d ins ->
  evalInstruction indata opt res) /\
  
  (forall indata ins res,
  evalInstruction_s indata ins res -> forall instruction_s_non_dup d opt,
  opt = optimize_instruction_s instruction_s_non_dup d ins ->
  evalInstruction_s indata opt res) /\
  
  (forall indata ins res,
  evalElif_instruction indata ins res -> forall instruction_s_non_dup d opt,
  opt = optimize_elif_instruction instruction_s_non_dup d ins ->
  evalElif_instruction indata opt res) /\
  
  (forall indata e ins res,
  evalCase_instruction indata e ins res -> forall instruction_s_non_dup d opt,
  opt = optimize_case_instruction instruction_s_non_dup d ins ->
  evalCase_instruction indata e opt res).
Proof.
apply sem_inst_mutind; intros; simpl in *; try einversion; try solve [subst; econstructor; eauto].
assert ( opt = ISeqCons (optimize_instruction instruction_s_non_dup d h) (optimize_instruction_s instruction_s_non_dup d t)
    \/
      (exists v tt, optimize_instruction_s instruction_s_non_dup d t = ISeqCons (SOInstructionConverge v) tt) 
    ).
induction (optimize_instruction_s instruction_s_non_dup d t); auto.
clear IHi.
induction i; eauto.
destruct H2.
rewrite H2.
econstructor; eauto.
destruct H2.
destruct H2.
rewrite H2 in H1.
assert ( opt = ISeqCons (optimize_instruction instruction_s_non_dup d h) (ISeqCons (SOInstructionConverge x) x0)
    \/
      (exists ih it, split_instrs x0 x = Some (ih, it)) 
    ).
induction (split_instrs x0 x); auto; induction a.
induction (insert_at_the_end_instruction a (optimize_instruction instruction_s_non_dup d h)); eauto.
destruct H3.
rewrite H3.
econstructor ; eauto.
destruct H3, H3.
rewrite H3 in H1.
assert ( opt = ISeqCons (optimize_instruction instruction_s_non_dup d h) (ISeqCons (SOInstructionConverge x) x0)
    \/
      (exists x, insert_at_the_end_instruction x1 (optimize_instruction instruction_s_non_dup d h) = Some x) 
    ).
induction (insert_at_the_end_instruction x1 (optimize_instruction instruction_s_non_dup d h)); eauto.
destruct H4. 
rewrite H4; econstructor; eauto.
destruct H4; rewrite H4 in H1.
induction (negb (instruction_s_non_dup x1)), (d (ISeqCons x3 x2)
         (ISeqCons (optimize_instruction instruction_s_non_dup d h) (ISeqCons (SOInstructionConverge x) x0))).

apply eq_sym in H2.
assert (D := H0 instruction_s_non_dup d (ISeqCons (SOInstructionConverge x) x0) H2).
inversion D; subst; inversion H10; subst.
assert (D' := split_instrs_correct_terminates_correct x0 (env'0, evn'0) res x1 x2 x H3 H11).
assert (D'' := insert_at_the_end_OK).
destruct D''.
clear H5.
assert (extractErrorType (OK env'0 evn'0) = None) by (simpl; auto).
assert (evalInstruction (env, evn) (optimize_instruction instruction_s_non_dup d h) (OK env'0 evn'0)) by eauto.
destruct D'.
destruct H7, H7, H7.
assert (D'' := H1 (env, evn) (optimize_instruction instruction_s_non_dup d h) (OK env'0 evn'0) H6 x1 (OK x4 x5) x3 H5 H4 H7).
eapply evalInstruction_sISeqConsOk; eauto.
destruct H7, H7.
apply equivExtractErr in H7.
rewrite H7 in *.
eapply evalInstruction_sISeqConsError; eauto.
subst.
eapply evalInstruction_sISeqConsOk; eauto.
rewrite H1; eapply evalInstruction_sISeqConsOk; eauto.
rewrite H1; eapply evalInstruction_sISeqConsOk; eauto.

(*h error*)
assert ( opt = ISeqCons (optimize_instruction instruction_s_non_dup d h) (optimize_instruction_s instruction_s_non_dup d t)
    \/
      (exists v tt, optimize_instruction_s instruction_s_non_dup d t = ISeqCons (SOInstructionConverge v) tt) 
    ).
induction (optimize_instruction_s instruction_s_non_dup d t); eauto.
clear IHi.
induction i; eauto.
destruct H1.
rewrite H1.
eapply evalInstruction_sISeqConsError with (env':=env') (evn':=evn'); eauto.
destruct H1, H1.
rewrite H1 in H0.
assert ( opt = ISeqCons (optimize_instruction instruction_s_non_dup d h) (ISeqCons (SOInstructionConverge x) x0)
    \/
      (exists ih it, split_instrs x0 x = Some (ih, it)) 
    ).
induction (split_instrs x0); auto; induction a.
induction (insert_at_the_end_instruction a (optimize_instruction instruction_s_non_dup d h)); eauto.
destruct H2.
rewrite H2.
eapply evalInstruction_sISeqConsError with (env':=env') (evn':=evn'); eauto.
destruct H2, H2.
rewrite H2 in H0.
assert ( opt = ISeqCons (optimize_instruction instruction_s_non_dup d h) (ISeqCons (SOInstructionConverge x) x0)
    \/
      (exists x, insert_at_the_end_instruction x1 (optimize_instruction instruction_s_non_dup d h) = Some x) 
    ).
induction (insert_at_the_end_instruction x1 (optimize_instruction instruction_s_non_dup d h)); eauto.
destruct H3.
rewrite H3.
eapply evalInstruction_sISeqConsError with (env':=env') (evn':=evn'); eauto.
destruct H3.
rewrite H3 in H0.
induction (negb (instruction_s_non_dup x1)).
induction (d (ISeqCons x3 x2)
         (ISeqCons (optimize_instruction instruction_s_non_dup d h) (ISeqCons (SOInstructionConverge x) x0))).
rewrite H0.
assert (evalInstruction (env, evn) (optimize_instruction instruction_s_non_dup d h) (Error e env' evn')) by eauto.
assert (extractErrorType (Error e env' evn') = Some e) by (simpl; auto).
assert (D := insert_at_the_end_Err).
destruct D.
clear H7.
eapply evalInstruction_sISeqConsError with (env':=env') (evn':=evn'); eauto.
rewrite H0.
eapply evalInstruction_sISeqConsError with (env':=env') (evn':=evn'); eauto.
rewrite H0.
eapply evalInstruction_sISeqConsError with (env':=env') (evn':=evn'); eauto.
Qed.

Definition convergeoptim2_optimize instruction_s_non_dup d ins := fst (optimize_end_branch_number_instruction_s 0 
        (remove_null_instruction_s (optimize_instruction_s instruction_s_non_dup d ins))).

Theorem convergeoptim2_optimize_instruction_s_correct : forall indata ins res instruction_s_non_dup d,
  evalInstruction_s indata ins res -> forall opt,
  opt = convergeoptim2_optimize instruction_s_non_dup d ins ->
  evalInstruction_s indata opt res.
Proof.
intros.
unfold convergeoptim2_optimize in *.
rewrite H0.
eapply optimize_end_branch_number_instruction_s_correct; eauto.
assert (D := remove_null_instruction_correct).
destruct D.
destruct H2.
clear -H0 H2 H.
assert (evalInstruction_s indata (optimize_instruction_s instruction_s_non_dup d ins) res).
eapply optimize_correct; eauto.
eapply H2; eauto.
Qed.

  
  
  
  
  
  
  
  
  
