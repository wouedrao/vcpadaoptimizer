Require Import Map MapTypes Values Environment Event Semantics Semantics_f Correctness TacUtils OptimUtils ConvergeOptim2.
Require Import Ast.
Require Import List String ZArith Lia.
Import ListNotations.

Fixpoint can_insert ins := match ins with
| ISeqEnd(SOInstructionEndBranch _) => true
| ISeqEnd(_) => false
| ISeqCons h t => can_insert t
end.

Fixpoint optimize_instruction instruction_s_non_dup
  (decider : Ast.instruction_s -> Ast.instruction_s -> bool)
  (i : Ast.instruction) : Ast.instruction :=
  match i with
  | Ast.INull => Ast.INull
  | Ast.IPragma p => Ast.IPragma p
  | Ast.IAssign sc aff => Ast.IAssign sc aff
  | Ast.IIf cond ins_s els_s => (Ast.IIf cond (optimize_instruction_s instruction_s_non_dup decider ins_s) (optimize_elif_instruction instruction_s_non_dup decider els_s))
  | Ast.ICase sc case_s => Ast.ICase sc (optimize_case_instruction instruction_s_non_dup decider case_s)
  | Ast.ILoop w ins_s => Ast.ILoop w (optimize_instruction_s instruction_s_non_dup decider ins_s)
  | _ => i
  end

with optimize_elif_instruction instruction_s_non_dup
  (decider : Ast.instruction_s -> Ast.instruction_s -> bool)
  (function_parameter : Ast.elif_instruction) : Ast.elif_instruction :=
  match function_parameter with
  | Ast.IElse ins_s => Ast.IElse (optimize_instruction_s instruction_s_non_dup decider ins_s)
  | Ast.IElif cond ins_s els_s => 
      Ast.IElif cond (optimize_instruction_s instruction_s_non_dup decider ins_s) (optimize_elif_instruction instruction_s_non_dup decider els_s)
  end

with optimize_instruction_s instruction_s_non_dup
  (decider : Ast.instruction_s -> Ast.instruction_s -> bool)
  (function_parameter : Ast.instruction_s) : Ast.instruction_s :=
  match function_parameter with
  | Ast.ISeqEnd ins => Ast.ISeqEnd (optimize_instruction instruction_s_non_dup decider ins)
  | Ast.ISeqCons h t =>
      let h_opt := optimize_instruction instruction_s_non_dup decider h in 
      let t_opt := optimize_instruction_s instruction_s_non_dup decider t in
      let default_result := (Ast.ISeqCons h_opt t_opt) in
      (
        match t_opt with
        | (Ast.ISeqCons (Ast.SOInstructionConverge v) tt_opt) =>
              (
                if can_insert tt_opt then
                  (
                      match insert_at_the_end_instruction tt_opt h_opt with
                      | Some inserted => 
                          let new_result := Ast.ISeqEnd inserted in
                          if negb (instruction_s_non_dup tt_opt) then
                            (
                              if decider new_result default_result then
                                new_result
                              else
                                default_result
                            )
                            else
                              default_result
                      | None => default_result
                      end
                    )
                else
                  default_result
              )
        | _ => default_result
        end
      )
  end

with optimize_case_instruction instruction_s_non_dup
  (decider : Ast.instruction_s -> Ast.instruction_s -> bool)
  (function_parameter : Ast.case_instruction) : Ast.case_instruction :=
  match function_parameter with
  | Ast.ICaseFinishDefault ins_s => Ast.ICaseFinishDefault (optimize_instruction_s instruction_s_non_dup decider ins_s)
  | Ast.ICaseCons (exp, ins_s) case_s => Ast.ICaseCons (exp, optimize_instruction_s instruction_s_non_dup decider ins_s) 
        (optimize_case_instruction instruction_s_non_dup decider case_s)
  end.

Lemma optimize_correct :
  (forall indata ins res,
  evalInstruction indata ins res -> forall instruction_s_non_dup d opt,
  opt = optimize_instruction instruction_s_non_dup d ins ->
  evalInstruction indata opt res) /\
  
  (forall indata ins res,
  evalInstruction_s indata ins res -> forall instruction_s_non_dup d opt,
  opt = optimize_instruction_s instruction_s_non_dup d ins ->
  evalInstruction_s indata opt res) /\
  
  (forall indata ins res,
  evalElif_instruction indata ins res -> forall instruction_s_non_dup d opt,
  opt = optimize_elif_instruction instruction_s_non_dup d ins ->
  evalElif_instruction indata opt res) /\
  
  (forall indata e ins res,
  evalCase_instruction indata e ins res -> forall instruction_s_non_dup d opt,
  opt = optimize_case_instruction instruction_s_non_dup d ins ->
  evalCase_instruction indata e opt res).
Proof.
apply sem_inst_mutind; intros; simpl in *; try einversion; try solve [subst; econstructor; eauto].
assert ( opt = ISeqCons (optimize_instruction instruction_s_non_dup d h) (optimize_instruction_s instruction_s_non_dup d t)
    \/
      (exists v tt, optimize_instruction_s instruction_s_non_dup d t = ISeqCons (SOInstructionConverge v) tt) 
    ).
induction (optimize_instruction_s instruction_s_non_dup d t); auto.
clear IHi.
induction i; eauto.
destruct H2.
rewrite H2.
econstructor; eauto.
destruct H2.
destruct H2.
rewrite H2 in H1.
assert ( opt = ISeqCons (optimize_instruction instruction_s_non_dup d h) (ISeqCons (SOInstructionConverge x) x0)
    \/
      (exists x, insert_at_the_end_instruction x0 (optimize_instruction instruction_s_non_dup d h) = Some x) 
    ).
induction (insert_at_the_end_instruction x0 (optimize_instruction instruction_s_non_dup d h)); eauto.
induction (can_insert x0); eauto.
destruct H3. 
rewrite H3; econstructor; eauto.
destruct H3; rewrite H3 in H1.
induction (can_insert x0).
induction (negb (instruction_s_non_dup x0)).
induction (d (ISeqEnd x1)
         (ISeqCons (optimize_instruction instruction_s_non_dup d h) (ISeqCons (SOInstructionConverge x) x0))).
rewrite H1.
apply eq_sym in H2.
assert (D := H0 instruction_s_non_dup d (ISeqCons (SOInstructionConverge x) x0) H2).
inversion D; subst; inversion H9; subst.
assert (D'' := insert_at_the_end_OK).
destruct D''.
clear H4.
assert (extractErrorType (OK env'0 evn'0) = None) by (simpl; auto).
assert (evalInstruction (env, evn) (optimize_instruction instruction_s_non_dup d h) (OK env'0 evn'0)) by eauto.
assert (D'' := H1 (env, evn) (optimize_instruction instruction_s_non_dup d h) (OK env'0 evn'0) H5 x0 res x1 H4 H3 H10).
econstructor; eauto.
rewrite H1.
eapply evalInstruction_sISeqConsOk; eauto.
rewrite H1.
eapply evalInstruction_sISeqConsOk; eauto.
rewrite H1.
eapply evalInstruction_sISeqConsOk; eauto.

(*h error*)
assert ( opt = ISeqCons (optimize_instruction instruction_s_non_dup d h) (optimize_instruction_s instruction_s_non_dup d t)
    \/
      (exists v tt, optimize_instruction_s instruction_s_non_dup d t = ISeqCons (SOInstructionConverge v) tt) 
    ).
induction (optimize_instruction_s instruction_s_non_dup d t); eauto.
clear IHi.
induction i; eauto.
destruct H1.
rewrite H1.
eapply evalInstruction_sISeqConsError with (env':=env') (evn':=evn'); eauto.
destruct H1, H1.
rewrite H1 in H0.
assert ( opt = ISeqCons (optimize_instruction instruction_s_non_dup d h) (ISeqCons (SOInstructionConverge x) x0)
    \/
      (exists x, insert_at_the_end_instruction x0 (optimize_instruction instruction_s_non_dup d h) = Some x) 
    ).
induction (insert_at_the_end_instruction x0 (optimize_instruction instruction_s_non_dup d h)); eauto.
induction (can_insert x0); eauto.
destruct H2.
rewrite H2.
eapply evalInstruction_sISeqConsError with (env':=env') (evn':=evn'); eauto.
destruct H2.
rewrite H2 in H0.
induction (can_insert x0).
induction (negb (instruction_s_non_dup x0)).
induction (d (ISeqEnd x1)
         (ISeqCons (optimize_instruction instruction_s_non_dup d h) (ISeqCons (SOInstructionConverge x) x0))).
rewrite H0.
assert (evalInstruction (env, evn) (optimize_instruction instruction_s_non_dup d h) (Error e env' evn')) by eauto.
assert (extractErrorType (Error e env' evn') = Some e) by (simpl; auto).
assert (D := insert_at_the_end_Err).
destruct D.
clear H6.
assert (D := H5 (env, evn) (optimize_instruction instruction_s_non_dup d h) (Error e env' evn') H3 x0 e x1 H4 H2).
econstructor; eauto.
rewrite H0.
eapply evalInstruction_sISeqConsError with (env':=env') (evn':=evn'); eauto.
rewrite H0.
eapply evalInstruction_sISeqConsError with (env':=env') (evn':=evn'); eauto.
rewrite H0.
eapply evalInstruction_sISeqConsError with (env':=env') (evn':=evn'); eauto.
Qed.

Definition convergeoptim3_optimize instruction_s_non_dup d ins := fst (optimize_end_branch_number_instruction_s 0 
        (optimize_instruction_s instruction_s_non_dup d ins)).

Theorem convergeoptim3_optimize_instruction_s_correct : forall indata ins res instruction_s_non_dup d,
  evalInstruction_s indata ins res -> forall opt,
  opt = convergeoptim3_optimize instruction_s_non_dup d ins ->
  evalInstruction_s indata opt res.
Proof.
intros.
unfold convergeoptim3_optimize in *.
rewrite H0.
eapply optimize_end_branch_number_instruction_s_correct; eauto.
eapply optimize_correct; eauto.
Qed.
  
  
  
  
  
  
  
  
  
  
