Require Import Environment MapTypes Semantics_f Semantics Values.
Require Import Ast.
Require Import List String ZArith Lia.
Import ListNotations.

Definition eval_unit prep_env c := match c with
| Compilation_Body ed => defineFunction ed prep_env
| Compilation_PkgDecl _ dec => evalDeclaration_de_baseList_f prep_env dec
| Compilation_PkgBody _ b1 b2 => 
    (
      match evalDeclaration_1_bodyList_f prep_env b1 with
      | None => None
      | Some env => evalDeclaration_2_bodyList_f env b2
      end
    )
end.

Fixpoint fusionExternal (ext1: StringMapModule.t (StringMapModule.t External)) ext2 := match ext1 with
| [] => ext2
| (pid, pct)::t => (pid, pct)::(fusionExternal t (StringMapModule.unset pid ext2))
end.

Fixpoint fromVisibleVarsToExt vars := match vars with
| [] => []
| (id, v)::t => StringMapModule.set id (Environment.EVar v) (fromVisibleVarsToExt t)
end.

Fixpoint fromVisibleFunsToExt funs := match funs with
| [] => []
| (fid, params)::t => StringMapModule.set fid (EFDecl params) (fromVisibleFunsToExt t)
end.


Definition extract c c_env := match c with
| Compilation_Body _ => []
| Compilation_PkgDecl _ _ => 
    List.app (fromVisibleVarsToExt (globalVars c_env)) (fromVisibleFunsToExt (localfunDecls c_env))
| Compilation_PkgBody _ _ _ => []
end.

Fixpoint extractGlobalVarsFromExt ext := match ext with
| [] => []
| (id, (Environment.EVar v))::t => StringMapModule.set id v (extractGlobalVarsFromExt t)
| _::t => (extractGlobalVarsFromExt t)
end.

Fixpoint extractFunDecFromExt ext := match ext with
| [] => []
| (fid, (EFDecl params))::t => StringMapModule.set fid params (extractFunDecFromExt t)
| _::t => (extractFunDecFromExt t)
end.

















